#include "LSC_Socket.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <sys/poll.h>
#include <assert.h>
#include <errno.h>
#include <string.h>


LSC_Socket::LSC_Socket(void)
{
  socket = -1;
  isInError = false;
}


void LSC_Socket::connect(const char * host, int port)
{
  if (isOpen() || isError())
    throw LSC_IOException("LSC_Socket already open or had an error");

  sockaddr_in addr;
  addr.sin_family=AF_INET;
  addr.sin_port = htons(port);
  struct hostent* hostname = gethostbyname(host);

  if (hostname == NULL)
  {
    throw LSC_IOException("No such host %s: %s", host, strerror(errno));
  }

  // To avoid a solaris alignment complaint
  assert(sizeof(addr.sin_addr) == sizeof(hostname->h_addr));
  memcpy(&addr.sin_addr, hostname->h_addr, sizeof(hostname->h_addr));
  //  *((long*)&addr.sin_addr) = *(long*)hostname->h_addr;

  int sock = ::socket(PF_INET, SOCK_STREAM, 0);
  if (sock == -1)
    throw LSC_IOException("Could not create socket.");

  if (::connect(sock, (sockaddr*)&addr, sizeof(sockaddr_in)) != 0)
  {
    ::close(sock);
    throw LSC_IOException("Could not connect to %s: %s", host, strerror(errno));
  }

  socket = sock;
}


int LSC_Socket::read(char * buf, int len)
{
  return readWithTimeout(buf, len, -1);
}


int LSC_Socket::readWithTimeout(char * buf, int len, long timeoutMillis)
{
  if (!isOpen() || isError())
    throw LSC_IOException("Socket is not open or had an error");

  try {
    return readWithTimeout(socket, buf, len, timeoutMillis);
  } catch (LSC_IOException ex) {
    setError();
    throw;
  }
}


long LSC_Socket::readIntoCharBuffer(LSC_CharBuffer & buffer)
{
  if (!isOpen() || isError())
    throw LSC_IOException("Socket is not open or had an error");
  return readIntoCharBuffer(socket, buffer);
}


long LSC_Socket::readIntoCharBufferWithTimeout(LSC_CharBuffer & buffer, long minlen, int timeoutSeconds)
{
  if (!isOpen() || isError())
    throw LSC_IOException("Socket is not open or had an error");
  return readIntoCharBufferWithTimeout(socket, buffer, minlen, timeoutSeconds);
}


long LSC_Socket::readIntoCharBuffer(int socket, LSC_CharBuffer & buffer)
{
  if (socket < 0)
    throw LSC_IOException("Socket is not open or had an error");

  // get writable append buffer
  char * wbuf = buffer.getWritable(MaxReadBlockSize);
  long bytesToRead = buffer.getFreeSize();

  int bytesRead = ::read(socket, wbuf, bytesToRead);
  if (bytesRead <= 0)
  {
    buffer.cancelWrite();
    return bytesRead;
  }

  buffer.commitWrite(bytesRead);
  return bytesRead;
}


long LSC_Socket::readIntoCharBufferWithTimeout(int socket, LSC_CharBuffer & buffer, long minlen, int timeoutSeconds)
{
  if (socket < 0)
    throw LSC_IOException("Socket is not open or had an error");

  long endTime, timeLeft;
  long totalBytesRead, bytesRead;
  int ret;

  struct pollfd pollFd[1];
  pollFd[0].fd = socket;
  endTime = time(NULL) + timeoutSeconds;

  totalBytesRead = 0;
  while (1)
  {
    pollFd[0].events = POLLIN;
    timeLeft = endTime - time(NULL);
    if (timeLeft <= 0)
    {
      throw LSC_TimeoutExceededException("CharBuffer::readIntoWithTimeout: timed out");
    }
    ret = poll(pollFd, 1, timeLeft*1000);
    if (0 == ret)
    {
      continue;
    }
    if (ret < 0)
    {
      // On INTR, just try again.
      if (errno == EINTR)
        continue;
      throw LSC_IOException("CharBuffer::readIntoWithTimeout: LSC_IOException");
    }

    // get writable append buffer
    char * wbuf = buffer.getWritable(MaxReadBlockSize);
    long bytesToRead = buffer.getFreeSize();

    bytesRead = ::read(socket, wbuf, bytesToRead);
    if (bytesRead == 0)
    {
      buffer.cancelWrite();
      return totalBytesRead;
    }
    if (bytesRead < 0)
    {
      buffer.cancelWrite();
      throw LSC_IOException();
    }

    buffer.commitWrite(bytesRead);
    totalBytesRead += bytesRead;
    if (totalBytesRead >= minlen)
    {
      //printf("CharBuffer::readIntoWithTimeout: read %d bytes\n", totalBytesRead);
      return totalBytesRead;
    }
  }
}


int LSC_Socket::readFullyWithTimeout(char * buf, int len, long timeoutMillis)
{
  if (!isOpen() || isError())
    throw LSC_IOException("Socket is not open or had an error");

  try {
    return readFullyWithTimeout(socket, buf, len, timeoutMillis);
  } catch (LSC_IOException ex) {
    setError();
    throw;
  }
}


int LSC_Socket::readPascalStringWithTimeout(char * buf, long len, long timeoutMillis)
{
  timeval expiryTime;
  gettimeofday(&expiryTime, NULL);
  expiryTime = timeadd(expiryTime, timeoutMillis*1000);

  char stringLen;
  while (true)
  {
    // Can throw LSC_IOException, LSC_TimeoutExceededException
    int ret = readWithTimeout(&stringLen, 1, timeoutMillis);
    if (ret == 0)
      return 0;
    else
    {
      assert(ret > 0);
      break;
    }
  }

  // Read enough data into buf.
  long bytesRead = 0;
  while (true)
  {
    timeval now;
    gettimeofday(&now, NULL);

    long timeToBlock = timediff(expiryTime, now)/1000;
    // Can throw LSC_IOException, LSC_TimeoutExceededException
    int ret = readWithTimeout(buf+bytesRead, (stringLen<len?stringLen:len)-bytesRead, timeToBlock);
    if (ret == 0)
    {
      setError();
      throw LSC_IOException("Read only %d bytes of a %d-byte string before EOF", bytesRead, stringLen);
    }

    assert(ret > 0);

    bytesRead += ret;
    if (bytesRead == len || bytesRead == stringLen)
      break;

    if (timeToBlock <= 0 && timeoutMillis >= 0)
      throw LSC_TimeoutExceededException("Timeout while reading string data");
  }

  // Discard remaining data in string.
  while (bytesRead < stringLen)
  {
    timeval now;
    gettimeofday(&now, NULL);

    char discard[256];
    long timeToBlock = timediff(expiryTime, now)/1000;
    // Can throw LSC_IOException, LSC_TimeoutExceededException
    int ret = readWithTimeout(discard, (stringLen-bytesRead)<256?stringLen-bytesRead:256, timeToBlock);
    if (ret == 0)
    {
      setError();
      throw LSC_IOException("Read only %d bytes of a %d-byte string before EOF", bytesRead, stringLen);
    }

    assert(ret > 0);
    bytesRead += ret;

    if (bytesRead == stringLen)
      break;

    if (timeToBlock < 0 && timeoutMillis >= 0)
      throw LSC_TimeoutExceededException("Timeout while reading excess string data");
  }

  if (bytesRead < len)
  {
    buf[bytesRead] = 0;
    return bytesRead;
  }

  buf[len-1] = 0;
  return len;
}


void LSC_Socket::writePascalString(char * buf, long timeoutMillis)
{
  assert(strlen(buf) < 256);
  char len = strlen(buf);
  // Can throw LSC_IOException of LSC_TimeoutExceededException

  // This is careless as it can allow timeoutMillis*2 time for the
  // write.  Fix it later.
  write(&len, 1, timeoutMillis);
  write(buf, len, timeoutMillis);
}


void LSC_Socket::write(const char* buf, int len, long timeoutMillis)
{
  if (!isOpen() || isError())
    throw LSC_IOException("Socket is not open or had an error");

  try {
    write(socket, buf, len, timeoutMillis);
  } catch (LSC_IOException ex) {
    setError();
    throw;
  }
}


bool LSC_Socket::isReadable(void)
{
  if (!isOpen() || isError())
    return false;

  struct pollfd pollFd;
  pollFd.events = POLLIN;
  pollFd.fd = socket;

  int ret = poll(&pollFd, 1, 0);
  if (ret == 1)
    return true;

  return false;
}


bool LSC_Socket::isOpen(void) const
{
  if (socket < 0)
    return false;
  return true;
}


int LSC_Socket::getFd(void) const
{
  return socket;
}



void LSC_Socket::close(void)
{
  if (!isOpen())
    // Already closed.
    return;

  assert(socket >= 0);

  ::close(socket);
  isInError = false;
  socket = -1;
}


int LSC_Socket::readWithTimeout(int socket, char* buf, int len, long timeoutMillis)
{
  // Determine expiry time and normalize.
  struct timeval expiryTime;
  gettimeofday(&expiryTime, NULL);
  expiryTime = timeadd(expiryTime, timeoutMillis*1000);
  struct pollfd pollFd;
  pollFd.fd = socket;
  int ret;

  while (true)
  {
    pollFd.events = POLLIN;
    struct timeval now;
    gettimeofday(&now, NULL);
    long timeToBlock = timediff(expiryTime, now)/1000;
    if (timeoutMillis < 0)
      ret = poll(&pollFd, 1, -1);
    else if (timeToBlock < 0)
      ret = poll(&pollFd, 1, 0);
    else
      ret = poll(&pollFd, 1, timeToBlock);

    if (ret == 0)
    {
      // Poll timeout exceeded without error.  If our timeToBlock
      // was already <=0, throw.
      // It's probably ok to throw any time ret == 0, but
      // this code is safer.
      if ((timeToBlock <= 0) && (timeoutMillis >= 0))
        throw LSC_TimeoutExceededException("Timeout exceeded in read");
      continue;
    }

    if (ret < 0)
    {
      if (errno == EINTR)
        continue;
      else
      {
        throw LSC_IOException("Error in poll()");
      }
    }

    ret = ::read(socket, buf, len);

    if (ret > 0)
      return ret;

    // if ret<0, error
    // if zero bytes read, treat it as error
    switch (errno)
    {
      default:
        //LSC_dprintf(1, "ret=%d, errno=%d\n", ret, errno);
        throw LSC_IOException("Unknown error in read");
    }
  }
  // This should not be reachable.
}


int LSC_Socket::readFullyWithTimeout(int socket, char* buf, int len, long timeoutMillis)
{
  timeval expiryTime;
  gettimeofday(&expiryTime, NULL);
  expiryTime = timeadd(expiryTime, timeoutMillis*1000);

  long totalBytesRead = 0;
  while (totalBytesRead < len)
  {
    timeval now;
    gettimeofday(&now, NULL);
    long timeToBlock = timediff(expiryTime, now)/1000;
    long bytesRead = readWithTimeout(socket, buf+totalBytesRead, len-totalBytesRead, timeToBlock);
    if (bytesRead == 0)
      return totalBytesRead;

    totalBytesRead += bytesRead;
    if (totalBytesRead == len)
      return totalBytesRead;

    if ((timeToBlock <= 0) && (timeoutMillis >= 0))
      throw LSC_TimeoutExceededException("Read only %d of %d bytes", totalBytesRead, len);
  }

  return totalBytesRead;
}


void LSC_Socket::write(int socket, const char* buf, int len, long timeoutMillis)
{
  // Determine expiry time and normalize.
  struct timeval expiryTime;
  gettimeofday(&expiryTime, NULL);
  expiryTime = timeadd(expiryTime, timeoutMillis*1000);

  long bytesWritten = 0;

  while (true)
  {
    int err = ::write(socket, buf + bytesWritten, len - bytesWritten);

    if (err > 0)
    {
      bytesWritten += err;
      if (len == bytesWritten)
        return;
      else
        // If we haven't written everything, try again.
        continue;
    }

    switch (errno)
    {
      default:
        // All other things are errors.
        throw LSC_IOException("Unknown error in write");
    }
  }
}


long LSC_Socket::timediff(const timeval& t1, const timeval& t2)
{
  return (t1.tv_sec-t2.tv_sec)*1000000+(t1.tv_usec - t2.tv_usec);
}


timeval LSC_Socket::timeadd(timeval t1, long timeDiffMicros)
{
  timeval tt = t1;
  tt.tv_usec += timeDiffMicros;
  tt.tv_sec  += tt.tv_usec/1000000;
  tt.tv_usec %= 1000000;
  return tt;
}

