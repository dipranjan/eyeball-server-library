// $Id: LSC_critsec.cpp,v 1.9 2005/12/19 18:41:37 larsb Exp $

/** All files of the DNDS Prototype are Copyright (C) 1999 by Andre Schoorl
 *  unless otherwise noted.
 *
 *  The DNDS Prototype is distributed in the public domain in the hope that it
 *  will be useful, but is provided `as is' and is subject to change without
 *  notice.  No warranty of any kind is made with regard to the software or
 *  documentation.  The author shall not be liable for for any error for
 *  incidental or consequential damages in connection with the software and
 *  the documentation.
 *
 *  Permission to copy the whole, unmodified DNDS package is granted provided
 *  that the copies are not made or distributed for resale (excepting nominal
 *  copying fees).  All redistributions in either source or binary form must
 *  include this disclaimer.
 */

#ifdef __linux__
	// Define this to get __USE_UNIX98 for <pthread.h>
	#define _XOPEN_SOURCE 500

	// Explicitly include this first
	#include <features.h>
#endif

#include "LSC_critsec.h"
#include <assert.h>

#define ASSERT(x) assert(x);
#define WARN(x) assert(1 || x);

#ifdef NEVER
#define CRITSEC_ASSERT(X)  ASSERT(X)
#define CRITSEC_DB_WARN(X) DB_WARN(X)
#else
#define CRITSEC_ASSERT(X)  ASSERT(X)
#define CRITSEC_DB_WARN(X)
#endif




LSC_Mutex::LSC_Mutex(void)
{
	init();
#ifndef _NGPT
	cleanup_push();
#endif
}

LSC_Mutex::~LSC_Mutex()
{
#ifndef _NGPT
	cleanup_pop();
#endif
	destroy();
}

void LSC_Mutex::init(void)
{
#ifdef WIN32
	InitializeCriticalSection(&mut);
#elif defined(__linux__)
	pthread_mutexattr_t attr;
	CRITSEC_ASSERT(pthread_mutexattr_init(&attr) == 0);
	#if ((__GLIBC__ >= 2) && (__GLIBC_MINOR__ >= 1))
		CRITSEC_ASSERT(pthread_mutexattr_settype(
			&attr, PTHREAD_MUTEX_RECURSIVE_NP) == 0
		);
	#else
		CRITSEC_ASSERT(pthread_mutexattr_setkind_np(
			&attr, PTHREAD_MUTEX_RECURSIVE_NP) == 0
		);
	#endif
	CRITSEC_ASSERT(pthread_mutex_init(&mut, &attr) == 0);
#elif defined(CRITSEC_RECURSIVE_MUTEX)
	holder = (pthread_t)-1,
	lockCount = 0;

	CRITSEC_ASSERT(pthread_mutex_init(&mut, NULL) == 0);
#else
	CRITSEC_ASSERT(pthread_mutex_init(&mut, NULL) == 0);
#endif
}

void LSC_Mutex::destroy(void)
{
#ifdef WIN32
	DeleteCriticalSection(&mut);
#elif defined(__linux__)
#ifdef _NGPT
	pthread_cleanup_push((void (*)(void *))pthread_mutex_unlock, (void *)&mut);
#endif
	WARN(pthread_mutex_destroy(&mut) == 0);
#ifdef _NGPT
	pthread_cleanup_pop(0);
#endif
#elif defined(CRITSEC_RECURSIVE_MUTEX)
	CRITSEC_DB_WARN(lockCount == 0);
	CRITSEC_DB_WARN(holder == (pthread_t)-1);

	WARN(pthread_mutex_destroy(&mut) == 0);
#else
	WARN(pthread_mutex_destroy(&mut) == 0);
#endif
}


#ifndef _NGPT
void LSC_Mutex::cleanup_push(void)
{
#ifdef WIN32
	// No cleanup handler for Windows
#elif defined(__linux__)// this does not work for new pthreads coming with later glibc's
  //	_pthread_cleanup_push(&_buffer,
  //		(void (*)(void *))pthread_mutex_unlock, (void *)&mut);
#elif defined(__FreeBSD__)
  pthread_cleanup_push((void (*)(void *))pthread_mutex_unlock, (void *)&mut);
#else
	__pthread_cleanup_push((void (*)(void *))pthread_mutex_unlock,
		(void *)&mut, (caddr_t)_getfp(), &_cleanup_info);
#endif
}

void LSC_Mutex::cleanup_pop(void)
{
#ifdef WIN32
	// No cleanup handler for Windows
#elif defined(__linux__)
  //	_pthread_cleanup_pop(&_buffer, 0);
#elif defined(__FreeBSD__)
	pthread_cleanup_pop(0);
#else
	__pthread_cleanup_pop(0, &_cleanup_info);
#endif
}
#endif // _NGPT


void LSC_Mutex::lock(void)
{
#ifdef WIN32
	EnterCriticalSection(&mut);
#elif defined(__linux__)
	CRITSEC_ASSERT(pthread_mutex_lock(&mut) == 0);
#elif defined(CRITSEC_RECURSIVE_MUTEX)
	// Solaris with recursion
	if (pthread_mutex_trylock(&mut) != 0)
	{
		// If holder is self, then self holds the lock already and
		// holder is not subject to change.
		// If holder is not self, then holder may change but never
		// to be self so the condition continues to be false.
		if (pthread_self() == holder)
		{
			lockCount++;
			return;
		}

		CRITSEC_ASSERT(pthread_mutex_lock(&mut) == 0);
	}

	// Mutex should be locked at this point
	CRITSEC_DB_WARN(lockCount == 0);
	holder = pthread_self();
	lockCount = 1;
#else
	// Solaris without recursion
	CRITSEC_ASSERT(pthread_mutex_lock(&mut) == 0);
#endif
}

void LSC_Mutex::unlock(void)
{
#ifdef WIN32
	LeaveCriticalSection(&mut);
#elif defined(__linux)
	CRITSEC_ASSERT(pthread_mutex_unlock(&mut) == 0);
#elif defined(CRITSEC_RECURSIVE_MUTEX)
	CRITSEC_DB_WARN(pthread_self() == holder);

	if (lockCount > 1)
	{
		lockCount--;
		return;
	}

	holder = (pthread_t)-1;
	lockCount = 0;

	CRITSEC_ASSERT(pthread_mutex_unlock(&mut) == 0);
#else
	CRITSEC_ASSERT(pthread_mutex_unlock(&mut) == 0);
#endif
}

LSC_Critical_Section::LSC_Critical_Section(LSC_Mutex& _mutex)
:
	pm(&_mutex)
{
	pm->lock();
}

LSC_Critical_Section::~LSC_Critical_Section()
{
	pm->unlock();
}

