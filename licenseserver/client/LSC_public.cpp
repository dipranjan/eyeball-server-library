#include "LSC_public.h"

#include <stdio.h>
#include <stdarg.h>



void LSC_Exception::setDetails(const char* newDetails, va_list ap)
{
  vsnprintf(details, 500, newDetails, ap);
}



LSC_IOException::LSC_IOException(const char* newDetails, ...)
{
  va_list ap;
  va_start(ap, newDetails);
  setDetails(newDetails, ap);
  va_end(ap);
}



LSC_TimeoutExceededException::LSC_TimeoutExceededException(const char* newDetails, ...)
{
  va_list ap;
  va_start(ap, newDetails);
  setDetails(newDetails, ap);
  va_end(ap);
}



LSC_LicenseUnavailableException::LSC_LicenseUnavailableException(const char* newDetails, ...)
{
  va_list ap;
  va_start(ap, newDetails);
  setDetails(newDetails, ap);
  va_end(ap);
}


