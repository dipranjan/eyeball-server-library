#ifndef _LSC_public_h_
#define _LSC_public_h_

#include <stdarg.h>


/**
 * Mother of all exceptions.
 */
class LSC_Exception
{
 public:
  LSC_Exception(void) { details[0] = 0; }

  void setDetails(const char* newDetails, va_list ap);

  const char * getDetails(void) { return details; }

 protected:
  char details[500];
};


class LSC_IOException : public LSC_Exception
{
 public:
  LSC_IOException(void) : LSC_Exception() { }

  LSC_IOException(const char* newDetails, ...);
};


class LSC_TimeoutExceededException: public LSC_Exception
{
 public:
  LSC_TimeoutExceededException(void) : LSC_Exception() { }

  LSC_TimeoutExceededException(const char* newDetails, ...);
};


class LSC_LicenseUnavailableException: public LSC_Exception
{
 public:
  LSC_LicenseUnavailableException(void) : LSC_Exception() { }

  LSC_LicenseUnavailableException(const char* newDetails, ...);
};


#endif
