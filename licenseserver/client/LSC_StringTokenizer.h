/** All files of the DNDS Prototype are Copyright (C) 1999 by Andre Schoorl
 *  unless otherwise noted.
 *
 *  The DNDS Prototype is distributed in the public domain in the hope that it
 *  will be useful, but is provided `as is' and is subject to change without
 *  notice.  No warranty of any kind is made with regard to the software or
 *  documentation.  The author shall not be liable for for any error for
 *  incidental or consequential damages in connection with the software and
 *  the documentation.
 *
 *  Permission to copy the whole, unmodified DNDS package is granted provided
 *  that the copies are not made or distributed for resale (excepting nominal
 *  copying fees).  All redistributions in either source or binary form must
 *  include this disclaimer.
 */

#ifndef _LSC_StringTokenizer_h_
#define _LSC_StringTokenizer_h_

#include <string>
using std::string;

string LSC_tolower(string _str);
bool LSC_compare_i(const string& _s1, const string& _s2);
string LSC_trim_whitespace(const string& _str);
string LSC_escape_quotes(const string& _str);
string LSC_itoa(const int _x);

class LSC_StringTokenizer
{
	private:
		string str;
		string::size_type pos;

	public:
		LSC_StringTokenizer();
		LSC_StringTokenizer(const string& _str);

		void setString(const string& _str);
		bool nextToken(string& _token);
		
		static string ltoa(const long _x);
};

#endif // AS_TOKEN_H

