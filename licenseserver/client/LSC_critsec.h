// $Id: LSC_critsec.h,v 1.9 2005/12/19 18:41:37 larsb Exp $

/** All files of the DNDS Prototype are Copyright (C) 1999 by Andre Schoorl
 *  unless otherwise noted.
 *
 *  The DNDS Prototype is distributed in the public domain in the hope that it
 *  will be useful, but is provided `as is' and is subject to change without
 *  notice.  No warranty of any kind is made with regard to the software or
 *  documentation.  The author shall not be liable for for any error for
 *  incidental or consequential damages in connection with the software and
 *  the documentation.
 *
 *  Permission to copy the whole, unmodified DNDS package is granted provided
 *  that the copies are not made or distributed for resale (excepting nominal
 *  copying fees).  All redistributions in either source or binary form must
 *  include this disclaimer.
 */

#ifndef _LSC_critsec_h_
#define _LSC_critsec_h_

#ifdef WIN32
#include <windows.h>
#else
#include <pthread.h>
#endif

#ifdef WIN32
	#ifdef DLL_VERSION
		#define DllImport __declspec(dllimport)
		#define DllExport __declspec(dllexport)
	#else
		#define DllImport
		#define DllExport
	#endif

	#ifdef SVVIDEO_EXPORTS
		#define CLASS_DECL_MUTEX DllExport
		#define CLASS_DECL_CRITSEC DllExport
	#else
		#define CLASS_DECL_MUTEX DllImport
		#define CLASS_DECL_CRITSEC DllImport
	#endif
#else
	#define CLASS_DECL_MUTEX
	#define CLASS_DECL_CRITSEC
#endif

#ifdef sun
// Use our own recursive mutex implementation under Solaris
#define CRITSEC_RECURSIVE_MUTEX
#endif

class LSC_Critical_Section;

class CLASS_DECL_MUTEX LSC_Mutex
{
	friend class LSC_Critical_Section;

	private:
#ifdef WIN32
		CRITICAL_SECTION mut;
#else
		pthread_mutex_t mut;

		#ifdef __linux__
			struct _pthread_cleanup_buffer _buffer;
		#elif defined __FreeBSD__
		#elif defined(CRITSEC_RECURSIVE_MUTEX)
			_cleanup_t _cleanup_info;

			// holder and lockCount are protected by mutex.
			// Only test or change if you're holding!
			pthread_t holder;
			int lockCount;
		#else
			_cleanup_t _cleanup_info;
		#endif
#endif

		void init(void);
		void destroy(void);

#ifndef _NGPT
		void cleanup_push(void);
		void cleanup_pop(void);
#endif
	public:
		LSC_Mutex();
		~LSC_Mutex();

		// No cond_wait functionality supported since we provide
		// recursive mutexes by default.
		void lock(void);
		void unlock(void);
};

class CLASS_DECL_CRITSEC LSC_Critical_Section
{
	private:
		LSC_Mutex *pm;

	public:
		LSC_Critical_Section(LSC_Mutex& _mutex);
		~LSC_Critical_Section();
};

#endif // AS_CRITSEC_H

