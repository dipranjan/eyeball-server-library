#include <string>
#include "LSC_Exceptions.h"


class LSC_Url
{
public:
  void setUrl(const char* url);


  std::string getProtocol();


  std::string getHost();


  long getPort(long def);


  std::string getPath();

  
 protected:
  std::string url;
};
