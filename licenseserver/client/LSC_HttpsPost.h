#include "LSC_public.h"
#include "LSC_SslSocket.h"

#include <string>
using std::string;


class LSC_HttpsPost
{
public:
  /**
   * Uses https to post to a url.
   *
   * @param url The url to post to.  Must be well-formed.  Must 
   * be urlEncoded already.
   * @param params HTTP POST params to add to the post.  Must 
   * be urlEncoded already.
   * @param timeoutMillis Time within which the request must complete.
   * @param responseCode Output of the response code from the server.
   * 200 is OK, everything else is an error.
   * @param responseParams The result is assumed to be nothing but 
   * urlEncoded parameters wrapped in an HTTP response.  This is the
   * result as a string.
   * @param responseParamsLen The number of bytes in responseParams.
   * If there are more than this number of bytes in the response,
   * the remainder are lost.
   * @throws LSC_ParseException if url cannot be parsed or it is not https.
   * @throws LSC_IOException if there is an error talking to the host.
   * @throws LSC_TimeoutExceededException if the post is not answered
   * within the timeout.
   *
   * The function can block for more than timeoutMillis time, but
   * it will block only so long as data continues to arrive.
   */
  void post(const char* url, const char* params, long timeoutMillis, long& responseCode, char* responseParams, long responseParamsLen);


  /**
   * Server must provide a certificate that has a distinguishing name
   * of serverName (in addition to the usual certificate verifications).
   * If serverName is "", no verification is done.  "" is the default.
   */
  void setAcceptedServerName(string serverName)
    { acceptedServerName = serverName; }

  /**
   * version 2: needed because of inconsistent openssl behavior
   */
  void setAcceptedServerName2(string serverName)
  { 
    acceptedServerName2 = serverName;
  }


  /**
   * This LSC_SslSocket is used by post to execute the request.  Call
   * setVerify() or other functions on this socket as needed before
   * calling post().  Repeated calls to post() on the same LSC_HttpsPost
   * use the same socket.  Do not call connect(), accept() or close() 
   * on this socket.
   */
  LSC_SslSocket socket;


  static string urlEncode(const string& str);


  static string urlDecode(const string& str);


  /**
   * Extracts and returns the value for a parameter in a parameter string.
   * The paramString must be urlEncoded.  The return value is 
   * urlDecoded.
   *
   * @throws LSC_NoSuchEntryException if the parameter is not in paramString.
   */
  static string getParam(const string& paramString, const char* param);


 protected:
  static unsigned int hex2int(const string& _s);


  string acceptedServerName;
  string acceptedServerName2;
};
