/*! \mainpage License Server
 *
 * The license server stores a list of license agreements with
 * companies authorized to run a software product.  Although the
 * company can make multiple copies of the software, only one copy may
 * execute at a time.  Further, that copy may execute only within the
 * terms of the license agreement.
 *
 * The LicenseServerStub provides access to the license server.
 * The client creates a LicenseServerStub and opens it using the name
 * of the license.  The license is then <i>leased</i> to the client
 * as long as the lease continues to be renewed.  The resulting lease
 * is accompanied by a collection of license terms, which the client
 * is expected to enforce.
 *
 * To run a demo, run licensetest.
 *
 * This file includes the following classes:
 *  LSC_Terms
 *  LSC_License
 *  LicenseServerStub
 */

#include "LSC_public.h"
#include "LSC_critsec.h"
#include "LSC_Log.h"

/**
 * See LSC_Log.h for logging callbacks.
 */


#include <string>
#include <vector>


/**
 * A set of terms.  Each term entry describes a restriction on the license,
 * and collectively they describe all the restrictions on one license.
 * Each entry is a key/value pair.  The key is the term name and describes the
 * term, such as "maxUsers".  The value parameterizes the term such as "5",
 * "01/01/92", or, for terms without parameters, just "".
 */
class LSC_Terms
{
 public:
  LSC_Terms(void) { }


  /**
   * Typical copy constructor.
   */
  LSC_Terms(const LSC_Terms& t2);


  /**
   * Retrieves a single term from this set of terms.
   *
   * @param termName The name of the term to retrieve.
   * @return The term value for termName.  "" if termName is not in
   * the set of terms.
   */
  std::string getTerm(std::string termName) const;


  /**
   * Retrieves a single term from this set of terms.
   *
   * @param index The index of the term within this set.  Indexes are numbered
   * from 0 through getNumTerms()-1.  Indexes do not change, except when
   * setTerm() is called.
   * @param termName The name of the term at index.
   * @param termValue The value of the term at index.
   */
  void getTerm(int index, std::string& termName, std::string& termValue) const;


  /**
   * Returns the number of different term entries for use with
   * getTerm(int, cstring&, cstring&).
   */
  long getNumTerms(void) const;


  /**
   * Adds a term to the set using the name termName and the value termValue.
   * Any added entry can be retrieved with getTerm().
   */
  void setTerm(const std::string& termName, const std::string& termValue);


 protected:
  /**
   * The set of terms.  The array is always packed since deleting is not possible.
   */
  struct Term
  {
    std::string name;
    std::string value;
  };

  std::vector<Term> terms;
};



class LSC_License
{
 public:
  LSC_License(void);


  /**
   * If the lessee hasn't change or if force is true, give the lease
   * to newLesseeName.
   */
  bool lease(std::string newLesseeName, time_t newExpiry, bool force = false);


  /**
   * Releases an existing lease.
   *
   * @param currentLesseeName The name of the current lessee.
   * @return true if lesseeName is the current lessee or the lease
   * is not held.
   */
  bool release(std::string currentLesseeName);


  /**
   * Returns the current or most recent lessee.
   */
  std::string getLesseeName(void) const { return lesseeName; }


  long getLeaseExpiry(void) const;


  /**
   * An identifier for the license.
   */
  std::string id;


  /**
   * The individual/company that owns this license.  This must agree
   * with the x509 sent by the client.
   */
  std::string owner;


  /**
   * The License's "home" server: the server that can give a direct
   * authorative answer regarding this license.  (Other servers
   * must consult this server, thereby giving an indirect
   * authorative answer.)  A string of the form host:port.
   */
  std::string home;


  /**
   * The terms of this license.
   */
  LSC_Terms terms;


 protected:
  long leaseExpiry;

  std::string lesseeName;
};



/**
 * A callback function can be registered to receive log messages.
 * The function is passed a log level, and a const char * message
 * (no newline).
 */

/// actions passed to the callback
enum LSC_LogLevel {LSC_Debug = 0, LSC_Info, LSC_Error};
/// type for callback function
typedef void (*LSC_LogCallback)(LSC_LogLevel, const char *);

extern LSC_LogCallback LSC_logCallback;

/// default callback function
void LSC_defaultCallback(LSC_LogLevel level, const char * message);

/**
 * Change callback function.
 */
void LSC_setLogCallback(LSC_LogCallback callback);



/**
 * Should allow for an explicit revokation from the server.  At least,
 * tryLicense should be able to transition to isLeased=false on
 * certain failure conditions.
 */
class LicenseServerStub
{
 public:
  /**
   *
   */
  LicenseServerStub(void);


  /**
   * A thread is created that periodically renews the lease.
   * The frequency of renewals is according to the renewPeriod.
   *
   * There several conditions for exit:
   *  - the license has expired (has not been renewed).
   *  - there were more than consecutive MaxCritFailedRenewals
   *    critical failures (ie., a NO answer from the server).
   *  - there were more than consecutive MaxFailedRenewals
   *    non-critical failures (ie., no response from the server).
   * A successful renewal resets the failure counters.
   * Upon exit, the thread calls the callback function
   * passing arg as the parameter.
   * Before the callback is called, this license is close()d.
   *
   * Must only be called before open.
   * Default is not to do async renewals.
   *
   * @param on true to turn on async or false to turn it off.
   * @param callback Called when renewal is not possible.  NULL to
   * prevent any function from being called.
   * @param arg is passed to callback() when it is called.  NULL is
   * acceptable.
   */
  typedef void (*AsyncRenewalCallback)(void*);
  void setAsyncRenewal(bool on, AsyncRenewalCallback callback = NULL, void* arg = NULL);


  /**
   * Renew() will request a lease of seconds duration.  Lower
   * values increase the bandwidth required but decrease the
   * time to recover from an unexpected failure.
   * Default is DefaultLeaseDuration.
   * Can be called before or after open().
   */
  void setLeaseDuration(long seconds) { leaseDuration = seconds; }


  /**
   * AsyncRenewals will wait at most between consecutive
   * renew calls.  This is not the same as the duration.
   * For example, the duration is 3 days, and renew period is 1 hour.
   * The duration is counted from the last successful renewal.
   * Should be between MinRenewPeriod and MaxMaxRenewPeriod; if not,
   * it is adjusted.  Recommended value is DefaultRenewPeriod.
   * Default is DefaultRenewPeriod.
   */
  void setMaxRenewPeriod(long seconds);


  /**
   * If set, opening a lease will never fail because another
   * execution holds the lease.  Instead, the lease will be
   * forcably released without the knowledge of the holder
   * and assigned to this execution.  The other execution will
   * fail.
   * Default is off.  Can be called before or after open, but
   * only affects subsequent open()s.
   */
  void setForcedLease(bool on) {forcedLease = on;}


  /**
   * If not set, the connection to the license server will be dropped
   * after a renewal is completed.  Regardless of this setting, the
   * connection to the license server is rebuilt if it has failed for
   * any reason since the last renewal.  Setting this on reduces
   * bandwidth but increases file descriptor and memory use.
   * Default is on.  Can be called at any time to affect future
   * renewals.
   */
  void setPersistentConnection(bool on) {persistentConnection = on;}


  /**
   * When attempting to connect to the license server, this set of
   * host will be used.  Order is ignored.  Default is a set of
   * known hosts.  Normally, this should not be changed.
   * May be called at any time to affect future connections.  Remember
   * that this class may disconnect at any time while it's open.
   *
   * The const char* version takes a semi-colon-delimited string
   * of hosts.
   */
  void setServerHosts(std::vector<std::string>& serverHosts)
    {this->serverHosts = serverHosts;}
  void setServerHosts(const char* serverHosts);


  /**
   * The certificate file must contain a certificate signed by
   * a CA and a matching private key.
   * File must be accessible for read.
   * This must be called before open().
   */
  void setCertificateFile(const char* certificateFile);
  bool setCertificateFile2(const char* certificateFile);


  /**
   * A file containing certificates of all the certificate authorities
   * we trust.  Must be called before open if at all.  The server
   * certificate is verified only if a caFile is specified.  If the
   * server certificate is not trusted or it is not a license server,
   * leases will fail.
   * File must be accessible for read.
   */
  void setCAFile(const char* caFile);
  bool setCAFile2(const char* caFile);


  /**
   * Returns the time at which the last successful renewal executed.
   */
  time_t getLastRenewalTime(void) const { return lastRenewalTime; }


  /**
   * Leases the license.  Either async renewal must be set, or
   * doTryLicense() must be called before getLeaseExpiry(), or the lease
   * will expire.
   *
   * Throws exactly like renew.
   *
   * @param licenseName The name of the license to lease.
   * @param executionName A name unique to this execution (in the whole
   * world).  If "", the library will generate one based on machine
   * information (hostname and process id).
   */
  void open(std::string licenseName, std::string executionName);


  /**
   * Releases an outstanding lease by this client.  Closes connections
   * to the license server.  Async renewals are stopped.
   */
  void close(void);


  /**
   * Renews this license.  This license must already be opened with
   * openLicense().  This must be called periodically, and is called
   * periodically if AsyncRenewal is set.  Updates lastRenewalTime.
   *
   * Throws an exception if the license cannot be obtained for any
   * reason (see details below).  These exceptions do not close this
   * object and do not imply the license is no longer leased, only that
   * the lease could not be renewed.  To determine if the license is
   * leased, call isLeased().  It is acceptable for the caller to
   * catch(...) and treat all exceptions as a failed renewal (no further
   * exception handling is required).
   *
   * @throws LSC_IOException The server could not be contacted or failed
   * to obey the protocol.
   * @throws LSC_LicenseUnavailableException The server communication succeeded,
   * and the server explicitly stated the license was not available.
   */
  void renew(void);


  /**
   * Returns the value of an application-specific license term in
   * the terms file.  Returns "" if the term does not exist.  Must
   * be called only while the license is open.
   */
  std::string getTerm(std::string termName) const;


  /**
   * Returns the time at which the most recently obtained lease will
   * expire, or 0 if it has already expired.  Return is seconds
   * since the epoch, just like time().
   */
  time_t getLeaseExpiry(void) const;


  /**
   * Returns true iff open() has been called and close() hasn't.
   */
  bool isOpen(void) const { return opened; }


  /**
   * Returns true iff a renewal has been made and not expired.
   */
  bool isLeased(void) const { return opened && leased; }


  /**
   * Get the associated LSC_License object.
   * Returns NULL if the license is not open.
   */
  const LSC_License * getLicense(void) const;

  /**
   * Get the connection status to the license server
   * Returns true if the license server is repsonding to posts
   */
   bool isLicenseConnected() const {return m_bLicenseConnected;}        

  /**
   * The message is logged at the remote server before another lease
   * is granted and before the license is closed.  If it cannot be
   * logged for the lease duration, the lease will expire.  Logging
   * on the license home server is not garaunteed as that host may
   * be down.  This object must be opened before this call is made.
   */
  void remoteLogOld(const char* message);

	/**
	 * remoteLog() function has a serious flaw. When async license is used,
	 * at renew time, a mutex is locked that remoteLog is waiting. If
	 * there is some problem with license servers, remoteLog can block
	 * for several minutes (#licnse servers * 40 seconds).
	 *
	 * To solve this problem, this function uses a separate mutex.
	 * I also changed the function names, so everyone will use remoteLog.
	 * The orginal function was renamed to remoteLogOld.
	 */
	void remoteLog(const char* message);


  /// Default value for the duration -- 6 hours.
  static const long DefaultLeaseDuration = 6*60*60;


  /// Default value for the renew period -- 1 hour.
  static const long DefaultRenewPeriod = 60*60; // one hour


  /// Lower limit on the renewPeriod.
  static const long MinRenewPeriod = 60*60;   // one hour


  /// Hard limit, never try renew more often than this.
  static const long MinMinRenewPeriod = 60*60;//15; // one hour, modified to avoid server exit within 5 mins after license failure


  /// Hard limit on the renew period, 2 hours.
  static const long MaxMaxRenewPeriod = 2*60*60;


  /**
   * If we fail for this many times critically, give up and exit.
   */
  static const long MaxCritFailedRenewals = 24*10;   // the number of hours must be given here


  /**
   * If we fail for this many times, give up and exit.
   */
  static const long MaxFailedRenewals = 24*10;       // the number of hours must be given here


 protected:
  /**
   * Connects (if needed) and talks to the server to obtain a lease.
   * Succeeds unless the function throws.  Tries each of the servers
   * in the serverHosts, and throws only if all attempts failed
   * (in which case, the last error is thrown).
   *
   * The ForServer version acts on one server only.  serverName
   * is a host name optionally followed by a ':' and port.
   *
   * Throws like renew().
   *
   * @param forced If true, the lease request is forced, overriding
   * any existing lease on this license.
   */
  void doTryLicense(bool force);
  void doTryLicenseForServer(std::string serverName, bool force);


  /**
   * Connects (if needed) and talks to the server to release a lease.
   * Succeeds unless the function throws.
   *
   * The ForServer version acts on one server only.  serverName 
   * is a host name optionally followed by a ':' and port.
   *
   * @throws LSC_IOException The server could not be contacted or failed
   * to obey the protocol.
   * @throws LSC_LicenseUnavailableException The server communication succeeded,
   * and the server explicitly stated the license does not exist or
   * is not held by this lessee (eg. it has been forcably taken by
   * another).
   */
  void doReleaseLicense(void);
  void doReleaseLicenseForServer(std::string serverHost);


  /**
   * Converts a license encoded as HTTP params into a License
   * class.  Expects fields to be id, lesseeName, expiryTime, and
   * term0, term1, ....
   *
   * @throws LSC_ParseException if there is a syntax error in the
   * response (fields are missing or terms do not have values).
   */
  void paramsToLicense(std::string params, LSC_License& newLicense);


  /**
   * The socket can open and close independant of whether this
   * object is open or closed.  The socket needs to be connected
   * while trying for a lease, and it might be connected at other
   * times.  A failure of the socket while trying a lease doesn't
   * cause this object to become closed.  Instead, the lease is tried
   * again later.
   */
  //  void openSocket();
  //  void closeSocket();


  /**
   * Reads data into socketIn until a full string (or more) is read.
   * Returns that string and removes it from socketIn.  If it throws,
   * the closeSocket() has already been called.
   *
   * @throws LSC_IOException if there is an error reading the stream.
   * @throws LSC_TimeoutExceededException if the data is not read before
   * timeoutTime.
   */
  //  std::string socketReadLine(time_t timeoutTime);


  /**
   * Called with a new thread on open().  Exits when no longer open.
   * Probably leaks stacks because nobody ever joins or otherwise
   * destroys this thread.
   */
  friend void* LicenseServerStub_autoRenew(void* arg);
  void autoRenew(void);


  bool asyncRenewal;
  bool m_bLicenseConnected; // webtools
  AsyncRenewalCallback asyncRenewalCallback;
  void* asyncRenewalArg;


  //  SslSocket sslSocket;


  //  CharBuffer socketIn;


  /**
   * Name of this execution.  Set on open.
   */
  std::string executionName;


  /**
   * License id for the license this object will renew.  Set on open.
   */
  std::string licenseId;


  /**
   * The license this LicenseServerStub represents.  Set iff this
   * object is open.
   */
  LSC_License license;


  /**
   * Seconds to request a lease for.
   */
  long leaseDuration;


  /**
   * Seconds to wait before a renew.
   */
  long renewPeriod;


  /**
   * true iff the license is "open" and would like to own the lease.
   * Test leased to see if the license is actually leased.
   */
  bool opened;


  /**
   * true iff the license is confirmed as leased, ie. We have
   * succeeded in a tryLicense.  If open is false, then leased
   * is false.  May be redundant to
   * (isOpen() && license.getExpiryTime() > 0).
   */
  bool leased;


  time_t lastRenewalTime;


  time_t lastRenewalAttemptTime;


  bool persistentConnection;


  bool forcedLease;


  /**
   * A set of hosts that can be used to connect to.
   */
  std::vector<std::string> serverHosts;


  /**
   * File containing a signed certificate and a private key for
   * this client.
   */
  std::string certificateFile;


  /**
   * File containing the certificates of all the certificate authorities
   * we trust.
   */
  std::string caFile;


  /**
   * Stores the serverHost from which the license was last successfully
   * renewed.  This server is tried first.  In the case of a redirect,
   * it may not even be in the serverHosts set.
   */
  std::string lastSuccessfulServerHost;


  /**
   * An ordered list of messages that must be logged at the remote
   * server.
   */
  std::vector<std::string> remoteLogMessages;


  /**
   * Protects all the variables in this object.  Hold it to read
   * or modify anything.
   */
  LSC_Mutex lock;

	/**
	 * Protects the remoteLogMessages structure.
	 */
	LSC_Mutex m_MessageLock;
};

void* LicenseServerStub_autoRenew(void* arg);





