#include "LSC_Log.h"
#include "LicenseServerStub.h"  // for callback

#include <stdio.h>
#include <stdarg.h>
#include <assert.h>


void LSC_Log(int level, char const* str, ...)
{
  char line[1000];
  va_list ap;
  va_start(ap, str);
  vsprintf(line, str, ap);
  va_end(ap);

  // call callback
  if (NULL != LSC_logCallback)
    {
      switch (level)
	{
	case 1:  (*LSC_logCallback)(LSC_Debug, line);   break;
	case 2:  (*LSC_logCallback)(LSC_Info,  line);   break;
      
	case 3:
	case 4:  
	default: // what else? treat it as error too
	  (*LSC_logCallback)(LSC_Error, line);   break;
	}
    }
  else
    {
      // fallback
      printf("%d: %s\n", level, line);
    }

}


