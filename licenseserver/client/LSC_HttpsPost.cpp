#include "LSC_HttpsPost.h"
#include "LSC_Url.h"
#include "LSC_CharBuffer.h"
//#include "LSC_Log.h"


void LSC_HttpsPost::post(const char* urlStr, const char* params, long timeoutMillis, long& responseCode, char* responseParams, long responseParamsLen)
{
  string host, path;
  long port;
  { // Can throw a LSC_ParseException
    LSC_Url url;
    url.setUrl(urlStr);

    host = url.getHost();
    port = url.getPort(443);
    path = url.getPath();

    if (url.getProtocol() != "https")
      throw LSC_ParseException("Url is not an https url");
  }

  // build request.
  string postLine("POST ");
  postLine += path;
  postLine += " HTTP/1.0\n";
  postLine += "Content-type: application/x-www-form-urlencoded\n";
  string paramsStr(params);
  char buf[100];
  sprintf(buf, "%d", paramsStr.length());
  postLine = postLine + "Content-length: " + buf + "\n\n" + paramsStr;

  // Can throw an LSC_IOException
  try {
//    socket.connect(host.c_str(), port);
    socket.connect(host.c_str(), port, timeoutMillis);
  } catch (LSC_IOException ex) {
    //    fprintf(stderr, "LSC_HttpsPost::post: %s\n", ex.getDetails());
    socket.close();
    throw ex;
  }

  // finish negotiation, can throw
  try {
    socket.finishNegotiation(timeoutMillis);
  } catch (LSC_IOException ex) {
    fprintf(stderr, "LSC_HttpsPost::post: %s\n", ex.getDetails());
    socket.close();
    throw ex;
  }

  if (acceptedServerName != "")
    {
      // Server must always provide a cert.
      X509* serverCert = socket.getPeerCertificate();
      char buf[5000];
      X509_NAME_oneline(X509_get_subject_name(serverCert), buf, 5000);
      buf[4999] = 0;
      X509_free(serverCert);
      serverCert = NULL;
      string returnedServerName = string(buf);
//      LSC_Log(1, "Comparing provided server subject DN of %s (%i) to accepted name of %s (%i)\n", returnedServerName.c_str(), returnedServerName.size(), acceptedServerName.c_str(), acceptedServerName.size());

      // 06/01/03 (lars)
      // we have a problem with the string geenrated by openssl, so we have to check two versions here
      if ((acceptedServerName != returnedServerName)&&(acceptedServerName2 != returnedServerName))
	throw LSC_IOException("Server name is %s, not %s nor %s.", returnedServerName.c_str(), acceptedServerName.c_str(), acceptedServerName2.c_str());
    }

  // send request
  try {
    socket.write(postLine.c_str(), postLine.length(), timeoutMillis);
  } catch (LSC_IOException ex) {
    //    fprintf(stderr, "LSC_HttpsPost::post: %s\n", ex.getDetails());
    socket.close();
    throw ex;
  }


  LSC_CharBuffer responseBuf;
  // Get response params.  Store the entire response in memory.
  while (responseBuf.getLength() < 10000)
    {
      char buf[1000];
      // Can throw LSC_IOException
      long bytesRead = -1;
      try {
	// We're using HTTP/1.0 so we must get an EOF on the ssl
	// at the end of the data.  HTTP/1.1 persistent connections
	// wreck this assumption so I don't use it.
	//	printf("LSC_HttpsPost.post: started read.\n");
	bytesRead = socket.readWithTimeout(buf, 1000, timeoutMillis);
	//	printf("LSC_HttpsPost.post: done read, bytes=%ld.\n", bytesRead);
      } catch (LSC_Exception ex) {
	//	printf("ex=%s\n", ex.getDetails());
	socket.close();
	throw;
	//break;
      }
      if (bytesRead == 0)
	break;

      responseBuf.add(buf, bytesRead);
    }

  socket.close();

  if (responseBuf.getLength() < 9)
    throw LSC_IOException("Timed out, not enough data read to form an http response.");
  responseBuf.discard(9);

  // Ensure it's null-termed for the atoi.
  char nullTerm = 0;
  responseBuf.add(&nullTerm, 1);

  //  printf("Response header=%s", responseBuf.getBase());

  responseCode = atoi(responseBuf.getBase());
  while (true)
    {
      long charsDiscarded = responseBuf.discardTo("\n");
      if (charsDiscarded == -1)
	throw LSC_IOException("No blank line in http response");
      if (responseBuf.getLength() > 0 &&
	  responseBuf.getBase()[0] == '\n')
	{
	  responseBuf.discard(1);
	  break;
	}
      if (responseBuf.getLength() > 1 &&
	  responseBuf.getBase()[0] == '\r' &&
	  responseBuf.getBase()[1] == '\n')
	{
	  responseBuf.discard(2);
	  break;
	}
    }

  int bytesToCopy = responseBuf.getLength();
  if (bytesToCopy > responseParamsLen-1)
    bytesToCopy = responseParamsLen-1;
  memcpy(responseParams, responseBuf.getBase(), bytesToCopy);
  responseParams[bytesToCopy] = 0;
}


string LSC_HttpsPost::urlEncode(const string& str)
{
  char szHexBuf[4];
  string rc(str);

  for (string::size_type i=0; i < rc.length(); i++)
    {
      if (isalnum(rc[i]))
	{
	  // Alpha-numeric, leave as is
	  continue;
	}
      else if (rc[i] == '\r')
	{
	  // Character we want to remove
	  rc.erase(i, 1);
	  i -= 1;
	}
      else
	{
	  // Character we need to encode
	  // Syntax is %XX in hex - adds two characters
	  sprintf(szHexBuf, "%%%.2hX", (unsigned char)rc[i]);
	  rc.replace(i, 1, szHexBuf);
	  i += 2;
	}
    }
  
  return rc;
}


unsigned int LSC_HttpsPost::hex2int(const string& _s)
{
  unsigned int t = 0;
  string::const_iterator i;
  for(i=_s.begin();i!=_s.end();i++)
    if(*i>='0' && *i<='9')
      t = (t<<4)+(*i-'0');
    else if(*i>='a' && *i<='f')
      t = (t<<4)+(*i-'a'+10);
    else if(*i>='A' && *i<='F')
      t = (t<<4)+(*i-'A'+10);
    else
      break;
  return t;
}


string LSC_HttpsPost::urlDecode(const string& str)
{
  string decoded;
  for(string::size_type i=0; i<str.length(); i++)
    if(str[i] == '+')
      decoded += ' ';
    else if(str[i] == '%') {
      decoded += (char) hex2int(string(str,i+1,2));
      i+=2;
    }
    else
      decoded += str[i];
  return decoded;
}


string LSC_HttpsPost::getParam(const string& paramStr, const char* paramName)
{
  string paramNameStr = paramName;
  paramNameStr += "=";

  string::size_type index = 0;

  // Loop until we find the paramName at the begining of a field name.
  // index 
  while (true)
    {
      index = paramStr.find(paramName, index);

      if (index == string::npos)
	throw LSC_NoSuchEntryException("%s does not appear in the param string %s", paramName, paramStr.c_str());

      if (index == 0 ||
	  paramStr[index-1] == '\n' ||
	  paramStr[index-1] == '&')
	// This is the beginning of a field name.
	break;
      
      index++;
    }

  string::size_type respStart = index + paramNameStr.length();

  string::size_type respEndAmpersand = paramStr.find("&", respStart);
  string::size_type respEndLF = paramStr.find("\n", respStart);
  string::size_type respEndCR = paramStr.find("\r", respStart);
  string::size_type respEnd = 500000;
  if (respEndAmpersand != string::npos && respEndAmpersand < respEnd)
    respEnd = respEndAmpersand;
  if (respEndLF != string::npos && respEndLF < respEnd)
    respEnd = respEndLF;
  if (respEndCR != string::npos && respEndCR < respEnd)
    respEnd = respEndCR;
  if (respEnd == 500000)
    {
      string ret = paramStr.substr(respStart);
      return urlDecode(ret);
    } else {
      string ret = paramStr.substr(respStart, respEnd-respStart);
      return urlDecode(ret);
    }
}
