#include "LSC_Exceptions.h"

#include <stdio.h>
#include <stdarg.h>


LSC_ParseException::LSC_ParseException(const char* newDetails, ...)
{
  va_list ap;
  va_start(ap, newDetails);
  setDetails(newDetails, ap);
  va_end(ap);
}



LSC_NoSuchEntryException::LSC_NoSuchEntryException(const char* newDetails, ...)
{
  va_list ap;
  va_start(ap, newDetails);
  setDetails(newDetails, ap);
  va_end(ap);
}



LSC_FileNotFoundException::LSC_FileNotFoundException(const char* newDetails, ...)
{
  va_list ap;
  va_start(ap, newDetails);
  setDetails(newDetails, ap);
  va_end(ap);
}


