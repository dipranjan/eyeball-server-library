#include "LicenseServerStub.h"
#include "LSC_StringTokenizer.h"
#include "LSC_HttpsPost.h"
#include "LSC_Log.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <errno.h>

#include <iostream>
#include <signal.h>

using std::string;
using std::vector;


using namespace std;

LSC_LogCallback LSC_logCallback = &LSC_defaultCallback;



void LSC_setLogCallback(LSC_LogCallback callback)
{
  assert(NULL != callback);
  LSC_logCallback = callback;
}


/// default callback function
void LSC_defaultCallback(LSC_LogLevel level, const char * message)
{
  switch (level)
  {
   case LSC_Error:
     cerr << message;
     //    printf("LSC Error: %s\n", message);
    break;

   case LSC_Info:
     cerr << message;
     //    printf("LSC: %s\n", message);
    break;

   case LSC_Debug:
     //printf("(lsc: %s)\n", message);
     cerr << message;
    break;
  }
}



LSC_Terms::LSC_Terms(const LSC_Terms& t2)
{
  terms = t2.terms;
}


string LSC_Terms::getTerm(string termName) const
{
  for (unsigned int i = 0; i < terms.size(); i++)
  {
    if (termName == terms[i].name)
      return terms[i].value;
  }

  return "";
}


void LSC_Terms::getTerm(int index, string& termName, string& termValue) const
{
  termName = terms[index].name;
  termValue = terms[index].value;
}


long LSC_Terms::getNumTerms(void) const
{
  return terms.size();
}


void LSC_Terms::setTerm(const string& termName, const string& termValue)
{
  for (unsigned int i = 0; i < terms.size(); i++)
  {
    if (termName == terms[i].name)
    {
      terms[i].value = termValue;
      return;
    }
  }

  Term t;
  t.name = termName;
  t.value = termValue;
  terms.push_back(t);
}



LSC_License::LSC_License(void)
{
  leaseExpiry = 0;
}


bool LSC_License::lease(string newLesseeName, time_t newExpiry, bool force)
{
  //  printf("License.lease (%ld): lessee=%s, expiry=%ld, newLessee=%s, newExpiry=%ld\n",
  //	 time(NULL),
  //	 lesseeName.c_str(), leaseExpiry, newLesseeName.c_str(), newExpiry);

  //  printf("License.lease: !force=%d, 2=%d, 3=%d, 4=%d\n",
  //	 !force, leaseExpiry > time(NULL), newLesseeName != lesseeName,
  //	 lesseeName != "");
  if (!force &&
      leaseExpiry > time(NULL) &&
      newLesseeName != lesseeName &&
      lesseeName != "")
    {
      return false;
    }

  lesseeName = newLesseeName;
  leaseExpiry = newExpiry;
  //  printf("License.lease: set leaseExpiry to %ld\n", newExpiry);
  return true;
}


bool LSC_License::release(string currentLesseeName)
{
  if (leaseExpiry > time(NULL) &&
      lesseeName != currentLesseeName)
    return false;

  leaseExpiry = 0;
  return true;
}


long LSC_License::getLeaseExpiry(void) const
{
  if (leaseExpiry <= (long)time(NULL) )
    return 0;
  else
    return leaseExpiry;
}



LicenseServerStub::LicenseServerStub(void)
{
  asyncRenewal = false;
  asyncRenewalCallback = NULL;
  asyncRenewalArg = NULL;

  opened = false;
  leased = false;

  lastRenewalTime = 0;
  lastRenewalAttemptTime = 0;

  leaseDuration = DefaultLeaseDuration;
  renewPeriod = DefaultRenewPeriod;
  persistentConnection = true;
  forcedLease = false;
  m_bLicenseConnected = true;

  //  serverHosts.push_back(string("patricia"));
}


/**
 * Creates a thread that periodically updates the terms files and
 * stores it.
 */
void LicenseServerStub::setAsyncRenewal(bool on, AsyncRenewalCallback callback, void* arg)
{
  asyncRenewal = on;
  asyncRenewalCallback = callback;
  asyncRenewalArg = arg;

}


void LicenseServerStub::setCertificateFile(const char* certificateFile)
{
  if (!setCertificateFile2(certificateFile)) {
    exit(2);
  }
}

bool LicenseServerStub::setCertificateFile2(const char* certificateFile)
{
  // check if file exists
  int fd = ::open(certificateFile, O_RDONLY);
  if (fd < 0)
  {
    LSC_Log(4, "LicenseServerStub::setCertificateFile: Could not open file '%s', err=%d.", 
      certificateFile, errno);

    return false;
  }
  ::close(fd);

  this->certificateFile = certificateFile;
  return true;
}


void LicenseServerStub::setCAFile(const char* caFile)
{
  if (!setCAFile2(caFile)) {
    exit(2);
  }
}

bool LicenseServerStub::setCAFile2(const char* caFile)
{
  // check if file exists
  int fd = ::open(caFile, O_RDONLY);
  if (fd < 0)
  {
    LSC_Log(4, "LicenseServerStub::setCAFile: Could not open file '%s', err=%d.", 
      caFile, errno);

    return false;
  }
  ::close(fd);

  this->caFile = caFile;
  return true;
}


void LicenseServerStub::setMaxRenewPeriod(long seconds)
{
  renewPeriod = seconds;
  // limit it to MinRenewPeriod and MaxMaxRenewPeriod
  if (renewPeriod < MinRenewPeriod)
    renewPeriod = MinRenewPeriod;
  if (renewPeriod > MaxMaxRenewPeriod)
    renewPeriod = MaxMaxRenewPeriod;
}


const LSC_License * LicenseServerStub::getLicense(void) const
{
  if (!isOpen())
    return NULL;
  return &license;
}


void LicenseServerStub::open(string licenseId, string executionName)
{
  assert(!isOpen());
  assert(!isLeased());

  if (executionName == "")
  {
    char hostname[500];
    int ret = gethostname(hostname, 500);
    if (ret != 0)
      hostname[0] = 0;
    executionName = hostname + LSC_StringTokenizer::ltoa(getpid());
  }

  this->licenseId = licenseId;
  this->executionName = executionName;
  this->leaseDuration = leaseDuration;

  LSC_Log(2, "LicenseId: '%s'  my clientId: '%s'.",
    licenseId.c_str(), executionName.c_str());

  try
  {
    doTryLicense(forcedLease);
  }
  catch (LSC_IOException ex)
  {
    // failed to connect
    LSC_Log(4, "Failed to connect to monitoring server: %s.", ex.getDetails() );
    throw ex;
  }
  catch (LSC_LicenseUnavailableException ex)
  {
    // failed to connect
    LSC_Log(4, "Failed to lease license: %s.", ex.getDetails() );
    throw ex;
  }

  opened = true;
  leased = true;
  long now = time(NULL);
  LSC_Log(2, "License lease obtained, %ld secs till expiry.", license.getLeaseExpiry() - now);

  if (asyncRenewal)
  {
    pthread_t tid;
    // Should create this as daemon: no need to join, does not
    // inhibit exit().
    /*int res =*/ pthread_create(&tid, NULL, LicenseServerStub_autoRenew, this);
  }
}


void* LicenseServerStub_autoRenew(void* arg)
{
  ((LicenseServerStub*)arg)->autoRenew();
  return NULL;
}


void LicenseServerStub::autoRenew(void)
{
  /**
   * Number of critically failed renewals since the last successful renewal.
   */
  long numCritFailedRenewals = 0;
  /**
   * Number of failed renewals since the last successful renewal.
   */
  long numFailedRenewals = 0;

  while (true)
    {
      time_t now = time(NULL);
      lock.lock();
      if (!isOpen() || !asyncRenewal)
	{
	  lock.unlock();
	  break;
	}

      time_t expiryTime = license.getLeaseExpiry();
      bool pastExpiry = false;
      if ((expiryTime == 0)&&(numFailedRenewals >= MaxFailedRenewals))
	pastExpiry = true;

      time_t nextRenewal;

      if (pastExpiry)
	{
	  if (numFailedRenewals <= 1)
	    nextRenewal = now;
	  else
	    nextRenewal = now + MinMinRenewPeriod;
	}
      else
	{
	  // compute next renewal
	  // relative to lastRenewalAttemptTime
	  long nextRenewalDiff = renewPeriod;
	  // subject to these constraints (in this order)
	  // less then thirf of the time till expiry
	  long tillExpiryThird = (expiryTime - lastRenewalAttemptTime) / 3;
	  if (tillExpiryThird >= 0)
	    if (nextRenewalDiff > tillExpiryThird)
	      nextRenewalDiff = tillExpiryThird;
	  // larger than MinMinRenewPeriod
	  if (nextRenewalDiff < MinMinRenewPeriod)
	    nextRenewalDiff = MinMinRenewPeriod;
	  // less then MaxMaxRenewPeriod
	  if (nextRenewalDiff > MaxMaxRenewPeriod)
	    nextRenewalDiff = MaxMaxRenewPeriod;
	  // relative to now
	  long nextRenewalDiff2 = lastRenewalAttemptTime - now + nextRenewalDiff;
	  // larger than 1
	  if (nextRenewalDiff2 < 1)
	    nextRenewalDiff2 = 1;
	  nextRenewal = nextRenewalDiff2 + now;
	}

      lock.unlock();

      LSC_Log(2, "AutoRenew: next renewal is in %ld secs, %ld secs till expiry (at %ld, now %ld.)",
	      nextRenewal-now, expiryTime-now, expiryTime, now);
      while (nextRenewal > now)
	{ // Sleep until nextRenewal.
	  sleep(nextRenewal-now);
	  now = time(NULL);
	}

      lock.lock();
      if (!isOpen() || !asyncRenewal)
	{
	  lock.unlock();
	  break;
	}

      now = time(NULL);
      LSC_Log(2, "AutoRenew: trying to renew, %ld secs till expiry.",
	      expiryTime-now);

      bool giveup = false;
      bool renewSuccess = false;
      try
	{
	  renew();
	  renewSuccess = true;
	}
      catch (LSC_IOException ex)
	{
	  renewSuccess = false;
	  // warn, but ignore
	  numFailedRenewals++;
	  // give up if failed and we are past the exiry
	  if (pastExpiry)
	    giveup = true;
	  // give up if too many attempts
	  if (numFailedRenewals >= MaxFailedRenewals)
	    giveup = true;
	  LSC_Log(3, "AutoRenew: renew failed.  %s", ex.getDetails());
	  if (!giveup)
	    {
	      //        LSC_Log(3, "AutoRenew: will retry, %d grace tries left (%d critical).",
	      //          MaxFailedRenewals-numFailedRenewals, MaxCritFailedRenewals-numCritFailedRenewals);
	      LSC_Log(3, "AutoRenew: Critical error.  Please contact Eyeball Networks Inc. immediately. You have %d hour(s) to fix the problem.  If the problem is not fixed, the server will shut down!", MaxFailedRenewals-numFailedRenewals);
       m_bLicenseConnected = false;
	  

	    }
	  else
	    LSC_Log(3, "AutoRenew: no more retries!  Failed %d times (%d critical).\n",
		    numFailedRenewals, numCritFailedRenewals);
	}
      catch (LSC_LicenseUnavailableException ex)
	{
	  renewSuccess = false;
	  // warn
	  numCritFailedRenewals++;
	  numFailedRenewals++;
	  // give up if failed and we are past the exiry
	  if (pastExpiry)
	    giveup = true;
	  // give up if too many attempts
	  if (numFailedRenewals >= MaxFailedRenewals)
	    giveup = true;
	  if (numCritFailedRenewals >= MaxCritFailedRenewals)
	    giveup = true;
	  LSC_Log(3, "AutoRenew: critical fail in renew!  %s", ex.getDetails());
	  if (!giveup)
	    {
	      //        LSC_Log(3, "AutoRenew: will retry, %d grace crit. tries left (%d total).",
	      //          MaxCritFailedRenewals-numCritFailedRenewals, MaxFailedRenewals-numFailedRenewals);
	      LSC_Log(3, "AutoRenew: Critical error.  Please contact Eyeball Networks Inc. immediately. You have %d hour(s) to fix the problem.  If the problem is not fixed, the server will shut down!", MaxCritFailedRenewals-numCritFailedRenewals);

          m_bLicenseConnected = false;


	    }
	  else
	    LSC_Log(3, "AutoRenew: no more retires!  Crit. fail %d times (%d total).\n",
		    numCritFailedRenewals, numFailedRenewals);
	}

      if (giveup)
	{
	  this->close();
	  // will exit!
	  LSC_Log(4, "AutoRenew: Unable to continue to lease!  Failed %d times (%d critical).  Calling EXIT callback!",
		  numFailedRenewals, numCritFailedRenewals);
	  if (NULL != asyncRenewalCallback)
	    asyncRenewalCallback(asyncRenewalArg);
	  lock.unlock();
	  break;
	}

      if (renewSuccess)
	{
	  // renew succeeded, reset retry counters
	  if ((0 != numCritFailedRenewals) ||
	      (0 != numFailedRenewals))
	    {
	      numCritFailedRenewals = 0;
	      numFailedRenewals = 0;
	      m_bLicenseConnected = true;
	      LSC_Log(2, "AutoRenew: retry succeeded, resetting failure counters.  %d grace crit. tries left (%d total).",
		      MaxCritFailedRenewals-numCritFailedRenewals, MaxFailedRenewals-numFailedRenewals);
	    }
	}

      lock.unlock();

    }// while

}


void LicenseServerStub::renew(void)
{
  //  printf("LicenseServerStub.renew (%ld):\n", time(NULL));

  // isOpen mustn't change while we doTryLicense (can't call
  // renew and open concurrently).
  LSC_Critical_Section cs(lock);
  assert(isOpen());

  try
  {
    // always use force=false here, otherwise we get around license exclusivity
    doTryLicense(false);
  }
  catch (LSC_IOException ex)
  {
    // warn, but ignore
    LSC_Log(3, "Renew: renew failed, could not connect to monitoring server: %s", ex.getDetails());
    throw ex;
  }
  catch (LSC_LicenseUnavailableException ex)
  {
    // warn, but ignore
    LSC_Log(3, "Renew: renew failed, license unavailable: %s", ex.getDetails());
    throw ex;
  }

  long now = time(NULL);
  LSC_Log(2, "License lease extended, %ld secs till expiry.", license.getLeaseExpiry() - now);
}


void LicenseServerStub::doTryLicense(bool force)
{
  LSC_Critical_Section cs(lock);

  lastRenewalAttemptTime = time(NULL);

  if (leased)
    leased = license.getLeaseExpiry() > 0;

  if (serverHosts.size() == 0)
    throw LSC_LicenseUnavailableException("Must specify server hosts");

  // Set when a doTryLicenseForServer executes without an LSC_IOException.
  bool successfulConnection = false;

  if (isLeased())
  {
    LSC_Log(1, "doTryLicense: trying home host %s.", license.home.c_str());
    try
    {
      doTryLicenseForServer(license.home, force);
      successfulConnection = true;
    }
    catch (LSC_IOException ex)
    {
      // Always absorb this and try another server.
    }
  }

  // LSC_LicenseUnavailableExceptions are passed to the caller.  They
  // are a definitive answer.
  for (unsigned long serverIndex = 0; serverIndex < serverHosts.size() && !successfulConnection; serverIndex++)
  { // For each server
    try
    {
      LSC_Log(1, "doTryLicense: trying host %s.", serverHosts[serverIndex].c_str());
      doTryLicenseForServer(serverHosts[serverIndex], force);
      // On success, don't try another host.
      successfulConnection = true;
    }
    catch (LSC_IOException ex)
    {
      LSC_Log(2, "doTryLicense: Host %s failed: %s.", serverHosts[serverIndex].c_str(), ex.getDetails());
      // On the last iteration, pass the exception.  Otherwise,
      // try the next host.
      if (serverIndex == serverHosts.size()-1)
        throw;
    }
  }

  // Otherwise we would have thrown by now.
  assert(successfulConnection);

  leased = license.getLeaseExpiry() > 0;
  lastRenewalTime = time(NULL);
}


void LicenseServerStub::doTryLicenseForServer(string serverHost, bool force)
{
  time_t requestTime = time(NULL) + leaseDuration;

  // taken out: it is for debugging only, not required
    LSC_Log(1, "doTryLicenseForServer: Host=%s, RequestTime=%ld, leaseDuration=%ld, force=%d.",
      serverHost.c_str(), requestTime, leaseDuration, force);

  string postUrl = "https://";
  postUrl += serverHost;
  postUrl += "/LicenseServer/servlet/CommandTryLicense";

  if (certificateFile == "")
    throw LSC_IOException("%s: Must call setCertificateFile first.", serverHost.c_str());

  LSC_HttpsPost httpsPost;
  // Can throw LSC_IOException

  httpsPost.socket.setCertificate(certificateFile.c_str(), certificateFile.c_str());
  if (caFile != "")
    {
      httpsPost.socket.setVerify(SSL_VERIFY_PEER, caFile.c_str(), NULL);
      httpsPost.setAcceptedServerName("/C=CA/ST=British Columbia/L=West Vancouver/O=Eyeball Networks, Inc./CN=License Server/Email=info@eyeball.com");

      // 06/01/02 (lars)
      // we do have a problem with inconsistent openssl behavior
      httpsPost.setAcceptedServerName2("/C=CA/ST=British Columbia/L=West Vancouver/O=Eyeball Networks, Inc./CN=License Server/emailAddress=info@eyeball.com");
    }


  string params = "&licenseId="+LSC_HttpsPost::urlEncode(licenseId);
  params += "&executionName="+LSC_HttpsPost::urlEncode(executionName);
  params += "&leaseExpiry="+LSC_StringTokenizer::ltoa(requestTime);
  params += "&version=1";
  params += force?"&force=1":"&force=0";


 LSC_Log(1, "Path=%s",params.c_str());
     

	m_MessageLock.lock();
	unsigned int iMessageNumber = remoteLogMessages.size();
	for (unsigned long i = 0; i < iMessageNumber; i++)
	{
		params += "&logMessage"+LSC_StringTokenizer::ltoa(i)+"="+LSC_HttpsPost::urlEncode(remoteLogMessages[i]);
	}
	m_MessageLock.unlock();

  long responseCode = -1;
  char responseParams[9000];
  //  printf("params=%s\n", params.c_str());
  // Can throw LSC_IOException
  try {
    httpsPost.post(postUrl.c_str(), params.c_str(), 20000, responseCode, responseParams, 9000);
  } catch (LSC_ParseException ex) {
    // I can't see how this would happen.
    LSC_Log(1, "doTryLicense: http request was not parsable: %s %s: %s.",
      postUrl.c_str(), params.c_str(), ex.getDetails());
    throw LSC_IOException("%s: HTTP request could not be parsed: %s, %s: %s", serverHost.c_str(), postUrl.c_str(), params.c_str(), ex.getDetails());
  } catch (LSC_TimeoutExceededException ex) {
    LSC_Log(1, "doTryLicense: timeout waiting for http response.");
    throw LSC_IOException("%s: Timeout waiting for HTTP response: %s", serverHost.c_str(), ex.getDetails());
  }


  if (responseCode != 200)
  { // Could not talk to the server.
    LSC_Log(1, "doTryLicense: server failed to answer: %s.", responseParams);
    throw LSC_IOException("%s: HTTP server failed to answer.  Could not process request: %s", serverHost.c_str(), responseParams);
  }

  //  printf("response=%s\n", responseParams);
  string errorCode;
  string errorMessage;
  try {
    errorCode = LSC_HttpsPost::urlDecode(LSC_HttpsPost::getParam(responseParams, "errorCode"));
    errorMessage = LSC_HttpsPost::urlDecode(LSC_HttpsPost::getParam(responseParams, "errorMessage"));
  } catch (LSC_NoSuchEntryException ex) {
    throw LSC_IOException("%s: Response is malformed.  It does not contain either an errorCode or errorMessage field: %s", serverHost.c_str(), responseParams);
  }

  if (errorCode != "200")
    {
      throw LSC_LicenseUnavailableException("%s: Lease was rejected: %s, %s.", serverHost.c_str(), errorCode.c_str(), errorMessage.c_str());
    }

  try {
    paramsToLicense(responseParams, license);
  } catch (LSC_ParseException ex) {
    throw LSC_IOException("%s: Response was not well formed: %s", serverHost.c_str(), ex.getDetails());
  }

	// delete messages already sent
	m_MessageLock.lock();
	if( iMessageNumber == remoteLogMessages.size() )
	{
		remoteLogMessages.clear();
	}
	else if( iMessageNumber != 0 )
	{
		vector<string>::iterator first, last;
		last = first = remoteLogMessages.begin();
		for( unsigned int i = 0; i < iMessageNumber ; i++ )
			last++;
		remoteLogMessages.erase(first, last);
	}
	m_MessageLock.unlock();

  lastSuccessfulServerHost = serverHost;
}


void LicenseServerStub::paramsToLicense(string params, LSC_License& newLicense)
{
  try {
    newLicense.id = LSC_HttpsPost::urlDecode(LSC_HttpsPost::getParam(params, "licenseId"));
    string lesseeName = LSC_HttpsPost::urlDecode(LSC_HttpsPost::getParam(params, "lesseeName"));
    newLicense.home = LSC_HttpsPost::urlDecode(LSC_HttpsPost::getParam(params, "home"));
    string expiryTimeStr = LSC_HttpsPost::urlDecode(LSC_HttpsPost::getParam(params, "leaseExpiry"));
    newLicense.lease(lesseeName.c_str(), atol(expiryTimeStr.c_str()));
  } catch (LSC_NoSuchEntryException ex) {
    throw LSC_ParseException("At least one of id, lesseeName, home, or leaseExpiry is missing from the response: %s", params.c_str());
  }


  //
  // The license described in params has enough fields (it may or 
  // may not have terms).  Extract the terms and test term syntax.

  try {
    for (long i = 0; true; i++)
      {
	string nextTerm = LSC_HttpsPost::urlDecode(LSC_HttpsPost::getParam(params, ("term"+LSC_StringTokenizer::ltoa(i)).c_str()));
	LSC_StringTokenizer tokens(nextTerm);
	string termName;
	bool ret = tokens.nextToken(termName);
	if (ret == false)
	  throw LSC_ParseException("Term does not start with a term name.");

	string termValue;
	ret = tokens.nextToken(termValue);
	if (ret == false)
	  throw LSC_ParseException("Term %s does not contain a value.", termName.c_str());

	newLicense.terms.setTerm(termName, termValue);
      }
  } catch (LSC_NoSuchEntryException ex) {
    // This is the only way out of the above forever loop.  Normal exit.
  }
}


void LicenseServerStub::close(void)
{
  LSC_Critical_Section cs(lock);

  // Ignore failure.
  try {
    if (isLeased())
      doReleaseLicense();
  } catch (LSC_IOException ex) {
  } catch (LSC_LicenseUnavailableException ex) {
  }
  //  closeSocket();

  leased = false;
  opened = false;
  LSC_Log(2, "License lease terminated.");
}


void LicenseServerStub::doReleaseLicense()
{
  LSC_Critical_Section cs(lock);

  assert(isLeased());

  if (serverHosts.size() == 0)
    throw LSC_IOException("Must specify server hosts");

  LSC_Log(1, "doReleaseLicense: trying home host %s.", license.home.c_str());
  try
  {
    doReleaseLicenseForServer(license.home);
    // On success, we're done.
    LSC_Log(1, "doReleaseLicense: success from home host.");
    return;
  }
  catch (LSC_IOException ex)
  {
    LSC_Log(1, "doReleaseLicense: Home host %s failed to release: %s.", license.home.c_str(), ex.getDetails());
    // Always absorb this and try another server.
  }

  // LSC_LicenseUnavailableExceptions are passed to the caller.  They
  // are a definitive answer.
  for (unsigned long serverIndex = 0; serverIndex < serverHosts.size(); serverIndex++)
  { // For each server
    try
    {
      LSC_Log(1, "doReleaseLicense: trying host %s.", serverHosts[serverIndex].c_str());
      doReleaseLicenseForServer(serverHosts[serverIndex]);
      // On success, don't try another host.
      break;
    }
    catch (LSC_IOException ex)
    {
      LSC_Log(1, "doReleaseLicense: Host %s failed: %s.", serverHosts[serverIndex].c_str(), ex.getDetails());
      // On the last iteration, pass the exception.  Otherwise,
      // try the next host.
      if (serverIndex == serverHosts.size()-1)
        throw;
    }
  }

  LSC_Log(1, "doReleaseLicense: success from non-home host.");
}


void LicenseServerStub::doReleaseLicenseForServer(string serverHost)
{
  if (certificateFile == "")
    throw LSC_IOException("Must call setCertificateFile first.");

  LSC_HttpsPost httpsPost;
  // Can throw LSC_IOException
  httpsPost.socket.setCertificate(certificateFile.c_str(), certificateFile.c_str());

  if (caFile != "")
  {
    httpsPost.socket.setVerify(SSL_VERIFY_PEER, caFile.c_str(), NULL);
    httpsPost.setAcceptedServerName("/C=CA/ST=British Columbia/L=West Vancouver/O=Eyeball Networks, Inc./CN=License Server/Email=info@eyeball.com");
    httpsPost.setAcceptedServerName2("/C=CA/ST=British Columbia/L=West Vancouver/O=Eyeball Networks, Inc./CN=License Server/emailAddress=info@eyeball.com");
    }

  // Server should probably require current executionName (lesseeName).
  string params = "&licenseId="+licenseId;
  params += "&executionName="+executionName;

	m_MessageLock.lock();
	unsigned int iMessageNumber = remoteLogMessages.size();
	for (unsigned long i = 0; i < iMessageNumber; i++)
	{
		params += "&logMessage"+LSC_StringTokenizer::ltoa(i)+"="+LSC_HttpsPost::urlEncode(remoteLogMessages[i]);
	}
	m_MessageLock.unlock();

  string postUrl = "https://" + serverHost + "/LicenseServer/servlet/CommandReleaseLicense";

  long responseCode = -1;
  char responseParams[9000];
  // Can throw LSC_IOException (LSC_ParseException and LSC_TimeoutExceeded are caught)
  try
  {
    httpsPost.post(postUrl.c_str(), params.c_str(), 20000, responseCode, responseParams, 9000);
  }
  catch (LSC_ParseException ex)
  {
    // I can't see how this would happen.
    LSC_Log(1, "doReleaseLicense: http request was not parsable: %s %s: %s.",
      postUrl.c_str(), params.c_str(), ex.getDetails());
    throw LSC_IOException("HTTP request could not be parsed: %s, %s: %s", postUrl.c_str(), params.c_str(), ex.getDetails());
  }
  catch (LSC_TimeoutExceededException ex)
  {
    LSC_Log(1, "doReleaseLicense: timeout waiting for http response.");
    throw LSC_IOException("Timeout waiting for HTTP response: %s", ex.getDetails());
  }

  if (responseCode != 200)
  { // Could not talk to license server
    LSC_Log(1, "doReleaseLicense: server failed to answer: %s.", responseParams);
    throw LSC_IOException("HTTP server could not process request: %s", responseParams);
  }

  string errorCode;
  string errorMessage;
  try
  {
    errorCode = LSC_HttpsPost::urlDecode(LSC_HttpsPost::getParam(responseParams, "errorCode"));
    errorMessage = LSC_HttpsPost::urlDecode(LSC_HttpsPost::getParam(responseParams, "errorMessage"));
  }
  catch (LSC_NoSuchEntryException ex)
  {
    throw LSC_IOException("Response is malformed.  It does not contain either an errorCode or errorMessage field: %s", responseParams);
  }

  if (errorCode != "200")
  {
    throw LSC_LicenseUnavailableException("Release was rejected: %s, %s.", errorCode.c_str(), errorMessage.c_str());
  }

	m_MessageLock.lock();
	if( iMessageNumber == remoteLogMessages.size() )
	{
		remoteLogMessages.clear();
	}
	else if( iMessageNumber != 0 )
	{
		vector<string>::iterator first, last;
		last = first = remoteLogMessages.begin();
		for( unsigned int i = 0; i < iMessageNumber ; i++ )
		last++;
		remoteLogMessages.erase(first, last);
	}
	m_MessageLock.unlock();

  // Release it no matter what, then report error to higher layer.
  string lesseeName = license.getLesseeName();
  license.release(lesseeName);
}


long LicenseServerStub::getLeaseExpiry(void) const
{
  if (!isLeased())
    return 0;

  return license.getLeaseExpiry();
}


string LicenseServerStub::getTerm(string termName) const
{
  return license.terms.getTerm(termName);
}


void LicenseServerStub::setServerHosts(const char* serverHosts)
{
  string serverHostsStr = serverHosts;
  vector<string> serverHostsSet;

  int lastStart = 0;
  while (true)
  {
    int index = serverHostsStr.find(";", lastStart);
    if (index == -1)
      break;

    string newHost = serverHostsStr.substr(lastStart, index-lastStart);
    //      string newHost = serverHostsStr.substr(0, 5);
    serverHostsSet.insert(serverHostsSet.end(), newHost);
    lastStart = index+1;
  }

  serverHostsSet.insert(serverHostsSet.end(), serverHostsStr.substr(lastStart, 1000));

  setServerHosts(serverHostsSet);
}


void LicenseServerStub::remoteLogOld(const char* message)
{
  LSC_Critical_Section cs(lock);

  remoteLogMessages.push_back(message);

//  LSC_Log(2, "Will log remotely message: '%s'.", message);
}

void LicenseServerStub::remoteLog(const char* message)
{
  LSC_Critical_Section cs(m_MessageLock);

  remoteLogMessages.push_back(message);

//  LSC_Log(2, "Will log remotely message: '%s'.", message);
}
