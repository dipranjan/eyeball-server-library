#ifndef _LSC_Exceptions_h_
#define _LSC_Exceptions_h_

#include "LSC_public.h"


class LSC_ParseException: public LSC_Exception
{
 public:
  LSC_ParseException(void) : LSC_Exception() { }

  LSC_ParseException(const char* newDetails, ...);
};


class LSC_NoSuchEntryException: public LSC_Exception
{
 public:
  LSC_NoSuchEntryException(void) : LSC_Exception() { }

  LSC_NoSuchEntryException(const char* newDetails, ...);
};


class LSC_FileNotFoundException: public LSC_Exception
{
 public:
  LSC_FileNotFoundException(void) : LSC_Exception() { }

  LSC_FileNotFoundException(const char* details, ...);
};


#endif
