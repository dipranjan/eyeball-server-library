#ifndef _LSC_Log_h_
#define _LSC_Log_h_


/**
 * Logging function.
 * 1 = debug, 2 = info, 3 = warning, 4 = error
 */
void LSC_Log(int level, char const* str, ...);


#endif
