// $Id: LSC_StringTokenizer.cpp,v 1.4 2003/04/24 18:18:45 vass Exp $

/** All files of the DNDS Prototype are Copyright (C) 1999 by Andre Schoorl
 *  unless otherwise noted.
 *
 *  The DNDS Prototype is distributed in the public domain in the hope that it
 *  will be useful, but is provided `as is' and is subject to change without
 *  notice.  No warranty of any kind is made with regard to the software or
 *  documentation.  The author shall not be liable for for any error for
 *  incidental or consequential damages in connection with the software and
 *  the documentation.
 *
 *  Permission to copy the whole, unmodified DNDS package is granted provided
 *  that the copies are not made or distributed for resale (excepting nominal
 *  copying fees).  All redistributions in either source or binary form must
 *  include this disclaimer.
 */

#include <stdio.h>
#include <assert.h>
#include <ctype.h>

#include "LSC_StringTokenizer.h"

string LSC_tolower(string _str)
{
	for (string::size_type i = 0; i < _str.size(); i++)
        _str[i] = tolower(_str[i]);

	return _str;
}

bool LSC_compare_i(const string& _s1, const string& _s2)
{
	return (LSC_tolower(_s1) == LSC_tolower(_s2));
}

string LSC_trim_whitespace(const string& _str)
{
	const string ws( " \t\r" );

	string::size_type start = _str.find_first_not_of(ws);

	if (start == string::npos)
		return string();

	string::size_type end   = _str.find_last_not_of(ws);

	assert(end != string::npos);

	return _str.substr(start, end - start + 1);
}

string LSC_escape_quotes(const string& _str)
{
	string rc(_str);

	for (string::size_type i=0; i < rc.length(); i++)
	{
		if ((rc[i] == '\'') || (rc[i] == '\"'))
		{
			// Character we need to encode
			string escaped = "\\";
			escaped += rc[i];

			rc.replace(i, 1, escaped);
			i += 1;
		}
	}

	return rc;
}

string LSC_StringTokenizer::ltoa(const long _x)
{
	char buf[70];
	sprintf(buf, "%ld", _x);
	//	printf("LSC_StringTokenizer.ltoa: converted %ld to %s\n", _x, buf);
	return buf;
}


LSC_StringTokenizer::LSC_StringTokenizer()
:
	str(),
	pos(0)
{
}

LSC_StringTokenizer::LSC_StringTokenizer(const string& _str)
:
	str(_str),
	pos(0)
{
}

void LSC_StringTokenizer::setString(const string& _str)
{
	str = _str;
	pos = 0;
}

bool LSC_StringTokenizer::nextToken(string& _token)
{
	string::size_type token_start(pos);

	bool within_token(false), within_quote(false);

	for (; pos < str.size(); pos++)
	{
		switch(str[pos])
		{
			// Comment - ignore everything after
			case '#':
				return false;
				break;

			// whitespace
			case ' ':
			case '\t':
			case '\n':
				if (within_quote)
					break;

				if (within_token)
				{
					_token = str.substr(token_start, pos-token_start);
					return true;
				}
				break;

			case '"':
				if (within_quote)
				{
					_token = str.substr(token_start, pos-token_start);
					pos++;
					return true;
				}

				token_start = pos+1;
				within_token = within_quote = true;
				break;

			default:
				if (!within_token)
				{
					token_start = pos;
					within_token = true;
				}
				break;
		}
	}

	// If we reach the end of line while still in a token, return it
	if (within_token)
	{
		_token = str.substr(token_start, pos-token_start);
		return true;
	}

	return false;
}

