#include "LSC_Random.h"

#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>
#include <assert.h>


bool LSC_Random::didInit = false;
LSC_Mutex LSC_Random::lock;
LSC_randctx LSC_Random::randomContext;


void LSC_Random::init()
{
  fprintf(stderr, "LSC_Random::init() ");
  for (int i = 0; i < RANDSIZ; i++)
    {
      timeval tv;
      // gettimeofday/usleep(1) takes about 20ms on Solaris 2.6 and
      // Linux, with high variability under 1ms on an
      // unloaded system, or the low 10 bits.  We'll mostly use
      // the low 8 bits.  This takes about 640ms to run for all the
      // usleeps.  If usleep was more accurate it could run in
      // 32us.
      usleep(1);
      gettimeofday(&tv, NULL);
      randomContext.randrsl[i] = tv.tv_usec;
      usleep(1);
      gettimeofday(&tv, NULL);
      randomContext.randrsl[i] ^= tv.tv_usec<<8;
      usleep(1);
      gettimeofday(&tv, NULL);
      randomContext.randrsl[i] ^= tv.tv_usec<<16;
      usleep(1);
      gettimeofday(&tv, NULL);
      randomContext.randrsl[i] ^= tv.tv_usec<<24;

      if (0 == i%8) fprintf(stderr, ".");
    }

  LSC_randinit(&randomContext, true);

  didInit = true;
  fprintf(stderr, "\n");
}


unsigned long LSC_Random::getNextRandom(unsigned long max)
{
  // This is all based on 4 bytes per long.  rand() returns a ub4,
  // which I need to have the same size as an unsigned long.
  assert(sizeof(unsigned long) == 4);
  assert(sizeof(ub4) == 4);

  LSC_Critical_Section cs(lock);

  if (!didInit)
    init();

  unsigned long r = LSC_isaac_rand(&randomContext);

  if (max == 0)  // to prevent div by 0
    return r;

  unsigned long highMultiple = (0xFFFFFFFF/max)*max;

  while (r >= highMultiple)
    {
      r = LSC_isaac_rand(&randomContext);
    }

  return r % max;
}
