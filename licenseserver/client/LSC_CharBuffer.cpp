#include "LSC_CharBuffer.h"
#include "LSC_Log.h"

#include <assert.h>
#include <errno.h>
#include <string.h>  // memmove
#include <strings.h>  // index on Solaris


LSC_CharBuffer::LSC_CharBuffer(void)
{
  buffer = NULL;
  bufferAllocation = 0;

  bufferFirstChar  = 0;
  bufferLastChar   = 0;

  writableMode = false;
}


LSC_CharBuffer::~LSC_CharBuffer(void)
{
  if (NULL != buffer)
    delete [] buffer;
}


void LSC_CharBuffer::add(const char * data, int len)
{
  if (writableMode)
    cancelWrite();
  ensureFreeSpace(len+1);

  assert(bufferAllocation-bufferLastChar >= len);
  memcpy(buffer+bufferLastChar, data, len);
  bufferLastChar += len;
  assert(bufferLastChar <= bufferAllocation);
}


long LSC_CharBuffer::getLength(void) const
{
  return bufferLastChar-bufferFirstChar;
}


const char * LSC_CharBuffer::getBase(void) const
{
  return buffer + bufferFirstChar;
}


void LSC_CharBuffer::empty(void)
{
  if (writableMode)
    cancelWrite();
  // we don't change the allocation
  bufferFirstChar  = 0;
  bufferLastChar   = 0;
}


void LSC_CharBuffer::discard(int number)
{
  if (writableMode)
    cancelWrite();

  assert(number <= bufferLastChar-bufferFirstChar);
  assert(number >= 0);
  assert(bufferLastChar <= bufferAllocation);

  //  memmove(buffer, buffer+number, charsInBuffer-number);
  bufferFirstChar += number;

  assert(bufferLastChar <= bufferAllocation);
}


long LSC_CharBuffer::discardTo(const char *str)
{
  if (writableMode)
    cancelWrite();

  //LSC_dprintf(0, "LSC_CharBuffer(%p).discardTo\n", this);
  long numDiscard = indexOf(str, -1);
  if (numDiscard == -1)
    return -1;

  discard(numDiscard+1);  // +1 to include the delimeter.
  return numDiscard+1;
}


long LSC_CharBuffer::getFreeSize(void) const
{
  return bufferAllocation - bufferLastChar;
}


long LSC_CharBuffer::indexOf(const char* str, long startIndex) const
{
  startIndex += bufferFirstChar+1;

  for (; startIndex < bufferLastChar; startIndex++)
  {
    if (buffer[startIndex] == 0)
      continue;

    if (index(str, buffer[startIndex]) != NULL)
      return startIndex-bufferFirstChar;
  }

  return -1;
}


/**
 * The idea here is to ensure there's at least 'size' extra bytes in the
 * buffer.
 * Must make sure buffer != NULL.  If buffer is empty but first char is
 * not at 0, reset first char.  If buffer is empty but large allocation,
 * shrink it.  If there's not enough space and first char is large, move
 * buffer data, otherwise realloc.
 */
void LSC_CharBuffer::ensureFreeSpace(long desFreeSize)
{
  if (writableMode)
    cancelWrite();

  if (buffer == NULL)
  {
    // Start a new buffer.
    assert(bufferFirstChar == 0);
    assert(bufferLastChar == 0);
    reallocBuffer(desFreeSize*2);
    return;
  }

  long usedSize = bufferLastChar-bufferFirstChar;
  long desSize = usedSize + desFreeSize;

  /**
   * There are several cases:
   * - current allocation size too small --> need to reallocate (this will copy)
   * - allocation size is OK, but too few space at the end --> repack (copy)
   * - too much waste in front --> repack (copy)
   */

  if (bufferAllocation < desSize)
  {
    // Buffer is not large enough.
    reallocBuffer( desSize*2 );
    return;
  }

  // Buffer is large enough.  If too large (over 10 times required),
  // shrink to save space.
  if ( desSize+1 < bufferAllocation/10)
  {
    // The allocation is large.  Reduce to save space.
    reallocBuffer( desSize*2 );
    return;
  }

  // Buffer is not too large, not too small.

  // if buffer is empty, we can reset firstChar
  if (bufferFirstChar == bufferLastChar)
  {
    bufferFirstChar = bufferLastChar = 0;
    return;
  }

  // If there's not enough space after bufferLastChar, move data to front.
  if (bufferAllocation - bufferLastChar < desFreeSize)
  {
    memmove(buffer, buffer+bufferFirstChar, bufferLastChar-bufferFirstChar);
    bufferLastChar -= bufferFirstChar;
    bufferFirstChar = 0;
    return;
  }
}


void LSC_CharBuffer::reallocBuffer(long newAlloc)
{
  assert(!writableMode);
  assert(newAlloc >= bufferLastChar-bufferFirstChar);

  char * newBuffer = new char[newAlloc];
  if (NULL == newBuffer)
  {
    LSC_Log(4, "LSC_CharBuffer::reallocBuffer: failed to allocate memory.");
    assert(NULL != newBuffer);
  }
  if (buffer != NULL)
  {
    memcpy(newBuffer, buffer+bufferFirstChar, bufferLastChar-bufferFirstChar);
    bufferLastChar -= bufferFirstChar;
    bufferFirstChar = 0;
    delete [] buffer;
  }

  buffer = newBuffer;
  bufferAllocation = newAlloc;
}


char * LSC_CharBuffer::getWritable(long minLen)
{
  if (writableMode)
    cancelWrite();
  // ensure free size, set writable mode, and return buffer base
  ensureFreeSpace(minLen + 1);
  writableMode = true;
  return buffer + bufferLastChar;
}


long LSC_CharBuffer::commitWrite(long addedLen)
{
  if (!writableMode)
    return -1; // error
  // make sure there is enough room
  long maxadd = bufferAllocation - bufferLastChar;
  if (addedLen > maxadd)
    addedLen = maxadd;
  // advance bufferLastChar
  bufferLastChar += addedLen;
  assert(bufferLastChar <= bufferAllocation);
  return addedLen;
}


void LSC_CharBuffer::cancelWrite(void)
{
  if (!writableMode)
    return;

  // nothing to do, really, just cancel the writableMode
  writableMode = false;
}



