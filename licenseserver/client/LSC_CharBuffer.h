#ifndef _LSC_CharBuffer_h_
#define _LSC_CharBuffer_h_


/**
 * Stores a variable number of characters, manages storage space.
 *
 * Discarding characters advances the base, not moves all the bytes.
 * Many of the functions are not re-entrant.
 */
class LSC_CharBuffer
{
 public:
  /**
  * Creates an empty buffer.
  */
  LSC_CharBuffer(void);


  ~LSC_CharBuffer(void);


  /**
   * Adds len bytes from data to the end of the buffer.  Never fails.
   */
  void add(const char * data, int len);


  /**
   * Returns the number of characters currently in the buffer.
   */
  long getLength(void) const;


  /**
   * Returns a pointer to the first character in the buffer.  The
   * returned value is valid so long as no other non-const functions in this
   * object are called.  The buffer can be manipulated
   * directly with the returned pointer, eg. by using strtok().
   */
  const char * getBase(void) const;


  /**
   * Discard the contents of the buffer.
   */
  void empty(void);


  /**
   * Removes number chars from the buffer.  number must be not
   * more than getNumCharsInBuffer().
   */
  void discard(int number);


  /**
   * Discards characters until a character in str (aka. a delimeter)
   * is discarded.
   *
   * @return The number of chars discarded, including the delimiter.
   * -1 if there is no delimiter in the buffer (in which case no
   * chars are discarded).  Cannot return 0.
   */
  long discardTo(const char* str);


  /**
   * Returns the extra free space at the end of the buffer.
   * This can be of interest in writable mode, for instance.
   */
  long getFreeSize(void) const;


  /**
   * Returns the index of the first occurance of any character
   * in str after (and not including) index startIndex, or -1 if
   * there is none.  Use startIndex=-1 to search the entire buffer.
   * Index is based at the return value of getBufferBase().
   */
  long indexOf(const char* str, long startIndex) const;


  /**
   * Get the buffer in writable mode.  The returned buffer is located atfer
   * the current contents, is of length minLen, and is writable.
   * After writing, commitWrite() or cancelWrite() must be called.
   * No reallocation is made while the buffer is in writable mode.
   */
  char * getWritable(long minLen);


  /**
   * Commit changes made in the buffer returned by getWritable, add addedLen
   * number of bytes written to the buffer.
   * @returns  number of bytes added, <0 on error (not in writable mode).
   */
  long commitWrite(long addedLen);


  /**
   * Cancels the writable mode, changes made to the writable buffer are lost.
   */
  void cancelWrite(void);


 protected:
  /**
   * Ensures there is at least 'desFreeSize' extra bytes available in buffer,
   * reallocating buffer if necessary.  Shrinks a buffer that is
   * large compared to the data it holds.
   */
  void ensureFreeSpace(long desFreeSize);


  /**
   * Sets bufferAllocation to size.  Reallocates buffer.  Moves data
   * to start of buffer.
   *
   * @newAlloc The new buffer size.  Must be at least as large as the
   * number of chars in the buffer.
   */
  void reallocBuffer(long newAlloc);


  /// pointer to the actual buffer
  char* buffer;
  /// number of bytes allocated for buffer
  long bufferAllocation;

  /**
   * The index of first character of the buffer, before it there is free space.
   */
  long bufferFirstChar;

  /**
   * The index of the character after the last useful char in the buffer.
   * bufferLastChar-bufferFirstChar = chars in buffer.
   */
  long bufferLastChar;

  /**
   * This is set after a getWritable() call, until a commitWrite() or cancelWrite().
   * While this is set, the contents and the allocation cannot be modified.
   */
  bool writableMode;
};


#endif
