#include "LSC_Url.h"

using std::string;

void LSC_Url::setUrl(const char* url)
{
  this->url = url;
}


string LSC_Url::getProtocol()
{
  string::size_type index = url.find(":");
  if (index == string::npos)
    throw LSC_ParseException("No colon in url.");

  return url.substr(0, index);
}


string LSC_Url::getHost()
{
  string::size_type start = url.find("/");
  if (start == string::npos)
    throw LSC_ParseException("No / in url.");

  if (url.length() < start+2)
    throw LSC_ParseException("Not enough chars after / in url.");

  start++;

  if (url[start] != '/')
    throw LSC_ParseException("First / is not followed by a second / in url.");

  start++;
  string::size_type firstSlash = url.find("/", start);
  string::size_type firstColon = url.find(":", start);
  if (firstSlash == string::npos)
    throw LSC_ParseException("Second slash is not followed by a third slash in url");

  string::size_type end = string::npos;
  if (firstColon != string::npos && firstColon < firstSlash)
    end = firstColon;
  else
    end = firstSlash;

  return url.substr(start, end-start);
}

#include <stdio.h>
#include <stdlib.h>




long LSC_Url::getPort(long def)
{
  string::size_type slash = url.find("/");
  if (slash == string::npos)
    throw LSC_ParseException("No / in url");

  if (url.length() < slash+2)
    throw LSC_ParseException("Not enough chars after / in url");

  slash++;

  if (url[slash] != '/')
    throw LSC_ParseException("First / is not followed by a second / in url.");

  slash++;
  string::size_type firstSlash = url.find("/", slash);
  string::size_type firstColon = url.find(":", slash);
  if (firstSlash == string::npos)
    throw LSC_ParseException("Second slash is not followed by a third slash in url");

  if (firstColon == string::npos || firstColon > firstSlash)
    return def;

  return atoi(url.substr(firstColon+1, firstSlash-firstColon).c_str());
}


string LSC_Url::getPath()
{
  string::size_type start = url.find("/");
  if (start == string::npos)
    throw LSC_ParseException("No / in url.");

  start = url.find("/", start+1);
  if (start == string::npos)
    throw LSC_ParseException("No second / in url.");

  start = url.find("/", start+1);
  if (start == string::npos)
    throw LSC_ParseException("No third / in url.");
  
  return url.substr(start);
}
