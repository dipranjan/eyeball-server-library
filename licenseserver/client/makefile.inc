# HOSTNAME is expected to be set
OPSYS=unknown
STDLIBS=-L/usr/local/ssl/lib -lssl -lcrypto -lpthread
WORKDIR=$(HOSTNAME)-ls

ifeq ($(findstring solaris,$(OSTYPE)),solaris)
OPSYS=SOLARIS
endif

ifeq ($(findstring linux,$(OSTYPE)),linux)
OPSYS=LINUX
endif

ifeq ($(findstring felicia,$(HOSTNAME)),felicia)
EXTLIBS=-L/usr/local/lib -lstlport_gcc -lsocket -lnsl -lposix4 -lm
endif

ifeq ($(findstring marge,$(HOSTNAME)),marge)
EXTLIBS=-L/usr/local/lib -lstlport_gcc
endif

SHAREDLS=$(WORKDIR)/shared-ls.lib

LIBS=$(STDLIBS) $(EXTLIBS)

INCLUDES = -I../shared -I/usr/local/ssl/include

ALLFLAGS = -DDEBUG -g -pipe -Wall -Wpointer-arith -Wcast-align -Wmissing-prototypes -Wmissing-declarations -Wnested-externs -Winline -D$(OPSYS) -D_POSIX_PTHREAD_SEMANTICS -D_REENTRANT $(INCLUDES)
 
$(WORKDIR)/%.o: %.cpp
	g++ $(ALLFLAGS) -c -o $@ $<
