#include "LicenseServerStub.h"
#include "LSC_StringTokenizer.h"

#include <stdio.h>
#include <unistd.h>  // for sleep
#include <sys/types.h>
#include <assert.h>
#include <cstdlib>
#include <string.h>

// We use callbacks just to show how to use them
void myLogCallback(LSC_LogLevel level, const char * mess);
void myAsyncExitCallback(void *);

void setParams(string certFileName, string CAFileName,
  string hosts, long duration, long renewPeriod, bool force);
int doOpen(string licenseName, bool async);
void doAsyncCycle(void);
void doManualCycle(long renewPeriod);
void printTerms(void);
void mycallback(void* arg);
void usage(void);


#define HOSTS_EXT   "ls1.eyeball.com;ls2.eyeball.com;ls3.eyeball.com"
#define HOSTS_EXTIP "216.187.87.83;10.2.1.75"
#define HOSTS_INT   "ls1internal;ls2internal;ls3internal"
#define HOSTS_ALL   HOSTS_EXT ";" HOSTS_INT


LicenseServerStub license;


void myLogCallback(LSC_LogLevel level, const char * mess)
{
  switch (level)
  {
   case LSC_Error:
    printf("LSCT Error: %s\n", mess);
    break;

   case LSC_Info:
    printf("LSCT Info: %s\n", mess);
    break;

   case LSC_Debug:
    printf("LSCT Debug: %s\n", mess);
    break;
  }
}


void myAsyncExitCallback(void *)
{
  printf("myAsyncExitCallback: Exiting.\n");
  exit(1);
}


void setParams(string certFileName, string CAFileName,
  string hosts, long duration, long renewPeriod, bool force)
{
  LSC_setLogCallback( &myLogCallback );

  license.setLeaseDuration(duration);
  license.setMaxRenewPeriod(renewPeriod);
  license.setForcedLease(force);
  license.setCertificateFile(certFileName.c_str());
  license.setCAFile(CAFileName.c_str());
  license.setServerHosts(hosts.c_str());
}


int doOpen(string licenseName, bool async)
{
  license.setAsyncRenewal(async, myAsyncExitCallback);

  try {
    license.open(licenseName, "");
  } catch (LSC_IOException ex) {
    printf("Failed to open the license: LSC_IOException: %s\n", ex.getDetails());
    return -1;
  } catch (LSC_LicenseUnavailableException ex) {
    printf("Failed to open the license: LSC_LicenseUnavailableException: %s\n", ex.getDetails());
    return -2;
  }

  printf("License successfully obtained.\n");
  return 0;
}


void doAsyncCycle(void)
{
  while (license.isOpen())
  {
    license.remoteLog(("LSC test client, time " + LSC_StringTokenizer::ltoa(time(NULL))).c_str());

    sleep(20);
  }
}


void doManualCycle(long renewPeriod)
{
  while (license.isLeased())
  {
    printf("I have the license, I go to sleep for %ld secs.\n", renewPeriod);
    long sleepTill = time(NULL) + renewPeriod;
    long toSleep = sleepTill - time(NULL);
    while (toSleep > 0)
    {
      sleep(toSleep);
      toSleep = sleepTill - time(NULL);
    }

    printf("Now I'm going to try to renew the license.\n");
    license.renew();
  }
}


void printTerms(void)
{
  if (!license.isOpen())
    return;

  // get license object
  const LSC_License * licObj = license.getLicense();
  if (NULL == licObj)
    return;

  // get number of terms
  long numTerms = licObj->terms.getNumTerms();
  printf("This license has %ld terms:\n", numTerms);

  // list terms
  for (int i=0; i<numTerms; i++)
  {
    string name = "?";
    string value = "?";
    licObj->terms.getTerm(i, name, value);
    printf("  term%d\t%s=%s\n", i+1, name.c_str(), value.c_str());
  }
}


void mycallback(void* arg)
{
  printf("Lease expired\n");
  exit(1);
}


void usage(void)
{
  printf("licensetest [async|manual|getterms] [params...]\n");
  printf(" \n");
  printf(" test     gets the license once, prints the terms, and exits.\n");
  printf(" async    uses the license API for renewals.\n");
  printf(" manual   calls renew periodically.\n");
  printf(" \n");
  printf(" Optional parameters are:\n");
  printf(" -license <name>     The name of the license.\n");
  printf(" -certfile <pemfile> File to use as certificate and private key.\n");
  printf(" -cafile <pemfile>   Certificate to verify the identity of the server.\n");
  printf(" -servers <hosts>    Semicolon-separated list of hostnames to use as servers.\n");
  printf(" -ints               Use internal license servers.\n");
  printf("                     Same as -servers '%s'\n", HOSTS_INT);
  printf(" -exts               Use external license servers.\n");
  printf("                     Same as -servers '%s'\n", HOSTS_EXT);
  printf(" -extsip             Use external license servers by IP.\n");
  printf("                     Same as -servers '%s'\n", HOSTS_EXTIP);
  printf(" -duration <secs>    The time until a renewal expires.\n");
  printf(" -renewtime <secs>   Time between consecutive renewals.\n");
  printf(" -force              The lease is forcedly taken from the current holder.\n");
  printf(" -fork               Fork in the backround.\n");
}


// global variables
int operation;
string licenseName;
string certFileName;
string CAFileName;
string hosts;
long leaseDuration;
bool force;
long renewPeriod;
bool forkq;


int processArgs(int argc, char ** argv)
{
  if (argc < 2)
  {
    usage();
    return -1;
  }

  operation = 0;

  // first arg is operation
  if (0 == strcmp(argv[1], "async"))
    operation = 1;
  else if (0 == strcmp(argv[1], "manual"))
    operation = 2;
  else if (0 == strcmp(argv[1], "test"))
    operation = 3;

  if (operation <= 0)
  {
    printf("Unknown command: %s\n", argv[1]);
    return -2;
  }

  licenseName = "dhaka-eb";
  certFileName = "dhaka.crtpvk.pem";
  CAFileName = "eyeball-root.crt.pem";
  hosts = HOSTS_ALL;
  leaseDuration = 30*60;
  force = false;
  renewPeriod = 60*5;
  forkq = false;

  int nextArg = 2;

  while (nextArg < argc)
  {
    if (strcmp(argv[nextArg], "-license") == 0)
    {
      nextArg++;
      if (nextArg >= argc)
      {
        printf("-license must be followed by the license name.\n");
        return -3;
      }

      licenseName = argv[nextArg++];
    }
    else if (strcmp(argv[nextArg], "-certfile") == 0)
    {
      nextArg++;
      if (nextArg >= argc)
      {
        printf("-certfile must be followed by a filename.\n");
        return -3;
      }

      certFileName = argv[nextArg++];
    }
    else if (strcmp(argv[nextArg], "-cafile") == 0)
    {
      nextArg++;
      if (nextArg >= argc)
      {
        printf("-cafile must be followed by a filename.\n");
        return -3;
      }

      CAFileName = argv[nextArg++];
    }
    else if (strcmp(argv[nextArg], "-servers") == 0)
    {
      nextArg++;
      if (nextArg >= argc)
      {
        printf("-servers must be followed by a list of hosts.\n");
        return -3;
      }

      hosts = argv[nextArg++];
    }
    else if (strcmp(argv[nextArg], "-ints") == 0)
    {
      nextArg++;
      hosts = HOSTS_INT;
    }
    else if (strcmp(argv[nextArg], "-exts") == 0)
    {
      nextArg++;
      hosts = HOSTS_EXT;
    }
    else if (strcmp(argv[nextArg], "-extsip") == 0)
    {
      nextArg++;
      hosts = HOSTS_EXTIP;
    }
    else if (strcmp(argv[nextArg], "-duration") == 0)
    {
      nextArg++;
      if (nextArg >= argc)
      {
        printf("-duration must be followed by a number of seconds.\n");
        return -4;
      }
      leaseDuration = atol(argv[nextArg++]);
    }
    else if (strcmp(argv[nextArg], "-renewtime") == 0)
    {
      nextArg++;
      if (nextArg >= argc)
      {
        printf("-renewtime must be followed by a number of seconds.\n");
        return -4;
      }
      renewPeriod = atol(argv[nextArg++]);
    }
    else if (strcmp(argv[nextArg], "-force") == 0)
    {
      nextArg++;
      force = true;
    }
    else if (strcmp(argv[nextArg], "-fork") == 0)
    {
      nextArg++;
      forkq = true;
    }
    else
    {
      printf("Unknown argument %s.  See --help.\n", argv[nextArg]);
      return -4;
    }
  }

  return 0;
}


// do what needs to be done initially
int doOperationFirst(void)
{
  switch (operation)
  {
   case 1:
    setParams(certFileName, CAFileName, hosts, leaseDuration, renewPeriod, force);
    if (0 != doOpen(licenseName, true))
      return 1;
    printTerms();
    break;

   case 2:
    setParams(certFileName, CAFileName, hosts, leaseDuration, renewPeriod, force);
    if (0 != doOpen(licenseName, false))
      return 1;
    printTerms();
    break;

   case 3:
    setParams(certFileName, CAFileName, hosts, leaseDuration, renewPeriod, force);
    if (0 != doOpen(licenseName, false))
      return 1;
    printTerms();
    license.close();
    break;
  }
  return 0;
}


// do what needs to be done in a cycle
int doOperationCycle(void)
{
  switch (operation)
  {
   case 1:
    doAsyncCycle();
    license.close();
    break;

   case 2:
    doManualCycle(renewPeriod);
    license.close();
    break;

   case 3:
    // nothing
    break;
  }
  return 0;
}


int doFork(void)
{
  long pid = (long)fork();
  assert(pid >= 0);
  if (0 != pid)
  {
    // parent
    printf("Background process %ld forked.\n", (long)pid);
    // always return 0;
    return 0;
  }
  // child
  printf("Background process starting.\n");

  // redo the init operations to reopen
  license.close();
  int res = doOperationFirst();
  if (0 != res)
    return res;

  res = doOperationCycle();
  return res;
}


int main(int argc, char ** argv)
{
  int res;
  res = processArgs(argc, argv);
  if (res < 0)
    return -1;

  // print out parameters
  printf("License name:     %s\n", licenseName.c_str());
  printf("Certificate file: %s\n", certFileName.c_str());
  printf("CA file:          %s\n", CAFileName.c_str());
  printf("L. servers:       %s\n", hosts.c_str());
  printf("Lease duration:   %ld s (%d m)\n", leaseDuration, (int)(leaseDuration/60));
  printf("Renew period:     %ld s (%d m)\n", renewPeriod, (int)(renewPeriod/60));
  printf("Force?            %d\n", (int)force);
  printf("Fork?             %d\n", (int)forkq);
  printf("\n");

  if (!forkq)
  {
    res = doOperationFirst();
    if (0 != res)
      return res;
    res = doOperationCycle();
  }
  else
  {
    res = doOperationFirst();
    if (0 != res)
      return res;
    // do fork
    res = doFork();
  }

  return res;
}


