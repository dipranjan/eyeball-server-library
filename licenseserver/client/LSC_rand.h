/*
------------------------------------------------------------------------------
rand.h: definitions for a random number generator
MODIFIED:
  960327: Creation (addition of randinit, really)
  970719: use context, not global variables, for internal state
  980324: renamed seed to flag
  980605: recommend RANDSIZL=4 for noncryptography.
------------------------------------------------------------------------------
*/

#ifndef _LSC_rand_h_
#define _LSC_rand_h_

#define RANDSIZL   (8)  /* I recommend 8 for crypto, 4 for simulations */
#define RANDSIZ    (1<<RANDSIZL)

typedef  unsigned       char ub1;
typedef  unsigned long  int  ub4;   /* unsigned 4-byte quantities */
typedef                 int  word;  /* fastest type available */

/* context of random number generator */
struct LSC_randctx
{
  ub4 randcnt;
  ub4 randrsl[RANDSIZ];
  ub4 randmem[RANDSIZ];
  ub4 randa;
  ub4 randb;
  ub4 randc;
};
typedef struct LSC_randctx  LSC_randctx;

/*
------------------------------------------------------------------------------
 If (flag==TRUE), then use the contents of randrsl[0..RANDSIZ-1] as the seed.
------------------------------------------------------------------------------
*/
void LSC_randinit(LSC_randctx *r, word flag);

void LSC_isaac(LSC_randctx *r);


/*
------------------------------------------------------------------------------
 Call rand(/o_ randctx *r _o/) to retrieve a single 32-bit random value
------------------------------------------------------------------------------
*/
#define LSC_isaac_rand(r) \
   (!(r)->randcnt-- ? \
     (LSC_isaac(r), (r)->randcnt=RANDSIZ-1, (r)->randrsl[(r)->randcnt]) : \
     (r)->randrsl[(r)->randcnt])

#endif  /* _LSC_rand_h_ */


