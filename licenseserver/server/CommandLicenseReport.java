import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Vector;
import java.util.Iterator;
import java.util.Date;
import java.text.SimpleDateFormat;


public class CommandLicenseReport
extends LicenseCommandHandler
{
  public void doMyGet(HttpServletRequest request,
		    HttpServletResponse response)
    throws IOException, ServletException
    {
      response.setContentType("text/html");

      PrintWriter out = new PrintWriter(response.getOutputStream());

      out.println("<html><body>");

      // some general info
      if (masterMode)
        out.println("<br>Running in master mode.");
      else
      {
        out.println("<br>Running in slave mode, master is at https://"+masterHost+".");
        out.println("<br>Normally licenses are handed out by the master.  See the ");
        out.println(" <a href=\"https://"+masterHost+"/LicenseServer/servlet/CommandLicenseReport\">report of the master</a>.");
      }

      Licenses licenses = getLicenses();
      if (licenses == null)
      {
        out.println("<br>ERROR: Couldn't obtain a license configuration.");
        out.println("</body></html>");
        out.close();
        response.setStatus(HttpServletResponse.SC_NO_CONTENT);
        return;
      }

      long age = System.currentTimeMillis() - licenses.getLoadedTime();
      out.println("<br>License configuration was read/refreshed " + age / (60000) + " minutes ago.");
      out.println("<br>License server software version: " + getVersion() + ".");
      Date currentTimeDate = new Date();
      currentTimeDate.setTime(System.currentTimeMillis());
      out.print("<br>Current server time: "+(new SimpleDateFormat()).format(currentTimeDate)+"");

      out.println("<p>");

      String colorHeader = "#0087E7";
      String colorRowNot = "#FFFBB4";
      String colorRowYes = "#97E793";

      String colorExpiry1 = "#FF2626";  // expired, red
      String colorExpiry2 = "#FF7D59";  // about to expire
      long expiry2Time = 3*24*60*60;  // 3 days

      long now = System.currentTimeMillis()/1000;

      out.println("<table>");
      out.print("<tr bgcolor="+colorHeader+">");
      out.println("<td colspan=\"4\">License (static)</td>");
      out.println("<td colspan=\"5\">Current Lease (dynamic)</td>");
      out.println("</tr>");
      out.print("<tr bgcolor="+colorHeader+">");
      out.println("<td>name</td><td>owner</td><td>expiry</td><td>terms</td>");
      out.print("<td>leased by</td><td>leased on</td>");
      out.println("<td>leased till</td><td>total leased time (hr)</td><td>last log</td>");
      out.println("</tr>");

      Vector ids = licenses.getSortedLicenseIds();

      Iterator iter = ids.iterator();
      while (iter.hasNext())
      {
	String id = (String)iter.next();
	License license = licenses.getLicense(id);

        String lessee = license.getLesseeName();
        boolean taken = false;
        if (null != lessee)  taken = true;

        out.print("<tr bgcolor="+(taken ? colorRowYes : colorRowNot ) +">");
	out.print("<td><b>" + id + "</b></td>");

        int maxOwnerLen = 28;
        if (license.owner.length() <= maxOwnerLen)
          out.print("<td>" + license.owner + "</td>");
        else
          out.print("<td>" + license.owner.substring(1,maxOwnerLen) + "...</td>");

        String licenseExpiryStr = "-";
        long tillLicenseExpiry = 0;
        out.print("<td");  // close below
        if (license.expiry > 0)
        {
          Date licenseExpiryDate = new Date(license.expiry*1000);
          licenseExpiryStr = (new SimpleDateFormat()).format(licenseExpiryDate);
          // check expiry (for color)
          long expiredAgo = now - license.expiry;
          if (expiredAgo >= 0)
            out.print(" bgcolor=\"" + colorExpiry1 + "\"");
          else if (expiredAgo >= -expiry2Time)
            out.print(" bgcolor=\"" + colorExpiry2 + "\"");
        }
        out.print(">" + licenseExpiryStr + "</td>");
	
        out.print("<td>" + license.getTerms() + "</td>");

        //out.print("<td>"+lessee+"</td>");
        out.print("<td>"+(lessee==null?"-":lessee)+"</td>");

        long leaseTime = license.getLeaseTime();
        Date leaseTimeDate = new Date();
        leaseTimeDate.setTime(leaseTime*1000);
        out.print("<td>"+(leaseTime==0?"-":(new SimpleDateFormat()).format(leaseTimeDate))+"</td>");

        long leaseExpiry = license.getLeaseExpiry();
        Date leaseExpiryDate = new Date();
        leaseExpiryDate.setTime(leaseExpiry*1000);
        out.print("<td>"+(leaseExpiry==0?"-":(new SimpleDateFormat()).format(leaseExpiryDate))+"</td>");

        String totaltime = (new Long(license.getTotalLeasedTime()/3600)).toString();
        if (0 == license.getTotalLeasedTime()) totaltime = "-";
        out.print("<td>" + totaltime + "</td>");

        String lastLog = license.getLastClientLog();
        out.print("<td>"+((lastLog==null)?"-" : lastLog) +"</td>");

        out.println("</tr>");
      }
      out.println("</table>");

      out.println("</body></html>");

      out.close();
      response.setStatus(HttpServletResponse.SC_OK);
    }
}
