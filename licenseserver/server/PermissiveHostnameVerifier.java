import com.sun.net.ssl.HostnameVerifier;


class PermissiveHostnameVerifier
implements HostnameVerifier
{
  public boolean verify(String urlHostname, String certHostname)
    {
      System.out.println("HostnameVerifier.verify: "+urlHostname+" against " +
			 certHostname);
      return true;
    }
}
