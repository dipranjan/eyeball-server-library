import java.io.*;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.FileInputStream;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import java.util.Vector;
import java.util.Date;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.net.MalformedURLException;
import java.security.cert.X509Certificate;
import java.security.KeyStore;
import javax.net.ssl.SSLSocketFactory;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpUtils;
import com.sun.net.ssl.HttpsURLConnection;
import com.sun.net.ssl.SSLContext;
import com.sun.net.ssl.KeyManagerFactory;

/**
 * Base class for all command handlers.
 * Command handlers need to define their doGet().
 * Before & after they have to call doSomeWorkBefore() and After().
 */
abstract class LicenseCommandHandler
  extends HttpServlet
{
  protected final String Version = "1.4.2";

  protected Properties properties = new Properties();

  // true if running in master mode, false if in slave mode
  protected boolean masterMode = true;

  // in slave mode, the address of the server
  protected String masterHost = "unknown";

  // certificate subject line used to identify license servers
  protected String licenseServerCertSubject =
    "/C=CA/ST=British Columbia/L=West Vancouver/O=Eyeball Networks, Inc./CN=License Server/Email=info@eyeball.com";

  //public boolean getMasterMode() { return masterMode; }

  protected long lastPeriodicChecksTime = 0;

  protected long  periodicChecksFrequency = 10*60;


  public void init()
    throws ServletException
    {
      System.out.println("CommandTryLicense: init");

      String PropsFileName = "/etc/licenseserver/properties";
      try {
        properties.load(new FileInputStream(PropsFileName));
        log("Properties read from file '"+PropsFileName+"'.");
      } catch (IOException ex) {
        log("Unable to open properties file '"+PropsFileName+"', using defaults.");
        // Not sure what I should do here.  Just continue blindly.
      }

      System.setProperty("java.protocol.handler.pkgs", "com.sun.net.ssl.internal.www.protocol");
      System.setProperty("javax.net.ssl.keystore", properties.getProperty("keystore.file", "/etc/licenseserver/eyeball-ls.pkcs12"));
      System.setProperty("javax.net.ssl.keyStorePassword", properties.getProperty("keystore.password", "keytool"));
      System.setProperty("javax.net.ssl.keyStoreType", properties.getProperty("keystore.type", "pkcs12"));
      System.setProperty("javax.net.ssl.trustStore", properties.getProperty("truststore.file", "/etc/licenseserver/truststore.jks"));
      System.setProperty("javax.net.ssl.trustStorePassword", properties.getProperty("truststore.password", "keytool"));

      // decide if we are master or slave
      masterMode = true;  // master by default
      if (properties.getProperty("masterMode", "none").equals("slave"))
        masterMode = false;
      if (!masterMode)
      {
        masterHost = properties.getProperty("masterHost", "");
        log("I am running in slave mode, master is at https://"+masterHost+".");
      } else {
        log("I am running in master mode.");
      }

      // setup stuff in order to be able to connect to another server using https/ssl
      try {
	SSLContext ctx;
	KeyManagerFactory kmf;
	KeyStore ks;
	char[] passphrase = properties.getProperty("keystore.password", "keytool").toCharArray();

	ctx = SSLContext.getInstance("TLS");
	kmf = KeyManagerFactory.getInstance("SunX509");
	ks = KeyStore.getInstance(properties.getProperty("keystore.type", "pkcs12"));

	ks.load(new FileInputStream(properties.getProperty("keystore.file", "/etc/licenseserver/eyeball-ls.pkcs12")), passphrase);

	kmf.init(ks, passphrase);
	ctx.init(kmf.getKeyManagers(), null, null);

	SSLSocketFactory factory = ctx.getSocketFactory();

	HttpsURLConnection.setDefaultSSLSocketFactory(factory);
      } catch (Exception ex) {
	// This can be an IOException, NoSuchAlgorithmException, KeyStoreException, or KeyManagementException
	throw new ServletException("Could not initialize SSL socket factory: " + ex.getMessage());
      }
    }


  public String getVersion()
  { return Version; }


  /**
   * Reloads the licenses from file, but only if the file had changed.
   * Should be synchronized to prevent the creation of two licenses objects.
   */
  protected Licenses getLicenses()
  {
    //      System.out.println("getLicenses: user.cwd=" + System.getProperty("user.dir"));
    // we have to load from file
    ServletContext ctx = getServletConfig().getServletContext();
    String licenseFileName = properties.getProperty("licenseFile", "/etc/licenseserver/license-file");

    int MaxCacheAge = 5*60*1000; // in msec
    long timeNow = System.currentTimeMillis();

    // licenses can come from three sources:
    // file (if master), master server (if slave), or cache
    boolean filePresent = false;
    Licenses licnsesFromFile = null;
    long fileDate = 0l;
    boolean masterPresent = false;
    Licenses licensesFromMaster = null;
    long masterDate = 0l;
    boolean cachePresent = false;
    Licenses licensesFromCache = null;
    long cacheDate = 0l;

    // in master mode check the file, even if we use the cached version
    if (masterMode)
    {
      File licensesFile = new File(licenseFileName);
      if (!licensesFile.exists())
      {
        log("getLicenses: Could not open licenses file '"+licenseFileName+"'.");
      }
      filePresent = true;
      fileDate = licensesFile.lastModified();
    }

    // try to get cached version
    try {
      licensesFromCache = (Licenses)ctx.getAttribute("mylicenses");
      if (null != licensesFromCache)
      {
        cachePresent = true;
        cacheDate = licensesFromCache.getLoadedTime();
        //System.out.println("getLicenses: licenses retrieved from cache t="+cacheDate+".");
      }
    } catch (ClassCastException ex) {
      System.out.println("LicenseCommandHandler.getLicenses: ClassCastException");
      // We'll treat it as if there is no mylicenses.  licenses = null.
    } catch (NullPointerException ex) {
    }

    // in slave mode: if no cache, or cache is old, try to obtain from meta
    if (!masterMode)
    {
      if (!cachePresent || (timeNow-cacheDate >= MaxCacheAge))
      {
        try {
          licensesFromMaster = getLicenseFromMasterHost();
          if (null != licensesFromMaster)
          {
            masterPresent = true;
            masterDate = licensesFromMaster.getLoadedTime();
            System.out.println("getLicenses: licenses retrieved from master t="+masterDate+".");
          }
        } catch (Exception ex) {
          licensesFromMaster = null;
        }
      }
    }

    // if none is present, freak out
    if (!filePresent && !cachePresent && !masterPresent)
    {
      if (masterMode)
        log("Error: could not obtain licenses.  Check the licenses file! ("+licenseFileName+").");
      else
        log("Error: could not obtain licenses.  Check the master server on "+masterHost+".");
      // remove cache
      ctx.removeAttribute("mylicenses");
      return null;
    }

    // at least one is present, decide which one to use
    // 1=file, 2=master, 3=cache
    int use = 1;
    if (filePresent)   use=1;
    if (cachePresent)  use=3;
    if (masterPresent) use=2;
    // if both cache and file is present, and file is newer, use file
    if (cachePresent && filePresent)
      if (cacheDate <= fileDate)
        use=1;
    System.out.println("getLicenses: ("+filePresent+", "+fileDate+") ("+
      masterPresent+", "+masterDate+") ("+
      cachePresent+", "+cacheDate+")  use: "+use);

    // set licenses into newLicenses
    Licenses newLicenses = null;
    switch (use)
    {
     case 2:
      newLicenses = licensesFromMaster;
      log("getLicenses: License configuration obtained from master server "+masterHost+".");
      break;

     case 3:
      newLicenses = licensesFromCache;
      // no log
      break;

     default: //1
      //System.out.println("LicenseCommandHandler.getLicenses: ctx="+ctx);
      Licenses licensesFromFile = new Licenses();
      newLicenses = new Licenses();
      System.out.println("user.dir="+System.getProperty("user.dir"));
      try {
        licensesFromFile.loadFromFile(licenseFileName);
        log("getLicenses: License configuration obtained from file '"+licenseFileName+"'.");
        newLicenses = licensesFromFile;
      } catch (IOException ex) {
        log("Could not open file "+licenseFileName+": " + ex.getMessage());
      }
      break;
    }

    if (null != newLicenses)
    {
      // if cached present, but not using it, transfer dynamic state
      if ((cachePresent) && (3 != use))
      {
        int count = newLicenses.setDynamicPropsFromLicenses(licensesFromCache);
        log("Preserved the state of " + count + " licenses.");
      }
      // set in cache
      ctx.setAttribute("mylicenses", newLicenses);
    }

    //      System.out.println("LicenseCommandHandler.getLicenses: licenses="+licenses);
    return newLicenses;
  }


  void sendLicense(License license, PrintWriter writer, String localHost)
  {
    writer.print("licenseId="+URLEncoder.encode(license.id));
    writer.print("&errorCode=200");
    writer.print("&errorMessage=Success");
    writer.print("&owner="+URLEncoder.encode(license.owner));
    String lesseeName = license.getLesseeName()==null?"":license.getLesseeName();
    writer.print("&lesseeName="+URLEncoder.encode(lesseeName));
    writer.print("&leaseExpiry="+URLEncoder.encode(""+license.getLeaseExpiry()));
    // if we are slave, 'home' is always the master
    if (!masterMode)
      writer.print("&home="+URLEncoder.encode(masterHost));
    else
      writer.print("&home="+URLEncoder.encode(localHost));
    Iterator iter = license.terms.entrySet().iterator();
    for (int i = 0; iter.hasNext(); i++)
    {
      Map.Entry entry = (Map.Entry)iter.next();
      String termName = (String)entry.getKey();
      String termValue = (String)entry.getValue();
      writer.print("&term"+i+"="+URLEncoder.encode(termName + " \"" + termValue + "\""));
    }
    writer.println();
  }


  public void sendHttpError(HttpServletResponse response, int errorCode, String message)
    {
      try {
	response.getWriter().println(message);
	response.setStatus(errorCode);
      } catch (IOException ex) {
	// Lost the client.  Quash the error.
      }
    }

  public void sendError(HttpServletResponse response, int errorCode, String message)
    {
      try {
	response.getWriter().println("errorCode="+errorCode+"&errorMessage="+URLEncoder.encode(message));
	response.setStatus(HttpServletResponse.SC_OK);
      } catch (IOException ex) {
	// Lost the client.  Quash the error.
      }
    }


  boolean authenticate(String authorizedDN, String providedDN)
    {
      System.out.println("authenticate: compare "+authorizedDN+" to "+providedDN);

      if (authorizedDN.equals(providedDN))
      {
	System.out.println("authenticate: approved because authorized and provided are identical.");
	return true;
      }

      if (authorizedDN.equals("nonerequired"))
      {
	System.out.println("authenticate: approved because authorization not required.");
	return true;
      }

      if (authorizedDN.equals("anyname") &&
	  !providedDN.equals("noneprovided"))
      {
	System.out.println("authenticate: approved because any provided name is acceptable");
	return true;
      }

      if (providedDN.equals(licenseServerCertSubject))
      {
        System.out.println("authenticate: approved because provided name is the license server.");
        return true;
      }

      System.out.println("authenticate: rejected.");
      return false;
    }


  protected Licenses getLicenseFromMasterHost()
  {
    System.out.println("getLicenseFromMasterHost");

    HttpsURLConnection.setDefaultHostnameVerifier(new PermissiveHostnameVerifier());

    // Can throw MalformedURLException or IOException
    URL url = null;
    try {
      url = new URL("https://"+masterHost+"/LicenseServer/servlet/CommandLicenseConfig");
      HttpsURLConnection conn = (HttpsURLConnection)url.openConnection();
      conn.connect();
      System.out.println("getLicenseFromMasterHost: Connected to master");
      int respCode = conn.getResponseCode();

      //      conn.setHostnameVerifier(new PermissiveHostnameVerifier());
      //      conn.set
      //      System.out.println("URL connection is " + conn);

      if (respCode != 200)
      {
        log("getLicenseFromMasterHost: Host "+masterHost+" returned an error of " + respCode + ".");
        return null;
      }

      InputStream in = conn.getInputStream();

      Licenses licenses = new Licenses();
      boolean res = licenses.loadFromStream(in);
      if (!res)
      {
        log("getLicenseFromMasterHost: Error parsing licenses conf obtained from master host "+masterHost+".");
        return null;
      }

      // OK, set in cache
      ServletContext ctx = getServletConfig().getServletContext();
      ctx.setAttribute("mylicenses", licenses);

      return licenses;

    } catch (MalformedURLException ex) {
      log("MalformedURLException in url=" + url + ".  Maybe JSSE not installed?");
      return null;
    } catch (IOException ex) {
      log("IOException while trying to connect to master. (" + ex.getMessage() + ")");
      return null;
    }
  }


  abstract public void doMyGet(HttpServletRequest request,
    HttpServletResponse response)
    throws IOException, ServletException;


  public void doSomeWorkBefore()
  {
    periodicChecks();
  }


  public void doSomeWorkAfter()
  {
  }


  public void doGet(HttpServletRequest request,
    HttpServletResponse response)
    throws IOException, ServletException
  {
    doSomeWorkBefore();
    doMyGet(request, response);
    doSomeWorkAfter();
  }


  protected void periodicChecks()
  {
    boolean needtodo = false;
    long now = System.currentTimeMillis() / 1000;
    if (0 == lastPeriodicChecksTime)
      needtodo = true;
    else
    {
      long ago = now - lastPeriodicChecksTime;
      if (ago > periodicChecksFrequency)
        needtodo = true;
    }
    if (!needtodo)
      return;

    // OK, time to do it
    log("periodic checks ... ");

    releaseExpiredLicenses();

    checkLicenseExpiries();

    // don't forget to update this, but only at the end
    lastPeriodicChecksTime = now;
  }


  /**
   * Cycles through all the licenses, and release()s those which are expired
   * Note that we do not rely on this, as there is no guarantee it is executed.
   */
  protected void releaseExpiredLicenses()
  {
    long now = System.currentTimeMillis()/1000;
    Licenses licenses = getLicenses();
    Vector ids = licenses.getLicenseIds();
    Iterator iter = ids.iterator();
    while (iter.hasNext())
    {
      String id = (String)iter.next();
      License license = licenses.getLicense(id);
      String lessee = license.getLesseeName();
      long leaseExpiry = license.getLeaseExpiry();
      if (0 != leaseExpiry)
        if (leaseExpiry < now)
        {
          long ago = now - leaseExpiry;
          log("Lease on license " + id + " has expired " + ago + " secs ago.");
          license.release( lessee );
        }
    }
  }


  protected void checkLicenseExpiries()
  {
    long now = System.currentTimeMillis()/1000;
    long threeDays = 3*24*60*60;
    long oneDay = 24*60*60;
    long threeHours = 3*60*60;
    Licenses licenses = getLicenses();
    Vector ids = licenses.getLicenseIds();
    Iterator iter = ids.iterator();
    while (iter.hasNext())
    {
      String id = (String)iter.next();
      License license = licenses.getLicense(id);
      long ago = now - license.expiry;
      long lastAgo = lastPeriodicChecksTime - license.expiry;
      if (license.expiry >= 0)
      {
        if (ago >= 0)
        {
          log("ExpiryWarning: license " + id + " has expired " + ago + " secs ago.");
        }
        // in 3 hours
        else if (ago >= -threeHours)
        {
          if (lastAgo < -threeHours)
            log("ExpiryWarning: license " + id + " will expire in less than 3 hours (" + -ago/60 + " minutes).");
        }
        // in 1 day
        else if (ago >= -oneDay)
        {
          if (lastAgo < -oneDay)
            log("ExpiryWarning: license " + id + " will expire in less than a day (" + -ago/3600 + " hours).");
        }
        // in 3 days
        else if (ago >= -threeDays)
        {
          if (lastAgo < -threeDays)
            log("ExpiryWarning: license " + id + " will expire in less than 3 days (" + -ago/3600 + " hours).");
        }
      }
    }
  }
}
