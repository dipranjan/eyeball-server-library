import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import java.io.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Vector;
import java.util.Iterator;
import java.util.Date;
import java.text.SimpleDateFormat;


public class CommandLicenseConfig
  extends LicenseCommandHandler
{
  public void doMyGet(HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException
  {
    String clientIdentity = (String)request.getAttribute("SSL_CLIENT_S_DN");
    response.setContentType("text/plain");

    PrintWriter out = new PrintWriter(response.getOutputStream());

    // must be master
    if (!masterMode)
    {
      out.println("Error: Not running in master mode.");
      out.close();
      response.setStatus(HttpServletResponse.SC_NO_CONTENT);
      return;
    }

    // check identity of peer

    log("CommandLicenseConfig: request from clientId=" + clientIdentity);
    if (null == clientIdentity)
    {
      out.println("Error: Missing client identity.");
      out.close();
      response.setStatus(HttpServletResponse.SC_FORBIDDEN);
      return;
    }

    if (!authenticate(licenseServerCertSubject, clientIdentity))
    {
      log("CommandLicenseConfig: Client is not a license server.  Request rejected.");
      out.println("Error: Client is not a license server.  Request rejected.");
      out.close();
      response.setStatus(HttpServletResponse.SC_FORBIDDEN);
      return;
    }

    // open licenses file
    String licenseFileName = properties.getProperty("licenseFile", "/etc/licenseserver/license-file");
    File licenseFile = null;
    try {
      licenseFile = new File(licenseFileName);
    } catch (Exception ex) {
      licenseFile = null;
    }
    if (null == licenseFile)
    {
      out.println("Error: Could not open file "+licenseFileName+".");
      out.close();
      response.setStatus(HttpServletResponse.SC_NOT_FOUND);
      return;
    }
    FileReader licenseFileReader = null;
    try {
      licenseFileReader = new FileReader(licenseFile);
    } catch (Exception ex) {
      licenseFileReader = null;
    }
    if (null == licenseFileReader)
    {
      out.println("Error: Could not open file "+licenseFileName+".");
      out.close();
      response.setStatus(HttpServletResponse.SC_NOT_FOUND);
      return;
    }

    int ch;
    while (licenseFileReader.ready())
    {
      try {
        ch = licenseFileReader.read();
      } catch (Exception ex) {
        break;
      }
      if (ch < 0) break;
      out.write(ch);
    }
    licenseFileReader.close();

    out.close();
    response.setStatus(HttpServletResponse.SC_OK);

    log("CommandLicenseConfig: Configuration sent to " + request.getRemoteAddr()+".");

    // additionaly, refresh licences
    Licenses licenses = getLicenses();
  }
}
