/* $Id: ServerStatus.java,v 1.1 2001/06/14 23:44:53 marwood Exp $
 *
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 *
 *
 * @author James Duncan Davidson <duncan@eng.sun.com>
 * @author Jason Hunter <jch@eng.sun.com>
 */

public class ServerStatus extends HttpServlet {

  public void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
    {
      PrintWriter out = response.getWriter();
      response.setContentType("text/plain");

      out.println("Right now this just confirms servlets can be called.");
      out.println("I'd like the to report statistics such as which licenses are");
      out.println("leased by who.");
      out.println();
      out.println("Servlet init parameters:");
      Enumeration e = getInitParameterNames();
      while (e.hasMoreElements()) {
	String key = (String)e.nextElement();
	String value = getInitParameter(key);
	out.println("   " + key + " = " + value); 
      }
      out.println();

      out.println("Context init parameters:");
      ServletContext context = getServletContext();
      Enumeration enum = context.getInitParameterNames();
      while (enum.hasMoreElements()) {
	String key = (String)enum.nextElement();
	Object value = context.getInitParameter(key);
	out.println("   " + key + " = " + value);
      }
      out.println();

      out.println("Context attributes:");
      enum = context.getAttributeNames();
      while (enum.hasMoreElements()) {
	String key = (String)enum.nextElement();
	Object value = context.getAttribute(key);
	out.println("   " + key + " = " + value);
      }
      out.println();
      
      out.println("Request attributes:");
      e = request.getAttributeNames();
      while (e.hasMoreElements()) {
	String key = (String)e.nextElement();
	Object value = request.getAttribute(key);
	out.println("   " + key + " = " + value);
      }
      out.println();
      out.println("Servlet Name: " + getServletName());
      out.println("Protocol: " + request.getProtocol());
      out.println("Scheme: " + request.getScheme());
      out.println("Server Name: " + request.getServerName());
      out.println("Server Port: " + request.getServerPort());
      out.println("Server Info: " + context.getServerInfo());
      out.println("Remote Addr: " + request.getRemoteAddr());
      out.println("Remote Host: " + request.getRemoteHost());
      out.println("Character Encoding: " + request.getCharacterEncoding());
      out.println("Content Length: " + request.getContentLength());
      out.println("Content Type: "+ request.getContentType());
      out.println("Locale: "+ request.getLocale());
      out.println("Default Response Buffer: "+ response.getBufferSize());
      out.println();
      out.println("Parameter names in this request:");
      e = request.getParameterNames();
      while (e.hasMoreElements()) {
	String key = (String)e.nextElement();
	String[] values = request.getParameterValues(key);
	out.print("   " + key + " = ");
	for(int i = 0; i < values.length; i++) {
	  out.print(values[i] + " ");
	}
	out.println();
      }
      out.println();
      out.println("Headers in this request:");
      e = request.getHeaderNames();
      while (e.hasMoreElements()) {
	String key = (String)e.nextElement();
	String value = request.getHeader(key);
	out.println("   " + key + ": " + value);
      }
      out.println();  
      out.println("Cookies in this request:");
      Cookie[] cookies = request.getCookies();
      for (int i = 0; i < cookies.length; i++) {
	Cookie cookie = cookies[i];
	out.println("   " + cookie.getName() + " = " + cookie.getValue());
      }
      out.println();
      
      out.println("Request Is Secure: " + request.isSecure());
      out.println("Auth Type: " + request.getAuthType());
      out.println("HTTP Method: " + request.getMethod());
      out.println("Remote User: " + request.getRemoteUser());
      out.println("Request URI: " + request.getRequestURI());
      out.println("Context Path: " + request.getContextPath());
      out.println("Servlet Path: " + request.getServletPath());
      out.println("Path Info: " + request.getPathInfo());
      out.println("Path Trans: " + request.getPathTranslated());
      out.println("Query String: " + request.getQueryString());
      
      out.println();
      HttpSession session = request.getSession();
      out.println("Requested Session Id: " +
		  request.getRequestedSessionId());
      out.println("Current Session Id: " + session.getId());
      out.println("Session Created Time: " + session.getCreationTime());
      out.println("Session Last Accessed Time: " +
		  session.getLastAccessedTime());
      out.println("Session Max Inactive Interval Seconds: " +
		  session.getMaxInactiveInterval());
      out.println();
      out.println("Session values: ");
      Enumeration names = session.getAttributeNames();
      while (names.hasMoreElements()) {
	String name = (String) names.nextElement();
	out.println("   " + name + " = " + session.getAttribute(name));
      }
      out.println();
      
      out.println("Diagnostics:");
      
      if (!request.isSecure())
      {
	out.println("You are not using a secure protocol.  The URL should start with https,");
	out.println("and Apache must be configured to use https/SSL.  See ");
	out.println("http://www.modssl.org.");
	out.println();
      }
      
      else if (request.getAttribute("SSL_PROTOCOL") == null)
      {
	out.println("SSL_PROTOCOL is not in the attributes.  You may need to add to your httpd.conf:");
	out.println("  <IfModule mod_jk.c>");
	out.println("  JkWorkersFile /etc/apache/mod_jk/workers.properties");
	out.println("  JkExtractSSL On");
	out.println("  JkEnvVar SSL_PROTOCOL default");
	out.println("  JkEnvVar SSL_CIPHER default");
	out.println("  JkEnvVar SERVER_SOFTWARE default");
	out.println("  JkEnvVar SSL_CLIENT_S_DN default");
	out.println("  JkEnvVar SSL_SERVER_S_DN default");
	out.println("  SSLOptions +StdEnvVars");
	out.println("  Include /etc/apache/mod_jk/tomcat-auto");
	out.println("  </IfModule>");
	out.println();
      }

      else if (request.getAttribute("SSL_CLIENT_S_DN") == null ||
	       "default".equals(request.getAttribute("SSL_CLIENT_S_DN")))
      {
	out.println("SSL_CLIENT_S_DN is not a client distinguishing name.  You probably need");
	out.println("the line:");
	out.println("  SSLVerifyClient require");
	out.println("Once you do this, you probably can't access this page from a browser since");
	out.println("I don't know how to give a client certificate to a browser.  The license");
	out.println("server client software always provides a certificate.");
	out.println();
      }
    }
}

