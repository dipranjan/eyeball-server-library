import java.util.HashMap;
import java.util.NoSuchElementException;
import java.util.Vector;
import java.util.Iterator;
import java.util.Properties;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParsePosition;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
// import java.io.FileWriter;
// import java.io.PrintWriter;


class Licenses
{
  protected HashMap licenses = new HashMap();

  /** the time when this object was read from the licenses file */
  protected long loadedTime = 0l;


  synchronized License getLicense(String licenseId)
    {
      License license = (License)licenses.get(licenseId);

      if (license == null)
	throw new NoSuchElementException("No entry for " + licenseId);

      return license;
    }


  synchronized boolean loadFromStream(InputStream in)
    throws IOException
  {
    //System.out.println("loadFromStream:");

    Properties licenseFile = new Properties();
    // Can throw FileNotFoundException
    licenseFile.load(in);

    // no turning back now
    licenses.clear();

    Iterator iter = licenseFile.keySet().iterator();
    while (iter.hasNext())
    {
      String propName = (String)iter.next();
      System.out.println("loadFromFile: Examing propName="+propName);

      if (!propName.startsWith("license.") || !propName.endsWith(".owner"))
        continue;

      // It's a license.*.owner property.

      String id = propName.substring(8, propName.length()-6);
      System.out.println("loadFromFile: It's a license owner, id="+id);
      if (id.equals(""))
        continue;

      License newLicense = new License();
      newLicense.id = id;
      newLicense.owner = licenseFile.getProperty("license."+id+".owner");
      if (newLicense.owner == null)
        continue;

      // no longer use home
      if (null != licenseFile.getProperty("license."+id+".home"))
        System.out.println("loadFromFile: Warning: Property 'home' not used any more! ("+id+")");

      // no longer use home
      String expiryStr = licenseFile.getProperty("license."+id+".expiry");
      if (null != expiryStr)
      {
        System.out.println("loadFromFile: expiry=" + expiryStr);
        SimpleDateFormat dateFromatter = new SimpleDateFormat("yyyy/MM/dd");
        ParsePosition parsePos = new ParsePosition(0);
        Date date = dateFromatter.parse(expiryStr, parsePos);
        // unparseable date
        if (null == date)
        {
          System.out.println("loadFromFile: Unparsable expiry date " + expiryStr);
          // rather then falling back on no expiry, drop whole license
          // This way somebody will notice that something is wrong
          continue;
        }
        newLicense.expiry = date.getTime() / 1000;
        System.out.println("loadFromFile: expiry=" + dateFromatter.format(date) + " " + date.getTime());
      }

      int numTerms = 0;
      while (true)
      { // Loop through .terms entries.
        String termKeyBase = "license."+id+".term"+(numTerms+1);
        String termName = licenseFile.getProperty(termKeyBase+".name");
        String termValue = licenseFile.getProperty(termKeyBase+".value");
        System.out.println("loadFromFile: Got term: "+termKeyBase+".name="+termName+", "+termKeyBase+".value="+termValue);
        if (termName == null ||
          termValue == null)
          break;

        newLicense.terms.put(termName, termValue);

        numTerms++;
      }

      licenses.put(newLicense.id, newLicense);
    }

    // done
    loadedTime = System.currentTimeMillis();
    //log("loadFromFile: Licenses loaded from file '" + filename + "', time=" + loadedTime);
    //System.out.println("loadFromFile: Licenses loaded from file '" + filename + "', time=" + loadedTime);

    /*
      BufferedReader fin = new BufferedReader(new FileReader(filename));
      if (fin == null)
	throw new IOException("File " + filename + " does not exist.");

      while (true)
      {
	License newLicense = new License();

	newLicense.id = fin.readLine();
	newLicense.owner = fin.readLine();
	newLicense.home = fin.readLine();
	String numTermsLine = fin.readLine();
	if (newLicense.id == null ||
	    newLicense.owner == null ||
	    numTermsLine == null)
	  break;

	long numTerms;
	try {
	  numTerms = Long.parseLong(numTermsLine);
	} catch (NumberFormatException ex) {
	  throw new IOException("Malformed numTerms line: " + ex.getMessage());
	}
	for (long i = 0; i < numTerms; i++)
	{
	  String termLine = fin.readLine();
	  if (termLine == null)
	    throw new IOException("Malformed file " + filename + ".  Not enough terms at end of file.");

	  int firstSpace = termLine.indexOf(' ');
	  if (firstSpace < 0)
	    throw new IOException("Malformed file " + filename + ".  Term line does not contain a space.");

	  String termName = termLine.substring(0, firstSpace);
	  String termValue = termLine.substring(firstSpace+1).trim();

	  //	  log("Adding term " + termName + "=" + termValue);

	  newLicense.terms.put(termName, termValue);
	}

	licenses.put(newLicense.id, newLicense);
      }

      fin.close();
      */
      return true;
  }


  synchronized void loadFromFile(String filename)
    throws IOException, FileNotFoundException
  {
    System.out.println("loadFromFile:");

    FileInputStream in = new FileInputStream(filename);

    boolean res = loadFromStream(in);
    if (!res)
      throw new IOException();

    //log("loadFromFile: Licenses loaded from file '" + filename + "', time=" + loadedTime);
    System.out.println("loadFromFile: Licenses loaded from file '" + filename + "', time=" + loadedTime);
  }


  synchronized Vector getLicenseIds()
  {
    Vector ids = new Vector();
    Iterator iter = licenses.keySet().iterator();
    while (iter.hasNext())
    {
      String id = (String)iter.next();
      ids.add(id);
    }

    return ids;
  }


  synchronized Vector getSortedLicenseIds()
  {
    Vector ids = new Vector();
    Iterator iter = licenses.keySet().iterator();
    while (iter.hasNext())
    {
      String id = (String)iter.next();
      ids.add(id);
    }
    // sort the vector
    java.util.Collections.sort(ids);
    //, new Comparator() {    });

    return ids;
  }


  /*
  synchronized static void log(String line)
    {
      try {
	PrintWriter out = new PrintWriter(new FileWriter("/tmp/logger", true));

	out.println(line);
	out.close();
      } catch (IOException ex) {
	// Not much we can do.  Too annoying to pass to the higher layer.
      }
    }
  */

  public long getLoadedTime() { return loadedTime; }


  /**
   * For each license, if it is found in the other too,
   * it transfers the dynamic state to this one.
   * @return the number of licenses processed.
   */
  public int setDynamicPropsFromLicenses(Licenses l2)
  {
    int count = 0;
    Vector ids1 = getLicenseIds();
    Iterator iter1 = ids1.iterator();
    while (iter1.hasNext())
    {
      String id = (String)iter1.next();
      // license here
      License license1 = getLicense(id);
      // check in other
      License license2 = null;
      try {
        license2 = l2.getLicense(id);
      } catch (NoSuchElementException ex) {
      }
      if (null == license2)
        continue;
      // we have it in both
      license1.setDynamicPropsFromLicense(license2);
      count++;
    }
    return count;
  }
}
