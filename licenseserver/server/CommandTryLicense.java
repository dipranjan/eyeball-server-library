import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import java.io.PrintWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.Enumeration;
import java.security.cert.X509Certificate;
import javax.servlet.ServletContext;
import java.util.NoSuchElementException;
import java.util.Map;
import java.util.Vector;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import com.sun.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpUtils;
import java.net.URLEncoder;
import com.sun.net.ssl.SSLContext;
import com.sun.net.ssl.KeyManagerFactory;
import java.security.KeyStore;
import java.io.FileInputStream;
import javax.net.ssl.SSLSocketFactory;


public class CommandTryLicense
extends LicenseCommandHandler
{
  public static void main(String argv[])
    throws Exception
    {
      System.setProperty("java.protocol.handler.pkgs", "com.sun.net.ssl.internal.www.protocol");
      System.setProperty("javax.net.ssl.keystore", "/tmp/marwood/.keystore");
      System.setProperty("javax.net.ssl.keyStorePassword", "keytool");
      System.setProperty("javax.net.ssl.trustStore", "/tmp/marwood/.keystore");
      System.setProperty("javax.net.ssl.trustStorePassword", "keytool");

      CommandTryLicense app = new CommandTryLicense();
      app.doTryLicense("david", "app1", 900000000, "davidowner", 2, true, new Vector(), new URL("https://dove.eyeball.com:12345/myTryLicense"), null);
    }


  public void init()
    throws ServletException
    {
      super.init();

      /*
      try {
	SSLContext ctx;
	KeyManagerFactory kmf;
	KeyStore ks;
	char[] passphrase = properties.getProperty("keystore.password", "keytool").toCharArray();

	ctx = SSLContext.getInstance("TLS");
	kmf = KeyManagerFactory.getInstance("SunX509");
	ks = KeyStore.getInstance(properties.getProperty("keystore.type", "pkcs12"));

	ks.load(new FileInputStream(properties.getProperty("keystore.file", "/etc/licenseserver/eyeball-ls.pkcs12")), passphrase);

	kmf.init(ks, passphrase);
	ctx.init(kmf.getKeyManagers(), null, null);

	SSLSocketFactory factory = ctx.getSocketFactory();

	HttpsURLConnection.setDefaultSSLSocketFactory(factory);
      } catch (Exception ex) {
	// This can be an IOException, NoSuchAlgorithmException, KeyStoreException, or KeyManagementException
	throw new ServletException("Could not initialize SSL socket factory: " + ex.getMessage());
      }
      */
    }


  public void doMyGet(HttpServletRequest request,
		    HttpServletResponse response)
    throws IOException, ServletException
    {
      String licenseId = (String)request.getParameter("licenseId");
      String owner = (String)request.getAttribute("SSL_CLIENT_S_DN");
      String executionName = (String)request.getParameter("executionName");
      String leaseExpiryStr = (String)request.getParameter("leaseExpiry");
      String ttlStr = (String)request.getParameter("ttl");
      String forceStr = ((String)request.getParameter("force")).trim();

      Vector logMessages = new Vector();
      for (int i = 0; true; i++)
      {
	String nextLog = (String)request.getParameter("logMessage"+i);
	//	System.out.println("Next log message " + i + " = " + nextLog);
	if (nextLog == null)
	  break;

	logMessages.add(nextLog);
      }


      if (licenseId == null ||
	  owner == null ||
	  executionName == null ||
	  leaseExpiryStr == null)
      {
	sendError(response, HttpServletResponse.SC_BAD_REQUEST, "licenseId, executionName, leaseExpiry, or client certificate missing.  licenseId=" + licenseId + ", leaseExpiry=" + leaseExpiryStr + ", owner=" + owner);
	return;
      }

      long leaseExpiry;
      try {
	leaseExpiry = Long.parseLong(leaseExpiryStr);
      } catch (NumberFormatException ex) {
	sendError(response, HttpServletResponse.SC_BAD_REQUEST, "leaseExpiry is not a number.");
	return;
      }

      long ttl;
      try {
	ttl = Long.parseLong(ttlStr);
      } catch (NumberFormatException ex) {
	ttl = 2;
      }

      boolean force = "1".equals(forceStr);

      String requestUrlStr = HttpUtils.getRequestURL(request).toString();
      URL requestUrl;
      requestUrl = new URL(requestUrlStr);

      doTryLicense(licenseId, executionName, leaseExpiry, owner, ttl, force, logMessages, requestUrl, response);
    }


  void doTryLicense(String licenseId, String executionName, long leaseExpiry, String owner, long ttl, boolean force, Vector logMessages, URL requestUrl, HttpServletResponse response)
    {
      //      System.out.println("doTryLicense:");
      log("TryLicense: ("+licenseId+","+executionName+"), requested expiry="+leaseExpiry+", client id="+owner+", force="+force);

      long now = System.currentTimeMillis()/1000;
      long maxLeaseDuration;
      try {
	maxLeaseDuration = Long.parseLong(properties.getProperty("maxLeaseDuration", "86400"));
      } catch (NumberFormatException ex) {
	log("Property maxLeaseDuration is malformed.  Using default.");
	maxLeaseDuration = 86400;
      }

      long minLeaseDuration;
      try {
	minLeaseDuration = Long.parseLong(properties.getProperty("minLeaseDuration", "20"));
      } catch (NumberFormatException ex) {
	log("Property minLeaseDuration is malformed.  Using default.");
	minLeaseDuration = 20;
      }

      Licenses licenses = getLicenses();
      License license = null;
      // in master mode, we must have the licenses
      if (masterMode)
      {
        if (licenses == null)
        {
	  log("("+licenseId+","+executionName+") Couldn't create a licenses collection.  Request rejected.");

	  sendError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Could not create a license collection.");
	  return;
        }
      } else {
        license = new License();
        license.id = licenseId;
      }

      if (null != licenses)
      {
        try {
          license = licenses.getLicense(licenseId);
        } catch (NoSuchElementException ex) {
          log("("+licenseId+","+executionName+") No such license id.  Request rejected.");
          sendError(response, HttpServletResponse.SC_NOT_FOUND, "License id " + licenseId + " does not exist.");
          return;
        }

        System.out.println("Found license.  owner is " + license.owner +
          ", connected client is " + owner);

        if (!authenticate(license.owner, owner))
        {
          log("("+licenseId+","+executionName+") Client is not the license owner.  Request rejected.");
          sendError(response, HttpServletResponse.SC_FORBIDDEN, "You are not the license owner.");
          return;
        }
      }

      // check license expiry
      System.out.println("doTryLicense: license.expiry = " + license.expiry + ", now = " + now);
      if (license.expiry >= 0)
      {
        if (now >= license.expiry)
        {
          log("("+licenseId+","+executionName+") Time is " + now + ", license expiry is " + license.expiry + ".  Request rejected because license is expired.");
          sendError(response, HttpServletResponse.SC_FORBIDDEN, "This license had expired.");
          return;
        }
      }

      if (now+minLeaseDuration > leaseExpiry)
      {
        log("("+licenseId+","+executionName+") Server time is " + now + " and requested expiry is " + leaseExpiry + ".  This is less than the minimum lease time of " + minLeaseDuration + " seconds.  Perhaps the client clock is slow.  Request cannot be processed.");
        //	try {
        sendHttpError(response, HttpServletResponse.SC_FORBIDDEN, "Server time is " + now + " and requested expiry is " + leaseExpiry + ".  This is less than the minimum lease time of " + minLeaseDuration + " seconds.  Perhaps the client clock is slow.  Request cannot be processed.");
        //	} catch (IOException ex) {
        // Lost the client.  Quash the error.
        //	}
        return;
      }

      if (now+maxLeaseDuration < leaseExpiry)
      {
	log("("+licenseId+","+executionName+") Server time is " + now + " and requested expiry is " + leaseExpiry + ".  This is more than the maximum lease time of " + maxLeaseDuration + " seconds.  Perhaps the client clock is fast.  Request cannot be processed.");
	//	try {
	  sendHttpError(response, HttpServletResponse.SC_FORBIDDEN, "Server time is " + now + " and requested expiry is " + leaseExpiry + ".  This is more than the maximum lease time of " + maxLeaseDuration + " seconds.  Perhaps the client clock is fast.  Request cannot be processed.");
	  //	} catch (IOException ex) {
	// Lost the client.  Quash the error.
	  //	}
	return;
      }

      //System.out.println("request url host="+requestUrl.getHost());
      String localHost = requestUrl.getHost()+(requestUrl.getPort()==-1?"":":"+requestUrl.getPort());

      // in slave mode, ask master
      if (!masterMode)
      {
	System.out.println("Consulting master if ttl is >= 1.");
	// consult the master
	if (ttl >= 1)
	  try {
	    doTryLicenseOtherHost(masterHost, licenseId, executionName, leaseExpiry, owner, ttl, force, response);
	    log("("+licenseId+","+executionName+") Request answered by master host.");
	    // If doTryLicenseOtherHost succeeds, then it has sent a response.
	    return;
	  } catch (IOException ex) {
	    // We'll give the benefit of the doubt and grant the lessee
	    log("("+licenseId+","+executionName+") Master could not be contacted: " + ex.getMessage() + ".  Attempting lease from this server instead.");
	  }
      }

      boolean ret = license.lease(executionName, leaseExpiry, force);
      if (ret == false)
      {
	log("("+licenseId+","+executionName+") Already leased by " + license.getLesseeName());
	sendError(response, HttpServletResponse.SC_CONFLICT, "Already leased by " + license.getLesseeName());
	return;
      }

      log("("+licenseId+","+executionName+") Lease granted.");

      // log the logMessages
      Iterator iter = logMessages.iterator();
      while (iter.hasNext())
      {
	String nextLog = (String)iter.next();
        license.setLastClientLog( nextLog );
	log("("+licenseId+","+executionName+") Log: " + nextLog);
      }

      try {
	sendLicense(license, response.getWriter(), localHost);
      } catch (IOException ex) {
	// Thrown by getWriter(), not sendLicense().
	log("("+licenseId+","+executionName+") Lease granted, but client could not be notified.");
	sendError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Couldn't create output stream.  Try again.");
	return;
      }

      response.setStatus(HttpServletResponse.SC_OK);
    }


  protected static void doTryLicenseOtherHost(String host, String licenseId, String executionName, long leaseExpiry, String owner, long ttl, boolean force, HttpServletResponse response)
    throws IOException
    {
      System.out.println("doTryLicenseOtherHost: host="+host+", licenseId="+licenseId+", executionName="+executionName+", leaseExpiry="+leaseExpiry+", owner="+owner+", ttl="+ttl+", force="+force);

      HttpsURLConnection.setDefaultHostnameVerifier(new PermissiveHostnameVerifier());

      // Can throw MalformedURLException or IOException
      URL url = new URL("https://"+host+"/LicenseServer/servlet/CommandTryLicense");
      HttpsURLConnection conn = (HttpsURLConnection)url.openConnection();
      //      conn.setHostnameVerifier(new PermissiveHostnameVerifier());
      //      conn.set
      //      System.out.println("URL connection is " + conn);

      conn.setRequestMethod("POST");
      conn.setDoOutput(true);
      conn.connect();
      PrintWriter otherOut = new PrintWriter(conn.getOutputStream());
      otherOut.print("&licenseId="+URLEncoder.encode(licenseId));
      otherOut.print("&executionName="+URLEncoder.encode(executionName));
      otherOut.print("&leaseExpiry="+URLEncoder.encode(""+leaseExpiry));
      otherOut.print("&ttl="+(ttl-1));
      otherOut.print("&force="+(force?"1":"0"));
      //      otherOut.println();
      otherOut.close();

      int respCode = conn.getResponseCode();
      if (respCode != 200)
	throw new IOException("Host returned an error of " + respCode + ": "+conn.getResponseMessage());

      InputStream in = conn.getInputStream();
      OutputStream clientOut = response.getOutputStream();
      while (true)
      {
	byte[] data = new byte[500];
	int bytesRead = in.read(data);
	if (bytesRead == -1)
	  break;

	//	System.out.println("doTryLicenseOtherHost: write bytesRead="+bytesRead);
	clientOut.write(data, 0, bytesRead);

	response.setStatus(HttpServletResponse.SC_OK);
      }
    }


  public void doPost(HttpServletRequest request,
		    HttpServletResponse response)
    throws IOException, ServletException
    {
      doGet(request, response);
    }


  //  protected SSLSocketFactory factory = null;
}
