#!/bin/bash -v

INSTALL_DIR=.

if test "$TOMCAT_HOME" = ""; then
  if test -d /usr/share/tomcat; then
    TOMCAT_HOME=/usr/share/tomcat
    echo "TOMCAT_HOME is not set.  Using $TOMCAT_HOME."
  else
    echo 'TOMCAT_HOME must be set to Tomcat installation directory (TOMCAT_HOME'
    echo 'must contain a webapps subdirectory).'
    exit
  fi
fi

/bin/rm -rf $TOMCAT_HOME/webapps/LicenseServer || exit 1
cp $INSTALL_DIR/LicenseServer.war $TOMCAT_HOME/webapps || exit 1
mkdir -p /etc/licenseserver || exit 1
cp $INSTALL_DIR/properties /etc/licenseserver || exit 1
cp $INSTALL_DIR/eyeball-ls.pkcs12 /etc/licenseserver || exit 1
cp $INSTALL_DIR/eyeball-ls.crtpvk.pem /etc/licenseserver || exit 1
cp $INSTALL_DIR/truststore.jks /etc/licenseserver || exit 1
cp $INSTALL_DIR/eyeball-root.crt.pem /etc/licenseserver || exit 1
cp $INSTALL_DIR/license-file /etc/licenseserver || exit 1

echo "Restart Apache and Tomcat.  Remove the ls2 directory.  Installation complete."
