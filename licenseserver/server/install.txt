Install Tomcat, Apache (www.apache.org), and Apache's Jk module.

Install Apache's modssl (www.modssl.org).  Do a bunch of configuration
to get modssl passing env vars to Tomcat.

Install jsse.  Add the three jsse jars and the tomcat servlet.jar to
jre/lib/ext.

Put LicenseServer.war in $TOMCAT_HOME/webapps.

Create /etc/licenseserver.

Obtain the Eyeball Networks private key/certificate PEM file.  For
development, use the one in client/eyeball-ca-cert.pem.  Copy it to
/etc/licenseserver/eyeball-ca-cert.pem.

Set Apache's http.conf SSLCertificateKeyFile to
/etc/licenseserver/eyeball-ca-cert.pem.

Add the Eyeball certificate to the keystore/truststore:
    keytool -import -file /etc/licenseserver/eyeball-ca-cert.pem -alias eyeball

Copy the sample properties into /etc/licenseserver

Copy the sample license-file into /etc/licenseserver
