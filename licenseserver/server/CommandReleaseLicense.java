import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Enumeration;
import java.security.cert.X509Certificate;
import javax.servlet.ServletContext;
import java.util.NoSuchElementException;
import java.util.Map;
import java.util.Vector;
import java.net.URL;
import javax.servlet.http.HttpUtils;


public class CommandReleaseLicense
extends LicenseCommandHandler
{
  public void doMyGet(HttpServletRequest request,
		    HttpServletResponse response)
    throws IOException, ServletException
    {
      String licenseId = (String)request.getParameter("licenseId");
      String owner = (String)request.getAttribute("SSL_CLIENT_S_DN");
      String executionName = (String)request.getParameter("executionName");

      Vector logMessages = new Vector();
      for (int i = 0; true; i++)
      {
	String nextLog = (String)request.getParameter("logMessage"+i);
	//	System.out.println("doReleaseLicense: Next log message " + i + " = " + nextLog);
	if (nextLog == null)
	  break;

	logMessages.add(nextLog);
      }

      if (licenseId == null ||
	  owner == null ||
	  executionName == null)
      {
	log("("+licenseId+","+executionName+") licenseId, executionName, or client certificate missing.  licenseId=" + licenseId + ", executionName=" + executionName + ", owner=" + owner);
	sendError(response, HttpServletResponse.SC_BAD_REQUEST, "licenseId, executionName, or client certificate missing.  licenseId=" + licenseId + ", executionName=" + executionName + ", owner=" + owner);
	return;
      }

      String requestUrlStr = HttpUtils.getRequestURL(request).toString();
      URL requestUrl;
      requestUrl = new URL(requestUrlStr);

      doReleaseLicense(licenseId, executionName, owner, logMessages, requestUrl, response);
    }


  void doReleaseLicense(String licenseId, String executionName, String owner, Vector logMessages, URL requestUrl, HttpServletResponse response)
    {
      if (!masterMode)
      {
        log("("+licenseId+","+executionName+") I am slave, release rejected.");
        sendError(response, HttpServletResponse.SC_FORBIDDEN, "Master is " + masterHost + ".");
        return;
      }

      Licenses licenses = getLicenses();
      if (licenses == null)
      {
	log("("+licenseId+","+executionName+") Couldn't create a licenses collection.  Release rejected.");
	sendError(response, HttpServletResponse.SC_NOT_FOUND, "There is no license collection, and so no such license.");
	return;
      }

      License license;
      try {
	license = licenses.getLicense(licenseId);
      } catch (NoSuchElementException ex) {
	log("("+licenseId+","+executionName+") License id " + licenseId + " does not exist.  Release rejected.");
	sendError(response, HttpServletResponse.SC_NOT_FOUND, "License id " + licenseId + " does not exist.");
	return;
      }


      //
      // Check if the client is the license owner.

      System.out.println("Found license.  owner is " + license.owner +
			 ", connected client is " + owner);

      if (!authenticate(license.owner, owner))
      {
	log("("+licenseId+","+executionName+") Client is not owner.  Release rejected.");
	sendError(response, HttpServletResponse.SC_FORBIDDEN, "You are not the license owner.");
	return;
      }


      // in slave mode, reject releases

      String localHost = requestUrl.getHost()+(requestUrl.getPort()==-1?"":":"+requestUrl.getPort());

      boolean ret = license.release(executionName);
      if (ret == false)
      {
	log("("+licenseId+","+executionName+") License is leased by " + license.getLesseeName() + ".  Release rejected.");
	sendError(response, HttpServletResponse.SC_CONFLICT, "License is leased by " + license.getLesseeName());
	return;
      }


      Iterator iter = logMessages.iterator();
      while (iter.hasNext())
      {
	String nextLog = (String)iter.next();
	log("("+licenseId+","+executionName+") Log: " + nextLog);
      }

      try {
	sendLicense(license, response.getWriter(), localHost);
      } catch (IOException ex) {
	// Thrown by getWriter(), not sendLicense().
	log("("+licenseId+","+executionName+") Release granted, but client could not be notified.");
	sendError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Couldn't create output stream.  Try again.");
	return;
      }

      log("("+licenseId+","+executionName+") License released.  Total leased time = " +
        license.getTotalLeasedTime()/3600 + " hrs." );
      response.setStatus(HttpServletResponse.SC_OK);
    }


  public void doPost(HttpServletRequest request,
		    HttpServletResponse response)
    throws IOException, ServletException
    {
      doGet(request, response);
    }
}
