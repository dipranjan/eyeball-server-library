import java.util.HashMap;
import java.util.Iterator;


/**
 * This class is not sufficiently synchronized for proper concurrent access.
 * At least, terms should be protected.
 */
class License
{
  /// these are static properties, obtained from the configuration
  public String id = "unknown";
  public String owner = "unknown";
  //String home = "unknown";
  public HashMap terms = new HashMap();
  public long expiry = -1;

  /// these are dynamic properties, nulled initially, and then updated
  /// time of current/last lease
  protected long leaseTime = 0;
  /// expiry of current/last lease
  protected long leaseExpiry = 0;
  /// name of the current/last lessee client
  protected String lesseeName = null;
  /// remember the last client log message here
  protected String lastClientLog = null;
  /// remember total leased time
  protected long totalLeasedTime = 0;


  synchronized boolean lease(String newLesseeName, long newExpiry, boolean force)
  {
    System.out.println("License.lease("+this+"): top: newExpiry="+newExpiry+", system time="+System.currentTimeMillis()/1000);

    if (!force &&
      leaseExpiry > System.currentTimeMillis()/1000 &&
      !lesseeName.equals("") &&
      !lesseeName.equals(newLesseeName))
      return false;

    // account for leased time
    long now = System.currentTimeMillis()/1000;
    if (0 != leaseTime)
      totalLeasedTime += (now - leaseTime);
    leaseTime = now;
    lesseeName = newLesseeName;
    leaseExpiry = newExpiry;
    lastClientLog = null;

    System.out.println("License.lease("+this+"): leaseTime="+leaseTime+", leaseExpiry="+leaseExpiry+", lesseeName="+lesseeName);
    return true;
  }


  synchronized boolean release(String currentLesseeName)
  {
    System.out.println("License.release("+this+"): leaseExpiry="+leaseExpiry+", system time="+System.currentTimeMillis()/1000+", lesseeName="+lesseeName+", currentlessee="+currentLesseeName);
    if (leaseExpiry > System.currentTimeMillis()/1000 &&
      !lesseeName.equals(currentLesseeName))
      return false;

    // account for leased time
    long now = System.currentTimeMillis()/1000;
    if (0 != leaseTime)
      totalLeasedTime += now - leaseTime;
    leaseTime = 0;
    lesseeName = null;
    leaseExpiry = 0;
    // remember last log message!
    // lastClientLog = ...

    return true;
  }


  /// transfer dynamic properties from another license
  public void setDynamicPropsFromLicense(License l2)
  {
    leaseTime = l2.leaseTime;
    leaseExpiry = l2.leaseExpiry;
    lesseeName = l2.lesseeName;
    lastClientLog = l2.lastClientLog;
    totalLeasedTime = l2.totalLeasedTime;
  }


  // check expiry, relase if in past
  public void checkExpiry()
  {
    if (0 != leaseExpiry)
    {
      long now = System.currentTimeMillis()/1000;
      if (leaseExpiry < now)
      {
        release( lesseeName );
      }
    }
  }


  public String getLesseeName()
  {
    checkExpiry();
    return lesseeName;
  }


  public long getLeaseExpiry()
  {
    checkExpiry();
    return leaseExpiry;
  }


  public long getLeaseTime()
  {
    return leaseTime;
  }


  public String getTerms()
  {
    StringBuffer tbuf = new StringBuffer("");

    Iterator iter = terms.keySet().iterator();
    while (iter.hasNext())
    {
      Object termKey = iter.next();
      Object termVal = terms.get(termKey);
      tbuf.append(termKey.toString());
      tbuf.append("=");
      tbuf.append(termVal.toString());
      tbuf.append(" ");
    }

    return tbuf.toString();
  }


  public String getLastClientLog()
  {
    return lastClientLog;
  }


  public void setLastClientLog(String newLog)
  {
    lastClientLog = newLog;
  }


  public long getTotalLeasedTime()
  {
    // compute extra time of the current lease
    long extra = 0;
    if (null != lesseeName)
    {
      long now = System.currentTimeMillis()/1000;
      extra = now - leaseTime;
    }
    return totalLeasedTime + extra;
  }
}
