1) Apache and Tomcat and JSSE

Install Apache with modssl and Tomcat 3.2 using mod_jk and Java JDK1.3
according to www.apache.org, www.modssl.org, and
jakarta.apache.org/tomcat/tomcat-3.3-doc/tomcat-ug.html.  For modssl,
use these directives in httpd.conf:
  SSLEngine on
  SSLCertificateFile    /etc/licenseserver/eyeball-ls.crtpvk.pem
  SSLCertificateKeyFile /etc/licenseserver/eyeball-ls.crtpvk.pem
  SSLCACertificateFile /etc/licenseserver/eyeball-root.crt.pem
  SSLVerifyClient optional

For mod_jk, above the usual installation, add the directives:
  JkExtractSSL On
  JkEnvVar SSL_PROTOCOL noneprovided
  JkEnvVar SSL_CIPHER noneprovided
  JkEnvVar SERVER_SOFTWARE noneprovided
  JkEnvVar SSL_CLIENT_S_DN noneprovided
  JkEnvVar SSL_SERVER_S_DN noneprovided
  SSLOptions +StdEnvVars

Install JSSE (java.sun.com/products/jsse) with Tomcat.  Because we're
using JDK1.3, we'll need JSSE 1.0.2.

At a minimum, you must be able to access all of the following pages:
	http://localhost/
	https://localhost/
	http://localhost:8081/tomcat-examples/servlets/
	http://localhost:8081/tomcat-examples/servlet/HelloWorldExample
	http://localhost/tomcat-examples/servlet/HelloWorldExample
	https://localhost/tomcat-examples/servlet/HelloWorldExample


2) Run install.sh

Untar ls2.tar.gz and run install.sh as root.  It will create
/etc/licenseserver and install the LicenseServer.war in Tomcat.


3) Create a license-file

Modify /etc/licenseserver/license-file to contain your licenses.  The
file format is documented in that file.


4) Restart Tomcat and Apache

This will extract the LicenseServer.war software and make it ready to
run and install the Apache SSL keys.


5) Test it

You should be able to access the following pages:
	https://localhost/tomcat-examples/servlet/HelloWorldExample
	https://localhost/LicenseServer/servlet/ServerStatus
	https://localhost/LicenseServer/servlet/CommandLicenseReport

The last of these pages should list the licenses you installed.  It's
a useful page for monitoring and debugging.


6) Watch it

Logs are in /var/log/tomcat/servlet.log (debug is in stdout.log).
