/* Copyright 1998 by the Massachusetts Institute of Technology.
 *
 * Permission to use, copy, modify, and distribute this
 * software and its documentation for any purpose and without
 * fee is hereby granted, provided that the above copyright
 * notice appear in all copies and that both that copyright
 * notice and this permission notice appear in supporting
 * documentation, and that the name of M.I.T. not be used in
 * advertising or publicity pertaining to distribution of the
 * software without specific, written prior permission.
 * M.I.T. makes no representations about the suitability of
 * this software for any purpose.  It is provided "as is"
 * without express or implied warranty.
 */

static const char rcsid[] = "$Id: ares_destroy.c,v 1.1 2005/10/12 00:17:10 larsb Exp $";

#include <stdlib.h>
#include "ares.h"
#include "ares_private.h"

void ares_destroy(ares_channel channel)
{
  int i;
  struct query *query;

  for (i = 0; i < channel->nservers; i++)
    ares__close_sockets(&channel->servers[i]);
  if (channel->servers!=NULL)
    free(channel->servers);
  for (i = 0; i < channel->ndomains; i++)
    {
      if (channel->domains[i]!=NULL)
	free(channel->domains[i]);
    }
  if (channel->domains!=NULL)
    free(channel->domains);
  
  if (channel->sortlist!=NULL)
    free(channel->sortlist);
  if (channel->lookups!=NULL)
    free(channel->lookups);
  
  while (channel->queries)
    {
      query = channel->queries;
      channel->queries = query->next;
      query->callback(query->arg, ARES_EDESTRUCTION, NULL, 0);
      if (query->tcpbuf!=NULL)
	free(query->tcpbuf);
      if (query->skip_server!=NULL)
	free(query->skip_server);
      if (query!=NULL)
	free(query);
    }
  if (channel!=NULL)
    free(channel);
}
