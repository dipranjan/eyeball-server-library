// ---------------------------------------------------------------------------
// File:       ecpasswd.cc
//
// Copyright:  Eyeball Networks Inc. 1999-2003
//
// Author:     Jozsef Vass
//
// Purpose:    Database password management utility
//
// Change log: 2003/04/04 - Added command line input of user and password
//             2001/04/23 - Created
// ---------------------------------------------------------------------------

#include "debug.h"
#include "log.h"
#include "options.h"
#include "util.h"
#include "simple_crypt.h"

#include <map>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

using std::map;
using std::string;

typedef map<string, string> StringMap;

#define FILE_ARG argv[0]
#define EXPECTED_NPOS1		0
#define EXPECTED_NPOS2		1

static const char * const optv[] =
{
	"h|help",
	"c|create",
	"d|display",
	"u:user",
	"p:password",
	NULL
};

static void usage(const char *prog_name)
{
	cerr << "Usage: " << prog_name << " [hcup] passwordfile" << endl
		<< "       " << prog_name << " -d[hup]" << endl
		<< endl
		<< "  -c, --create             create a new file" << endl
		<< "  -d, --display            don't update file; display result to stdout" << endl
		<< "  -u, --user user          user to add" << endl
		<< "  -p, --password password  password" << endl
		<< "  -h, --help               display this help and exit" << endl;

	exit(1);
}

static void brief_usage(const char *prog_name, const char *reason)
{
	if (reason)
		cerr << prog_name << ": " << reason << endl;

	cerr << "Try `" << prog_name << " --help' for more information." << endl;

	exit(1);
}

StringMap ReadPasswdFile(FILE *fp,const string& ProgName,const string& FileName)
{
	char *cBuf = new char[200];
	StringMap passwd;
	string sUser;
	string sPasswd;

	while( fgets(cBuf,200,fp) != NULL )
	{
		char *pSep = strchr(cBuf,':');
		if( pSep == NULL )
		{
			fprintf(stderr,"%s: invalid entry in file %s: %s",ProgName.c_str(),FileName.c_str(),cBuf);
			continue;
		}
		sUser.assign(cBuf,pSep-cBuf);
		sPasswd.assign(pSep+1);

//		DB(sUser);
//		DB(sPasswd);

		passwd[sUser]=sPasswd;
	}

	return passwd;
}

int WritePasswdFile(FILE *fp,StringMap sm,const string& ProgName,const string &FileName)
{
	for( StringMap::iterator sm_iter=sm.begin();sm_iter!=sm.end();sm_iter++ )
	{
//		DB((sm_iter->first)<<":"<<(sm_iter->second));
		if( fprintf(fp,"%s:%s",(sm_iter->first).c_str(),(sm_iter->second).c_str()) == EOF )
		{
			fprintf(stderr,"%s: cannot write file %s\n",ProgName.c_str(),FileName.c_str());
			return -1;
		}
	}

	return 0;
}

string ReadUser(const string& sProgName)
{
	char *cBuf = new char[150];
	string sUserName;

	printf("Please enter MySQL database user name for Eyeball server components: ");
	scanf("%s",cBuf);

	if( strchr(cBuf,':')!=NULL )
	{
		fprintf(stderr,"%s: username contains illegal character \':\'\n", sProgName.c_str());
		exit(-1);
	}
	sUserName.assign(cBuf);

	return sUserName;
}

string ReadPassword(const string& sProgName, const string& sUserName)
{
	char *cBuf = new char[150];
	string sPassword1;
	string sPassword2;

	string sPrompt = "Please enter password for database user " + sUserName + ": ";
	cBuf = getpass(sPrompt.c_str());
	sPassword1.assign(cBuf);

	string sPrompt2 = "Please re-type password for database user " + sUserName + ": ";
	cBuf = getpass(sPrompt2.c_str());
	sPassword2.assign(cBuf);

	if( sPassword1.empty() )
	{
		fprintf(stderr,"%s: empty password not allowed \n", sProgName.c_str());
		exit(-1);
	}

	if( sPassword1 != sPassword2 )
	{
		fprintf(stderr,"%s: password verification error\n", sProgName.c_str());
		exit(-1);
	}

	return sPassword1;
}

int main(int argc, const char *argv[])
{
	int optchar;
	const char *optarg;
	int npos(0);

	Options opts(*argv, optv);
	opts.ctrls(Options::PARSE_POS);

	bool bDisplayResult(false);
	bool bCreateNewFile(false);
	string sUserName;
	string sPassword;

	OptArgvIter iter(--argc, ++argv);

	while ((optchar = opts(iter, optarg)))
	{
		switch (optchar)
		{
			case 'h':
				usage(opts.name());
				break;

			case 'c':
				bCreateNewFile = true;
				break;

			case 'd':
				bDisplayResult = true;
				break;

			case 'u':
				if( !optarg )
					brief_usage(opts.name(), NULL);
				sUserName = optarg;
				break;

			case 'p':
				if( !optarg )
					brief_usage(opts.name(), NULL);
				sPassword = optarg;
				break;

			case Options::POSITIONAL:
				// Push positional arguments to the front
				argv[npos++] = optarg;
				break;

			case Options::BADCHAR:		// bad option ("-%c", *optarg)
			case Options::BADKWD:		// bad long-option ("--%s", optarg)
			case Options::AMBIGUOUS:	// ambiguous long-option ("--%s", optarg)
			default:
				brief_usage(opts.name(), NULL);
				break;
		}
	}

	string sPasswordFile;

	switch(npos)
	{
		case EXPECTED_NPOS1:
			if( !bDisplayResult )
				brief_usage(opts.name(), "Incorrect number of arguments");
			break;

		case EXPECTED_NPOS2:
			sPasswordFile = FILE_ARG;
			break;

		default:
			brief_usage(opts.name(), "Incorrect number of arguments");
			break;
	}
	
	// make sure that the password file exist
	if( !bDisplayResult && !bCreateNewFile && !file_exist(sPasswordFile.c_str()) )
	{
		fprintf(stderr,"%s: cannot modify file %s; use '-c' to create it\n",opts.name(),sPasswordFile.c_str());
		exit(-1);
	}

	string sCryptPassword;

	if( sUserName.empty() )
		sUserName = ReadUser(opts.name());

	if( sPassword.empty() )
		sPassword = ReadPassword(opts.name(), sUserName);

	sCryptPassword = Crypt::simple_crypt(sPassword);

	if( bDisplayResult )
	{
		fprintf(stdout,"%s:%s\n", sUserName.c_str(), sCryptPassword.c_str());
		return 0;
	}

	FILE *fp(NULL);

	if( bCreateNewFile )
	{
		if( (fp=fopen(sPasswordFile.c_str(),"w"))==NULL )
		{
			fprintf(stderr,"%s: cannot open file %s\n",opts.name(),sPasswordFile.c_str());
			exit(-1);
		}

		StringMap sm;
		sm[sUserName] = sCryptPassword+'\n';

		if( WritePasswdFile(fp,sm,opts.name(),sPasswordFile) == -1 )
		{
			fclose(fp);
			exit(-1);
		}
		fclose(fp);

		fprintf(stdout,"MySQL user %s added to password authentication file.\n",sUserName.c_str());

		return 0;
	}

	if( (fp=fopen(sPasswordFile.c_str(),"r"))==NULL )
	{
		fprintf(stderr,"%s: cannot open file %s\n",opts.name(),sPasswordFile.c_str());
		exit(-1);
	}
	StringMap sm = ReadPasswdFile(fp,opts.name(),sPasswordFile);

	fclose(fp);

	bool bUpdate(true);

	if( sm.find(sUserName) == sm.end() )
	{
		bUpdate = false;
	}

	sm[sUserName] = sCryptPassword+'\n';

	if( (fp=fopen(sPasswordFile.c_str(),"w"))==NULL )
	{
		fprintf(stderr,"%s: cannot open file %s\n",opts.name(),sPasswordFile.c_str());
		exit(-1);
	}

	if( WritePasswdFile(fp,sm,opts.name(),sPasswordFile) == -1 )
	{
		fclose(fp);
		exit(-1);
	}
	fclose(fp);

	if( bUpdate )
	{
		fprintf(stdout,"Updated password for MySQL user %s.\n",sUserName.c_str());
	}
	else
	{
		fprintf(stdout,"MySQL user %s added to password authentication file.\n",sUserName.c_str());
	}

	return 0;
}
