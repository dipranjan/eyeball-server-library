#include "Base.h"
#include "DnsInterface.h"
#include "DnsResult.h"

extern "C"
{
	#include "ares.h"
	#include "ares_dns.h"
}


using std::string;


DnsInterface::DnsInterface()
{
  int status = 0;
  if ((status = ares_init(&m_Channel)) != ARES_SUCCESS)
    {
      DB_ASSERT(0);
    }
}

DnsInterface::~DnsInterface()
{
  ares_destroy(m_Channel);
}

void DnsInterface::GetTimeout(timeval* tvp)
{
  struct timeval tv;
  tvp = ares_timeout(m_Channel, NULL, &tv);
}

bool DnsInterface::isSupported(const string& sService)
{
  DB("Is supported: " << sService.c_str());
  return true;
}

bool DnsInterface::buildFdSet(FdSet& fdset)
{
  int size = ares_fds(m_Channel, &fdset.read, &fdset.write);
  if (size > fdset.size)
    {
      fdset.size = size;
    }

  return (size == 0) ? false : true;
}

void DnsInterface::process(FdSet& fdset)
{
  ares_process(m_Channel, &fdset.read, &fdset.write);
}


DnsResult* DnsInterface::lookup(
	const string& sDomain, 
	const string& sService,
	const string& sProtocol,
	DnsHandler* handler)
{
  DnsResult* result = new DnsResult(*this, handler);
  result->lookup(sDomain, sService, sProtocol);
  return result;
}


DnsHandler::~DnsHandler()
{
}


/*
int main()
{
//DnsInterface ob;
DnsResolve ob;

              int iResult = 0;
              HRESULT hr = p->Lookup(pc->m_sDomainName, "sip", "udp", &iResult);
              if (SUCCEEDED(hr) && iResult > 0)
              {
                      int i = 0;
                      while (i < iResult)
                      {
                              AddressPortPair addr;
                              p->GetAddress(i, (DWORD*)&(addr.first));
                              p->GetPort(i, &(addr.second));
			      printf("addr: %s\n",LookupHostName(htonl(addr.first)));	
			      printf("port: %d\n",addr.second);	

                 //             pc->m_ProxyServrMap[i] = addr;
                              i++;
                      }
                   //   pc->m_iServerIndex = -1;
                   //   pc->m_dwProxyAddr = pc->m_ProxyServrMap[0].first;
                   //   pc->m_wProxyPort = pc->m_ProxyServrMap[0].second;

		}


return 0;

}

*/

