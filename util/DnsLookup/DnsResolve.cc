
#include "ENetwork.h"

#include "Base.h"

#include "Timer.h"
#include "DnsResult.h"
#include "DnsInterface.h"
//#include "Thread.h"
#include "DnsResolve.h"
#include "DebugLog.h"

using std::string;
using std::map;
using std::list;



#define LOOKUP_TIMEOUT 10000 // 10 seconds

LOG_FILE(LOG_FILENAME_SRV);



DnsResolve::DnsResolve():
	m_pDnsInterface(NULL),
	m_resultMap()
{
	m_pDnsInterface = new DnsInterface;
	LOG_ENABLE(GetLogEnabled(LOG_SRV_LOOKUP));
	LOG_OPEN();
}

DnsResolve::~DnsResolve()
{
	LOG_CLOSE();
	SAFE_DELETE(m_pDnsInterface);

	list<DnsResult*>::iterator iter = m_FailedResults.begin();
	for (; iter != m_FailedResults.end(); iter = m_FailedResults.begin()) {
		delete (*iter);
		m_FailedResults.erase(iter);
	}
}

bool DnsResolve::GetResult(int iIndex, AddressPortPair& addr)
{
	if (iIndex >= (int)m_resultMap.size())
		return false;

	addr = m_resultMap[iIndex];
	return true;
}


int DnsResolve::Lookup(
	const string& sDomain,
	const string& sService,
	const string& sProtocol) 
{
  m_resultMap.clear();

  DnsResult* result = m_pDnsInterface->lookup(sDomain, sService, sProtocol, this);

  int iSelectErrors = 0;

  while (TRUE)
    {
      FdSet fdset;
      if (!m_pDnsInterface->buildFdSet(fdset))
	{
	  DB("buildFdSet exception.");
	  break;
	}

      struct timeval tv;

      tv.tv_sec = 30;
      tv.tv_usec = 0;

      //      m_pDnsInterface->GetTimeout(&tv);
      
      int ret = fdset.select(tv);
      if (ret>0)
        {
	  m_pDnsInterface->process(fdset);
          iSelectErrors = 0;
        }
      else
	{
	  //	  result->destroy();
	  //	  delete result;
          int myerrno = errno;
          DB("DnsResolve lookup select returned " << ret << " " << myerrno);
          iSelectErrors++;
          if (ret == 0) {
            iSelectErrors = 10;
          }
          if (iSelectErrors >= 10) {
            break;
          }
	}
      
    }

  int iSize = m_resultMap.size();
  if (iSize <= 1)
    {
      if (iSelectErrors >= 10) {
        m_FailedResults.push_back(result);
      }
      else {
        delete result;
      }
      return iSize;
    }

  // randamize the map, all addresses have identical priority and weight
  ResultMap tempMap = m_resultMap;
  m_resultMap.clear();

  //srand(timeGetTime());
  srand(time(NULL));
  for (int i = 0; i < iSize; i++)
    {
      int iRandom = rand() % tempMap.size();
      ResultMap::iterator iter = tempMap.begin();
      while (iRandom > 0)
	{
	  iter++;
	  iRandom--;
	}

      m_resultMap[i] = iter->second;
      tempMap.erase(iter);
    }

  delete result;
  return iSize;
}


void DnsResolve::Handle(DnsResult* result)
{
	DB("Handling result:");
	while (result->available() == DnsResult::Available)
	{
		Tuple t = result->next();
		WORD wPort = ntohs(t.m_anonv4.sin_port);

		DB_AND_LOG("SRV Result ip:  " << LookupHostName(t.m_anonv4.sin_addr.s_addr).c_str());
		DB_AND_LOG("SRV Result port: " << wPort);

		if (wPort == 65535 || wPort == 0)
			continue;

		// all in host byte order
		AddressPortPair addr;
		addr.first = ntohl(t.m_anonv4.sin_addr.s_addr);
		addr.second = wPort;
		int iSize = m_resultMap.size();
		m_resultMap[iSize] = addr;
	}
}

/*
int main(int argc, char * argv[])
{
//DnsInterface ob;
DnsResolve *p = new DnsResolve;
                                                                                                                             
              int iResult = -1;
	      if(argc != 4)
		return -1;
	
              iResult = p->Lookup(argv[1], argv[2], argv[3]);
	      printf("iResult=%d\n",iResult);

              if ( iResult > 0)
              {
                      int i = 0;
                      while (i < iResult)
                      {
                              AddressPortPair addr;
				
 			      if(!p->GetResult(i, addr)  )
				printf("fail\n");
			      else
			      {
				  struct in_addr ia;
			          ia.s_addr = htonl(addr.first);
				  printf("Addr: %s\n",inet_ntoa(ia));
//                                  printf("addr: %s\n",LookupHostName(htonl(addr.first)));
				  printf("Port: %d\n",addr.second);
			      }

                                                                                                                             
                              i++;
                      }
                   //   pc->m_iServerIndex = -1;
                   //   pc->m_dwProxyAddr = pc->m_ProxyServrMap[0].first;
                   //   pc->m_wProxyPort = pc->m_ProxyServrMap[0].second;
                                                                                                                             
                }


delete p;

return 0;
                                                                                                                             
}
*/

