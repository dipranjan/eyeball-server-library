
#ifndef DNS_RESOLVE_H
#define DNS_RESOLVE_H

#include "DnsInterface.h"
#include <utility>


typedef std::pair<u_int32_t, u_int16_t> AddressPortPair;



class DnsResolve : public DnsHandler
{
public:
	DnsResolve();
	virtual ~DnsResolve();

	int Lookup(
		const std::string& sDomain,
		const std::string& sService,
		const std::string& sProtocol);

	virtual void Handle(DnsResult* result);

	bool GetResult(int iIndex, AddressPortPair& addr);

private:
	DnsInterface* m_pDnsInterface;
	typedef std::map<int, AddressPortPair> ResultMap;
	ResultMap m_resultMap;
	std::list<DnsResult*> m_FailedResults;
};

#endif
