#include "Base.h"
#include "DnsInterface.h"
#include "DnsResult.h"


extern "C"
{	
	#include "ares.h"
	#include "ares_dns.h"
}

#include <arpa/nameser.h>

using std::string;
using std::list;
using std::vector;
using std::map;


DnsResult::DnsResult(DnsInterface& interfaceObj, DnsHandler* handler) :
        m_Interface(interfaceObj),
        m_pHandler(handler),
        m_iSRVCount(0),
        m_sTarget(),
        m_sLookupTarget(),
        m_sLookupHeader(),
        m_iTransport(TRANSPORT_UNKNOWN),
        m_iPort(-1),
        m_Type(Pending),
        m_sPassHostFromAAAAtoA(),
        m_lResults(),
        m_PreferredNAPTR(),
        m_iCumulativeWeight(0),
        m_vSRVResults(),
#ifdef USE_IPV6
	m_lAAAARecords()
#endif
	m_mARecords()
{
}

DnsResult::~DnsResult()
{
  //assert(m_Type != Pending);
}

void DnsResult::destroy()
{
  assert(this);   
  if (m_Type == Pending)
    {
      m_Type = Destroyed;
    }
  else
    {
      delete this;
    }
}

DnsResult::Type DnsResult::available()
{
  assert(m_Type != Destroyed);
  if (m_Type == Available)
    {
      if (!m_lResults.empty())
	{
	  return Available;
	}
      else
	{
	  primeResults();
	  return available(); // recurse
	}
    }
  else
    {
      return m_Type;
    }
}

Tuple DnsResult::next() 
{
  assert(available() == Available);
  Tuple next = m_lResults.front();
  m_lResults.pop_front();
  return next;
}

void DnsResult::lookup(
	const string& sTarget,
	const string& sService,
	const string& sProtocol)
{
  m_sTarget = sTarget;

  m_sLookupHeader = "_";
  m_sLookupHeader += sService;
  m_sLookupHeader += "._";
  m_sLookupHeader += sProtocol;

  m_sLookupTarget = m_sLookupHeader;
  m_sLookupTarget += ".";
  m_sLookupTarget += sTarget;

  DB("DnsResult::lookup " << m_sLookupTarget.c_str());

  if (sProtocol == "udp")
    m_iTransport = TRANSPORT_UDP;
  else
    m_iTransport = TRANSPORT_TCP;

#if 0
  m_iPort = getDefaultPort(m_iTransport, 0);
  lookupAAAARecords(m_sTarget); // for current target and port                   
#else
  //lookupNAPTR(); // for current target
  lookupSRV(m_sLookupTarget);
  m_iSRVCount = 1;
#endif
}


void DnsResult::lookupAAAARecords(const string& target)
{
#if defined(USE_IPV6)
	DB("Doing host (AAAA) lookup: " << target.c_str());
	m_sPassHostFromAAAAtoA = target; // hackage
	ares_query(m_Interface.m_Channel, target.c_str(), C_IN, T_AAAA, DnsResult::aresAAAACallback, this); 
#else // !USE_IPV6
	lookupARecords(target);
#endif
}

void DnsResult::lookupARecords(const string& target)
{
	DB("Doing Host (A) lookup: " << target.c_str());
	ares_gethostbyname(m_Interface.m_Channel, target.c_str(), AF_INET, DnsResult::aresHostCallback, this);
}

void DnsResult::lookupNAPTR()
{
	DB ("Doing NAPTR lookup: " << m_sTarget.c_str());
	ares_query(m_Interface.m_Channel, m_sTarget.c_str(), C_IN, T_NAPTR, DnsResult::aresNAPTRCallback, this); 
}

void DnsResult::lookupSRV(const string& target)
{
	DB("Doing SRV lookup: " << target.c_str());
	ares_query(m_Interface.m_Channel, target.c_str(), C_IN, T_SRV, DnsResult::aresSRVCallback, this); 
}

void DnsResult::aresHostCallback(void *arg, int status, struct hostent* result)
{
	DnsResult *thisp = reinterpret_cast<DnsResult*>(arg);
	DB ("Received A result for: " << thisp->m_sTarget.c_str());
	thisp->processHost(status, result);
}

void DnsResult::aresNAPTRCallback(void *arg, int status, unsigned char *abuf, int alen)
{
	DnsResult *thisp = reinterpret_cast<DnsResult*>(arg);
	DB("Received NAPTR result for: " << thisp->m_sTarget.c_str());
	thisp->processNAPTR(status, abuf, alen);
}


void DnsResult::aresSRVCallback(void *arg, int status, unsigned char *abuf, int alen)
{
	DnsResult *thisp = reinterpret_cast<DnsResult*>(arg);
	DB("Received SRV result for: " << thisp->m_sTarget.c_str());
	thisp->processSRV(status, abuf, alen);
}

void DnsResult::aresAAAACallback(void *arg, int status, unsigned char *abuf, int alen)
{
	DnsResult *thisp = reinterpret_cast<DnsResult*>(arg);
	DB("Received AAAA result for: " << thisp->m_sTarget.c_str());
	thisp->processAAAA(status, abuf, alen);
}

void DnsResult::processNAPTR(int status, unsigned char* abuf, int alen)
{
   DB("DnsResult::processNAPTR() " << status);

   // This function assumes that the NAPTR query that caused this
   // callback is the ONLY outstanding query that might cause
   // a callback into this object
   if (m_Type == Destroyed)
   {
      destroy();
      return;
   }

   if (status == ARES_SUCCESS)
   {
      const unsigned char *aptr = abuf + HFIXEDSZ;
      int qdcount = DNS_HEADER_QDCOUNT(abuf);    /* question count */
      for (int i = 0; i < qdcount && aptr; i++)
      {
         aptr = skipDNSQuestion(aptr, abuf, alen);
      }
      
      int ancount = DNS_HEADER_ANCOUNT(abuf);    /* answer record count */
      for (int i = 0; i < ancount && aptr; i++)
      {
         NAPTR naptr;
         aptr = parseNAPTR(aptr, abuf, alen, naptr);
         
         if (aptr)
         {
            ////DB ("Adding NAPTR record: " << naptr);
			if (m_Interface.isSupported(naptr.service) && naptr < m_PreferredNAPTR)
            {
               m_PreferredNAPTR = naptr;
               ////DB (<< "Picked preferred: " << m_PreferredNAPTR);
            }
         }
      }

      // This means that dns / NAPTR is misconfigured for this client 
      if (m_PreferredNAPTR.key.empty())
      {
         //DB ("No NAPTR records that are supported by this client");
         m_Type = Finished;
         m_pHandler->Handle(this);
         return;
      }

      int nscount = DNS_HEADER_NSCOUNT(abuf);    /* name server record count */
      ////DB (<< "Found " << nscount << " nameserver records");
      for (int i = 0; i < nscount && aptr; i++)
      {
         // this will ignore NS records
         aptr = parseAdditional(aptr, abuf, alen);
      }

      int arcount = DNS_HEADER_ARCOUNT(abuf);    /* additional record count */
      //DB ("Found " << arcount << " additional records");
      for (int i = 0; i < arcount && aptr; i++)
      {
         // this will store any related SRV and A records
         // don't store SRV records other than the one in the selected NAPTR
         aptr = parseAdditional(aptr, abuf, alen);
      }

      if (ancount == 0) // didn't find any NAPTR records
      {
         //DB ("There are no NAPTR records so do an SRV lookup instead");
         goto NAPTRFail; // same as if no NAPTR records
      }
      else if (m_vSRVResults.empty())
      {
         // didn't find any SRV records in Additional, so do another query

         assert(m_iSRVCount == 0);
         assert(!m_PreferredNAPTR.replacement.empty());

         ////DB (<< "No SRV record for " << m_PreferredNAPTR.replacement << " in additional section");
         m_Type = Pending;
         m_iSRVCount++;
         lookupSRV(m_PreferredNAPTR.replacement);
      }
      else
      {
         // This will fill in m_lResults based on the DNS result
         std::sort(m_vSRVResults.begin(),m_vSRVResults.end()); // !jf! uggh
         primeResults();
      }
   }
   else
   {
      {
         char* errmem=0;
         DB ("NAPTR lookup failed: " << ares_strerror(status, &errmem));
         ares_free_errmem(errmem);
      }
      
      // This will result in no NAPTR results. In this case, send out SRV
      // queries for each supported protocol
     NAPTRFail:
     lookupSRV(m_sLookupTarget);
     m_iSRVCount++;

      DB ("Doing SRV query " << m_iSRVCount << " for " << m_sTarget);
   }
}

void DnsResult::processSRV(int status, unsigned char* abuf, int alen)
{
   assert(m_iSRVCount>=0);
   m_iSRVCount--;
   ////DB (<< "DnsResult::processSRV() " << m_iSRVCount << " status=" << status);

   // There could be multiple SRV queries outstanding, but there will be no
   // other DNS queries outstanding that might cause a callback into this
   // object.
   if (m_Type == Destroyed && m_iSRVCount == 0)
   {
      destroy();
      return;
   }

   if (status == ARES_SUCCESS)
   {
      const unsigned char *aptr = abuf + HFIXEDSZ;
      int qdcount = DNS_HEADER_QDCOUNT(abuf);    /* question count */
      for (int i = 0; i < qdcount && aptr; i++)
      {
         aptr = skipDNSQuestion(aptr, abuf, alen);
      }
      
      int ancount = DNS_HEADER_ANCOUNT(abuf);    /* answer record count */
      for (int i = 0; i < ancount && aptr; i++)
      {
         SRV srv;
         aptr = parseSRV(aptr, abuf, alen, srv);
         
         if (aptr)
         {
			if (srv.key.find(m_sLookupHeader) == 0)
			{
				srv.transport = m_iTransport;
			}
            else
            {
               ////DB (<< "Skipping SRV " << srv.key);
               continue;
            }

            ////DB (<< "Adding SRV record (no NAPTR): " << srv);
            m_vSRVResults.push_back(srv);
         }
      }

      int nscount = DNS_HEADER_NSCOUNT(abuf);    /* name server record count */
      ////DB (<< "Found " << nscount << " nameserver records");
      for (int i = 0; i < nscount && aptr; i++)
      {
         // this will ignore NS records
         aptr = parseAdditional(aptr, abuf, alen);
      }
      
      int arcount = DNS_HEADER_ARCOUNT(abuf);    /* additional record count */
      ////DB (<< "Found " << arcount << " additional records");
      for (int i = 0; i < arcount && aptr; i++)
      {
         // this will store any related A records
         aptr = parseAdditional(aptr, abuf, alen);
      }
   }
   else
   {
      char* errmem=0;
      //DB("SRV lookup failed: " << ares_strerror(status, &errmem));
      ares_free_errmem(errmem);
   }

   // no outstanding queries 
   if (m_iSRVCount == 0) 
   {
      if (m_vSRVResults.empty())
      {
         ////DB (<< "No SRV records for " << m_sTarget << ". Trying A records");
         //Aug. 2, 2006 Removed by Zion
         //lookupAAAARecords(m_sTarget);
      }
      else
      {
         std::sort(m_vSRVResults.begin(),m_vSRVResults.end()); // !jf! uggh
         primeResults();
      }
   }
}

void DnsResult::processAAAA(int status, unsigned char* abuf, int alen)
{
#ifdef USE_IPV6
   ////DB (<< "DnsResult::processAAAA() " << status);
   // This function assumes that the AAAA query that caused this callback
   // is the _only_ outstanding DNS query that might result in a
   // callback into this function
   if ( m_Type == Destroyed )
   {
      destroy();
      return;
   }
   if (status == ARES_SUCCESS)
   {
     const unsigned char *aptr = abuf + HFIXEDSZ;

     int qdcount = DNS_HEADER_QDCOUNT(abuf); /*question count*/
     for (int i=0; i < qdcount && aptr; i++)
     {
       aptr = skipDNSQuestion(aptr, abuf, alen);
     }

     int ancount = DNS_HEADER_ANCOUNT(abuf); /*answer count*/
     for (int i=0; i < ancount && aptr ; i++)
     {
       struct in6_addr aaaa;
       aptr = parseAAAA(aptr,abuf,alen,&aaaa);
       if (aptr)
       {
         Tuple tuple(aaaa,m_iPort,m_iTransport);
         ////DB (<< "Adding " << tuple << " to result set");

         // !jf! Should this be going directly into m_lResults or into m_mARecords
         // !jf! Check for duplicates? 
         m_lResults.push_back(tuple);
       }
     }
   }
   else
   {
      char* errmem=0;
      ////DB (<< "Failed async dns query: " << ares_strerror(status, &errmem));
      ares_free_errmem(errmem);
   }
   lookupARecords(m_sPassHostFromAAAAtoA);
#else
	assert(0);
#endif
}

void DnsResult::processHost(int status, struct hostent* result)
{
   ////DB (<< "DnsResult::processHost() " << status);
   
   // This function assumes that the A query that caused this callback
   // is the _only_ outstanding DNS query that might result in a
   // callback into this function
   if ( m_Type == Destroyed )
   {
      destroy();
      return;
   }

   if (status == ARES_SUCCESS)
   {
//p      DB("DNS A lookup canonical name: " << result->h_name);
      for (char** pptr = result->h_addr_list; *pptr != 0; pptr++)
      {
         in_addr addr;
         addr.s_addr = *((u_int32_t*)(*pptr));
         Tuple tuple(addr, m_iPort, m_iTransport);
         ////DB (<< "Adding " << tuple << " to result set");
         // !jf! Should this be going directly into m_lResults or into m_mARecords
         // !jf! Check for duplicates? 
         m_lResults.push_back(tuple);
      }
   }
   else
   {
      char* errmem=0;
      ////DB (<< "Failed async A query: " << ares_strerror(status, &errmem));
      ares_free_errmem(errmem);
   }

   if (m_iSRVCount == 0)
   {
      bool changed = (m_Type == Pending);
      if (m_lResults.empty())
      {
         m_Type = Finished;
      }
      else 
      {
         m_Type = Available;
      }
      if (changed) m_pHandler->Handle(this);
   }
}

void DnsResult::primeResults()
{
   //assert(m_Type != Pending);
   //assert(m_Type != Finished);
   assert(m_lResults.empty());

   if (!m_vSRVResults.empty())
   {
      SRV next = retrieveSRV();
      ////DB (<< "Primed with SRV=" << next);
      if ( m_mARecords.count(next.target) 
#ifdef USE_IPV6 
           + mAAAARecords.count(next.target)
#endif
         )
      {
#ifdef USE_IPV6 
         std::list<struct in6_addr>& aaaarecs = mAAAARecords[next.target];
         for (std::list<struct in6_addr>::const_iterator i=aaaarecs.begin();
	         i!=aaaarecs.end(); i++)
         {
            Tuple tuple(*i,next.port,next.transport);
            //DB (<< "Adding " << tuple << " to result set");
            m_lResults.push_back(tuple);
         }
#endif
         std::list<struct in_addr>& arecs = m_mARecords[next.target];
         for (std::list<struct in_addr>::const_iterator i=arecs.begin(); i!=arecs.end(); i++)
         {
            Tuple tuple(*i, next.port, next.transport);
            //DB (<< "Adding " << tuple << " to result set");
            m_lResults.push_back(tuple);
         }
#if !defined(WIN32) && !defined(__SUNPRO_CC) && !defined(__INTEL_COMPILER)
         //DB (<< "Try: " << Inserter(m_lResults));
#endif


         bool changed = (m_Type == Pending);
         m_Type = Available;
         if (changed) m_pHandler->Handle(this);

         // recurse if there are no results. This could happen if the DNS SRV
         // target record didn't have any A/AAAA records. This will terminate if we
         // run out of SRV results. 
         if (m_lResults.empty())
         {
            primeResults();
         }
      }
      else
      {
         // !jf! not going to test this for now
         // this occurs when the A records were not provided in the Additional
         // Records of the DNS result
         // we will need to store the SRV record that is being looked up so we
         // can populate the resulting Tuples 
         m_Type = Pending;
         m_iPort = next.port;
         m_iTransport = next.transport;
         //DB (<< "No A or AAAA record for " << next.target << " in additional records");
         lookupAAAARecords(next.target);
         // don't call primeResults since we need to wait for the response to
         // AAAA/A query first
      }
   }
   else
   {
      bool changed = (m_Type == Pending);
      m_Type = Finished;
      if (changed) m_pHandler->Handle(this);
   }

   // Either we are finished or there are results primed
   assert(m_Type == Finished || 
          m_Type == Pending || 
          (m_Type == Available && !m_lResults.empty()));
}

// implement the selection algorithm from rfc2782 (SRV records)
DnsResult::SRV DnsResult::retrieveSRV()
{
   assert(!m_vSRVResults.empty());

   const SRV& srv = *m_vSRVResults.begin();
   if (srv.cumulativeWeight == 0)
   {
      int priority = srv.priority;
   
      m_iCumulativeWeight=0;
      for (std::vector<SRV>::iterator i=m_vSRVResults.begin(); 
           i!=m_vSRVResults.end() && i->priority == priority; i++)
      {
         m_iCumulativeWeight += i->weight;
         i->cumulativeWeight = m_iCumulativeWeight;
      }
   }
   
   int selected = rand() % (m_iCumulativeWeight+1);

   //DB (<< "cumulative weight = " << m_iCumulativeWeight << " selected=" << selected);

   std::vector<SRV>::iterator i;
   for (i=m_vSRVResults.begin(); i!=m_vSRVResults.end(); i++)
   {
      if (i->cumulativeWeight >= selected)
      {
         break;
      }
   }
   
   if (i == m_vSRVResults.end())
   {
      //InfoLog (<< "SRV Results problem selected=" << selected << " cum=" << m_iCumulativeWeight);
#if !defined(WIN32) && !defined(__SUNPRO_CC) && !defined(__INTEL_COMPILER)
      //InfoLog (<< "SRV: " << Inserter(m_vSRVResults) ); // !cj! does not work in windows 
#endif
   }
   assert(i != m_vSRVResults.end());
   SRV next = *i;
   m_iCumulativeWeight -= next.cumulativeWeight;
   m_vSRVResults.erase(i);
   
#if !defined(WIN32) && !defined(__SUNPRO_CC) && !defined(__INTEL_COMPILER)
   //DB (<< "SRV: " << Inserter(m_vSRVResults));
#endif

   return next;
}


// adapted from the adig.c example in ares
const unsigned char*  DnsResult::parseAdditional(
	const unsigned char *aptr,
    const unsigned char *abuf, 
    int alen)
{
   char *name=0;
   int len=0;
   int status=0;

   // Parse the RR name. 
   status = ares_expand_name(aptr, abuf, alen, &name, &len);
   if (status != ARES_SUCCESS)
   {
      //DB (<< "Failed parse of RR");
      return NULL;
   }
   aptr += len;

   /* Make sure there is enough data after the RR name for the fixed
    * part of the RR.
    */
   if (aptr + RRFIXEDSZ > abuf + alen)
   {
      //DB (<< "Failed parse of RR");
      free(name);
      return NULL;
   }
  
   // Parse the fixed part of the RR, and advance to the RR data
   // field. 
   int type = DNS_RR_TYPE(aptr);
   int dlen = DNS_RR_LEN(aptr);
   aptr += RRFIXEDSZ;
   if (aptr + dlen > abuf + alen)
   {
      //DB (<< "Failed parse of RR");
      free(name);
      return NULL;
   }

   // Display the RR data.  Don't touch aptr. 
   string key = name;
   free(name);

   if (type == T_SRV)
   {
      SRV srv;
      
      // The RR data is three two-byte numbers representing the
      // priority, weight, and port, followed by a domain name.
      srv.key = key;
      srv.priority = DNS__16BIT(aptr);
      srv.weight = DNS__16BIT(aptr + 2);
      srv.port = DNS__16BIT(aptr + 4);
      status = ares_expand_name(aptr + 6, abuf, alen, &name, &len);
      if (status != ARES_SUCCESS)
      {
         return NULL;
      }
      srv.target = name;
      free(name);
      
      // Only add SRV results for the preferred NAPTR target as per rfc3263
      // (section 4.1). 
      assert(!m_PreferredNAPTR.key.empty());
      if (srv.key == m_PreferredNAPTR.replacement && srv.target != ".")
      {
		 if (srv.key.find(m_sLookupHeader) == 0)
		 {
			 srv.transport = m_iTransport;
		 }
         else
         {
            //DB (<< "Skipping SRV " << srv.key);
            return aptr + dlen;
         }
         
         //DB (<< "Inserting SRV: " << srv);
         m_vSRVResults.push_back(srv);
      }

      return aptr + dlen;

   }
   else if (type == T_A)
   {
      // The RR data is a four-byte Internet address. 
      if (dlen != 4)
      {
         //DB (<< "Failed parse of RR");
         return NULL;
      }

      struct in_addr addr;
      memcpy(&addr, aptr, sizeof(struct in_addr));
      //DB (<< "From additional: " << key << ":" << DnsUtil::inet_ntop(addr));

      // Only add the additional records if they weren't already there from
      // another query
      if (m_mARecords.count(key) == 0)
      {
         m_mARecords[key].push_back(addr);
      }
      return aptr + dlen;
   }
#ifdef USE_IPV6
   else if (type == T_AAAA)
   {
      if (dlen != 16) // The RR is 128 bits of ipv6 address
      {
         //DB (<< "Failed parse of RR");
         return NULL;
      }
      struct in6_addr addr;
      memcpy(&addr, aptr, sizeof(struct in6_addr));
      //DB (<< "From additional: " << key << ":" << DnsUtil::inet_ntop(addr));
      // Only add the additional records if they weren't already there from
      // another query
      if (mAAAARecords.count(key) == 0)
      {
         mAAAARecords[key].push_back(addr);
      }
      return aptr + dlen;
   }
#endif
   else // just skip it (we don't care :)
   {
      ////DB (<< "Skipping: " << key);
      return aptr + dlen;
   }
}

// adapted from the adig.c example in ares
const unsigned char* DnsResult::skipDNSQuestion(
	const unsigned char *aptr,
    const unsigned char *abuf,
    int alen)
{
   char *name=0;
   int status=0;
   int len=0;
   
   // Parse the question name. 
   status = ares_expand_name(aptr, abuf, alen, &name, &len);
   if (status != ARES_SUCCESS)
   {
      //DB (<< "Failed parse of RR");
      return NULL;
   }
   aptr += len;

   // Make sure there's enough data after the name for the fixed part
   // of the question.
   if (aptr + QFIXEDSZ > abuf + alen)
   {
      free(name);
      //DB (<< "Failed parse of RR");
      return NULL;
   }

   // Parse the question type and class. 
   //int type = DNS_QUESTION_TYPE(aptr);
   //int dnsclass = DNS_QUESTION_CLASS(aptr);
   aptr += QFIXEDSZ;
   
   free(name);
   return aptr;
}

// adapted from the adig.c example in ares
const unsigned char*  DnsResult::parseSRV(
	const unsigned char *aptr,
    const unsigned char *abuf, 
    int alen,
    SRV& srv)
{
   char *name;
   int len=0;
   int status=0;

   // Parse the RR name. 
   status = ares_expand_name(aptr, abuf, alen, &name, &len);
   if (status != ARES_SUCCESS)
   {
      //DB (<< "Failed parse of RR");
      return NULL;
   }
   aptr += len;

   /* Make sure there is enough data after the RR name for the fixed
    * part of the RR.
    */
   if (aptr + RRFIXEDSZ > abuf + alen)
   {
      free(name);
      //DB (<< "Failed parse of RR");
      return NULL;
   }
  
   // Parse the fixed part of the RR, and advance to the RR data
   // field. 
   int type = DNS_RR_TYPE(aptr);
   int dlen = DNS_RR_LEN(aptr);
   aptr += RRFIXEDSZ;
   if (aptr + dlen > abuf + alen)
   {
      free(name);
      //DB (<< "Failed parse of RR");
      return NULL;
   }
   string key = name;
   free(name);

   // Display the RR data.  Don't touch aptr. 
   if (type == T_SRV)
   {
      // The RR data is three two-byte numbers representing the
      // priority, weight, and port, followed by a domain name.
      srv.key = key;
      srv.priority = DNS__16BIT(aptr);
      srv.weight = DNS__16BIT(aptr + 2);
      srv.port = DNS__16BIT(aptr + 4);
      status = ares_expand_name(aptr + 6, abuf, alen, &name, &len);
      if (status != ARES_SUCCESS)
      {
         //DB (<< "Failed parse of RR");
         return NULL;
      }
      srv.target = name;
      free(name);

      return aptr + dlen;

   }
   else
   {
      //DB (<< "Failed parse of RR");
      return NULL;
   }
}
      

#ifdef USE_IPV6
// adapted from the adig.c example in ares
const unsigned char*  DnsResult::parseAAAA(
	const unsigned char *aptr,
    const unsigned char *abuf, 
    int alen,
    in6_addr * aaaa)
{
   char *name;
   int len=0;
   int status=0;

   // Parse the RR name. 
   status = ares_expand_name(aptr, abuf, alen, &name, &len);
   if (status != ARES_SUCCESS)
   {
      //DB (<< "Failed parse of RR");
      return NULL;
   }
   aptr += len;

   /* Make sure there is enough data after the RR name for the fixed
    * part of the RR.
    */
   if (aptr + RRFIXEDSZ > abuf + alen)
   {
      free(name);
      //DB (<< "Failed parse of RR");
      return NULL;
   }
  
   // Parse the fixed part of the RR, and advance to the RR data
   // field. 
   int type = DNS_RR_TYPE(aptr);
   int dlen = DNS_RR_LEN(aptr);
   aptr += RRFIXEDSZ;
   if (aptr + dlen > abuf + alen)
   {
      free(name);
      //DB (<< "Failed parse of RR");
      return NULL;
   }
   string key = name;
   free(name);

   // Display the RR data.  Don't touch aptr. 

   if (type == T_AAAA)
   {
      // The RR data is 128 bits of ipv6 address in network byte
      // order
      memcpy(aaaa, aptr, sizeof(in6_addr));
      return aptr + dlen;
   }
   else

   {
      //DB (<< "Failed parse of RR");
      return NULL;
   }
}
#endif


// adapted from the adig.c example in ares
const unsigned char*  DnsResult::parseNAPTR(
	const unsigned char *aptr,
    const unsigned char *abuf, 
    int alen,
    NAPTR& naptr)
{
   const unsigned char* p=0;
   char* name=0;
   int len=0;
   int status=0;

   // Parse the RR name. 
   status = ares_expand_name(aptr, abuf, alen, &name, &len);
   if (status != ARES_SUCCESS)
   {
      //DB (<< "Failed parse of RR");
      return NULL;
   }
   aptr += len;

   // Make sure there is enough data after the RR name for the fixed
   // part of the RR.
   if (aptr + RRFIXEDSZ > abuf + alen)
   {
      free(name);
      //DB (<< "Failed parse of RR");
      return NULL;
   }
  
   // Parse the fixed part of the RR, and advance to the RR data
   // field. 
   int type = DNS_RR_TYPE(aptr);
   int dlen = DNS_RR_LEN(aptr);
   aptr += RRFIXEDSZ;
   if (aptr + dlen > abuf + alen)
   {
      free(name);
      //DB (<< "Failed parse of RR");
      return NULL;
   }

   string key = name;
   free(name);

   // Look at the RR data.  Don't touch aptr. 
   if (type == T_NAPTR)
   {
      // The RR data is two two-byte numbers representing the
      // order and preference, followed by three character strings
      // representing flags, services, a regex, and a domain name.

      naptr.key = key;
      naptr.order = DNS__16BIT(aptr);
      naptr.pref = DNS__16BIT(aptr + 2);
      p = aptr + 4;
      len = *p;
      if (p + len + 1 > aptr + dlen)
      {
         //DB (<< "Failed parse of RR");
         return NULL;
      }
      naptr.flags.assign((const char*)(p+1), len);
      
      p += len + 1;
      len = *p;
      if (p + len + 1 > aptr + dlen)
      {
         //DB (<< "Failed parse of RR");
         return NULL;
      }
      naptr.service.assign((const char*)(p+1), len);

      p += len + 1;
      len = *p;
      if (p + len + 1 > aptr + dlen)
      {
         //DB (<< "Failed parse of RR");
         return NULL;
      }
      naptr.regex.assign((const char*)(p+1), len);

      p += len + 1;
      status = ares_expand_name(p, abuf, alen, &name, &len);
      if (status != ARES_SUCCESS)
      {
         //DB (<< "Failed parse of RR");
         return NULL;
      }
      naptr.replacement = name;
      
      free(name);
      return aptr + dlen;
   }
   else
   {
      //DB (<< "Failed parse of RR");
      return NULL;
   }
}

DnsResult::NAPTR::NAPTR():
	order(0),
	pref(0)
{
}

bool DnsResult::NAPTR::operator<(const DnsResult::NAPTR& rhs) const
{
   if (key.empty()) // default value
   {
      return false;
   }
   else if (rhs.key.empty()) // default value
   {
      return true;
   }
   else if (order < rhs.order)
   {
      return true;
   }
   else if (order == rhs.order)
   {
      if (pref < rhs.pref)
      {
         return true;
      }
      else if (pref == rhs.pref)
      {
         return replacement < rhs.replacement;
      }
   }
   return false;
}

DnsResult::SRV::SRV(): 
	priority(0),
	weight(0),
	cumulativeWeight(0),
	port(0)
{
}

bool DnsResult::SRV::operator<(const DnsResult::SRV& rhs) const
{
   if (priority < rhs.priority)
   {
      return true;
   }
   else if (priority == rhs.priority)
   {
      if (weight < rhs.weight)
      {
         return true;
      }
      else if (weight == rhs.weight)
      {
         if (transport < rhs.transport)
         {
            return true;
         }
         else if (transport == rhs.transport)
         {
            if (target < rhs.target)
            {
               return true;
            }
         }
      }
   }
   return false;
}



