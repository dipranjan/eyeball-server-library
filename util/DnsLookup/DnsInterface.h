

#ifndef DNS_INTERFACE_H
#define DNS_INTERFACE_H


#include <assert.h>
#include <sys/socket.h>

#include "DnsResult.h"



extern "C"
{
  struct ares_channeldata;
}


typedef int SOCKET;




class FdSet
{
 public:
  FdSet() : size(0)
    {
      FD_ZERO(&read);
      FD_ZERO(&write);
    }

  int select(struct timeval& tv)
    {
      return ::select(size, &read, &write, NULL, &tv);
    }

  int selectMilliSeconds(unsigned long ms)
    {
      struct timeval tv;
      tv.tv_sec = (ms/1000);
      tv.tv_usec = (ms%1000)*1000;
      return ::select(size, &read, &write, NULL, &tv);
    }

  bool readyToRead(SOCKET fd)
    {
      return (FD_ISSET(fd, &read) != 0);
    }

  bool readyToWrite(SOCKET fd)
    {
      return (FD_ISSET(fd, &write) != 0);
    }

  void setRead(SOCKET fd)
    {
      assert(FD_SETSIZE >= 8);
      FD_SET(fd, &read);
      size = (int(fd+1) > size) ? int(fd+1) : size;
    }

  void setWrite(SOCKET fd)
    {
      FD_SET(fd, &write);
      size = (int(fd+1) > size) ? int(fd+1) : size;

    }

  void clear(SOCKET fd)
    {
      FD_CLR(fd, &read);
      FD_CLR(fd, &write);
    }

  void reset()
    {
      size = 0;
      FD_ZERO(&read);
      FD_ZERO(&write);
    }

  // Make this stuff public for async dns/ares to use
  fd_set read;
  fd_set write;
  int size;
};

class DnsResult;


class DnsHandler
{
public: 
	virtual ~DnsHandler() = 0;

	// call when dns entries are available (or nothing found)
	// this may be called synchronously with the call to lookup
	virtual void Handle(DnsResult* result) = 0;
};


   
class DnsInterface
{
public:

	// Used to create an asynchronous Dns Interface. Any lookup requests will
	// be queued for later processing. It is critical that the consumer of the
	// DnsResult be in the same thread that is processing the async results
	// since there is no locking on the DnsResult
	// Will throw DnsInterface::Exception if ares fails to initialize
	DnsInterface();
	virtual ~DnsInterface();

public:
	// return if the client supports the specified service (e.g. SIP+D2T)
	bool isSupported(const std::string& sService);

	// adds the appropriate file descriptors to the fdset to allow a
	// select/poll call to be made 
	bool buildFdSet(FdSet& fdset);

	void GetTimeout(timeval *tvp);

	// process any dns results back from the async dns library (e.g. ares). If
	// there are results to report, post an event to the fifo
	void process(FdSet& fdset);

	// For each of the following calls, immediately return a DnsResult to the
	// caller. If synchronous, the DnsResult is complete and may block for an
	// arbitrary amount of time. In the synchronous case, the transactionId is
	// not useful. If asynchronous, the DnsResult will be returned immediately
	// and is owned by the caller. If queries are outstanding, it is not valid
	// for the caller to delete the DnsResult.
	// 
	// First determine a transport.  Second, determine a set of ports and ip
	// addresses. These can be returned to the client by asking the DnsResult
	// for the next result in the form of a Transport:Tuple. The client can
	// continue to ask the DnsResult to return more tuples. If the tuples for
	// the current transport are exhausted, move on to the next preferred
	// transport (if there is one)
	DnsResult* lookup(
		const std::string& sDomain, 
		const std::string& sService,
		const std::string& sProtocol,
		DnsHandler* handler = 0);

protected: 
	// When complete or partial results are ready, call DnsHandler::process()
	// For synchronous DnsInterface, set to 0
	struct ares_channeldata* m_Channel;
	friend class DnsResult;
};


#endif
