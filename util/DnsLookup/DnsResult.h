#ifndef DNS_RESULT_H
#define DNS_RESULT_H

//p struct hostent;
class DnsInterface;
class DnsHandler;



#include <list>
#include <map>
#include <string>
#include <vector>

#include "inet.h"
#include <netdb.h>
#include <stdio.h>
#include <cstring> 


enum TransportType
{
   TRANSPORT_UNKNOWN = 0,
   TRANSPORT_UDP,
   TRANSPORT_TCP,
   TRANSPORT_TLS,
   TRANSPORT_SCTP,
   TRANSPORT_DCCP,
   TRANSPORT_MAX
};


#if !defined(T_NAPTR)
#define T_NAPTR 35
#endif

#if !defined(T_SRV)
#define T_SRV 33
#endif

#if !defined(T_AAAA)
#define T_AAAA 28
#endif


class Tuple
{
public:
	Tuple()
	{
		memset(&m_anonv4, 0, sizeof(sockaddr_in));
		m_iTransportType = TRANSPORT_UNKNOWN;
	}

	Tuple(const in_addr& ipv4, int iport, TransportType iType)
	{
		m_iTransportType = iType;
		memset(&m_anonv4, 0, sizeof(sockaddr_in));
		m_anonv4.sin_addr = ipv4;
		m_anonv4.sin_port = htons(iport);
		m_anonv4.sin_family = AF_INET;
	}

	union 
	{
		sockaddr_in m_anonv4; // 16 bytes
#ifdef USE_IPV6
		sockaddr_in6 m_anonv6;
#endif
	};
	TransportType m_iTransportType;
};


class DnsResult
{
 public:
  DnsResult(DnsInterface& interfaceObj, DnsHandler* handler);
  ~DnsResult();

  typedef enum
    {
      Available, // A result is available now
      Pending,   // More results may be pending 
      Finished,  // No more results available and none pending
      Destroyed  // the associated transaction has been deleted
    } Type;

  // Check if there are tuples available now. Will load new tuples in if
  // necessary at a lower priority. 
  Type available();

  // return the next tuple available for this query. The caller should have
  // checked available() before calling next()
  Tuple next();

  // return the target of associated query
  std::string target() const {return m_sTarget;}

  // Will delete this DnsResult if no pending queries are out there or wait
  // until the pending queries get responses and then delete
  void destroy();

  // Used to store a NAPTR result
  class NAPTR
    {
    public:
      NAPTR();
      // As defined by RFC3263
      bool operator<(const NAPTR& rhs) const;

      std::string key; // NAPTR record key

      int order;
      int pref;
      std::string flags;
      std::string service;
      std::string regex;
      std::string replacement;
    };

  class SRV
    {
    public:
      SRV();
      // As defined by RFC3263, RFC2782
      bool operator<(const SRV& rhs) const;

      std::string key; // SRV record key

      TransportType transport;
      int priority;
      int weight;
      int cumulativeWeight; // for picking 
      int port;
      std::string target;
    };


 private:
  // Called by DnsInterface. Has the rules for determining the transport
  // from a uri as per rfc3263 and then does a NAPTR lookup or an A
  // lookup depending on the uri
  void lookup(
	      const std::string& sDomain, 
	      const std::string& sService,
	      const std::string& sProtocol);

  // performs host record lookups (A and AAAA) on target. 
  // May be asynchronous if ares is used. 
  // Otherwise, load the results into mResults
  void lookupARecords(const std::string& target);

  void lookupAAAARecords(const std::string& target);

  // performs a NAPTR lookup on mTarget. May be asynchronous if ares is
  // used. Otherwise, load the results into mResults
  void lookupNAPTR();

  // peforms an SRV lookup on target. May be asynchronous if ares is
  // used. Otherwise, load the results into mResults
  void lookupSRV(const std::string& target);

  // process a NAPTR record as per rfc3263
  void processNAPTR(int status, unsigned char* abuf, int alen);

  // process an SRV record as per rfc3263 and rfc2782. There may be more
  // than one SRV dns request outstanding at a time
  void processSRV(int status, unsigned char* abuf, int alen);

  // process the result of a AAAA query
  void processAAAA(int status, unsigned char* abuf, int alen);
  //
  // process an A record as per rfc3263
  void processHost(int status, struct hostent* result);

  // compute the cumulative weights for the SRV entries with the lowest
  // priority, then randomly pick according to RFC2782 from the entries with
  // the lowest priority based on weights. When all entries at the lowest
  // priority are chosen, pick the next lowest priority and repeat. After an
  // SRV entry is selected, remove it from mSRVResults
  SRV retrieveSRV();

  // Will retrieve the next SRV record and compute the prime the mResults
  // with the appropriate Tuples. 
  void primeResults();

  // this will parse any ADDITIONAL records from the dns result and stores
  // them in the DnsResult. Only keep additional SRV records where
  // mNAPTRResults contains a record with replacement is that SRV
  // Store all A records for convenience in mARecords 
  // and all AAAA records for convenience in mAAAARecords 
  const unsigned char* parseAdditional(
				       const unsigned char* aptr, 
				       const unsigned char* abuf,
				       int alen);

  // The callbacks associated with ares queries
  static void aresNAPTRCallback(void *arg, int status, unsigned char *abuf, int alen);
  static void aresSRVCallback(void *arg, int status, unsigned char *abuf, int alen);
  static void aresAAAACallback(void *arg, int status, unsigned char *abuf, int alen);
  static void aresHostCallback(void *arg, int status, struct hostent* result);

  // Some utilities for parsing dns results
  static const unsigned char* skipDNSQuestion(
					      const unsigned char *aptr,
					      const unsigned char *abuf,
					      int alen);

  static const unsigned char* parseSRV(
				       const unsigned char *aptr,
				       const unsigned char *abuf, 
				       int alen,
				       SRV& srv);

#ifdef USE_IPV6
  static const unsigned char* parseAAAA(
					const unsigned char *aptr,
					const unsigned char *abuf, 
					int alen,
					in6_addr * aaaa);
#endif

  static const unsigned char* parseNAPTR(
					 const unsigned char *aptr,
					 const unsigned char *abuf, 
					 int alen,
					 NAPTR& naptr);

 private:
  DnsInterface& m_Interface;
  DnsHandler* m_pHandler;
  int m_iSRVCount;
	
  std::string m_sTarget;
  std::string m_sLookupTarget;
  std::string m_sLookupHeader;

  TransportType m_iTransport; // current
  int m_iPort; // current
  Type m_Type;

  //Ugly hack
  std::string m_sPassHostFromAAAAtoA;

  // This is where the current pending (ordered) results are stored. As they
  // are retrieved by calling next(), they are popped from the front of the list
  std::list<Tuple> m_lResults;

  // The best NAPTR record. Only one NAPTR record will be selected
  NAPTR m_PreferredNAPTR;

  // used in determining the next SRV record to use as per rfc2782
  int m_iCumulativeWeight; // for current priority

  // All SRV records sorted in order of preference
  std::vector<SRV> m_vSRVResults;

  // All cached A records associated with this query/queries
  typedef std::list<struct in_addr> InAddrList;
  std::map<std::string,InAddrList > m_mARecords;

#ifdef USE_IPV6
  // All cached AAAA records associated with this query/queries
  typedef std::list<struct in6_addr> InAddr6List;
  std::map<std::string,InAddr6List> m_lAAAARecords;
#endif

  friend class DnsInterface;
};


#endif // DNS_RESULT_H
