#include "dassert.h"
#include <stdio.h>
#include <stdarg.h>


void db_printf(int cond, const char* str, ...)
{
  if (cond == 0)
    return;

  va_list ap;
  va_start(ap, str);
  vprintf(str, ap);
  va_end(ap);
}

