// LongLongMT.cc
// Implementation for the thread-safe 64-bit long longeger class
//
// Zion Kwok
// July 26, 2006
//
// (C) Eyeball Networks
//////////////////////////////////////////////

#include "LongLongMT.h"
#include "debug.h"

//! Default Constructor
CLongLongMT::CLongLongMT()
: m_iValue(0)
{
  DB_ENTER(CLongLongMT::CLongLongMT);

  pthread_mutex_init(&m_Mutex, NULL);
}

//! Copy Constructor
CLongLongMT::CLongLongMT(CLongLongMT& longlongMT)
: m_iValue(longlongMT)
{
  DB_ENTER(CLongLongMT::CLongLongMT2);

  pthread_mutex_init(&m_Mutex, NULL);
}

//! Constructor
CLongLongMT::CLongLongMT(long long value)
: m_iValue(value)
{
  DB_ENTER(CLongLongMT::CLongLongMT3);

  pthread_mutex_init(&m_Mutex, NULL);
}

//! Destructor
CLongLongMT::~CLongLongMT()
{
  DB_ENTER(CLongLongMT::~CLongLongMT);

  pthread_mutex_destroy(&m_Mutex);
}

//! Assignment operator
CLongLongMT& CLongLongMT::operator=(CLongLongMT& longlongMT)
{
  DB_ENTER(CLongLongMT::operator=);

  pthread_mutex_lock(&m_Mutex);
  m_iValue = longlongMT;
  pthread_mutex_unlock(&m_Mutex);

  return *this;
}

CLongLongMT& CLongLongMT::operator=(long long x)
{
  DB_ENTER(CLongLongMT::operator=2);

  pthread_mutex_lock(&m_Mutex);
  m_iValue = x;
  pthread_mutex_unlock(&m_Mutex);

  return *this;
}

//! Addition
CLongLongMT& CLongLongMT::operator+=(long long x)
{
  DB_ENTER(CLongLongMT::operator+=);

  pthread_mutex_lock(&m_Mutex);
  m_iValue += x;
  pthread_mutex_unlock(&m_Mutex);

  return *this;
}

//! Substraction
CLongLongMT& CLongLongMT::operator-=(long long x)
{
  DB_ENTER(CLongLongMT::operator-=);

  pthread_mutex_lock(&m_Mutex);
  m_iValue -= x;
  pthread_mutex_unlock(&m_Mutex);

  return *this;
}

//! Multiplication
CLongLongMT& CLongLongMT::operator*=(long long x)
{
  DB_ENTER(CLongLongMT::operator*=);

  pthread_mutex_lock(&m_Mutex);
  m_iValue *= x;
  pthread_mutex_unlock(&m_Mutex);

  return *this;
}

//! Division 
CLongLongMT& CLongLongMT::operator/=(long long x)
{
  DB_ENTER(CLongLongMT::operator/=);

  pthread_mutex_lock(&m_Mutex);
  m_iValue /= x;
  pthread_mutex_unlock(&m_Mutex);

  return *this;
}

//! Modulo 
CLongLongMT& CLongLongMT::operator%=(long long x)
{
  DB_ENTER(CLongLongMT::operator%=);

  pthread_mutex_lock(&m_Mutex);
  m_iValue %= x;
  pthread_mutex_unlock(&m_Mutex);

  return *this;
}

//! Prefix Increment
CLongLongMT& CLongLongMT::operator++()
{
  DB_ENTER(CLongLongMT::operator++prefix);

  pthread_mutex_lock(&m_Mutex);
  ++m_iValue;
  pthread_mutex_unlock(&m_Mutex);

  return *this;
}

//! Postfix Increment -- very inefficient
CLongLongMT CLongLongMT::operator++(int)
{
  DB_ENTER(CLongLongMT::operator++postfix);

  pthread_mutex_lock(&m_Mutex);
  CLongLongMT iRet(m_iValue++);
  pthread_mutex_unlock(&m_Mutex);

  return iRet;
}

//! Prefix Decrement
CLongLongMT& CLongLongMT::operator--()
{
  DB_ENTER(CLongLongMT::operator--prefix);

  pthread_mutex_lock(&m_Mutex);
  --m_iValue; 
  pthread_mutex_unlock(&m_Mutex);

  return *this;
}

//! Postfix Decrement
CLongLongMT CLongLongMT::operator--(int)
{
  DB_ENTER(CLongLongMT::operator--postfix);

  pthread_mutex_lock(&m_Mutex);
  CLongLongMT iRet(m_iValue--);
  pthread_mutex_unlock(&m_Mutex);

  return iRet;
}

//! Shift right
CLongLongMT& CLongLongMT::operator>>=(long long x)
{
  DB_ENTER(CLongLongMT::operator>>=);

  pthread_mutex_lock(&m_Mutex);
  m_iValue >>= x; 
  pthread_mutex_unlock(&m_Mutex);

  return *this;
}

//! Shift left
CLongLongMT& CLongLongMT::operator<<=(long long x)
{
  DB_ENTER(CLongLongMT::operator<<=);

  pthread_mutex_lock(&m_Mutex);
  m_iValue <<= x; 
  pthread_mutex_unlock(&m_Mutex);

  return *this;
}

//! And
CLongLongMT& CLongLongMT::operator&=(long long x)
{
  DB_ENTER(CLongLongMT::operator&=);

  pthread_mutex_lock(&m_Mutex);
  m_iValue &= x; 
  pthread_mutex_unlock(&m_Mutex);

  return *this;
}

//! Or
CLongLongMT& CLongLongMT::operator|=(long long x)
{
  DB_ENTER(CLongLongMT::operator|=);

  pthread_mutex_lock(&m_Mutex);
  m_iValue |= x; 
  pthread_mutex_unlock(&m_Mutex);

  return *this;
}

//! Xor
CLongLongMT& CLongLongMT::operator^=(long long x)
{
  DB_ENTER(CLongLongMT::operator^=);

  pthread_mutex_lock(&m_Mutex);
  m_iValue ^= x; 
  pthread_mutex_unlock(&m_Mutex);

  return *this;
}


//! Get long longeger value
CLongLongMT::operator long long()
{
  DB_ENTER(CLongLongMT::operator long long);

  pthread_mutex_lock(&m_Mutex);
  long long iValue = m_iValue;
  pthread_mutex_unlock(&m_Mutex);

  return iValue;
}



