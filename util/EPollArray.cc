#include "EPollArray.h"
#include "debug.h"
#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#ifdef DEBUG
        #include <iostream.h>
#endif

extern int g_iVerboseLevel;
/* Constructor */
NEB::CEPollArray::CEPollArray() :
        m_iMaxFd(0),
	m_ptrEventFds(NULL)
{
        SetMaxFds();
        m_iMaxFd = GetMaxFiles();

	m_ptrEventFds = new struct epoll_event[m_iMaxFd];

	memset(m_ptrEventFds, 0, m_iMaxFd * sizeof(struct epoll_event));

	m_iEpollFd = epoll_create(m_iMaxFd);
}
/* Add an fd */
bool NEB::CEPollArray::Add(int p_iFd, int p_iEvents)
{
        if(p_iFd < 0 || p_iFd >= m_iMaxFd)
                return false;

	struct epoll_event ev;
	memset(&ev, 0, sizeof(ev));
	ev.data.fd = p_iFd;
	ev.events = p_iEvents;

        return (epoll_ctl(m_iEpollFd, EPOLL_CTL_ADD, p_iFd, &ev) == 0);
}
/* Modify an fd */
bool NEB::CEPollArray::Modify(int p_iFd, int p_iEvents)
{
        if(p_iFd < 0 || p_iFd >= m_iMaxFd)
                return false;

	struct epoll_event ev;
	memset(&ev, 0, sizeof(ev));
	ev.data.fd = p_iFd;
	ev.events = p_iEvents;

        return (epoll_ctl(m_iEpollFd, EPOLL_CTL_MOD, p_iFd, &ev) == 0);
}
/* Remove an fd */
bool NEB::CEPollArray::Remove(int p_iFd)
{
        if(p_iFd < 0 || p_iFd >= m_iMaxFd)
                return false;

	struct epoll_event ev;
	memset(&ev, 0, sizeof(ev));
	return (epoll_ctl(m_iEpollFd, EPOLL_CTL_DEL, p_iFd, &ev) == 0);
}
/* Destructor*/
NEB::CEPollArray::~CEPollArray()
{
	delete [] m_ptrEventFds;
}
/* Set maximum fd */
rlim_t NEB::CEPollArray::SetMaxFds()
{
/*
        int fds[70000];
        
        memset(fds, 0, sizeof(fds));
        for (int i = 0; i < 70000; i++)
        {
                int fd = open("/dev/null", O_RDONLY);
                if (fd == -1)
                {
                        fds[i] = -1;
                        break;
                }
                fds[i] = fd;
        }

        for (int i = 0; i < 70000; i++)
                if (fds[i] > 0)
                        close(fds[i]);

*/

        rlimit limit;
        getrlimit(RLIMIT_NOFILE, &limit);
        limit.rlim_cur = limit.rlim_max;
        
        int ret = setrlimit(RLIMIT_NOFILE, &limit);
        if (ret != 0)
        {
                ASSERT(0 && "Cannot setrlimit");
        } 

        return limit.rlim_max;
}
/* Get maximum possible number of files */
int NEB::CEPollArray::GetMaxFiles(int p_iTryAtMost)
{
        int FDcount, i, f1, maxFD;
        int* Array;
        rlimit limit;

        // get current theoretical maximal fd
        getrlimit(RLIMIT_NOFILE, &limit);
        maxFD = limit.rlim_cur;
        if (p_iTryAtMost > maxFD) 
                p_iTryAtMost = maxFD;

        FDcount = 0;
        // alloc array, to remember which were opened
        Array = new int[p_iTryAtMost];
        if(NULL == Array) 
                return -1;

        for(i = 0; i < p_iTryAtMost; i++)
        {
                f1 = open("/dev/null", O_RDONLY);
                
                if (f1 < 0)
                        continue;   // could not open!
                // could open, count & remember
                Array[FDcount] = f1;
                FDcount++;
        }
        // do the closes
        for(i = 0; i < FDcount; i++)
                close(Array[i]);

/*        if(FDcount < p_iTryAtMost)
        {
                DB("There are only " << FDcount << " unique file descriptors, "
                        "instead of " << p_iTryAtMost);
        }*/


        delete [] Array;

        return FDcount;
}

int NEB::CEPollArray::poll(int ms)
{
	int iRet = ::epoll_wait(m_iEpollFd, m_ptrEventFds, m_iMaxFd, ms);	
	if (iRet > 0) {
		m_iNumEvents = iRet;
	}
	else {
		m_iNumEvents = 0;
	}
	return iRet;
}
