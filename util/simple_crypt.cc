// ---------------------------------------------------------------------------
// File:       crypt.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Simple encryption routines, i.e. for writing to registry
// 
// Change Log:
// ---------------------------------------------------------------------------


#include "simple_crypt.h"

#include "debug.h"

#include <stdio.h>
#include <stdlib.h>
#include <map>
#include <string.h>

using std::string;
using std::map;

typedef map<string, string> StringMap;


// ---------------------------------------------------------------------------
// Macros
// ---------------------------------------------------------------------------

/*
#define HEX_STAT(X) \
	{ \
		char buf[256]; \
		sprintf(buf, "%hx", (unsigned char)X); \
		DB(#X << " = 0x" << buf); \
	}
*/


// ---------------------------------------------------------------------------
// Local functions
// ---------------------------------------------------------------------------

static string rot13(string _str)
{
  // Note: Only manipulates alphabetic characters: [a-z,A-Z]
  register char cap, byte;
  string::size_type iLen = _str.length();

  for (string::size_type i = 0; i < iLen; i++)
    {
      byte = _str[i];

      cap = byte & 32;
      byte &= ~cap;
      byte = ((byte >= 'A') && (byte <= 'Z') ? ((byte - 'A' + 13) % 26 + 'A') : byte) | cap;

      _str[i] = byte;
    }

  return _str;
}

// Alternate MSB and LSB nibbles from start and end of string, working inwards
// Very simple, but should help against basic frequency attacks
static string swap_nibbles(string _str)
{
  string::size_type i, j;
  string::size_type iHalfLen = _str.length() >> 1;
  char left, right;

  for (i = 0, j = _str.length() - 1; i < iHalfLen; i++, j--)
    {
      left  = _str[i];
      right = _str[j];

      _str[i] = ((0x0f & right) << 4) | ((0xf0 & left)  >> 4);
      _str[j] = ((0x0f & left) << 4)  | ((0xf0 & right) >> 4);
    }

  return _str;
}

static string unswap_nibbles(string _str)
{
  string::size_type i, j;
  string::size_type iHalfLen = _str.length() >> 1;
  char left, right;

  for (i = 0, j = _str.length() - 1; i < iHalfLen; i++, j--)
    {
      left  = _str[i];
      right = _str[j];

      _str[i] = ((0x0f & left)  << 4) | ((0xf0 & right) >> 4);
      _str[j] = ((0x0f & right) << 4) | ((0xf0 & left)  >> 4);
    }

  return _str;
}

// Return hex version of string
static string hex_string(const string& _str)
{
  string rc;
  char szHexBuf[5];
  string::size_type iLen = _str.length();

  for (string::size_type i = 0; i < iLen; i++)
    {
      sprintf(szHexBuf, "%.2hx", (unsigned char)_str[i]);
      rc += szHexBuf;
    }

  return rc;
}

static string unhex_string(const string& _str)
{
  string rc;
  //	char cHex;
  short int cHex;
  string::size_type iLen = _str.length();

  for (string::size_type i = 0; i < iLen; i += 2)
    {
      sscanf(_str.substr(i, 2).c_str(), "%hx", &cHex);
      rc += char(cHex);
    }

  return rc;
}

static string random_string(int _len)
{
  string rc;

  // Seed pseudo-random number generator from clock
  srand(time(NULL));

  for (int i=0; i < _len; i++)
    {
      rc += (char)rand();
    }

  return rc;
}


static StringMap ReadPasswdFile(FILE *fp)
{
  char *cBuf = new char[200];
  StringMap passwd;
  string sUser;
  string sPasswd;

  while( fgets(cBuf, 200, fp) != NULL )
    {
      char *pSep = strchr(cBuf,':');
      if( pSep == NULL )
	{
	  continue;
	}
      sUser.assign(cBuf, pSep - cBuf);
      sPasswd.assign(pSep + 1);

      //truncate newline
      if (sPasswd.size() > 0) {
        sPasswd.resize(sPasswd.size() - 1);
      }

      passwd[sUser] = sPasswd;
    }

  delete [] cBuf;

  return passwd;
}

// ---------------------------------------------------------------------------
// Exported functions
// ---------------------------------------------------------------------------

string Crypt::simple_crypt(const string& _str, bool bHex)
{
  string sOutput = swap_nibbles(rot13(_str) + random_string(_str.length()));
  if (bHex) {
    return hex_string(sOutput);
  }
  return sOutput;
}

string Crypt::simple_decrypt(const string& _str, bool bHex)
{
  string sDecrypt;
  if (bHex) {
    sDecrypt = rot13(unswap_nibbles(unhex_string(_str)));
  }
  else {
    sDecrypt = rot13(unswap_nibbles(_str));
  }
  return sDecrypt.substr(0, sDecrypt.length() >> 1);
}

string Crypt::get_passwd(const string& sPasswdFile,const string& sUser)
{
  //	DB_ENTER(Crypt::get_passwd);

  FILE *fp;

  if( (fp=fopen(sPasswdFile.c_str(),"r"))==NULL )
    {
      return "";
    }

  StringMap sm = ReadPasswdFile(fp);
  fclose(fp);

  StringMap::iterator user = sm.find(sUser);
  if( user == sm.end() )
    {
      return "";
    }

  return simple_decrypt(user->second);
}

bool Crypt::set_passwd(const string& sPasswdFile,const string& sUser,const string& sPassword)
{
  //    DB_ENTER(Crypt::set_passwd);

  FILE *fp = NULL;
  bool bSuccess = false;

  StringMap sm;
  if( (fp=fopen(sPasswdFile.c_str(),"r"))!=NULL )
  {
    sm = ReadPasswdFile(fp);
    fclose(fp);
    fp = NULL;
  }

  sm[sUser] = simple_crypt(sPassword);

  unsigned int iSize = 0;

  StringMap::iterator user = sm.begin();
  for (; user != sm.end(); user++) {
    iSize += (user->first).size() + (user->second).size() + 2;
  }

  std::string sNewPasswordFile;
  sNewPasswordFile.reserve(iSize);
      
  user = sm.begin();
  for (; user != sm.end(); user++) {
    sNewPasswordFile += user->first;
    sNewPasswordFile += ":";
    sNewPasswordFile += user->second;
    sNewPasswordFile += "\n";
  }

  if ( (fp=fopen(sPasswdFile.c_str(),"w"))!=NULL )
  {
    if (fwrite(sNewPasswordFile.data(), 1, iSize, fp) == iSize) {
      bSuccess = true;
    }
    fclose(fp);
  }
  return bSuccess;
}




