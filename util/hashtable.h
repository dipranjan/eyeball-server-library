////////////////////////////////////////////////////////////////////////////////
//
// hashtable using chaining in case of collisions
// provides string hashing for a tuple (string,T*)
// uses SDBMHash or FastGoodHash to hash strings
// prefer this over STL hash_map due to memory leaks
//
//

#ifndef HASHTABLE_H
#define HASHTABLE_H

#include <string>
#include <pthread.h>
#include <semaphore.h>

#include "debug.h"
//#include "log.h"

#include <iostream>
using namespace std;



// string hashing function
unsigned int SDBMHash(const string& str, unsigned int tableSize);

unsigned long int FastGoodHash(const unsigned char *k, unsigned long int length, unsigned long int initval, unsigned int tableSize);

int FastGoodStringHash30(const string& s);

//#define USE_SDBM_HASH


template <class T> class HashTable
{

  typedef struct _chain_elem
  {
    string key;
    T *elem;
    _chain_elem *next;
  } chain_elem;

  chain_elem **m_Table;
  int m_iHashTableSize;
  int m_iBits;
  int m_iMask;

 public:

  HashTable();
  virtual ~HashTable();

  virtual int init(int iHashTableSize);

  virtual int addToTable(const string& key, T *elem, int hash = -1);
  virtual int updateTable(const string& key, T *elem, int hash = -1);
  virtual int delFromTable(const string& key, int hash = -1);
  virtual int delKeyFromTable(const string& key, int hash = -1);

  //For backwards compatibility only
  T* getFromTable(const string& key);

  virtual bool getFromTable(const string& key, T& elem, int hash = -1);

  virtual void clear();

  int m_iCount;
  virtual int count();

};




// thread-safe version of the hashtable
template <class T> class HashTableMT : public HashTable<T>
{

  pthread_mutex_t m_Mutex;

 public:

  HashTableMT();
  virtual ~HashTableMT();

  virtual int init(int iHashTableSize);

  virtual int addToTable(const string& key, T *elem, int hash = -1);
  virtual int updateTable(const string& key, T *elem, int hash = -1);
  virtual int delFromTable(const string& key, int hash = -1);
  virtual int delKeyFromTable(const string& key, int hash = -1);

  virtual bool getFromTable(const string& key, T& elem, int hash = -1);

  virtual void clear();

  virtual int count();

};





template<class T> HashTable<T>::HashTable()
{
  m_iHashTableSize = 0;
  m_Table = NULL;
  m_iCount=0;
//LOG("constructor" << m_iCount );
}// template<class T> HashTable<T>::HashTable()


template<class T> HashTable<T>::~HashTable()
{
  if (m_Table!=NULL) {
    clear();
    delete[] m_Table;
  }

}// template<class T> HashTable<T>::~HashTable()




template<class T> int HashTable<T>::init(int iLoad)
{
  int i;
  m_iCount=0;
//LOG("init" << m_iCount );

  if (iLoad < 2) {
    iLoad = 2;
  }

  //Design for 33-67% load and power of 2
  int iMaxLoad = iLoad + (iLoad >> 1);
  int iSize = 2;
  m_iBits = 1;
        
  while (iSize < iMaxLoad) {
    iSize <<= 1;
    m_iBits++;
  } 

  m_iHashTableSize = iSize;
  m_iMask = iSize - 1;

  m_Table = new chain_elem*[m_iHashTableSize];
  for (i=0;i<m_iHashTableSize;i++)
    m_Table[i] = NULL;

  return 0;

}// template<class T> int HashTable<T>::init(int iLoad)



template<class T> int HashTable<T>::addToTable(const string& key, T *elem, int hash /* = -1 */)
{
  chain_elem *tmp;
  chain_elem *prev = NULL;

  if (m_iHashTableSize == 0) {
    cerr << "Hash table not initialized\n";
    return 0;
  }

  if (hash == -1) {
#ifdef USE_SDBM_HASH
    hash = SDBMHash(key,m_iHashTableSize);
#else
    hash = FastGoodStringHash30(key);
#endif
  }
  hash &= m_iMask;

  tmp = m_Table[hash];

  while (tmp != NULL) {
    prev = tmp;
    tmp = tmp->next;
  }

  tmp = new chain_elem;
  tmp->next = NULL;
  tmp->key  = key;
  tmp->elem = elem;

  if (prev == NULL) {
    m_Table[hash] = tmp;
  }
  else {
    prev->next = tmp;
  }
  ++m_iCount;
//LOG("addToTable " << m_iCount );
  return 0;

}// template<class T> int HashTable<T>::addToTable(const string& key, T *elem, int hash /* = -1 */)


template<class T> int HashTable<T>::updateTable(const string& key, T *elem, int hash /* = -1 */)
{
  chain_elem *tmp;
  chain_elem *prev = NULL;

  if (m_iHashTableSize == 0) {
    cerr << "Hash table not initialized\n";
    return 0;
  }

  if (hash == -1) {
#ifdef USE_SDBM_HASH
    hash = SDBMHash(key,m_iHashTableSize);
#else
    hash = FastGoodStringHash30(key);
#endif
  }
  hash &= m_iMask;

  tmp = m_Table[hash];

  while (tmp != NULL) {
    if (tmp->key == key) {
      if (tmp->elem != elem) {
        delete tmp->elem;
        tmp->elem = elem;
      }
      return 0;
    }
    prev = tmp;
    tmp = tmp->next;
  }

  tmp = new chain_elem;
  tmp->next = NULL;
  tmp->key  = key;
  tmp->elem = elem;

  if (prev == NULL) {
    m_Table[hash] = tmp;
  }
  else {
    prev->next = tmp;
  }
//LOG("updateTable " << m_iCount );

  return 0;

}// template<class T> int HashTable<T>::updateTable(const string& key, T *elem, int hash /* = -1 */)




template<class T> int HashTable<T>::delFromTable(const string& key, int hash /* = -1 */)
{
  int result = 0;
  chain_elem *tmp, *tmp2;
  bool found = false;

  if (m_iHashTableSize == 0) {
    cerr << "Hash table not initialized\n";
    return 0;
  }

  if (hash == -1) {
#ifdef USE_SDBM_HASH
    hash = SDBMHash(key,m_iHashTableSize);
#else
    hash = FastGoodStringHash30(key);
#endif
  }
  hash &= m_iMask;

  tmp = m_Table[hash];
  if (tmp!=NULL)
    {
      if (tmp->key==key)
	{// first element in chain
	  m_Table[hash] = tmp->next;
	  delete tmp->elem;
	  delete tmp;

	  result = 1;
	}
      else
	{
	  tmp2 = m_Table[hash];
	  while (tmp!=NULL && !found)
	    {
	      if (key==tmp->key)
		found = true;
	      else
		{
		  tmp2 = tmp;
		  tmp = tmp->next;
		}
	    }
	
	  if (found==true)
	    {
	      tmp2->next = tmp->next;
	      delete tmp->elem;
	      delete tmp;
	      result = 1;
	    }
	}
    }
  --m_iCount;
//LOG("delFromTable " << m_iCount );
  return result;

}// template<class T> int HashTable<T>::delFromTable(const string& key, int hash /* = -1 */)


//! just delete key, not element
template<class T> int HashTable<T>::delKeyFromTable(const string& key, int hash /* = -1 */)
{
  int result = 0;
  chain_elem *tmp, *tmp2;
  bool found = false;

  if (m_iHashTableSize == 0) {
    cerr << "Hash table not initialized\n";
    return 0;
  }

  if (hash == -1) {
#ifdef USE_SDBM_HASH
    hash = SDBMHash(key,m_iHashTableSize);
#else
    hash = FastGoodStringHash30(key);
#endif
  }
  hash &= m_iMask;

  tmp = m_Table[hash];
  if (tmp!=NULL)
    {
      if (tmp->key==key)
	{// first element in chain
	  m_Table[hash] = tmp->next;
	  delete tmp;

	  result = 1;
	}
      else
	{
	  tmp2 = m_Table[hash];
	  while (tmp!=NULL && !found)
	    {
	      if (key==tmp->key)
		found = true;
	      else
		{
		  tmp2 = tmp;
		  tmp = tmp->next;
		}
	    }
	
	  if (found==true)
	    {
	      tmp2->next = tmp->next;
	      delete tmp;
	      result = 1;
	    }
	}
    }
//LOG("delKeyFromTable " << m_iCount );
  return result;

}// template<class T> int HashTable<T>::delKeyFromTable(const string& key, int hash /* = -1 */)



template<class T> T* HashTable<T>::getFromTable(const string& key)
{
  chain_elem *tmp;
  T* elem = NULL;
  bool found = false;

  if (m_iHashTableSize == 0) {
    cerr << "Hash table not initialized\n";
    return NULL;
  }

#ifdef USE_SDBM_HASH
  int hash = SDBMHash(key,m_iHashTableSize);
#else
  int hash = FastGoodStringHash30(key);
#endif
  hash &= m_iMask;

  tmp = m_Table[hash];
  while (tmp!=NULL && !found)
    {
      if (key==tmp->key)
	found = true;
      else
	tmp = tmp->next;

    }
      
  if (found==true)
    {
      elem = tmp->elem;
    }
//LOG("getFromTable1 " << m_iCount );
  return elem;

}// template<class T> T* HashTable<T>::getFromTable(const string& key, int hash /* = -1 */)


template<class T> bool HashTable<T>::getFromTable(const string& key, T& elem, int hash /* = -1 */)
{
  chain_elem *tmp;
  bool found = false;

  if (m_iHashTableSize == 0) {
    cerr << "Hash table not initialized\n";
    return false;
  }

  if (hash == -1) {
#ifdef USE_SDBM_HASH
    hash = SDBMHash(key,m_iHashTableSize);
#else
    hash = FastGoodStringHash30(key);
#endif
  }
  hash &= m_iMask;

  tmp = m_Table[hash];
  while (tmp!=NULL && !found)
    {
      if (key==tmp->key)
        found = true;
      else
        tmp = tmp->next;

    }

  if (found==true)
    {
      elem = *(tmp->elem);
    }

//LOG("getFromTable2 " << m_iCount );
  return found;
}

template<class T> void HashTable<T>::clear()
{
  int i;
  chain_elem* tmp;

  //! iterate through the chains and delete elements
  for (i=0;i<m_iHashTableSize;i++)
    {
      tmp = m_Table[i];
      while (tmp!=NULL)
	{
	  m_Table[i] = tmp->next;
	  if (tmp->elem!=NULL)
	    delete tmp->elem;
	  delete tmp;
	  tmp = m_Table[i];
	}
      m_Table[i] = NULL;
    }
  m_iCount=0;
//LOG("clear " << m_iCount );
}// template<class T> void HashTable<T>::clear()


template<class T> int HashTable<T>::count()
{
	return m_iCount;
}






template<class T> HashTableMT<T>::HashTableMT() : HashTable<T>()
{
  pthread_mutex_init(&m_Mutex,NULL);
  HashTable<T>::m_iCount=0;
//LOG("HashTableMT() " << HashTable<T>::m_iCount );
}// template<class T> HashTableMT<T>::HashTableMT()



template<class T> HashTableMT<T>::~HashTableMT()
{
  pthread_mutex_destroy(&m_Mutex);
}// template<class T> HashTableMT<T>::~HashTableMT()


template<class T> int HashTableMT<T>::init(int iLoad)
{
  int res;

  pthread_mutex_lock(&m_Mutex);

  res = HashTable<T>::init(iLoad);
  HashTable<T>::m_iCount=0;
//LOG("init2() " << HashTable<T>::m_iCount );

  pthread_mutex_unlock(&m_Mutex);

  return res;

}// template<class T> int HashTableMT<T>::init(int iHashTableSize)


template<class T> int HashTableMT<T>::addToTable(const string& key, T *elem, int hash /* = -1 */)
{
  int result;

  pthread_mutex_lock(&m_Mutex);
  result = HashTable<T>::addToTable(key,elem,hash);
  pthread_mutex_unlock(&m_Mutex);

  return result;
}// template<class T> int HashTableMT<T>::addToTable(const string& key, T *elem, int hash /* = -1 */)


template<class T> int HashTableMT<T>::updateTable(const string& key, T *elem, int hash /* = -1 */)
{
  int result;

  pthread_mutex_lock(&m_Mutex);
  result = HashTable<T>::updateTable(key,elem,hash);
  pthread_mutex_unlock(&m_Mutex);

  return result;
}// template<class T> int HashTableMT<T>::updateTable(const string& key, T *elem, int hash /* = -1 */)


template<class T> int HashTableMT<T>::delFromTable(const string& key, int hash /* = -1 */)
{
  int result;

  pthread_mutex_lock(&m_Mutex);
  result = HashTable<T>::delFromTable(key,hash);
  pthread_mutex_unlock(&m_Mutex);

  return result;
}// template<class T> int HashTableMT<T>::delFromTable(const string& key, int hash /* = -1 */) 


template<class T> int HashTableMT<T>::delKeyFromTable(const string& key, int hash /* = -1 */)
{
  int result;

  pthread_mutex_lock(&m_Mutex);
  result = HashTable<T>::delKeyFromTable(key,hash);
  pthread_mutex_unlock(&m_Mutex);

  return result;
}// template<class T> int HashTableMT<T>::delKeyFromTable(const string& key, int hash /* = -1 */) 


template<class T> bool HashTableMT<T>::getFromTable(const string& key, T& elem, int hash /* = -1 */)
{
  pthread_mutex_lock(&m_Mutex);

  bool bRet = HashTable<T>::getFromTable(key, elem, hash);

  pthread_mutex_unlock(&m_Mutex);
  
  return bRet;

}// template<class T> bool HashTableMT<T>::getFromTable(const string& key, T& elem, int hash /* = -1 */)

template<class T> int HashTableMT<T>::count()
{
  return HashTable<T>::count();

}

template<class T> void HashTableMT<T>::clear()
{
  pthread_mutex_lock(&m_Mutex);

  HashTable<T>::clear();

  pthread_mutex_unlock(&m_Mutex);
}// template<class T> void HashTableMT<T>::clear()



#endif
