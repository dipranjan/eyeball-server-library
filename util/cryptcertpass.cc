#include "simple_crypt.h"
#include <unistd.h>
#include <iostream>
#include <fstream>

using std::string;

int main(int argc, char* argv[])
{
        std::string sPrompt("Enter the password: ");
	char *pPass = getpass(sPrompt.c_str());
	std::string sPassword(pPass);
	if(sPassword.empty())
	{
	        std::cerr << "Password cannot be empty\n";
		return -1;
	}
	std::string sFileName;
	std::cout << "Enter filename: ";
	std::cin >> sFileName;
	if(sFileName.empty())
	{
	        std::cerr << "Filename cannot be empty\n";
		return -1;
	}
	std::string sEncryptedPassword(Crypt::simple_crypt(sPassword));
	std::ofstream PassFile(sFileName.c_str());
	PassFile << sEncryptedPassword;
	PassFile.close();
	
        return 0;
}
