#if !defined(_IOException_h)
#define _IOException_h

#include "Exception.h"
#include <assert.h>
#include <stdarg.h>
#include <strings.h>

/// Class thrown when io error occurs
class IOException : public Exception
{
        public:
                /// Constructor with empty argument list.
                IOException() : Exception() {}
                /// Overloaded constructor with variable length argument list.
                IOException(const char* p_NewDetails, ...)
                {
                        va_list ap;
                        va_start(ap, p_NewDetails);
                        SetDetails(p_NewDetails, ap);
                        va_end(ap);
                }
                /*
                // This interacts poorly with IOException(newDetails,...).  The 
                // constructor is prefered if the only parameter can be cast to
                // a void* (ie. one pointer as a parameter, like a char*).
                // I don't think I use it so I removed it.
                IOException(const char* newDetails, va_list ap)
                {
                        printf("ap=%p\n", ap);
                        setDetails(newDetails, ap);
                }
                */
};

#endif
