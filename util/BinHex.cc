//BinHex.h
//
//Zion Kwok
//June 30, 2006
//
//(C) Eyeball Networks

#include "BinHex.h"
#include "debug.h"

void convertBinToHex(const unsigned char* pszInputBin, unsigned int uiInputLen, unsigned char* pszOutputHex)
{
  DB_ENTER(convertBinToHex);  

  const unsigned char hex[16] = {'0','1','2','3','4','5','6','7',
                                 '8','9','a','b','c','d','e','f'};

  for (unsigned int i = 0; i < uiInputLen; i++) {
    int iBinValue = pszInputBin[i];
    pszOutputHex[i << 1]       = hex[iBinValue >> 4];
    pszOutputHex[(i << 1) + 1] = hex[iBinValue & 0xF];
  }
}

bool convertHexToBin(const unsigned char* pszInputHex, unsigned int uiInputLen, unsigned char* pszOutputBin)
{
  DB_ENTER(convertHexToBin);  

  if (uiInputLen & 1) {
    return false;
  }

  bool bValidInput = true;
  int iBinaryValue = 0;
  for (unsigned int i = 0; i < uiInputLen; i++) {
    int iHexValue = pszInputHex[i];
    if (('0' <= iHexValue) && (iHexValue <= '9')) {
      iBinaryValue |= iHexValue - '0';
    }
    else if (('a' <= iHexValue) && (iHexValue <= 'f')) {
      iBinaryValue |= 10 + iHexValue - 'a';
    }
    else if (('A' <= iHexValue) && (iHexValue <= 'F')) {
      iBinaryValue |= 10 + iHexValue - 'A';
    }
    else {
      bValidInput = false;
      break;
    }
    if (i & 1) {
      pszOutputBin[i >> 1] = (char)iBinaryValue;
      iBinaryValue = 0;
    }
    else {
      iBinaryValue <<= 4;
    }
  }

  return bValidInput;
}

