#include "CPollArray.h"
#include "debug.h"
#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#ifdef DEBUG
        #include <iostream.h>
#endif

extern int g_iVerboseLevel;
/* Constructor */
NEB::CPollArray::CPollArray() :
        m_iMaxFd(0),
        m_iMaxUsedFd(0),
        m_ptrPollFds(NULL),
	m_ptrEventFds(NULL)
{
        SetMaxFds();
        m_iMaxFd = GetMaxFiles();

        m_ptrPollFds = new struct pollfd[m_iMaxFd];
	m_ptrEventFds = new struct pollfd[m_iMaxFd];

	memset(m_ptrPollFds, 0, m_iMaxFd * sizeof(struct pollfd));
	memset(m_ptrEventFds, 0, m_iMaxFd * sizeof(struct pollfd));

        int i;
        for(i = 0; i < m_iMaxFd; i++)
        {
                m_ptrPollFds[i].fd = -1;
        }
}
/* Add an fd */
bool NEB::CPollArray::Add(int p_iFd, short p_sEvents)
{
        if(p_iFd < 0 || p_iFd >= m_iMaxFd)
                return false;

        if(-1 != m_ptrPollFds[p_iFd].fd)
                return false;
                
        m_ptrPollFds[p_iFd].fd = p_iFd;
        m_ptrPollFds[p_iFd].events = p_sEvents;
        m_ptrPollFds[p_iFd].revents = 0;
        
        if(p_iFd > m_iMaxUsedFd)
                m_iMaxUsedFd = p_iFd;
                
        return true;
}
/* Modify an fd */
bool NEB::CPollArray::Modify(int p_iFd, short p_sEvents)
{
        if(p_iFd < 0 || p_iFd >= m_iMaxFd)
                return false;

        m_ptrPollFds[p_iFd].events = p_sEvents;
        
        return true;
}
/* Remove an fd */
bool NEB::CPollArray::Remove(int p_iFd)
{
        if(p_iFd < 0 || p_iFd >= m_iMaxFd)
                return false;

        if(-1 == m_ptrPollFds[p_iFd].fd)
                return false;
                
        m_ptrPollFds[p_iFd].fd = -1;
        m_ptrPollFds[p_iFd].events = 0;
        m_ptrPollFds[p_iFd].revents = 0;
        // Adjust m_iMaxUsedFd, if needed
        if(m_iMaxUsedFd == p_iFd)
        {
                while(m_iMaxUsedFd)
                {
                        if(m_ptrPollFds[m_iMaxUsedFd].fd != -1)
                                break;
                        --m_iMaxUsedFd;
                }
        }
        
        return true;
}
/* Destructor*/
NEB::CPollArray::~CPollArray()
{
        while(m_iMaxUsedFd > 0)
        {
                if(-1 != m_ptrPollFds[m_iMaxUsedFd].fd)
                {
                        CloseSocket(m_ptrPollFds[m_iMaxUsedFd].fd);
                        m_ptrPollFds[m_iMaxUsedFd].fd = -1;
                }
                m_ptrPollFds[m_iMaxUsedFd].events = 0;
                m_ptrPollFds[m_iMaxUsedFd].revents = 0;
                
                --m_iMaxUsedFd;
        }
        
        delete [] m_ptrPollFds;
	delete [] m_ptrEventFds;
}
/* Closes a socket */
bool NEB::CPollArray::CloseSocket(int p_iFd)
{
        if(-1 == p_iFd)
                return false;
                
        if(shutdown(p_iFd, SHUT_RDWR))
                return false;
        
        if(close(p_iFd))
                return false;
        
        return true;
}
/* Set maximum fd */
rlim_t NEB::CPollArray::SetMaxFds()
{
/*
        int fds[70000];
        
        memset(fds, 0, sizeof(fds));
        for (int i = 0; i < 70000; i++)
        {
                int fd = open("/dev/null", O_RDONLY);
                if (fd == -1)
                {
                        fds[i] = -1;
                        break;
                }
                fds[i] = fd;
        }

        for (int i = 0; i < 70000; i++)
                if (fds[i] > 0)
                        close(fds[i]);
*/

        rlimit limit;
        getrlimit(RLIMIT_NOFILE, &limit);
        limit.rlim_cur = limit.rlim_max;
        
        int ret = setrlimit(RLIMIT_NOFILE, &limit);
        if (ret != 0)
        {
                ASSERT(0 && "Cannot setrlimit");
        } 

        return limit.rlim_max;
}
/* Get maximum possible number of files */
int NEB::CPollArray::GetMaxFiles(int p_iTryAtMost)
{
        int FDcount, i, f1, maxFD;
        int* Array;
        rlimit limit;

        // get current theoretical maximal fd
        getrlimit(RLIMIT_NOFILE, &limit);
        maxFD = limit.rlim_cur;
        if (p_iTryAtMost > maxFD) 
                p_iTryAtMost = maxFD;

        FDcount = 0;
        // alloc array, to remember which were opened
        Array = new int[p_iTryAtMost];
        if(NULL == Array) 
                return -1;

        for(i = 0; i < p_iTryAtMost; i++)
        {
                f1 = open("/dev/null", O_RDONLY);
                
                if (f1 < 0)
                        continue;   // could not open!
                // could open, count & remember
                Array[FDcount] = f1;
                FDcount++;
        }
        // do the closes
        for(i = 0; i < FDcount; i++)
                close(Array[i]);

/*        if(FDcount < p_iTryAtMost)
        {
                DB("There are only " << FDcount << " unique file descriptors, "
                        "instead of " << p_iTryAtMost);
        }*/


        delete [] Array;

        return FDcount;
}

int NEB::CPollArray::poll(int ms)
{
	int iRet = ::poll(m_ptrPollFds, m_iMaxUsedFd + 1, ms);	
	if (iRet > 0) {
		m_iNumEvents = iRet;
		int e = 0;
		for (int i = 0; i <= m_iMaxUsedFd; i++) {
			if (m_ptrPollFds[i].revents == 0) continue;

			m_ptrEventFds[e] = m_ptrPollFds[i];
			if (++e == iRet) break;
		}
	}
	else {
		m_iNumEvents = 0;
	}
	return iRet;
}
