//BinHex.h
//
//Zion Kwok
//June 30, 2006
//
//(C) Eyeball Networks

#ifndef EB_BIN_HEX
#define EB_BIN_HEX

void convertBinToHex(const unsigned char* pszInputBin, unsigned int uiInputLen, unsigned char* pszOutputHex);
bool convertHexToBin(const unsigned char* pszInputHex, unsigned int uiInputLen, unsigned char* pszOutputBin);

#endif
