// ---------------------------------------------------------------------------
// File:       utypes.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    
// 
// Change Log:
// ---------------------------------------------------------------------------

#ifndef UTYPES_H
#define UTYPES_H

typedef unsigned char   u_int8_t;
typedef          short  int16_t;
typedef unsigned short  u_int16_t;
typedef unsigned int    u_int32_t;
typedef          int    int32_t;

#endif // UTYPES_H
