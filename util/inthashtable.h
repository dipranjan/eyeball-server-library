////////////////////////////////////////////////////////////////////////////////
//
// hashtable using chaining in case of collisions
// provides int hashing for a tuple (int,T*)
//
//

#ifndef INT_HASHTABLE_H
#define INT_HASHTABLE_H

#include <pthread.h>
#include <semaphore.h>


#include <iostream>
using std::cerr;







template <class T> class IntHashTable
{

  typedef struct _chain_elem
  {
    int key;
    T *elem;
    _chain_elem *next;
  } chain_elem;

  chain_elem **m_Table;
  int m_iHashTableSize;
  int m_iMask;

 public:

  IntHashTable();
  virtual ~IntHashTable();

  virtual int init(int iLoad);

  virtual int addToTable(int key, T *elem);
  virtual int updateTable(int key, T *elem);
  virtual int delFromTable(int key);
  virtual int delKeyFromTable(int key);

  T* getFromTable(int key);
  virtual bool getFromTable(int key, T& value);

  virtual void clear();

};




// thread-safe version of the hashtable
template <class T> class IntHashTableMT : public IntHashTable<T>
{

  pthread_mutex_t m_Mutex;

 public:

  IntHashTableMT();
  virtual ~IntHashTableMT();

  virtual int init(int iLoad);

  virtual int addToTable(int key, T *elem);
  virtual int updateTable(int key, T *elem);
  virtual int delFromTable(int key);
  virtual int delKeyFromTable(int key);

  virtual bool getFromTable(int key, T& value);

  virtual void clear();

};





template<class T> IntHashTable<T>::IntHashTable()
{
  m_iHashTableSize = 0;
  m_Table = NULL;
}// template<class T> IntHashTable<T>::IntHashTable()


template<class T> IntHashTable<T>::~IntHashTable()
{
  if (m_Table!=NULL) {
    clear();
    delete[] m_Table;
  }

}// template<class T> IntHashTable<T>::~IntHashTable()




template<class T> int IntHashTable<T>::init(int iLoad)
{
  int i;

  if (iLoad < 1) {
    iLoad = 1;
  }

  //Design for 33-67% load and power of 2
  int iMaxLoad = iLoad + (iLoad >> 1);
  int iSize = 1;

  while (iSize < iMaxLoad) {
    iSize <<= 1;
  }

  m_iHashTableSize = iSize;
  m_iMask = iSize - 1;

  m_Table = new chain_elem*[m_iHashTableSize];
  for (i=0;i<m_iHashTableSize;i++)
    m_Table[i] = NULL;

  return 0;

}// template<class T> int IntHashTable<T>::init(int iHashTableSize)



template<class T> int IntHashTable<T>::addToTable(int key, T *elem)
{
  chain_elem *tmp;
  chain_elem *prev = NULL;

  if (m_iHashTableSize == 0) {
    cerr << "Hash table not initialized\n";
    return 0;
  }

  int hash = key & m_iMask;

  tmp = m_Table[hash];

  while (tmp != NULL) {
    prev = tmp;
    tmp = tmp->next;
  }

  tmp = new chain_elem;
  tmp->next = NULL;
  tmp->key  = key;
  tmp->elem = elem;

  if (prev == NULL) {
    m_Table[hash] = tmp;
  }
  else {
    prev->next = tmp;
  }

  return 0;

}// template<class T> int IntHashTable<T>::addToTable(int key, T *elem)

template<class T> int IntHashTable<T>::updateTable(int key, T *elem)
{
  chain_elem *tmp;
  chain_elem *prev = NULL;

  if (m_iHashTableSize == 0) {
    cerr << "Hash table not initialized\n";
    return 0;
  }

  int hash = key & m_iMask;

  tmp = m_Table[hash];

  while (tmp != NULL) {
    if (tmp->key == key) {
      delete tmp->elem;
      tmp->elem = elem;
      return 0;
    }
    prev = tmp;
    tmp = tmp->next;
  }

  tmp = new chain_elem;
  tmp->next = NULL;
  tmp->key  = key;
  tmp->elem = elem;

  if (prev == NULL) {
    m_Table[hash] = tmp;
  }
  else {
    prev->next = tmp;
  }

  return 0;

}// template<class T> int IntHashTable<T>::updateTable(int key, T *elem)




template<class T> int IntHashTable<T>::delFromTable(int key)
{
  int result = 0;
  chain_elem *tmp, *tmp2;
  int hash = key & m_iMask;
  bool found = false;

  if (m_iHashTableSize == 0) {
    cerr << "Hash table not initialized\n";
    return 0;
  }

  tmp = m_Table[hash];
  if (tmp!=NULL)
    {
      if (tmp->key==key)
	{// first element in chain
	  m_Table[hash] = tmp->next;
	  delete tmp->elem;
	  delete tmp;

	  result = 1;
	}
      else
	{
	  tmp2 = m_Table[hash];
	  while (tmp!=NULL && !found)
	    {
	      if (key==tmp->key)
		found = true;
	      else
		{
		  tmp2 = tmp;
		  tmp = tmp->next;
		}
	    }
	
	  if (found==true)
	    {
	      tmp2->next = tmp->next;
	      delete tmp->elem;
	      delete tmp;
	      result = 1;
	    }
	}
    }

  return result;

}// template<class T> int HashTable<T>::delFromTable(int key)


//! just delete key, not element
template<class T> int IntHashTable<T>::delKeyFromTable(int key)
{
  int result = 0;
  chain_elem *tmp, *tmp2;
  int hash = key & m_iMask;
  bool found = false;

  if (m_iHashTableSize == 0) {
    cerr << "Hash table not initialized\n";
    return 0;
  }

  tmp = m_Table[hash];
  if (tmp!=NULL)
    {
      if (tmp->key==key)
	{// first element in chain
	  m_Table[hash] = tmp->next;
	  delete tmp;

	  result = 1;
	}
      else
	{
	  tmp2 = m_Table[hash];
	  while (tmp!=NULL && !found)
	    {
	      if (key==tmp->key)
		found = true;
	      else
		{
		  tmp2 = tmp;
		  tmp = tmp->next;
		}
	    }
	
	  if (found==true)
	    {
	      tmp2->next = tmp->next;
	      delete tmp;
	      result = 1;
	    }
	}
    }

  return result;

}// template<class T> int HashTable<T>::delKeyFromTable(int key)



template<class T> T* IntHashTable<T>::getFromTable(int key)
{
  chain_elem *tmp;
  T* elem = NULL;
  bool found = false;
  int hash = key & m_iMask;

  if (m_iHashTableSize == 0) {
    cerr << "Hash table not initialized\n";
    return NULL;
  }

  tmp = m_Table[hash];
  while (tmp!=NULL && !found)
    {
      if (key==tmp->key)
	found = true;
      else
	tmp = tmp->next;

    }
      
  if (found==true)
    {
      elem = tmp->elem;
    }

  return elem;

}// template<class T> T* IntHashTable<T>::getFromTable(int key)

template<class T> bool IntHashTable<T>::getFromTable(int key, T& value)
{
  T* t = getFromTable(key);
  if (t != NULL) {
    value = *t;
    return true;
  }
  return false; 
}

template<class T> void IntHashTable<T>::clear()
{
  int i;
  chain_elem* tmp;

  //! iterate through the chains and delete elements
  for (i=0;i<m_iHashTableSize;i++)
    {
      tmp = m_Table[i];
      while (tmp!=NULL)
	{
	  m_Table[i] = tmp->next;
	  if (tmp->elem!=NULL)
	    delete tmp->elem;
	  delete tmp;
	  tmp = m_Table[i];
	}

      m_Table[i] = NULL;
    }
}// template<class T> void IntHashTable<T>::clear()









template<class T> IntHashTableMT<T>::IntHashTableMT() : IntHashTable<T>()
{
  pthread_mutex_init(&m_Mutex,NULL);
}// template<class T> IntHashTableMT<T>::IntHashTableMT()



template<class T> IntHashTableMT<T>::~IntHashTableMT()
{
  pthread_mutex_destroy(&m_Mutex);
}// template<class T> IntHashTableMT<T>::~IntHashTableMT()


template<class T> int IntHashTableMT<T>::init(int iLoad)
{
  int res;

  pthread_mutex_lock(&m_Mutex);
  res = IntHashTable<T>::init(iLoad);
  pthread_mutex_unlock(&m_Mutex);

  return res;

}// template<class T> int IntHashTableMT<T>::init(int iHashTableSize)


template<class T> int IntHashTableMT<T>::addToTable(int key, T *elem)
{
  int result;

  pthread_mutex_lock(&m_Mutex);
  result = IntHashTable<T>::addToTable(key,elem);
  pthread_mutex_unlock(&m_Mutex);

  return result;
}// template<class T> int IntHashTableMT<T>::addToTable(int key, T *elem)

template<class T> int IntHashTableMT<T>::updateTable(int key, T *elem)
{
  int result;

  pthread_mutex_lock(&m_Mutex);
  result = IntHashTable<T>::updateTable(key,elem);
  pthread_mutex_unlock(&m_Mutex);

  return result;
}// template<class T> int IntHashTableMT<T>::updateTable(int key, T *elem)

template<class T> int IntHashTableMT<T>::delFromTable(int key)
{
  int result;

  pthread_mutex_lock(&m_Mutex);
  result = IntHashTable<T>::delFromTable(key);
  pthread_mutex_unlock(&m_Mutex);

  return result;
}// template<class T> int IntHashTableMT<T>::delFromTable(int key)


template<class T> int IntHashTableMT<T>::delKeyFromTable(int key)
{
  int result;

  pthread_mutex_lock(&m_Mutex);
  result = IntHashTable<T>::delKeyFromTable(key);
  pthread_mutex_unlock(&m_Mutex);

  return result;
}// template<class T> int IntHashTableMT<T>::delKeyFromTable(int key)


template<class T> bool IntHashTableMT<T>::getFromTable(int key, T& value)
{
  pthread_mutex_lock(&m_Mutex);
  bool ret = IntHashTable<T>::getFromTable(key, value);
  pthread_mutex_unlock(&m_Mutex);
  
  return ret;

}// template<class T> T* IntHashTableMT<T>::getFromTable(int key)



template<class T> void IntHashTableMT<T>::clear()
{
  pthread_mutex_lock(&m_Mutex);

  IntHashTable<T>::clear();

  pthread_mutex_unlock(&m_Mutex);
}// template<class T> void IntHashTableMT<T>::clear()



#endif
