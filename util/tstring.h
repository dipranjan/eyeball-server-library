
#ifndef __TSTRING__
#define __TSTRING__

#include <string>
#include <iostream>

#include <wchar.h>

#define USE_WCHAR

#ifdef USE_WCHAR
typedef std::basic_string<wchar_t> my_wstring;
#else
typedef std::basic_string<unsigned short> my_wstring;
#endif

#ifdef UNICODE
typedef my_wstring ustring;
#else
typedef std::string ustring;
#endif // UNICODE

class tstring : public ustring
{
public:
	// Constructors
	tstring();
	tstring(const std::string& s);
	tstring(const my_wstring& s);
	tstring(const char* p);
	tstring(const unsigned short* p);
	tstring(const wchar_t* p);
	virtual ~tstring() {}

	/**
	 * Converts to one-byte-per-character string. Only takes the lower byte.
	 * The upper byte is lost.
	 */
	std::string mbs(void) const;

	/**
	 * Converts to two-byte-per-character string. No information is lost.
	 * For single byte characters, zero is inserted to upper byte.
	 */
	std::string to2bs(void) const; 

	friend std::ostream& operator<<(std::ostream& s, const tstring& v);
};

inline tstring operator+(const tstring& string1, const tstring& string2)
{
	tstring s;
	s.assign(string1);
	s.append(string2);
	return s;
}

tstring from2bs(const std::string& in);

#endif // __TSTRING__
