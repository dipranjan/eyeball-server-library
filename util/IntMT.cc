// IntMT.cc
// Implementation for the thread-safe integer class
//
// Zion Kwok
// July 14, 2006
//
// (C) Eyeball Networks
//////////////////////////////////////////////

#include "IntMT.h"
#include "debug.h"

//! Default Constructor
CIntMT::CIntMT()
: m_iValue(0)
{
  DB_ENTER(CIntMT::CIntMT);

  pthread_mutex_init(&m_Mutex, NULL);
}

//! Copy Constructor
CIntMT::CIntMT(CIntMT& intMT)
: m_iValue(intMT)
{
  DB_ENTER(CIntMT::CIntMT2);

  pthread_mutex_init(&m_Mutex, NULL);
}

//! Constructor
CIntMT::CIntMT(int value)
: m_iValue(value)
{
  DB_ENTER(CIntMT::CIntMT3);

  pthread_mutex_init(&m_Mutex, NULL);
}

//! Destructor
CIntMT::~CIntMT()
{
  DB_ENTER(CIntMT::~CIntMT);

  pthread_mutex_destroy(&m_Mutex);
}

//! Assignment operator
CIntMT& CIntMT::operator=(CIntMT& intMT)
{
  DB_ENTER(CIntMT::operator=);

  pthread_mutex_lock(&m_Mutex);
  m_iValue = intMT;
  pthread_mutex_unlock(&m_Mutex);

  return *this;
}

CIntMT& CIntMT::operator=(int x)
{
  DB_ENTER(CIntMT::operator=2);

  pthread_mutex_lock(&m_Mutex);
  m_iValue = x;
  pthread_mutex_unlock(&m_Mutex);

  return *this;
}

//! Addition
CIntMT& CIntMT::operator+=(int x)
{
  DB_ENTER(CIntMT::operator+=);

  pthread_mutex_lock(&m_Mutex);
  m_iValue += x;
  pthread_mutex_unlock(&m_Mutex);

  return *this;
}

//! Substraction
CIntMT& CIntMT::operator-=(int x)
{
  DB_ENTER(CIntMT::operator-=);

  pthread_mutex_lock(&m_Mutex);
  m_iValue -= x;
  pthread_mutex_unlock(&m_Mutex);

  return *this;
}

//! Multiplication
CIntMT& CIntMT::operator*=(int x)
{
  DB_ENTER(CIntMT::operator*=);

  pthread_mutex_lock(&m_Mutex);
  m_iValue *= x;
  pthread_mutex_unlock(&m_Mutex);

  return *this;
}

//! Division 
CIntMT& CIntMT::operator/=(int x)
{
  DB_ENTER(CIntMT::operator/=);

  pthread_mutex_lock(&m_Mutex);
  m_iValue /= x;
  pthread_mutex_unlock(&m_Mutex);

  return *this;
}

//! Modulo 
CIntMT& CIntMT::operator%=(int x)
{
  DB_ENTER(CIntMT::operator%=);

  pthread_mutex_lock(&m_Mutex);
  m_iValue %= x;
  pthread_mutex_unlock(&m_Mutex);

  return *this;
}

//! Prefix Increment
CIntMT& CIntMT::operator++()
{
  DB_ENTER(CIntMT::operator++prefix);

  pthread_mutex_lock(&m_Mutex);
  ++m_iValue;
  pthread_mutex_unlock(&m_Mutex);

  return *this;
}

//! Postfix Increment -- very inefficient
CIntMT CIntMT::operator++(int)
{
  DB_ENTER(CIntMT::operator++postfix);

  pthread_mutex_lock(&m_Mutex);
  CIntMT iRet(m_iValue++);
  pthread_mutex_unlock(&m_Mutex);

  return iRet;
}

//! Prefix Decrement
CIntMT& CIntMT::operator--()
{
  DB_ENTER(CIntMT::operator--prefix);

  pthread_mutex_lock(&m_Mutex);
  --m_iValue; 
  pthread_mutex_unlock(&m_Mutex);

  return *this;
}

//! Postfix Decrement
CIntMT CIntMT::operator--(int)
{
  DB_ENTER(CIntMT::operator--postfix);

  pthread_mutex_lock(&m_Mutex);
  CIntMT iRet(m_iValue--);
  pthread_mutex_unlock(&m_Mutex);

  return iRet;
}

//! Shift right
CIntMT& CIntMT::operator>>=(int x)
{
  DB_ENTER(CIntMT::operator>>=);

  pthread_mutex_lock(&m_Mutex);
  m_iValue >>= x; 
  pthread_mutex_unlock(&m_Mutex);

  return *this;
}

//! Shift left
CIntMT& CIntMT::operator<<=(int x)
{
  DB_ENTER(CIntMT::operator<<=);

  pthread_mutex_lock(&m_Mutex);
  m_iValue <<= x; 
  pthread_mutex_unlock(&m_Mutex);

  return *this;
}

//! And
CIntMT& CIntMT::operator&=(int x)
{
  DB_ENTER(CIntMT::operator&=);

  pthread_mutex_lock(&m_Mutex);
  m_iValue &= x; 
  pthread_mutex_unlock(&m_Mutex);

  return *this;
}

//! Or
CIntMT& CIntMT::operator|=(int x)
{
  DB_ENTER(CIntMT::operator|=);

  pthread_mutex_lock(&m_Mutex);
  m_iValue |= x; 
  pthread_mutex_unlock(&m_Mutex);

  return *this;
}

//! Xor
CIntMT& CIntMT::operator^=(int x)
{
  DB_ENTER(CIntMT::operator^=);

  pthread_mutex_lock(&m_Mutex);
  m_iValue ^= x; 
  pthread_mutex_unlock(&m_Mutex);

  return *this;
}


//! Get integer value
CIntMT::operator int()
{
  DB_ENTER(CIntMT::operator int);

  pthread_mutex_lock(&m_Mutex);
  int iValue = m_iValue;
  pthread_mutex_unlock(&m_Mutex);

  return iValue;
}



