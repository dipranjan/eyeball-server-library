// DoubleMT.h
// Interface for the thread-safe double-precision floating-point number class
//
// Zion Kwok
// July 26, 2006
//
// (C) Eyeball Networks
//////////////////////////////////////////////

#ifndef EB_DOUBLE_MT_H_
#define EB_DOUBLE_MT_H_

#include <pthread.h>

class CDoubleMT 
{
public:
	//! Default Constructor
	CDoubleMT();

	//! Copy Constructor
	CDoubleMT(CDoubleMT& doubleMT);

	//! Constructor
	CDoubleMT(double value);

	//! Destructor
	~CDoubleMT();

	//! Assignment operator
	CDoubleMT& operator=(CDoubleMT& doubleMT);
	CDoubleMT& operator=(double x);

	//! Addition
	CDoubleMT& operator+=(double x);

	//! Substraction
	CDoubleMT& operator-=(double x);

	//! Multiplication
	CDoubleMT& operator*=(double x);

	//! Division 
	CDoubleMT& operator/=(double x);

	//! Get doubleeger value
	operator double();	

private:
	double m_fValue;
	pthread_mutex_t m_Mutex;
};


#endif //EB_DOUBLE_MT_H_

