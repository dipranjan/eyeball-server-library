// MapCache.h
// Map-based cache implementation
//
// Deletes entries from cache based on time
// Expiry is checked at each GET and PUT 
//
// Intended for a map that is backed up by
// File I/O in applications where memory is
// limited but hard disk backup is available 
// 
// By Zion Kwok
// June 23, 2006
//
// (C) 2006 Eyeball Networks
////////////////////////////////////////////////

#ifndef EB_MAP_CACHE_H_
#define EB_MAP_CACHE_H_

#include <map>
#include <vector>

using std::map;
using std::vector;

#include <time.h>
#include "debug.h"

template <class KEY, class VALUE> class CMapCache
{
public:
  //! Constructor
  CMapCache(int iExpirySec = 3600);

  //! Destructor
  virtual ~CMapCache();

  //! Put
  void Put(const KEY& key, const VALUE& value);

  //! Get
  bool Get(const KEY& key, VALUE& value);

  //! Check Expiry
  void CheckExpiry(int iCurrTime);

  //! Pop Expired
  bool PopExpired(KEY& key, VALUE& value);

  //! Pop Oldest 
  bool PopOldest(KEY& key, VALUE& value);

  //! Get Size
  int Size() const;

  //! Is Empty
  bool IsEmpty() const;

  //! Clear
  void Clear();

private:
  //! Renew Expiry
  void RenewExpiry(int iIndex, int iCurrTime);

  //! Add Expiry
  void AddExpiry(const KEY& key, int iCurrTime);

  //! Remove Expiry
  void RemoveExpiry(int iIndex);

  struct keytime_t {
    KEY key;
    time_t timestamp;
    int iPrevIndex;
    int iNextIndex;
  }; 

  struct valueindex_t {
    VALUE value;
    int iVectorIndex;
  }; 

  int m_iExpirySec; 
  int m_iQueueFront;
  int m_iQueueBack;
  int m_iQueueSize;

  vector<keytime_t>* m_pVectorQueue;
  map<KEY, valueindex_t>* m_pMap;
};

template <class KEY, class VALUE> class CMapCacheMT : public CMapCacheMT<KEY, VALUE>
{
public:
  //! Constructor
  CMapCacheMT(int iExpirySec = 3600);

  //! Destructor
  virtual ~CMapCacheMT();

  //! Put
  void Put(const KEY& key, const VALUE& value);

  //! Get
  bool Get(const KEY& key, VALUE& value);

  //! Check Expiry
  void CheckExpiry();

  //! Pop Expired
  bool PopExpired(KEY& key, VALUE& value);

  //! Pop Oldest 
  bool PopOldest(KEY& key, VALUE& value);

  //! Get Size
  int Size() const;

  //! Is Empty
  bool IsEmpty() const;

  //! Clear
  void Clear();

private:
  pthread_mutex_t* m_pMutex;
};


//! Constructor
template <class KEY, class VALUE> CMapCache<KEY, VALUE>::CMapCache(int iExpirySec)
: m_iExpirySec(iExpirySec),
  m_pVectorQueue(new vector<keytime_t>),
  m_pMap(new map<KEY, valueindex_t>)
{
  DB_ENTER(CMapCache::CMapCache);
}

//! Destructor
template <class KEY, class VALUE> CMapCache<KEY, VALUE>::~CMapCache()
{
  DB_ENTER(CMapCache::~CMapCache);

  delete m_pMap;
  delete m_pVectorQueue;
}

//! Put
template <class KEY, class VALUE>
void CMapCache<KEY, VALUE>::Put(const KEY& key, const VALUE& value)
{
  DB_ENTER(CMapCache::Put);

  //Check for expired values
  int iCurrTime = 0;
  if (m_iExpirySec > 0) {
    iCurrTime = time(NULL);

    CheckExpiry(iCurrTime);
  }

  m_pMap->iterator = m_pMap->find(key);

  if (m_pMap->iterator != m_pMap->end()) {
    valueindex_t& valueindex = *(m_pMap->iterator);
    
    RenewExpiry(valueindex.iVectorIndex, iCurrTime);
    
    valueindex.value = value;
    valueindex.iVectorIndex = m_iQueueBack;
  }
  else {
    AddExpiry(key, iCurrTime);

    valueindex_t v;
    v.value = value;
    v.iVectorIndex = m_iQueueBack;
    (*m_pMap)[key] = v;
  }
}

//! Get 
template <class KEY, class VALUE> bool CMapCache<KEY, VALUE>::Get(const KEY& key, VALUE& value)
{
  DB_ENTER(CMapCache::Get);

  //Check for expired values
  int iCurrTime = 0;
  if (m_iExpirySec > 0) {
    iCurrTime = time(NULL);

    CheckExpiry(iCurrTime);
  }

  m_pMap->iterator = m_pMap->find(key);
  bool bRet = false;
  if (m_pMap->iterator != m_pMap->end()) {
    valueindex_t& valueindex = *(m_pMap->iterator);
    value = valueindex.value;

    RenewExpiry(valueindex.iVectorIndex, iCurrTime);
    valueindex.iVectorIndex = m_iQueueBack;
    bRet = true;
  }
  return bRet;
}

//! Check Expiry
template <class KEY, class VALUE> void CMapCache<KEY, VALUE>::CheckExpiry(int iCurrTime)
{
  DB_ENTER(CMapCache::CheckExpiry);

  //Check for expired values
  while (m_iQueueSize > 1) {
    keytime_t& keytime = (*m_pVectorQueue)[m_iQueueFront];
    if (m_iExpirySec > 0) {
      if ((keytime.timestamp + m_iExpirySec - iCurrTime) >= 0) break;
    } 

    //Remove from Map
    m_pMap->iterator = m_pMap->find(keytime.key);
    if (m_pMap->iterator != m_pMap->end()) {
      m_pMap->erase(m_pMap->iterator);
    }

    //Pop from queue
    RemoveExpiry(m_iQueueFront);
  } 
}

//! Pop Expired
template <class KEY, class VALUE> bool CMapCache<KEY, VALUE>::PopExpired(KEY& key, VALUE& value)
{
  DB_ENTER(CMapCache::PopExpired);

  bool bRet = false;

  if (m_iQueueSize > 1) {
    keytime_t& keytime = (*m_pVectorQueue)[m_iQueueFront];

    bool bPop = true;
    if (m_iExpirySec > 0) {
      int iCurrTime = time(NULL);
      if ((keytime.timestamp + m_iExpirySec - iCurrTime) >= 0) { 
        bPop = false;
      }
    }

    if (bPop) {
      //Remove from Map
      m_pMap->iterator = m_pMap->find(keytime.key);
      if (m_pMap->iterator != m_pMap->end()) {
        value = m_pMap->iterator->value;
        m_pMap->erase(m_pMap->iterator);
        key = keytime.key;
        bRet = true;
      }

      //Pop from queue
      RemoveExpiry(m_iQueueFront);
    }
  }

  return bRet;
}  

//! Pop Oldest 
template <class KEY, class VALUE> bool CMapCache<KEY, VALUE>::PopOldest(KEY& key, VALUE& value)
{
  DB_ENTER(CMapCache::PopOldest);

  bool bRet = false;

  if (m_iQueueSize > 1) {
    keytime_t& keytime = (*m_pVectorQueue)[m_iQueueFront];

    //Remove from Map
    m_pMap->iterator = m_pMap->find(keytime.key);
    if (m_pMap->iterator != m_pMap->end()) {
      value = m_pMap->iterator->value;
      m_pMap->erase(m_pMap->iterator);
      key = keytime.key;
      bRet = true;
    }

    //Pop from queue
    RemoveExpiry(m_iQueueFront);
  }

  return bRet;
}  

//! Get Size 
template <class KEY, class VALUE> int CMapCache<KEY, VALUE>::Size() const
{
  DB_ENTER(CMapCache::Size);

  return m_iQueueSize;
}

//! Is Empty
template <class KEY, class VALUE> bool CMapCache<KEY, VALUE>::IsEmpty() const
{
  DB_ENTER(CMapCache::IsEmpty);

  return (m_iQueueSize == 0);
}

//! Clear
template <class KEY, class VALUE> void CMapCache<KEY, VALUE>::Clear()
{
  DB_ENTER(CMapCache::Clear);

  m_iQueueFront = -1;
  m_iQueueBack = -1;
  m_iQueueSize = 0;
  m_pMap->clear();
  m_pVectorQueue->clear();
}

template <class KEY, class VALUE> void CMapCache<KEY, VALUE>::RenewExpiry(int iIndex, int iCurrTime)
{
  DB_ENTER(CMapCache::RenewExpiry);

  keytime_t& keytime = (*m_pVectorQueue)[iIndex];
  keytime.timestamp = iCurrTime;
  if (iIndex != m_iQueueBack) {
    //Re-order. First remove links
    if (keytime.iPrevIndex == -1) {
      m_iQueueFront = keytime.iNextIndex;
    }
    else {
      (*m_pVectorQueue)[keytime.iPrevIndex].iNextIndex = keytime.iNextIndex;
    }
    (*m_pVectorQueue)[keytime.iNextIndex].iPrevIndex = keytime.iPrevIndex;
    
    //Add to the back
    keytime.iPrevIndex = m_iQueueBack;
    keytime.iNextIndex = -1;
    m_iQueueBack = (*m_pVectorQueue)[m_iQueueBack].iNextIndex = iIndex;
  }
}

//! Add Expiry 
template <class KEY, class VALUE> void CMapCache<KEY, VALUE>::AddExpiry(const KEY& key, int iCurrTime)
{
  DB_ENTER(CMapCache::AddExpiry);

  //New Element
  keytime_t keytime;
  keytime.key = key;
  keytime.timestamp = iCurrTime;
  keytime.iNextIndex = -1;

  if (m_iQueueSize > 0) {
    keytime.iPrevIndex = m_iQueueBack;
    m_iQueueBack = (*m_pVectorQueue)[m_iQueueBack].iNextIndex = m_iQueueSize;
  }
  else {
    keytime.iPrevIndex = -1;
    m_iQueueBack = m_iQueueFront = m_iQueueSize;
  }
  m_iQueueSize++;
  m_pVectorQueue->push_back(keytime);
}

//! Remove Expiry 
template <class KEY, class VALUE> void CMapCache<KEY, VALUE>::RemoveExpiry(int iIndex)
{
  DB_ENTER(CMapCache::RemoveExpiry);

  keytime_t& keytime = (*m_pVectorQueue)[iIndex];

  if (keytime.iPrevIndex == -1) {
    m_iQueueFront = keytime.iNextIndex;
  }
  else {
    (*m_pVectorQueue)[keytime.iPrevIndex].iNextIndex = keytime.iNextIndex;
  }

  if (keytime.iNextIndex == -1) {
    m_iQueueBack = -1;
  }
  else {
    (*m_pVectorQueue)[keytime.iNextIndex].iPrevIndex == -1;
  }

  if ((iIndex + 1) < m_iQueueSize) {
    //Move last element to this position
    keytime = (*m_pVectorQueue)[m_iQueueSize - 1];

    if (keytime.iPrevIndex == -1) {
      m_iQueueFront = iIndex;
    }
    else {
      (*m_pVectorQueue)[keytime.iPrevIndex].iNextIndex = iIndex;
    }

    if (keytime.iNextIndex == -1) {
      m_iQueueBack = iIndex;
    }
    else {
      (*m_pVectorQueue)[keytime.iNextIndex].iPrevIndex = iIndex;
    }

    (*m_pMap)[keytime.key].iVectorIndex = iIndex;
  }

  m_pVectorQueue->resize(--m_iQueueSize);
}

//! Constructor
template <class KEY, class VALUE> CMapCacheMT<KEY, VALUE>::CMapCacheMT(int iExpirySec)
: CMapCache<KEY, VALUE>::CMapCache(iExpirySec),
  m_pMutex(new pthread_mutex_t)
{
  DB_ENTER(CMapCacheMT::CMapCacheMT);

  pthread_mutex_init(m_pMutex, NULL);
}

//! Destructor
template <class KEY, class VALUE> CMapCacheMT<KEY, VALUE>::~CMapCacheMT()
{
  DB_ENTER(CMapCacheMT::~CMapCacheMT);

  pthread_mutex_destroy(m_pMutex);
  delete m_pMutex;
}

//! Put
template <class KEY, class VALUE> void CMapCacheMT<KEY, VALUE>::Put(const KEY& key, const VALUE& value)
{
  DB_ENTER(CMapCacheMT::Put);

  pthread_mutex_lock(m_pMutex);
  CMapCache<KEY, VALUE>::Put(key, value);
  pthread_mutex_unlock(m_pMutex);
}

//! Get 
template <class KEY, class VALUE> bool CMapCacheMT<KEY, VALUE>::Get(const KEY& key, VALUE& value)
{
  DB_ENTER(CMapCacheMT::Get);

  pthread_mutex_lock(m_pMutex);
  bool bRet = CMapCache<KEY, VALUE>::Get(key, value);
  pthread_mutex_unlock(m_pMutex);

  return bRet;
}

//! Check Expiry
template <class KEY, class VALUE> void CMapCacheMT<KEY, VALUE>::CheckExpiry()
{
  DB_ENTER(CMapCacheMT::CheckExpiry);

  pthread_mutex_lock(m_pMutex);
  CMapCache<KEY, VALUE>::CheckExpiry(time(NULL));
  pthread_mutex_unlock(m_pMutex);
}

//! Pop Expired
template <class KEY, class VALUE> bool CMapCacheMT<KEY, VALUE>::PopExpired(KEY& key, VALUE& value)
{
  DB_ENTER(CMapCacheMT::PopExpired);

  pthread_mutex_lock(m_pMutex);
  bool bRet = CMapCache<KEY, VALUE>::PopExpired(key, value);
  pthread_mutex_unlock(m_pMutex);

  return bRet;
}

//! Pop Oldest 
template <class KEY, class VALUE> bool CMapCacheMT<KEY, VALUE>::PopOldest(KEY& key, VALUE& value)
{
  DB_ENTER(CMapCacheMT::PopOldest);

  pthread_mutex_lock(m_pMutex);
  bool bRet = CMapCache<KEY, VALUE>::PopOldest(key, value);
  pthread_mutex_unlock(m_pMutex);

  return bRet;
}

//! Get Size
template <class KEY, class VALUE> int CMapCacheMT<KEY, VALUE>::Size() const
{
  DB_ENTER(CMapCacheMT::Size);

  pthread_mutex_lock(m_pMutex);
  int iSize = CMapCache<KEY, VALUE>::Size();
  pthread_mutex_unlock(m_pMutex);

  return iSize;
}

//! Is Empty
template <class KEY, class VALUE> bool CMapCacheMT<KEY, VALUE>::IsEmpty() const
{
  DB_ENTER(CMapCacheMT::IsEmpty);

  pthread_mutex_lock(m_pMutex);
  bool bIsEmpty = CMapCache<KEY, VALUE>::IsEmpty();
  pthread_mutex_unlock(m_pMutex);

  return bIsEmpty;
}

//! Clear
template <class KEY, class VALUE> void CMapCacheMT<KEY, VALUE>::Clear()
{
  DB_ENTER(CMapCacheMT::Clear);

  pthread_mutex_lock(m_pMutex);
  CMapCache<KEY, VALUE>::Clear();
  pthread_mutex_unlock(m_pMutex);
}

#endif //EB_MAP_CACHE_H_
