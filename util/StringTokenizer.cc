// $Id: StringTokenizer.cc,v 1.1 2005/05/31 03:05:18 farhan Exp $

/** All files of the DNDS Prototype are Copyright (C) 1999 by Andre Schoorl
 *  unless otherwise noted.
 *
 *  The DNDS Prototype is distributed in the public domain in the hope that it
 *  will be useful, but is provided `as is' and is subject to change without
 *  notice.  No warranty of any kind is made with regard to the software or
 *  documentation.  The author shall not be liable for for any error for
 *  incidental or consequential damages in connection with the software and
 *  the documentation.
 *
 *  Permission to copy the whole, unmodified DNDS package is granted provided
 *  that the copies are not made or distributed for resale (excepting nominal
 *  copying fees).  All redistributions in either source or binary form must
 *  include this disclaimer.
 */

#include <stdio.h>

#ifdef WIN32
#include <windows.h>
#endif

#include "StringTokenizer.h"

#include "debug.h"

using std::string;


string ToSingleByteStr(const string& _str)
{
	string s8Text = "";
	for (string::size_type i = 0; i < _str.size(); i+=2)
		s8Text.push_back(_str[i]);
	return s8Text;
}

string ToMultiByteStr(const string& _str)
{
	string s16Text = "";
	for (string::size_type i = 0; i < _str.size(); i++)
	{
		s16Text.push_back(_str[i]);
		s16Text.push_back(0);
	}
	return s16Text;
}

bool lt_lowerstring::operator()(const std::string& s1, const std::string& s2) const
{
	// fast replacement for:
	//return tolower(s1) < tolower(s2);

	int s1_size = s1.size();
	int s2_size = s2.size();
	for (int i = 0; i < s1_size; i++)
	{
		if (i >= s2_size)
			return false;

		int s1_i = tolower((int)s1[i]);
		int s2_i = tolower((int)s2[i]);
		LT_CHECK(s1_i, s2_i);
	}

	return (s1_size < s2_size);
}

bool lt_lowertstring::operator()(const tstring& s1, const tstring& s2) const
{
	// fast replacement for:
	//return tolower(s1) < tolower(s2);

	int s1_size = s1.size();
	int s2_size = s2.size();
	for (int i = 0; i < s1_size; i++)
	{
		if (i >= s2_size)
			return false;

		int s1_i = tolower((int)s1[i]);
		int s2_i = tolower((int)s2[i]);
		LT_CHECK(s1_i, s2_i);
	}

	return (s1_size < s2_size);
}

string tolower(string _str)
{
	for (string::size_type i = 0; i < _str.size(); i++)
		_str[i] = tolower(_str[i]);

	return _str;
}

tstring tolower(tstring _str)
{
	for (tstring::size_type i = 0; i < _str.size(); i++)
		if( _str[i] < 256 )
			_str[i] = tolower(_str[i]);

	return _str;
}

bool compare_i(const string& _s1, const string& _s2)
{
	return (tolower(_s1) == tolower(_s2));
}

string trim_whitespace(const string& _str)
{
	const string ws( " \t\r\n" );

	string::size_type start = _str.find_first_not_of(ws);

	if (start == string::npos)
		return string();

	string::size_type end   = _str.find_last_not_of(ws);

	DB_ASSERT(end != string::npos);

	return _str.substr(start, end - start + 1);
}

tstring trim_whitespace(const tstring& _str)
{
	const tstring ws(tstring(" \t\r\n"));

	tstring::size_type start = _str.find_first_not_of(ws);

	if (start == string::npos)
		return string();

	string::size_type end   = _str.find_last_not_of(ws);

	DB_ASSERT(end != string::npos);

	return _str.substr(start, end - start + 1);
}

string skip_whitespace(const string& _str)
{
	string sNew("");
	for( string::size_type i = 0; i < _str.size(); i++ )
	{
		int c;
		switch( (c = _str[i]) )
		{
			case ' ':
			case '\t':
			case '\n':
			case '\r':
				continue;
		}
		sNew += c;
	}

	return sNew;
}

bool IsWhitespaceOnly(const string& _str)
{
	return trim_whitespace(_str).empty();
}

string escape_quotes(const string& _str)
{
	string rc(_str);

	for (string::size_type i=0; i < rc.length(); i++)
	{
		if ((rc[i] == '\'') || (rc[i] == '\"'))
		{
			// Character we need to encode
			string escaped = "\\";
			escaped += rc[i];

			rc.replace(i, 1, escaped);
			i += 1;
		}
	}

	return rc;
}

string itoa(const int _x)
{
	char buf[34];	// MSDN says max. of these functions is 33, so be safe.
	sprintf(buf, "%d", _x);
	return buf;
}

string utoa(const unsigned int _x)
{
	char buf[34];	// MSDN says max. of these functions is 33, so be safe.
	sprintf(buf, "%u", _x);
	return buf;
}

string itoh(const int _x)
{
	char buf[34];
	sprintf(buf, "%x", _x);
	return buf;
}

string utoh(const unsigned int _x)
{
	char buf[34];
	sprintf(buf, "%x", _x);
	return buf;
}

static bool IsEscapedChar(char ch)
{
	// letters
	if (isalpha(ch))
		return false;

	// numbers
	if ((ch >= '0') && (ch <= '9'))
		return false;

	// symbols
	switch (ch)
	{
	case '-':
	case '_':
	case '.':
	case '@':
		return false;
	}

	return true;
}

std::string escape_html(const std::string& str)
{
	string sEncoded;
	for (unsigned int i = 0; i < str.size(); i++)
	{
		if (IsEscapedChar(str[i]))
		{
			char buf[64];
			sprintf(buf, "%02x", str[i]);
			sEncoded += (string)"%" + buf;
		}
		else
		{
			// append normal character
			sEncoded += str[i];
		}
	}

	return sEncoded;
}

std::string unescape_html(const std::string& str)
{
	string sDecoded;
	for (unsigned int i = 0; i < str.size(); i++)
	{
		if (str[i] != '%')
		{
			// append normal character
			sDecoded += str[i];
			continue;
		}

		unsigned int n = 0;

		if ((str.size() < i + 3) ||
			(sscanf(str.c_str() + 1, "%02x", &n) != 1))
		{
			DB_ASSERT(0 && "invalid escaped string");
			sDecoded += '%';
			continue;
		}

		// advance the current string position
		i += 2;

		// append unescaped character
		sDecoded += (char)n;
	}

	return sDecoded;
}

std::string EncodeUcn(const tstring& sPlainText)
{
	std::string sRet;

	for (unsigned int i = 0; i < sPlainText.size(); i++)
	{
#ifdef UNICODE
		unsigned short ch = sPlainText[i];
		if (ch <= 0xff)
		{
			if ('\\' == ch)
			{
				// escape the backslash
				sRet += '\\';
			}

			// unencoded character
			sRet += (0xff & ch);
			continue;
		}
		
		char szCode[64];
		sprintf(szCode, "%4x", ch);

		sRet += "\\u";
		sRet += szCode;
#else
		if ('\\' == sPlainText[i])
		{
			sRet += '\\';
		}

		sRet += sPlainText[i];
#endif //UNICODE
	}

	return sRet;
}

tstring DecodeUcn(const std::string& sUcnText)
{
	tstring sRet;

	for (unsigned int i = 0; i < sUcnText.size(); i++)
	{
		if ('\\' != sUcnText[i])
		{
			// unencoded character
			sRet += (unsigned char)sUcnText[i];
			continue;
		}

		// read next character
		i++;
		if (i >= sUcnText.size())
			break; // error

#ifdef UNICODE
		if ('u' == sUcnText[i])
		{
			if (i + 4 >= sUcnText.size())
				break;  // error

			std::string sCode = sUcnText.substr(i + 1, 4);
			if (sCode.size() != 4)
				break;  // error

			unsigned int ch = 0;
			sscanf(sCode.c_str(), "%4x", &ch);

			sRet += ch;

			i += 4;

			continue;
		}
#endif // UNICODE

		// handle as unencoded character
		sRet += sUcnText[i];
	}

	return sRet;
}

string StringTokenizer::ltoa(const long _x)
{
	char buf[70];
	sprintf(buf, "%ld", _x);
//	printf("StringTokenizer.ltoa: converted %ld to %s\n", _x, buf);
	return buf;
}

StringTokenizer::StringTokenizer()
:
	str(),
	pos(0)
{
}

StringTokenizer::StringTokenizer(const string& _str)
:
	str(_str),
	pos(0)
{
}

void StringTokenizer::setString(const string& _str)
{
	str = _str;
	pos = 0;
}

bool StringTokenizer::nextToken(string& _token)
{
	string::size_type token_start(pos);

	bool within_token(false), within_quote(false);

	for (; pos < str.size(); pos++)
	{
		switch(str[pos])
		{
			// Comment - ignore everything after
			case '#':
				return false;
				break;

			// whitespace
			case ' ':
			case '\t':
			case '\n':
				if (within_quote)
					break;

				if (within_token)
				{
					_token = str.substr(token_start, pos-token_start);
					return true;
				}
				break;

			case '"':
				if (within_quote)
				{
					_token = str.substr(token_start, pos-token_start);
					pos++;
					return true;
				}

				token_start = pos+1;
				within_token = within_quote = true;
				break;

			default:
				if (!within_token)
				{
					token_start = pos;
					within_token = true;
				}
				break;
		}
	}

	// If we reach the end of line while still in a token, return it
	if (within_token)
	{
		_token = str.substr(token_start, pos-token_start);
		return true;
	}

	return false;
}

