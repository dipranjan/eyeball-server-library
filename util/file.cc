/** All files of the DNDS Prototype are Copyright (C) 1999 by Andre Schoorl
 *  unless otherwise noted.
 *
 *  The DNDS Prototype is distributed in the public domain in the hope that it
 *  will be useful, but is provided `as is' and is subject to change without
 *  notice.  No warranty of any kind is made with regard to the software or
 *  documentation.  The author shall not be liable for for any error for
 *  incidental or consequential damages in connection with the software and
 *  the documentation.
 *
 *  Permission to copy the whole, unmodified DNDS package is granted provided
 *  that the copies are not made or distributed for resale (excepting nominal
 *  copying fees).  All redistributions in either source or binary form must
 *  include this disclaimer.
 */

#include <assert.h>
#include <fcntl.h>			// open() - linux
#include <stdio.h>
#include <sys/types.h>
#include <sys/time.h>		// gettimeofday()
#include <sys/resource.h>	// getrlimit()
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>


#include "file.h"

#include "debug.h"
#include "retval.h"
#include "StringTokenizer.h"
#include "util.h"


using namespace std;


using namespace std;

// Maximum line length for calculating number of lines
#define MAX_LINE	1024

File::File(const string& _filename)
:
	filename(_filename),
	fp(NULL),
	is_pipe(false)
{
}

File::~File()
{
	if (fp)
	{
		if (is_pipe)
			pclose(fp);
		else
			fclose(fp);
	}
}

bool File::is_absolute(const string& _filename)
{
	string::const_iterator first_char(_filename.begin());

	// Handle case of empty string
	RFIT(first_char == _filename.end());

	return *first_char == '/';
}

bool File::is_regular(const string& _filename)
{
	struct stat buf;

	if (stat(_filename.c_str(), &buf) != 0)
	{
		// Error
		return 0;
	}

	// File exists, so now check its mode
	return S_ISREG(buf.st_mode);
}

bool File::is_dir(const string& _filename)
{
	struct stat buf;

	if (stat(_filename.c_str(), &buf) != 0)
	{
		// Error
		return 0;
	}

	// File exists, so now check its mod
	return S_ISDIR(buf.st_mode);
}

bool File::remove(const string& _filename)
{
	return unlink(_filename.c_str()) == 0;
}

bool File::rename(const string& _source, const string& _dest)
{
	return ::rename(_source.c_str(), _dest.c_str()) == 0;
}

int File::num_bytes(const string& _filename)
{
	struct stat buf;

	if (stat(_filename.c_str(), &buf) != 0)
	{
		return -1;
	}

	// Total size, in bytes
	return buf.st_size;
}

int File::num_lines(const string& _filename)
{
	char buf[MAX_LINE];
	int i = 0;

	File file(_filename);
	ASSERT(file.open_read());

	while(fgets(buf, MAX_LINE, file.get_fp()))
	{
		i++;
	}

	return i;
}

string File::get_temporary_name(const string& _prefix)
{
	struct timeval tv;
	ASSERT(gettimeofday(&tv, NULL) == 0);
	srand(tv.tv_usec);

	char rand_buf[128];
	string filename;

	do
	{
#ifdef __linux__
	  sprintf(rand_buf, "%ld", random());
#else
		sprintf(rand_buf, "%ld", random());
#endif
		filename = _prefix + rand_buf;
	} while (File::is_regular(filename));

	return filename;
}

long File::set_max_fds(void)
{
#if defined(linux)
	// Linux kernel (2.2.14) has a resizing array for the fds.  It starts
	// sized at 32 and a poll of more than 32 fds fails.  Opening max fds
	// pushes the size out to it's maximum (1024).
	int fds[70000];
	memset(fds, 0, sizeof(fds));
	for (int i = 0; i < 70000; i++)
	{
		int fd = open("/dev/null", O_RDONLY);
		if (fd == -1)
		{
			fds[i] = -1;
			break;
		}
		fds[i] = fd;
	}

	for (int i = 0; i < 70000; i++)
		if (fds[i] > 0)
			close(fds[i]);

	//DB("Because this is linux, I've had to push the size of the kernel fd array to its max.");
#endif

	rlimit limit;
	getrlimit(RLIMIT_NOFILE, &limit);
	limit.rlim_cur = limit.rlim_max;
//	DB_STAT(limit.rlim_cur);
//	DB_STAT(limit.rlim_max);
	int ret = setrlimit(RLIMIT_NOFILE, &limit);
	if (ret != 0)
	{
		// How could it be that I can't set the limit to the maximum?
		ASSERT(0 && "Cannot setrlimit");
	} 

	return limit.rlim_max;
}

long File::get_max_files(long lTryAtMost)
{
//	DB_ENTER(File::get_max_files);

	long FDcount, i, f1, maxFD;
	long* Array;
	rlimit limit;

	// get current theoretical maximal fd
	getrlimit(RLIMIT_NOFILE, &limit);
	maxFD = limit.rlim_cur;
	if (lTryAtMost > maxFD) 
		lTryAtMost = maxFD;

	FDcount = 0;
	// alloc array, to remember which were opened
	Array = new long[lTryAtMost];
	if (NULL == Array) 
		return -1;

	for (i=0; i<lTryAtMost; i++)
	{
		f1 = open("/dev/null", O_RDONLY);
		//f1 = socket(AF_INET, SOCK_STREAM, 0);
		if (f1 < 0)
			continue;   // could not open!
		// could open, count & remember
		Array[FDcount] = f1;
		FDcount++;
	}
	// do the closes
	for (i=0; i<FDcount; i++)
		close(Array[i]);

/*
	if (FDcount < lTryAtMost)
	{
		DB("There are only " << FDcount << " unique file descriptors, "
			"instead of "<< lTryAtMost);
	}
*/

	delete[] Array;

	return FDcount;
//	return MIN(5000, FDcount);
}

vector<string> File::read_tokens_from_file(const string& _filename)
{
	//DB_ENTER(File::read_tokens_from_file);

	vector<string> rc;

	File file(_filename);
	if (!file.open_read())
		return rc;

	StringTokenizer st;
	string token, line;

	while (file.read_line(line))
	{
		st.setString(line);

		while (st.nextToken(token))
			rc.push_back(token);
	}

	return rc;
}

bool File::open_append(void)
{
	is_pipe = false;
	return (fp = fopen(filename.c_str(), "a")) != NULL;
}

bool File::open_read(void)
{
	is_pipe = false;
	return (fp = fopen(filename.c_str(), "r")) != NULL;
}

bool File::open_write(void)
{
	is_pipe = false;
	return (fp = fopen(filename.c_str(), "w")) != NULL;
}

bool File::open_pipe_read(void)
{
	is_pipe = true;
	return (fp = popen(filename.c_str(), "r")) != NULL;
}

bool File::open_pipe_write(void)
{
	is_pipe = true;
	return (fp = popen(filename.c_str(), "w")) != NULL;
}

bool File::append_bytes(char *_val, int _len)
{
	ASSERT(fp);

	return (int)fwrite(_val, 1, _len, fp) == _len;
}

bool File::read_bytes(char*& _val, int& _len)
{
	ASSERT(fp);

	int len;
	if ((len = num_bytes(filename)) == -1)
		return false;

	_val = new char[len];
	_len = len;

	ASSERT((int)fread(_val, 1, _len, fp) == _len);

	return true;
}

bool File::read_line(string& _line)
{
	ASSERT(fp);

	char buf[MAX_LINE];
	if (!fgets(buf, MAX_LINE, fp))
		return false;

	// Get rid of newline
	buf[strlen(buf)-1] = '\0';

	_line = buf;
	return true;
}

DIR *Directory::read(const string& _filename)
{
	DIR *dirp;

	dirp = opendir(_filename.c_str());
	if (!dirp)
	{
		cerr << "Error: Could not read from " << _filename << endl;
		exit(1);
	}

	return dirp;
}

int Directory::num_entries(const string& _filename)
{
	DIR *dirp;
	struct dirent *dp;
	int num_entries;

	dirp = opendir(_filename.c_str());
	if (!dirp)
		return 0;

	num_entries = 0;
	while ((dp = readdir(dirp)) != NULL)
		num_entries++;

	if (closedir(dirp) != 0)
		return 0;

	// Expect to at least have entries for "." and ".."
	if (num_entries <= 2)
		return 0;

	return num_entries;
}

bool File::seek(long _offset)
{
	ASSERT(fp);
	return fseek(fp, _offset, SEEK_SET) == 0;
}

