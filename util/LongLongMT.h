// LongLongMT.h
// Interface for the thread-safe 64-bit long longeger class
//
// Zion Kwok
// July 26, 2006
//
// (C) Eyeball Networks
//////////////////////////////////////////////

#ifndef EB_LONG_LONG_MT_H_
#define EB_LONG_LONG_MT_H_

#include <pthread.h>

class CLongLongMT 
{
public:
	//! Default Constructor
	CLongLongMT();

	//! Copy Constructor
	CLongLongMT(CLongLongMT& longlongMT);

	//! Constructor
	CLongLongMT(long long value);

	//! Destructor
	~CLongLongMT();

	//! Assignment operator
	CLongLongMT& operator=(CLongLongMT& longlongMT);
	CLongLongMT& operator=(long long x);

	//! Addition
	CLongLongMT& operator+=(long long x);

	//! Substraction
	CLongLongMT& operator-=(long long x);

	//! Multiplication
	CLongLongMT& operator*=(long long x);

	//! Division 
	CLongLongMT& operator/=(long long x);

	//! Modulo 
	CLongLongMT& operator%=(long long x);

	//! Prefix Increment
	CLongLongMT& operator++();

	//! Prefix Decrement
	CLongLongMT& operator--();

	//! Shift right
	CLongLongMT& operator>>=(long long x);

	//! Shift left
	CLongLongMT& operator<<=(long long x);

	//! And
	CLongLongMT& operator&=(long long x);

	//! Or 
	CLongLongMT& operator|=(long long x);

	//! Xor 
	CLongLongMT& operator^=(long long x);

	//! Get long longeger value
	operator long long();	

private:
	//! Postfix Increment -- very inefficient
	CLongLongMT operator++(int);

	//! Postfix Decrement -- very inefficient
	CLongLongMT operator--(int);

	long long m_iValue;
	pthread_mutex_t m_Mutex;
};


#endif //EB_LONG_LONG_MT_H_

