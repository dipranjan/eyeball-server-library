// ---------------------------------------------------------------------------
// File:       TextUtil.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2002
//
// Purpose:    text-related utilities
//
// Change Log:
// ---------------------------------------------------------------------------

#include "Base.h"  // automatic use of precompiled headers 

#include <stdio.h>

#include "debug.h"
#include "TextUtil.h"

using std::string;

std::string itoa(const int iX)
{
	char buf[34];	// MSDN says max. of these functions is 33, so be safe.
	sprintf(buf, "%d", iX);

	return buf;
}

string tolower(string _str)
{
	for (string::size_type i = 0; i < _str.size(); i++)
        _str[i] = tolower(_str[i]);

	return _str;
}

string toupper(string _str)
{
	for (string::size_type i = 0; i < _str.size(); i++)
        _str[i] = toupper(_str[i]);

	return _str;
}

#if 0
bool lt_lowerstring::operator()(const std::string& s1, const std::string& s2) const
{
	// fast replacement for:
	//return tolower(s1) < tolower(s2);

	int s1_size = s1.size();
	int s2_size = s2.size();
	for (int i = 0; i < s1_size; i++)
	{
		if (i >= s2_size)
			return false;

		int temp1 = s1[i];
		int temp2 = s2[i];

		int s1_i = tolower(temp1);
		int s2_i = tolower(temp2);
		LT_CHECK(s1_i, s2_i);
	}

	return (s1_size < s2_size);
}
	
bool compare_i(const string& _s1, const string& _s2)
{
	return (tolower(_s1) == tolower(_s2));
}

tstring trim_whitespace(const tstring& _str)
{
	const tstring ws(tstring(" \t\r"));

	unsigned int start = _str.find_first_not_of(ws);

	if (start == string::npos)
		return string();

	unsigned int end = _str.find_last_not_of(ws);

	DB_ASSERT(end != string::npos);

	return _str.substr(start, end - start + 1);
}

string escape_quotes(const string& _str)
{
	string rc(_str);

	for (string::size_type i=0; i < rc.length(); i++)
	{
		if ((rc[i] == '\'') || (rc[i] == '\"'))
		{
			// Character we need to encode
			string escaped = "\\";
			escaped += rc[i];

			rc.replace(i, 1, escaped);
			i += 1;
		}
	}

	return rc;
}



static bool IsEscapedChar(char ch)
{
	// letters
	if (isalpha(ch))
		return false;

	// numbers
	if ((ch >= '0') && (ch <= '9'))
		return false;

	// symbols
	switch (ch)
	{
	case '-':
	case '_':
	case '.':
	case '@':
		return false;
	}

	return true;
}


std::string escape(const std::string& str)
{
	string sEncoded;
	for (unsigned int i = 0; i < str.size(); i++)
	{
		if (IsEscapedChar(str[i]))
		{
			char buf[64];
			sprintf(buf, "%02x", str[i]);
			sEncoded += (string)"%" + buf;
		}
		else
		{
			// append normal character
			sEncoded += str[i];
		}
	}

	return sEncoded;
}

static bool IsInvalidFileNameChar(TCHAR ch)
{
	// symbols that cannot be part of a filename
	switch (ch)
	{
	case '\\':
	case '/':
	case ':':
	case '*':
	case '?':
	case '"':
	case '<':
	case '>':
	case '|':
		return true;
	}

	return false;
}

tstring escape_filename(const tstring& str)
{
	tstring sEncoded;
	for (unsigned int i = 0; i < str.size(); i++)
	{
		if (IsInvalidFileNameChar(str[i]))
		{
			char buf[64];
			sprintf(buf, "%02x", str[i]);
			sEncoded += (tstring)"%" + tstring(buf);
		}
		else
		{
			// append normal character
			sEncoded += str[i];
		}
	}

	return sEncoded;
}

std::string unescape(const std::string& str)
{
	string sDecoded;
	for (unsigned int i = 0; i < str.size(); i++)
	{
		if (str[i] != '%')
		{
			// append normal character
			sDecoded += str[i];
			continue;
		}

		unsigned int n = 0;

		if ((str.size() < i + 3) ||
			(sscanf(&str[i + 1], "%02x", &n) != 1))
		{
			DB_ASSERT(0 && "invalid escaped string");
			sDecoded += '%';
			continue;
		}

		// advance the current string position
		i += 2;

		// append unescaped character
		sDecoded += (char)n;
	}

	return sDecoded;
}

bool FindAndReplace(
	tstring& sBuf, 
	const tstring& sBefore,
	const tstring& sAfter)
{
	unsigned int pos = sBuf.find(sBefore);
	if (tstring::npos == pos)
		return false;

	sBuf.replace(pos, sBefore.length(), sAfter);

	return true;
}

// StringTokenizer class implementation
StringTokenizer::StringTokenizer()
:
	str(),
	pos(0)
{
}

StringTokenizer::StringTokenizer(const string& _str)
:
	str(_str),
	pos(0)
{
}

void StringTokenizer::setString(const string& _str)
{
	str = _str;
	pos = 0;
}

bool StringTokenizer::nextToken(string& _token)
{
	string::size_type token_start(pos);

	bool within_token(false), within_quote(false);

	for (; pos < str.size(); pos++)
	{
		switch (str[pos])
		{
			// Comment - ignore everything after
			case '#':
				return false;
				break;

			// whitespace
			case ' ':
			case '\t':
			case '\n':
				if (within_quote)
					break;

				if (within_token)
				{
					_token = str.substr(token_start, pos-token_start);
					return true;
				}
				break;

			case '"':
				if (within_quote)
				{
					_token = str.substr(token_start, pos-token_start);
					pos++;
					return true;
				}

				token_start = pos+1;
				within_token = within_quote = true;
				break;

			default:
				if (!within_token)
				{
					token_start = pos;
					within_token = true;
				}
				break;
		}
	}

	// If we reach the end of line while still in a token, return it
	if (within_token)
	{
		_token = str.substr(token_start, pos-token_start);
		return true;
	}

	return false;
}

// ---------------------------------------------------------------------------
// Constants and Definitions
// ---------------------------------------------------------------------------

const struct 
{
	const char* szChar;
	const char* szEscape;
} g_HtmlEscape[] = 
{
	{"<", "&lt;"},
	{">", "&gt;"}
};

const struct 
{
	const char* szChar;
	const char* szEscape;
} g_HtmlUnescape[] = 
{
	{"\"", "&quot;"},
	{"&", "&amp;"},
	{"<", "&lt;"},
	{">", "&gt;"},
	{" ", "&nbsp;"},
	{"�", "&iexcl;"},
	{"�", "&cent;"},
	{"�", "&pound;"},
	{"�", "&curren;"},
	{"�", "&yen;"},
	{"�", "&brvbar;"},
	{"�", "&brkbar;"},
	{"�", "&sect;"},
	{"�", "&copy;"},
	{"�", "&ordf;"},
	{"�", "&laquo;"},
	{"�", "&not;"},
	{"�", "&reg;"},
	{"�", "&macr;"},
	{"�", "&hibar;"},
	{"�", "&deg;"},
	{"�", "&plusmn;"},
	{"�", "&sup2;"},
	{"�", "&sup3;"},
	{"�", "&acute;"},
	{"�", "&micro;"},
	{"�", "&para;"},
	{"�", "&middot;"},
	{"�", "&cedil;"},
	{"�", "&sup1;"},
	{"�", "&ordm;"},
	{"�", "&raquo;"},
	{"�", "&frac14;"},
	{"�", "&frac12;"},
	{"�", "&frac34;"},
	{"�", "&iquest;"},
	{"�", "&times;"}/*,
	{"�", "&divide;"},
	{"�", "&oslash;"}*/
};


// ---------------------------------------------------------------------------
// Helper functions
// ---------------------------------------------------------------------------

tstring EscapeHtmlText(const tstring& sTextIn)
{
    tstring sText = sTextIn;
    tstring::size_type posNext = 0;
    for (posNext = 0; posNext < sText.length(); posNext++)
    {
        for (int i = 0; i < sizeof(g_HtmlEscape) / sizeof(g_HtmlEscape[0]); i++)
        {
            tstring::size_type posNext2 = 0;
            if ((posNext2 = sText.find(tstring(g_HtmlEscape[i].szChar), posNext)) == posNext)
            {
                sText.replace(
					posNext, 
					strlen(g_HtmlEscape[i].szChar), 
					tstring(g_HtmlEscape[i].szEscape));
                posNext += strlen(g_HtmlEscape[i].szEscape) - strlen(g_HtmlEscape[i].szChar);
                break;
            }
        }
    }
    return sText;
}

tstring UnescapeHtmlText(const tstring& sTextIn)
{
    tstring sText = sTextIn;
    for (int i = 0; i < sizeof(g_HtmlUnescape) / sizeof(g_HtmlUnescape[0]); i++)
    {
        tstring::size_type posNext;
        while((posNext = sText.find(tstring(g_HtmlUnescape[i].szEscape))) != tstring::npos)
        {
            sText.replace(
				posNext, 
				strlen(g_HtmlUnescape[i].szEscape), 
				tstring(g_HtmlUnescape[i].szChar));
        }
    }

    return sText;
}

bool IsHtmlTag(const tstring& sHtmlTag)
{
	const struct 
	{
		const char* szTagName;
	} aHtmlTags[] = 
	{
		{"b"},
		{"i"},
		{"u"},
		{"html"},
		{"body"},
		{"font"},
		{"strong"},
		{"em"}
	};

    for (int i = 0; i < sizeof(aHtmlTags) / sizeof(aHtmlTags[0]); i++)
    {
        if (sHtmlTag == tstring(aHtmlTags[i].szTagName))
        {
            return true;
        }
    }

    return false;
}

void ReplaceHtmlSpace(tstring& sTextIn)
{
	while (true)
	{
		int iPos = sTextIn.find(tstring("%20"));
		if (iPos == tstring::npos)
			return;

		sTextIn.replace(iPos, 3, tstring(" "));
	}
}

	
tstring Html2Text(const tstring& sTextIn)
{
    tstring sText;
	tstring sTextOut;

    sText = sTextIn;
    while(sText.length() > 0)
    {
        // Trim the white spaces...
        tstring::size_type posNext;
        
        posNext = sText.find_first_of(tstring("<"));
        if(posNext != 0)
        {
            tstring sOutText;
            if(posNext == tstring::npos)
            {
                sOutText = sText;
                sText = tstring("");
            }
            else
            {
                sOutText = sText.substr(0, posNext);
                sText = sText.substr(posNext);
            }
            sTextOut += UnescapeHtmlText(sOutText);
        }
        else
        {
            tstring sFormatText;
			tstring sFormatTextIn;
			tstring sHtmlTag;
            bool bEndTag;

            // Separate the tag text from the main string
            tstring::size_type posEnd;
            posEnd = sText.find_first_of(tstring(">"), 1);
            if(posEnd == string::npos)
            {
                sFormatTextIn = sText.substr(0);
                sFormatText = tstring("");
                sText = tstring("");
            }
            else
            {
                tstring::size_type posNext2 = sText.find_first_of(tstring("<"), 1);
                if(posNext2 < posEnd && posNext2 != tstring::npos)
                {
                    sFormatTextIn = sText.substr(0, posNext2);
                    sFormatText = tstring("");
                    sText = sText.substr(posNext2);
                }
                else
                {
                    sFormatTextIn = sText.substr(0, posEnd+1);
                    sFormatText = sText.substr(1, posEnd-1);
                    sText = sText.substr(posEnd+1);
                }
            }

            // separate the tag name
            sFormatText = tolower(sFormatText.mbs());
            sFormatText = trim_whitespace(sFormatText).mbs();

            posNext = sFormatText.find_first_of(tstring(" \t\n\r"));
            if(posNext == tstring::npos)
            {
                sHtmlTag = sFormatText;
                sFormatText = "";
            }
            else
            {
                sHtmlTag = sFormatText.substr(0, posNext);
                sFormatText = sFormatText.substr(posNext + 1);
            }


            bEndTag = false;
            if(sHtmlTag.length() > 0 && sHtmlTag.at(0) == '/')
            {
                bEndTag = true;
                sHtmlTag = sHtmlTag.substr(1);
            }

            if(!IsHtmlTag(sHtmlTag))
            {
                sTextOut += sFormatTextIn;
            }
        }
    }

	ReplaceHtmlSpace(sTextOut);

    return sTextOut;
}

string ToByteArray(const tstring& sIn)
{
	if (sIn.empty())
		return "";

	return string((char*)sIn.c_str(), sIn.size() * sizeof(TCHAR));
}

tstring FromByteArray(const std::string& sIn)
{
	if (sIn.size() < sizeof(TCHAR))
		return tstring("");

	return tstring("");

//	return ustring((TCHAR*)sIn.c_str(), sIn.size() / sizeof(TCHAR));
}

std::string EncodeUcn(const tstring& sPlainText)
{
	std::string sRet;

	for (unsigned int i = 0; i < sPlainText.size(); i++)
	{
#ifdef _UNICODE
		unsigned short ch = sPlainText[i];
		if (ch <= 0xff)
		{
			if ('\\' == ch)
			{
				// escape the backslash
				sRet += '\\';
			}

			// unencoded character
			sRet += (0xff & ch);
			continue;
		}
		
		char szCode[64];
		sprintf(szCode, "%4x", ch);

		sRet += "\\u";
		sRet += szCode;
#else
		if ('\\' == sPlainText[i])
		{
			sRet += '\\';
		}

		sRet += sPlainText[i];
#endif //_UNICODE
	}

	return sRet;
}

tstring DecodeUcn(const std::string& sUcnText)
{
	tstring sRet;

	for (unsigned int i = 0; i < sUcnText.size(); i++)
	{
		if ('\\' != sUcnText[i])
		{
			// unencoded character
			sRet += (unsigned char)sUcnText[i];
			continue;
		}

		// read next character
		i++;
		if (i >= sUcnText.size())
			break; // error

#ifdef _UNICODE
		if ('u' == sUcnText[i])
		{
			if (i + 4 >= sUcnText.size())
				break;  // error

			std::string sCode = sUcnText.substr(i + 1, 4);
			if (sCode.size() != 4)
				break;  // error

			unsigned int ch = 0;
			sscanf(sCode.c_str(), "%4x", &ch);

			sRet += (TCHAR)ch;

			i += 4;

			continue;
		}
#endif // _UNICODE

		// handle as unencoded character
		sRet += sUcnText[i];
	}

	return sRet;
}

// Message Box Functions
int WindowsMessageBoxEx(const tstring& sText, UINT uType)
{
	//OutputDebugString(("MsgBox: " + _sText + "\n").c_str());

	return 0;
#if 0
		MessageBox(
		NULL, 
		sText.c_str(), 
		NULL, 
		uType);
#endif
}

void WindowsMessageBox(const tstring& sText)
{
    WindowsMessageBoxEx(sText, MB_ICONEXCLAMATION | MB_OK);
}

APPLICATION_SPECIFIC_MSG_BOX *g_pCustomMsgBox = WindowsMessageBox;
APPLICATION_SPECIFIC_MSG_BOX *g_pCustomBlockingMsgBox = WindowsMessageBox;
#endif
