// ---------------------------------------------------------------------------
// File:       registry.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2002
//
// Purpose:    Registry access functions
// 
// Change Log: Add global array: 30 May 2000
// ---------------------------------------------------------------------------

#include "Base.h"  // automatic use of precompiled headers 

#pragma warning(disable:4786) // Disable length warnings from STL

//#include <string>

//#include <map>
using std::map;

#include "debug.h"
#include "registry.h"
#include "ReturnCodes.h"
//#include "SmartPtr.h"
#include "TextUtil.h"

// ---------------------------------------------------------------------------
// Global variables
// ---------------------------------------------------------------------------

static tstring s_sCompany = tstring("Eyeball");
#ifdef PRO_VERSION
static tstring s_sAppName = tstring("Communicator");
#else
static tstring s_sAppName = tstring("Chat");
#endif //PRO_VERSION
static tstring s_sVersion = tstring("2.0");


// ---------------------------------------------------------------------------
// Defines
// ---------------------------------------------------------------------------

#define APP_TITLE() (s_sCompany + tstring(" ") + s_sAppName)
#define REG_BASE() (tstring("Software\\") + s_sCompany + tstring("\\") + s_sAppName + tstring("\\") + s_sVersion)

#define TOP_DIR() REG_BASE()

#define SUB_DIR(X) (REG_BASE() + tstring("\\") + tstring(X))

#define AUD_DIR()    SUB_DIR("Audio")
#define DSP_DIR()    SUB_DIR("Display")
#define VID_DIR()    SUB_DIR("Video")
#define WND_DIR()    SUB_DIR("Windows")
#define SKN_DIR()    SUB_DIR("Skins")
#define ALERT_DIR()  SUB_DIR("Alerts")
#define NET_DIR()    SUB_DIR("Network")
#define FTR_DIR()    SUB_DIR("FileTransfer")
#define PVC_DIR()	 SUB_DIR("Privacy")

//#define CREATE_HTTP_DEBUG_LOG

#ifdef CREATE_HTTP_DEBUG_LOG
#define DEBUG_LOG_DIR() tstring("Software\\Eyeball\\DebugLog")
#endif

#define RUN_DIR()  tstring("Software\\Microsoft\\Windows\\CurrentVersion\\Run")

#define SND_DIR() (tstring("AppEvents\\Schemes\\Apps\\") + s_sCompany + s_sAppName)

#define SND_CRT() "\\.Current"
#define SND_DEF() "\\.Default"

#define MAKE_SND_KEY(X) SND_DIR() + "\\" + X + SND_CRT()

struct
{
    EcEventType  eventType;
    DWORD        eventFlags;
    const TCHAR* szEventName;
    const TCHAR* szDefaultSound;
    BOOL         bDefaultTbAlert;
} g_arEvents[] = 
{
#ifdef PRO_VERSION
    { EC_EVENT_VIDEO_CHAT_REQUEST,  ECEF_SOUND,
        tstring("Received Video Call"),       tstring("video.wav"), 1},
#else
    { EC_EVENT_VIDEO_CHAT_REQUEST,  ECEF_SOUND,
        tstring("Received Video Chat Request"),       tstring("video.wav"), 1},
#endif // PRO_VERSION

    { EC_EVENT_RECEIVE_TEXT,        ECEF_SOUND | ECEF_ALERT | ECEF_TBALERT,
        tstring("Received Text Message"),             tstring("alert.wav"), 1},

    { EC_EVENT_RECEIVE_FILE,        ECEF_SOUND,
        tstring("Received File"),             tstring("alert.wav"), 1},

    { EC_EVENT_RECEIVE_VIDEO,       ECEF_SOUND,
        tstring("Received Video Message"),    tstring("alert.wav"), 1},

    { EC_EVENT_USER_ONLINE,         ECEF_SOUND | ECEF_ALERT | ECEF_TBALERT,
        tstring("User comes Online"),         tstring("contact.wav"), 1},

    { EC_EVENT_USER_ADD_CONTACT,    ECEF_ALERT | ECEF_TBALERT,
        tstring("User adds me to their Contact List"), tstring(""), 1},

#ifdef PRO_VERSION
    { EC_EVENT_USER_ENTER_CHAT,     ECEF_ALERT,
        tstring("User enters Video Call"), tstring(""), 1},
#else
    { EC_EVENT_USER_ENTER_CHAT,     ECEF_ALERT,
        tstring("User starts Video Chat"), tstring(""), 1},
#endif // PRO_VERSION

#ifdef PRO_VERSION
    { EC_EVENT_USER_LEAVE_CHAT, ECEF_ALERT,
        tstring("User exits Video Call"), tstring(""), 1},
#else
    { EC_EVENT_USER_LEAVE_CHAT, ECEF_ALERT,
        tstring("User ends Video Chat"), tstring(""), 1},
#endif // PRO_VERSION

    { EC_EVENT_ASK_DISCONNECT,      ECEF_ALERT | ECEF_HIDE,
        tstring("Disconnect from Service"),  tstring(""), 1},

#ifdef PRO_VERSION
    { EC_EVENT_EXIT_CHAT,           ECEF_ALERT | ECEF_HIDE,
        tstring("Confirm Exit Call"),        tstring(""), 1},
#else
    { EC_EVENT_EXIT_CHAT,           ECEF_ALERT | ECEF_HIDE,
        tstring("Confirm Exit Chat"),        tstring(""), 1},
#endif

    { EC_EVENT_RECEIVE_EMAIL_AIM,   ECEF_ALERT | ECEF_HIDE,
        tstring("Receive AOL Email"),        tstring(""), 1},

    { EC_EVENT_RECEIVE_EMAIL_YAHOO, ECEF_ALERT | ECEF_HIDE,
        tstring("Receive Yahoo! Email"),     tstring(""), 1},

    { EC_EVENT_RECEIVE_EMAIL_MSN,   ECEF_ALERT | ECEF_HIDE,
        tstring("Receive MSN Email"),        tstring(""), 1},

    { EC_EVENT_CANCEL_FILE,         ECEF_ALERT,
        tstring("File Transfer is cancelled"),     tstring(""), 1}
};


// ---------------------------------------------------------------------------
// Type definitions
// ---------------------------------------------------------------------------

struct RegistryEntry
{
	RegistryEntry() : 
		hkey(HKEY_LOCAL_MACHINE),
		basekey(), 
		subkey(), 
		str_val(), 
		num_val(0), 
		is_num(TRUE) 
	{
	}

	HKEY hkey;
	tstring basekey;
	tstring subkey;
	tstring str_val;
	DWORD num_val;
	BOOL is_num;
	BOOL auto_create;
};

typedef map<EcRegistryItem, RegistryEntry> KeyMap;


// ---------------------------------------------------------------------------
// Globals
// ---------------------------------------------------------------------------

static KeyMap g_KeyMap;
static BOOL g_bDisplayedAdminError(FALSE);

// ---------------------------------------------------------------------------
// Macros
// ---------------------------------------------------------------------------

#define BEGIN_HKEY(X) \
	{ HKEY hkey = (X)

#define END_HKEY() \
	}

#define INSERT_NUM_EX(KEY, BASEKEY, SUBKEY, DEFAULT, AUTO_CREATE) \
	{ \
		RegistryEntry x; \
		x.hkey    = hkey; \
		x.basekey = BASEKEY; \
		x.subkey  = SUBKEY; \
		x.num_val = DEFAULT; \
		x.is_num  = TRUE; \
		x.auto_create  = AUTO_CREATE; \
		g_KeyMap.insert(KeyMap::value_type(KEY, x)); \
	}

#define INSERT_STR_EX(KEY, BASEKEY, SUBKEY, DEFAULT, AUTO_CREATE) \
	{ \
		RegistryEntry x; \
		x.hkey    = hkey; \
		x.basekey = BASEKEY; \
		x.subkey  = SUBKEY; \
		x.str_val = DEFAULT; \
		x.is_num  = FALSE; \
		x.auto_create  = AUTO_CREATE; \
		g_KeyMap.insert(KeyMap::value_type(KEY, x)); \
	}

#define INSERT_NUM(KEY, BASEKEY, SUBKEY, DEFAULT) INSERT_NUM_EX(KEY, BASEKEY, SUBKEY, DEFAULT, TRUE)
#define INSERT_STR(KEY, BASEKEY, SUBKEY, DEFAULT) INSERT_STR_EX(KEY, BASEKEY, SUBKEY, DEFAULT, TRUE)


// ---------------------------------------------------------------------------
// Internal helper functions
// ---------------------------------------------------------------------------

// User-specific entries
static void InitializeUserKeys(void)
{
	BEGIN_HKEY(HKEY_CURRENT_USER);

	// Top-level
	INSERT_STR(REG_MY_EYEBALL_ID,          TOP_DIR(), "MyEyeballID",                     "");
	INSERT_STR(REG_PASSWORD,               TOP_DIR(), "Password",                        "");
	INSERT_NUM(REG_SAVE_PASSWORD,          TOP_DIR(), "SavePassword",                     0);
	INSERT_NUM(REG_AUTO_LOGIN,             TOP_DIR(), "AutoLogin",                        1);
	INSERT_NUM(REG_RECENT_IM,              TOP_DIR(), "RecentIm",                         0);
	INSERT_STR(REG_RECENT_EYEBALL_ID,      TOP_DIR(), "RecentEyeballID",                 "");
	INSERT_STR(REG_RECENT_SESSION,         TOP_DIR(), "RecentSession",                   "");

#ifdef PRO_VERSION
	INSERT_NUM(REG_CLIENT_TYPE,            TOP_DIR(), "ClientType",                  0x3f0b);
#else
	INSERT_NUM(REG_CLIENT_TYPE,            TOP_DIR(), "ClientType",                  0x3e08);
#endif //PRO_VERSION

	// Privacy
	INSERT_NUM(REG_AUTO_ACCEPT_CALL,       PVC_DIR(), "AutoAcceptCalls",                  0);
	INSERT_NUM(REG_AUTO_ADD_CONTACT,       PVC_DIR(), "AutoAddContact",                   0);
	INSERT_NUM(REG_BLOCK_UNAUTHORIZED_TEXT,PVC_DIR(), "BlockUnauthorizedText",            0);
	INSERT_NUM(REG_DND_DURING_CHAT,		   PVC_DIR(), "DndDuringChat",		              0);
	INSERT_NUM(REG_AUTO_ACCEPT_FILES,	   PVC_DIR(), "AutoAcceptFiles",				  0);

    // FileTransfer
	INSERT_STR(REG_TRANSFER_INCOME_PATH,   FTR_DIR(), "IncomePath",                      "");
	INSERT_STR(REG_RECENT_FILE,            FTR_DIR(), "RecentFile",                      "");

	// Display
	INSERT_NUM(REG_TEXT_SIZE,              DSP_DIR(), "TextSize",                         2);
	INSERT_NUM(REG_DOCKING,                DSP_DIR(), "WindowDocking",                    1);
	INSERT_NUM(REG_SHOW_CONTACTS,          DSP_DIR(), "ShowContacts",                     1);
	INSERT_NUM(REG_SHOW_ONTOP,             DSP_DIR(), "ShowOnTop",                        0);
	INSERT_NUM(REG_SHOW_PREVIEW,           DSP_DIR(), "ShowPreview",                      1);
	INSERT_NUM(REG_SHOW_STATUS,            DSP_DIR(), "ShowStatus",                       1);
	INSERT_NUM(REG_HIDE_ON_CLOSE,          DSP_DIR(), "HideOnClose",                      1);
	INSERT_NUM(REG_AWAY_TIMEOUT,           DSP_DIR(), "AwayTimeout",					   10);
	INSERT_NUM(REG_FIRST_RUN,              DSP_DIR(), "FirstRun",                        -1);

	// Skins
	INSERT_STR(REG_SKIN_DIR,               SKN_DIR(), "SkinDirectory",                   "");

	// Audio
	INSERT_NUM(REG_AUDIO_BUFFER_SIZE_INIT, AUD_DIR(), "AudioBufferSizeInit",			  0);
	INSERT_NUM(REG_AUDIO_BUFFER_SIZE,      AUD_DIR(), "AudioBufferSize",                  0);
	INSERT_NUM(REG_AUDIO_CAPTURE_MODE,     AUD_DIR(), "AudioCaptureMode",                 1);
	INSERT_NUM(REG_AUDIO_CAPTURE_DEVICE,   AUD_DIR(), "AudioCaptureDevice",               0);
	INSERT_NUM(REG_AUDIO_PLAYBACK_DEVICE,  AUD_DIR(), "AudioPlaybackDevice",              0);
	INSERT_NUM(REG_AUDIO_MIC_VOLUME    ,   AUD_DIR(), "AudioMicVolume",                 100);



	// Video
	INSERT_NUM(REG_VIDEO_CAPTURE_MODE,     VID_DIR(), "VideoCaptureMode",                 2);
	INSERT_NUM(REG_VIDEO_CAPTURE_DEVICE,   VID_DIR(), "VideoCaptureDevice",               0);
	INSERT_NUM(REG_VIDEO_CAPTURE_INPUT,    VID_DIR(), "VideoCaptureInput",                0);
	INSERT_NUM(REG_NOTIFY_INVITE,          VID_DIR(), "NotifyInvite",                     1);
	INSERT_NUM(REG_VIDEO_CODEC,            VID_DIR(), "VideoCodec",                       0);
 	INSERT_NUM(REG_QUALITY_PROFILE,        VID_DIR(), "QualityProfile",                   1);
 	INSERT_NUM(REG_VIDEO_BRIGHTNESS,       VID_DIR(), "Brightness",                     127);
 	INSERT_NUM(REG_VIDEO_CONTRAST,         VID_DIR(), "Contrast",                        64);   
 	INSERT_NUM(REG_VIDEO_SATURATION,       VID_DIR(), "Saturation",                      64);
 	INSERT_NUM(REG_VIDEO_HUE,			   VID_DIR(), "Hue",							  0);

	// Windows
	INSERT_NUM(REG_CLIST_WND_POS,          WND_DIR(), "ContactListWindowPos",    0x0054020a);
	INSERT_NUM(REG_INCOMINGCALL_WND_POS,   WND_DIR(), "IncomingCallWindowPos",   0x01290191);
	INSERT_NUM(REG_LOGIN_WND_POS,          WND_DIR(), "LoginWindowPos",          0x00500203);
	INSERT_NUM(REG_MAIN_WND_POS,           WND_DIR(), "MainWindowPos",           0x005500e8);
	INSERT_NUM(REG_MEDIA_WND_POS,          WND_DIR(), "MediaSettingsWindowPos",  0x014e0145);
	INSERT_NUM(REG_PEER1_WND_POS,          WND_DIR(), "Peer1WindowPos",          0x0055013d);
	INSERT_NUM(REG_PEER2_WND_POS,          WND_DIR(), "Peer2WindowPos",          0x005501f9);
	INSERT_NUM(REG_PEER3_WND_POS,          WND_DIR(), "Peer3WindowPos",          0x0116013d);
	INSERT_NUM(REG_PEER4_WND_POS,          WND_DIR(), "Peer4WindowPos",          0x011601f9);
	INSERT_NUM(REG_PEER5_WND_POS,          WND_DIR(), "Peer5WindowPos",          0x005502b5);
	INSERT_NUM(REG_PEER6_WND_POS,          WND_DIR(), "Peer6WindowPos",          0x011602b5);
	INSERT_NUM(REG_PEER7_WND_POS,          WND_DIR(), "Peer7WindowPos",          0x00550371);
	INSERT_NUM(REG_PEER8_WND_POS,          WND_DIR(), "Peer8WindowPos",          0x01160371);
	INSERT_NUM(REG_PQP_WND_POS,            WND_DIR(), "PQPWindowPos",            0x01230141);
	INSERT_NUM(REG_PREVIEW_WND_POS,        WND_DIR(), "PreviewWindowPos",        0x00540024);

    // Windows - should we keep these as per Window base?
    INSERT_NUM(REG_TEXTMSG_FONT_BOLD,      WND_DIR(), "TextFontBold",                     0);
	INSERT_NUM(REG_TEXTMSG_FONT_ITALIC,    WND_DIR(), "TextFontItalic",                   0);
	INSERT_NUM(REG_TEXTMSG_FONT_COLOR,     WND_DIR(), "TextFontColor",                    0);

#ifdef CREATE_HTTP_DEBUG_LOG
	// DebugLog
	INSERT_NUM(REG_LOG_QUALITY_ADAPTATION, DEBUG_LOG_DIR(), "LogQA",                      1);
	INSERT_NUM(REG_LOG_AUDIO_RECEIVER,	   DEBUG_LOG_DIR(), "LogAR",                      1);
	INSERT_NUM(REG_LOG_IM_CONNECTION,      DEBUG_LOG_DIR(), "LogIMC",                     1);
	INSERT_NUM(REG_LOG_VS_CONNECTION,      DEBUG_LOG_DIR(), "LogVSC",                     1);
	INSERT_NUM(REG_LOG_HTTP,               DEBUG_LOG_DIR(), "LogHttp",                    1);
#endif

	// Alerts
	INSERT_NUM(REG_ALERT_SOUND,            ALERT_DIR(), "SoundAlert",                     1);
    for(int i = 0; i < sizeof(g_arEvents)/sizeof(g_arEvents[0]); i++)
    {
        if(g_arEvents[i].eventFlags & ECEF_ALERT)
        {
            tstring sAlias = g_arEvents[i].szEventName;
    	    INSERT_NUM(EcRegistryItem(REG_ALERT_FIRST + i), 
                ALERT_DIR(), 
                sAlias, 
                g_arEvents[i].bDefaultTbAlert);
        }
    }

	// Sounds
	INSERT_STR(REG_SOUND_APP_NAME, SND_DIR(), "", APP_TITLE());

    for(i = 0; i < sizeof(g_arEvents)/sizeof(g_arEvents[0]); i++)
    {
        if(g_arEvents[i].eventFlags & ECEF_SOUND)
        {
            tstring sAlias = g_arEvents[i].szEventName;
    	    INSERT_STR(EcRegistryItem(REG_SOUND_ALIAS_FIRST + i), 
                tstring(SND_DIR() + "\\") + sAlias + SND_CRT(), 
                tstring(""), 
                g_arEvents[i].szDefaultSound);
        }
    }

    INSERT_STR_EX(REG_AUTO_RUN_PATH, RUN_DIR(), APP_TITLE(), "", FALSE);

	END_HKEY();
}

// Machine-related entries
static void InitializeMachineKeys(void)
{
	BEGIN_HKEY(HKEY_LOCAL_MACHINE);

	// Network
	INSERT_NUM(REG_NETWORK_OPTIONS,        NET_DIR(), "NetworkOptions",                    3);
	INSERT_NUM(REG_PEER_PORT,              NET_DIR(), "PeerPort",                          0);
	INSERT_NUM(REG_NETWORK_PORT,           NET_DIR(), "NetworkPort",                    5500);
	INSERT_NUM(REG_NETWORK_PORT_MIN,       NET_DIR(), "NetworkPortMin",                 5500);
	INSERT_NUM(REG_NETWORK_PORT_MAX,       NET_DIR(), "NetworkPortMax",                 5560);
	INSERT_NUM(REG_NETWORK_PORT_LAST,      NET_DIR(), "NetworkPortLast",                5500);
	INSERT_STR(REG_SERVICE_ADDRESS,        NET_DIR(), "ServiceAddress",  "chat2.eyeball.com");
	INSERT_NUM(REG_SERVICE_PORT,           NET_DIR(), "ServicePort",                    5501);
	INSERT_STR(REG_CHATROOM_ADDRESS,       NET_DIR(), "ChatRoomAddress", "chatroom.eyeball.com");
	INSERT_NUM(REG_CHATROOM_PORT,          NET_DIR(), "ChatRoomPort",                   5515);
	INSERT_NUM(REG_TRANSFER_PORT,          NET_DIR(), "TransferPort",                      0);
	INSERT_NUM(REG_FIREWALL_TTL,           NET_DIR(), "FirewallTTL",                       2);
	INSERT_STR(REG_DOMAIN,			       NET_DIR(), "Domain",							  "");
	INSERT_NUM(REG_FIREWALL_PORT,          NET_DIR(), "FirewallPort",   			    5503);
	INSERT_STR(REG_FIREWALL_SERVER,        NET_DIR(), "FirewallServer",  "chat0.eyeball.com");
	INSERT_STR(REG_HTTP_PROXY_ADDRESS,     NET_DIR(), "HttpProxyAddress",				  "");
	INSERT_NUM(REG_HTTP_PROXY_PORT,        NET_DIR(), "HttpProxyPort",                     0);
	INSERT_NUM(REG_DETECT_UDP,			   NET_DIR(), "DetectUdp",                         0);
	INSERT_NUM(REG_FORCE_HTTP,			   NET_DIR(), "ForceHttp",                         0);
	INSERT_NUM(REG_HTTP_PORT,			   NET_DIR(), "HttpPort",                         80);
	INSERT_NUM(REG_HTTPS_PORT,             NET_DIR(), "HttpsPort",                       443);
	INSERT_NUM(REG_HTTP_SEND_BUFFER_SIZE,  NET_DIR(), "HttpSendBS",                   2*1024);
	INSERT_NUM(REG_HTTP_AUDIO_BUFFER_SIZE, NET_DIR(), "HttpAudioBS",                    4800);
	INSERT_NUM(REG_HTTP_VIDEO_BUFFER_SIZE, NET_DIR(), "HttpVideoBS",                  8*1024);
	INSERT_NUM(REG_FORCE_P2PREFLECTOR,     NET_DIR(), "ForceP2PReflector",                 0);
	
#ifdef PRO_VERSION
	INSERT_STR(REG_WEB_ADDRESS,            NET_DIR(), "WebAddress",      "www.eyeball.com");
#else
	INSERT_STR(REG_WEB_ADDRESS,            NET_DIR(), "WebAddress",      "www.eyeballchat.com");
#endif // PRO_VERSION

	// 0, automatic detect
	// 1, prompt user to specify
	// 2, low connection speed
	// 3, high connection speed
	INSERT_NUM(REG_CONN_SPEED,             NET_DIR(), "ConnectionSpeed",                  0);

	END_HKEY();
};

// Set keys and defaults for internal operation now - may have to change
// for public release
static void InitializeKeyMap(void)
{
	InitializeUserKeys();
	InitializeMachineKeys();
}

BOOL GetRegistryEntry(HKEY _hKey, const tstring& _basekey, const tstring& _subkey, DWORD& _value)
{
	DWORD dwType, dwValueSize = sizeof(DWORD);
	LONG lnResult;
	HKEY hKey;

	lnResult = RegOpenKeyEx(
		_hKey,
		_basekey.c_str(),
		0,
		KEY_READ,
		&hKey);

	if (lnResult != ERROR_SUCCESS)
	{
		DB("Could not open registry base key " << _basekey.mbs().c_str());
		return FALSE;
	}

	lnResult = RegQueryValueEx(
					hKey,
					_subkey.c_str(),
					0,
					&dwType,
					(LPBYTE)&_value,
					&dwValueSize);

	RegCloseKey(hKey);

	if (lnResult != ERROR_SUCCESS)
	{
		DB("Could not query registry key: " << _subkey.mbs().c_str());
		return FALSE;
	}

	if (dwType != REG_DWORD)
	{
		DB("Registry key has incorrect type (" << dwType << "), expected REG_DWORD");
		return FALSE;
	}

	if (dwValueSize != sizeof(DWORD))
	{
		DB("Registry key has incorrect size (" << dwValueSize << "), expected sizeof(DWORD)");
		return FALSE;
	}

	return TRUE;
}

BOOL GetRegistryEntry(
	HKEY _hKey, 
	const tstring& _basekey, 
	const tstring& _subkey, 
	tstring& _value)
{
	DWORD dwType = 0;
	DWORD dwValueSize = 0;
	LONG lnResult;
	HKEY hKey;

	lnResult = RegOpenKeyEx(
					_hKey,
					_basekey.c_str(),
					0,
					KEY_READ,
					&hKey);

	if (lnResult != ERROR_SUCCESS)
	{
		DB("Could not open registry base key " << _basekey.mbs().c_str());
		return FALSE;
	}

	// Query first without value to find length
	lnResult = RegQueryValueEx(
				hKey,
				_subkey.c_str(),
				0,
				&dwType,
				NULL,
				&dwValueSize);

	if (lnResult != ERROR_SUCCESS)
	{
		DB("Could not query registry key: " << _subkey.mbs().c_str());

		RegCloseKey(hKey);
		return FALSE;
	}

	if (dwType != REG_SZ)
	{
		DB("Registry key has incorrect type (" << dwType << "), expected REG_SZ");

		RegCloseKey(hKey);
		return FALSE;
	}

	// If it is a null string RegQueryValueEx() does not provide NULL terminator for us
	if (dwValueSize == 0)
	{
		_value = "";

		RegCloseKey(hKey);
		return TRUE;
	}

	SharedPtr<TCHAR> pValue(new TCHAR[dwValueSize]);

	lnResult = RegQueryValueEx(
					hKey,
					_subkey.c_str(),
					0,
					&dwType,
					(LPBYTE)pValue.get(),
					&dwValueSize);

	RegCloseKey(hKey);

	if (lnResult != ERROR_SUCCESS)
	{
		DB("Could not query registry key: " << _subkey.mbs().c_str());
		return FALSE;
	}

	_value = "";		// This is for BoundsChecker
	_value = pValue.get();

	return TRUE;
}

// SetRegistryEntry creates the key if it doesn't already exist
BOOL SetRegistryEntry(
	HKEY _hKey, 
	const tstring& _basekey, 
	const tstring& _subkey, 
	DWORD _value)
{
	DWORD dwDisp, dwType(REG_DWORD), dwValueSize(sizeof(DWORD));
	LONG lnResult;
	HKEY hKey;

	lnResult = RegCreateKeyEx(
		_hKey,
		_basekey.c_str(),
		0,
		NULL,
		REG_OPTION_NON_VOLATILE,
		KEY_ALL_ACCESS,
		NULL,
		&hKey,
		&dwDisp);

	if (lnResult != ERROR_SUCCESS)
	{
		// Had an error creating the key.  If the error is ERROR_ACCESS_DENIED, we're running
		// Windows NT/2000 and are not an administrator, so we'll say so (first time only).
		// All other errors cause us to fail and exit.

		if ((lnResult == ERROR_ACCESS_DENIED) && !g_bDisplayedAdminError)
		{
			// Not an administrator on this machine...
			//MSG_BOX("Settings will not be saved between sessions unless\n" \
			//	"you are an administrator on this workstation.");

			// Set flag to TRUE so we don't keep displaying this error for multiple keys
			g_bDisplayedAdminError = TRUE;
		}

		return FALSE;
	}

	lnResult = RegSetValueEx(
		hKey,
		_subkey.c_str(),
		0,
		dwType,
		(LPBYTE)&_value,
		dwValueSize);

	if (lnResult != ERROR_SUCCESS)
	{
		// Error writing to Registry, so close key and throw an exception
		RegCloseKey(hKey);

		return FALSE;
	}
	
	// Flush and close the key, then exit
	RegFlushKey(hKey); // rarely used, but this is a valid case
	RegCloseKey(hKey);

	return TRUE;
}

BOOL SetRegistryEntry(
	HKEY _hKey, 
	const tstring& _basekey, 
	const tstring& _subkey, 
	const tstring& _value)
{
	DWORD dwDisp = 0;
	DWORD dwType = REG_SZ;
	DWORD dwValueSize = _value.length() * sizeof(TCHAR);
	LONG lnResult;
	HKEY hKey;

	lnResult = RegCreateKeyEx(
		_hKey,
		_basekey.c_str(),
		0,
		NULL,
		REG_OPTION_NON_VOLATILE,
		KEY_ALL_ACCESS,
		NULL,
		&hKey,
		&dwDisp);

	if (lnResult != ERROR_SUCCESS)
	{
		// Had an error creating the key.  If the error is ERROR_ACCESS_DENIED, we're running
		// Windows NT/2000 and are not an administrator, so we'll say so (first time only).
		// All other errors cause us to fail and exit.

		if ((lnResult == ERROR_ACCESS_DENIED) && !g_bDisplayedAdminError)
		{
			// Not an administrator on this machine...
			//MSG_BOX("Settings will not be saved between sessions unless\n" \
			//	"you are an administrator on this workstation.");

			// Set flag to TRUE so we don't keep displaying this error for multiple keys
			g_bDisplayedAdminError = TRUE;
		}

		return FALSE;
	}

	lnResult = RegSetValueEx(
		hKey,
		_subkey.c_str(),
		0,
		dwType,
		(LPBYTE)_value.c_str(),
		dwValueSize);

	if (lnResult != ERROR_SUCCESS)
	{
		// Error writing to Registry, so close key and throw an exception
		RegCloseKey(hKey);

		return FALSE;
	}
	
	// Flush and close the key, then exit
	RegFlushKey(hKey); // rarely used, but this is a valid case
	RegCloseKey(hKey);

	return TRUE;
}

BOOL DeleteRegistryEntry(
	HKEY _hKey, 
	const tstring& _basekey, 
	const tstring& _subkey)
{
	DWORD dwDisp, dwType(REG_DWORD), dwValueSize(sizeof(DWORD));
	LONG lnResult;
	HKEY hKey;

	lnResult = RegCreateKeyEx(
		_hKey,
		_basekey.c_str(),
		0,
		NULL,
		REG_OPTION_NON_VOLATILE,
		KEY_ALL_ACCESS,
		NULL,
		&hKey,
		&dwDisp);

	if (lnResult != ERROR_SUCCESS)
	{
		// Had an error creating the key.  If the error is ERROR_ACCESS_DENIED, we're running
		// Windows NT/2000 and are not an administrator, so we'll say so (first time only).
		// All other errors cause us to fail and exit.

		if ((lnResult == ERROR_ACCESS_DENIED) && !g_bDisplayedAdminError)
		{
			// Not an administrator on this machine...
			//MSG_BOX("Settings will not be saved between sessions unless\n" \
			//	"you are an administrator on this workstation.");

			// Set flag to TRUE so we don't keep displaying this error for multiple keys
			g_bDisplayedAdminError = TRUE;
		}

		return FALSE;
	}

	lnResult = RegDeleteValue(
		hKey,
		_subkey.c_str());

	if (lnResult != ERROR_SUCCESS)
	{
		// Error writing to Registry, so close key and throw an exception
		RegCloseKey(hKey);

		return FALSE;
	}
	
	// Flush and close the key, then exit
	RegFlushKey(hKey); // rarely used, but this is a valid case
	RegCloseKey(hKey);

	return TRUE;
}

// ---------------------------------------------------------------------------
// Exported functions
// ---------------------------------------------------------------------------

// Function to check for existence of all valid registry entries and assign
// default values for any missing fields.  

void EcCreateRegistry(EcRegistryItem i)
{
	KeyMap::const_iterator racer;
	racer = g_KeyMap.find(i);
	if (racer == g_KeyMap.end())
		return;
	
	const RegistryEntry& regEntry = racer->second;
	if(!regEntry.auto_create)
		return;

	if (regEntry.is_num)
	{
		DWORD dwValue;
		if (!EcGetRegistryEntry(i, dwValue)) 
			EcSetRegistryEntry(i, regEntry.num_val);
	}
	else
	{
		tstring sValue;
		if (!EcGetRegistryEntry(i, sValue)) 
			EcSetRegistryEntry(i, regEntry.str_val);
	}
}

	
BOOL EcInitializeRegistry(
	const tstring& sCompany,
	const tstring& sAppName,
	const tstring& sVersion)
{
	if (!sCompany.empty())
	{
		s_sCompany = sCompany;
	}

	if (!sAppName.empty())
	{
		s_sAppName = sAppName;
	}

	if (!sVersion.empty())
	{
		s_sVersion = sVersion;
	}

	BOOL rc(TRUE);

	InitializeKeyMap();

	if (sAppName != tstring("Sdk"))
	{
		KeyMap::const_iterator racer;

		for (racer = g_KeyMap.begin(); racer != g_KeyMap.end(); racer++)
		{
			EcRegistryItem i = racer->first;
			EcCreateRegistry(i);
		}
	}
	else
	{
		// delete unused registry
		EcDeleteRegistryEntry(REG_PEER_PORT);
		EcDeleteRegistryEntry(REG_NETWORK_PORT);

		// create registry if not exists
		EcCreateRegistry(REG_AUDIO_BUFFER_SIZE_INIT);
		EcCreateRegistry(REG_AUDIO_BUFFER_SIZE);
		EcCreateRegistry(REG_AUDIO_CAPTURE_MODE);
		EcCreateRegistry(REG_VIDEO_CAPTURE_MODE);
		EcCreateRegistry(REG_VIDEO_CAPTURE_DEVICE);
		EcCreateRegistry(REG_QUALITY_PROFILE);
		EcCreateRegistry(REG_VIDEO_CAPTURE_INPUT);
		EcCreateRegistry(REG_AUDIO_CAPTURE_DEVICE);
		EcCreateRegistry(REG_AUDIO_PLAYBACK_DEVICE);
		EcCreateRegistry(REG_VIDEO_BRIGHTNESS);
		EcCreateRegistry(REG_VIDEO_CONTRAST);
		EcCreateRegistry(REG_VIDEO_SATURATION);
		EcCreateRegistry(REG_VIDEO_HUE);

		EcCreateRegistry(REG_CONN_SPEED);
		EcCreateRegistry(REG_FIREWALL_TTL);
		EcCreateRegistry(REG_NETWORK_PORT_MIN);
		EcCreateRegistry(REG_NETWORK_PORT_MAX);
		EcCreateRegistry(REG_NETWORK_PORT_LAST);
		EcCreateRegistry(REG_NETWORK_OPTIONS);

		EcCreateRegistry(REG_AUTO_ACCEPT_CALL);
		EcCreateRegistry(REG_AUTO_ADD_CONTACT);
		EcCreateRegistry(REG_BLOCK_UNAUTHORIZED_TEXT);

		EcCreateRegistry(REG_DETECT_UDP);

#ifdef CREATE_HTTP_DEBUG_LOG
		EcCreateRegistry(REG_LOG_QUALITY_ADAPTATION);
		EcCreateRegistry(REG_LOG_AUDIO_RECEIVER);
		EcCreateRegistry(REG_LOG_IM_CONNECTION);
		EcCreateRegistry(REG_LOG_VS_CONNECTION);
		EcCreateRegistry(REG_LOG_HTTP);
#endif
	}
	

	return rc;
}

// Now returns default value if query from registry fails.

BOOL EcGetRegistryEntry(EcRegistryItem regEntrytItem, DWORD& _value)
{
	KeyMap::const_iterator iter = g_KeyMap.find(regEntrytItem);

	RFI(iter == g_KeyMap.end());

	const RegistryEntry& regEntry = iter->second;

	RFUT(regEntry.is_num);

	BOOL rc = GetRegistryEntry(regEntry.hkey, regEntry.basekey, regEntry.subkey, _value);

	// If that failed, assign default so we can keep going
	if (!rc)
	{
		DB("Could not read entry from registry, assigning default value");
		_value = regEntry.num_val;
	}

	return rc;
}

BOOL EcGetRegistryEntry(EcRegistryItem regEntrytItem, tstring& _value)
{
	KeyMap::const_iterator iter = g_KeyMap.find(regEntrytItem);

	if (iter == g_KeyMap.end())
	{
		// EcInitializeRegistry() needs to be called
//		DB_ASSERT(0 && "registry not initialized");
		return FALSE;
	}

	const RegistryEntry& regEntry = iter->second;

	RFIT(regEntry.is_num);

	BOOL rc = GetRegistryEntry(regEntry.hkey, regEntry.basekey, regEntry.subkey, _value);

	// If that failed, assign default so we can keep going
	if (!rc)
	{
		DB("Could not read entry from registry, assigning default value");
		_value = regEntry.str_val;
	}

	return rc;
}
#endif
BOOL EcSetRegistryEntry(EcRegistryItem regEntrytItem, DWORD _value)
{
#if 0
	KeyMap::const_iterator iter = g_KeyMap.find(regEntrytItem);

	RFI(iter == g_KeyMap.end());

	const RegistryEntry& regEntry = iter->second;

	BOOL rc = SetRegistryEntry(regEntry.hkey, regEntry.basekey, regEntry.subkey, _value);

    if (!rc)
	{
		DB("Could not write entry to registry");
	}
#endif
	return TRUE;
}

BOOL EcSetRegistryEntry(EcRegistryItem regEntrytItem, const tstring& _value)
{
#if 0
	KeyMap::const_iterator iter = g_KeyMap.find(regEntrytItem);

	RFI(iter == g_KeyMap.end());

	const RegistryEntry& regEntry = iter->second;

	BOOL rc = SetRegistryEntry(regEntry.hkey, regEntry.basekey, regEntry.subkey, _value);

    if (!rc)
	{
		DB("Could not write entry to registry");
	}
#endif
	return TRUE;
}
BOOL EcDeleteRegistryEntry(EcRegistryItem regEntrytItem)
{
	KeyMap::const_iterator iter = g_KeyMap.find(regEntrytItem);

	RFI(iter == g_KeyMap.end());

	const RegistryEntry& regEntry = iter->second;

	BOOL rc = DeleteRegistryEntry(regEntry.hkey, regEntry.basekey, regEntry.subkey);

    if (!rc)
	{
		//DB("Could not delete entry from registry");
	}

	return rc;
}

tstring EcGetEventName(EcEventType nId)
{
    for(UINT nIndex = 0; nIndex < sizeof(g_arEvents)/sizeof(g_arEvents[0]); nIndex++)
    {
        if(nId == g_arEvents[nIndex].eventType)
        {
            return g_arEvents[nIndex].szEventName;
        }
    }
    return "";
}

EcEventType EcGetEventId(const tstring& _alert)
{
    for(UINT nIndex = 0; nIndex < sizeof(g_arEvents)/sizeof(g_arEvents[0]); nIndex++)
    {
        if(_alert == g_arEvents[nIndex].szEventName)
        {
            return g_arEvents[nIndex].eventType;
        }
    }
    return EC_EVENT_NULL;
}

UINT EcGetEventFlags(const tstring& _alert)
{
    for(UINT nIndex = 0; nIndex < sizeof(g_arEvents)/sizeof(g_arEvents[0]); nIndex++)
    {
        if(_alert == g_arEvents[nIndex].szEventName)
        {
            return g_arEvents[nIndex].eventFlags;
        }
    }
    return 0;
}

BOOL EcGetSoundRegistryEntry(const tstring& _alert, tstring& _value)
{
    for(UINT nIndex = 0; nIndex < sizeof(g_arEvents)/sizeof(g_arEvents[0]); nIndex++)
    {
        if(_alert == g_arEvents[nIndex].szEventName)
        {
            return EcGetRegistryEntry(EcRegistryItem(REG_SOUND_ALIAS_FIRST + nIndex), _value);
        }
    }
    return FALSE;
}

BOOL EcSetSoundRegistryEntry(const tstring& _alert, const tstring& _value)
{
    for(UINT nIndex = 0; nIndex < sizeof(g_arEvents)/sizeof(g_arEvents[0]); nIndex++)
    {
        if (_alert == g_arEvents[nIndex].szEventName)
        {
            return EcSetRegistryEntry(EcRegistryItem(REG_SOUND_ALIAS_FIRST + nIndex), _value);
        }
    }
    return FALSE;
}

BOOL EcGetAlertRegistryEntry(const tstring& _alert, DWORD& _value)
{
    for(UINT nIndex = 0; nIndex < sizeof(g_arEvents)/sizeof(g_arEvents[0]); nIndex++)
    {
        if(_alert == g_arEvents[nIndex].szEventName)
        {
            return EcGetRegistryEntry(EcRegistryItem(REG_ALERT_FIRST + nIndex), _value);
        }
    }
    return FALSE;
}

BOOL EcSetAlertRegistryEntry(const tstring& _alert, DWORD _value)
{
    for(UINT nIndex = 0; nIndex < sizeof(g_arEvents)/sizeof(g_arEvents[0]); nIndex++)
    {
        if(_alert == g_arEvents[nIndex].szEventName)
        {
            return EcSetRegistryEntry(EcRegistryItem(REG_ALERT_FIRST + nIndex), _value);
        }
    }
    return FALSE;
}

