// ---------------------------------------------------------------------------
// File:       crypt.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2002
//
// Purpose:    Simple encryption routines, i.e. for writing to registry
// 
// Change Log:
// ---------------------------------------------------------------------------

#include "Base.h"  // automatic use of precompiled headers 

#ifndef _WIN32_WCE
#include <time.h>		// time()
#endif

#include "crypt.h"

#if 1 //for Linux yamada
#include "Timer.h" //use timeGetTime()
#endif //for Linux yamada

using std::string;


// ---------------------------------------------------------------------------
// Macros
// ---------------------------------------------------------------------------

#define HEX_STAT(X) \
	{ \
		char buf[256]; \
		sprintf(buf, "%hx", (unsigned char)X); \
		DB(#X << " = 0x" << buf); \
	}


// ---------------------------------------------------------------------------
// Local functions
// ---------------------------------------------------------------------------

static string rot13(string _str)
{
	// Note: Only manipulates alphabetic characters: [a-z,A-Z]
	register char cap, byte;

	for (string::size_type i = 0; i < _str.length(); i++)
	{
		byte = _str[i];

		cap = byte & 32;
		byte &= ~cap;
		byte = ((byte >= 'A') && (byte <= 'Z') ? ((byte - 'A' + 13) % 26 + 'A') : byte) | cap;

		_str[i] = byte;
	}

	return _str;
}

// Alternate MSB and LSB nibbles from start and end of string, working inwards
// Very simple, but should help against basic frequency attacks
static string swap_nibbles(string _str)
{
	string::size_type i, j;
	char left, right;

	for (i = 0, j = _str.length() - 1; i < _str.length() / 2; i++, j--)
	{
		left  = _str[i];
		right = _str[j];

		_str[i] = ((0x0f & right) << 4) | ((0xf0 & left)  >> 4);
		_str[j] = ((0x0f & left) << 4)  | ((0xf0 & right) >> 4);
	}

	return _str;
}

static string unswap_nibbles(string _str)
{
	string::size_type i, j;
	char left, right;

	for (i = 0, j = _str.length() - 1; i < _str.length() / 2; i++, j--)
	{
		left  = _str[i];
		right = _str[j];

		_str[i] = ((0x0f & left)  << 4) | ((0xf0 & right) >> 4);
		_str[j] = ((0x0f & right) << 4) | ((0xf0 & left)  >> 4);
	}

	return _str;
}

// Return hex version of string
static string hex_string(const string& _str)
{
	string rc;
	char szHexBuf[5];

	for (string::size_type i = 0; i < _str.length(); i++)
	{
		sprintf(szHexBuf, "%.2hx", (unsigned char)_str[i]);
		rc += szHexBuf;
	}

	return rc;
}

static string unhex_string(const string& _str)
{
	string rc;
	short int cHex;

	for (string::size_type i = 0; i < _str.length(); i += 2)
	{
		sscanf(_str.substr(i, 2).c_str(), "%hx", &cHex);
		rc += (char)cHex;
	}

	return rc;
}

static string random_string(int _len)
{
	string rc;

	// Seed pseudo-random number generator from clock
#if 1 //dor Linux yamada
	srand(timeGetTime());
#else
#ifndef _WIN32_WCE
	srand(time(NULL));
#else
	srand(GetTickCount());
#endif
#endif //for linux yamada
	for (int i=0; i < _len; i++)
	{
		rc += (char)rand();
	}

	return rc;
}


// ---------------------------------------------------------------------------
// Exported functions
// ---------------------------------------------------------------------------

string Crypt::simple_crypt(const string& _str)
{
	return hex_string(swap_nibbles(rot13(_str) + random_string(_str.length())));
}

string Crypt::simple_decrypt(const string& _str)
{
	string sDecrypt = rot13(unswap_nibbles(unhex_string(_str)));
	return sDecrypt.substr(0, sDecrypt.length() / 2);
}
