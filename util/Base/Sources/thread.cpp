/** All files of the DNDS Prototype are Copyright (C) 1999 by Andre Schoorl
 *  unless otherwise noted.
 *
 *  The DNDS Prototype is distributed in the public domain in the hope that it
 *  will be useful, but is provided `as is' and is subject to change without
 *  notice.  No warranty of any kind is made with regard to the software or
 *  documentation.  The author shall not be liable for for any error for
 *  incidental or consequential damages in connection with the software and
 *  the documentation.
 *
 *  Permission to copy the whole, unmodified DNDS package is granted provided
 *  that the copies are not made or distributed for resale (excepting nominal
 *  copying fees).  All redistributions in either source or binary form must
 *  include this disclaimer.
 */

#include "Base.h"  // automatic use of precompiled headers 

#ifdef WIN32
#ifndef _WIN32_WCE
#include <process.h>
#endif
#include <windows.h>
#endif

#include "debug.h"
#include "Thread.h"

Thread::Thread(void * (*start_routine)(void *), void * arg)
{

#ifdef WIN32

#ifndef _WIN32_WCE
	unsigned int lpThreadAddr;
	m_ThreadId = _beginthreadex(
		NULL,
		0,
		(unsigned int (__stdcall *)(void *))start_routine,
		arg,
		0,
		&lpThreadAddr
	);
#else
	unsigned long lpThreadAddr;

	m_ThreadId = (unsigned long) CreateThread(
		NULL,
		0,
		(unsigned long (__stdcall *)(void *))start_routine,
		arg,
		0,
		&lpThreadAddr);

#endif

	ASSERT(m_ThreadId != 0);
#else
	ASSERT(pthread_create(&m_ThreadId, NULL, start_routine, arg) == 0);
#endif
}

Thread::~Thread()
{
	// DB("Thread::~Thread");

#ifdef WIN32
	//We should not terminate thread, we would wait for thread join
	//ASSERT(TerminateThread(m_ThreadId, 0));
	//_endthreadex(m_ThreadId);
	WARN(CloseHandle((HANDLE)m_ThreadId));
#else
	// Just warn if cancel fails since thread may already have ended
	// Mainly for Solaris
	WARN(pthread_cancel(m_ThreadId) == 0);
#endif
}

#ifdef WIN32
void Thread::set_priority(int priority)
{
	// DB("Thread::set_priority");

	BOOL bRet = SetThreadPriority((HANDLE)m_ThreadId, priority);
	DB_ASSERT(bRet);
}
#endif

void Thread::join(void)
{
	// DB("Thread::join");

#ifdef WIN32
	WARN(WaitForSingleObject((HANDLE)m_ThreadId, INFINITE) != WAIT_FAILED);
#else
	WARN(pthread_join(m_ThreadId, NULL) == 0);
#endif
}
