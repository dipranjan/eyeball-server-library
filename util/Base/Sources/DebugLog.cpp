// ---------------------------------------------------------------------------
// File:       DebugLog.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2002
//
// Purpose:    log debug information to file
// 
// Change Log:
// ---------------------------------------------------------------------------
#include "Base.h"
#ifndef _WIN32_WCE
#include <fstream.h>
#include <stdio.h>		// sprintf()
#include <time.h>		// cftime()
#endif

#include "debug.h"
#include "Registry.h"
#include "DebugLog.h"

#define LOG_REG_BASE "Software\\Eyeball\\DebugLog"

bool GetLogEnabled(DebugLogItem item)
{
	DWORD value = 0;
	if (item == LOG_WINDOW_SIZE_ADAPTATION)
	{
		if (GetRegistryEntry(HKEY_CURRENT_USER, LOG_REG_BASE, "LogWSA", value))
			return value != 0;
		return false;
	}

	if (item == LOG_QUALITY_ADAPTATION)
	{
		if (GetRegistryEntry(HKEY_CURRENT_USER, LOG_REG_BASE, "LogQA", value))
			return value != 0;
		return false;
	}

	if (item == LOG_DB_ASSERT)
	{
		if (GetRegistryEntry(HKEY_CURRENT_USER, LOG_REG_BASE, "LogDBA", value))
			return value != 0;
		return false;
	}

	if (item == LOG_VIDEO_CAPTURE)
	{
		if (GetRegistryEntry(HKEY_CURRENT_USER, LOG_REG_BASE, "LogVC", value))
			return value != 0;
		return false;
	}

	if (item == LOG_FIREWALL)
	{
		if (GetRegistryEntry(HKEY_CURRENT_USER, LOG_REG_BASE, "LogFW", value))
			return value != 0;
		return false;
	}
	
	if (item == LOG_AUDIO_RECEIVER)
	{
		if (GetRegistryEntry(HKEY_CURRENT_USER, LOG_REG_BASE, "LogAR", value))
			return value != 0;
		return false;
	}

	if (item == LOG_IM_CONNECTION)
	{
		if (GetRegistryEntry(HKEY_CURRENT_USER, LOG_REG_BASE, "LogIMC", value))
			return value != 0;
		return false;
	}

	if (item == LOG_VS_CONNECTION)
	{
		if (GetRegistryEntry(HKEY_CURRENT_USER, LOG_REG_BASE, "LogVSC", value))
			return value != 0;
		return false;
	}

	if (item == LOG_HTTP)
	{
		if (GetRegistryEntry(HKEY_CURRENT_USER, LOG_REG_BASE, "LogHttp", value))
			return value != 0;
		return false;
	}

	return false;
}

#ifndef _WIN32_WCE
CDebugLog::CDebugLog(char* szName):
	m_pLogFile(NULL),
	m_bEnable(false)
{
	strcpy(m_szFileName, szName);
}

CDebugLog::~CDebugLog()
{
	Close();
}


void CDebugLog::SetFile(const char* szName)
{
	if (m_pLogFile)
		return;

	strcpy(m_szFileName, szName);
}


void CDebugLog::Enable(bool bEnable)
{
	if (m_pLogFile)
	{
		Close();
	}

	m_bEnable = bEnable;
}


void CDebugLog::Open()
{
	printf("open file1\n");
	if (m_pLogFile || !m_bEnable)
		return;
	printf("open file2\n");
	WARN(m_pLogFile = new ofstream(m_szFileName));
}


void CDebugLog::OpenApp()
{
	if (m_pLogFile || !m_bEnable)
		return;

	WARN(m_pLogFile = new ofstream(m_szFileName, ios::app));

	*m_pLogFile << "\n**************************************************\n";

}

void CDebugLog::Log(char* szLog)
{
	if (m_pLogFile)
		*m_pLogFile << LocalTime().c_str() << " " << szLog << endl;
}


void CDebugLog::Close()
{
	if (!m_pLogFile)
		return;

	*m_pLogFile << flush;

	SAFE_DELETE(m_pLogFile);
}

string CDebugLog::LocalTime(void)
{
   struct tm *newtime;
   long ltime;

   time(&ltime);

   /* Obtain coordinated universal time: */
   newtime = localtime(&ltime);

   char buf[40];

   sprintf(buf, "%02d:%02d:%02d %04d-%02d-%02d", newtime->tm_hour, newtime->tm_min,
	   newtime->tm_sec, newtime->tm_year + 1900, newtime->tm_mon + 1, newtime->tm_mday);

   return buf;
}

static CDebugLog cLogAssert(LOG_FILENAME_DBA);
void LogAssert(char* expr, char* filename, int line)
{
	static bool bInit = false;
	static bool bEnabled = GetLogEnabled(LOG_DB_ASSERT);
	if (!bInit)
	{
		bInit = true;
		cLogAssert.Enable(bEnabled);
		cLogAssert.Open();
	}
	if (!bEnabled)
		return;

	char szBuf[200];
	sprintf(szBuf, "\nFile: %s \nLine: %d \nAssert: %s", filename, line, expr);
	cLogAssert.Log(szBuf);
}

#else
void LogAssert(char* expr, char* filename, int line)
{
	// do nothing
}
#endif


