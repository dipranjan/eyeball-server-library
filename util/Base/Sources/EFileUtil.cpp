// ---------------------------------------------------------------------------
// File:       EFileUtil.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2002
//
// Purpose:    Helper functions for handling files
//
// Change Log:
// ---------------------------------------------------------------------------

#include "Base.h"  // automatic use of precompiled headers 

// not all includes required
#if 1 //for Linux yamada
//
#else
#include <atlbase.h>

#include <shlobj.h>

#include "AtlWFile.h"
#endif //for Linux yamada

#include "EFileUtil.h"
//#include "SmartPtr.h"
#include "TextUtil.h"
#include "tstring.h"
//#include <winver.h> // required by Windows CE

using std::string;

// limit number of unique file names generated - e.g. filename(n).ext
#define MAX_DUPLICATE_FILE_NAMES (10000)

// Get version of a specified file
bool GetVersionOfFile (const tstring& _filename, tstring& _version)
{
#ifndef _WIN32_WCE
	DWORD dwScratch;
	DWORD* pdwLangChar;
	DWORD dwInfSize ;
	UINT uSize;
	TCHAR szVersion[32];
	TCHAR szResource[80];
	TCHAR* pszVersion = szVersion;

	tstring sFileName(_filename);

	if ((dwInfSize = GetFileVersionInfoSize(const_cast<TCHAR*>(sFileName.c_str()), &dwScratch)) == 0)
		return false;

	ScopedArray<BYTE> pbyInfBuff(new BYTE [dwInfSize]);
	memset (pbyInfBuff.get(), 0, dwInfSize);

	if (!GetFileVersionInfo(&sFileName[0], 0, dwInfSize, pbyInfBuff.get()))
		return false;

	if (!VerQueryValue(
		pbyInfBuff.get(),
		tstring("/VarFileInfo/Translation"),
		(void**)(&pdwLangChar),
		&uSize))
		return false;

	wsprintf(szResource, tstring("/StringFileInfo/%04X%04X/FileVersion"),
		LOWORD (*pdwLangChar), HIWORD (*pdwLangChar));

	if (!VerQueryValue (pbyInfBuff.get(),
		szResource,
		(void**)(&pszVersion),
		&uSize))
		return false;

	_version = pszVersion;

	return true;
#else
	return false;
#endif
}

// Get version of a currently executing file (works for DLL's too)
tstring GetVersionOfCurrent(void)
{
#if 1 //for Linux yamada
//現在実行中のファイルのバージョンを得る
	tstring version;
	return version;
#else
	tstring filename;
	filename.resize(MAX_PATH);
	BOOL bRet = GetModuleFileName(NULL, &filename[0], filename.size());
	DB_ASSERT(bRet);

	tstring version;
	bRet = GetVersionOfFile(filename, version);
	DB_ASSERT(bRet);

	return version;
#endif //for Linux yamada
}

tstring GetPathName(const tstring& sDirName, const tstring& sFileName)
{
	if (sFileName.find_first_of('/') != tstring::npos)
	{
		DB_ASSERT(0 && "filename contains backslash");
		return "";
	}

	if (sDirName.empty())
		return sFileName;

	if (sFileName.empty())
		return sDirName;

	tstring sRet = sDirName;
	if (sDirName[sDirName.length() - 1] != '/')
	{
		sRet += '/';
	}
	sRet += sFileName;

	return sRet;
}

tstring GetDirName(const tstring& sPathName)
{
	tstring::size_type idx = sPathName.find_last_of(tstring("/"));
	if (idx == tstring::npos)
		return tstring("");

	return sPathName.substr(0, idx);
}

tstring GetFileName(const tstring& sPathName)
{
	tstring::size_type idx = sPathName.find_last_of('/');
	if (idx == tstring::npos)
		return sPathName;

	return sPathName.substr(idx + 1);
}

tstring GetFileExtension(const tstring& sPathName)
{
	tstring::size_type idxDot = sPathName.find_last_of('.');
	if (idxDot == tstring::npos)
		return "";

	tstring::size_type idxSlash = sPathName.find_last_of('/');
	if (idxSlash != tstring::npos)
	{
		if (idxSlash > idxDot)
			return "";
	}

	tstring sExt = sPathName.substr(idxDot + 1);
	// return the lower case
	return sExt;
}

DWORD GetFileSize(const tstring& sFileName)
{
#if 1 //for Linux yamada
	DWORD nSize;
	return nSize;
#else
	HANDLE hFile = GetFileHandle(sFileName);
	
	if (!hFile)
        return 0;
 

	DWORD nSize = GetFileSize(hFile, NULL);

	CloseHandle(hFile);
	
	return nSize;
#endif //for Linux yamada

}

DWORD GetFileSize(const HANDLE hFile)
{
#if 1 //for Linux yamada
//FDからファイルサイズをもとめる
	DWORD nSize;
	return nSize;
#else
	return GetFileSize(hFile, NULL);
#endif //for Linux yamada
}

HANDLE GetFileHandle(const tstring& sFileName)
{
#if 1 //for Linux yamada
//ファイル名からFDを求める
        return NULL;
#else
	HANDLE hFile = CreateFile(
		sFileName.c_str(), 
		GENERIC_READ, 
		FILE_SHARE_READ, 
		NULL, 
		OPEN_EXISTING, 
		FILE_ATTRIBUTE_NORMAL, 
		NULL);
	
	if (hFile == INVALID_HANDLE_VALUE)
        return NULL;

	return hFile;
#endif //for Linux yamada
}



tstring GetModuleDir(TECInstancePtr hInst)
{
#if 1 //for Linux yamada
//指定されたモジュールを含む実行ファイルの、ディレクトリ名を取得。
	return "";
#else
	TCHAR szFileName[MAX_PATH];
	BOOL bRet = GetModuleFileName(
		hInst, 
		szFileName, 
		sizeof(szFileName) / sizeof(TCHAR));
	DB_ASSERT(bRet);

	return GetDirName(szFileName);
#endif //for Linux yamada
}

bool CreateAllDirectories(const tstring& sDirName)
{
	tstring sPath = sDirName;

	// remove ending backslash if it exists
	if (sPath[sPath.length() - 1] == '/')
	{
		sPath.erase(sPath.length() - 1);
	}

	// base case ... if directory exists
	if (DirectoryExists(sPath)) 
		return true;
	
	// recursive call, one less directory
	unsigned pos = sPath.find_last_of('/');
	if (pos == tstring::npos)
		return false;

	if (!CreateAllDirectories(sPath.substr(0, pos)))
		return false;
	
	// actual work
#if 1 //for Linux yamada
	//...
#else
	if (!CreateDirectory(sPath.c_str(), NULL))
		return false;
#endif //for Linux yamada
	
	return true;
}

bool DirectoryExists(const tstring& sDirName)
{
#if 1 //for Linux yamada
		return false;
#else
	DWORD dwAttributes = GetFileAttributes(sDirName.c_str());
	if ((~0) == dwAttributes)
	{
		// error occurred
		return false;
	}

	if (!(FILE_ATTRIBUTE_DIRECTORY & dwAttributes))
	{
		// not a directory
		return false;
	}

	return true;
#endif //for Linux yamada
}

bool FileExists(const tstring& sPathName)
{
#if 1 //for Linux yamada
		return false;
#else
	ATLW::CFile file;
	return 0 != file.FileExists(sPathName.c_str());
#endif //for Linux yamada
}

bool IsExecutable(const tstring& sPathName)
{
#ifndef _WIN32_WCE
	string sExt = GetFileExtension(sPathName);
	
	if ("exe" == sExt)
		return true;

	if ("com" == sExt)
		return true;

	if ("bat" == sExt)
		return true;

	if ("pif" == sExt)
		return true;

	if ("vbs" == sExt)
		return true;
	
#endif
	return false;
}

tstring GenerateUniquePathName(const tstring& sPathName)
{
	tstring sDirectory = GetDirName(sPathName);
	tstring sFileName = GetFileName(sPathName);

	tstring sNewPath = sPathName;
	
	int i = 0;
	while (FileExists(sNewPath.c_str()))
	{
		if (i++ >= MAX_DUPLICATE_FILE_NAMES)
		{
			// Prevent infinite loops in case of error in FileExists function.
			DB_ASSERT(0 && "unable to generate unique filename");
			return "";
		}

		// Generate a unique filename.
		unsigned int pos = sFileName.find_last_of(tstring("."));
		tstring sFileBase = sFileName.substr(0, pos);

		// sFileExt is of the form ".ext" or an empty string
		tstring sFileExt;
		if (pos != tstring::npos)
		{
			sFileExt = sFileName.substr(pos);
		}

		tstring sNewFileName = sFileBase + " (" + itoa(i) + ")" + sFileExt;

		sNewPath = GetPathName(sDirectory, sNewFileName);
	}

	return sNewPath;
}

tstring GetTempInternetDir(void)
{
	TCHAR szDir[MAX_PATH];
	memset(szDir, 0, sizeof(szDir));

#if 1 //for Linux yamada
	//...
#else
#ifndef _WIN32_WCE
	HRESULT hr = SHGetSpecialFolderPath(
		NULL,
		szDir,
		CSIDL_INTERNET_CACHE,
		true);
#else
	// Windows CE doesn't support temporary Internet files folder
	HRESULT hr = E_FAIL;
#endif
	if (FAILED(hr))
	{
		// use alternative API
		GetTempPath(MAX_PATH, szDir);
	}
#endif //for Linux yamada

	return szDir;
}

tstring GetTempDir(void)
{
	TCHAR szDir[MAX_PATH];
	memset(szDir, 0, sizeof(szDir));

#if 1 //for Linux yamada
	//...
#else
	GetTempPath(MAX_PATH, szDir);
#endif //for Linux yamada

	return szDir;
}


bool IsSupportedImageFormat(const tstring& sFileName)
{
	string sExt = GetFileExtension(sFileName).mbs();

	// video clip
	if (sExt == "avi" || sExt == "mpeg" || sExt == "mpg")
		return true;

	if (sExt == "bmp" || sExt == "gif" || sExt == "png" || sExt == "jpg" || sExt == "jpeg")
		return true;

	return false;
}
