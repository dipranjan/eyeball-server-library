/********************************************************************
 * 
 * File: module.cpp
 * Author: Infranet Solutions Ltd. 1999
 * Copyright: Infranet Solutions Ltd. 1999
 *
 * Purpose: Base class for modules
			Bass class of capture, encoder, network, RTP_Session classes
 * 
 * Change Log: 
 *
 *******************************************************************/

#include "Base.h"  // automatic use of precompiled headers 

#include "Module.h"
#include "debug.h"
#include "critsec.h"
#include "ReturnCodes.h"



Module::Module() :
	m_pModuleMutex(NULL)
{
	m_pModuleMutex = new Mutex;
}

Module::~Module()
{
	DB_ASSERT(m_moduleMap.empty() && "the module map is not empty!");
	delete m_pModuleMutex;
}

int Module::TargetBatchProcess(Buffer* pBuffer)
{
	Critical_Section cs(*m_pModuleMutex);

	if (m_moduleMap.size() == 0)
		return ES_FAILURE;

	ModuleMap::iterator iter;

	int iRet = ES_SUCCESS;

	for (iter = m_moduleMap.begin(); iter != m_moduleMap.end(); iter++)
		iRet = iter->second->Process(pBuffer);

	return iRet;
}


BOOL Module::AddTarget(Module* pTargetModule, 
					   int tmID)
{
	Critical_Section cs(*m_pModuleMutex);

	if (m_moduleMap.find(tmID) != m_moduleMap.end())
		return FALSE;

	m_moduleMap[tmID] = pTargetModule;

	return TRUE;
}


BOOL Module::RemoveTarget(int tmID)
{
	Critical_Section cs(*m_pModuleMutex);

	if (m_moduleMap.find(tmID) == m_moduleMap.end())
		return FALSE;

	m_moduleMap.erase(tmID);

	return TRUE;
}


BOOL Module::RemoveAllTargets()
{
	Critical_Section cs(*m_pModuleMutex);

	m_moduleMap.clear();

	return TRUE;
}

BOOL Module::AcceptData()
{
	return TRUE;
}

BOOL Module::TargetAcceptData()
{
	Critical_Section cs(*m_pModuleMutex);

	ModuleMap::iterator iter;

	BOOL bAccept = TRUE;
	for (iter = m_moduleMap.begin(); iter != m_moduleMap.end(); iter++)
	{
		bAccept &= iter->second->AcceptData();
	}

	return bAccept;
}
