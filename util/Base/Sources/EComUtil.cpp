// ---------------------------------------------------------------------------
// File:       EComUtil.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2002
//
// Purpose:    COM Utilities
// 
// Change Log:
// ---------------------------------------------------------------------------

#include "Base.h"  // automatic use of precompiled headers 

#include "EComUtil.h"

using std::string;

CoInit::CoInit()
{
	m_dwThreadId = GetCurrentThreadId();
    m_bOk = true;


	// Initialize COM
#if _WIN32_WINNT >= 0x0400 & defined(_ATL_FREE_THREADED) | defined(_WIN32_WCE)
    HRESULT hRes = CoInitializeEx(NULL, COINIT_MULTITHREADED);
#else
    HRESULT hRes = CoInitialize(NULL);
#endif


	if (FAILED(hRes))
	{
		DB_ASSERT(0 && "COM Initialization failed.");
		m_bOk = false;
	}
}

CoInit::~CoInit()
{
	// This object needs to be destroyed by the same thread that created it.
	DWORD dwThreadId = GetCurrentThreadId();
	if (m_dwThreadId != dwThreadId)
	{
		DB_ASSERT(0 && "Object destroyed by incorrect thread");
		m_bOk = false;
	}

	if (m_bOk)
	{
		// Release COM
		CoUninitialize();
	}
}

CComBSTR eyeball::GetUrlFromClientSite(IOleClientSite* pClientSite)
{
	if (NULL == pClientSite)
		return "";

	// Obtain URL from container moniker.
	CComPtr<IMoniker> spmk;
	
	HRESULT hr = 0;
	hr = pClientSite->GetMoniker(
		OLEGETMONIKER_TEMPFORUSER,
		OLEWHICHMK_CONTAINER,
		&spmk);
	if (FAILED(hr))
		return "";

	LPOLESTR pszDisplayName;
	hr = spmk->GetDisplayName(NULL, NULL, &pszDisplayName);
	if (FAILED(hr))
		return "";

	CComBSTR bstrURL(pszDisplayName);
	CoTaskMemFree((LPVOID)pszDisplayName);

	return bstrURL;
}

void eyeball::StringFromBstr(string& sOut, const BSTR bstrIn)
{
	if (!bstrIn)
	{
		// Create an empty string
		sOut.erase();
		return;
	}

	USES_CONVERSION;
	sOut = OLE2CA(bstrIn);
}

void eyeball::StringFromBstr(tstring& sOut, const BSTR bstrIn)
{
	if (!bstrIn)
	{
		// Create an empty string
		sOut.erase();
		return;
	}

	USES_CONVERSION;
	sOut = OLE2CT(bstrIn);
}

const tstring eyeball::ToString(const BSTR bstr)
{
	tstring s;
	StringFromBstr(s, bstr);

	return s;
}

const std::string eyeball::ToStringA(const BSTR bstr)
{
	if (NULL == bstr)
		return std::string();

	USES_CONVERSION;
	return OLE2CA(bstr);
}

int eyeball::BstrFromString(BSTR* pbstrOut, const tstring& sIn)
{
	if (pbstrOut == NULL)
		return 0;

	if (*pbstrOut != NULL)
	{
		DB_ASSERT(0 && "possible memory leak");
	}

	if (0 == sIn.length())
	{
		*pbstrOut = NULL;
		return 0;
	}

	CComBSTR bstr(sIn.c_str());

	// Commit to pbstrOut
	HRESULT hr = bstr.CopyTo(pbstrOut);
	if (FAILED(hr))
		return 0;

	return bstr.Length();
}

int eyeball::BstrFromString(CComBSTR& bstrOut, const tstring& sIn)
{
	return BstrFromString(&bstrOut, sIn);
}

const CComBSTR eyeball::ToBstr(const tstring& sIn)
{
	CComBSTR bstr(sIn.c_str());
	return bstr;
}

HRESULT eyeball::CopyBstr(BSTR* pbstrOut, const BSTR bstrIn)
{
	DB_ASSERT(NULL != pbstrOut);
	if (NULL == pbstrOut)
		return E_POINTER;

	if (!bstrIn)
	{
		*pbstrOut = NULL;
		return S_OK;
	}

	*pbstrOut = SysAllocStringLen(bstrIn, SysStringLen(bstrIn));

	if (NULL == pbstrOut)
		return E_OUTOFMEMORY;

	return S_OK;
}

VARIANT_BOOL eyeball::ToVB(BOOL b)
{
	return b ? VARIANT_TRUE : VARIANT_FALSE;
}

VARIANT_BOOL eyeball::ToVB(bool b)
{
	return b ? VARIANT_TRUE : VARIANT_FALSE;
}

BOOL eyeball::ToBOOL(VARIANT_BOOL b)
{
	return b == VARIANT_FALSE ? FALSE : TRUE;
}

bool eyeball::Tobool(VARIANT_BOOL b)
{
	return b == VARIANT_FALSE ? false : true;
}


bool IsActiveXControlRegistered(const tstring& sProgId)
{
	CComPtr<IUnknown> pUnknown;
	pUnknown.CoCreateInstance(eyeball::ToBstr(sProgId));
	if (!pUnknown)
		return false;

	return true;
}

typedef HRESULT(* fnStdApi)(void);

bool RegisterActiveXControl(const tstring& sFileName)
{
	bool bRet = false;

	HINSTANCE hLib = LoadLibrary(sFileName.c_str());
	if (NULL == hLib)
		return false;

#ifndef _WIN32_WCE
	fnStdApi ptr = (fnStdApi)GetProcAddress(hLib, "DllRegisterServer");
#else
	fnStdApi ptr = (fnStdApi)GetProcAddress(hLib, TEXT("DllRegisterServer"));
#endif

	if (NULL != ptr)
	{
#ifndef _WIN32_WCE
		OleInitialize(NULL);
#else
		CoInitializeEx(NULL, COINIT_MULTITHREADED);
#endif

		HRESULT hr = ptr();
		if (SUCCEEDED(hr))
			bRet = true;
#ifndef _WIN32_WCE
		OleUninitialize();
#else
		CoUninitialize();
#endif
	}

	FreeLibrary(hLib);
	
	return bRet;
}

bool eyeball::CheckActiveXControl(
	const tstring& sProgId,
	const tstring& sFileName)
{
	if (IsActiveXControlRegistered(sProgId))
		return true;

	if (!RegisterActiveXControl(sFileName))
		return false;

	return true; //IsActiveXControlRegistered(sProgId);
}
