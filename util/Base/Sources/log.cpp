// $Id: log.cpp,v 1.1.1.1 2005/12/24 04:04:31 sazzad Exp $

#include <fstream>
#include <unistd.h>		// getpid()
#include <stdio.h>		// sprintf()
#include <time.h>		// cftime()

#include "log.h"

#include "debug.h"

using std::string;

// Instantiate static variables
ostream *Log::log_out;

void Log::open(const string& _filename)
{
	ASSERT(!log_out);
	WARN(log_out = new ofstream(_filename.c_str(), ios::app));
}

void Log::close(void)
{
	if (!log_out)
		return;

	*log_out << flush;

	delete log_out;
	log_out = NULL;
}

string Log::generate_filename(const string& _name)
{
	char pid[5];
#ifdef __linux__
	sprintf(pid, "%d", getpid());
#else
	sprintf(pid, "%ld", getpid());
#endif

	return _name + pid + ".log";
}

string Log::localtime(void)
{
	// mm/dd/yyyy HH:MM:SS
	// 1234567891123456789
	char buf[20];

	time_t tNow = time(NULL);
	strftime(buf, sizeof(buf), "%Y-%m-%d %T", ::localtime(&tNow));

	return buf;
}

