#include "Base.h"
#include "critsec.h"
#include "DataStat.h"
#include "Timer.h"

using std::list;


#define DEF_PERIOD 10000
#define DEF_UNIT   1

#define ENTER_CRIT_SEC() Critical_Section cs(*m_pMutex)

#if 0
#define DS_ASSERT(X) ASSERT(X)
#else
#define DS_ASSERT(X) DB_ASSERT(X)
#endif


DataStat::DataStat():
	m_dataQ(),
	m_pMutex(NULL),
	m_iPeriod(DEF_PERIOD),
	m_rUnit(DEF_UNIT),
	m_rStat(0),
	m_iData(0)
{
	m_pMutex = new Mutex;
}


DataStat::~DataStat()
{
	Reset();

	SAFE_DELETE(m_pMutex);
}


void DataStat::Reset()
{
	ENTER_CRIT_SEC();

	m_dataQ.clear();

	m_rStat = 0;

	m_iData = 0;
}

void DataStat::SetPeriod(int iPeriod)
{
	ENTER_CRIT_SEC();
	
	m_iPeriod = iPeriod;
}


void DataStat::SetUnit(double rUnit)
{
	ENTER_CRIT_SEC();
	
	m_rUnit = rUnit;
}

void DataStat::AddData(int iData)
{
	ENTER_CRIT_SEC();

	DataEntry de;

	de.iData = iData;
	de.dwTime = timeGetTime();

	m_dataQ.push_back(de);

	m_iData += iData;

	if (m_dataQ.size() <= 1)
		return;

	DataEntry front = m_dataQ.front();

	if (de.dwTime < front.dwTime)
	{
		Reset();
		m_rStat = 0;
		return;
	}

	int iInterval = int(de.dwTime - front.dwTime);

	while (iInterval > m_iPeriod)
	{
		m_iData -= front.iData;

		m_dataQ.pop_front();
		
		if (m_dataQ.empty())
		{
			m_rStat = 0;
			return;
		}

		front = m_dataQ.front();

		if (de.dwTime < front.dwTime)
		{
			Reset();
			m_rStat = 0;
			return;
		}		
		
		iInterval = int(de.dwTime - front.dwTime);
	}

	if (iInterval  < m_iPeriod/5)
	{
		m_rStat = 0;
		return;
	}

	m_rStat = m_iData * 1000.0 / iInterval;
}


double DataStat::GetStat()
{
	ENTER_CRIT_SEC();
	
	return m_rStat * m_rUnit;
}




DataStat2::DataStat2():
	m_dataQ(),
	m_pMutex(NULL),
	m_iPeriod(DEF_PERIOD),
	m_rUnit(DEF_UNIT),
	m_rStat(0),
	m_dwFailed(0),
	m_dwSucceeded(0),
	m_iMode(0)
{
	m_pMutex = new Mutex;
}


DataStat2::~DataStat2()
{
	Reset();

	SAFE_DELETE(m_pMutex);
}


void DataStat2::Reset()
{
	ENTER_CRIT_SEC();

	m_dataQ.clear();

	m_rStat = 0;

	m_dwFailed = 0;

	m_dwSucceeded = 0;
}

void DataStat2::SetPeriod(int iPeriod)
{
	ENTER_CRIT_SEC();
	
	m_iPeriod = iPeriod;
}



void DataStat2::SetUnit(double rUnit)
{
	ENTER_CRIT_SEC();
	
	m_rUnit = rUnit;
}

void DataStat2::SetMode(int iMode)
{
	ENTER_CRIT_SEC();

	if (iMode < 0 || iMode > 1)
		return;

	m_iMode = iMode;
}


void DataStat2::AddData(DWORD dwFailed, DWORD dwSucceeded)
{
	ENTER_CRIT_SEC();

	if (m_iMode == 0)
		UpdateStatMode0(dwFailed, dwSucceeded);

	if (m_iMode == 1)
		UpdateStatMode1(dwFailed, dwSucceeded);
}


void DataStat2::UpdateStatMode0(DWORD dwFailed, DWORD dwSucceeded)
{
	DataEntry2 de;

	de.dwFailed = dwFailed;
	de.dwSucceeded = dwSucceeded;
	de.dwTime = timeGetTime();

	m_dataQ.push_back(de);

	// accumulated value
	m_dwFailed += dwFailed;
	m_dwSucceeded += dwSucceeded;

	if (m_dataQ.size() <= 1)
	{
		m_rStat = 0;
		return;
	}

	DataEntry2 front = m_dataQ.front();

	if (de.dwTime < front.dwTime)
	{
		DS_ASSERT(0);
		Reset();
		return;
	}

	int iInterval = int(de.dwTime - front.dwTime);

	while (iInterval > m_iPeriod)
	{
		if (m_dwFailed < front.dwFailed || 
			m_dwSucceeded < front.dwSucceeded)
		{
			DS_ASSERT(0);
			Reset();
			return;
		}

		m_dwFailed -= front.dwFailed;
		m_dwSucceeded -= front.dwSucceeded;

		m_dataQ.pop_front();
		
		if (m_dataQ.empty())
		{
			m_rStat = 0;
			return;
		}

		front = m_dataQ.front();

		if (de.dwTime < front.dwTime)
		{
			DS_ASSERT(0);
			Reset();
			return;
		}

		iInterval = int(de.dwTime - front.dwTime);
	}

	if (iInterval  < m_iPeriod/5)
	{
		m_rStat = 0;
		return;
	}

	if (m_dwFailed == 0)
		m_rStat = 0;
	else
		m_rStat = m_dwFailed  * 1.0 / (m_dwFailed + m_dwSucceeded);
}



void DataStat2::UpdateStatMode1(DWORD dwFailed, DWORD dwSucceeded)
{
	DataEntry2 de;

	de.dwFailed = dwFailed;
	de.dwSucceeded = dwSucceeded;
	de.dwTime = timeGetTime();

	m_dataQ.push_back(de);

	if (m_dataQ.size() <= 1)
	{
		m_rStat = 0;
		return;
	}

	DataEntry2 front = m_dataQ.front();

	if (de.dwTime < front.dwTime)
	{
		DS_ASSERT(0);
		Reset();
		return;
	}

	int iInterval = int(de.dwTime - front.dwTime);

	while (iInterval > m_iPeriod)
	{
		m_dataQ.pop_front();
		
		if (m_dataQ.empty())
		{
			m_rStat = 0;
			return;
		}

		front = m_dataQ.front();

		if (de.dwTime < front.dwTime)
		{
			DS_ASSERT(0);
			Reset();
			return;
		}

		iInterval = int(de.dwTime - front.dwTime);
	}

	if (iInterval  < m_iPeriod/5)
	{
		m_rStat = 0;
		return;
	}

	if (de.dwFailed < front.dwFailed || 
		de.dwSucceeded < front.dwSucceeded)
	{
		Reset();
		return;
	}

	dwFailed = de.dwFailed - front.dwFailed;

	dwSucceeded = de.dwSucceeded - front.dwSucceeded;

	if (dwFailed == 0)
	{
		m_rStat = 0;
		return;
	}

	m_rStat = dwFailed * 1.0 / (dwFailed + dwSucceeded);
}


double DataStat2::GetStat()
{
	ENTER_CRIT_SEC();
	
	return m_rStat * m_rUnit;
}

