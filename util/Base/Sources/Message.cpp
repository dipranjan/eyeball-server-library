// ---------------------------------------------------------------------------
// File:       Message.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2002
//
// Purpose:    Base class for network messages
//
// Change Log:
// ---------------------------------------------------------------------------

//#ifdef WIN32
#include "Base.h"  // automatic use of precompiled headers
//#endif

#ifdef WIN32
//#include <winsock2.h>		// hton*, ntoh*
#else
#include <netinet/in.h>
#endif

#include "Message.h"

using std::string;


// ---------------------------------------------------------------------------
// CommandMessage struct
// ---------------------------------------------------------------------------

CommandMessage::CommandMessage()
:
	m_u8Version(MESSAGE_VERSION),
	m_u8MessageType(0),
	m_u16PayloadLength(0),

	m_u16Command(0),
	m_u16Value(0)
{
}

CommandMessage::CommandMessage(u_int8_t  _u8MessageType)
:
	m_u8Version(MESSAGE_VERSION),
	m_u8MessageType(_u8MessageType),
	m_u16PayloadLength(0),

	m_u16Command(0),
	m_u16Value(0)
{
}

void CommandMessage::serialize(string& sBuf) const
{
	sBuf.erase();

	SERIALIZE8(m_u8Version);
	SERIALIZE8(m_u8MessageType);

	// Use dynamically assigned local length for possible derived class(es)
	// so we can keep this method const.
	u_int16_t u16PayloadLength = payload_length();
	SERIALIZE16(u16PayloadLength);

	SERIALIZE16(m_u16Command);
	SERIALIZE16(m_u16Value);
}

int CommandMessage::deserialize(const string& sBuf)
{
	int iOffset = 0;

	if (sBuf.size() < CM_LENGTH)
		return -1;

	DESERIALIZE8(m_u8Version);
	DESERIALIZE8(m_u8MessageType);
	DESERIALIZE16(m_u16PayloadLength);

	DESERIALIZE16(m_u16Command);
	DESERIALIZE16(m_u16Value);

	return iOffset;
}

unsigned int CommandMessage::size(void) const
{
	return CM_LENGTH + payload_length();
}

#ifndef _WIN32_WCE
ostream& operator<<(ostream& s, const CommandMessage& v)
{
	s << (unsigned int)v.m_u8Version
		<< " " << (unsigned int)v.m_u8MessageType
		<< " " << v.m_u16PayloadLength

		<< " " << v.m_u16Command
		<< " " << v.m_u16Value;

	return s;
}
#endif
