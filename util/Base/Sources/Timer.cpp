// ---------------------------------------------------------------------------
// File:       Timer.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2002
//
// Purpose:    Wrapper class for multimedia timer
// 
// Change Log:
// ---------------------------------------------------------------------------

#include "Base.h"  // automatic use of precompiled headers 

#if 1 //for Linux yamada
#include "Event.h"//
#else
#include <windows.h>
#include <mmsystem.h>
#endif //for Linux yamada

#include "critsec.h"
#include "debug.h"
#include "ReturnCodes.h"
#include "Timer.h"
#include "Thread.h"
#include "TextUtil.h"

//#define DEBUG_TIMER

Timer::Timer()
:
	m_pMutex(new Mutex),

	m_nIDTimer(NULL),
	m_pfnCallback(NULL),
	m_pUserArg(NULL),

	m_fuEvent(0)
{
#ifdef DEBUG_TIMER
	DB("Timer::Timer");
#endif
}

Timer::~Timer()
{
#ifdef DEBUG_TIMER
	DB("Timer::~Timer");
#endif

	close();

	SAFE_DELETE(m_pMutex);
}

bool Timer::internal_open(TIMERCALLBACK pfnCallback, UINT nDelay, UINT nRes, UINT fuEvent, void *pUserArg)
{
	Critical_Section critsec(*m_pMutex);

    DB_ASSERT(pfnCallback);
    DB_ASSERT(nDelay > 10);

	//DB_ASSERT(m_nIDTimer == NULL);
	RFI(m_nIDTimer != NULL);

	// Resolution is by default set to zero for highest possible accuracy - according to
	// MSDN this may unecessarily increase overhead
	m_nIDTimer = timeSetEvent(nDelay, nRes, TimeProc, (uintptr_t) this, fuEvent);

	RFIN(m_nIDTimer);

	m_pfnCallback = pfnCallback;
    m_pUserArg    = pUserArg;
	m_fuEvent     = fuEvent;

	return true;
}

bool Timer::open_periodic(TIMERCALLBACK pfnCallback, UINT nPeriod, UINT nRes, void *pUserArg)
{
#ifdef DEBUG_TIMER
	DB("Timer::open_periodic");
#endif

	return internal_open(pfnCallback, nPeriod, nRes, TIME_PERIODIC, pUserArg);
}

// high-resolution one-shot
bool Timer::open_oneshot(TIMERCALLBACK pfnCallback, UINT nDelay, UINT nRes, void *pUserArg)
{
#ifdef DEBUG_TIMER
	DB("Timer::open_oneshot");
#endif

    return internal_open(pfnCallback, nDelay, nRes, TIME_ONESHOT, pUserArg);
}

bool Timer::close(void)
{
#ifdef DEBUG_TIMER
	DB("Timer::close");
#endif

	Critical_Section critsec(*m_pMutex);

	bool rc(true);

	if (!m_nIDTimer)
		rc = false;
	else
	{
		WARN(timeKillEvent(m_nIDTimer) == TIMERR_NOERROR);
		m_nIDTimer = NULL;
	}

	m_pfnCallback = NULL;
	m_pUserArg    = NULL;
	m_fuEvent     = 0;

	return rc;
}

// Timer proc for multimedia timer callback set with timeSetEvent().
//
// Calls procedure specified when Timer object was created. The 
// dwUser parameter contains "this" pointer for associated Timer object.
// 
#if 1 //for Linux yamada
#define UNREFERENCED_PARAMETER(P) (P)
void Timer::TimeProc(UINT uID, UINT uMsg, DWORD dwUser, DWORD dw1, DWORD dw2)
#else
void CALLBACK Timer::TimeProc(UINT uID, UINT uMsg, DWORD dwUser, DWORD dw1, DWORD dw2)
#endif //for Linux yamada
{
	UNREFERENCED_PARAMETER(uID);
	UNREFERENCED_PARAMETER(uMsg);
	UNREFERENCED_PARAMETER(dw1);
	UNREFERENCED_PARAMETER(dw2);

	// dwUser contains ptr to Timer object
	Timer *pTimer = (Timer *) dwUser;

	// Lock critical section for the entire duration of the callback.  This is acceptable
	// even if the user callback accesses open() or close() since the mutex can be re-acquired
	// by the same thread.
//#ifndef _WIN32_WCE	
	Critical_Section critsec(*pTimer->m_pMutex);
//#endif
	// There is a possibility for a race condition where this callback thread is pre-empted
	// and another thread calls close before this point.  Thus check that timer ID is still
	// valid after we acquire the critical section, and exit otherwise.
	if (pTimer->m_nIDTimer == NULL)
	{
		DB("Timer::TimeProc - timer already closed by another thread");
		return;
	}

//#ifndef _WIN32_WCE
#if 1
	// If it's a one-shot, then null out timer ID so they can potentially open again in
	// callback and avoid asserting.  Otherwise keep ticking and wait for close()
	if (pTimer->m_fuEvent == TIME_ONESHOT)
		pTimer->m_nIDTimer = NULL;
#endif

	// Call user-specified callback and pass back user specified data
	(pTimer->m_pfnCallback) (pTimer->m_pUserArg);
}
bool Timer::open_periodic(HANDLE hEvent, UINT nPeriod, UINT nRes)
{
	Critical_Section critsec(*m_pMutex);

	if (m_nIDTimer)
		return false;

	m_nIDTimer = timeSetEvent(nPeriod,                //event delay, in milliSeconds
		                      nRes,                   //resolution in milliSeconds
							  (LPTIMECALLBACK)hEvent, //handle to the event object
							  NULL,                   //user callback data
							  TIME_PERIODIC|TIME_CALLBACK_EVENT_SET); //timer event type

	return true;
}

bool Timer::open_oneshot(HANDLE hEvent, UINT nPeriod, UINT nRes)
{
	Critical_Section critsec(*m_pMutex);

	if (m_nIDTimer)
		return false;

	m_nIDTimer = timeSetEvent(nPeriod,                //event delay, in milliSeconds
		                      nRes,                   //resolution in milliSeconds
							  (LPTIMECALLBACK)hEvent, //handle to the event object
							  NULL,                   //user callback data
							  TIME_ONESHOT|TIME_CALLBACK_EVENT_SET); //timer event type

	return true;
}


#if 0 //for Linux yamada
// ---------------------------------------------------------------------------
// StopWatch class implementation
// ---------------------------------------------------------------------------

StopWatch::StopWatch() :
	m_llTotalTime(0),
	m_nCount(0)
{
	m_liStartTime.QuadPart = 0;
	m_liStopTime.QuadPart = 0;
	QueryPerformanceFrequency(&m_liFrequency);
}

void StopWatch::Reset()
{
	m_llTotalTime = 0;
	m_nCount = 0;
	m_liStartTime.QuadPart = 0;
	m_liStopTime.QuadPart = 0;
}

void StopWatch::Start()
{
	QueryPerformanceCounter(&m_liStartTime);
}

void StopWatch::Stop()
{
	QueryPerformanceCounter(&m_liStopTime);
	m_llTotalTime += m_liStopTime.QuadPart - m_liStartTime.QuadPart;
	m_nCount++;
}

void StopWatch::PrintCurrent()
{
	if (0 == m_liFrequency.QuadPart)
		return;

	QueryPerformanceCounter(&m_liStopTime);
	LONGLONG llCurrentTime = m_liStopTime.QuadPart - m_liStartTime.QuadPart;

	float fElapsedTime = ( 1000.0f * (float)llCurrentTime ) / 
					(float)m_liFrequency.QuadPart;

	char buf[100];
	sprintf(buf, "Current time: %6.4f [ms]\n", fElapsedTime);

	//OutputDebugStringA(buf);
	printf(buf);
}

void StopWatch::PrintAverage(int _nInterval)
{
	if (m_nCount % _nInterval == 0)
	{
		char buf[256];
	
		float fElapsedTime = ( 1000.0f * (float)m_llTotalTime ) / 
					( (float)m_nCount * (float)m_liFrequency.QuadPart );

		if (m_nCount == 1)
		{
			sprintf(buf, "Elapsed time: %6.4f [ms]\n", fElapsedTime);
		}
		else
		{
			sprintf(buf, "Count: %d  Average time: %6.4f [ms]\n", 
				m_nCount, fElapsedTime);
		}

		//OutputDebugStringA(buf);
		printf(buf); // causes BoundsChecker error?
	}
}

void StopWatch::PrintTotal(int _nInterval)
{
	if (0 == m_liFrequency.QuadPart)
	{
		DB_ASSERT(0);
		return;
	}

	if (m_nCount % _nInterval == 0)
	{
		char buf[256];

		float fElapsedTime = ( 1000.0f * (float)m_llTotalTime ) / 
					(float)m_liFrequency.QuadPart;

		if (m_nCount == 1)
		{
			sprintf(buf, "Total time: %6.4f [ms]\n", fElapsedTime);
		}
		else
		{
			sprintf(buf, "Count: %d  Total time: %6.4f [ms]\n", 
				m_nCount, fElapsedTime);
		}

		//OutputDebugStringA(buf);
		printf(buf); // causes BoundChecker error?
	}
}

UINT StopWatch::GetMilliseconds(void)
{
	QueryPerformanceCounter(&m_liStopTime);
	LONGLONG llElapsedTime = m_liStopTime.QuadPart - m_liStartTime.QuadPart;

	double fElapsedTime = ( 1000.0f * (double)llElapsedTime ) / 
					(double)m_liFrequency.QuadPart;

	return (UINT)(fElapsedTime + 0.5);
}
#endif //for Linux yamada

#ifdef _WIN32_WCE

// Timer Event structure
struct TimerEvent
{
	TimerEvent():
		m_bPeriodic(false),
		m_uTriggerTime(0),
		m_uDelay(0),
		m_pTimeProc(NULL),
		m_dwUser(0)
	{
	}

  // one shot or periodic
  bool m_bPeriodic;

  // in milliseconds.
  UINT m_uTriggerTime; 

  // undefined if m_bPeriodic == false.
  UINT m_uDelay;

  LPTIMECALLBACK m_pTimeProc;
  DWORD m_dwUser;
};




// Timer Event helper variable

class TimerThread
{
public:
	TimerThread()
	{
		m_hWakeupEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
		m_pAccessMutex = new Mutex;
		m_bContinue = true;
		m_uTimerID = 1;
		m_bEnableDebug = false;
		m_pThread = new Thread(TimerEventThread, this);
	}

	~TimerThread()
	{
		m_bContinue = false;
		SetEvent(m_hWakeupEvent);
		m_pThread->join();
		SAFE_DELETE(m_pThread);
		SAFE_DELETE(m_pAccessMutex);
#if 1 //for Linux yamada
		CLOSE_EVENT(m_hWakeupEvent);
#else
		CLOSE_HANDLE(m_hWakeupEvent);
#endif //for Linux yamada
	}

	void* ThreadImpl();

	MMRESULT timeSetEvent(
		UINT delay,
		UINT resolution,
		LPTIMECALLBACK lpTimeProc, 
		DWORD dwUser,
		UINT fuEvent);

	MMRESULT timeKillEvent(UINT uTimerID);

private:
	typedef std::map<UINT, TimerEvent> TimerEventMap;
	TimerEventMap m_TimerEventMap;

	HANDLE  m_hWakeupEvent;
	Mutex*  m_pAccessMutex;
	Thread* m_pThread;
	UINT	m_uTimerID;
	volatile bool m_bContinue;
	bool    m_bEnableDebug;

private:
	static void* TimerEventThread(void *arg);
	bool GetEarliestEvent(UINT& uTimerID);
};

static TimerThread* s_pTimerThread = NULL;
using std::map;

// Timer Event helper functions
bool TimerThread::GetEarliestEvent(UINT& uTimerID)
{
	if (m_TimerEventMap.size() == 0)
		return false;

	UINT uTriggerTime = UINT_MAX;
 
	TimerEventMap::iterator iter = m_TimerEventMap.begin();

	for (; iter != m_TimerEventMap.end(); iter++)
	{
		TimerEvent& info = iter->second;
		if (info.m_uTriggerTime < uTriggerTime)
		{
			uTriggerTime = info.m_uTriggerTime;
			uTimerID = iter->first;
		}
	}
	return true;
}


void* TimerThread::TimerEventThread(void* arg)
{
	TimerThread* pThis = (TimerThread*)arg;
	return pThis->ThreadImpl();
}

void* TimerThread::ThreadImpl()
{
#if 1 //for Linux yamada
	DWORD dwWakeupTime = timeGetTime();
#else
	OutputDebugString(L"Starting timer event thread...\n");
	DWORD dwWakeupTime = GetTickCount();
#endif //for Linux yamada

	while (m_bContinue)
	{
		// After the Critical_Section, if foundEvent == false then there are no
		// events in the event map and the other variables are all undefined.
		// Otherwise, if foundToProcess is true then eventToProcess
		// is set to an event to process.  Otherwise, millisToWait is an amount of
		// time to wait until the next event triggers.
		UINT uTimerID = 0;
		int iWait = 20000;
		bool bTrigger = false;
		TimerEvent te;
		if (true)
		{
			Critical_Section critsec(*m_pAccessMutex);
			if (GetEarliestEvent(uTimerID))
			{
				DWORD uNow = timeGetTime();
				te = m_TimerEventMap[uTimerID];
				if (uNow >= (te.m_uTriggerTime - 20))
				{
					m_TimerEventMap.erase(uTimerID);
					if (te.m_bPeriodic)
					{
						te.m_uTriggerTime += te.m_uDelay;
						m_TimerEventMap[uTimerID] = te;
					}
					if (te.m_bPeriodic && te.m_uTriggerTime <= uNow)
					{
						// far behind, ingored
						bTrigger = false;
						iWait = 0;
					}
					else
						bTrigger = true;
				}
				else
				{
					iWait = int (te.m_uTriggerTime - uNow);
				}
			}
		}
		if (bTrigger)
		{
#if 0 //for Linux yamada
			if (m_bEnableDebug)
			{
				TCHAR sz[100];
				wsprintf(sz, L"%s%u%s", L"Timer thread callback: ", uTimerID, L"\n");
				OutputDebugString(sz);
			}
#endif //for Linux yamada

			te.m_pTimeProc(uTimerID, 0, te.m_dwUser, 0, 0);
		
#if 0 //for Linux yamada
			if (m_bEnableDebug)
			{
				DWORD dwElapsed = GetTickCount() - dwWakeupTime;
				dwWakeupTime += dwElapsed;
				TCHAR sz[100];
				wsprintf(sz, L"%s%u%s", L"Timer thread wakes up interval: ", dwElapsed, L"\n");
				OutputDebugString(sz);
			}
#endif //for Linux yamada

			continue;
		}
    
		if (iWait > 20000)
		{
#if 0 //for Linux yamada
			TCHAR sz[100];
			wsprintf(sz, L"%s%i%s", L"Timer thread wait interval: ", iWait/1000, L"\n");
			OutputDebugString(sz);
#endif //for Linux yamada

			iWait = 20000;
		}

		if (iWait < 20)
			continue;

		WaitForSingleObject(m_hWakeupEvent, iWait);

#if 0 //for Linux yamada
		if (m_bEnableDebug)
		{
			DWORD dwElapsed = GetTickCount() - dwWakeupTime;
			dwWakeupTime += dwElapsed;
			TCHAR sz[100];
			wsprintf(sz, L"%s%u%s", L"Timer thread breaks interval: ", dwElapsed, L"\n");
			OutputDebugString(sz);
		}
#endif //for Linux yamada

	}

#if 0 //for Linux yamada
	OutputDebugString(L"Exiting timer event thread...\n");
#endif //for Linux yamada

	return NULL;
}




MMRESULT TimerThread::timeSetEvent(
	UINT delay,
	UINT resolution,
	LPTIMECALLBACK lpTimeProc, 
    DWORD dwUser,
	UINT fuEvent)
{
	TimerEvent te;
	te.m_bPeriodic = ((fuEvent & TIME_PERIODIC) != 0);
	if (te.m_bPeriodic)
		te.m_uDelay = delay;
	te.m_uTriggerTime = timeGetTime() + delay;
	te.m_pTimeProc = lpTimeProc;
	te.m_dwUser = dwUser;

	UINT uTimerID = m_uTimerID++;
	
	if (m_uTimerID == 0)
		m_uTimerID = 1;

#if 0 //for Linux yamada
	if (m_bEnableDebug)
	{
		TCHAR sz[100];
		wsprintf(sz, L"%s%u%s%u%s", L"Timer set event: ", uTimerID, L" Periodic: ", te.m_bPeriodic,  L"\n");
		OutputDebugString(sz);
	}
#endif //for Linux yamada

	Critical_Section critsec(*m_pAccessMutex);
	m_TimerEventMap[uTimerID] = te;

	// wake up the thread
	SetEvent(m_hWakeupEvent);

	return uTimerID;
}

MMRESULT TimerThread::timeKillEvent(UINT uTimerID)
{
#if 0 //for Linux yamada
	if (m_bEnableDebug)
	{
		TCHAR sz[100];
		wsprintf(sz, L"%s%u%s", L"Timer kill event: ", uTimerID, L"\n");
		OutputDebugString(sz);
	}
#endif //for Linux yamada

	if (true)
	{
		Critical_Section critsec(*m_pAccessMutex);
		m_TimerEventMap.erase(uTimerID);
	}

	SetEvent(m_hWakeupEvent);
	return TIMERR_NOERROR;
}

MMRESULT timeKillEvent(UINT uTimerID)
{
	if (s_pTimerThread)
	{
		s_pTimerThread->timeKillEvent(uTimerID);
	}
	return TIMERR_NOERROR;
}


MMRESULT timeSetEvent(
	UINT delay,
	UINT resolution,
	LPTIMECALLBACK lpTimeProc, 
    DWORD dwUser,
	UINT fuEvent)
{
	CreateTimerThread();
	return s_pTimerThread->timeSetEvent(delay, resolution, lpTimeProc, dwUser, fuEvent);
}


DWORD timeGetTime(void)
{
	// Win32: The timeGetTime function retrieves the system time, in milliseconds.
	// The system time is the time elapsed since Windows was started.

	// WinCE: The GetTickCount function retrieves the number of milliseconds
	// that have elapsed since Windows CE was started. 

#if 1 //for Linux yamada	
	timeval tv;
	gettimeofday(&tv, NULL);
	return (1000 * tv.tv_sec + tv.tv_usec / 1000);
#else
	return GetTickCount();
#endif //for Linux yamada
}

void CreateTimerThread()
{
	if (!s_pTimerThread)
	{
		s_pTimerThread = new TimerThread;
	}
}

void CloseTimerThread()
{
	SAFE_DELETE(s_pTimerThread);
}

#endif
