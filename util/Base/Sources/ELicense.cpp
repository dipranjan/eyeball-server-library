// ---------------------------------------------------------------------------
// File:       ELicense.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2002
//
// Purpose:    Implementation of ELicense - Licensing Functions (LPK-based)
//
// Notes:      For more information regarding LPK-based ActiveX Licensing,
//             see MSDN, keyword CComClassFactory2.
//             
//             LPK_Tool.exe can be used to generate LPK files.  Design-time
//             licensing must be enabled for this to work.
// ---------------------------------------------------------------------------

#include "Base.h"  // automatic use of precompiled headers 

#include "EAtlUtil.h"
#include "ELicense.h"
#include "EComUtil.h"
#include "EFileUtil.h"

using std::string;
using namespace eyeball;

extern CComEyeballModule _Module;


// ---------------------------------------------------------------------------
// Helper functions
// ---------------------------------------------------------------------------

static string DirName(const string& _sFilename)
{
	string sBuf(_sFilename);
	string::size_type idx;

	idx = sBuf.find_last_of('\\');

	if (idx != string::npos)
		sBuf.erase(idx);

	return sBuf;
}

static bool ReadKeyFromFile(HINSTANCE hInst, tstring sFilename, CComBSTR& bstrKey)
{
	tstring sPath = GetPathName(GetModuleDir(hInst), sFilename);

	HANDLE hFile = CreateFile(
		sPath.c_str(),
		GENERIC_READ,
		FILE_SHARE_READ,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL);
	if (INVALID_HANDLE_VALUE == hFile)
	{
		int iError = GetLastError();
		if (iError == ERROR_FILE_NOT_FOUND)
		{
			DB("License file not found.");
		}
		return false;
	}

	DB("Reading license file.");

	char buf[1024];
	DWORD nBytesRead;

	BOOL bRet = ReadFile(
		hFile, 
		&buf, 
		sizeof(buf), 
		&nBytesRead, 
		NULL);

	CLOSE_HANDLE(hFile);

	if (!bRet)
		return false;

	string sRead;
	sRead.assign(buf, nBytesRead);
	BstrFromString(&bstrKey, sRead);

	return true;
}


// ---------------------------------------------------------------------------
// ELicense Class
// ---------------------------------------------------------------------------

BOOL ELicense::IsLicenseValid(void)
{
#ifdef _DEBUG
	return TRUE;  // always licensed
#else
	CComBSTR bstrKey;
	if (!ReadKeyFromFile(
		_Module.GetModuleInstance(),
		LICENSE_FILE, 
		bstrKey))
	{
		return FALSE;
	}
	
	BOOL bLicensed = VerifyLicenseKey(bstrKey);
	DB("Licensed: " << (bLicensed ? "yes" : "no"));

	return bLicensed;
#endif
}

BOOL ELicense::GetLicenseKey(DWORD /*dwReserved*/, BSTR* pbstr)
{
	USES_CONVERSION;
	if (pbstr == NULL)
		return FALSE;

	*pbstr = ::SysAllocString(T2OLE(_T(LICENSE_KEY)));

	return TRUE;
}

BOOL ELicense::VerifyLicenseKey(BSTR bstrKey)
{
	USES_CONVERSION;

	return !lstrcmp(OLE2T(bstrKey), _T(LICENSE_KEY));
}

