// ---------------------------------------------------------------------------
// File:       EComError.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    error code description for all Eyeball ActiveX controls 
//
// Change Log:
// ---------------------------------------------------------------------------

#include "base.h"
#include "tstring.h"
#include "EComError.h"

tstring GetComErrorDesc(HRESULT hr)
{
	tstring sErrorDesc;

	if (GetCommonErrorDesc(hr, sErrorDesc))
		return sErrorDesc;

	if (GetContactListErrorDesc(hr, sErrorDesc))
		return sErrorDesc;

	if (GetEyeballVideoErrorDesc(hr, sErrorDesc))
		return sErrorDesc;

	if (GetVideoMessageErrorDesc(hr, sErrorDesc))
		return sErrorDesc;

	if (GetChatRoomErrorDesc(hr, sErrorDesc))
		return sErrorDesc;

	if (GetVideoMailErrorDesc(hr, sErrorDesc))
		return sErrorDesc;

	if (GetWhiteboardErrorDesc(hr, sErrorDesc))
		return sErrorDesc;

	if (GetVideoWindowErrorDesc(hr, sErrorDesc))
		return sErrorDesc;

	if (GetInternetRelayChatErrorDesc(hr, sErrorDesc))
		return sErrorDesc;

	sErrorDesc = _T("Unknown error.");
	return sErrorDesc;
}


bool GetCommonErrorDesc(HRESULT hr, tstring& sErrorDesc)
{
	switch (hr)
	{
	case E_UNEXPECTED_ERROR:
		sErrorDesc = _T("Unexpected.");
		return true;

	case E_NOT_LOGGED_IN:
		sErrorDesc = _T("Not logged in.");
		return true;

	case E_ALREADY_LOGGED_IN:
		sErrorDesc = _T("Already logged in.");
		return true;

	case E_SERVER_NETWORK:
		sErrorDesc = _T("Server is not properly set.");
		return true;

	case E_LOGIN:
		sErrorDesc = _T("Failed to log into the server. Please verify the User ID and password.");
		return true;

	case E_EMPTY_USER_NAME:
		sErrorDesc = _T("Empty User ID.");
		return true;

	case E_INVALID_USER_NAME:
		sErrorDesc = _T("User ID does not exist.");
		return true;

	case E_NO_RESPONSE:
		sErrorDesc = _T("No response.");
		return true;

	case E_EMPTY_FILE_NAME:
		sErrorDesc = _T("Empty file name.");
		return true;

	case E_EMPTY_TEXT_MESSAGE:
		sErrorDesc = _T("Empty text message.");
		return true;

	case E_SERVICE_NOT_SUBSCRIBED:
		sErrorDesc = _T("Service not subscribed.");
		return true;

	case E_SERVICE_NOT_LICENSED:
		sErrorDesc = _T("Service not licensed.");
		return true;

	case E_OPEN_FILE:
		sErrorDesc = _T("Failed to open the destination file. Make sure the file is not in use.");
		return true;

	case E_FILE_NOT_EXIST:
		sErrorDesc = _T("The specified file does not exist.");
		return true;

	case E_VIDEO_CAPTURE_DEVICE:
		sErrorDesc = _T("The video capture device is not working.");
		return true;

	case E_AUDIO_CAPTURE_DEVICE:
		sErrorDesc = _T("The audio capture device is not working.");
		return true;

	case E_USER_NOT_ONLINE:
		sErrorDesc = _T("The specified user is not online.");
		return true;

	case E_NULL_POINTER:
		sErrorDesc = _T("NULL pointer.");
		return true;
	}

	return false;
}

bool GetContactListErrorDesc(HRESULT hr, tstring& sErrorDesc)
{
	switch (hr)
	{
	case E_IM_SERVICE_NAME:
		sErrorDesc = _T("Invalid service name.");
		return true;

	case E_FILE_TRANSFER_ID:
		sErrorDesc = _T("Invalid file transfer ID.");
		return true;

	case E_PROFILE_INDEX:
		sErrorDesc = _T("Invalid profile index.");
		return true;

	case E_PROFILE_VALUE:
		sErrorDesc = _T("Invalid profile value.");
		return true;

	case E_PHOTOGRAPH_FORMAT:
		sErrorDesc = _T("The photograph format is not supported.");
		return true;

	case E_IM_NOT_LOGGED_IN:
		sErrorDesc = _T("Not logged into the service.");
		return true;

	case E_NOT_IN_CONTACT_LIST:
		sErrorDesc = _T("The specified user is not in the contact list.");
		return true;
	}

	return false;
}


bool GetEyeballVideoErrorDesc(HRESULT hr, tstring& sErrorDesc)
{
	switch (hr)
	{
	case E_NOT_IN_SESSION:
		sErrorDesc = _T("Not in a session.");
		return true;

	case E_ALREADY_IN_SESSION:
		sErrorDesc = _T("Already in the session.");
		return true;

	case E_EMPTY_SESSION_NAME:
		sErrorDesc = _T("Empty session name.");
		return true;

	case E_CREATE_SESSION:
		sErrorDesc = _T("Failed to create the session. Please specify a different session name.");
		return true;

	case E_JOIN_SESSION:
		sErrorDesc = _T("Failed to join the session. Please verify the session name and password.");
		return true;

	case E_USER_NOT_IN_SESSION:
		sErrorDesc = _T("The specified user is not in the session.");
		return true;

	// permission
	case E_NOT_HOST:
		sErrorDesc = _T("You are not the host of the session.");
		return true;

	//  volume
	case E_MICROPHONE_VOLUME:
		sErrorDesc = _T("Failed to set the microphone volume.");
		return true;

	case E_WAVE_VOLUME:
		sErrorDesc = _T("Failed to set the wave volume.");
		return true;

	case E_JPEG_CONTROL:
		sErrorDesc = _T("The JPEG control is not registered.");
		return true;

	case E_CONTROL_WINDOW:
		sErrorDesc = _T("Failed to create the control window.");
		return true;
	}

	return false;
}


bool GetVideoMessageErrorDesc(HRESULT hr, tstring& sErrorDesc)
{
	switch (hr)
	{
	case E_NO_CAPTURE_DEVICE:
		sErrorDesc = _T("No video or audio capture device is available for recording a video message.");
		return true;

	case E_AUDIO_CAPTURE_FORMAT:
		sErrorDesc = _T("The audio capture format is not supported.");
		return true;

	case E_VIDEO_CAPTURE_FORMAT:
		sErrorDesc = _T("The video capture format is not supported.");
		return true;

	case E_DIRECTX_VERSION:
		sErrorDesc = _T("DirectX 7.0+ is requried to support recording or playing of video messages.");
		return true;

	case E_VIDEO_COMPRESSION_FILTER:
		sErrorDesc = _T("The required video compression filter is not registered.");
		return true;

	case E_AUDIO_COMPRESSION_FILTER:
		sErrorDesc = _T("The required audio compression filter is not registered.");
		return true;

	case E_PLAY_VIDEO_MESSAGE:
		sErrorDesc = _T("Failed to play the message. The required video decompression filter is probably not registered.");
		return true;

	case E_START_GRAPH:
		sErrorDesc = _T("Failed to start the graph.");
		return true;

	case E_STOP_GRAPH:
		sErrorDesc = _T("Failed to stop the graph.");
		return true;

	case E_MESSAGE_NOT_READY:
		sErrorDesc = _T("No video message available.");
		return true;
	}
	return false;
}

bool GetChatRoomErrorDesc(HRESULT hr, tstring& sErrorDesc)
{
	switch(hr)
	{
	case E_ALREADY_IN_ROOM:
		sErrorDesc = _T("Already in a room.");
		return true;

	case E_ROOM_NAME:
		sErrorDesc = _T("Invalid room name.");
		return true;
	
	case E_ROOM_TYPE:
		sErrorDesc = _T("Invalid room type.");
		return true;
	
	case E_ROOM_FULL:
		sErrorDesc = _T("Room is full.");
		return true;
	
	case E_ROOM_PASSWORD:
		sErrorDesc = _T("Invalid room password.");
		return true;
	
	case E_NOT_IN_ROOM:
		sErrorDesc = _T("Not in a room.");
		return true;
	
	case E_LEAVE_TO_CREATE_NEW:
		sErrorDesc = _T("Leave the current room and then try again.");
		return true;
	
	case E_CATEGORY:
		sErrorDesc = _T("Invalid category.");
		return true;
	
	case E_ROOM_GREETINGS:
		sErrorDesc = _T("Invalid room greetings.");
		return true;
	
	case E_ROOM_DETAILS:
		sErrorDesc = _T("Invalid room details.");
		return true;

	case E_ROOM_LIMIT:
		sErrorDesc = _T("Invalid room limit.");
		return true;

	}
	return false;
}

bool GetVideoMailErrorDesc(HRESULT hr, tstring& sErrorDesc)
{
	switch(hr)
	{
	case E_MESSAGE_ID:
		sErrorDesc = _T("Invalid message ID.");
		return true;

	case E_EMPTY_RECIPIENTS:
		sErrorDesc = _T("No recipients.");
		return true;

	case E_HEADER_PARAM:
		sErrorDesc = _T("Invalid message parameter.");
		return true;

	case E_FOLDERNAME_EMPTY:
		sErrorDesc = _T("Foldername empty.");
		return true;

	case E_SYSTEM_FOLDER:
		sErrorDesc = _T("System folder.");
		return true;

	case E_FOLDER_EXISTS:
		sErrorDesc = _T("Folder already exists.");
		return true;

	case E_FOLDER_NOT_FOUND:
		sErrorDesc = _T("Folder not found.");
		return true;

	case E_OVERWRITE_NOT_ALLOWED:
		sErrorDesc = _T("File exists, overwrite is not allowed.");
		return true;

	case E_NO_VALID_RECEIVER:
		sErrorDesc = _T("File data is not sent: no valid receiver.");
		return true;

	case E_TRANSFER_IN_PROGRESS:
		sErrorDesc = _T("Transfer in progress.");
		return true;

	case E_NOT_ENOUGH_QUOTA:
		sErrorDesc = _T("Not enough quota left.");
		return true;
	}

	return false;
}

bool GetWhiteboardErrorDesc(HRESULT hr, tstring& sErrorDesc)
{
	// The whiteboard control doesn't not throw any exception
	return false;
}

bool GetVideoWindowErrorDesc(HRESULT hr, tstring& sErrorDesc)
{
	// The video window control doesn't throw any exception
	return false;
}

bool GetInternetRelayChatErrorDesc(HRESULT hr, tstring& sErrorDesc)
{
	switch (hr)
	{
	case E_NOT_IN_CHANNEL:
		sErrorDesc = _T("Not on a channel.");
		return true;

	case E_EMPTY_CHANNEL_NAME:
		sErrorDesc = _T("Empty channel name.");
		return true;

	case E_NO_COMMAND:
		sErrorDesc = _T("No command specified.");
		return true;

	case E_INVALID_COMMAND:
		sErrorDesc = _T("Invalid command.");
		return true;

	case E_NOT_ENOUGH_PARAMS:
		sErrorDesc = _T("Not enough parameters.");
		return true;

	case E_CHANNEL_NOT_EXIST:
		sErrorDesc = _T("Channel not exist.");
		return true;

	default:
		return false;
	}

	return false;
}
