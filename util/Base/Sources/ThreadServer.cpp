// ---------------------------------------------------------------------------
// File:       ThreadServer.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2002
//
// Purpose:    Base-class for "server" objects with an internal thread
//
// Change Log:
// ---------------------------------------------------------------------------

#include "Base.h"  // automatic use of precompiled headers 

#if 1 //for Linux yamada
#include "ELinuxUtil.h"
#else
#include "EWinUtil.h"
#endif //for Linux yamada
#include "Event.h"
#include "ThreadServer.h"
#include "Thread.h"

using namespace eyeball;


// ---------------------------------------------------------------------------
// ThreadServer class
// ---------------------------------------------------------------------------

ThreadServer::ThreadServer()
:
	m_pThread(NULL),
	m_pThreadEvent(new InternalThreadEvent),
	m_hResponseEvent(NULL),
	m_bResponseValue(false),
	m_dwLastError(0)
{
	m_hResponseEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	DB_ASSERT(NULL != m_hResponseEvent);
}

ThreadServer::~ThreadServer()
{
	// Internal thread object should be destroyed before reaching this point.
	DB_ASSERT(NULL == m_pThread.get());

#if 1 //for Linux yamada
	CLOSE_EVENT(m_hResponseEvent);
#else
	CLOSE_HANDLE(m_hResponseEvent);
#endif //for linux yamada
}

// Protected methods
bool ThreadServer::SetEventAndWait(u_int16_t uEvent)
{
	//DB_ENTER(ThreadServer::SetEventAndWait);

	m_dwLastError = 0;

	ResetEvent(m_hResponseEvent);
	m_pThreadEvent->SetEvent(uEvent);

	DWORD dwResult = CustomWaitForSingleObject(m_hResponseEvent, INFINITE);
	if (dwResult != WAIT_OBJECT_0)
	{
		DB_ASSERT(0);
		return false;
	}

	if (0 != m_dwLastError)
	{
		// Copy over the error-code from the internal thread
		SetLastError(m_dwLastError);
	}

	return m_bResponseValue;
}

void ThreadServer::StartInternalThread(void)
{
	ResetEvent(m_hResponseEvent);
	m_pThread.reset(new Thread(ThreadProc, this));
	CustomWaitForSingleObject(m_hResponseEvent, INFINITE);
}

void ThreadServer::StopInternalThread(void)
{
	if (m_pThread.get() == NULL)
		return;

	DWORD dwExitCode = 0;
#if 1 //for Linux yamada
#define STILL_ACTIVE 0
	BOOL bRet = TRUE;
#else
	BOOL bRet = GetExitCodeThread(m_pThread->GetHandle(), &dwExitCode);
#endif //for Linux yamadfa
	if (bRet && (STILL_ACTIVE == dwExitCode))
	{
		SetEventAndWait(TSTE_INTERNAL_QUIT);
	}
	else
	{
		DB_ASSERT(0);  // thread already exit
	}

	m_pThread->join();
	m_pThread.reset();
}

// Internal thread procedure
void* ThreadServer::ThreadProc(void *arg)
{
	ThreadServer* pThis = (ThreadServer*)(arg);
	// Indicate that the thread has been created.
	SetEvent(pThis->m_hResponseEvent);

	// Execute the subclass's internal thread implementation.
	pThis->InternalThreadImpl();

	// Indicate that the thread is about to quit, in response to a quit event.
	SetEvent(pThis->m_hResponseEvent);
	pThis = NULL;

	return NULL;
}
