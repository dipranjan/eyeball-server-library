// ---------------------------------------------------------------------------
// File:       EWinUtil.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2002
//
// Purpose:    Helper functions for Windows applications
//
// Change Log:
// ---------------------------------------------------------------------------

#include "Base.h"  // automatic use of precompiled headers 

#define _WINSOCKAPI_
#include <windows.h>
#include <commctrl.h>
#include <windowsx.h>

#include <tchar.h>		// _T
#include <shlobj.h>		// SHBrowseForFolder

#include <atlbase.h>

#include "EComUtil.h"
#include "EFileUtil.h"
#include "EWinUtil.h"
#include "debug.h"
#include "ReturnCodes.h"
//#include "SmartPtr.h"
#include "TextUtil.h"
#include "Timer.h"

using std::list;
using std::map;
using std::string;
using namespace eyeball;

typedef list<string> StringList;

#ifndef IDANI_CAPTION
// This constant works, but it is not defined in the latest platform SDK.
// However, there is no mention of this constant no longer being supported.
// The rectangle animation should be disabled if this causes problems.
#define IDANI_CAPTION 3
#endif


// ---------------------------------------------------------------------------
// Implementation
// ---------------------------------------------------------------------------

LONG eyeball::GetDialogData(HWND hDlg)
{
	return GetWindowLong(hDlg, DWL_USER);
}

LONG eyeball::SetDialogData(HWND hDlg, LONG lData)
{
	return SetWindowLong(hDlg, DWL_USER, lData);
}

LONG eyeball::GetWindowData(HWND hWnd)
{
	return GetWindowLong(hWnd, GWL_USERDATA);
}

LONG eyeball::SetWindowData(HWND hWnd, LONG lData)
{
	return SetWindowLong(hWnd, GWL_USERDATA, lData);
}

long eyeball::GetComboData(HWND hDlg, int nID)
{
	HWND hWndCombo = GetDlgItem(hDlg, nID);
	int index = SendMessage(hWndCombo, CB_GETCURSEL, 0, 0);
	if (index == CB_ERR)
	{
		return 0;
	}
	return (long)SendMessage(hWndCombo, CB_GETITEMDATA, index, 0);
}

bool eyeball::GetListBoxItemText(
	HWND hwndListBox, 
	int index, 
	tstring& sListBoxItemText)
{
	if (!::IsWindow(hwndListBox))
		return false;

	// find the length of the string
	int len = SendMessage(hwndListBox, LB_GETTEXTLEN, index, 0);
	RFI(len == LB_ERR);

	if (len <= 0)
	{
		sListBoxItemText = "";
		return (0 == len);
	}

	// allocate a buffer of the right size and get the string
	SharedArray<TCHAR> pBuf(new TCHAR[len + 1]);
	SendMessage(hwndListBox, LB_GETTEXT, index, (WPARAM)pBuf.get());
	sListBoxItemText = tstring(pBuf.get());
	
	return true;
}

LONG eyeball::GetListBoxItemData(HWND hwndListBox, int index)
{
	return SendMessage(hwndListBox, LB_GETITEMDATA, index, 0);
}

BOOL eyeball::GetWindowText(HWND hWnd, 
							tstring& sBuffer, 
							bool _bTrimWhitespace)
{
	// Dynamic memory allocation for buffer
	int iLen = GetWindowTextLength(hWnd);
	SharedArray<TCHAR> pszBuf(new TCHAR[iLen + 1]);

	BOOL bRet = ::GetWindowText(hWnd, pszBuf.get(), iLen + 1);
	sBuffer = _bTrimWhitespace ? trim_whitespace(pszBuf.get()) : pszBuf.get();
	
	return bRet;
}

BOOL eyeball::SetWindowText(HWND hwnd, const tstring& sText)
{
	BOOL bRet = ::SetWindowText(hwnd, sText.c_str());
	DB_ASSERT(bRet);

	return bRet;
}

BOOL eyeball::GetDlgItemText(
	HWND hDlg, 
	int nID, 
	tstring& sBuffer, 
	bool _bTrimWhitespace)
{
	return eyeball::GetWindowText(
		GetDlgItem(hDlg, nID), 
		sBuffer, 
		_bTrimWhitespace);
}

BOOL eyeball::SetDlgItemText(HWND hWnd, int nID, const tstring& sText)
{
	DB_ASSERT(IsWindow(GetDlgItem(hWnd, nID)));
	return ::SetDlgItemText(hWnd, nID, sText.c_str());
}

tstring eyeball::SGetDlgItemText(HWND hwnd, int nID)
{
	tstring sText;
	GetDlgItemText(hwnd, nID, sText);
	return sText;
}

tstring eyeball::SGetWindowText(HWND hwnd)
{
	tstring sText;
	GetWindowText(hwnd, sText);
	return sText;
}

bool eyeball::InitSlider(HWND hDlg, UINT nID, WORD wMin, WORD wMax, LONG step)
{
	HWND hwndSlider = GetDlgItem(hDlg, nID);

	SendMessage(hwndSlider,	TBM_SETRANGE, (WPARAM)TRUE, MAKELPARAM(wMin, wMax));
	SendMessage(hwndSlider, TBM_SETPAGESIZE, (WPARAM)0, step);	

	return true;
}

void eyeball::SetSliderValue(HWND hDlg, UINT nID, LONG val)
{
	SendMessage(GetDlgItem(hDlg, nID), TBM_SETPOS, (WPARAM)TRUE, val);
}

long eyeball::GetSliderValue(HWND hDlg, UINT nID)
{
	return SendMessage(GetDlgItem(hDlg, nID), TBM_GETPOS, 0, 0);
}

bool eyeball::InitProgress(HWND hDlg, UINT nID, WORD min, WORD max)
{
	int iRet = SendMessage(GetDlgItem(hDlg, nID), PBM_SETRANGE, 0, MAKELPARAM(min, max));

	return iRet != 0;
}

void eyeball::SetProgressValue(HWND hDlg, UINT nID, LONG val)
{
	SendMessage(GetDlgItem(hDlg, nID), PBM_SETPOS, val, 0);
}

long eyeball::GetProgressValue(HWND hDlg, UINT nID)
{
	return SendDlgItemMessage(hDlg, nID, PBM_GETPOS, 0, 0);
}

void eyeball::SetProgressColor(HWND hDlg, UINT nID, COLORREF col)
{
#ifndef _WIN32_WCE
	SendMessage(GetDlgItem(hDlg, nID), PBM_SETBARCOLOR,
		0, (LONG)col);
#else
	DB_ASSERT(0 && "Method not supported under Windows CE.");
#endif
}

bool eyeball::SetMenuItemText(HMENU hMenu, UINT nID, const tstring& sNewText)
{
	MENUITEMINFO mii;
	ZeroMemory(&mii, sizeof(mii));

	TCHAR buf[255];
	memset(buf, 0, sizeof(buf));

	mii.cbSize = sizeof(mii);
#ifndef _WIN32_WCE
	mii.fMask = MIIM_STRING;
#else
	mii.fMask = MIIM_TYPE;
	mii.fType = MFT_STRING; // ??
#endif

	mii.dwTypeData = buf;
	mii.cch = sizeof(buf) / sizeof(buf[0]);

	TCSCPY(buf, sNewText.c_str());

	if (!SetMenuItemInfo(hMenu, nID, FALSE, &mii))
		return false;

	return true;
}

bool eyeball::GetMenuItemText(HMENU hMenu, UINT nID, tstring& sText)
{
	MENUITEMINFO mii;
	ZeroMemory(&mii, sizeof(mii));

	TCHAR buf[255];
	memset(buf, 0, sizeof(buf));

	mii.cbSize = sizeof(mii);
#ifndef _WIN32_WCE
	mii.fMask = MIIM_STRING;
#else
	mii.fMask = MIIM_TYPE;
#endif;

	mii.dwTypeData = buf;
	mii.cch = sizeof(buf) / sizeof(buf[0]);

	if (!GetMenuItemInfo(hMenu, nID, FALSE, &mii))
		return false;

	if (0 == mii.dwTypeData)
	{
		sText.clear();
		return false;
	}

	sText = (LPCTSTR)mii.dwTypeData;

	return true;
}

bool eyeball::AddMenuItem(HMENU hMenu, UINT nID, const tstring& sText)
{
	MENUITEMINFO mii;
	ZeroMemory(&mii, sizeof(mii));

	TCHAR buf[255];
	TCSCPY(buf, sText.c_str());

	mii.cbSize = sizeof(mii);
	mii.fMask = MIIM_ID | MIIM_TYPE;
	mii.wID = nID;
	mii.dwTypeData = buf;
	mii.cch = sizeof(buf);

#ifndef _WIN32_WCE
	InsertMenuItem(hMenu, -1, TRUE, &mii);
#else
	// appended to the end of the menu
	InsertMenu(hMenu, -1, MF_BYPOSITION, nID, buf);
#endif

	return true;
}

BOOL eyeball::HandlePopupMenu(
	HINSTANCE hInstance,
	HWND hwnd, 
	WORD menu, 
	int xPos, 
	int yPos, 
	int sysWidth /*= 0*/, 
	int sysHeight /*= 0*/, 
	bool bInWindow /*= true*/,
	UINT uFlags /*= TPM_LEFTALIGN | TPM_TOPALIGN | TPM_RIGHTBUTTON*/)
{
	HMENU hPopupMenu = LoadMenu(hInstance, MAKEINTRESOURCE(menu));
	HMENU hSubMenu   = GetSubMenu(hPopupMenu, 0);

	RECT windowRect;
	memset(&windowRect, 0, sizeof(windowRect));

	if (bInWindow)
		GetWindowRect(hwnd, &windowRect);

	ETrackPopupMenu(
		hSubMenu,
		uFlags, //TPM_LEFTALIGN | TPM_TOPALIGN | TPM_RIGHTBUTTON,
		windowRect.left + sysWidth  + xPos,
		windowRect.top  + sysHeight + yPos,
		hwnd);

	DestroyMenu(hPopupMenu);
	hPopupMenu = NULL;

	return TRUE;
}

bool eyeball::HandleStringPopupMenu(
	HWND hWnd,
	const list<string> lsStrings,
	string& sSelectedString,
	int xPos, 
	int yPos, 
	UINT uFlags /*= TPM_LEFTALIGN | TPM_TOPALIGN | TPM_RIGHTBUTTON*/)
{
#ifndef _WIN32_WCE
	HMENU hMenu = CreatePopupMenu();
	if (NULL == hMenu)
		return false;

	if (lsStrings.empty())
		return false;

	map<UINT, string> mpIdString;

	UINT nCtlId = 1;
	StringList::const_iterator iter = lsStrings.begin();
	for (; iter != lsStrings.end(); iter++)
	{
		string sMenuItemText = *iter;
		if (sMenuItemText.empty())
		{
			DB_ASSERT(0);
			continue;
		}

		AddMenuItem(hMenu, nCtlId, sMenuItemText);

		mpIdString[nCtlId] = sMenuItemText;
		nCtlId++;
	}

	uFlags |= TPM_RETURNCMD;

	UINT nSelectedId = ETrackPopupMenu(
		hMenu,
		uFlags,
		xPos,
		yPos,
		hWnd);
	
	DestroyMenu(hMenu);
	hMenu = NULL;

	if (0 == nSelectedId)
		return false;

	if (nSelectedId >= nCtlId)
	{
		DB_ASSERT(0);
		return false;
	}

	sSelectedString = mpIdString[nSelectedId];

	return true;
#else
	return false;
#endif
}

UINT eyeball::ETrackPopupMenu(
	HMENU hMenu,
	UINT uFlags,
	int x,
	int y,
	HWND hWnd)
{
	// Workaround possible bug in OS, mentioned in Usenet
	// The bug involves the menu for a notification icon not going away
	// when the mouse is clicked away from the menu.
	SetForegroundWindow(hWnd);

	BOOL bRet = TrackPopupMenu(
		hMenu,
		uFlags,
		x,
		y,
		0,
		hWnd,
		NULL);

	// Workaround possible bug in OS, mentioned in Usenet
	PostMessage(hWnd, WM_NULL, 0, 0);

	// Process WM_PAINT messages to redraw the window covered by the menu.
	DispatchPaintMessages();

	return (UINT)bRet;
}

HMENU eyeball::InsertCustomSystemMenu(
	HINSTANCE hInst, 
	const tstring& sTitle, 
	HWND hwnd, 
	WORD menu)
{
	DB_ASSERT(hwnd && menu);

	HMENU hSysMenu = GetSystemMenu(hwnd, FALSE);
	DB_ASSERT(hSysMenu);

	InsertMenu(hSysMenu, 0, MF_BYPOSITION | MF_SEPARATOR, NULL, NULL);
	
	HMENU hPopupMenu = LoadMenu(hInst, MAKEINTRESOURCE(menu));
	DB_ASSERT(hPopupMenu);

	HMENU hSubMenu   = GetSubMenu(hPopupMenu, 0);
	DB_ASSERT(hSubMenu);

	InsertMenu(hSysMenu, 0, MF_BYPOSITION | MF_STRING | MF_POPUP, 
		(UINT_PTR)hSubMenu, sTitle.c_str());

	DB_ASSERT(hPopupMenu);

	return hPopupMenu;
}

BOOL eyeball::GetSelectedTreeItemText(HWND hwndTV, tstring& sText)
{
	HTREEITEM hItem = TreeView_GetSelection(hwndTV);
	if (hItem == NULL)
		return FALSE;

	return GetTreeItemText(hwndTV, hItem, sText);
}

BOOL eyeball::GetTreeItemText(HWND hwndTV, HANDLE hItem, tstring& sText)
{
	TCHAR buf[512]; // should be dynamic - but I'm not sure how

	TVITEM tvi;
	tvi.mask = TVIF_HANDLE | TVIF_TEXT;
	tvi.hItem = (HTREEITEM)hItem;
	tvi.cchTextMax = sizeof(buf) / sizeof(buf[0]);
	tvi.pszText = buf;

	TreeView_GetItem(hwndTV, &tvi);

	sText = tvi.pszText;

	return TRUE;
}

LONG eyeball::GetTreeItemData(HWND hwndTV, HANDLE hItem)
{
	TVITEM tvi;
	tvi.mask = TVIF_HANDLE | TVIF_PARAM;
	tvi.hItem = (HTREEITEM)hItem;
	tvi.lParam = 0;

	TreeView_GetItem(hwndTV, &tvi);

	return tvi.lParam;
}

BOOL eyeball::SetTreeItemData(HWND hwndTV, HANDLE hItem, LONG lData)
{
	TVITEM tvi;
	tvi.mask = TVIF_HANDLE | TVIF_PARAM;
	tvi.hItem = (HTREEITEM)hItem;
	tvi.lParam = lData;

	TreeView_SetItem(hwndTV, &tvi);

	return tvi.lParam;
}

DWORD eyeball::GetTreeItemState(HWND hwndTV, HANDLE hItem)
{
	TVITEM tvi;
	tvi.mask = TVIF_HANDLE | TVIF_STATE;
	tvi.hItem = (HTREEITEM)hItem;
	tvi.state = 0;
	tvi.stateMask = 0xffff;

	TreeView_GetItem(hwndTV, &tvi);

	return tvi.state;
}

void eyeball::DispatchPaintMessages(void)
{
	// Process all WM_PAINT messages
	MSG msg;
	while (PeekMessage(&msg, NULL, WM_PAINT, WM_PAINT, PM_REMOVE)) 
	{ 
		DispatchMessage(&msg); 
	}
}

DWORD eyeball::CustomWaitForSingleObject(
	HANDLE hHandle, 
	DWORD dwMilliseconds)
{
	// Custom version of WaitForSingleObject()
#ifndef _WIN32_WCE
	DWORD dwStartTime = timeGetTime();
	DWORD dwResult(0);

	for(;;)
	{
		// Handle Windows paint messages only
		DispatchPaintMessages();
		
		DWORD dwTimeout(0);
		DWORD dwElapsed = timeGetTime() - dwStartTime;
		if (dwElapsed < dwMilliseconds)
		{
			dwTimeout = dwMilliseconds - dwElapsed;
		}

		// Wait for the event
		dwResult = MsgWaitForMultipleObjects(
			1, 
			&hHandle, 
			FALSE,
			dwTimeout,
			QS_PAINT);
		
		if (dwResult != WAIT_OBJECT_0 + 1)
			break;
	}
#else
	DWORD dwResult = WaitForSingleObject(hHandle, dwMilliseconds);
#endif

	return dwResult;
}

// DoLockDlgRes - loads and locks a dialog template resource. 
// Returns the address of the locked resource. 
// lpszResName - name of the resource 
DLGTEMPLATE* WINAPI eyeball::DoLockDlgRes(
	HINSTANCE hInst, 
	LPCTSTR lpszResName, 
	LPCTSTR lpType)
{ 
    HRSRC hrsrc = FindResource(hInst, lpszResName, lpType); 

	if (!hrsrc)
	{
		DB("DoLockDlgRes - FindResource failed");
		return NULL;
	}

    HGLOBAL hglb = LoadResource(hInst, hrsrc); 

	if (!hglb)
	{
		DB("DoLockDlgRes - LoadResource failed");
		return NULL;
	}
    
	return (DLGTEMPLATE *) LockResource(hglb); 
}

tstring eyeball::BrowseForFolder(HWND hwndParent, const tstring& _sTitle)
{
#ifndef _WIN32_WCE
	CoInit comInitializer;

	// Get IMalloc handle first:
	LPMALLOC pMalloc;
	SHGetMalloc(&pMalloc);

	// First use this as temporary display name
	TCHAR pszPath[MAX_PATH];

	BROWSEINFO browseInfo;
	browseInfo.hwndOwner      = hwndParent;
	browseInfo.pidlRoot       = NULL;
	TCHAR pszDiaplayName[MAX_PATH];
	browseInfo.pszDisplayName = pszDiaplayName;
	browseInfo.lpszTitle      = _sTitle.c_str();

	browseInfo.ulFlags        = BIF_RETURNONLYFSDIRS;
	browseInfo.lpfn           = NULL;
	browseInfo.lParam         = NULL;
	browseInfo.iImage         = 0;

	ITEMIDLIST *pIdList = SHBrowseForFolder(&browseInfo);

	if (pIdList)
	{
		// Retrieve absolute path from ID list
		WARN(SHGetPathFromIDList(pIdList, pszPath));
	}
	else
	{
		TCSCPY(pszPath, _T(""));
	}

	// After doing something with lpBrowseInfo, free memory:
	pMalloc->Free(pIdList);
	pMalloc->Release();

	return pszPath;
#else
	return _T("");
#endif
}

// Helper Function for GotoURL()
static LONG GetRegKey(HKEY key, LPCTSTR subkey, LPTSTR retdata)
{
    HKEY hkey;
    LONG retval = RegOpenKeyEx(key, subkey, 0, KEY_QUERY_VALUE, &hkey);

    if (retval == ERROR_SUCCESS) {

		TCHAR data[MAX_PATH];
#ifndef _WIN32_WCE
        long datasize = MAX_PATH;
		RegQueryValue(hkey, NULL, data, &datasize);
#else
		unsigned long datasize = sizeof(TCHAR) * MAX_PATH;
		RegQueryValueEx(hkey, NULL, NULL, REG_NONE, (LPBYTE)data, &datasize);
#endif
		lstrcpy(retdata,data);
		RegCloseKey(hkey);
    }

    return retval;
}

HINSTANCE eyeball::GotoURL(const tstring& sUrl, int showcmd)
{
    HINSTANCE result = ShellExecute(
		NULL, 
		_T("open"), 
		sUrl.c_str(), 
		NULL,
		NULL, 
		showcmd);
    return result;
}

// Similar to GotoURL, but for mailto: et al.
HINSTANCE eyeball::GotoAlternateURL(LPCTSTR url, int showcmd)
{
    return ShellExecute(NULL, _T("open"), url, NULL, NULL, showcmd);
}

bool eyeball::IsVisible(HWND hwnd)
{ 
	// Suppresses bounds checker error reports
	// when it is known that the handle may be invalid.

	return IsWindow(hwnd) && IsWindowVisible(hwnd); 
}

bool eyeball::CustomizeClassWndProc(
	LPCTSTR lpClassName,
	WNDPROC& OldWndProc,
	WNDPROC NewWndProc,
	HINSTANCE hInstance)
{
	if (NULL != OldWndProc)
		return false;  // already subclassed

	HWND hWndTemp = CreateWindow(lpClassName, 
		_T(""), 0, 0, 0, 0, 0, NULL, NULL, hInstance, NULL);
	if (hWndTemp == NULL)
		return false;

#ifndef _WIN32_WCE
	OldWndProc = (WNDPROC)SetClassLong(hWndTemp, 
		GCL_WNDPROC, (LONG)NewWndProc);

	// Set the background brush to NULL.
	DWORD dw = SetClassLong(hWndTemp, GCL_HBRBACKGROUND, (LONG)NULL);
#else
	DB_ASSERT(0 && "Method not supported under Windows CE.");
#endif



	DestroyWindow(hWndTemp);

	return true;
}

bool eyeball::CustomizeWndProc(
	HWND hWnd,
	WNDPROC& OldWndProc,
	WNDPROC NewWndProc)
{
	WNDPROC WndProc = (WNDPROC)SetWindowLong(hWnd, GWL_WNDPROC, (LONG)NewWndProc);
	if (NULL == OldWndProc)
	{
		OldWndProc = WndProc;
	}

	DB_ASSERT(OldWndProc == WndProc);

	return (0 != WndProc);
}

void eyeball::RemoveTaskbarIcon(HWND hWnd)
{
	LONG lStyle = GetWindowLong(hWnd, GWL_EXSTYLE) | WS_EX_TOOLWINDOW;
	SetWindowLong(hWnd, GWL_EXSTYLE, lStyle);
}

// HIWORD is the major version
// LOWORD is the minor version
DWORD eyeball::GetRichEditVersion()
{
#ifndef _WIN32_WCE
    static DWORD dwVersion = -1;

    if (dwVersion == -1)
    {
        dwVersion = MAKELONG(0, 1);
        HINSTANCE hModule = ::LoadLibrary(_T("riched20.dll"));
        if (hModule != NULL)
        {
            dwVersion = MAKELONG(0, 2);
            tstring sVersion;
            if (GetVersionOfFile("riched20.dll", sVersion))
            {
                if (sVersion.mbs() >= "5.30")
                {
                    dwVersion = MAKELONG(0, 3);
                }
            }
            ::FreeLibrary(hModule);
        }
    }
    return dwVersion;
#else
	return 0;
#endif
}

// workaround for MSLU bug
BOOL eyeball::GetTextExtentPoint32U(
	HDC hdc, 
	LPCTSTR lpString,
	int cbString,
	LPSIZE lpSize)
{
#ifndef _WIN32_WCE
	if (IsWindowUnicode(GetDesktopWindow()))
	{
		// use default function
		return GetTextExtentPoint32(hdc, lpString, cbString, lpSize);
	}

	// convert to MBCS and then use ANSI function
	USES_CONVERSION;
	std::string sText = T2A((TCHAR*)lpString);

	return GetTextExtentPoint32A(hdc, sText.c_str(), sText.length(), lpSize);
#else
	return FALSE;
#endif
}

BOOL eyeball::DrawTextU(
	HDC hdc,
	LPCTSTR lpString,
	int nString,
	LPRECT lprc,
	UINT uFlags)
{
#ifndef _WIN32_WCE
	if (IsWindowUnicode(GetDesktopWindow()))
	{
		// use default function
		return DrawText(hdc, lpString, nString, lprc, uFlags);
	}

	// convert to MBCS and then use ANSI function
	USES_CONVERSION;
	std::string sText = T2A((TCHAR*)lpString);

	return DrawTextA(hdc, sText.c_str(), sText.length(), lprc, uFlags);
#else
	return FALSE;
#endif
}

HWND eyeball::FindChildByClass(HWND hWndParent, const tstring& sClass)
{
#ifndef _WIN32_WCE
	return FindWindowEx(hWndParent, NULL, sClass.c_str(), NULL);
#else
	DB_ASSERT(0 && "Method not supported under Windows CE.");
	return NULL;
#endif
}

tstring eyeball::GetGuidString(void)
{
#ifndef _WIN32_WCE
	GUID guid;
	HRESULT hr = CoCreateGuid(&guid);
	if (FAILED(hr))
		return "";

	WCHAR buf[256];
	memset(buf, 0, sizeof(buf));
	if (0 == StringFromGUID2(guid, buf, sizeof(buf) / sizeof(buf[0])))
		return "";

	return tstring(buf);
#else
	DWORD dwTime = timeGetTime();
	return itoa((int)dwTime);
#endif
}


// ---------------------------------------------------------------------------
// Dialog Data Class
// ---------------------------------------------------------------------------

DlgData::DlgData()
:
	m_hWnd(NULL)
{
}

bool DlgData::Init(HWND hDlg, DlgData* pThis)
{
	pThis->m_hWnd = hDlg;
	bool bRet = pThis->OnInit();
	SetDialogData(hDlg, (LONG)pThis);

	return bRet;
}

// Called in OnDestroy()
bool DlgData::Destroy(HWND hDlg)
{
	DlgData* pThis = (DlgData*)SetDialogData(hDlg, 0);
	RFIN(pThis);
	bool bRet = pThis->OnDestroy();
	
	SAFE_DELETE(pThis);

	return bRet;
}

DlgData* DlgData::Ptr(HWND hDlg)
{
	return (DlgData*)GetDialogData(hDlg);
}

#ifdef _UNICODE

// Fix for broken MSLU (Unicows) ModifyMenu function
BOOL _stdcall Fixed_ModifyMenuW(HMENU hMenu, UINT uPosition, UINT uFlags,
                               UINT uIDNewItem, LPCTSTR lpNewItem)
{
#ifndef _WIN32_WCE
	if (uFlags & (MF_BITMAP | MF_OWNERDRAW))
	{
		// This is not actually a string, so it is okay to call
		// straight through without converting the parameter
		return ModifyMenuA(hMenu, uPosition, uFlags, uIDNewItem, 
			(LPCSTR)lpNewItem);
	}
	else
	{
		// Only convert real strings, not bitmap handles or user data
		// Get the size of the Unicode string
		size_t len = wcslen(lpNewItem);
		
		// Allocate enough space for the Ansi string
		LPSTR newItemA = (LPSTR)_alloca((len * 2) + 1);
		
		// Convert the string from Unicode
		WideCharToMultiByte(CP_ACP,
			0,
			lpNewItem,
			-1,
			newItemA,
			(len * 2) + 1,
			NULL,
			NULL);
		
		return ModifyMenuA(hMenu, uPosition, uFlags, uIDNewItem, newItemA);
	}
#else
	DB_ASSERT(0 && "Method not supported under Windows CE.");
	return FALSE;
#endif

}

extern "C"
{
	extern FARPROC Unicows_ModifyMenuW = (FARPROC)&Fixed_ModifyMenuW;
}

#endif //_UNICODE


#ifdef _WIN32_WCE


HINSTANCE ShellExecute(
    HWND hwnd, 
    LPCTSTR lpOperation,
    LPCTSTR lpFile, 
    LPCTSTR lpParameters, 
    LPCTSTR lpDirectory,
    INT nShowCmd
)
{
	SHELLEXECUTEINFO sei;
	memset(&sei, 0, sizeof(SHELLEXECUTEINFO));

	sei.cbSize = sizeof(SHELLEXECUTEINFO);
	sei.fMask = SEE_MASK_FLAG_NO_UI | SEE_MASK_NOCLOSEPROCESS;
	sei.hwnd = hwnd; // handle to any message window
	sei.lpVerb = lpOperation;
	sei.lpFile = lpFile;
	sei.lpParameters = lpParameters;
	sei.lpDirectory = lpDirectory;
	sei.nShow = nShowCmd;
	sei.hInstApp = NULL;

	if (ShellExecuteEx(&sei))
	{
		return sei.hInstApp;
	}

	return 0;
}

#endif //_WIN32_WCE

