/** All files of the DNDS Prototype are Copyright (C) 1999 by Andre Schoorl
 *  unless otherwise noted.
 *
 *  The DNDS Prototype is distributed in the public domain in the hope that it
 *  will be useful, but is provided `as is' and is subject to change without
 *  notice.  No warranty of any kind is made with regard to the software or
 *  documentation.  The author shall not be liable for for any error for
 *  incidental or consequential damages in connection with the software and
 *  the documentation.
 *
 *  Permission to copy the whole, unmodified DNDS package is granted provided
 *  that the copies are not made or distributed for resale (excepting nominal
 *  copying fees).  All redistributions in either source or binary form must
 *  include this disclaimer.
 */

#include <fstream>//.h>
#include <time.h>

#ifndef WIN32
	#include <unistd.h>		// sleep()
	#include <values.h>		// MAXINT
#else
	#include <windows.h>	// Sleep()
	#define sleep(X) \
		Sleep((X)*1000)

	#include <limits.h>		// INT_MAX
	#define MAXINT \
		INT_MAX
#endif

// Make sure we have debug set to compile this
#ifndef DEBUG
#define DEBUG
#endif

#include "debug.h"

#ifdef DEBUG_TIME_ACCURACY
#include <sys/time.h>	// gettimeofday()
#include <stdio.h>		// sprintf()
#endif

// Static variables
int Debug::exit_depth = MAXINT;
int Debug::print_depth = MAXINT;
int Debug::indent_depth = 0;

bool Debug::cerr_enable = true;
ostream *Debug::os_ptr = NULL;

#ifdef DEBUG_THREADS
Mutex Debug::mutex;
#endif

using std::string;

Debug::Debug(char *_func_name)
:
	func_name(_func_name),
	prev_exit_depth(exit_depth),
	prev_print_depth(print_depth)
{
	if (print_depth > 1)
	{
		DB(func_name << "()");
		DB("{");

		indent_depth++;
	}

	if (!(exit_depth > 1))
	{
		preline();
		DB_PRINT("Debug: Maximum stack depth reached, suspected infinite loop?"
			<< endl);

		preline();
		DB_PRINT("Debug: Call DB_SET_EXIT_DEPTH if this is not an error"
			<< endl);

		exit(1);
	}

	exit_depth--;
	print_depth--;
}

Debug::~Debug()
{
	// Did we just come back from a print depth override?
	if (prev_print_depth < print_depth)
	{
		DB("...");
	}

	exit_depth = prev_exit_depth;
	print_depth = prev_print_depth;

	if (print_depth > 1)
	{
		indent_depth--;

		DB("}");
	}
}

void Debug::Assert(char *expr, char *filename, int line)
{
	preline();
	DB_PRINT(filename << ":" << line << ": Assertion `"<< expr << "' failed." << endl);
	abort();
}

void Debug::delay(int num_seconds)
{
	int i;

	DB_INDENT();

	for (i=num_seconds; i > 0; i--)
	{
		DB_PRINT(i << " " << flush);
		sleep(1);
	}
	DB_PRINT(endl);
}

void Debug::indent(void)
{
	preline();
	for (int i=0; i < indent_depth; i++)
	{
		DB_PRINT(" ");
	}
	
}

void Debug::preline(void)
{
// Removed for now since server is single-threaded, time is more useful
/*
	DB_PRINT(getpid());
#ifdef DEBUG_THREADS
	DB_PRINT("," << pthread_self());
#endif
*/
#ifdef _INDENT_DB_PRINT
	DB_PRINT("         ");
#endif	
	DB_PRINT(localtime());
	DB_PRINT(" ");
}

void Debug::print_time(void)
{
#ifdef DEBUG_TIME_ACCURACY
	struct timeval tv;
	gettimeofday(&tv, NULL);
	time_t when(tv.tv_sec);
	struct tm *tm;
	tm = ::localtime(&when);

	// ctime style but with greater accuracy and a timezone inserted
	char *format1 = "%a %b %e %H:%M:%S";
	char *format2 = " %Z %Y";

	char buf1[256], buf2[256], buf3[256];

	ASSERT(strftime(buf1, sizeof(buf1), format1, tm) != 0);
	ASSERT(strftime(buf2, sizeof(buf2), format2, tm) != 0);
	sprintf(buf3, ".%02d", (int)(tv.tv_usec / 1e4));

	DB(buf1 << buf3 << buf2 << " (" << when << buf3 << ")");
#else
	time_t when(time(NULL));
	const struct tm *tm;
	tm = ::localtime(&when);

	// ctime style but with a timezone inserted
	char *format = "%a %b %e %H:%M:%S %Z %Y";

	char out[256];
	ASSERT(strftime(out, sizeof(out), format, tm) != 0);
	DB(out << " (" << when << ")");
#endif
}

void Debug::redirect(const char *_filename, bool _cerr_enable)
{
	if (os_ptr)
	{
		delete os_ptr;
		os_ptr = NULL;
	}

	if (_filename)
		os_ptr = new ofstream(_filename);

	cerr_enable = _cerr_enable;
}

void Debug::set_exit_depth(int _exit_depth)
{
	exit_depth = _exit_depth;
}

void Debug::set_print_depth(int _print_depth)
{
	print_depth = _print_depth;

	DB("...");
}

void Debug::warn(char *expr, char *filename, int line)
{
	preline();
	DB_PRINT(filename << ":" << line << ": Warning `"
		<< expr << "' failed." << endl);
}

string Debug::localtime(void)
{
	// mm/dd/yyyy HH:MM:SS
	// 1234567891123456789
	char buf[20];

	time_t tNow = time(NULL);
	strftime(buf, sizeof(buf), "%Y-%m-%d %T", ::localtime(&tNow));

	return buf;
}

