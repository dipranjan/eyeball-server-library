// ---------------------------------------------------------------------------
// File:       CodecPlugin.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2002
//
// Purpose:    Class definitions for CodecPlugin.
//			   This is used to load/unload module and to access interface.
// 
// Change Log:
// ---------------------------------------------------------------------------

#include "Base.h"  // automatic use of precompiled headers 

#include "CodecPlugin.h"
#include "debug.h"
#include "EFileUtil.h"
#include "EUtil.h"
#include "TextUtil.h"

///////////////////////////////for directory access /////////////////
#include <sys/types.h>
#include <dirent.h>
////////////////////////////////////////////////////////////////////

#include <dlfcn.h>


using std::string;


///////////////test /////////

/////////////////////////////

// ---------------------------------------------------------------------------
// Static member variables
// ---------------------------------------------------------------------------

void* CodecPlugin::ms_hModuleInstance = NULL;


// ---------------------------------------------------------------------------
// Defines
// ---------------------------------------------------------------------------

#define TEXT_EYEBALL_EXPORTED_FUNCTION		TEXT("EyeballGetModule")
#define EYEBALL_EXPORTED_FUNCTION           "EyeballGetModule"
#define CODEC_LIB_DIR 						"/usr/local/lib/"
#define CODEC_LIB_DIR_CURRENT 						"./"

// ---------------------------------------------------------------------------
// Local helper functions
// ---------------------------------------------------------------------------

static string GetModuleDir(void)
{
	return string(CODEC_LIB_DIR);
}


// ---------------------------------------------------------------------------
// class CodecPlugin
// ---------------------------------------------------------------------------

CodecPlugin::CodecPlugin(CodecType _codec_type, 
						  int _m_iInterfaceVersion, 
						  int _m_iMajorVersion,						  						  
						  int _m_iMinorVersion)
:	
	m_CodecType(_codec_type),
	m_iInterfaceVersion(_m_iInterfaceVersion),
	m_iMajorVersion(_m_iMajorVersion),
	m_iMinorVersion(_m_iMinorVersion),
	m_sFilename(""),
	m_hLibrary(NULL),
	m_pCodecInterface(NULL)
{
}

CodecPlugin::~CodecPlugin()
{
	Unload();
}

BOOL CodecPlugin::Load(char chPayloadType)
{
	
	m_payloadType = chPayloadType; ////////////new add///////
	if (Preload(chPayloadType))
		return TRUE;

	return FALSE;
}

BOOL CodecPlugin::Preload(char chPayloadType)
{
	if (m_hLibrary)
		return chPayloadType == m_pCodecInterface->chPayloadType;

	string filename;

	
	if (chPayloadType == 100)
	{
		filename = string("libCodec1.so");
	}
	else if (chPayloadType == 101)
	{
		filename = string("libCodec2.so");
	}
	
	else filename = string ("unknown");
		
	
	if (Load(filename))
	{
		if (m_pCodecInterface->chPayloadType == chPayloadType)
		{			
			return TRUE;
		}
		else
		{
			Unload();
			return FALSE;
		}
	}
	return FALSE;
}


BOOL CodecPlugin::Load(const string& _filename)
{
	
	CodecModule ptr;
	CodecInterface *codec_interface;
	bool search = true;
	bool libraryLoaded = false;
	void* hLibrary;
	
	
	string sFilename = GetPathName(GetModuleDir(), _filename);
	DB("CodecPlugin::Load - " << sFilename);


if ( _filename.compare("unknown"))
{
	
	// Load the codec DLL

	hLibrary = dlopen(sFilename.c_str(),RTLD_LAZY);
	printf ("After open of dll in current directory.\n");
	
	if (!hLibrary)
	{
		DB("Dlopne failed :"<<dlerror());
		// Try without specifying the path.
		sFilename = GetPathName(CODEC_LIB_DIR_CURRENT, _filename);;
		
	//	hLibrary = dlopen(sFilename.c_str(),RTLD_NOW|RTLD_GLOBAL);
		hLibrary = dlopen(sFilename.c_str(),RTLD_LAZY);
		if (!hLibrary)
			{
			DB("Dlopne failed :"<<dlerror());	
			//search = true;	
			//return false;
			}
		else search = false;	
	}
	else  search = false;	

}	
///////////////////////////////////////HERE CURRENT DIRECTORY SEARCH SHOULD BE DONE///////////
// codec already loaded successfully 	
if (!search)
{
	ptr = (CodecModule)dlsym(hLibrary, "EyeballGetModule");

	if (!ptr)
	{
		printf("Dlsym failed %s\n",dlerror());
		//DB(sFilename.c_str() << " does not export " << EYEBALL_EXPORTED_FUNCTION);
		dlclose(hLibrary);
		return false;
	}

	// Use it to get a pointer to the codec structure

	codec_interface = (ptr)();
	if (!codec_interface)
	{
		DB("Could not query codec interface from " << sFilename.c_str());
		dlclose(hLibrary);
		return false;
	}

	if (codec_interface->iInterfaceVersion != m_iInterfaceVersion)
	{
		DB("Error: " << sFilename.c_str() << " has incorrect interface version");
		dlclose(hLibrary);
		return false;
	}

	if (codec_interface->codecType != m_CodecType)
	{
		DB("Error: " << sFilename.c_str() << " is of incorrect codec type");
		dlclose(hLibrary);
		return false;
	}

	if (codec_interface->iMajorVersion != m_iMajorVersion)
	{
		DB("Error: " << sFilename.c_str() << " has incorrect major version");
		dlclose(hLibrary);
		return false;
	}

	// Successfully loaded, commit changes to member variables
	m_sFilename = sFilename;
	m_hLibrary = hLibrary;
	m_pCodecInterface = codec_interface;

	return true;
}
// directory search must be done
else
{	
  DIR *dp;
  struct dirent *ep;

       dp = opendir ("./");
  if (dp != NULL)
  {
    while (ep = readdir (dp))
	 {	
		 if (ep ==NULL) break;
			 
       // puts (ep->d_name);
	if (strstr (ep->d_name,".so")==NULL) 
		continue;
	
	
	sFilename = GetPathName(CODEC_LIB_DIR_CURRENT, ep->d_name);
	
	printf ("File name = %s\n",sFilename.c_str());
	
		
	//hLibrary = dlopen(sFilename.c_str(),RTLD_NOW|RTLD_GLOBAL);
	hLibrary = dlopen(sFilename.c_str(),RTLD_LAZY);
//	printf ("After open of dll in current directory.\n");
	if (!hLibrary)
			{
			DB("Dlopne failed :"<<dlerror());	
			//return false;
				continue;
			}
	
	ptr = (CodecModule)dlsym(hLibrary, "EyeballGetModule");


	if (!ptr)
	{
		printf("Dlsym failed %s\n",dlerror());
		//DB(sFilename.c_str() << " does not export " << EYEBALL_EXPORTED_FUNCTION);
		
		dlclose(hLibrary);
		hLibrary = NULL;
		continue;
	}

	// Use it to get a pointer to the codec structure


	codec_interface = (ptr)();
//	printf ("After codec_interface\n");
	if (!codec_interface)
	{
		DB("Could not query codec interface from " << sFilename.c_str());
		printf("codec_interface is null \n");
		dlclose(hLibrary);
		hLibrary = NULL;
		continue;
		
	}

	if (codec_interface->iInterfaceVersion != m_iInterfaceVersion)
	{
		DB("Error: " << sFilename.c_str() << " has incorrect interface version");
		printf("iInterfaceVersion is null \n");
		dlclose(hLibrary);
		hLibrary = NULL;
		continue;
		
	}

	if (codec_interface->codecType != m_CodecType)
	{
		DB("Error: " << sFilename.c_str() << " is of incorrect codec type");
		printf("codecType is null \n");
		dlclose(hLibrary);
		hLibrary = NULL;
		continue;
	}
	if (codec_interface->chPayloadType != m_payloadType)
	{
		DB("Error: " << sFilename.c_str() << " is of incorrect codec type");
		printf("codecType is null \n");
		dlclose(hLibrary);
		hLibrary = NULL;
		continue;
	}

//	printf ("Loop breaking %p\n",hLibrary);
	libraryLoaded = true;
	break;
	}	
	
    (void) closedir (dp);
	
	if (libraryLoaded)
	{
	printf ("every thing is ok.\n");
	m_sFilename = sFilename;
	m_hLibrary = hLibrary;
	m_pCodecInterface = codec_interface;
	return true;
	}
	else
	{
	printf("Every thing is not ok. \n");		
	return false;
	}
  }
  else
	  {    
	  perror ("Couldn't open the directory");
	  return false;
	  }

}	
/////////////////////////////////////////////////////////////////////////////////////////////


}

BOOL CodecPlugin::Unload(void)
{
	// This can happen a lot, so avoid DB_ENTER to save space
	DB("CodecPlugin::Unload - " << m_sFilename.c_str());

	bool rc(true);

	if (!m_hLibrary)
		rc = false;
	else
	{
		dlclose(m_hLibrary);
		m_hLibrary = NULL;
	}

	m_sFilename.erase();
	m_hLibrary = NULL;
	m_pCodecInterface = NULL;

	return rc;
}

BaseEncoder *CodecPlugin::NewEncoder(void)
{
	ASSERT(m_pCodecInterface);

	return (BaseEncoder *)m_pCodecInterface->NewEncoder();
}

BaseDecoder *CodecPlugin::NewDecoder(void)
{
	ASSERT(m_pCodecInterface);

	return (BaseDecoder *)m_pCodecInterface->NewDecoder();
}

void CodecPlugin::SetModuleInstance(void* hModuleInstance)
{
	ms_hModuleInstance = hModuleInstance;
}

void * CodecPlugin::GetModuleInstance(void)
{
	return ms_hModuleInstance;
}
