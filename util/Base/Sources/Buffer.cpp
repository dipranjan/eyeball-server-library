/********************************************************************
 * 
 * File: Buffer.cpp
 * Author: Eyeball Networks Inc.
 * Copyright: Eyeball Networks Inc. 1999-2002
 *
 * Purpose: Provides the base class Buffer
 * 
 * Change Log: 
 *
 *******************************************************************/

#include "Base.h"  // automatic use of precompiled headers 
#include "Buffer.h"

#include "debug.h"


FrameBuffer::FrameBuffer(): 
	m_size(0), 
	m_alloc_size(0),
	m_bp(NULL)
{
	//SetBufferSize(176*144*3);	
}

FrameBuffer::FrameBuffer(int size)	
{
	SetBufferSize(size);	
}

FrameBuffer::~FrameBuffer()
{
	Free();
}


const FrameBuffer& FrameBuffer::operator=(const FrameBuffer& b)
{
	if(this == &b) {
		Free();
		Copy(b);
	}
	return *this;
}


void FrameBuffer::Copy(const FrameBuffer& b)
{
	m_size = b.m_size;
	if(m_size > 0){
		m_bp = new BYTE[m_size];
		ASSERT(m_bp);
		memcpy(m_bp, b.m_bp, m_size);
	}	
}

void FrameBuffer::Free()
{	
	DELETE_ARRAY(m_bp);
	m_size = 0;	
}

void FrameBuffer::DataCopy(BYTE* buffer, int size)
{
	if(size == 0||buffer == NULL) return;
	if(size > m_alloc_size)	
		SetBufferSize(size);
	memcpy(m_bp, buffer, size);
	m_size = size;
}


void FrameBuffer::iDataCopy(BYTE* buffer, int size)
{
	if(size == 0||buffer == NULL) return;
	ASSERT(size <= m_size);	
	memcpy(buffer,m_bp,size);
}


void FrameBuffer::SetBufferSize(int size)
{	
	
	if(size > m_alloc_size){
		DELETE_ARRAY(m_bp);
		m_alloc_size = size;
		m_bp = new BYTE[m_alloc_size];
		ASSERT(m_bp);
		//OutputDebugString("FrameBuffer::SetMinSize \n");
	}

	m_size = size;

}
