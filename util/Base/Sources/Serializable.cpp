// ---------------------------------------------------------------------------
// File:       Serializable.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2002
//
// Purpose:    Base class for network messages
//
// Change Log:
// ---------------------------------------------------------------------------

//#ifdef WIN32
#include "Base.h"  // automatic use of precompiled headers
//#endif
#include "inet.h"
#include "Serializable.h"

using std::string;


// ---------------------------------------------------------------------------
// Serialization/deserialization helper functions
// ---------------------------------------------------------------------------

inline void InlineSerializeBytes(
	const void* p, 
	unsigned int uSize, 
	string& sBuf)
{
	sBuf.append((const char*)p, uSize);
}

void SerializeBytes(const void* p, unsigned int uSize, string& sBuf)
{
	InlineSerializeBytes(p, uSize, sBuf);
}

void Serialize8(u_int8_t u8, string& sBuf)
{
	InlineSerializeBytes(&u8, sizeof(u_int8_t), sBuf);
}

void Serialize16(u_int16_t u16, string& sBuf)
{
	u_int16_t t = htons(u16);
	InlineSerializeBytes(&t, sizeof(u_int16_t), sBuf);
}

void Serialize32(u_int32_t u32, string& sBuf)
{
	u_int32_t t = htonl(u32);
	InlineSerializeBytes(&t, sizeof(u_int32_t), sBuf);
}

void SerializeCString(const char* p, unsigned int uSize, string& sBuf)
{
	InlineSerializeBytes(p, uSize, sBuf);
}

void SerializeString8(const string& s, string& sBuf)
{
	DB_ASSERT(s.size() <= 255);
	u_int8_t uSize = MIN(255, s.size());

	Serialize8(uSize, sBuf);
	InlineSerializeBytes(s.data(), uSize, sBuf);
}

void SerializeString16(const string& s, string& sBuf)
{
	DB_ASSERT(s.size() <= 65535);
	u_int16_t uSize = MIN(65535, s.size());

	Serialize16(uSize, sBuf);
	InlineSerializeBytes(s.data(), uSize, sBuf);
}

void SerializeString32(const string& s, string& sBuf)
{
	u_int32_t uSize = s.size();

	Serialize32(uSize, sBuf);
	InlineSerializeBytes(s.data(), uSize, sBuf);
}

inline bool InlineDeserializeBytes(
	void* p,
	unsigned int uSize,
	const string& sBuf,
	int& iOffset)
{
	if ((int)sBuf.size() < iOffset + uSize)
	{
		DB_ASSERT(0 && "Deserialization failed");
		return false;
	}

	memcpy(p, &sBuf.data()[iOffset], uSize);
	iOffset += uSize;

	return true;
}

bool DeserializeBytes(
	void* p, 
	unsigned int uSize, 
	const string& sBuf,
	int& iOffset)
{
	return InlineDeserializeBytes(p, uSize, sBuf, iOffset);
}

bool Deserialize8(u_int8_t& u8, const string& sBuf, int& iOffset)
{
	return InlineDeserializeBytes(&u8, sizeof(u_int8_t), sBuf, iOffset);
}

bool Deserialize16(u_int16_t& u16, const string& sBuf, int& iOffset)
{
	if (!InlineDeserializeBytes(&u16, sizeof(u_int16_t), sBuf, iOffset))
		return false;

	u16 = ntohs(u16);

	return true;
}

bool Deserialize32(u_int32_t& u32, const string& sBuf, int& iOffset)
{
	if (!InlineDeserializeBytes(&u32, sizeof(u_int32_t), sBuf, iOffset))
		return false;

	u32 = ntohl(u32);

	return true;
}

bool DeserializeCString(
	char* p, 
	unsigned int uSize, 
	const string& sBuf, 
	int& iOffset)
{
	if (!InlineDeserializeBytes(p, uSize, sBuf, iOffset))
		return false;

	p[uSize - 1] = '\0';

	return true;
}

bool DeserializeString8(string& s, const string& sBuf, int& iOffset)
{
	u_int8_t uSize = 0;
	if (!Deserialize8(uSize, sBuf, iOffset))
		return false;

	if ((int)sBuf.size() < iOffset + uSize)
	{
		DB_ASSERT(0 && "Deserialization failed");
		return false;
	}

	s.assign(sBuf, iOffset, uSize);
	iOffset += uSize;

	return true;
}

bool DeserializeString16(string& s, const string& sBuf, int& iOffset)
{
	u_int16_t uSize = 0;
	if (!Deserialize16(uSize, sBuf, iOffset))
		return false;

	if ((int)sBuf.size() < iOffset + uSize)
	{
		DB_ASSERT(0 && "Deserialization failed");
		return false;
	}

	s.assign(sBuf, iOffset, uSize);
	iOffset += uSize;

	return true;
}

bool DeserializeString32(string& s, const string& sBuf, int& iOffset)
{
	u_int32_t uSize = 0;
	if (!Deserialize32(uSize, sBuf, iOffset))
		return false;

	if ((int)sBuf.size() < iOffset + uSize)
	{
		DB_ASSERT(0 && "Deserialization failed");
		return false;
	}

	s.assign(sBuf, iOffset, uSize);
	iOffset += uSize;

	return true;
}
