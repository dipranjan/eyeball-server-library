// ---------------------------------------------------------------------------
// File:       ELinuxUtil.cpp
//
// Copyright:  ITAccess Inc. 
//
// Purpose:    Helper functions specific to Linux
//
// Change Log:
// ---------------------------------------------------------------------------

#include "Base.h"  // automatic use of precompiled headers 

#include "ELinuxUtil.h"
#include "Event.h"

// ---------------------------------------------------------------------------
// Implementation
// ---------------------------------------------------------------------------


DWORD eyeball::CustomWaitForSingleObject(
	HANDLE hHandle, 
	DWORD dwMilliseconds)
{
	DWORD dwResult = WaitForSingleObject(hHandle, dwMilliseconds);
	return dwResult;
}


