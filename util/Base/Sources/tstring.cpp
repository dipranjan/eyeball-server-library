// File: tstring.cpp

#include "Base.h"  // automatic use of precompiled headers 


#include "tstring.h"
#include "TextUtil.h"

// Constructors
tstring::tstring()
:ustring() 
{
}

tstring::tstring(const ustring& s) 
:ustring(s){

}

tstring::tstring(const char* p)
:ustring(p){

}


#ifdef AUTO_POINTER_CONVERT
tstring::operator LPCTSTR(void) const
{
	return c_str();
}

tstring::operator LPTSTR(void)
{
	return const_cast<TCHAR*>(c_str());
}
#endif //AUTO_POINTER_CONVERT

#ifdef AUTO_STRING_CONVERT
tstring::operator std::string(void) const
{
	return mbs();
}
#endif //AUTO_STRING_CONVERT

std::string tstring::mbs(void) const
{
	std::string s;
	s.assign(c_str());
	return s;
}

tstring tstring::ToLower()
{
	std::string s = mbs();
	return s;
}

#ifndef _WIN32_WCE
ostream& operator<<(ostream& s, const tstring& v)
{
	USES_CONVERSION;
	s << T2CA(v.c_str());
	return s;
}
#endif
