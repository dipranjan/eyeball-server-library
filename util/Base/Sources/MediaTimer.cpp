/********************************************************************
 * 
 * File: media-timer.cpp
 * Author: Infranet Solutions Ltd. 1999
 * Copyright: Infranet Solutions Ltd. 1999
 *
 * Purpose: ?
 * 
 * Change Log: 
 *
 *******************************************************************/

#include "Base.h"  // automatic use of precompiled headers 

#include <math.h>
#include <stdlib.h>
#include "config.h"
#include "sys-time.h"
#include "MediaTimer.h"

#ifndef _WIN32_WCE
#include <sys/timeb.h>
#include <time.h>
#endif
  

MediaTimer* MediaTimer::instance_;

MediaTimer::MediaTimer()
{
	instance_ = this;
	offset_ = rand();
}

MediaTimer::~MediaTimer()
{
	instance_ = 0;
}

/*
 * Default media timestamp -- convert unix system clock
 * into a 90Khz timestamp.  Grabbers override this virtual
 * method if they can provide their own time base.
 *
 * XXX
 * We save the corresponding unix time stamp to handle the
 * unix_ts() call the transmitter will make to get the correspondence
 * between the media timestamp & unix time.
 */
u_int32_t MediaTimer::media_ts()
{
	//timeval tv;
	
#ifndef _WIN32_WCE
   struct _timeb tb;     
   _ftime( &tb);

	//gettimeofday(&tv, 0);  

	u_int32_t u = tb.millitm;
	u = (u << 3) + u; /* x 9 */
	/* sec * 90Khz + (msec * 90Khz) / 1e3 */
	u = tb.time * 90000 + (u * 10);
	return (u + offset_);
#else
	// this API is not used
	return 0;
#endif
}

/*
 * compute media time corresponding to the current unix time.
 * in this generic routine, this is the same as media_ts() but,
 * if a grabber has hardware or kernel timestamping, this routine
 * must compute the correspondence between the hardware timestamp
 * and the unix clock and appropriately offset the timestamp to
 * correspond to the current clock.  (This information if vital
 * for cross-media synchronization.)
 */
u_int32_t MediaTimer::ref_ts()
{
	return (media_ts());
}
