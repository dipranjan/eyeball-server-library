// ---------------------------------------------------------------------------
// File:       EVent.cpp
//
// Copyright:  ITAccess Inc. 
//
// Purpose:    Event functions specific for Linux.
//
// Change Log:
// ---------------------------------------------------------------------------

#include "Base.h"  // automatic use of precompiled headers 
#include "Event.h"
#include <sys/time.h>
#include <unistd.h>


// ---------------------------------------------------------------------------
// Implementation
// ---------------------------------------------------------------------------

/*
 Event object
*/
typedef struct{
	BOOL				Signal;		//TRUE:シグナル「あり」、FALSE:シグナル「なし」
	BOOL				ManualReset;//TRUE:手動リセット、FALSE:自動リセット
	pthread_mutex_t	Mutex;			//ミューテックス
	pthread_cond_t	Condition;		//状態変数
} EVENT_OBJECT, *PEVENT_OBJECT;


/*
 CreateEvent
 イベントオブジェクトを生成する。
 注:第1、第4引数は使用しない。WINAPIとの互換性の為にのみ存在する。
*/
HANDLE CreateEvent(void* EventAttributes, BOOL ManualReset, BOOL InitialState, void* Name)
{
	PEVENT_OBJECT event;
	
	event = (PEVENT_OBJECT)malloc(sizeof(EVENT_OBJECT));	//Event objectの確保
	if (event){
		pthread_cond_init(&event->Condition, NULL);	//状態変数初期化	
		pthread_mutex_init(&event->Mutex, NULL);	//ミューテックス初期化
		event->ManualReset = ManualReset;
		event->Signal = InitialState;
		return (HANDLE)event;
	}
	else{
		return NULL;	//Event objectの確保が出来なかった
	}
	
}


/*
 CloseEvent(HANDLE hEvent);
 イベントオブジェクトを削除る。 
*/
BOOL CloseEvent(HANDLE hEvent)
{
	PEVENT_OBJECT event = (PEVENT_OBJECT)hEvent;
	
	pthread_cond_destroy(&event->Condition);	//状態変数の削除
	pthread_mutex_destroy(&event->Mutex);		//ミューテックスの削除
	free(hEvent);								//Event objectの削除
	return TRUE;
}	


/*
 SetEvent(HANDLE hEvent);
 イベントオブジェクトをシグナル状態にする。 
*/
BOOL SetEvent(HANDLE hEvent)
{
	PEVENT_OBJECT event = (PEVENT_OBJECT)hEvent;
	if (pthread_mutex_lock(&event->Mutex) == 0){
		event->Signal = TRUE;					//シグナルを「あり」とする
		pthread_cond_broadcast(&event->Condition);	//状態変数へシグナル通知
//		pthread_cond_signal(&event->Condition);	//状態変数へシグナル通知
		pthread_mutex_unlock(&event->Mutex);	
		return TRUE;
	}
	else{
		return FALSE;	//ミューテックスのロックが出来なかった
	}

}


/*
 ResetEvent
 イベントオブジェクトを非シグナル状態にする。
*/
BOOL ResetEvent(HANDLE hEvent)
{
	PEVENT_OBJECT event = (PEVENT_OBJECT)hEvent;

	if (pthread_mutex_lock(&event->Mutex) == 0){
		event->Signal = FALSE;				//シグナルを「なし」とする	
		pthread_mutex_unlock(&event->Mutex);
		return TRUE;
	}
	else{
//printf("Sawamura ResetEvent fail\n");
		return FALSE;	//ミューテックスのロックが出来なかった
	}

}
		

/*
 WaitForSingleObject 
 指定されたイベントオブジェクトの現在の状態をチェックする。
 オブジェクトが未通知状態になっていれば、呼び出し元スレッドは待機状態になる。
 この関数は、指定されたイベントオブジェクトが通知済状態になったとき、またはタイムアウト間隔が経過したときに終了する。
 注:WINAPIのWaitForSingleObjectはオブジェクトの種類が限定されていないが、
    このWaitForSingleObjectはオブジェクトをイベントオブジェクトに限定する。
    従って第１引数でイベントオブジェクト以外を渡してはならない。
*/
DWORD WaitForSingleObject(HANDLE hEvent, DWORD dwMilliseconds)
{
	PEVENT_OBJECT event = (PEVENT_OBJECT)hEvent;
	struct timespec abstime; 
	timeval tv;
	int sts;

//printf("Sawamura WaitForSingleObject\n");

	if (pthread_mutex_lock(&event->Mutex) == 0){
		if (event->Signal){
			//現在シグナル「あり」なので直に戻る
			if (!event->ManualReset)	event->Signal = FALSE; //自動リセットならシグナルを「なし」とする。
			pthread_mutex_unlock(&event->Mutex);
//printf("Sawamura thread Signal\n");
			return (WAIT_OBJECT_0);	//イベント通知あり
		}
		else{
			//現在シグナル「なし」
			if (dwMilliseconds == 0){
				//	待たない指定なので直ぐに戻る
				pthread_mutex_unlock(&event->Mutex);
				return  (WAIT_TIMEOUT);//タイムアウト
			}
			if (dwMilliseconds == INFINITE){
				//タイムアウトなし(永久)
				sts = pthread_cond_wait(&event->Condition, &event->Mutex);
			}
			else{
				//タイムアウトあり
				gettimeofday(&tv, NULL);//タイムアウト時刻を算出するために現在の絶対時刻を取得
				tv.tv_sec += (dwMilliseconds / 1000);	//タイムアウト値(m秒単位)に秒の桁があれば秒に加算	
				tv.tv_usec += ((dwMilliseconds % 1000 ) * 1000);	//余りを単位変換（ｍ−＞μ)してμ秒に加算
				tv.tv_sec += tv.tv_usec/1000000;	//μ秒に加算した結果に秒の桁があれば秒へ加算
				tv.tv_usec %= 1000000;				//余りをμ秒へ設定
				sts = EINTR;
				while(sts == EINTR){ 
					//戻り値が"EINTR"の場合はリトライする
					sts = pthread_cond_timedwait(&event->Condition, &event->Mutex, (timespec *)&tv);
				}
			}
			//Waitから戻ってきたので戻り値をチェックする
			pthread_mutex_unlock(&event->Mutex);
			if (sts == 0){
				//イベント通知「あり」
				if (!event->ManualReset)	event->Signal = FALSE; //自動リセットならシグナルを「なし」とする。
				pthread_mutex_unlock(&event->Mutex);
				return (WAIT_OBJECT_0);	//イベント通知あり
			}
			else if (sts == ETIMEDOUT){
				//タイムアウト
				pthread_mutex_unlock(&event->Mutex);
				return  (WAIT_TIMEOUT);//タイムアウト
			}
			else{
				//有り得ない
				pthread_mutex_unlock(&event->Mutex);
				return  (WAIT_FAILED);//何らかのエラー
			}
		}	
	}
	return (WAIT_FAILED);	//ミューテックスのロックが出来なかった
}


/*
 WaitForMultipleObjects
 指定された複数のイベントオブジェクトのひとつがシグナル状態になるか、
 または、タイムアウト時間が経過すると、制御を返します。
 注:WINAPIのWaitForMultipleObjectsはオブジェクトの種類が限定されていないが、
    このWaitForMultipleObjectsはオブジェクトをイベントオブジェクトに限定する。
    従って第2引数でイベントオブジェクト以外を渡してはならない。
    第三引数は意味を持たない、常にFALSEとして処理する。
 <実装に因る制限事項>
  このWaitForMultipleObjects関数は、複数のイベントを1つのスレッドで待つ手段として
  複数のイベントオブジェクトのサインフラグをポーリングする事によって実現している。
  その為にポーリングの休止中（スリープ中）にそのイベントオブジェクトにサインが送られた場合に
 （と言うか、サインは常にそのタイミングで送られるであろう）そのサインによって先に再開した別スレッドや
  または自動リセット機能によってサインフラグがOFFされる可能性がある。
  その場合、本関数がそのイベントオブジェクトのサインフラグを見た時点では既にそのフラグは既に落とされている。
  従って、本関数はサイン（イベント）を知らずに待ち続ける結果となる。
  この様な現象を回避する為には本関数で指定するイベントオブジェクトを同時に他のスレッドで待つ事がない様にする
  必要がある。
  逆に言えば、その様な場合は本関数を使用することは避けるべきである。
*/
DWORD WaitForMultipleObjects(DWORD nCount, HANDLE *pHandles, BOOL bWaitAll, DWORD dwMilliseconds)
{
	timeval tv,now;
	HANDLE *pEvents ;
	PEVENT_OBJECT event;
	DWORD count,sts;
	
	if (dwMilliseconds != INFINITE){
		//タイムアウト時刻算出
		gettimeofday(&tv, NULL);//現在の絶対時刻を取得
		tv.tv_sec += (dwMilliseconds / 1000);	//タイムアウト値(m秒単位)に秒の桁があれば秒に加算	
		tv.tv_usec += ((dwMilliseconds % 1000 ) * 1000);	//余りを単位変換（ｍ−＞μ)してμ秒に加算
		tv.tv_sec += tv.tv_usec/1000000;	//μ秒に加算した結果に秒の桁があれば秒へ加算
		tv.tv_usec %= 1000000;				//余りをμ秒へ設定
	}
	
	//タイムアウトまたはイベント通知までループ
	while (1){
		
		//タイムアウトを調べる
		if (dwMilliseconds != INFINITE){//時間指定あり
			gettimeofday(&now, NULL);//現在の絶対時刻を取得
			if (timercmp(&now,&tv,>)) return (WAIT_TIMEOUT);//タイムアウト
		}
		//指定イベント数分イベント通知の有無を調べる
		for (pEvents=pHandles,count=0; count<nCount; count++){
			if (sts=WaitForSingleObject(*(pEvents + count), 10) == WAIT_OBJECT_0) return (WAIT_OBJECT_0 + count);//イベント通知あり
#ifndef SLEEP_DISABLE
			usleep(1000);//少しスリープ
#else
			usleep(10);//少しスリープ
#endif
		}
		if (dwMilliseconds == 0) return (WAIT_TIMEOUT);//タイムアウト
	}
}

