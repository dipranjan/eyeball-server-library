// ---------------------------------------------------------------------------
// File:       CodecPlugin.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2002
//
// Purpose:    Class definitions for CodecPlugin.
//			   This is used to load/unload module and to access interface.
// 
// Change Log:
// ---------------------------------------------------------------------------

#include "Base.h"  // automatic use of precompiled headers 

#include "CodecPlugin.h"
#include "debug.h"
#include "EFileUtil.h"
#include "EUtil.h"
#include "TextUtil.h"

using std::string;


#ifndef win32
	#define  HINSTANCE void *
	
	///////////////////////////////for directory access /////////////////
	#include <sys/types.h>
	#include <dirent.h>
	////////////////////////////////////////////////////////////////////

	#include <dlfcn.h>
#endif

// ---------------------------------------------------------------------------
// Static member variables
// ---------------------------------------------------------------------------

HINSTANCE CodecPlugin::ms_hModuleInstance = NULL;


// ---------------------------------------------------------------------------
// Defines
// ---------------------------------------------------------------------------

#define TEXT_EYEBALL_EXPORTED_FUNCTION		TEXT("EyeballGetModule")
#define EYEBALL_EXPORTED_FUNCTION           "EyeballGetModule"

#ifndef win32
	#define CODEC_LIB_DIR 						"/usr/local/lib/"
	#define CODEC_LIB_DIR_CURRENT 						"./"
#endif
// ---------------------------------------------------------------------------
// Local helper functions
// ---------------------------------------------------------------------------
#ifdef win32
static tstring GetModuleDir(void)
{
	return GetModuleDir(CodecPlugin::GetModuleInstance());
}
#else
static string GetModuleDir(void)
{
	return string(CODEC_LIB_DIR);
}
#endif

// ---------------------------------------------------------------------------
// class CodecPlugin
// ---------------------------------------------------------------------------

CodecPlugin::CodecPlugin(CodecType _codec_type, 
						  int _m_iInterfaceVersion, 
						  int _m_iMajorVersion,						  						  
						  int _m_iMinorVersion)
:	
	m_CodecType(_codec_type),
	m_iInterfaceVersion(_m_iInterfaceVersion),
	m_iMajorVersion(_m_iMajorVersion),
	m_iMinorVersion(_m_iMinorVersion),
	m_sFilename(""),
	m_hLibrary(NULL),
	m_pCodecInterface(NULL)
{
}

CodecPlugin::~CodecPlugin()
{
	Unload();
}

BOOL CodecPlugin::Load(char chPayloadType)
{
#ifdef win32
	if (Preload(chPayloadType))
		return TRUE;

	if (m_hLibrary)
		return chPayloadType == m_pCodecInterface->chPayloadType;


	tstring filename(GetModuleDir());

	
	filename += _T("\\*.dll");

	WIN32_FIND_DATA find_data;
	HANDLE hFind = FindFirstFile(filename.c_str(), &find_data);

	if (hFind == INVALID_HANDLE_VALUE)
		return false;

	if (Load(find_data.cFileName))
	{
		if (m_pCodecInterface->chPayloadType == chPayloadType)
		{
			ASSERT(FindClose(hFind));
			return true;
		}

		ASSERT(Unload());
	}

	while (FindNextFile(hFind, &find_data))
	{
		if (Load(find_data.cFileName))
		{
			if (m_pCodecInterface->chPayloadType == chPayloadType)
			{
				ASSERT(FindClose(hFind));
				return true;
			}

			ASSERT(Unload());
		}
	}

	ASSERT(FindClose(hFind));

	return false;
#else
	m_payloadType = chPayloadType; ////////////new add///////
	if (Preload(chPayloadType))
		return TRUE;

	return FALSE;
#endif
}

BOOL CodecPlugin::Preload(char chPayloadType)
{
	if (m_hLibrary)
		return chPayloadType == m_pCodecInterface->chPayloadType;

	
#ifdef win32	
	tstring filename;

	if (chPayloadType == 104)
	{
		filename = _T("EyeStream7.DLL");
	}
	else if (chPayloadType == 101)
	{
		filename = _T("MELP.DLL");
	}
	else if (chPayloadType == 3)
	{
		filename = _T("GSM.DLL");
	}
	else if (chPayloadType == 1)
	{
		filename = _T("CELP.DLL");
	}
	else
		return FALSE;

	if (Load(filename))
	{
		if (m_pCodecInterface->chPayloadType == chPayloadType)
		{			
			return TRUE;
		}
		else
		{
			Unload();
			return FALSE;
		}
	}
	return FALSE;
#else

	string filename;

	
	if (chPayloadType == 34)
	{
		filename = string("libh263.so");
	}
	else if (chPayloadType == 0)  //ref: shantanu
	{
		filename = string("libg711.so");
	}
	else filename = string ("unknown");
		
	
	if (Load(filename))
	{
		if (m_pCodecInterface->chPayloadType == chPayloadType)
		{			
			return TRUE;
		}
		else
		{
			Unload();
			return FALSE;
		}
	}
	return FALSE;

#endif
}

#ifdef win32
BOOL CodecPlugin::Load(const tstring& _filename)
{
	
	tstring sFilename = GetPathName(GetModuleDir(), _filename);

	// This can happen a lot, so avoid DB_ENTER to save space
	//DB("CodecPlugin::Load - " << sFilename);

	// If a library is already loaded, check whether it matches the user request
	if (m_hLibrary != NULL)
		return sFilename == m_sFilename;

	// Load the codec DLL
	HINSTANCE hLibrary = LoadLibrary(sFilename.c_str());
	if (!hLibrary)
	{
		// Try without specifying the path.
		sFilename = _filename;
		hLibrary = LoadLibrary(sFilename.c_str());
		if (!hLibrary)
			return false;
	}

	CodecModule ptr;
#ifndef _WIN32_WCE
	ptr = (CodecModule)GetProcAddress(hLibrary, EYEBALL_EXPORTED_FUNCTION);
#else
	ptr = (CodecModule)GetProcAddress(hLibrary, TEXT_EYEBALL_EXPORTED_FUNCTION);
#endif

	if (!ptr)
	{
		//DB(sFilename.c_str() << " does not export " << EYEBALL_EXPORTED_FUNCTION);
		FreeLibrary(hLibrary);
		return false;
	}

	// Use it to get a pointer to the codec structure
	CodecInterface *codec_interface = (ptr)();
	if (!codec_interface)
	{
		//DB("Could not query codec interface from " << sFilename.c_str());
		FreeLibrary(hLibrary);
		return false;
	}

	if (codec_interface->iInterfaceVersion != m_iInterfaceVersion)
	{
		//DB("Error: " << sFilename.c_str() << " has incorrect interface version");
		FreeLibrary(hLibrary);
		return false;
	}

	if (codec_interface->codecType != m_CodecType)
	{
		//DB("Error: " << sFilename.c_str() << " is of incorrect codec type");
		FreeLibrary(hLibrary);
		return false;
	}

	if (codec_interface->iMajorVersion != m_iMajorVersion)
	{
		//DB("Error: " << sFilename.c_str() << " has incorrect major version");
		FreeLibrary(hLibrary);
		return false;
	}

	//if (codec_interface->minor_version != m_iMinorVersion)
	//{
		//DB("Warning: " << filename.c_str() << " has incorrect minor version");		
	//}

	// Successfully loaded, commit changes to member variables
	m_sFilename = sFilename;
	m_hLibrary = hLibrary;
	m_pCodecInterface = codec_interface;

	return true;


}


#else
BOOL CodecPlugin::Load(const string& _filename)
{
	CodecModule ptr;
	CodecInterface *codec_interface;
	bool search = true;
	bool libraryLoaded = false;
	void* hLibrary;
	
	
	string sFilename = GetPathName(GetModuleDir(), _filename);
	DB("CodecPlugin::Load - " << sFilename);


if ( _filename.compare("unknown"))
{
	
	// Load the codec DLL

	hLibrary = dlopen(sFilename.c_str(),RTLD_LAZY);
	printf ("After open of dll in current directory.\n");
	
	if (!hLibrary)
	{
		DB("Dlopne failed :"<<dlerror());
		// Try without specifying the path.
		sFilename = GetPathName(CODEC_LIB_DIR_CURRENT, _filename);;
		
	//	hLibrary = dlopen(sFilename.c_str(),RTLD_NOW|RTLD_GLOBAL);
		hLibrary = dlopen(sFilename.c_str(),RTLD_LAZY);
		if (!hLibrary)
			{
			DB("Dlopne failed :"<<dlerror());	
			//search = true;	
			//return false;
			}
		else search = false;	
	}
	else  search = false;	

}	
///////////////////////////////////////HERE CURRENT DIRECTORY SEARCH SHOULD BE DONE///////////
// codec already loaded successfully 	
if (!search)
{
	ptr = (CodecModule)dlsym(hLibrary, "EyeballGetModule");

	if (!ptr)
	{
		printf("Dlsym failed %s\n",dlerror());
		//DB(sFilename.c_str() << " does not export " << EYEBALL_EXPORTED_FUNCTION);
		dlclose(hLibrary);
		return false;
	}

	// Use it to get a pointer to the codec structure

	codec_interface = (ptr)();
	if (!codec_interface)
	{
		DB("Could not query codec interface from " << sFilename.c_str());
		dlclose(hLibrary);
		return false;
	}

	if (codec_interface->iInterfaceVersion != m_iInterfaceVersion)
	{
		DB("Error: " << sFilename.c_str() << " has incorrect interface version");
		dlclose(hLibrary);
		return false;
	}

	if (codec_interface->codecType != m_CodecType)
	{
		DB("Error: " << sFilename.c_str() << " is of incorrect codec type");
		dlclose(hLibrary);
		return false;
	}

	if (codec_interface->iMajorVersion != m_iMajorVersion)
	{
		DB("Error: " << sFilename.c_str() << " has incorrect major version");
		dlclose(hLibrary);
		return false;
	}

	// Successfully loaded, commit changes to member variables
	m_sFilename = sFilename;
	m_hLibrary = hLibrary;
	m_pCodecInterface = codec_interface;

	return true;
}
// directory search must be done
else
{	
  DIR *dp;
  struct dirent *ep;

       dp = opendir ("./");
  if (dp != NULL)
  {
    while (ep = readdir (dp))
	 {	
		 if (ep ==NULL) break;
			 
       // puts (ep->d_name);
	if (strstr (ep->d_name,".so")==NULL) 
		continue;
	
	
	sFilename = GetPathName(CODEC_LIB_DIR_CURRENT, ep->d_name);
	
	printf ("File name = %s\n",sFilename.c_str());
	
		
	//hLibrary = dlopen(sFilename.c_str(),RTLD_NOW|RTLD_GLOBAL);
	hLibrary = dlopen(sFilename.c_str(),RTLD_LAZY);
//	printf ("After open of dll in current directory.\n");
	if (!hLibrary)
			{
			DB("Dlopne failed :"<<dlerror());	
			//return false;
				continue;
			}
	
	ptr = (CodecModule)dlsym(hLibrary, "EyeballGetModule");


	if (!ptr)
	{
		printf("Dlsym failed %s\n",dlerror());
		//DB(sFilename.c_str() << " does not export " << EYEBALL_EXPORTED_FUNCTION);
		
		dlclose(hLibrary);
		hLibrary = NULL;
		continue;
	}

	// Use it to get a pointer to the codec structure


	codec_interface = (ptr)();
//	printf ("After codec_interface\n");
	if (!codec_interface)
	{
		DB("Could not query codec interface from " << sFilename.c_str());
		printf("codec_interface is null \n");
		dlclose(hLibrary);
		hLibrary = NULL;
		continue;
		
	}

	if (codec_interface->iInterfaceVersion != m_iInterfaceVersion)
	{
		DB("Error: " << sFilename.c_str() << " has incorrect interface version");
		printf("iInterfaceVersion is null \n");
		dlclose(hLibrary);
		hLibrary = NULL;
		continue;
		
	}

	if (codec_interface->codecType != m_CodecType)
	{
		DB("Error: " << sFilename.c_str() << " is of incorrect codec type");
		printf("codecType is null \n");
		dlclose(hLibrary);
		hLibrary = NULL;
		continue;
	}
	if (codec_interface->chPayloadType != m_payloadType)
	{
		DB("Error: " << sFilename.c_str() << " is of incorrect codec type");
		printf("codecType is null \n");
		dlclose(hLibrary);
		hLibrary = NULL;
		continue;
	}

//	printf ("Loop breaking %p\n",hLibrary);
	libraryLoaded = true;
	break;
	}	
	
    (void) closedir (dp);
	
	if (libraryLoaded)
	{
	printf ("every thing is ok.\n");
	m_sFilename = sFilename;
	m_hLibrary = hLibrary;
	m_pCodecInterface = codec_interface;
	return true;
	}
	else
	{
	printf("Every thing is not ok. \n");		
	return false;
	}
  }
  else
	  {    
	  perror ("Couldn't open the directory");
	  return false;
	  }

}	
/////////////////////////////////////////////////////////////////////////////////////////////
}

#endif
BOOL CodecPlugin::Unload(void)
{
	// This can happen a lot, so avoid DB_ENTER to save space
	//DB("CodecPlugin::Unload - " << m_sFilename.c_str());

	bool rc(true);

	if (!m_hLibrary)
		rc = false;
	else
	{
		#ifdef win32
		FreeLibrary(m_hLibrary);
		#else
		dlclose(m_hLibrary);
		#endif
		m_hLibrary = NULL;
	}

	m_sFilename.erase();
	m_hLibrary = NULL;
	m_pCodecInterface = NULL;

	return rc;
}

BaseEncoder *CodecPlugin::NewEncoder(void)
{
	ASSERT(m_pCodecInterface);

	return (BaseEncoder *)m_pCodecInterface->NewEncoder();
}

BaseDecoder *CodecPlugin::NewDecoder(void)
{
	ASSERT(m_pCodecInterface);

	return (BaseDecoder *)m_pCodecInterface->NewDecoder();
}

void CodecPlugin::SetModuleInstance(HINSTANCE hModuleInstance)
{
	ms_hModuleInstance = hModuleInstance;
}

HINSTANCE CodecPlugin::GetModuleInstance(void)
{
	return ms_hModuleInstance;
}


// ---------------------------------------------------------------------------
// class CodecPluginIterator
// ---------------------------------------------------------------------------

#ifdef win32

CodecPluginIterator::CodecPluginIterator(int _interface_version)
:
	m_iInterfaceVersion(_interface_version),

	//m_findData,
	m_hFind(INVALID_HANDLE_VALUE)
{
	memset(&m_findData, 0, sizeof(m_findData));
}

CodecPluginIterator::~CodecPluginIterator()
{
	if (m_hFind != INVALID_HANDLE_VALUE)
	{
		ASSERT(FindClose(m_hFind));
		m_hFind = INVALID_HANDLE_VALUE;
	}
}

bool CodecPluginIterator::Stat(const tstring& _filename, tstring& _description)
{
	// Load the codec DLL
	HINSTANCE _library = LoadLibrary(_filename.c_str());
	if (!_library)
		return false;

	CodecModule ptr;
#ifndef _WIN32_WCE
	ptr = (CodecModule)GetProcAddress(_library, EYEBALL_EXPORTED_FUNCTION);
#else
	ptr = (CodecModule)GetProcAddress(_library, TEXT_EYEBALL_EXPORTED_FUNCTION);
#endif
	if (!ptr)
	{
		//DB(_filename.c_str() << " does not export " << EYEBALL_EXPORTED_FUNCTION);
		FreeLibrary(_library);
		return false;
	}

	// Use it to get a pointer to the codec structure
	CodecInterface *codec_interface = (ptr)();
	if (!codec_interface)
	{
		//DB("Could not query codec interface from " << _filename.c_str());
		FreeLibrary(_library);
		return false;
	}

	if (codec_interface->iInterfaceVersion != m_iInterfaceVersion)
	{
		//DB("Error: " << _filename.c_str() << " has incorrect interface version");
		FreeLibrary(_library);
		return false;
	}

	// "%s %d.%d \"%s\""
	_description = _filename
		+ tstring(" ") + itoa(codec_interface->iMajorVersion)
		+ tstring(".") + itoa(codec_interface->iMinorVersion)
		+ tstring("\"") + codec_interface->szDescription + tstring("\"");

	tstring type;
	switch(codec_interface->codecType)
	{
		case CODEC_TYPE_AUDIO:
			type = "Audio";
			break;

		case CODEC_TYPE_VIDEO:
			type = "Video";
			break;

		default:
			type = "Unknown";
			break;
	}

	// " (%s %d)"
	_description += " (" + type + " " + tstring(itoa(codec_interface->chPayloadType)) + ")";

	FreeLibrary(_library);

	return true;
}

// Access functions to iterate through available plugins
void CodecPluginIterator::StartIteration(void)
{
	tstring filename(GetModuleDir());
	filename += _T("\\*.dll");

	m_hFind = FindFirstFile(filename.c_str(), &m_findData);
}

bool CodecPluginIterator::GetNextItem(tstring& _description)
{
	if (m_hFind == INVALID_HANDLE_VALUE)
		return false;

	for (;;)
	{
		// See if we can load current file
		if (Stat(m_findData.cFileName, _description))
			break;

		// We only get here if we have to skip over DLL's that aren't ours
		if (!FindNextFile(m_hFind, &m_findData))
		{
			ASSERT(FindClose(m_hFind));
			m_hFind = INVALID_HANDLE_VALUE;
			return false;
		}
	}

	// Advance cursor to next matching file
	if (!FindNextFile(m_hFind, &m_findData))
	{
		ASSERT(FindClose(m_hFind));
		m_hFind = INVALID_HANDLE_VALUE;
	}

	return true;
}

#endif