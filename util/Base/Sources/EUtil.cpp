// ---------------------------------------------------------------------------
// File:       EUtil.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2002
//
// Purpose:    Some utility functions
//
// Change Log:
// ---------------------------------------------------------------------------

#include "Base.h"  // automatic use of precompiled headers 

// not all includes required
#if 1 //for Linux yamada
//
#else
#include <atlbase.h>
#ifndef _WIN32_WCE
	#include <sys/timeb.h>
	#include <time.h>
#endif
#include "AtlWFile.h"
#endif //for Linux yamada
#include "debug.h"
#include "utypes.h"
#include "Timer.h"

#include "EUtil.h"

using std::string;

DWORD  GetSystemTime(void)
{
#if 0
	SYSTEMTIME system_time;

	GetSystemTime(&system_time);

	DWORD milliSec = system_time.wMilliseconds + 1000 * \
			(system_time.wSecond + (system_time.wMinute+system_time.wHour*60)*60);

	return milliSec;
#endif

#ifndef _WIN32_WCE
	struct _timeb timebuffer;
   
   _ftime( &timebuffer );

   //Time in milli seconds since midnight (00:00:00),
   //January 1, 1970, coordinated universal time (UTC).
   //should use timeGetTime() soon if we don't need end to end delay!!!!
   DWORD milliSec = (timebuffer.time-960830000)*1000 + timebuffer.millitm;

   return milliSec;
#else
   return timeGetTime();
#endif
}

int DIFF_SEQNO(u_int16_t x, u_int16_t y)
{
	int t = (x - y)%65536;
	
	if(t>32768)
		t -= 65536;
	
	else if(t<=-32768)
		t += 65536;
	
	return t;
}

TECStrArray::TECStrArray(DWORD size)
{
	m_array = new tstring[size];
        m_size = size;
}
TECStrArray::~TECStrArray()
{
	delete [] m_array;
}
void TECStrArray::Detach(SAFEARRAY** ppsa)
{
	ppsa = (SAFEARRAY**)m_array;
}
void TECStrArray::PutString(DWORD nindex,const tstring& sValue)
{
	if(nindex + 1 > m_size)
		return;
	m_array[nindex] = sValue;
}






