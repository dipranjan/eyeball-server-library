// $Id: critsec.cpp,v 1.1.1.1 2005/12/24 04:04:28 sazzad Exp $

/** All files of the DNDS Prototype are Copyright (C) 1999 by Andre Schoorl
 *  unless otherwise noted.
 *
 *  The DNDS Prototype is distributed in the public domain in the hope that it
 *  will be useful, but is provided `as is' and is subject to change without
 *  notice.  No warranty of any kind is made with regard to the software or
 *  documentation.  The author shall not be liable for for any error for
 *  incidental or consequential damages in connection with the software and
 *  the documentation.
 *
 *  Permission to copy the whole, unmodified DNDS package is granted provided
 *  that the copies are not made or distributed for resale (excepting nominal
 *  copying fees).  All redistributions in either source or binary form must
 *  include this disclaimer.
 */

#include "Base.h"  // automatic use of precompiled headers 

#ifdef __linux__
	// Define this to get __USE_UNIX98 for <pthread.h>
	#define _XOPEN_SOURCE 500

	// Explicitly include this first
	#include <features.h>
#endif

#include "critsec.h"
#include "debug.h"

void Mutex::reset(void)
{
//#ifdef WIN32
//	InitializeCriticalSection(&mut);
//#elif defined(__linux__)
	pthread_mutexattr_t attr;
		
	ASSERT(pthread_mutexattr_init(&attr) == 0); //added on 6-4-05
	//#if ((__GLIBC__ >= 2) && (__GLIBC_MINOR__ >= 1))
		ASSERT(pthread_mutexattr_settype(
			&attr, PTHREAD_MUTEX_RECURSIVE) == 0
		);
	//#else
	//	ASSERT(pthread_mutexattr_setkind_np(
	//		&attr, PTHREAD_MUTEX_RECURSIVE_NP) == 0
	//	);
	//#endif
	ASSERT(pthread_mutex_init(&mut, &attr) == 0);
//#else
//	holder = (pthread_t)-1,
//	lockCount = 0;
//
//	ASSERT(pthread_mutex_init(&mut, NULL) == 0);
//#endif
}

void Mutex::close(void)
{
#ifdef WIN32
	DeleteCriticalSection(&mut);
#else
	#ifndef __linux__
		DB_ASSERT(lockCount == 0);
		DB_ASSERT(holder == (pthread_t)-1);
	#endif
	pthread_mutex_destroy(&mut);
#endif
}

Critical_Section::Critical_Section(Mutex& _mutex)
:
	pm(&_mutex)
{
//	cleanup_push(); // mazhar as  pthread_cleanup_push() method changed
			
#ifdef WIN32
	cleanup_push();
	//OutputDebugString("waiting for cs ++++++++++++++++\n");
	EnterCriticalSection(&pm->mut);
	//OutputDebugString("entered cs -------------------\n");
#elif defined(__linux__)

	// pthread_cleanup_push() method changed //mazhar
	pthread_cleanup_push((void (*)(void *))pthread_mutex_unlock, (void *)&pm->mut);	

	int n = pthread_mutex_lock(&pm->mut);
	if(n!=0)
		printf("\n\n\n\n\n\nthe mutex lock returned non zero value = %d\n\n\n\n\n\n\n",n);
	if(n==EINVAL)
		printf("pthread_mutex_lock: the mutex has not been properly initialized.\n");	
	if(n==EDEADLK)
		printf("pthread_mutex_lock: deadlock problem.\n");	
	
	//ASSERT(n!=EINVAL && "pthread_mutex_lock: the mutex has not been properly initialized.");	
	//ASSERT(n!=EDEADLK && "pthread_mutex_lock: deadlock problem.");	

	//  thread_cleanup_pop changed
	pthread_cleanup_pop(0);
#else
	cleanup_push();

	if (pthread_mutex_trylock(&pm->mut) != 0)
	{
		// If holder is self, then self holds the lock already and
		// holder is not subject to change.
		// If holder is not self, then holder may change but never
		// to be self so the condition continues to be false.
		if (pthread_self() == pm->holder)
		{
			pm->lockCount++;
			return;
		}

		ASSERT(pthread_mutex_lock(&pm->mut) == 0);
	}

	DB_ASSERT(pm->lockCount == 0);
	pm->holder = pthread_self();
	pm->lockCount = 1;
#endif	
}

Critical_Section::~Critical_Section()
{
#ifdef WIN32
	LeaveCriticalSection(&pm->mut);
#else
	#ifndef __linux__
		DB_ASSERT(pthread_self() == pm->holder);

		if (pm->lockCount > 1)
		{
			pm->lockCount--;
			cleanup_pop();
			return;
		}

		pm->holder = (pthread_t)-1;
		pm->lockCount = 0;
	#endif

	//ASSERT(pthread_mutex_unlock(&pm->mut) == 0);

	// pthread_cleanup_push() method changed //mazhar
	pthread_cleanup_push((void (*)(void *))pthread_mutex_unlock, (void *)&pm->mut);	
	int n=pthread_mutex_unlock(&pm->mut);
	if(n!=0)
		printf("\n\n\n\n\n\nthe mutex unlock returned non zero value = %d\n\n\n\n\n\n\n",n);
	if(n==EINVAL)
		printf("pthread_mutex_unlock: the mutex has not been properly initialized.\n");	
	if(n==EPERM)
		printf("pthread_mutex_unlock: the calling thread does not own the mutex\n");	

	//cleanup_pop();

	//  thread_cleanup_pop changed
	pthread_cleanup_pop(0);
#endif
}

void Critical_Section::cleanup_push(void)
{
#ifdef WIN32
	// No cleanup handler for Windows
#elif defined(__linux__)	
	//_pthread_cleanup_push(&_buffer,
	//	(void (*)(void *))pthread_mutex_unlock, (void *)&pm->mut);				
#else
	__pthread_cleanup_push((void (*)(void *))pthread_mutex_unlock,
		(void *)&pm->mut, (caddr_t)_getfp(), &_cleanup_info);
#endif
}

void Critical_Section::cleanup_pop(void)
{
#ifdef WIN32
	// No cleanup handler for Windows
#elif defined(__linux__)
	//_pthread_cleanup_pop(&_buffer, 0);	
#else
	__pthread_cleanup_pop(0, &_cleanup_info);
#endif
}

#ifndef WIN32
int Critical_Section::cond_wait(pthread_cond_t *c)
{
	return pthread_cond_wait(c, &pm->mut);
}
#endif
