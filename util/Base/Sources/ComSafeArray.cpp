// ---------------------------------------------------------------------------
// File:       ComSafeArray.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2002
//
// Purpose:    Implementation of ComSafeArray.
// 
// Change Log:
// ---------------------------------------------------------------------------

#include "Base.h"  // automatic use of precompiled headers 

#include "ComSafeArray.h"

#ifdef _WIN32_WCE
void __stdcall  _com_issue_error(long errcode)
{
	::SetLastError(errcode);
}
#endif
CComSafeArray::CComSafeArray()
{ 
	ATLSafeArrayInit(this);
	vt = VT_EMPTY; 
}

CComSafeArray::CComSafeArray(const SAFEARRAY& saSrc, VARTYPE vtSrc)
{
	ATLSafeArrayInit(this);
	vt = (VARTYPE)(vtSrc | VT_ARRAY);
	_com_util::CheckError(::SafeArrayCopy((LPSAFEARRAY)&saSrc, &parray));
}

CComSafeArray::CComSafeArray(LPCSAFEARRAY pSrc, VARTYPE vtSrc)
{
	ATLSafeArrayInit(this);
	vt = (VARTYPE)(vtSrc | VT_ARRAY);
	_com_util::CheckError(::SafeArrayCopy((LPSAFEARRAY)pSrc, &parray));
}

CComSafeArray::CComSafeArray(const CComSafeArray& saSrc)
{
	ATLSafeArrayInit(this);
	*this = saSrc;
}

CComSafeArray::CComSafeArray(const VARIANT& varSrc)
{
	ATLSafeArrayInit(this);
	*this = varSrc;
}

CComSafeArray::CComSafeArray(LPCVARIANT pSrc)
{
	ATLSafeArrayInit(this);
	*this = pSrc;
}

CComSafeArray::CComSafeArray(VARTYPE vtSrc, DWORD dwElements)
{
	ATLSafeArrayInit(this);
	CreateOneDim(vtSrc, dwElements);
}

// Operations
void CComSafeArray::Attach(VARIANT& varSrc)
{
	DB_ASSERT(varSrc.vt & VT_ARRAY);

	// Free up previous safe array if necessary
	Clear();

	// give control of data to CComSafeArray
	memcpy(this, &varSrc, sizeof(varSrc));
	varSrc.vt = VT_EMPTY;
}

void CComSafeArray::Detach(void)
{
	vt = VT_EMPTY;
}

void CComSafeArray::Detach(VARIANT* pvar)
{
	DB_ASSERT(NULL != pvar);
	*pvar = *this;

	vt = VT_EMPTY;
}

void CComSafeArray::Detach(LPSAFEARRAY* ppsa)
{
	DB_ASSERT(NULL != ppsa);
	DB_ASSERT(NULL != parray);
	DB_ASSERT(vt & VT_ARRAY);

	*ppsa = parray;
	vt = VT_EMPTY;
}

// Assignment operators
CComSafeArray& CComSafeArray::operator=(const CComSafeArray& saSrc)
{
	DB_ASSERT(saSrc.vt & VT_ARRAY);

	_com_util::CheckError(::VariantCopy(this, (LPVARIANT)&saSrc));
	return *this;
}

CComSafeArray& CComSafeArray::operator=(const VARIANT& varSrc)
{
	DB_ASSERT(varSrc.vt & VT_ARRAY);

	_com_util::CheckError(::VariantCopy(this, (LPVARIANT)&varSrc));
	return *this;
}

CComSafeArray& CComSafeArray::operator=(LPCVARIANT pSrc)
{
	DB_ASSERT(pSrc->vt & VT_ARRAY);

	_com_util::CheckError(::VariantCopy(this, (LPVARIANT)pSrc));
	return *this;
}

CComSafeArray& CComSafeArray::operator=(const _variant_t& varSrc)
{
	DB_ASSERT(varSrc.vt & VT_ARRAY);

	_com_util::CheckError(::VariantCopy(this, (LPVARIANT)&varSrc));
	return *this;
}

// Comparison operators
BOOL CComSafeArray::operator==(const SAFEARRAY& saSrc) const
{
	return ATLCompareSafeArrays(parray, (LPSAFEARRAY)&saSrc);
}

BOOL CComSafeArray::operator==(LPCSAFEARRAY pSrc) const
{
	return ATLCompareSafeArrays(parray, (LPSAFEARRAY)pSrc);
}

BOOL CComSafeArray::operator==(const CComSafeArray& saSrc) const
{
	if (vt != saSrc.vt)
		return FALSE;

	return ATLCompareSafeArrays(parray, saSrc.parray);
}

BOOL CComSafeArray::operator==(const VARIANT& varSrc) const
{
	if (vt != varSrc.vt)
		return FALSE;

	return ATLCompareSafeArrays(parray, varSrc.parray);
}

BOOL CComSafeArray::operator==(LPCVARIANT pSrc) const
{
	if (vt != pSrc->vt)
		return FALSE;

	return ATLCompareSafeArrays(parray, pSrc->parray);
}

BOOL CComSafeArray::operator==(const _variant_t& varSrc) const
{
	if (vt != varSrc.vt)
		return FALSE;

	return ATLCompareSafeArrays(parray, varSrc.parray);
}


void CComSafeArray::CreateOneDim(VARTYPE vtSrc, DWORD dwElements,
	const void* pvSrcData, long nLBound)
{
	// Setup the bounds and create the array
	SAFEARRAYBOUND rgsabound;
	rgsabound.cElements = dwElements;
	rgsabound.lLbound = nLBound;
	Create(vtSrc, 1, &rgsabound);

	// Copy over the data if neccessary
	if (pvSrcData != NULL)
	{
		void* pvDestData;
		AccessData(&pvDestData);
		memcpy(pvDestData, pvSrcData, GetElemSize() * dwElements);
		UnaccessData();
	}
}

DWORD CComSafeArray::GetSize(DWORD nDim)
{
	DB_ASSERT(nDim > 0 && nDim <= GetDim());

	long nUBound = 0;
	long nLBound = 0;

	GetUBound(nDim, &nUBound);
	GetLBound(nDim, &nLBound);

	long n = nUBound + 1 - nLBound;
	if (n < 0)
	{
		DB_ASSERT(0 && "negative size");
		return 0;
	}

	return n;
}

void CComSafeArray::ResizeOneDim(DWORD dwElements)
{
	DB_ASSERT(GetDim() == 1);

	SAFEARRAYBOUND rgsabound;

	rgsabound.cElements = dwElements;
	rgsabound.lLbound = 0;

	Redim(&rgsabound);
}

void CComSafeArray::Create(VARTYPE vtSrc, DWORD dwDims, DWORD* rgElements)
{
	DB_ASSERT(rgElements != NULL);

	// Allocate and fill proxy array of bounds (with lower bound of zero)
	SAFEARRAYBOUND* rgsaBounds = new SAFEARRAYBOUND[dwDims];

	for (DWORD dwIndex = 0; dwIndex < dwDims; dwIndex++)
	{
		// Assume lower bound is 0 and fill in element count
		rgsaBounds[dwIndex].lLbound = 0;
		rgsaBounds[dwIndex].cElements = rgElements[dwIndex];
	}

#ifndef _WIN32_WCE
	try
#endif
	{
		Create(vtSrc, dwDims, rgsaBounds);
	}
#ifndef _WIN32_WCE
	catch(_com_error & e)
	{
		// Must free up memory
		delete[] rgsaBounds;
		_com_raise_error(e.Error());
	}
#endif

	delete[] rgsaBounds;
}

void CComSafeArray::Create(VARTYPE vtSrc, DWORD dwDims, SAFEARRAYBOUND* rgsabound)
{
	DB_ASSERT(dwDims > 0);
	DB_ASSERT(rgsabound != NULL);

	// Validate the VARTYPE for SafeArrayCreate call
	DB_ASSERT(!(vtSrc & VT_ARRAY));
	DB_ASSERT(!(vtSrc & VT_BYREF));
	DB_ASSERT(!(vtSrc & VT_VECTOR));
	DB_ASSERT(vtSrc != VT_EMPTY);
	DB_ASSERT(vtSrc != VT_NULL);

	// Free up old safe array if necessary
	Clear();

	parray = ::SafeArrayCreate(vtSrc, dwDims, rgsabound);

#ifndef _WIN32_WCE
	if (parray == NULL)
		_com_raise_error(E_OUTOFMEMORY);
#endif

	vt = unsigned short(vtSrc | VT_ARRAY);
}

void CComSafeArray::AccessData(void** ppvData)
{
	_com_util::CheckError(::SafeArrayAccessData(parray, ppvData));
}

void CComSafeArray::UnaccessData()
{
	_com_util::CheckError(::SafeArrayUnaccessData(parray));
}

void CComSafeArray::AllocData()
{
	_com_util::CheckError(::SafeArrayAllocData(parray));
}

void CComSafeArray::AllocDescriptor(DWORD dwDims)
{
	_com_util::CheckError(::SafeArrayAllocDescriptor(dwDims, &parray));
}

void CComSafeArray::Copy(LPSAFEARRAY* ppsa)
{
	_com_util::CheckError(::SafeArrayCopy(parray, ppsa));
}

void CComSafeArray::GetLBound(DWORD dwDim, long* pLbound)
{
	_com_util::CheckError(::SafeArrayGetLBound(parray, dwDim, pLbound));
}

void CComSafeArray::GetUBound(DWORD dwDim, long* pUbound)
{
	_com_util::CheckError(::SafeArrayGetUBound(parray, dwDim, pUbound));
}

void CComSafeArray::GetElement(long lIndex, void* pvData)
{
	DB_ASSERT(GetDim() == 1);

	_com_util::CheckError(::SafeArrayGetElement(parray, &lIndex, pvData));
}

void CComSafeArray::GetElementEx(long* rgIndices, void* pvData)
{
	_com_util::CheckError(::SafeArrayGetElement(parray, rgIndices, pvData));
}

void CComSafeArray::PtrOfIndex(long* rgIndices, void** ppvData)
{
	_com_util::CheckError(::SafeArrayPtrOfIndex(parray, rgIndices, ppvData));
}

void CComSafeArray::PutElement(long lIndex, void* pvData)
{
	DB_ASSERT(GetDim() == 1);

	_com_util::CheckError(::SafeArrayPutElement(parray, &lIndex, pvData));
}

void CComSafeArray::PutElementEx(long* rgIndices, void* pvData)
{
	_com_util::CheckError(::SafeArrayPutElement(parray, rgIndices, pvData));
}

void CComSafeArray::Redim(SAFEARRAYBOUND* psaboundNew)
{
	_com_util::CheckError(::SafeArrayRedim(parray, psaboundNew));
}

void CComSafeArray::Lock()
{
	_com_util::CheckError(::SafeArrayLock(parray));
}

void CComSafeArray::Unlock()
{
	_com_util::CheckError(::SafeArrayUnlock(parray));
}

void CComSafeArray::Destroy()
{
	_com_util::CheckError(::SafeArrayDestroy(parray));
}

void CComSafeArray::DestroyData()
{
	_com_util::CheckError(::SafeArrayDestroyData(parray));
}

void CComSafeArray::DestroyDescriptor()
{
	_com_util::CheckError(::SafeArrayDestroyDescriptor(parray));
}

CComSafeArray::~CComSafeArray()
{ 
	Clear(); 
}
void CComSafeArray::Clear()
{ 
	// if it is an array
	if (vt & VT_ARRAY)
	{
		if (parray)
			Destroy();
		ATLSafeArrayInit(this); 
	}
	else
		_com_util::CheckError(::VariantClear(this)); 
}

CComSafeArray::operator LPVARIANT()
{ 
	return this; 
}

CComSafeArray::operator LPCVARIANT() const
{ 
	return this; 
}

DWORD CComSafeArray::GetDim()
{
	return ::SafeArrayGetDim(parray); 
}

DWORD CComSafeArray::GetElemSize()
{ 
	return ::SafeArrayGetElemsize(parray); 
}

///////////////////////////////////////////////////////////////////////////////
// CComSafeArray Helpers
void ATLSafeArrayInit(CComSafeArray* psa)
{
	memset(psa, 0, sizeof(*psa));
}

BOOL ATLCompareSafeArrays(SAFEARRAY* parray1, SAFEARRAY* parray2)
{
	BOOL bCompare = FALSE;

	// If one is NULL they must both be NULL to compare
	if (parray1 == NULL || parray2 == NULL)
	{
		return parray1 == parray2;
	}

	// Dimension must match and if 0, then arrays compare
	DWORD dwDim1 = ::SafeArrayGetDim(parray1);
	DWORD dwDim2 = ::SafeArrayGetDim(parray2);
	if (dwDim1 != dwDim2)
		return FALSE;
	else if (dwDim1 == 0)
		return TRUE;

	// Element size must match
	DWORD dwSize1 = ::SafeArrayGetElemsize(parray1);
	DWORD dwSize2 = ::SafeArrayGetElemsize(parray2);
	if (dwSize1 != dwSize2)
		return FALSE;

	long* pLBound1 = NULL;
	long* pLBound2 = NULL;
	long* pUBound1 = NULL;
	long* pUBound2 = NULL;

	void* pData1 = NULL;
	void* pData2 = NULL;

#ifndef _WIN32_WCE
	try
#endif
	{
		// Bounds must match
		pLBound1 = new long[dwDim1];
		pLBound2 = new long[dwDim2];
		pUBound1 = new long[dwDim1];
		pUBound2 = new long[dwDim2];

		size_t nTotalElements = 1;

		// Get and compare bounds
		for (DWORD dwIndex = 0; dwIndex < dwDim1; dwIndex++)
		{
			_com_util::CheckError(::SafeArrayGetLBound(
				parray1, dwIndex+1, &pLBound1[dwIndex]));
			_com_util::CheckError(::SafeArrayGetLBound(
				parray2, dwIndex+1, &pLBound2[dwIndex]));
			_com_util::CheckError(::SafeArrayGetUBound(
				parray1, dwIndex+1, &pUBound1[dwIndex]));
			_com_util::CheckError(::SafeArrayGetUBound(
				parray2, dwIndex+1, &pUBound2[dwIndex]));

			// Check the magnitude of each bound
			if (pUBound1[dwIndex] - pLBound1[dwIndex] !=
				pUBound2[dwIndex] - pLBound2[dwIndex])
			{
				delete[] pLBound1;
				delete[] pLBound2;
				delete[] pUBound1;
				delete[] pUBound2;

				return FALSE;
			}

			// Increment the element count
			nTotalElements *= pUBound1[dwIndex] - pLBound1[dwIndex] + 1;
		}

		// Access the data
		_com_util::CheckError(::SafeArrayAccessData(parray1, &pData1));
		_com_util::CheckError(::SafeArrayAccessData(parray2, &pData2));

		// Calculate the number of bytes of data and compare
		size_t nSize = nTotalElements * dwSize1;
		int nOffset = memcmp(pData1, pData2, nSize);
		bCompare = nOffset == 0;

		// Release the array locks
		_com_util::CheckError(::SafeArrayUnaccessData(parray1));
		_com_util::CheckError(::SafeArrayUnaccessData(parray2));
	}
#ifndef _WIN32_WCE
	catch(_com_error & e)
	{
		// Clean up bounds arrays
		delete[] pLBound1;
		delete[] pLBound2;
		delete[] pUBound1;
		delete[] pUBound2;

		// Release the array locks
		if (pData1 != NULL)
			_com_util::CheckError(::SafeArrayUnaccessData(parray1));
		if (pData2 != NULL)
			_com_util::CheckError(::SafeArrayUnaccessData(parray2));

		_com_raise_error(e.Error());
	}
#endif	

	// Clean up bounds arrays
	delete[] pLBound1;
	delete[] pLBound2;
	delete[] pUBound1;
	delete[] pUBound2;

	return bCompare;
}

void ATLCreateOneDimArray(VARIANT& varSrc, DWORD dwSize)
{
	UINT nDim;

	// Clear VARIANT and re-create SafeArray if necessary
	if (varSrc.vt != (VT_UI1 | VT_ARRAY) ||
		(nDim = ::SafeArrayGetDim(varSrc.parray)) != 1)
	{
		_com_util::CheckError(::VariantClear(&varSrc));
		varSrc.vt = VT_UI1 | VT_ARRAY;

		SAFEARRAYBOUND bound;
		bound.cElements = dwSize;
		bound.lLbound = 0;
		varSrc.parray = ::SafeArrayCreate(VT_UI1, 1, &bound);
#ifndef _WIN32_WCE
		if (varSrc.parray == NULL)
			_com_raise_error(E_OUTOFMEMORY);
#endif
	}
	else
	{
		// Must redimension array if necessary
		long lLower, lUpper;
		_com_util::CheckError(::SafeArrayGetLBound(varSrc.parray, 1, &lLower));
		_com_util::CheckError(::SafeArrayGetUBound(varSrc.parray, 1, &lUpper));

		// Upper bound should always be greater than lower bound
		long lSize = lUpper - lLower;
		if (lSize < 0)
		{
			DB_ASSERT(FALSE);
			lSize = 0;

		}

		if ((DWORD)lSize != dwSize)
		{
			SAFEARRAYBOUND bound;
			bound.cElements = dwSize;
			bound.lLbound = lLower;
			_com_util::CheckError(::SafeArrayRedim(varSrc.parray, &bound));
		}
	}
}

void ATLCopyBinaryData(SAFEARRAY* parray, const void* pvSrc, DWORD dwSize)
{
	// Access the data, copy it and unaccess it.
	void* pDest;
	_com_util::CheckError(::SafeArrayAccessData(parray, &pDest));
	memcpy(pDest, pvSrc, dwSize);
	_com_util::CheckError(::SafeArrayUnaccessData(parray));
}

