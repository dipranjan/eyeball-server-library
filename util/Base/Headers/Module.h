// ---------------------------------------------------------------------------
// File:       module.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Provides the base class Module
// 
// Change Log:
// ---------------------------------------------------------------------------

#ifndef EYECONTACT_MODULE_H
#define EYECONTACT_MODULE_H

#if 0 //for linux yamada
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif //for Linux yamada

#include "EUtil.h"


#define EXPORT_KEY BASE_EXPORT_KEY
#include "export.h"


class Buffer;
class Module;
class Mutex;

typedef enum tagTargetModuleIdentifier
{
	MODULE_VIDEO_CAPTURE = 0,
	MODULE_VIDEO_ENCODER,
	MODULE_VIDEO_ENCODER_EX,
	MODULE_VIDEO_PACKETIZER,
	MODULE_VIDEO_ARCHIVER,

	MODULE_VIDEO_DEPACKETIZER = 20,
	MODULE_VIDEO_DECODER,
	MODULE_VIDEO_RENDERER

} TargetModuleID;





typedef std::map<int, Module*> ModuleMap;

/**
 * Base class for Eyeball Modules.
 *
 * A Module performs processing on a data buffer.  Modules can be connected
 * to other Modules to form a data-flow graph, with each Module being a node.
 *
 * Each module contains a "target", which is a pointer to the next module
 * in the graph.
 */
class CLASS_DECL_EXPORT Module
{
public:
	/**
	 * Performs module-specific processing on the buffer.
	 */
	virtual int Process(Buffer*) = 0;


	/**
	* Call the Process method on all connected modules.
	*
	* @param pBuffer: The pointer to the buffer.
	*
	* @return ES_SUCCESS if successful; other negative values if failed.
	*/
	int TargetBatchProcess(Buffer* pBuffer);


	/**
	* Add a new target module.
	*
	* @param pTargetModule: The pointer to the new target module.
	* @param tmID:          The identifier of the new target module.
	*
	* @return TRUE if successful; FALSE otherwise.
	*/
	BOOL AddTarget(Module* pTargetModule, int tmID);


	/**
	* Remove a connected target identified by tmID.
	*
	* @param tmID: The identifier of the target to be removed.
	*
	* @return TRUE if successful; FALSE otherwise.
	*/
	BOOL RemoveTarget(int tmID);
	
	/**
	* Remove all connected targets.
	*
	* @return TRUE if successful; FALSE otherwise.
	*/
	BOOL RemoveAllTargets();


	/**
	* Determine if this module is able to accept new data for process
	*
	* @return TRUE if accepted; FALSE otherwise.
	*/
	virtual BOOL AcceptData();

	/**
	* Determine if the target module is able to accept new data for process
	*
	* @return TRUE if accepted; FALSE otherwise.
	*/
	virtual BOOL TargetAcceptData();


protected:
	Module();
	virtual ~Module();

private:
	ModuleMap  m_moduleMap;
	Mutex*     m_pModuleMutex;
};

#endif // EYECONTACT_MODULE_H
