// ---------------------------------------------------------------------------
// File:       CodecInterface.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Interface definition for codec plug-ins
// 
// Change Log:
// ---------------------------------------------------------------------------

#ifndef CODEC_INTERFACE_H
#define CODEC_INTERFACE_H


#define CODEC_INTERFACE_VER 0x1


enum CodecType
{
	CODEC_TYPE_UNKNOWN = 0,
	CODEC_TYPE_AUDIO = 1,
	CODEC_TYPE_VIDEO = 2
};

struct BaseEncoder { virtual ~BaseEncoder() {} };
struct BaseDecoder { virtual ~BaseDecoder() {} };

/**
 * Codec Interface
 */
struct CodecInterface
{
	/**
	 * version of combined interface, should almost never change
	 */
	int iInterfaceVersion;
	
	/**
	 * version of plug-in, for incompatible changes
	 */
	int iMajorVersion;

	/**
	 * version of plug-in, for compatible changes
	 */
	int iMinorVersion;

	/**
	 * type of codec, currently either audio or video
	 */
	CodecType codecType;

	/**
	 * should match 7-bit field in RTP header
	 */
	char chPayloadType;

	/**
	 * description of codec
	 */
	char* szDescription;

	BaseEncoder *(*NewEncoder)(void);
	BaseDecoder *(*NewDecoder)(void);
};


// The following ifdef block is the standard way of creating macros which make
// exporting from a DLL simpler.  All files within this DLL are compiled with
// the EYEBALL_CODEC_EXPORTS symbol defined on the command line.  This symbol
// should not be defined on any project that uses this DLL.  This way any other
// project whose source files include this file see EYEBALL_CODEC_API functions
// as being imported from a DLL, whereas this DLL sees symbols defined with
// this macro as being exported.
#ifdef WIN32
	#ifdef EYEBALL_CODEC_EXPORTS
	#define EYEBALL_CODEC_API __declspec(dllexport)
	#else
	#define EYEBALL_CODEC_API __declspec(dllimport)
	#endif
#else
	#define EYEBALL_CODEC_API extern "C" 
#endif


// Exported symbol is EyeballGetModule
EYEBALL_CODEC_API CodecInterface *EyeballGetModule(void);
typedef CodecInterface * (* CodecModule)(void);

EYEBALL_CODEC_API void test(void);
typedef void testptr(void);


#endif // CODEC_INTERFACE_H
