// ---------------------------------------------------------------------------
// File:       DxVersion.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Check which DirectX version installed in the user computer  
// 
// Change Log:
// ---------------------------------------------------------------------------

#ifndef _DX_VERSION_H_
#define _DX_VERSION_H_

/**
 * Finds the DirectX version by testing known interfaces.
 *
 * @return DirectX version, e.g. 0x700
 */
DWORD GetDXVersionInternal(void);

/**
 * Same as above function, but caches the result.
 *
 * @return DirectX version, e.g. 0x700
 */
DWORD GetDXVersion(void);

BOOL  VmCheckDXVersion(DWORD dwMinVersion = 0x800);

#endif  //_DX_VERSION_H_