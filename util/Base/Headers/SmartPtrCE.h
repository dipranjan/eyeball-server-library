#ifndef SMART_PTR_CE_H
#define SMART_PTR_CE_H

//  class noncopyable  -------------------------------------------------------//

//  Private copy constructor and copy assignment ensure classes derived from
//  class noncopyable cannot be copied.

class noncopyable
{
protected:
    noncopyable(){}
    ~noncopyable(){}
private:  // emphasize the following members are private
    noncopyable( const noncopyable& );
    const noncopyable& operator=( const noncopyable& );
}; 



//  SharedPtr  --------------------------------------------------------------//

//  An enhanced relative of ScopedPtr with reference counted copy semantics.
//  The object pointed to is deleted when the last SharedPtr pointing to it
//  is destroyed or reset.

template<typename T> class SharedPtr {
  public:
   typedef T element_type;

   explicit SharedPtr(T* p =0) : px(p) {
       pn = new long(1); 
   }

   SharedPtr(const SharedPtr& r) : px(r.px)
   {
      ++*(pn = r.pn);
   }  // never throws

   ~SharedPtr() { dispose(); }

   SharedPtr& operator=(const SharedPtr& r) {
      share(r.px,r.pn);
      return *this;
   }


   void reset(T* p=0)
   {
      if ( px == p ) return;  // fix: self-assignment safe
      if (--*pn == 0) { delete px; }
	  else {
		  pn = new long;
	  }
      *pn = 1;
      px = p;
   } // reset

   T& operator*() const
   { 
	   return *px; 
   }  // never throws

   T* operator->() const
   { 
	   return px; 
   }  // never throws

   T* get() const
   { 
	   return px;
   }  // never throws

   T*     px;     // contained pointer
   long*  pn;     // ptr to reference counter


   void dispose() 
   { 
	   if (--*pn == 0) 
	   { 
		   delete px;
		   delete pn;
	   }
   }

   void share(T* rpx, long* rpn)
   {
      if (pn != rpn) {
         dispose();
         px = rpx;
         ++*(pn = rpn);

      }
   } // share
};  // SharedPtr

template<typename T, typename U>
  inline bool operator==(const SharedPtr<T>& a, const SharedPtr<U>& b)
    { return a.get() == b.get(); }

template<typename T, typename U>
  inline bool operator!=(const SharedPtr<T>& a, const SharedPtr<U>& b)
    { return a.get() != b.get(); }

//  SharedArray  ------------------------------------------------------------//

//  SharedArray extends SharedPtr to arrays.
//  The array pointed to is deleted when the last SharedArray pointing to it
//  is destroyed or reset.

template<typename T> class SharedArray {
  public:
   typedef T element_type;

   explicit SharedArray(T* p =0) : px(p)
   {
       pn = new long(1); 
   }

   SharedArray(const SharedArray& r) : px(r.px)  // never throws
   {
      ++*(pn = r.pn);
   }

   ~SharedArray() { dispose(); }

   SharedArray& operator=(const SharedArray& r)
   {
      if (pn != r.pn) {
         dispose();
         px = r.px;
         ++*(pn = r.pn);
      }
      return *this;
   } // operator=

   void reset(T* p=0) {
      if ( px == p ) return;  // fix: self-assignment safe
	  if (--*pn == 0) { delete [] px; }
      else 
	  { // allocate new reference counter
		  pn = new long; 
      } // allocate new reference counter
      *pn = 1;
      px = p;
   } // reset

   T* get() const                     { return px; }  // never throws
   T& operator[](int i) const { return px[i]; }  // never throws

  private:

   T*     px;     // contained pointer
   long*  pn;     // ptr to reference counter

   void dispose() { if (--*pn == 0) { delete [] px; delete pn; } }
};  // SharedArray

template<typename T>
  inline bool operator==(const SharedArray<T>& a, const SharedArray<T>& b)
    { return a.get() == b.get(); }

template<typename T>
  inline bool operator!=(const SharedArray<T>& a, const SharedArray<T>& b)
    { return a.get() != b.get(); }

#endif // SMART_PTR_CE_H
