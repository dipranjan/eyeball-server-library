// ---------------------------------------------------------------------------
// File:       EUtil.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    
// 
// Change Log:
// ---------------------------------------------------------------------------

#ifndef _EUTIL_H
#define _EUTIL_H

#if 0 //for Linux yamada
#include <windows.h>
#endif

//#include <string>

#include "utypes.h"
#include "Base.h"
#include "tstring.h"

#include <errno.h>

// ---------------------------------------------------------------------------
// Macros
// ---------------------------------------------------------------------------

// Helpers for next macros
#define _ebMakeString(X) #X
#define _ebMakeLine(X) _ebMakeString(X)

// Compile time messages
#define _ebMessage(X) message(__FILE__ "(" _ebMakeLine(__LINE__) ") : " X)
#define _ebWarning(X) _ebMessage("warning : " X)
#define _ebError(X) _ebMessage("error : " X)

// ---------------------------------------------------------------------------
// Defines
// ---------------------------------------------------------------------------
#ifndef max
#define max(a,b)            (((a) > (b)) ? (a) : (b))
#endif

#ifndef min
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif


#define MAX(a,b)        ( (a) > (b) ? (a) : (b) )
#define MIN(a,b)		( (a) > (b) ? (b) : (a) )

#define SAFE_DELETE(X)  { if (X) { delete (X);     (X) = NULL; } }
#define SAFE_RELEASE(X) { if (X) { (X)->Release(); (X) = NULL; } }

#define SAFE_FREE(X)  { if (X) { free(X);     (X) = NULL; } }


#define RELEASE_OBJECT(X) { if(X) { DWORD dwRef = X->Release(); X = NULL;} }
#define DB_REIE(HR, X)    { if (FAILED(HR)) { /*MSG_BOX((X));*/ return E_FAIL;} }
#define REIE(HR)          { if (FAILED(HR)) { return E_FAIL;} }
#define RSIS(HR)          { if (SUCCEEDED(HR)) { return S_OK;} }
#define REIN(PTR)         { if (PTR == NULL) return E_FAIL; }


#define DELETE_ARRAY(X) \
	{ if (X) { delete [] (X);  (X) = NULL; } }

#define DELETE_SINGLE(X) \
	SAFE_DELETE(X)

#if 1//for Linux yamada b�
//Linuxɂ�ANDLE̊TÔ�LOSE_HANDLE(x)͈ȉ̂ꂩɑΉ�v�
#define CLOSE_EVENT(X) { if (X) { BOOL ret = CloseEvent(X); DB_ASSERT(ret); (X) = NULL; } }
#define CLOSE_THREAD(X) { if (X) { BOOL ret = CloseThread(X); DB_ASSERT(ret); (X) = NULL; } }
#define CLOSE_FILE(X) { if (X) { BOOL ret = close(*(int *)X); DB_ASSERT(ret); (X) = NULL; } }

#else
#define CLOSE_HANDLE(X) { if (X) { BOOL ret = CloseHandle(X); DB_ASSERT(ret); (X) = NULL; } }
#endif //for Linux yamada

// SD_BOTH is not defined in Windows CE
#if !defined(SD_BOTH)
#define SD_BOTH 2
#endif

#if 1 //for Linux yamada
#define CLOSE_SOCKET(X) \
	{ \
		if (X != INVALID_SOCKET) \
		{ \
				WARN(shutdown(X, SD_BOTH) == 0); \
				WARN(close(X) == 0); \
				(X) = INVALID_SOCKET; \
		} \
	}
#else
#define CLOSE_SOCKET(X) \
	{ \
		if (X != INVALID_SOCKET) \
		{ \
				WARN(shutdown(X, SD_BOTH) == 0); \
				WARN(closesocket(X) == 0); \
				(X) = INVALID_SOCKET; \
		} \
	}
#endif //for Linux yamada

#define WSA_CLOSE_EVENT(X) \
	{ if (X) { ASSERT(WSACloseEvent(X)); (X) = NULL; } }

#define TCSNCPY(DEST, SOURCE, COUNT) \
	{ _tcsncpy(DEST, SOURCE, (COUNT)-1); (DEST)[(COUNT)-1] = '\0'; }

#define TCSCPY(DEST, SOURCE) \
	TCSNCPY(DEST, SOURCE, sizeof(DEST) / sizeof(DEST[0]))

#define STRNCPY(DEST, SOURCE, COUNT) \
	{ strncpy(DEST, SOURCE, (COUNT)-1); (DEST)[(COUNT)-1] = '\0'; }

#define STRCPY(DEST, SOURCE) \
	STRNCPY(DEST, SOURCE, sizeof(DEST))

#define NULL_TERMINATE(X) \
		X[sizeof(X)-1] = '\0'

#define NULL_STRING(X) \
	    X[0] = '\0'

// Helper macros
#define NEXT_SEQNO(X) \
	(((X) + 1) % 65536)

#define PREV_SEQNO(X) \
	(((X) - 1) % 65536)

int DIFF_SEQNO(u_int16_t x, u_int16_t y);

#define MAX_SEQNO(X, Y) \
	(DIFF_SEQNO((X),(Y))>0? (X):(Y))

#define LT_CHECK(A, B) \
		if ((A) != (B)) \
			return (A) < (B)

// Returns (X / Y) as a percentage 
// - handles div. by zero by returning 0.
// - handles > 100 by returning 100.
#define PERCENT(X, Y) \
	MIN(((Y) ? (100.0 * (X) / (Y)) : 0.0), 100.0)


// Functions

#if 0 //for Linux yamada
DWORD  GetSystemTime(void);
#endif // for Linux

#if 1 //for Linux yamada
#define SetLastError(X) (errno)=X
#define GetLastError() (errno)
#define WSASetLastError(X) (errno)=X
#define WSAGetLastError() (errno)
#endif
typedef void SAFEARRAY;
class TECStrArray
{
public:
    TECStrArray(DWORD size=0);
    virtual ~TECStrArray();

    void Detach(SAFEARRAY** ppsa);
    void PutString(DWORD nindex,const tstring& sValue);
private:
    tstring *m_array;
    DWORD   m_size;
};

#endif // _EUTIL_H
