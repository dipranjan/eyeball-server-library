// ---------------------------------------------------------------------------
// File:       DebugLog.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    log debug information to file
// 
// Change Log:
// ---------------------------------------------------------------------------


#ifndef DEBUG_LOG_H
#define DEBUG_LOG_H

#if 1 //for Linux yamada
#define LOG_FILE(X)
#define LOG_ENABLE(X)
#define LOG_OPEN()
#define LOG_OPEN_APP()
#define LOG_SETFILE(X)
#define LOG(X)
#define DB_AND_LOG(X)
#define LOG_CLOSE()
enum DebugLogItem
{
	LOG_WINDOW_SIZE_ADAPTATION,
	LOG_QUALITY_ADAPTATION,
	LOG_DB_ASSERT,
	LOG_VIDEO_CAPTURE,
	LOG_FIREWALL,
	LOG_AUDIO_RECEIVER,
	LOG_IM_CONNECTION,
	LOG_VS_CONNECTION,
	LOG_HTTP
};

#else
#ifndef _WIN32_WCE
#include <iostream.h>
#include <fstream.h>
#include <strstrea.h>

#include <string>
using std::string;

#endif
enum DebugLogItem
{
	LOG_WINDOW_SIZE_ADAPTATION,
	LOG_QUALITY_ADAPTATION,
	LOG_DB_ASSERT,
	LOG_VIDEO_CAPTURE,
	LOG_FIREWALL,
	LOG_AUDIO_RECEIVER,
	LOG_IM_CONNECTION,
	LOG_VS_CONNECTION,
	LOG_HTTP
};

#define LOG_FILENAME_WSA "C:\\WindowSizeAdaptation.log"
#define LOG_FILENAME_QA  "C:\\QualityAdaptation.log"
#define LOG_FILENAME_QA2 "C:\\QualityAdaptation2.log"
#define LOG_FILENAME_DBA "C:\\DbAssert.log"
#define LOG_FILENAME_VC  "C:\\VideoCapture.log"
#define LOG_FILENAME_BA  "C:\\BroadcastAdaptation.log"
#define LOG_FILENAME_FW  "C:\\Firewall.log"
#define LOG_FILENAME_AR  "C:\\AudioReceiver.log"
#define LOG_FILENAME_IM_CONN  "C:\\IMCtrlConn.log"
#define LOG_FILENAME_VS_CONN  "C:\\VSCtrlConn.log"
#define LOG_FILENAME_MEDIA_CONN  "C:\\EBMediaConn.log"
#define LOG_FILENAME_HTTP  "C:\\EBHttp.log"


bool GetLogEnabled(DebugLogItem item);
void LogAssert(char* expr, char* filename, int line);

#ifndef _WIN32_WCE
class CDebugLog
{
public:
	CDebugLog(char* szName);
	~CDebugLog();

public:
	void SetFile(const char* szName);
	void Enable(bool bEnable);
	void Open(void);
	void OpenApp(void);
	void Close(void);
	void Log(char* szLog);

private:
	string LocalTime(void);

private:
	ostream *m_pLogFile;
	char    m_szFileName[200];
	bool    m_bEnable;
};



#define LOG_FILE(X) \
	static CDebugLog s_debugLog(X);


#define LOG_ENABLE(X)\
	s_debugLog.Enable(X);

#define LOG_OPEN() \
	s_debugLog.Open();

#define LOG_OPEN_APP() \
	s_debugLog.OpenApp();

#define LOG_SETFILE(X) \
	s_debugLog.SetFile(X);

#define LOG(X) \
	{\
	char buf[2048]; \
	ostrstream s(buf, sizeof(buf)); \
	s << X; \
	buf[s.pcount()] = '\0'; \
	s_debugLog.Log(buf);\
	}

#define DB_AND_LOG(X) \
	{ DB(X); LOG(X)}

#define LOG_CLOSE() \
	s_debugLog.Close();

#else // _WIN32_WCE
#define LOG_FILE(X)
#define LOG_ENABLE(X)
#define LOG_OPEN()
#define LOG_OPEN_APP()
#define LOG_SETFILE(X)
#define LOG(X)
#define DB_AND_LOG(X)
#define LOG_CLOSE()
#endif // _WIN32_WCE

#endif //for Linux yamada

#endif //DEBUG_LOG_H
