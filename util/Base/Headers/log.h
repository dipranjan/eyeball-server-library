// $Id: log.h,v 1.1.1.1 2005/12/24 04:03:39 sazzad Exp $

#ifndef AS_LOG_H
#define AS_LOG_H


#include <string>
#include <iostream>
#include <ostream>
using namespace std;

#include <pthread.h>

#ifndef DEBUGLOG

#define LOG_OPEN(X) 
#define LOG(X) 
#define LOGL(Y,X) 
#define LOG_CLOSE()
#define DB_AND_LOG(X) 
#define DB_AND_LOGL(Y,X)
#define DB_AND_LOG_STAT(X)
#define DB_AND_LOGL_STAT(Y,X)
#define LOG_STAT(X) 
#define LOGL_STAT(Y,X)

#else

#define LOG_OPEN(X) \
	Log::open(X)

#define LOG(X) \
	if (Log::log_out) \
	{ \
		*Log::log_out << Log::localtime() << " " << pthread_self() << " " << X << endl; \
	}

#define LOGL(Y,X) \
	if (g_iVerboseLevel >= Y && Log::log_out) \
	{ \
		*Log::log_out << Log::localtime() << " " << pthread_self() << " " << X << endl; \
	}

#define LOG_CLOSE() \
	Log::close()

#define DB_AND_LOG(X) \
	{ DB(X); LOG(X); }

#define DB_AND_LOGL(Y,X) \
	{ DB(X); LOGL(Y,X); }

#define DB_AND_LOG_STAT(X) \
    DB_AND_LOG(#X << " = " << X)

#define DB_AND_LOGL_STAT(Y,X) \
    DB_AND_LOGL(Y,#X << " = " << X)

#define LOG_STAT(X) \
	LOG(#X << " = " << X)

#define LOGL_STAT(Y,X) \
	LOGL(Y,#X << " = " << X)
#endif
class Log
{
public:
	static ostream *log_out;

	static void open(const std::string& _filename);
	static void close(void);

	static std::string generate_filename(const std::string& _name = "output");

	static std::string localtime(void);
};

#endif // AS_LOG_H

