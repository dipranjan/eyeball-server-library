// ---------------------------------------------------------------------------
// File:       sys-time.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    System dependent time includes
// 
// Change Log:
// ---------------------------------------------------------------------------

#ifndef SYS_TIME_H
#define SYS_TIME_H

#ifndef _WIN32_WCE

#include <sys/types.h>

#ifndef WIN32
#include <sys/time.h>
#else
#include <sys/timeb.h>
#endif

#endif // _WIN32_WCE

#endif // SYS_TIME_H
