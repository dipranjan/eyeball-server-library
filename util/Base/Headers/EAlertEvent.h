// ---------------------------------------------------------------------------
// File:       EAlertEvent.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Alert Events constants
// 
// Change Log:
// ---------------------------------------------------------------------------

#ifndef _EC_ALERT_EVENT_H
#define _EC_ALERT_EVENT_H

// Events constants
enum EcEventType
{
    EC_EVENT_NULL,

	EC_EVENT_FIRST,

    EC_EVENT_RECEIVE_TEXT = EC_EVENT_FIRST,
	EC_EVENT_RECEIVE_FILE,
	EC_EVENT_RECEIVE_VIDEO,
    EC_EVENT_VIDEO_CHAT_REQUEST,
	EC_EVENT_USER_ONLINE,
	EC_EVENT_USER_ADD_CONTACT,
	EC_EVENT_USER_ENTER_CHAT,
	EC_EVENT_USER_LEAVE_CHAT,
	EC_EVENT_ASK_DISCONNECT,
	EC_EVENT_EXIT_CHAT,
	EC_EVENT_RECEIVE_EMAIL_AIM,
	EC_EVENT_RECEIVE_EMAIL_YAHOO,
	EC_EVENT_RECEIVE_EMAIL_MSN,

    EC_EVENT_CANCEL_FILE,

    EC_EVENT_LAST
};

enum 
{
	ECEF_SOUND   = 0x00000001,   // Sound notification
	ECEF_ALERT   = 0x00000002,   // Alert Dialog
	ECEF_TBALERT = 0x00000004,   // Alert near Task Bar
	ECEF_HIDE    = 0x00008000    // Alert Not shown in preferences dialog
};

// Events MACROS
#define IS_SOUND_EVENT(dwFlags) (((dwFlags) & ECEF_SOUND) != 0)
#define IS_ALERT_EVENT(dwFlags) (((dwFlags) & ECEF_ALERT) != 0)
#define IS_TBALERT_EVENT(dwFlags) (((dwFlags) & ECEF_TBALERT) != 0)
#define IS_EVENT_VISIBLE(dwFlags) (((dwFlags) & ECEF_HIDE) == 0)


#endif // _EC_ALERT_EVENT_H
