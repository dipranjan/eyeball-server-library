// ---------------------------------------------------------------------------
// File:       ComSafeArray.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Declaration of ComSafeArray
// 
// Change Log:
// ---------------------------------------------------------------------------

#ifndef COM_SAFE_ARRAY_H
#define COM_SAFE_ARRAY_H

#include <comdef.h>

typedef const SAFEARRAY* LPCSAFEARRAY;
typedef const VARIANT* LPCVARIANT;

class CComSafeArray : public tagVARIANT
{
public:
	// Constructors
	CComSafeArray();
	CComSafeArray(const SAFEARRAY& saSrc, VARTYPE vtSrc);
	CComSafeArray(LPCSAFEARRAY pSrc, VARTYPE vtSrc);
	CComSafeArray(const CComSafeArray& saSrc);
	CComSafeArray(const VARIANT& varSrc);
	CComSafeArray(LPCVARIANT pSrc);
	CComSafeArray(VARTYPE vtSrc, DWORD dwElements);
	~CComSafeArray();

	// Operators
	CComSafeArray& operator=(const CComSafeArray& saSrc);
	CComSafeArray& operator=(const VARIANT& varSrc);
	CComSafeArray& operator=(LPCVARIANT pSrc);
	CComSafeArray& operator=(const _variant_t& varSrc);

	BOOL operator==(const SAFEARRAY& saSrc) const;
	BOOL operator==(LPCSAFEARRAY pSrc) const;
	BOOL operator==(const CComSafeArray& saSrc) const;
	BOOL operator==(const VARIANT& varSrc) const;
	BOOL operator==(LPCVARIANT pSrc) const;
	BOOL operator==(const _variant_t& varSrc) const;

	operator LPVARIANT(void);
	operator LPCVARIANT(void) const;

	void Clear();
	void Attach(VARIANT& varSrc);
	void Detach(void);
	void Detach(VARIANT* pvar);
	void Detach(LPSAFEARRAY* ppsa);

	// One dim array helpers
	void CreateOneDim(VARTYPE vtSrc, DWORD dwElements,
		const void* pvSrcData = NULL, long nLBound = 0);
	void ResizeOneDim(DWORD dwElements);

	// Multi dim array helpers
	void Create(VARTYPE vtSrc, DWORD dwDims, DWORD* rgElements);
	DWORD GetSize(DWORD nDim = 1);

	// API wrappers
	void Create(VARTYPE vtSrc, DWORD dwDims, SAFEARRAYBOUND* rgsabounds);
	void AccessData(void** ppvData);
	void UnaccessData(void);
	void AllocData(void);
	void AllocDescriptor(DWORD dwDims);
	void Copy(LPSAFEARRAY* ppsa);
	void GetLBound(DWORD dwDim, long* pLBound);
	void GetUBound(DWORD dwDim, long* pUBound);
	void GetElement(long lIndex, void* pvData);
	void GetElementEx(long* rgIndices, void* pvData);
	void PtrOfIndex(long* rgIndices, void** ppvData);
	void PutElement(long lIndex, void* pvData);
	void PutElementEx(long* rgIndices, void* pvData);
	void Redim(SAFEARRAYBOUND* psaboundNew);
	void Lock(void);
	void Unlock(void);
	DWORD GetDim(void);
	DWORD GetElemSize(void);
	void Destroy(void);
	void DestroyData(void);
	void DestroyDescriptor(void);
};

/////////////////////////////////////////////////////////////////////////////
// Helper for initializing CComSafeArray
void ATLSafeArrayInit(CComSafeArray* psa);

BOOL ATLCompareSafeArrays(SAFEARRAY* parray1, SAFEARRAY* parray2);

#endif // COM_SAFE_ARRAY_H
