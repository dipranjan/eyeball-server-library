/** All files of the DNDS Prototype are Copyright (C) 1999 by Andre Schoorl
 *  unless otherwise noted.
 *
 *  The DNDS Prototype is distributed in the public domain in the hope that it
 *  will be useful, but is provided `as is' and is subject to change without
 *  notice.  No warranty of any kind is made with regard to the software or
 *  documentation.  The author shall not be liable for for any error for
 *  incidental or consequential damages in connection with the software and
 *  the documentation.
 *
 *  Permission to copy the whole, unmodified DNDS package is granted provided
 *  that the copies are not made or distributed for resale (excepting nominal
 *  copying fees).  All redistributions in either source or binary form must
 *  include this disclaimer.
 */

#ifndef AS_THREAD_H
#define AS_THREAD_H

#ifndef WIN32
#include <pthread.h>
#endif

#define EXPORT_KEY BASE_EXPORT_KEY
#include "export.h"


class CLASS_DECL_EXPORT Thread
{
	private:
#ifdef WIN32
		unsigned long m_ThreadId;
#else
#if 1 //for Linux yamada
		pthread_t m_ThreadId;
#else
		pthread_t thread_id;
#endif //for Linux yamada
#endif

	public:
		Thread(void * (*start_routine)(void *), void *arg = NULL);
		~Thread();

#ifdef WIN32
		void set_priority(int priority);
		HANDLE GetHandle(void) { return (HANDLE)m_ThreadId; }
#endif
		void join(void);
};

#endif // AS_THREAD_H

