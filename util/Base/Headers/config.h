/********************************************************************
 * 
 * File: config.h
 * Author: Infranet Solutions Ltd. 1999
 * Copyright: Infranet Solutions Ltd. 1999
 *
 * Purpose: 
 * 
 * Change Log: 
 *
 *******************************************************************/


#ifndef config_h
#define config_h

#if defined(sgi) || defined(__bsdi__) || defined(__FreeBSD__)
#include <sys/types.h>
#elif defined(linux)
#include <sys/bitypes.h>
#else
#ifdef ultrix
#include <sys/types.h>
#endif
/*XXX*/
#ifdef sco
typedef char int8_t;
#else
//typedef signed char int8_t;
#endif
typedef unsigned char u_int8_t;
typedef short int16_t;
typedef unsigned short u_int16_t;
//typedef unsigned int u_int32_t;
typedef int int32_t;
#endif

#if defined(sun) 
#if defined(__cplusplus)
extern "C" {
#endif
//int srandom(int);
//int random(void);
#if defined(__cplusplus)
}
#endif
#endif

#include <stdlib.h>
//#include <time.h>		/* For clock_t */


#ifdef WIN32

#define WIN32_LEAN_AND_MEAN
//#define _WINSOCKAPI_
//#include <winsock2.h>

#define MAXHOSTNAMELEN	256

#define _SYS_NMLN	9
struct utsname {
	char sysname[_SYS_NMLN];
	char nodename[_SYS_NMLN];
	char release[_SYS_NMLN];
	char version[_SYS_NMLN];
	char machine[_SYS_NMLN];
};

typedef char *caddr_t;

struct iovec {
	caddr_t iov_base;
	int	    iov_len;
};

#ifndef TIMEZONE_DEFINED_
#define TIMEZONE_DEFINED_
struct timezone {
	int tz_minuteswest;
	int tz_dsttime;
};
#endif /* ! TIMEZONE_DEFINED_ */

typedef int pid_t;
typedef int uid_t;
typedef int gid_t;
    
#if defined(__cplusplus)
extern "C" {
#endif 

int uname(struct utsname *); 
int getopt(int, char * const *, const char *);
int strcasecmp(const char *, const char *);
int srandom(int);
int random(void);
int gettimeofday(struct timeval *p, struct timezone *z);
int gethostid(void);
int getuid(void);
int getgid(void);
int nice(int);
int sendmsg(int, struct msghdr*, int);

#ifndef _WIN32_WCE
#include <process.h>
#endif
        
#define bzero(dest,count) memset(dest,0,count)
#define bcopy(src,dest,size) memcpy(dest,src,size)
#if defined(__cplusplus)
}
#endif /* WIN32 */

#ifdef sgi
#include <math.h>
#endif

#define ECONNREFUSED	WSAECONNREFUSED
#define ENETUNREACH	WSAENETUNREACH
#define EHOSTUNREACH	WSAEHOSTUNREACH
#define EWOULDBLOCK	WSAEWOULDBLOCK

#define M_PI		3.14159265358979323846

#endif /* WIN32 */

#endif
