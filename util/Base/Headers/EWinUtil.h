// ---------------------------------------------------------------------------
// File:       EWinUtil.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Helper functions specific to windows
//
// Change Log:
// ---------------------------------------------------------------------------

#ifndef _EC_WIN_UTIL_H_
#define _EC_WIN_UTIL_H_

//#include <windows.h>
//#include <string>


// ---------------------------------------------------------------------------
// Required library:
// ---------------------------------------------------------------------------

#ifndef _WIN32_WCE
#ifndef _EC_NO_WINMM
#pragma comment(lib, "winmm.lib")
#endif //_EC_NO_WINMM
#endif

// ---------------------------------------------------------------------------
// Macros
// ---------------------------------------------------------------------------

// For checking standard non-skinned buttons
#define CHECK(X, Y) \
	CheckDlgButton(hDlg, X, Y ? BST_CHECKED : BST_UNCHECKED)

#define IS_CHECKED(X) \
	(BST_CHECKED == IsDlgButtonChecked(hDlg, X))

#define DLG_RETURN(X) \
	{ SetWindowLong(hDlg, DWL_MSGRESULT, X); return TRUE; }

#define ENABLE(X, Y) \
	EnableWindow(GetDlgItem(hDlg, X), Y)

#define CHECK_MENUITEM(X, Y, Z) \
	CheckMenuItem((X), (Y), (Z) ? MF_CHECKED : MF_UNCHECKED)

#define ENABLE_MENUITEM(X, Y, Z) \
	EnableMenuItem((X), (Y), MF_BYCOMMAND | ((Z) ? MF_ENABLED : MF_GRAYED))

#define COPY_STATE(X) \
	ENABLE_MENUITEM(hMenu, (X), ::IsWindowEnabled(GetDlgItem(X)));

#define ENABLE_MENUITEM_POS(X, Y, Z) \
	EnableMenuItem((X), (Y), MF_BYPOSITION | ((Z) ? MF_ENABLED : MF_GRAYED))

#define SEND_COMMAND(X, Y) \
	SendMessage(X, WM_COMMAND, (WPARAM)MAKELONG(Y, BN_CLICKED), 0)

#define SETDEFID(X, Y) \
	SendMessage(X, DM_SETDEFID, (WPARAM)Y, 0)

#define TOGGLE(X, Y) \
	(X).Set##Y(!(X).Get##Y());

#define SET_DIALOG_CALLBACK(IDX, RESOURCE, DIALOGPROC) \
	pHdr->apRes[IDX] = DoLockDlgRes(eyeball::GetAppInstance(), MAKEINTRESOURCE(RESOURCE), RT_DIALOG); \
	pHdr->callbackProc[IDX] = DIALOGPROC

#define DELETE_OBJECT(X) \
	{ if (X) { int ret = ::DeleteObject(X); DB_ASSERT(ret); (X) = NULL; } }

#define DELETE_DC(X) \
	{ if (X) { ASSERT(::DeleteDC(X)); (X) = NULL; } }

#define DESTROY_WINDOW(X) \
	{ if ((X) && (::IsWindow(X))) { int ret = ::DestroyWindow(X); DB_ASSERT(ret); (X) = NULL; } }


#ifdef _WIN32_WCE

// wrapper of ShellExecute for Windows CE
TECInstancePtr ShellExecute(
    TECWindow hwnd, 
    tstring lpOperation,
    tstring lpFile, 
    tstring lpParameters, 
    tstring lpDirectory,
    int nShowCmd
);

#endif


// ---------------------------------------------------------------------------
// Utility functions
// ---------------------------------------------------------------------------

namespace eyeball
{
	// Window data accessors

	LONG GetDialogData(TECWindow hDlg);
	LONG SetDialogData(TECWindow hDlg, LONG lData);  // returns old value
	LONG GetWindowData(TECWindow hWnd);
	LONG SetWindowData(TECWindow hWnd, LONG lData);  // returns old value

	// Dialog item functions

	long GetComboData(TECWindow hDlg, int nID);

	// Writes the list box item's text into sListBoxItemText
	// Returns true on success, false otherwise
	bool GetListBoxItemText(
		TECWindow hwndListBox,            // handle to the list box
		int index,                   // index in list
		tstring& sListBoxItemText);  // output
	LONG GetListBoxItemData(TECWindow hwndList, int index);

	// Get the string from a window, as a string
	BOOL GetWindowText(TECWindow hWnd, tstring& sBuffer, bool _bTrimWhitespace = true);
	BOOL SetWindowText(TECWindow hwnd, const tstring& sText);

	BOOL GetDlgItemText(
		TECWindow hDlg, 
		int nID, 
		tstring& sBuffer, 
		bool _bTrimWhitespace = true);
	BOOL SetDlgItemText(TECWindow hwnd, int nID, const tstring& sText);

	tstring SGetDlgItemText(TECWindow hDlg, int nID);
	tstring SGetWindowText(TECWindow hDlg);

	bool InitSlider(TECWindow hDlg, UINT nID, WORD wMin, WORD wMax, LONG step);
	void SetSliderValue(TECWindow hDlg, UINT nID, LONG val);
	long GetSliderValue(TECWindow hDlg, UINT nID);

	bool InitProgress(TECWindow hDlg, UINT nID, WORD min, WORD max);
	void SetProgressValue(TECWindow hDlg, UINT nID, LONG val);
	long GetProgressValue(TECWindow hDlg, UINT nID);
	void SetProgressColor(TECWindow hDlg, UINT nID, TECColor col);

	// Menu item functions
	bool SetMenuItemText(TECWindowMenu hMenu, UINT nID, const tstring& sNewText);
    bool GetMenuItemText(TECWindowMenu hMenu, UINT nID, tstring& sText);
	bool AddMenuItem(TECWindowMenu hMenu, UINT nID, const tstring& sText);

	BOOL HandlePopupMenu(
		TECInstancePtr hInstance,
		TECWindow hwnd, WORD menu,
		int xPos, int yPos,
		int sysWidth = 0, int sysHeight = 0,
		bool bInWindow = true,
#ifndef _WIN32_WCE
        UINT uFlags = TPM_LEFTALIGN | TPM_TOPALIGN | TPM_RIGHTBUTTON);
#else
        UINT uFlags = TPM_LEFTALIGN | TPM_TOPALIGN);
#endif

	/**
	 * Pops up a context menu and returns the selected string.
	 *
	 * @param hwnd             handle to the window to contain the menu
	 * @param lsStrings        list of strings to display in the menu
	 * @param sSelectedString  string to receive the selected ID
	 *
	 * @return true if an item is selected, false otherwise
	 */
	bool HandleStringPopupMenu(
		TECWindow hWnd,
		const std::list<std::string> lsStrings,
		std::string& sSelectedString,
		int xPos, 
		int yPos, 
#ifndef _WIN32_WCE
		UINT uFlags = TPM_LEFTALIGN | TPM_TOPALIGN | TPM_RIGHTBUTTON);
#else
		UINT uFlags = TPM_LEFTALIGN | TPM_TOPALIGN);
#endif

	/**
	 * Wrapper around TrackPopupMenu API.
	 * Fixes some possible bugs in the API.
	 * Handles paint messages after displaying the menu.
	 *
	 * Parameters similar to TrackPopupMenu API.
	 *
	 * @return UINT
	 */
	UINT ETrackPopupMenu(
		TECWindowMenu hMenu,
		UINT uFlags,
		int x,
		int y,
		TECWindow hWnd);

	// Returns a handle to the popup menu, which must be destroyed
	// by the application before it exits.
	TECWindowMenu InsertCustomSystemMenu(
		TECInstancePtr hInst, 
		const tstring& sTitle, 
		TECWindow hwnd, 
		WORD menu);

	// Tree View functions
	BOOL GetSelectedTreeItemText(TECWindow hwndTV, tstring& sText);
	BOOL GetTreeItemText(TECWindow hwndTV, HANDLE hItem, tstring& sText);
	LONG GetTreeItemData(TECWindow hwndTV, HANDLE hItem);
	BOOL SetTreeItemData(TECWindow hwndTV, HANDLE hItem, LONG lData);
    DWORD GetTreeItemState(TECWindow hwndTV, HANDLE hItem);

	// Other Win32 Utility Functions
	void DispatchPaintMessages(void);
	DWORD CustomWaitForSingleObject(
		HANDLE hHandle, 
		DWORD dwMilliseconds);

	// DoLockDlgRes - loads and locks a dialog template resource. 
	// Returns the address of the locked resource. 
	// lpszResName - name of the resource 
#if 0 /* Sawamura for Linux */
	DLGTEMPLATE * WINAPI DoLockDlgRes(
		TECInstancePtr hInst, 
		tstring lpszResName, 
		tstring lpType);
#endif
	// Shell Functions
	tstring BrowseForFolder(TECWindow hwndParent, const tstring& _sTitle = tstring(""));
	TECInstancePtr GotoURL(const tstring& sUrl, int showcmd);
	TECInstancePtr GotoAlternateURL(tstring url, int showcmd);

	bool IsVisible(TECWindow hwnd);
	
	bool CustomizeClassWndProc(
		tstring lpClassName,
		TECWindowProc& OldWndProc,
		TECWindowProc NewWndProc,
		TECInstancePtr hInstance);

	bool CustomizeWndProc(
		TECWindow hWnd,
		TECWindowProc& OldWndProc,
		TECWindowProc NewWndProc);

	void RemoveTaskbarIcon(TECWindow hWnd);

    // HIWORD is the major version
    // LOWORD is the minor version
	DWORD GetRichEditVersion();

	// workaround for GetTextExtentPoint32 problem with japanese win9x
	BOOL GetTextExtentPoint32U(
		TECDrawContext hdc, 
		tstring lpString,
		int cbString,
		TECSize lpSize);
	BOOL DrawTextU(
		TECDrawContext hdc,
		tstring lpString,
		int nString,
		TECRect lprc,
		UINT uFlags);

	TECWindow FindChildByClass(TECWindow hWndParent, const tstring& sClass);

	tstring GetGuidString(void);
}


// ---------------------------------------------------------------------------
// Helper Classes
// ---------------------------------------------------------------------------

class DlgData
{
public:
	DlgData();
	virtual ~DlgData() {}

	static bool Init(TECWindow hDlg, DlgData* p);  // call in OnInitDialog()
	static bool Destroy(TECWindow hDlg);  // call in OnDestroy()
	
	static DlgData* Ptr(TECWindow hDlg);

	virtual bool OnInit(void) { return true; }
	virtual bool OnDestroy(void) { return true; }

public:
	TECWindow m_hWnd;
};

#endif // _EC_WIN_UTIL_H_

