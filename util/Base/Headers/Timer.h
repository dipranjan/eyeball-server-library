// ---------------------------------------------------------------------------
// File:       Timer.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Wrapper class for multimedia timer
// 
// Change Log:
// ---------------------------------------------------------------------------

#ifndef TIMER_H
#define TIMER_H

#if 1 //for linux yamada
#include <sys/time.h>

#else //for Linux yamada
#define _WINSOCKAPI_
#include <windows.h>
#include <mmsystem.h>

#ifndef _WIN32_WCE
// Required library:
#ifndef _EC_NO_WINMM
#pragma comment(lib, "winmm.lib")
#endif //_EC_NO_WINMM
#endif

#endif //for Linux yamada


// Return void * for consistency with Thread class
typedef void *(*TIMERCALLBACK)(void *);

// Classes

// Timer
//
// Wrapper class for Windows multimedia timer services. Provides
// both periodic and one-shot events. User must supply callback
// for periodic events.
// 

class Mutex;

class Timer
{
	protected:
		Mutex *m_pMutex;

		UINT m_nIDTimer;
		TIMERCALLBACK m_pfnCallback;
		void *m_pUserArg;

		// Type
		UINT m_fuEvent;

		// Internal callback for Windows SDK
#if 1 //for Linux yamada
		static void TimeProc(UINT uID, UINT uMsg, DWORD dwUser, DWORD dw1, DWORD dw2);
#else
		static void CALLBACK TimeProc(UINT uID, UINT uMsg, DWORD dwUser, DWORD dw1, DWORD dw2);
#endif //for Linux yamada

		bool internal_open(TIMERCALLBACK pfnCallback, UINT nDelay, UINT nRes, UINT fuEvent, void *pUserArg);

	public:
	    Timer();
	    ~Timer();

		// times are in milliseconds w/ optional user argument
		bool open_periodic(TIMERCALLBACK pfnCallback, UINT nPeriod, UINT nRes = 0, void *pUserArg = NULL);
		bool open_oneshot(TIMERCALLBACK pfnCallback, UINT nDelay, UINT nRes = 0, void *pUserArg = NULL);

		bool open_periodic(HANDLE hEvent, UINT nPeriod, UINT nRes = 0);
		bool open_oneshot(HANDLE hEvent, UINT nPeriod, UINT nRes = 0);

		bool close(void);
};

#if 0 //for Linux yamada 暫定（多分使わない）
class StopWatch
{
public:
	StopWatch();

	void Reset();
	void Start();
	void Stop();
	
	void PrintCurrent();
	void PrintAverage(int _nInterval = 1);
	void PrintTotal(int _nInterval = 1);

	UINT GetMilliseconds(void);

private:
	LARGE_INTEGER m_liStartTime;
	LARGE_INTEGER m_liStopTime;
	LONGLONG m_llTotalTime;
	DWORD m_nCount;
	LARGE_INTEGER m_liFrequency;
};

/**
 * Simple class to time a single execution of a section of code.
 */
class ProfileSection
{
public:
	ProfileSection()
	{
		m_sw.Start();
	}

	~ProfileSection()
	{
		m_sw.Stop();
		m_sw.PrintTotal(1);
	}

private:
	StopWatch m_sw;
};
#endif //for Linux yamada

#ifdef _WIN32_WCE

/* timer data types */
typedef void (*LPTIMECALLBACK)(UINT uTimerId, UINT uMsg, DWORD dwUser, DWORD dw1, DWORD dw2);

#define TIME_ONESHOT    0x0000   /* program timer for single event */
#define TIME_PERIODIC   0x0001   /* program for continuous periodic event */
#define TIME_CALLBACK_FUNCTION 4
#define TIME_CALLBACK_EVENT_SET 8
#define TIME_CALLBACK_EVENT_PULSE 16
#define TIMERR_NOERROR        (0)                  /* no error */


/* timer function prototypes */

#if 1 //for Linux yamada 
typedef UINT        MMRESULT;   /* error return code, 0 means no error */
#else
//typedef UINT        MMRESULT;   /* error return code, 0 means no error */
#endif //for Linux yamada

MMRESULT timeSetEvent(
	UINT delay,
	UINT resolution,
	LPTIMECALLBACK lpTimeProc, 
	DWORD dwUser,
	UINT fuEvent);

MMRESULT timeKillEvent(UINT uTimerId);

DWORD timeGetTime(void);

void CreateTimerThread();
void CloseTimerThread();


#endif

#endif // TIMER_H
