// ---------------------------------------------------------------------------
// File:       EComError.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    error code definitions for all Eyeball ActiveX controls 
//
// Change Log:
// ---------------------------------------------------------------------------
#ifndef ECOM_ERROR_H
#define ECOM_ERROR_H

#define HRESULT_COMMON(X)			((HRESULT)(0x80040000 + (X)))

// unexpected error
#define E_UNEXPECTED_ERROR			HRESULT_COMMON(0)

// not logged in to the Eyeball server
#define E_NOT_LOGGED_IN				HRESULT_COMMON(1)

// already logged in to the Eyeball server
#define E_ALREADY_LOGGED_IN			HRESULT_COMMON(2)

// the server is not properly set or failed to locate the DNS server
#define E_SERVER_NETWORK			HRESULT_COMMON(3)

// Failed to login to the server. User name or password is not correct
#define E_LOGIN						HRESULT_COMMON(4)

// empty user name
#define E_EMPTY_USER_NAME           HRESULT_COMMON(5)

// invalid user name (length is too long or syntax or user doesn't exist)
#define E_INVALID_USER_NAME			HRESULT_COMMON(6)

// no response
#define E_NO_RESPONSE				HRESULT_COMMON(7)

// empty file name
#define E_EMPTY_FILE_NAME           HRESULT_COMMON(8)

// empty text message
#define E_EMPTY_TEXT_MESSAGE		HRESULT_COMMON(9)

// service is not subscribed
#define E_SERVICE_NOT_SUBSCRIBED	HRESULT_COMMON(10)

// service is not licensed
#define E_SERVICE_NOT_LICENSED		HRESULT_COMMON(11)

// faild to open the destination file. File may be in use
#define E_OPEN_FILE					HRESULT_COMMON(12)

// file doesnot exist
#define E_FILE_NOT_EXIST			HRESULT_COMMON(13)

// the video capture device is not working properly
#define E_VIDEO_CAPTURE_DEVICE		HRESULT_COMMON(14)

// the audio capture device is not working properly
#define E_AUDIO_CAPTURE_DEVICE		HRESULT_COMMON(15)

// The specified user is not online
#define E_USER_NOT_ONLINE			HRESULT_COMMON(16)

// NULL pointer
#define E_NULL_POINTER				HRESULT_COMMON(17)


// ---------------------------------------------------------------------------
// specific error codes for the Eyeball Contact list control
// ---------------------------------------------------------------------------

#define HRESULT_CONTACT_LIST(X)		((HRESULT)(0x80040100 + (X)))

// invalid service name
#define E_IM_SERVICE_NAME			HRESULT_CONTACT_LIST(0)

// invalid file transfer ID
#define E_FILE_TRANSFER_ID			HRESULT_CONTACT_LIST(1)

// invalid profile index
#define E_PROFILE_INDEX				HRESULT_CONTACT_LIST(2)

// invalid profile value
#define E_PROFILE_VALUE				HRESULT_CONTACT_LIST(3)

// unsupported photograph format
#define E_PHOTOGRAPH_FORMAT			HRESULT_CONTACT_LIST(4)

// not logged in to the third party instant messaging service
#define E_IM_NOT_LOGGED_IN			HRESULT_CONTACT_LIST(5)

// The specified user is not in the contact list
#define E_NOT_IN_CONTACT_LIST		HRESULT_CONTACT_LIST(6)

// ---------------------------------------------------------------------------
// specific error codes for the Eyeball Video control
// ---------------------------------------------------------------------------

#define HRESULT_EYEBALL_VIDEO(X)	((HRESULT)(0x80040200 + (X)))

// not in a session
#define E_NOT_IN_SESSION			HRESULT_EYEBALL_VIDEO(0)

// already in the session
#define E_ALREADY_IN_SESSION		HRESULT_EYEBALL_VIDEO(1)

// empty session name
#define E_EMPTY_SESSION_NAME		HRESULT_EYEBALL_VIDEO(2)

// failed to create the session. The session name has been used
#define E_CREATE_SESSION			HRESULT_EYEBALL_VIDEO(3)

// failed to join the session. The session name or password is not corrected
#define E_JOIN_SESSION				HRESULT_EYEBALL_VIDEO(4)

// the specified user is not in the session
#define E_USER_NOT_IN_SESSION		HRESULT_EYEBALL_VIDEO(5)

// not the session host
#define E_NOT_HOST					HRESULT_EYEBALL_VIDEO(6)

// failed to set the microphone volume. DirectX 8.0 is required
#define E_MICROPHONE_VOLUME			HRESULT_EYEBALL_VIDEO(7)

// failed to set the wave volume
#define E_WAVE_VOLUME				HRESULT_EYEBALL_VIDEO(8)

// The required JPEG control for snapshot is not register
#define E_JPEG_CONTROL				HRESULT_EYEBALL_VIDEO(9)

// Failed to create the control window
#define E_CONTROL_WINDOW			HRESULT_EYEBALL_VIDEO(10)


// ---------------------------------------------------------------------------
// specific error codes for the Eyeball Video Message control
// ---------------------------------------------------------------------------

#define HRESULT_VIDEO_MESSAGE(X)	((HRESULT)(0x80040300 + (X)))

// there is no available capture device for recording video messages
#define E_NO_CAPTURE_DEVICE			HRESULT_VIDEO_MESSAGE(0)

// The audio capture format is not supported
#define E_AUDIO_CAPTURE_FORMAT		HRESULT_VIDEO_MESSAGE(1)

// The video capture format is not supported
#define E_VIDEO_CAPTURE_FORMAT		HRESULT_VIDEO_MESSAGE(2)

// DirectX version is too low or not installed properly
#define E_DIRECTX_VERSION			HRESULT_VIDEO_MESSAGE(3)

// The required auido compression filter is not registered
#define E_AUDIO_COMPRESSION_FILTER	HRESULT_VIDEO_MESSAGE(4)

// The required video compression filter is not registered
#define E_VIDEO_COMPRESSION_FILTER	HRESULT_VIDEO_MESSAGE(5)

// Failed to play the video message. DirectX version is too low or
// the required video decompression filter is not registered
#define E_PLAY_VIDEO_MESSAGE		HRESULT_VIDEO_MESSAGE(6)

// Failed to start the graph
#define E_START_GRAPH				HRESULT_VIDEO_MESSAGE(7)

// Failed to stop the graph
#define E_STOP_GRAPH				HRESULT_VIDEO_MESSAGE(8)

// No video message available
#define E_MESSAGE_NOT_READY			HRESULT_VIDEO_MESSAGE(9)


// ---------------------------------------------------------------------------
// specific error codes for the Eyeball Chat Room control
// ---------------------------------------------------------------------------

#define HRESULT_CHAT_ROOM(X)		((HRESULT)(0x80040400 + (X)))

// already entered in a room
#define E_ALREADY_IN_ROOM		HRESULT_CHAT_ROOM(0)

// invalid room name
#define E_ROOM_NAME				HRESULT_CHAT_ROOM(1)

// invalid room typr
#define E_ROOM_TYPE				HRESULT_CHAT_ROOM(2)

// room is full, failed to enter
#define E_ROOM_FULL				HRESULT_CHAT_ROOM(3)

// incorrect room password
#define E_ROOM_PASSWORD			HRESULT_CHAT_ROOM(4)

// not in a room
#define E_NOT_IN_ROOM			HRESULT_CHAT_ROOM(5)

// leave the current room to create a new room
#define E_LEAVE_TO_CREATE_NEW	HRESULT_CHAT_ROOM(6)

// invalid category
#define E_CATEGORY				HRESULT_CHAT_ROOM(7)

// invalid room greetings for create room
#define E_ROOM_GREETINGS		HRESULT_CHAT_ROOM(8)

// invalid room details for create room
#define E_ROOM_DETAILS			HRESULT_CHAT_ROOM(9)
	
// invalid room details for create room
#define E_ROOM_LIMIT			HRESULT_CHAT_ROOM(10)

// ---------------------------------------------------------------------------
// specific error codes for the Eyeball Video Mail control
// ---------------------------------------------------------------------------

#define HRESULT_VIDEO_MAIL(X)		((HRESULT)(0x80040500 + (X)))

// invalid message id 
#define E_MESSAGE_ID				HRESULT_VIDEO_MAIL(0)

// empty recipients field
#define E_EMPTY_RECIPIENTS			HRESULT_VIDEO_MAIL(1)

// unknow param specified for vm info header
#define E_HEADER_PARAM				HRESULT_VIDEO_MAIL(2)

// empty folder name
#define E_FOLDERNAME_EMPTY			HRESULT_VIDEO_MAIL(3)

// "inbox", "sent", "trash" folders
#define E_SYSTEM_FOLDER				HRESULT_VIDEO_MAIL(4)

// mail folder exists
#define E_FOLDER_EXISTS				HRESULT_VIDEO_MAIL(5)

// mail folder not found
#define E_FOLDER_NOT_FOUND			HRESULT_VIDEO_MAIL(6)

// file overwrite failed in server
#define E_OVERWRITE_NOT_ALLOWED		HRESULT_VIDEO_MAIL(7)

// no valid receiver found
#define E_NO_VALID_RECEIVER			HRESULT_VIDEO_MAIL(8)

// file upload/download in progress
#define E_TRANSFER_IN_PROGRESS		HRESULT_VIDEO_MAIL(9)

#define E_NOT_ENOUGH_QUOTA			HRESULT_VIDEO_MAIL(10)

// ---------------------------------------------------------------------------
// specific error codes for the Eyeball Video Window control
// ---------------------------------------------------------------------------

#define HRESULT_VIDEO_WINDOW(X)		((HRESULT)(0x80040600 + (X)))


// ---------------------------------------------------------------------------
// specific error codes for the Eyeball Whiteboard control
// ---------------------------------------------------------------------------

#define HRESULT_WHITE_BOARD(X)		((HRESULT)(0x80040700 + (X)))


// ---------------------------------------------------------------------------
// specific error codes for the Eyeball Internet relay chat control
// ---------------------------------------------------------------------------

#define HRESULT_INTERNET_RELAY_CHAT(X)	((HRESULT)(0x80040800 + (X)))
#define E_NOT_IN_CHANNEL			HRESULT_INTERNET_RELAY_CHAT(0)
#define E_EMPTY_CHANNEL_NAME		HRESULT_INTERNET_RELAY_CHAT(1)
#define E_NO_COMMAND				HRESULT_INTERNET_RELAY_CHAT(2)
#define E_INVALID_COMMAND			HRESULT_INTERNET_RELAY_CHAT(3)
#define E_NOT_ENOUGH_PARAMS			HRESULT_INTERNET_RELAY_CHAT(4)
#define E_CHANNEL_NOT_EXIST			HRESULT_INTERNET_RELAY_CHAT(5)

// methods
bool GetCommonErrorDesc(HRESULT hr, tstring& sErrorDesc);
bool GetContactListErrorDesc(HRESULT hr, tstring& sErrorDesc);
bool GetEyeballVideoErrorDesc(HRESULT hr, tstring& sErrorDesc);
bool GetVideoMessageErrorDesc(HRESULT hr, tstring& sErrorDesc);
bool GetChatRoomErrorDesc(HRESULT hr, tstring& sErrorDesc);
bool GetVideoMailErrorDesc(HRESULT hr, tstring& sErrorDesc);
bool GetWhiteboardErrorDesc(HRESULT hr, tstring& sErrorDesc);
bool GetVideoWindowErrorDesc(HRESULT hr, tstring& sErrorDesc);
bool GetInternetRelayChatErrorDesc(HRESULT hr, tstring& sErrorDesc);

#endif // ECOM_ERROR_H

