// ---------------------------------------------------------------------------
// File:       critsec.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Implements Critical Sections
//
// Change Log:
// ---------------------------------------------------------------------------

#ifndef EC_CRITSEC_H
#define EC_CRITSEC_H

#ifdef WIN32
#include <windows.h>
#else
#include <pthread.h>
#endif

#define EXPORT_KEY BASE_EXPORT_KEY
#include "export.h"


class Critical_Section;

class CLASS_DECL_EXPORT Mutex
{
#if 1 // for Linux yamada
	friend class Critical_Section;
#else
	friend Critical_Section;
#endif // for Linux yamada
	private:
#ifdef WIN32
		CRITICAL_SECTION mut;
#else
		pthread_mutex_t mut;

		#ifndef __linux__
			// holder and lockCount are protected by mutex.
			// Only test or change if you're holding!
			pthread_t holder;
			int lockCount;
		#endif
#endif

		void reset(void);
		void close(void);

	public:
		Mutex() { reset(); }
		~Mutex() { close(); }
};

class CLASS_DECL_EXPORT Critical_Section
{
	private:
		Mutex *pm;

#ifdef WIN32
#elif defined(__linux__)
		struct _pthread_cleanup_buffer _buffer;
#else
		_cleanup_t _cleanup_info;
#endif

		void cleanup_push(void);
		void cleanup_pop(void);

	public:
		Critical_Section(Mutex& _mutex);
		~Critical_Section();

#ifndef WIN32
		int cond_wait(pthread_cond_t *c);
#endif
};

#endif // EC_CRITSEC_H

