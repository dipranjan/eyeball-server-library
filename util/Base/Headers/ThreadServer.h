// ---------------------------------------------------------------------------
// File:       ThreadServer.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Abstract base class for "server" objects with an internal 
//             thread.
//
// ---------------------------------------------------------------------------

#ifndef _THREAD_SERVER_H_
#define _THREAD_SERVER_H_

#include "Event.h"

typedef Event<u_int16_t> InternalThreadEvent;

/**
 * The ThreadServer class contains a thread object.
 * By deriving off ThreadServer, the sub-class avoids having to deal directly
 * with the thread object.  In addition, support is provided for block-and-wait
 * inter-thread communication.
 *
 * USAGE
 *
 * 1. Include the header file and derive off ThreadServer.
 * 2. Implement InternalThreadImpl(), the object's thread.
 * 3. The Internal thread should handle the event object returned by
 *    m_pThreadEvent->GetHandle(), by calling m_pThreadEvent->GetVar().
 *    This outputs a number which identifies the type of event.  One possible
 *    thread event ID is TSTE_INTERNAL_QUIT, which indicates the 
 *    InternalThreadImpl() function should return.
 * 4. Call StartInternalThread/StopInternalThread as required.
 */
class ThreadServer : noncopyable
{
public:
	ThreadServer();
	virtual ~ThreadServer();

protected:
	static void* ThreadProc(void *arg);

	/**
	 * Internal thread implementation - defined by subclasses.
	 */
	virtual void InternalThreadImpl(void) = 0;

	/**
	 * Initializes the internal thread object and executes the thread.
	 *
	 * @note the function blocks waiting for the thread to start.
	 */
	virtual void StartInternalThread(void);

	/**
	 * Terminates the internal thread (by setting the internal quit event) 
	 * and deletes the internal thread object.
	 *
	 * @note the function blocks waiting for the thread to exit.
	 */
	virtual void StopInternalThread(void);

	/**
	 * This sets an internal thread event and waits for the response.
	 *
	 * @return response value, defined by sub-class
	 */
	virtual bool SetEventAndWait(u_int16_t uEvent);

protected:
	SharedPtr<class Thread> m_pThread;
	SharedPtr<InternalThreadEvent> m_pThreadEvent;

	HANDLE m_hResponseEvent;
	bool m_bResponseValue;
	DWORD m_dwLastError;
};


// ---------------------------------------------------------------------------
// Thread event IDs
// ---------------------------------------------------------------------------

// Thread should exit in response to this event.  This event is set by the
// StopInternalThread() function.
#define TSTE_INTERNAL_QUIT   (1)

// Thread should call its internal open function in response to this event.
#define TSTE_INTERNAL_OPEN   (2)

// Thread should call its internal close function in response to this event.
#define TSTE_INTERNAL_CLOSE  (3)

// Note: additional events can be defined by the subclasses.

#endif // _THREAD_SERVER_H_
