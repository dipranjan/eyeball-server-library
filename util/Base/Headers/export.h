// Intentionally do not use conventional #ifndef macros to ensure this file is always included

// NOTE: EXPORT_KEY should be defined to another preprocessor constant describing the project's
//       intended compilation:
//
//       2              - DLL export
//       1              - DLL import
//
//       Not defined    - Conventional static linking

#undef CLASS_DECL_EXPORT

#ifdef WIN32
	#if EXPORT_KEY == 2
		#define CLASS_DECL_EXPORT __declspec(dllexport)
	#elif EXPORT_KEY == 1
		#define CLASS_DECL_EXPORT __declspec(dllimport)
	#else
		#define CLASS_DECL_EXPORT
	#endif
#else
	#define CLASS_DECL_EXPORT
#endif

// Clear entry to avoid preprocessor pollution
#undef EXPORT_KEY
