//
// Written by Bjarke Viksoe (bjarke@viksoe.dk)
// Copyright (c) 2001 Bjarke Viksoe.
//
// This code may be used in compiled form in any way you desire. This
// file may be redistributed by any means PROVIDING it is 
// not sold for profit without the authors written consent, and 
// providing that this notice and the authors name is included. 
//
// This file is provided "as is" with no expressed or implied warranty.
// The author accepts no liability if it causes any damage to you or your
// computer whatsoever. It's free, so don't hassle me about it.
//
// Beware of bugs.

#ifndef __ATLWFILE_H__
#define __ATLWFILE_H__

#ifndef __cplusplus
   #error ATL requires C++ compilation (use a .cpp suffix)
#endif

#ifndef __ATLBASE_H__
	#error atlwfile.h requires atlbase.h to be included first
#endif

#ifndef INVALID_SET_FILE_POINTER
#define INVALID_SET_FILE_POINTER ((DWORD)-1)
#endif

namespace ATLW
{

// Win32 File wrapper class
// Important: Don't make the destructor "virtual" because we need
//            the class v-table to look like the HANDLE type!

class CFile
{
public:
   HANDLE m_hFile;
public:
   CFile() 
   {
      m_hFile = INVALID_HANDLE_VALUE;
   };
   ~CFile()
   { 
      Close(); 
   };
   operator HFILE() const { return (HFILE)m_hFile; };
   operator HANDLE() const { return m_hFile; };
   BOOL Open(LPCTSTR pstrFileName, 
             DWORD dwAccess=GENERIC_READ, 
             DWORD dwShareMode=FILE_SHARE_READ, 
             DWORD dwFlags=OPEN_EXISTING,
             DWORD dwAttributes=FILE_ATTRIBUTE_NORMAL)
   {
#ifndef _WIN32_WCE
      ATLASSERT(!::IsBadStringPtr(pstrFileName,-1));
#endif
      Close();
      // Attempt file creation
      HANDLE hFile = ::CreateFile(pstrFileName, 
         dwAccess, 
         dwShareMode, 
         NULL,
         dwFlags, 
         dwAttributes, 
         NULL);
      if( hFile == INVALID_HANDLE_VALUE ) return FALSE;
      m_hFile = hFile;
      return TRUE;
   };
   BOOL Create(LPCTSTR pstrFileName,
               DWORD dwAccess=GENERIC_WRITE, 
               DWORD dwShareMode=0 /*DENY ALL*/, 
               DWORD dwFlags=CREATE_ALWAYS,
               DWORD dwAttributes=FILE_ATTRIBUTE_NORMAL)
   {
      return Open(pstrFileName, dwAccess, dwShareMode, dwFlags, dwAttributes);
   };
   void Close()
   {
      if( m_hFile==INVALID_HANDLE_VALUE ) return;
      ::CloseHandle(m_hFile);
      m_hFile = INVALID_HANDLE_VALUE;
   };
   BOOL IsOpen() const
   {
      return m_hFile!=INVALID_HANDLE_VALUE;
   }
   void Attach(HANDLE hHandle)
   {
      Close();
      m_hFile = hHandle;
   }   
   HANDLE Detach()
   {
      HANDLE h = m_hFile;
      m_hFile = INVALID_HANDLE_VALUE;
      return h;
   }
   BOOL Read(LPVOID lpBuf, DWORD nCount)
   {
      ATLASSERT(m_hFile!=INVALID_HANDLE_VALUE);
      ATLASSERT(lpBuf!=NULL);
      ATLASSERT(!::IsBadWritePtr(lpBuf, nCount));
      if( nCount==0 ) return TRUE;   // avoid Win32 "null-read"
      DWORD dwRead;
      if( !::ReadFile(m_hFile, lpBuf, nCount, &dwRead, NULL) ) return FALSE;
      // Win32s will not return an error all the time (usually DISK_FULL)
      return nCount==dwRead;
   }
   BOOL Read(LPVOID lpBuf, DWORD nCount, LPDWORD pdwRead)
   {
      ATLASSERT(m_hFile!=INVALID_HANDLE_VALUE);
      ATLASSERT(lpBuf);
      ATLASSERT(!::IsBadWritePtr(lpBuf, nCount));
      ATLASSERT(pdwRead);
      *pdwRead = 0;
      if( nCount==0 ) return TRUE;   // avoid Win32 "null-read"
      if( !::ReadFile(m_hFile, lpBuf, nCount, pdwRead, NULL) ) return FALSE;
      // Win32s will not return an error all the time (usually DISK_FULL)
      return nCount==*pdwRead;
   }
   BOOL Write(LPCVOID lpBuf, DWORD nCount)
   {
      ATLASSERT(m_hFile!=INVALID_HANDLE_VALUE);
      ATLASSERT(lpBuf!=NULL);
      ATLASSERT(!::IsBadReadPtr(lpBuf, nCount));   
      if( nCount==0 ) return TRUE; // avoid Win32 "null-write" option
      DWORD dwWritten;
      if( !::WriteFile(m_hFile, lpBuf, nCount, &dwWritten, NULL) ) return FALSE;
      // Win32s will not return an error all the time (usually DISK_FULL)
      return nCount==dwWritten;
   }
   BOOL Write(LPCVOID lpBuf, DWORD nCount, LPDWORD pdwWritten)
   {
      ATLASSERT(m_hFile!=INVALID_HANDLE_VALUE);
      ATLASSERT(lpBuf);
      ATLASSERT(!::IsBadReadPtr(lpBuf, nCount));
      ATLASSERT(pdwWritten);    
      *pdwWritten = 0;
      if( nCount==0 ) return TRUE; // avoid Win32 "null-write" option
      if( !::WriteFile(m_hFile, lpBuf, nCount, pdwWritten, NULL) ) return FALSE;
      // Win32s will not return an error all the time (usually DISK_FULL)
      return nCount==*pdwWritten;
   }
   DWORD Seek(LONG lOff, UINT nFrom)
   {
      ATLASSERT(m_hFile!=INVALID_HANDLE_VALUE);
      DWORD dwNew = ::SetFilePointer(m_hFile, lOff, NULL, (DWORD)nFrom);
      if( dwNew==INVALID_SET_FILE_POINTER ) return (DWORD)-1;
      return dwNew;
   }
   DWORD GetPosition() const
   {
      ATLASSERT(m_hFile!=INVALID_HANDLE_VALUE);
      DWORD dwPos = ::SetFilePointer(m_hFile, 0, NULL, FILE_CURRENT);
      if( dwPos == INVALID_SET_FILE_POINTER ) return (DWORD)-1;
      return dwPos;
   }
   BOOL Flush()
   {
      ATLASSERT(m_hFile!=INVALID_HANDLE_VALUE);
      if( m_hFile==INVALID_HANDLE_VALUE ) return FALSE;
      if( ::FlushFileBuffers(m_hFile)==0 ) return FALSE;
      return TRUE;
   }
   DWORD GetSize() const
   {
      ATLASSERT(m_hFile!=INVALID_HANDLE_VALUE);
      return ::GetFileSize(m_hFile, NULL);
   }
   DWORD GetType() const
   {
#ifndef _WIN32_WCE
      ATLASSERT(m_hFile!=INVALID_HANDLE_VALUE);
      return ::GetFileType(m_hFile);
#else
	  DB_ASSERT(0 && "Method not supported under Windows CE.");
	  return 0; // unknown type
#endif
   }
   BOOL GetFileTime(FILETIME *ftCreate, FILETIME *ftAccess, FILETIME *ftModified)
   {
      ATLASSERT(m_hFile!=INVALID_HANDLE_VALUE);
      return ::GetFileTime(m_hFile, ftCreate, ftAccess, ftModified);
   }
   static BOOL FileExists(LPCTSTR pstrFileName)
   {
#ifndef _WIN32_WCE
      ATLASSERT(!::IsBadStringPtr(pstrFileName, MAX_PATH));
#endif
      return ::GetFileAttributes(pstrFileName)!=0xFFFFFFFF;
   };
   static BOOL Delete(LPCTSTR pstrFileName)
   {
#ifndef _WIN32_WCE
      ATLASSERT(!::IsBadStringPtr(pstrFileName, MAX_PATH));
#endif
      return ::DeleteFile(pstrFileName);
   };
   static BOOL Rename(LPCTSTR pstrSourceFileName, LPCTSTR pstrTargetFileName)
   {
#ifndef _WIN32_WCE
      ATLASSERT(!::IsBadStringPtr(pstrSourceFileName, MAX_PATH));
      ATLASSERT(!::IsBadStringPtr(pstrTargetFileName, MAX_PATH));
#endif
      return ::MoveFile(pstrSourceFileName, pstrTargetFileName);
   };
};

class CTemporaryFile : public CFile
{
public:
   TCHAR m_szFileName[MAX_PATH];
public:
   ~CTemporaryFile()
   { 
      Close();
      Delete(m_szFileName);
   };
   BOOL Create(LPTSTR pstrFileName, UINT cchFilename,
               DWORD dwAccess=GENERIC_WRITE, 
               DWORD dwShareMode=0 /*DENY ALL*/, 
               DWORD dwFlags=CREATE_ALWAYS,
               DWORD dwAttributes=FILE_ATTRIBUTE_NORMAL)
   {
#ifndef _WIN32_WCE
      ATLASSERT(!::IsBadStringPtr(pstrFileName, cchFilename));
#endif
      if( cchFilename>0 ) {
         ::GetTempPath(cchFilename, pstrFileName);
         ::GetTempFileName(pstrFileName, _T("BV"), 0, pstrFileName);
      }
      ::lstrcpy(m_szFileName, pstrFileName);
      return Open(pstrFileName, dwAccess, dwShareMode, dwFlags, dwAttributes);
   };
};


}; //namespace ATL


#endif // __ATLWFILE_H___
