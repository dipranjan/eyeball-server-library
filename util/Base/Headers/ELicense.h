// ---------------------------------------------------------------------------
// File:       ELicense.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Declaration of ELicense - Licensing Functions
//
// Change Log:
// ---------------------------------------------------------------------------

#ifndef _ELICENSE_H_
#define _ELICENSE_H_

#include <atlbase.h>


// ---------------------------------------------------------------------------
// Constants and Definitions
// ---------------------------------------------------------------------------

// The design time license is valid if LICENSE_FILE is located in the
// module directory, and its text matches LICENSE_KEY.  A valid design time
// license allows creation of a runtime license (LPK file)
#define LICENSE_KEY "License Key, Copyright (c) 2001 Eyeball Networks Inc."
#define LICENSE_FILE "eyeball.lic"

/**
 * Eyeball Chat COM/ActiveX control license functions.
 */
class ELicense
{
protected:
	static BOOL IsLicenseValid(void);
	static BOOL GetLicenseKey(DWORD dwReserved, BSTR* pbstr);
	static BOOL VerifyLicenseKey(BSTR bstrKey);

public:
	/**
	 * Replacement for CoCreateInstance that supports licensing.
	 *
	 * @param rclsid        class identifier (CLSID) of the object
	 * @param pUnkOuter     pointer to controlling IUnknown
	 * @param dwClsContext  context for running executable code
	 * @param riid          reference to the identifier of the interface
	 * @param ppv           address of output variable that receives 
     *                      the interface pointer requested in riid
	 *
	 * @return HRESULT
	 */
	static HRESULT CoCreateInstanceLic(
		REFCLSID rclsid,
		LPUNKNOWN pUnkOuter, 
		DWORD dwClsContext,
		REFIID riid,
		LPVOID* ppv)
	{
		if (NULL == ppv)
			return E_POINTER;
		
		CComPtr<IClassFactory2> spClassFactory2;
		
		HRESULT hr = CoGetClassObject(
			rclsid, 
			dwClsContext, 
			NULL,
			IID_IClassFactory2, 
			(void**)&spClassFactory2);
		
		if (FAILED(hr))
			return E_FAIL;
		
		if (!spClassFactory2)
			return E_FAIL;
		
		CComBSTR bstrLicense(LICENSE_KEY);
		
		hr = spClassFactory2->CreateInstanceLic(
			pUnkOuter, 
			NULL,
			riid, 
			bstrLicense, 
			ppv);
		
		if (FAILED(hr))
			return E_FAIL;
		
		if (NULL == *ppv)
			return E_FAIL;
		
		return S_OK;
	}

};

#endif // _ELICENSE_H_
