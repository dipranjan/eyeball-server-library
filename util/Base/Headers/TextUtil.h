// File: TextUtil.h

/** All files of the DNDS Prototype are Copyright (C) 1999 by Andre Schoorl
 *  unless otherwise noted.
 *
 *  The DNDS Prototype is distributed in the public domain in the hope that it
 *  will be useful, but is provided `as is' and is subject to change without
 *  notice.  No warranty of any kind is made with regard to the software or
 *  documentation.  The author shall not be liable for for any error for
 *  incidental or consequential damages in connection with the software and
 *  the documentation.
 *
 *  Permission to copy the whole, unmodified DNDS package is granted provided
 *  that the copies are not made or distributed for resale (excepting nominal
 *  copying fees).  All redistributions in either source or binary form must
 *  include this disclaimer.
 */

#ifndef EC_TEXTUTIL_H
#define EC_TEXTUTIL_H

#include "tstring.h"

std::string tolower(std::string _str);
std::string toupper(std::string _str);

struct lt_lowerstring
{
	bool operator()(const std::string& s1, const std::string& s2) const;
};

bool compare_i(const std::string& _s1, const std::string& _s2);
tstring trim_whitespace(const tstring& _str);
std::string escape_quotes(const std::string& _str);

std::string itoa(const int iX);

/**
 * Encodes string objects.
 * This function should be compatible with the JScript unescape function.
 * The set of unescaped symbols may be slightly different.
 *
 * @param str  input string to be encoded
 *
 * @return encoded string
 */
std::string escape(const std::string& str);

/**
 * Encodes string objects to be compatible with Windows file system.
 * This function should be compatible with the JScript unescape function.
 * The set of unescaped symbols may be slightly different.
 *
 * @param str  input string to be encoded
 *
 * @return encoded string
 */
tstring escape_filename(const tstring& str);

/**
 * Decodes string objects encoded with the escape function.
 * This function should be compatible with the JScript escape function.
 *
 * @param str  input string to be decoded
 *
 * @return decoded string
 */
std::string unescape(const std::string& str);

/**
 * Replaces text in a string.
 *
 *@param sBuf     buffer containing the string to be searched
 *@param sBefore  replace one instance of this string
 *@param sAfter   replace with this string
 *
 *@return true if found and replaced, else false
 */
bool FindAndReplace(tstring& sBuf, const tstring& sBefore, const tstring& sAfter);

class StringTokenizer
{
	private:
		std::string str;
		std::string::size_type pos;

	public:
		StringTokenizer();
		StringTokenizer(const std::string& _str);

		void setString(const std::string& _str);
		bool nextToken(std::string& _token);
};

/////////////////////////////////////////////////////////////////////////////
// HTML functions

tstring EscapeHtmlText(const tstring& sTextIn);
tstring UnescapeHtmlText(const tstring& sTextIn);
bool IsHtmlTag(const tstring& sHtmlTag);
tstring Html2Text(const tstring& sTextIn);

std::string ToByteArray(const tstring& sIn);
tstring FromByteArray(const std::string& sIn);

// UCN functions
std::string EncodeUcn(const tstring& sPlainText);
tstring DecodeUcn(const std::string& sUcnText);

// Message Box functions

// Define a function pointer type
typedef void (APPLICATION_SPECIFIC_MSG_BOX)(const tstring& _sText);

extern APPLICATION_SPECIFIC_MSG_BOX *g_pCustomMsgBox;
extern APPLICATION_SPECIFIC_MSG_BOX *g_pCustomBlockingMsgBox;
 
#define MSG_BOX(X) \
	(*g_pCustomMsgBox)(X)

#define BLOCKING_MSG_BOX(X) \
	(*g_pCustomBlockingMsgBox)(X)

void WindowsMessageBox(const tstring& _sText);

#endif // EC_TEXTUTIL_H
