// ---------------------------------------------------------------------------
// File:       hashfun.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Common hash functions
//
// Change Log:
// ---------------------------------------------------------------------------

#ifndef EC_HASHFUN_H
#define EC_HASHFUN_H

#include <string>
namespace __gnu_cxx{
template<>
struct hash<std::string>
{
	size_t operator()(const std::string& v) const
		{ hash<const char *> h; return h(v.c_str()); }
};
}
#endif //EC_HASHFUN_H

