// ---------------------------------------------------------------------------
// File:       utypes.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    
// 
// Change Log:
// ---------------------------------------------------------------------------

#ifndef UTYPES_H
#define UTYPES_H

typedef unsigned char   u_int8_t;
typedef          short  int16_t;
typedef unsigned short  u_int16_t;
typedef unsigned int    u_int32_t;
typedef          int    int32_t;

#if 1 //for Linux yamada
typedef void* HANDLE; ///////�����u�I
typedef int SOCKET;
typedef unsigned int UINT;
typedef unsigned short WORD;
typedef long LONG;
typedef unsigned int DWORD;
typedef unsigned char BYTE;
typedef bool BOOL;
#define TRUE  1
#define FALSE 0

#endif //for Linux yamada


#endif // UTYPES_H
