/********************************************************************
 * 
 * File: StreamType.h
 * Author: Eyeball Networks Inc.
 * Copyright: Eyeball Networks Inc. 2001
 *
 * Purpose: Provides the StreamType enum type
 * 
 * Change Log: 
 *
 *******************************************************************/


#ifndef EYECONTACT_STREAM_TYPE_H
#define EYECONTACT_STREAM_TYPE_H


enum StreamType 
{
	ST_VIDEO_RECEIVER,	
	ST_VIDEO_SENDER,		
	ST_AUDIO_RECEIVER,
	ST_AUDIO_SENDER,
	ST_ALL_STREAMS
};

/**
 * StreamStatistics
 */
struct StreamStatistics
{
	StreamStatistics() {}
	virtual ~StreamStatistics() {}
};


#endif //EYECONTACT_STREAM_TYPE_H
