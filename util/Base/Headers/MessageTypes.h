// ---------------------------------------------------------------------------
// File:       MessageTypes.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Base class for network messages
//
// Change Log:
// ---------------------------------------------------------------------------

#ifndef EC_MESSAGE_TYPES_H
#define EC_MESSAGE_TYPES_H


// ---------------------------------------------------------------------------
// Definitions
// ---------------------------------------------------------------------------

// Minimum version required for certain messages
#define MIN_VER_PORT_SETTINGS	7


// ---------------------------------------------------------------------------
// Message Types
// ---------------------------------------------------------------------------

enum VersionnedMessageType
{
	VERSION_ERROR = 0,

	P2ECS_REQUEST_MESSAGE = 16,
	ECS_TEXT_MESSAGE,
	ECS_FILE_TRANSFER_MESSAGE,
	P2ECS_COMMAND_MESSAGE,
	ECS_GROUP_CHAT_INFO_MESSAGE,
	ECS_ECHO_ADDRESS_MESSAGE,
	ECS_USERLIST_MESSAGE,
	ECS_PROFILE_MESSAGE,
	ECS_XML_USERLIST_MESSAGE,
	ECS_XML_MESSAGE,

	ECS2P_RESPONSE_MESSAGE = 32,
	ECS2P_RESPONSE_MESSAGE_EX,
	ECS2P_CONTACT_MESSAGE,
	ECS2P_EYEBALL_LIST_MESSAGE,

	P2P_STARTUP_MESSAGE = 64,
	P2P_QOS_MESSAGE,
	P2P_ADAPTATION_MESSAGE,
	P2P_KEEPALIVE_MESSAGE,
	P2P_PORT_SETTINGS_MESSAGE,

	P2P_EPCHAT_MESSAGE,
	P2P_EPTEXT_MESSAGE,
	P2P_EPADAPTATION_MESSAGE,
	// P2P expando message
	P2P_CONFERENCE_MESSAGE,

	P2P_FILE_TRANSFER_MESSAGE = 80,
	P2P_DATA_MESSAGE,

	// multi-party messges between peer and group chat server
	MP_CONTROL_MESSAGE = 96,
	MP_QOS_MESSAGE,
	MP_TEXT_MESSAGE,

	// Video Messaging Server
	VM_TRANSMIT_MESSAGE = 128,
	VM_EXT_TRANSMIT_MESSAGE,	

	//message type for chatroom server
	CR_STATUS_MESSAGE = 144,
	CRC2S_CONNECT_MESSAGE,
	CRC2S_REQUEST_MESSAGE,
	CRS2C_RESPONSE_MESSAGE,
	CRC2S_ENTER_ROOM_MESSAGE,
	CRS2C_USER_LIST_MESSAGE,
	CRS2C_ROOM_LIST_MESSAGE,
	CRS2C_CATEGORY_LIST_MESSAGE,
	CR_TEXT_MESSAGE,
	CRS2C_USERINFO_MESSAGE,
	CRC2S_CREATE_ROOM_MESSAGE,
    CRC2S_ENTER_ROOM_DIRECT_MESSAGE,
	CRC2S_SET_USER_DETAIL_MESSAGE,
	CRS2C_AD_URL_MESSAGE,
	CRC2S_CONNECT_MESSAGE_EX,
	
	// message for Im
	IM_BASE_MESSAGE = 160,
	IM_INFO_MESSAGE,
	IM_TEXT_MESSAGE,
	IM_CONTACT_MESSAGE,
	IM_VIDEO_MESSAGE,

	PROXY_MESSAGE = 176,
	EXPANDO_MESSAGE,

	ECHO_MESSAGE = 196,

	// broadcast message
	BROADCAST_MESSAGE = 200,
	WHITEBOARD_MESSAGE,
	CONTACT_LIST_MESSAGE,
	MEDIA_STREAM_MESSAGE = 205,
	COLLABORATION_DATA_MESSAGE,
};

#endif // EC_MESSAGE_TYPES_H

