// ---------------------------------------------------------------------------
// File:       CodecPlugin.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Class definitions for CodecPlugin.
//			   This is used to load/unload module and to access interface.
// 
// Change Log:
// ---------------------------------------------------------------------------

#ifndef CODEC_PLUGIN_H
#define CODEC_PLUGIN_H

//#ifdef WIN32
//#define _WINSOCKAPI_
//#include <winsock2.h>
//#endif

//#include "tstring.h"

#include "CodecInterface.h"


#define EXPORT_KEY BASE_EXPORT_KEY
#include "export.h"

using std::string;

/**
 * CodecPlugin is used to load/unload a module and access its interface.
 */
class CLASS_DECL_EXPORT CodecPlugin 
{
public:
	/**
	 * Constructor.
	 *
	 * @param codecType          codec type (see enum CodecType)
	 * @param iInterfaceVersion  version of combined interface
	 * @param iMajorVersion      version of plug-in, for incompatible changes
	 * @param iMinorVersion      version of plug-in, for compatible changes
	 */
	CodecPlugin(
		CodecType codecType, 
		int iInterfaceVersion, 
		int iMajorVersion, 
		int iMinorVersion = 0);
	~CodecPlugin();

	/**
	 * Retrieves the codec interface.
	 *
	 * @see CodecInterface
	 *
	 * @return pointer to codec interface
	 */
	CodecInterface* GetInterface(void) { return m_pCodecInterface; }

	/**
	 * Loads a module using the given payload type.
	 * All DLL's in the current directory are loaded, until one is found
	 * that supports the given payload type.
	 *
	 * @param chPayloadType  codec payload type
	 *
	 * @return TRUE on success, FALSE on failure
	 */
	BOOL Load(char chPayloadType);

	/** 
	* Loads a well-known file using the given payload type
	*
	* @param chPayloadType codec payload type
	*
	* @return TRUE on success, FALSE on failure
	*/
	BOOL Preload(char chPayloadType);

	/**
	 * Loads a module using the given filename.
	 *
	 * @return TRUE on success, FALSE on failure
	 */
	BOOL Load(const string& sFilename);

	/**
	 * Unloads the currently loaded codec.
	 *
	 * @return TRUE on success, FALSE on failure
	 */
	BOOL Unload(void);

	/**
	 * Creates a new encoder object, using the loaded module.
	 *
	 * @return pointer to new encoder
	 */
	BaseEncoder *NewEncoder(void);

	/**
	 * Creates a new decoder object, using the loaded module.
	 *
	 * @return pointer to new decoder
	 */
	BaseDecoder *NewDecoder(void);

	/**
	 * Sets the instance handle used to find the module directory.
	 *
	 * @param hInst  module instance handle
	 */
	static void SetModuleInstance(void* hInst);

	/**
	 * Retrieves the instance handle used to find the module directory.
	 *
	 * @return module instance handle
	 */
	static void* GetModuleInstance(void);

private:
	CodecType  m_CodecType;
	char 		m_payloadType;
	int        m_iInterfaceVersion;
	int        m_iMajorVersion;
	int        m_iMinorVersion;

	string m_sFilename;
	void*  m_hLibrary;
	CodecInterface *m_pCodecInterface;

	static void* ms_hModuleInstance;
};

#endif // CODEC_PLUGIN_H
