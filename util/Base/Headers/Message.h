// ---------------------------------------------------------------------------
// File:       Message.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Base class for network messages.  Be very careful here
//             since alignment and padding are platform dependent!
//
// Change Log:
// ---------------------------------------------------------------------------

#ifndef EC_MESSAGE_H
#define EC_MESSAGE_H

//#include <string>

#ifndef _WIN32_WCE
#include <iostream.h>
#endif

#include "utypes.h"
#include "Serializable.h"


// ---------------------------------------------------------------------------
// Defines
// ---------------------------------------------------------------------------

#define EYEBALL_CHAT_LE_09      4
#define EYEBALL_CHAT_LE_10      6
#define EYEBALL_CHAT_LE_11      7
#define EYEBALL_CHAT_LE_12      8   
#define EYEBALL_CHAT_20         9
#define EYEBALL_CHAT_22         10
#define EYEBALL_CHAT_23			11

#define EYEBALL_GROUPCHAT_01    128 
#define EYEBALL_GROUPCHAT_02    129 
 

#define EYEBALL_VIDEOMESSAGE_01 196	  

#define EYEBALL_CHATROOM				152
#define EYEBALL_CHATROOM_UNICODE		153

#define EYEBALL_IM              160

#ifdef _WIN32_WCE
#define PRO_VERSION
#endif

#ifdef PRO_VERSION
#define MESSAGE_VERSION         EYEBALL_CHAT_23
#else
#define MESSAGE_VERSION         EYEBALL_CHAT_22
#endif

// Note: use CM_LENGTH instead of sizeof(CommandMessage), as sizeof takes
// into account the v-table.
#define CM_LENGTH					8		// bytes

#define CM_GET_VERSION(BUF)			((u_int8_t *)BUF)[0]
#define CM_GET_MESSAGE_TYPE(BUF)	((u_int8_t *)BUF)[1]
#define CM_GET_PAYLOAD_LENGTH(BUF)	ntohs(((u_int16_t *)BUF)[1])



// ---------------------------------------------------------------------------
// Misc Helpers
// ---------------------------------------------------------------------------

class CommandMessage : public ISerializable
{
public:
	CommandMessage();
	CommandMessage(u_int8_t u8MessageType);

	// Convert to/from network byte order
	virtual void serialize(std::string& sBuf) const;
	virtual int  deserialize(const std::string& sBuf);

	virtual u_int16_t payload_length(void) const { return 0; }
	virtual unsigned int size(void) const;

#ifndef _WIN32_WCE
	friend ostream& operator<<(ostream& s, const CommandMessage& v);
#endif

	static u_int8_t MessageType(const std::string& sBuf)
	{
		if (sBuf.size() < CM_LENGTH)
			return 0;

		return sBuf[1];
	}

	u_int8_t  m_u8Version;
	u_int8_t  m_u8MessageType;
	u_int16_t m_u16PayloadLength;	// only used in deserialize

	u_int16_t m_u16Command;
	u_int16_t m_u16Value;
};


class ForwardMessage
{
public:
	ForwardMessage():
	  m_sUserName(),
	  m_sMessage()
	  {}

	~ForwardMessage()
	{
	  m_sUserName.clear();
	  m_sMessage.clear();
	}


	std::string m_sUserName;
	std::string m_sMessage;
};


#endif // EC_MESSAGE_H
