// ---------------------------------------------------------------------------
// File:       ReturnCodes.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Defines the return codes used by EyeballMedia objects
// 
// Change Log:
// ---------------------------------------------------------------------------

#ifndef RETURN_CODES_H
#define RETURN_CODES_H

#include "debug.h"


// ---------------------------------------------------------------------------
// Error codes
// ---------------------------------------------------------------------------

#define ES_SUCCESS			0
#define ES_FAILURE			-1

#define DECODER_ERROR		-1001
#define DECODER_SUCCESS		-1002
#define DECODER_PARTIAL		-1003

#define ES_NOT_CONNECTED	-1004
#define ES_CANNOT_CONNECT   -1005
#define ES_LOST_CONNECTION  -1006

#define ES_AGAIN			-1007

// ---------------------------------------------------------------------------
// Booleans
// ---------------------------------------------------------------------------

#define RET_IF(S, R) \
	{ \
		if (S) \
		{ \
			DB(__FILE__ << ":" << __LINE__ << ": `" << #S << "' failed."); \
			return R; \
		} \
	}

#define RFI(S) RET_IF(S, FALSE)
#define RTI(S) RET_IF(S, TRUE)
#define RNI(S) RET_IF(S, NULL)
#define RZI(S) RET_IF(S, 0)

// FALSE
#define RFIF(X) RFI((X) == FALSE)
#define RFIT(X) RFI((X) == TRUE)
#define RFIN(X) RFI((X) == NULL)
#define RFIZ(X) RFI((X) == 0)

#define RFUF(X) RFI((X) != FALSE)
#define RFUT(X) RFI((X) != TRUE)
#define RFUN(X) RFI((X) != NULL)
#define RFUZ(X) RFI((X) != 0)

// TRUE
#define RTIF(X) RTI((X) == FALSE)
#define RTIT(X) RTI((X) == TRUE)
#define RTIN(X) RTI((X) == NULL)
#define RTIZ(X) RTI((X) == 0)

#define RTUF(X) RTI((X) != FALSE)
#define RTUT(X) RTI((X) != TRUE)
#define RTUN(X) RTI((X) != NULL)
#define RTUZ(X) RTI((X) != 0)

// NULL
#define RNIF(X) RNI((X) == FALSE)
#define RNIT(X) RNI((X) == TRUE)
#define RNIN(X) RNI((X) == NULL)
#define RNIZ(X) RNI((X) == 0)

#define RNUF(X) RNI((X) != FALSE)
#define RNUT(X) RNI((X) != TRUE)
#define RNUN(X) RNI((X) != NULL)
#define RNUZ(X) RNI((X) != 0)

// 0
#define RZIF(X) RZI((X) == FALSE)
#define RZIT(X) RZI((X) == TRUE)
#define RZIN(X) RZI((X) == NULL)
#define RZIZ(X) RZI((X) == 0)

#define RZUF(X) RZI((X) != FALSE)
#define RZUT(X) RZI((X) != TRUE)
#define RZUN(X) RZI((X) != NULL)
#define RZUZ(X) RZI((X) != 0)

#endif //RETURN_CODES_H
