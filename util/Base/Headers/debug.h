/** All files of the DNDS Prototype are Copyright (C) 1999 by Andre Schoorl
 *  unless otherwise noted.
 *
 *  The DNDS Prototype is distributed in the public domain in the hope that it
 *  will be useful, but is provided `as is' and is subject to change without
 *  notice.  No warranty of any kind is made with regard to the software or
 *  documentation.  The author shall not be liable for for any error for
 *  incidental or consequential damages in connection with the software and
 *  the documentation.
 *
 *  Permission to copy the whole, unmodified DNDS package is granted provided
 *  that the copies are not made or distributed for resale (excepting nominal
 *  copying fees).  All redistributions in either source or binary form must
 *  include this disclaimer.
 */

#ifndef AS_DEBUG_H
#define AS_DEBUG_H

/* -------------------------------------------------------------------------
   Define DEBUG as a compiler flag in all source files to enable debugging
   messages.  For example:

      gcc -DDEBUG -c debug.cc

   Provided macros are:

      ASSERT             - assert replacement with debug support
      STREAM             - used for writing to ostream with debug support
      WARN               - similar to ASSERT but does not exit
      DB                 - used for printing messages to cerr
      DB_ASSERT          - same as ASSERT, but only included in DEBUG mode
      DB_ENTER           - call this when entering a function
      DB_REDIRECT        - redirect debug output to file and/or cerr
      DB_SET_EXIT_DEPTH  - override exit level for subsequent calls
      DB_SET_PRINT_DEPTH - override print level for subsequent calls
      DB_STAT            - useful for printing values of variables/functions
      DB_STREAM          - same as STREAM, but only included in DEBUG mode
      DB_TIME            - print out the current date and time accurate
                           to the hundredths of a second
      DB_WARN            - similar to DB_ASSERT but does not exit

   Typical usage might be something like:

      #include "debug.h"

      void main(void)
      {
         int i = 22;

         DB_ENTER(main);
         DB_SET_PRINT_DEPTH(2);
         DB_SET_EXIT_DEPTH(10);

         DB("Hello world, i = " << i);
      }

   If DEBUG is not defined, default is no debugging and macros with DB_
   prefix are expanded to nothing, meaning no code is included.  Macros
   without this prefix such as ASSERT expand to code in both cases, but
   will not include debugging support unless DEBUG is defined.
   ------------------------------------------------------------------------- */

#ifdef _DEBUG
	#define DEBUG
#endif //_DEBUG

// Temporarily disable NDEBUG to make sure we get a definition for assert()
#ifdef WIN32
	#ifdef NDEBUG
		#define NDEBUG_BACKUP NDEBUG
	#endif

	#undef NDEBUG
#endif //WIN32

#include <assert.h>

// Restore NDEBUG to its previous setting
#ifdef WIN32
	#ifdef NDEBUG_BACKUP
		#define NDEBUG NDEBUG_BACKUP
		#undef NDEBUG_BACKUP
	#endif
#endif

#ifndef DEBUG

	#ifndef ASSERT
		#define ASSERT(X) \
			assert(X)
	#endif

	#define STREAM(S, X) \
		S << X

	#define WARN(X) \
		(X)

	// Debug mode off, so disable all DB macros
	#define DB(X)
	#define DB1(X)
	#define DB2(X)
	#define DB3(X)
	#define DB4(X)
	#define DB_ASSERT(X)
	#define DB_DELAY()
	#define DB_ENTER(X)
	#define DB_REDIRECT(X, F)
	#define DB_SET_EXIT_DEPTH(X)
	#define DB_SET_PRINT_DEPTH(X)
	#define DB_STAT(X)
	#define DB_STREAM(S, X)
	#define DB_TIME()
	#define DB_WARN(X)

#else // DEBUG

	#ifndef __cplusplus

		// Provide minimal support for C programs
		#define ASSERT(X) \
			assert(X)

		#define DB_ASSERT(X) \
			ASSERT(X)

		#ifdef WIN32
			#define DB(X) \
				OutputDebugString(X)
		#else
			#define DB(X) \
				fprintf(stderr, X);
			//#define DB4(X) \
			//	fprintf(stderr, X);
			#define DB1(X) \
				fprintf(stderr, X);
			#define DB2(X) \
				fprintf(stderr, X);
			#define DB3(X) \
				fprintf(stderr, X);
		#endif

	#else // __cplusplus

		// Compile time options
		#define DEBUG_THREADS
		//#define DEBUG_TIME_ACCURACY

		#ifdef WIN32
			#include <strstrea.h>
			#include <process.h>

			#define getpid() \
				"0x" << hex << GetCurrentProcessId()

			#define pthread_self() \
				"0x" << hex << GetCurrentThreadId()
		#else
			#include <iostream>
			#include <cstdio>					
			#include <ostream>
			#include <cstdlib>
			#include <climits>			
			#include <stdint.h>
			using namespace std;			

			//p #include <unistd.h>			// getpid()
		#endif

		#ifdef DEBUG_THREADS
			#ifdef __linux__
			#include <bits/sigset.h>
			#endif

		#include "critsec.h"
		#endif

		#ifdef WIN32
			#ifdef DLL_VERSION
				#define DllImport __declspec(dllimport)
				#define DllExport __declspec(dllexport)
			#else
				#define DllImport
				#define DllExport
			#endif

			#ifdef SVVIDEO_EXPORTS
				#define CLASS_DECL_DEBUG DllExport
			#else
				#define CLASS_DECL_DEBUG DllImport
			#endif
		#else
			#define CLASS_DECL_DEBUG
		#endif

#include <string>

class CLASS_DECL_DEBUG Debug
{
private:
	char *func_name;
	int prev_exit_depth, prev_print_depth;

public:
	static int exit_depth;
	static int indent_depth;
	static int print_depth;

	static bool cerr_enable;
	static ostream *os_ptr;

#ifdef DEBUG_THREADS
	static Mutex mutex;
#endif

	Debug(char *_func_name);
	~Debug();

	static void Assert(char *expr, char *filename, int line);
	static void delay(int num_seconds = 5);
	static void indent(void);
	static void preline(void);
	static void print_time(void);
	static void redirect(const char *_filename = NULL, bool _cerr_enable = true);
	static void set_exit_depth(int _exit_depth);
	static void set_print_depth(int _print_depth);
	static void warn(char *expr, char *filename, int line);

	static std::string localtime(void);
};

// Private helper macros

#ifdef WIN32
	static char db_stream_buf[2048];

	#define DB_STREAM_PRINT(S, X) \
		{ \
			ostrstream s(db_stream_buf, sizeof(db_stream_buf)); \
			//s <<"\t\t" << X; \
			s << X; \
			db_stream_buf[s.pcount()] = '\0'; \
			OutputDebugString(db_stream_buf); \
		}
#else
	#define DB_STREAM_PRINT(S, X) \
		S << X
#endif

#ifdef DEBUG_THREADS
	#define DB_ACQUIRE_CRITSEC() \
		Critical_Section critsec(Debug::mutex)
	#define DB_STREAM_PRINT_THREAD(S) \
		DB_STREAM_PRINT(S, "," << pthread_self())
#else
	#define DB_ACQUIRE_CRITSEC()
	#define DB_STREAM_PRINT_THREAD(S)
#endif

#define DB_PRINT(X) \
	{ \
		DB_ACQUIRE_CRITSEC(); \
		if (Debug::cerr_enable) \
			DB_STREAM_PRINT(cerr, X); \
		if (Debug::os_ptr) \
			DB_STREAM_PRINT(*Debug::os_ptr, X); \
	}

// Removed for now since server is single-threaded, time is more useful
//DB_STREAM_PRINT(S, getpid());
//DB_STREAM_PRINT_THREAD(S);

#define DB_STREAM_INDENT(S) \
	{ \
		DB_STREAM_PRINT(S, Debug::localtime()); \
		DB_STREAM_PRINT(S, " "); \
		for (int i=0; i < Debug::indent_depth; i++) \
		{ \
			DB_STREAM_PRINT(S, " "); \
		} \
	}

#define DB_INDENT() \
	Debug::indent()

// Macros defined in both debug and normal modes

#ifndef ASSERT
#define ASSERT(X) \
	if ((X) == 0) \
		Debug::Assert(#X, __FILE__, __LINE__)
#endif

#define STREAM(S, X) \
	{ \
		DB_STREAM_INDENT(S); \
		DB_STREAM_PRINT(S, X); \
	}

#define WARN(X) \
	if ((X) == 0) \
		Debug::warn(#X, __FILE__, __LINE__)

// Macros defined only if debugging is enabled
#define DB4(X) \
	if (Debug::print_depth > 0) \
	{ \
		DB_INDENT(); \
		DB_PRINT("   4++  " << X << endl); \
	}
#define DB(X) \
	if (Debug::print_depth > 0) \
	{ \
		DB_INDENT(); \
		DB_PRINT("   ++  " << X << endl); \
	}
#ifndef _TRACE_LEVEL_3
#define DB3(X)
#else
#define _TRACE_LEVEL_2
#define _TRACE_LEVEL_1
#define DB3(X) \
	if (Debug::print_depth > 0) \
	{ \
		DB_INDENT(); \
		DB_PRINT("   3++  " << X << endl); \
	}
#endif
#ifndef _TRACE_LEVEL_2
#define DB2(X)
#else
#define _TRACE_LEVEL_1
#define DB2(X) \
	if (Debug::print_depth > 0) \
	{ \
		DB_INDENT(); \
		DB_PRINT("   2++  " << X << endl); \
	}
#endif
#ifndef _TRACE_LEVEL_1
#define DB1(X)
#else
#define DB1(X) \
	if (Debug::print_depth > 0) \
	{ \
		DB_INDENT(); \
		DB_PRINT("   1++  " << X << endl); \
	}
#endif

#define DB_ASSERT(X) \
	ASSERT(X)

#define DB_DELAY() \
	Debug::delay();

#define DB_ENTER(X) \
	Debug debug(#X)

#define DB_REDIRECT(X, F) \
	Debug::redirect(X, F)

#define DB_SET_EXIT_DEPTH(X) \
	Debug::set_exit_depth(X)

#define DB_SET_PRINT_DEPTH(X) \
	Debug::set_print_depth(X)

#define DB_STAT(X) \
	DB(#X << " = " << (X))

#define DB_STREAM(S, X) \
	STREAM(S, X)

#define DB_TIME() \
	Debug::print_time()

#define DB_WARN(X) \
	WARN(X)

#endif // __cplusplus

#endif // DEBUG

#endif // AS_DEBUG_H

