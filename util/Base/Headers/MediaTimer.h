/********************************************************************
 * 
 * File: MediaTimer.h
 * Author: Infranet Solutions Ltd. 1999
 * Copyright: Infranet Solutions Ltd. 1999
 *
 * Purpose: ?
 * 
 * Change Log: 
 *
 *******************************************************************/

#ifndef media_timer_h
#define media_timer_h

#include "config.h"
#include "sys-time.h"
#include "utypes.h"

#define EXPORT_KEY BASE_EXPORT_KEY
#include "export.h"


class CLASS_DECL_EXPORT MediaTimer {
    public:
	MediaTimer();
	virtual ~MediaTimer();
	static inline MediaTimer* instance() { return (instance_); }
	virtual u_int32_t media_ts();
	virtual u_int32_t ref_ts();
	inline u_int32_t offset() const { return (offset_); }
    private:
	static MediaTimer* instance_;
    protected:
	u_int32_t offset_;	/* random offset */
};

#endif
