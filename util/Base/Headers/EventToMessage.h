#ifndef EVENT_TO_MESSAGE_H
#define EVENT_TO_MESSAGE_H

#include "ThreadServer.h"

// This is an adapter class, which handles windows events by posting messages 
// to the specified window.
class EventToMessage : public ThreadServer
{
public:
	EventToMessage()
	{
		m_NetEvent.SetHandle(CreateEvent(NULL, FALSE, FALSE, NULL));
		SetNotifyInfo(NULL, 0, 0);
		
		StartInternalThread();
	}

	virtual ~EventToMessage()
	{
		StopInternalThread();
	}

	void SetNotifyInfo(HWND hWnd, UINT uMsg, WPARAM wParam = 0)
	{
		m_hWnd = hWnd;
		m_uMsg = uMsg;
		m_wParam = wParam;
	}

	virtual void InternalThreadImpl(void)
	{
		HANDLE hEventArray[2];
		hEventArray[0] = m_NetEvent.GetHandle();
		hEventArray[1] = m_pThreadEvent->GetHandle();

		DWORD dwWait = 0;

		while (true)
		{
			dwWait = WaitForMultipleObjects(2, hEventArray, FALSE, INFINITE);
			switch (dwWait)
			{
			case WAIT_OBJECT_0:  // net event
				if (::IsWindow(m_hWnd))
				{
					PostMessage(m_hWnd, m_uMsg, m_wParam, 0);
				}
				break;

			default:
				return;
			}
		}
	}

	HANDLE GetEventHandle(void)
	{
		return m_NetEvent.GetHandle();
	}

private:
	volatile HWND m_hWnd;
	volatile UINT m_uMsg;
	volatile WPARAM m_wParam;
	SimpleEvent m_NetEvent;
};

#endif //EVENT_TO_MESSAGE_H