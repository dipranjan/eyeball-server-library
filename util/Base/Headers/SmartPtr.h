//  Boost smart_ptr.hpp header file  -----------------------------------------//

//  (C) Copyright Greg Colvin and Beman Dawes 1998, 1999. Permission to copy,
//  use, modify, sell and distribute this software is granted provided this
//  copyright notice appears in all copies. This software is provided "as is"
//  without express or implied warranty, and with no claim as to its
//  suitability for any purpose.

//  See http://www.boost.org for most recent version including documentation.

//  Revision History
//  24 Jul 00  Change throw() to // never throws.  See lib guidelines
//             Exception-specification rationale. (Beman Dawes)
//  22 Jun 00  Remove #if continuations to fix GCC 2.95.2 problem (Beman Dawes)
//   1 Feb 00  Additional SharedPtr BOOST_NO_MEMBER_TEMPLATES workarounds
//             (Dave Abrahams)
//  31 Dec 99  Condition tightened for no member template friend workaround
//             (Dave Abrahams)
//  30 Dec 99  Moved BOOST_NMEMBER_TEMPLATES compatibility code to config.hpp
//             (Dave Abrahams)
//  30 Nov 99  added operator ==, operator !=, and std::swap and std::less
//             specializations for shared types (Darin Adler)
//  11 Oct 99  replaced op[](int) with op[](std::size_t) (Ed Brey, Valentin
//             Bonnard), added SharedPtr workaround for no member template
//             friends (Matthew Langston)
//  25 Sep 99  added SharedPtr::swap and SharedArray::swap (Luis Coelho).
//  20 Jul 99  changed name to smart_ptr.hpp, #include <boost/config.hpp>,
//             #include <boost/utility.hpp> and use noncopyable
//  17 May 99  remove ScopedArray and SharedArray operator*() as
//             unnecessary (Beman Dawes)
//  14 May 99  reorder code so no effects when bad_alloc thrown (Abrahams/Dawes)
//  13 May 99  remove certain throw() specifiers to avoid generated try/catch
//             code cost (Beman Dawes)
//  11 May 99  get() added, conversion to T* placed in macro guard (Valentin
//             Bonnard, Dave Abrahams, and others argued for elimination
//             of the automatic conversion)
//  28 Apr 99  #include <memory> fix (Valentin Bonnard)
//  28 Apr 99  rename transfer() to share() for clarity (Dave Abrahams)
//  28 Apr 99  remove unsafe SharedArray template conversions(Valentin Bonnard)
//  28 Apr 99  p(r) changed to p(r.px) for clarity (Dave Abrahams)
//  21 Apr 99  reset() self assignment fix (Valentin Bonnard)
//  21 Apr 99  dispose() provided to improve clarity (Valentin Bonnard)
//  27 Apr 99  leak when new throws fixes (Dave Abrahams)
//  21 Oct 98  initial Version (Greg Colvin/Beman Dawes)

#ifndef BOOST_SMART_PTR_HPP
#define BOOST_SMART_PTR_HPP

//#include <boost/config.hpp>   // for broken compiler workarounds
#ifndef _WIN32_WCE
#include <cstddef>            // for std::size_t
#endif
#include <memory>             // for std::auto_ptr
#include <algorithm>          // for std::swap 
//#include <boost/utility.hpp>  // for noncopyable
#include <functional>         // for std::less

#ifndef _WIN32_WCE
#define SP_TRY try
#define SP_CATCH(X) catch(X)
#define SP_THROW    throw
#else
#define SP_TRY if(true)
#define SP_CATCH(X) if(false)
#define SP_THROW
#endif

#if 0 //for Linux yamada
#define USE_INTERLOCKED
#endif //for Linux yamada

//  Boost config.hpp configuration header file  ------------------------------//

//  (C) Copyright Boost.org 1999. Permission to copy, use, modify, sell and
//  distribute this software is granted provided this copyright notice appears
//  in all copies. This software is provided "as is" without express or implied
//  warranty, and with no claim as to its suitability for any purpose.

//  See http://www.boost.org for most recent version.

//  Boost config.hpp policy and rationale documentation has been moved to
//  http://www.boost.org/libs/config

//  Revision History (excluding minor changes for specific compilers)
//    4 Oct 00  BOOST_NO_STD_MIN_MAX (Jeremy Siek)
//   29 Sep 00  BOOST_NO_INTEGRAL_INT64_T (Jens Maurer)
//   25 Sep 00  BOOST_NO_STD_ALLOCATOR (Jeremy Siek)
//   18 SEP 00  BOOST_NO_SLIST, BOOST_NO_HASH, 
//              BOOST_NO_TEMPLATED_ITERATOR_CONSTRUCTORS
//              BOOST_NO_LIMITS (Jeremy Siek)
//    1 Sep 00  BOOST_NO_PRIVATE_IN_AGGREGATE added. (Mark Rodgers)
//   23 Jul 00  Fixed spelling of BOOST_NO_INCLASS_MEMBER_INITIALIZATION in
//              comment (Dave Abrahams). 
//   10 Jul 00  BOOST_NO_POINTER_TO_MEMBER_CONST added (Mark Rodgers)
//   26 Jun 00  BOOST_NO_STD_ITERATOR, BOOST_MSVC_STD_ITERATOR,
//              BOOST_NO_STD_ITERATOR_TRAITS, BOOST_NO_USING_TEMPLATE,
//              added (Jeremy Siek)
//   20 Jun 00  BOOST_MSVC added (Aleksey Gurtovoy)
//   14 Jun 00  BOOST_NO_DEPENDENT_TYPES_IN_TEMPLATE_VALUE_PARAMETERS (Jens M.)
//   22 Mar 00  BOOST_MSVC6_MEMBER_TEMPLATES added (Dave Abrahams)
//   18 Feb 00  BOOST_NO_INCLASS_MEMBER_INITIALIZATION added (Jens Maurer)
//   26 Jan 00  Borland compiler support added (John Maddock)
//   26 Jan 00  Sun compiler support added (J�rg Schaible)
//   30 Dec 99  BOOST_NMEMBER_TEMPLATES compatibility moved here from
//              smart_ptr.hpp. (Dave Abrahams)
//   15 Nov 99  BOOST_NO_OPERATORS_IN_NAMESPACE,
//              BOOST_NO_TEMPLATE_PARTIAL_SPECIALIZATION added (Beman Dawes)
//   11 Oct 99  BOOST_NO_STDC_NAMESPACE refined; <cstddef> supplied
//   29 Sep 99  BOOST_NO_STDC_NAMESPACE added (Ed Brey)
//   24 Sep 99  BOOST_DECL added (Ed Brey)
//   10 Aug 99  Endedness flags added, GNU CC support added
//   22 Jul 99  Initial version
 

#ifndef BOOST_CONFIG_HPP
#define BOOST_CONFIG_HPP

//  Conformance Flag Macros  -------------------------------------------------//
//
//  Conformance flag macros should identify the absence of C++ Standard 
//  conformance rather than its presence.  This ensures that standard conforming
//  compilers do not require a lot of configuration flag macros.  It places the
//  burden where it should be, on non-conforming compilers.  In the future,
//  hopefully, less rather than more conformance flags will have to be defined.

//  BOOST_NO_DEPENDENT_TYPES_IN_TEMPLATE_VALUE_PARAMETERS: Template value
//  parameters cannot have a dependent type, for example
//  "template<class T, typename T::type value> class X { ... };"

//  BOOST_NO_INCLASS_MEMBER_INITIALIZATION: Compiler violates std::9.4.2/4. 

//  BOOST_NO_INTEGRAL_INT64_T: int64_t as defined by <boost/cstdint.hpp> is
//  not an integral type.

//  BOOST_NO_MEMBER_TEMPLATES: Member template functions not fully supported.
//  Also see BOOST_MSVC6_MEMBER_TEMPLATES in the Compiler Control section below.

//  BOOST_NO_MEMBER_TEMPLATE_FRIENDS: Member template friend syntax
//  ("template<class P> friend class frd;") described in the C++ Standard,
//  14.5.3, not supported.

//  BOOST_NO_OPERATORS_IN_NAMESPACE: Compiler requires inherited operator
//  friend functions to be defined at namespace scope, then using'ed to boost.
//  Probably GCC specific.  See boost/operators.hpp for example.

//  BOOST_NO_POINTER_TO_MEMBER_CONST: The compiler does not correctly handle
//  pointers to const member functions, preventing use of these in overloaded 
//  function templates.  See boost/functional.hpp for example.

//  BOOST_NO_PRIVATE_IN_AGGREGATE: The compiler misreads 8.5.1, treating classes
//  as non-aggregate if they contain private or protected member functions. 

//  BOOST_NO_STD_ITERATOR: The C++ implementation fails to provide the
//  std::iterator class.

//  BOOST_NO_STD_ITERATOR_TRAITS: The compiler does not provide a standard
//  compliant implementation of std::iterator_traits. Note that
//  the compiler may still have a non-standard implementation.

//  BOOST_NO_STDC_NAMESPACE: The contents of C++ standard headers for C library
//  functions (the <c...> headers) have not been placed in namespace std.
//  Because the use of std::size_t is so common, a specific workaround for
//  <cstddef> (and thus std::size_t) is provided in this header (see below).
//  For other <c...> headers, a workaround must be provided in the boost header:
//
//    #include <cstdlib>  // for abs
//    #ifdef BOOST_NO_STDC_NAMESPACE
//      namespace std { using ::abs; }
//    #endif

//  BOOST_NO_TEMPLATE_PARTIAL_SPECIALIZATION. Class template partial
//  specialization (14.5.4 [temp.class.spec]) not supported.

//  BOOST_NO_USING_TEMPLATE: The compiler will not accept a using declaration
//  that imports a template from the global namespace into a named namespace.
//  Probably Borland specific.

//  Compiler Control or Information Macros  ----------------------------------//
//
//  Compilers often supply features outside of the C++ Standard which need to be
//  controlled or detected. As usual, reasonable default behavior should occur
//  if any of these macros are not defined.

//  BOOST_DECL:  Certain compilers for Microsoft operating systems require
//  non-standard class and function decoration if dynamic load library linking
//  is desired.  BOOST_DECL supplies that decoration, defaulting to a nul string
//  so that it is harmless when not required.  Boost does not encourage the use
//  of BOOST_DECL - it is non-standard and to be avoided if practical to do so.

//  BOOST_DECL_EXPORTS:  User defined, BOOST_DECL_EXPORTS causes BOOST_DECL to
//  be defined as __declspec(dllexport) rather than __declspec(dllimport).

//  BOOST_MSVC6_MEMBER_TEMPLATES:  Microsoft Visual C++ 6.0 has enough member
//  template idiosyncrasies (being polite) that BOOST_NO_MEMBER_TEMPLATES is
//  defined for this compiler. BOOST_MSVC6_MEMBER_TEMPLATES is defined to allow
//  compiler specific workarounds.

//  BOOST_MSVC: defined as _MSC_VER for the Microsoft compiler only. In general,
//  boost headers should test for a specific conformance flag macro (for
//  example, BOOST_NO_MEMBER_TEMPLATE_FRIENDS) rather than a specific compiler.
//  VC++ is a special case, however, since many libraries try to support it yet
//  it has so many conformance issues that sometimes it is just easier to test
//  for it directly. On the other hand, the obvious way to do this doesn't work,
//  as many non-Microsoft compilers define _MSC_VER.  Thus BOOST_MSVC.

//  BOOST_MSVC_STD_ITERATOR: Microsoft's broken version of std::iterator
//  is being used. 

//  BOOST_SYSTEM_HAS_STDINT_H: There are no 1998 C++ Standard headers <stdint.h> 
//  or <cstdint>, although the 1999 C Standard does include <stdint.h>. 
//  If <stdint.h> is present, <boost/stdint.h> can make good use of it,
//  so a flag is supplied (signalling presence; thus the default is not
//  present, conforming to the current C++ standard).

//  BOOST_NO_SLIST: The C++ implementation does not provide the slist class.

//  BOOST_NO_HASH: The C++ implementation does not provide the hash_set
//  or hash_map classes.

//  BOOST_STD_EXTENSION_NAMESPACE: The name of the namespace in which the slist,
//  hash_set and/or hash_map templates are defined in this implementation (if any).

//  BOOST_NO_TEMPLATED_ITERATOR_CONSTRUCTORS: The standard library does not provide
//  templated iterator constructors for its containers.

//  BOOST_NO_LIMITS: The C++ implementation does not provide the <limits> header.

//  BOOST_NO_INTRINSIC_WCHAR_T: The C++ implementation does not provide wchar_t,
//  or it is really a synonym for another integral type. Use this symbol to
//  decide whether it is appropriate to explicitly specialize a template on
//  wchar_t if there is already a specialization for other integer types.

//  BOOST_NO_STD_ALLOCATOR: The C++ standard library does not provide
//  a standards conforming std::allocator.

//  BOOST_NO_STD_MIN_MAX: The C++ standard library does not provide
//  the min() and max() template functions that should be in <algorithm>.

//  Compilers are listed in alphabetic order (except VC++ last - see below)---//

//  GNU CC (also known as GCC and G++)  --------------------------------------//

# if defined __GNUC__
#   if __GNUC__ == 2 && __GNUC_MINOR__ <= 95
#     include <iterator>  // not sure this is the right way to do this -JGS
#     if !defined(_CXXRT_STD) && !defined(__SGI_STL) // need to ask Dietmar about this -JGS
#       define BOOST_NO_STD_ITERATOR
#       define BOOST_NO_LIMITS
#     endif
#     define BOOST_NO_MEMBER_TEMPLATE_FRIENDS
#     define BOOST_NO_OPERATORS_IN_NAMESPACE
#   endif
#   if __GNUC__ == 2 && __GNUC_MINOR__ <= 8
#     define BOOST_NO_MEMBER_TEMPLATES
#   endif

//  Kai C++ ------------------------------------------------------------------//

#elif defined __KCC
#   define BOOST_NO_SLIST
#   define BOOST_NO_HASH

//  Greenhills C++ -----------------------------------------------------------//

#elif defined __ghs
#   define BOOST_NO_SLIST
#   define BOOST_NO_HASH

//  Borland ------------------------------------------------------------------//

#elif defined __BORLANDC__
#   define BOOST_NO_SLIST
#   define BOOST_NO_HASH
#   if __BORLANDC__ <= 0x0551
#     define BOOST_NO_INTEGRAL_INT64_T
#     define BOOST_NO_PRIVATE_IN_AGGREGATE
#   endif
#   if __BORLANDC__ <= 0x0550
// Borland C++ Builder 4 and 5:
#     define BOOST_NO_MEMBER_TEMPLATE_FRIENDS
#     define BOOST_NO_USING_TEMPLATE
#     if __BORLANDC__ == 0x0550
// Borland C++ Builder 5, command-line compiler 5.5:
#       define BOOST_NO_OPERATORS_IN_NAMESPACE
#     endif
#   endif
#   if defined BOOST_DECL_EXPORTS
#     define BOOST_DECL __declspec(dllexport)
#   else
#     define BOOST_DECL __declspec(dllimport)
#   endif

//  Intel  -------------------------------------------------------------------//

# elif defined __ICL
#   include <iterator>  // not sure this is the right way to do this -JGS
#   if __SGI_STL_PORT >= 0x400 || __SGI_STL_PORT >= 0x321 && defined(__STL_USE_NAMESPACES)
        // a perfectly good implementation of std::iterator is supplied
#   elif defined(__SGI_STL_ITERATOR)
#     define BOOST_NO_STD_ITERATOR // No std::iterator in this case
#   else // assume using dinkumware's STL that comes with VC++ 6.0
#     define BOOST_MSVC_STD_ITERATOR
#     define BOOST_NO_STD_ITERATOR_TRAITS
#     define BOOST_NO_STDC_NAMESPACE
#     define BOOST_NO_SLIST
#     define BOOST_NO_HASH
#     define BOOST_NO_TEMPLATED_ITERATOR_CONSTRUCTORS
#     define BOOST_NO_STD_ALLOCATOR
#     define BOOST_NO_STD_MIN_MAX
#   endif


//  Metrowerks CodeWarrior  --------------------------------------------------//

# elif defined  __MWERKS__
#   if __MWERKS__ <= 0x4000
#     define BOOST_NO_MEMBER_TEMPLATE_FRIENDS
#   endif
#   if __MWERKS__ <= 0x2301
#     define BOOST_NO_POINTER_TO_MEMBER_CONST
#   endif
#   if __MWERKS__ >= 0x2300
#     define BOOST_SYSTEM_HAS_STDINT_H
#   endif
#   if defined BOOST_DECL_EXPORTS
#     define BOOST_DECL __declspec(dllexport)
#   else
#     define BOOST_DECL __declspec(dllimport)
#   endif

#   define BOOST_STD_EXTENSION_NAMESPACE Metrowerks

//  Sun Workshop Compiler C++ ------------------------------------------------//

# elif defined  __SUNPRO_CC
#    if __SUNPRO_CC <= 0x500
#      define BOOST_NO_MEMBER_TEMPLATES
#      define BOOST_NO_TEMPLATE_PARTIAL_SPECIALIZATION
#    endif

//  Microsoft Visual C++ (excluding Intel/EDG front end)  --------------------//
//
//  Must remain the last #elif since some other vendors (Metrowerks, for
//  example) also #define _MSC_VER

# elif defined _MSC_VER
#   define BOOST_MSVC _MSC_VER

    // turn off the warnings before we #include anything
#   pragma warning( disable : 4786 ) // ident trunc to '255' chars in debug info
#   pragma warning( disable : 4503 ) // warning: decorated name length exceeded
#   pragma warning( disable : 4284 ) // APS

//#   if _MSC_VER <= 1200  // 1200 == VC++ 6.0
#   if _MSC_VER <= 1300  // 1300 == VC++ 7.0
#     define BOOST_NO_INCLASS_MEMBER_INITIALIZATION
#     define BOOST_NO_PRIVATE_IN_AGGREGATE

#     define BOOST_NO_INTEGRAL_INT64_T
#     define BOOST_NO_INTRINSIC_WCHAR_T

//    VC++ 6.0 has member templates but they have numerous problems including
//    cases of silent failure, so for safety we define:
#     define BOOST_NO_MEMBER_TEMPLATES
//    For VC++ experts wishing to attempt workarounds, we define:
#     define BOOST_MSVC6_MEMBER_TEMPLATES

#     define BOOST_NO_MEMBER_TEMPLATE_FRIENDS
#     define BOOST_NO_TEMPLATE_PARTIAL_SPECIALIZATION
#     define BOOST_NO_DEPENDENT_TYPES_IN_TEMPLATE_VALUE_PARAMETERS

#     include <iterator>  // not sure this is the right way to do this -JGS
#     if __SGI_STL_PORT >= 0x400 || __SGI_STL_PORT >= 0x321 && defined(__STL_USE_NAMESPACES)
        // a perfectly good implementation of std::iterator is supplied
#     elif defined(__SGI_STL_ITERATOR)
#       define BOOST_NO_STD_ITERATOR // No std::iterator in this case
#     else
#       define BOOST_MSVC_STD_ITERATOR 1
#       define BOOST_NO_SLIST
#       define BOOST_NO_HASH
#       define BOOST_NO_TEMPLATED_ITERATOR_CONSTRUCTORS
#       define BOOST_NO_STD_ALLOCATOR
//#       define BOOST_NO_STD_MIN_MAX // (APS)
#     endif
#     define BOOST_NO_STD_ITERATOR_TRAITS


// Make sure at least one standard library header is included so that library
// implementation detection will work, even if no standard headers have been
// included in front of a boost header. (Ed Brey 5 Jun 00)
#ifndef _WIN32_WCE
#     include <cstddef>
#endif
// Determine if the standard library implementation is already pulling names
// into std.  STLport defines the following if so. (Ed Brey 5 Jun 00)
#     ifndef __STL_IMPORT_VENDOR_CSTD
#       define BOOST_NO_STDC_NAMESPACE
#     endif

#   endif

#   if defined BOOST_DECL_EXPORTS
#     define BOOST_DECL __declspec(dllexport)
#   else
#     define BOOST_DECL __declspec(dllimport)
#   endif

# endif // Microsoft (excluding Intel/EDG frontend) 

# ifndef BOOST_DECL
#   define BOOST_DECL  // default for compilers not needing this decoration.
# endif

//  end of compiler specific portion  ----------------------------------------//

#ifndef BOOST_STD_EXTENSION_NAMESPACE
# define BOOST_STD_EXTENSION_NAMESPACE std
#endif

// Check for old name "BOOST_NMEMBER_TEMPLATES" for compatibility  -----------//
// Don't use BOOST_NMEMBER_TEMPLATES. It is deprecated and will be removed soon.
#if defined( BOOST_NMEMBER_TEMPLATES ) && !defined( BOOST_NO_MEMBER_TEMPLATES )
  #define BOOST_NO_MEMBER_TEMPLATES
#endif

//  BOOST_NO_STDC_NAMESPACE workaround  --------------------------------------//
//
//  Because std::size_t usage is so common, even in boost headers which do not
//  otherwise use the C library, the <cstddef> workaround is included here so
//  that ugly workaround code need not appear in many other boost headers.
//  NOTE WELL: This is a workaround for non-conforming compilers; <cstddef> 
//  must still be #included in the usual places so that <cstddef> inclusion
//  works as expected with standard conforming compilers.  The resulting
//  double inclusion of <cstddef> is harmless.

# ifdef BOOST_NO_STDC_NAMESPACE
#ifndef _WIN32_WCE
#   include <cstddef>
#endif
    namespace std { using ::ptrdiff_t; using ::size_t; }
    // using ::wchar_t; removed since wchar_t is a C++ built-in type (Ed Brey)
# endif

#ifdef BOOST_NO_STD_MIN_MAX
namespace std {
  template <class _Tp>
  inline const _Tp& min(const _Tp& __a, const _Tp& __b) {
    return __b < __a ? __b : __a;
  }
  template <class _Tp>
  inline const _Tp& max(const _Tp& __a, const _Tp& __b) {
    return  __a < __b ? __b : __a;
  }
}
#endif

#endif  // BOOST_CONFIG_HPP

//namespace boost {

//  boost utility.hpp header file  -------------------------------------------//

//  (C) Copyright boost.org 1999. Permission to copy, use, modify, sell
//  and distribute this software is granted provided this copyright
//  notice appears in all copies. This software is provided "as is" without
//  express or implied warranty, and with no claim as to its suitability for
//  any purpose.

//  See http://www.boost.org for most recent version including documentation.

//  Classes appear in alphabetical order

//  Revision History
//  26 Jan 00  protected noncopyable destructor added (Miki Jovanovic)
//  10 Dec 99  next() and prior() templates added (Dave Abrahams)
//  30 Aug 99  moved cast templates to cast.hpp (Beman Dawes)
//   3 Aug 99  cast templates added
//  20 Jul 99  name changed to utility.hpp 
//   9 Jun 99  protected noncopyable default ctor
//   2 Jun 99  Initial Version. Class noncopyable only contents (Dave Abrahams)

#ifndef BOOST_UTILITY_HPP
#define BOOST_UTILITY_HPP

//#include <boost/config.hpp>
#ifndef _WIN32_WCE
#include <cstddef>            // for size_t
#endif
#include <utility>            // for std::pair

//namespace boost {

//  next() and prior() template functions  -----------------------------------//

    //  Helper functions for classes like bidirectional iterators not supporting
    //  operator+ and operator-.
    //
    //  Usage:
    //    const std::list<T>::iterator p = get_some_iterator();
    //    const std::list<T>::iterator prev = prior(p);

    //  Contributed by Dave Abrahams

    template <class T>
    T next(T x) { return ++x; }

    template <class T>
    T prior(T x) { return --x; }


//  class noncopyable  -------------------------------------------------------//

    //  Private copy constructor and copy assignment ensure classes derived from
    //  class noncopyable cannot be copied.

    //  Contributed by Dave Abrahams

    class noncopyable
    {
    protected:
        noncopyable(){}
        ~noncopyable(){}
    private:  // emphasize the following members are private
        noncopyable( const noncopyable& );
        const noncopyable& operator=( const noncopyable& );
    }; // noncopyable

//  class tied  -------------------------------------------------------//

    // A helper for conveniently assigning the two values from a pair
    // into separate variables. The idea for this comes from Jaakko J�rvi's
    // Binder/Lambda Library.

    // Constributed by Jeremy Siek

    template <class A, class B>
    class tied {
    public:
      inline tied(A& a, B& b) : _a(a), _b(b) { }
      template <class U, class V>
      inline tied& operator=(const std::pair<U,V>& p) {
        _a = p.first;
        _b = p.second;
        return *this;
      }
    protected:
      A& _a;
      B& _b;
    };

    template <class A, class B>
    inline tied<A,B> tie(A& a, B& b) { return tied<A,B>(a, b); }

//} // namespace boost

#endif  // BOOST_UTILITY_HPP

//  ScopedPtr  --------------------------------------------------------------//

//  ScopedPtr mimics a built-in pointer except that it guarantees deletion
//  of the object pointed to, either on destruction of the ScopedPtr or via
//  an explicit reset().  ScopedPtr is a simple solution for simple needs;
//  see SharedPtr (below) or std::auto_ptr if your needs are more complex.

template<typename T> class ScopedPtr : noncopyable {

  T* ptr;

 public:
  typedef T element_type;

  explicit ScopedPtr( T* p=0 ) : ptr(p) {}  // never throws
  ~ScopedPtr()                 { delete ptr; }

  void reset( T* p=0 )          { if ( ptr != p ) { delete ptr; ptr = p; } }
  T& operator*() const          { return *ptr; }  // never throws
  T* operator->() const         { return ptr; }  // never throws
  T* get() const                { return ptr; }  // never throws
#ifdef BOOST_SMART_PTR_CONVERSION
  // get() is safer! Define BOOST_SMART_PTR_CONVERSION at your own risk!
  operator T*() const           { return ptr; }  // never throws 
#endif
  };  // ScopedPtr

//  ScopedArray  ------------------------------------------------------------//

//  ScopedArray extends ScopedPtr to arrays. Deletion of the array pointed to
//  is guaranteed, either on destruction of the ScopedArray or via an explicit
//  reset(). See SharedArray or std::vector if your needs are more complex.

template<typename T> class ScopedArray : noncopyable {

  T* ptr;

 public:
  typedef T element_type;

  explicit ScopedArray( T* p=0 ) : ptr(p) {}  // never throws
  ~ScopedArray()                    { delete [] ptr; }

  void reset( T* p=0 )               { if ( ptr != p ) {delete [] ptr; ptr=p;} }

  T* get() const                     { return ptr; }  // never throws
#ifdef BOOST_SMART_PTR_CONVERSION
  // get() is safer! Define BOOST_SMART_PTR_CONVERSION at your own risk!
  operator T*() const                { return ptr; }  // never throws
#else 
  T& operator[](std::size_t i) const { return ptr[i]; }  // never throws
#endif
  };  // ScopedArray

//  SharedPtr  --------------------------------------------------------------//

//  An enhanced relative of ScopedPtr with reference counted copy semantics.
//  The object pointed to is deleted when the last SharedPtr pointing to it
//  is destroyed or reset.

template<typename T> class SharedPtr {
  public:
   typedef T element_type;

   explicit SharedPtr(T* p =0) : px(p) {
      SP_TRY { pn = new long(1); }  // fix: prevent leak if new throws
      SP_CATCH(...) { delete p; SP_THROW; } 
   }

   SharedPtr(const SharedPtr& r) : px(r.px) {
#ifdef USE_INTERLOCKED
      InterlockedIncrement(r.pn);
      pn = r.pn;
#else
      ++*(pn = r.pn);
#endif
   }  // never throws

   ~SharedPtr() { dispose(); }

   SharedPtr& operator=(const SharedPtr& r) {
      share(r.px,r.pn);
      return *this;
   }

#if !defined( BOOST_NO_MEMBER_TEMPLATES )
   template<typename Y>
      SharedPtr(const SharedPtr<Y>& r) : px(r.px) {  // never throws 
#ifdef USE_INTERLOCKED
         InterlockedIncrement(r.pn);
         pn = r.pn;
#else
         ++*(pn = r.pn); 
#endif
      }

   template<typename Y>
      SharedPtr(std::auto_ptr<Y>& r) { 
         pn = new long(1); // may throw
         px = r.release(); // fix: moved here to stop leak if new throws
      } 

   template<typename Y>
      SharedPtr& operator=(const SharedPtr<Y>& r) { 
         share(r.px,r.pn);
         return *this;
      }

   template<typename Y>
      SharedPtr& operator=(std::auto_ptr<Y>& r) {
         // code choice driven by guarantee of "no effect if new throws"
         if (*pn == 1) { delete px; }
         else { // allocate new reference counter
           long * tmp = new long(1); // may throw
#ifdef USE_INTERLOCKED
           InterlockedDecrement(pn);
#else
           --*pn; // only decrement once danger of new throwing is past
#endif
           pn = tmp;
         } // allocate new reference counter
         px = r.release(); // fix: moved here so doesn't leak if new throws 
         return *this;
      }
#else
      SharedPtr(std::auto_ptr<T>& r) { 
         pn = new long(1); // may throw
         px = r.release(); // fix: moved here to stop leak if new throws
      } 

      SharedPtr& operator=(std::auto_ptr<T>& r) {
         // code choice driven by guarantee of "no effect if new throws"
         if (*pn == 1) { delete px; }
         else { // allocate new reference counter
           long * tmp = new long(1); // may throw
#ifdef USE_INTERLOCKED
           InterlockedDecrement(pn);
#else
           --*pn; // only decrement once danger of new throwing is past
#endif
           pn = tmp;
         } // allocate new reference counter
         px = r.release(); // fix: moved here so doesn't leak if new throws 
         return *this;
      }
#endif

   void reset(T* p=0) {
      if ( px == p ) return;  // fix: self-assignment safe
#ifdef USE_INTERLOCKED
      if (InterlockedDecrement(pn) == 0) { delete px; }
#else
      if (--*pn == 0) { delete px; }
#endif
      else { // allocate new reference counter
        SP_TRY { pn = new long; }  // fix: prevent leak if new throws
        SP_CATCH(...) {
#ifdef USE_INTERLOCKED
          InterlockedIncrement(pn);
#else
          ++*pn;  // undo effect of --*pn above to meet effects guarantee 
#endif
          delete p;
          SP_THROW;
        } // catch
      } // allocate new reference counter
      *pn = 1;
      px = p;
   } // reset

   T& operator*() const          { return *px; }  // never throws
   T* operator->() const         { return px; }  // never throws
   T* get() const                { return px; }  // never throws
 #ifdef BOOST_SMART_PTR_CONVERSION
   // get() is safer! Define BOOST_SMART_PTR_CONVERSION at your own risk!
   operator T*() const           { return px; }  // never throws 
 #endif

   long use_count() const        { return *pn; }  // never throws
   bool unique() const           { return *pn == 1; }  // never throws

   void swap(SharedPtr<T>& other)  // never throws
     { std::swap(px,other.px); std::swap(pn,other.pn); }

// Tasteless as this may seem, making all members public allows member templates
// to work in the absence of member template friends. (Matthew Langston)
// Don't split this line into two; that causes problems for some GCC 2.95.2 builds
#if defined(BOOST_NO_MEMBER_TEMPLATES) || !defined( BOOST_NO_MEMBER_TEMPLATE_FRIENDS )
   private:
#endif

   T*     px;     // contained pointer
   long*  pn;     // ptr to reference counter

// Don't split this line into two; that causes problems for some GCC 2.95.2 builds
#if !defined( BOOST_NO_MEMBER_TEMPLATES ) && !defined( BOOST_NO_MEMBER_TEMPLATE_FRIENDS )
   template<typename Y> friend class SharedPtr;
#endif

#ifdef USE_INTERLOCKED
   void dispose() { if (InterlockedDecrement(pn) == 0) { delete px; delete pn; } }
#else
   void dispose() { if (--*pn == 0) { delete px; delete pn; } }
#endif

   void share(T* rpx, long* rpn) {
      if (pn != rpn) {
         dispose();
         px = rpx;
#ifdef USE_INTERLOCKED
         InterlockedIncrement(rpn);
         pn = rpn;
#else
         ++*(pn = rpn);
#endif
      }
   } // share
};  // SharedPtr

template<typename T, typename U>
  inline bool operator==(const SharedPtr<T>& a, const SharedPtr<U>& b)
    { return a.get() == b.get(); }

template<typename T, typename U>
  inline bool operator!=(const SharedPtr<T>& a, const SharedPtr<U>& b)
    { return a.get() != b.get(); }

//  SharedArray  ------------------------------------------------------------//

//  SharedArray extends SharedPtr to arrays.
//  The array pointed to is deleted when the last SharedArray pointing to it
//  is destroyed or reset.

template<typename T> class SharedArray {
  public:
   typedef T element_type;

   explicit SharedArray(T* p =0) : px(p) {
      SP_TRY { pn = new long(1); }  // fix: prevent leak if new throws
      SP_CATCH(...) { delete [] p; SP_THROW; } 
   }

   SharedArray(const SharedArray& r) : px(r.px)  // never throws
   {
#ifdef USE_INTERLOCKED
      InterlockedIncrement(r.pn);
      pn = r.pn;
#else
      ++*(pn = r.pn);
#endif
   }

   ~SharedArray() { dispose(); }

   SharedArray& operator=(const SharedArray& r) {
      if (pn != r.pn) {
         dispose();
         px = r.px;
#ifdef USE_INTERLOCKED
         InterlockedIncrement(r.pn);
         pn = r.pn;
#else
         ++*(pn = r.pn);
#endif
      }
      return *this;
   } // operator=

   void reset(T* p=0) {
      if ( px == p ) return;  // fix: self-assignment safe
#ifdef USE_INTERLOCKED
      if (InterlockedDecrement(pn) == 0) { delete [] px; }
#else
	  if (--*pn == 0) { delete [] px; }
#endif
      else { // allocate new reference counter
        SP_TRY { pn = new long; }  // fix: prevent leak if new throws
        SP_CATCH(...) {
#ifdef USE_INTERLOCKED
          InterlockedIncrement(pn);
#else
          ++*pn;  // undo effect of --*pn above to meet effects guarantee 
#endif
          delete [] p;
          SP_THROW;
        } // catch
      } // allocate new reference counter
      *pn = 1;
      px = p;
   } // reset

   T* get() const                     { return px; }  // never throws
 #ifdef BOOST_SMART_PTR_CONVERSION
   // get() is safer! Define BOOST_SMART_PTR_CONVERSION at your own risk!
   operator T*() const                { return px; }  // never throws
 #else 
   T& operator[](std::size_t i) const { return px[i]; }  // never throws
 #endif

   long use_count() const             { return *pn; }  // never throws
   bool unique() const                { return *pn == 1; }  // never throws

   void swap(SharedArray<T>& other)  // never throws
     { std::swap(px,other.px); std::swap(pn,other.pn); }

  private:

   T*     px;     // contained pointer
   long*  pn;     // ptr to reference counter

#ifdef USE_INTERLOCKED
   void dispose() { if (InterlockedDecrement(pn) == 0) { delete [] px; delete pn; } }
#else
   void dispose() { if (--*pn == 0) { delete [] px; delete pn; } }
#endif

};  // SharedArray

template<typename T>
  inline bool operator==(const SharedArray<T>& a, const SharedArray<T>& b)
    { return a.get() == b.get(); }

template<typename T>
  inline bool operator!=(const SharedArray<T>& a, const SharedArray<T>& b)
    { return a.get() != b.get(); }

//} // namespace boost

//  specializations for things in namespace std  -----------------------------//

#ifndef BOOST_NO_TEMPLATE_PARTIAL_SPECIALIZATION

namespace std {

// Specialize std::swap to use the fast, non-throwing swap that's provided
// as a member function instead of using the default algorithm which creates
// a temporary and uses assignment.

template<typename T>
  inline void swap(SharedPtr<T>& a, SharedPtr<T>& b)
    { a.swap(b); }

template<typename T>
  inline void swap(SharedArray<T>& a, SharedArray<T>& b)
    { a.swap(b); }

// Specialize std::less so we can use shared pointers and arrays as keys in
// associative collections.

// It's still a controversial question whether this is better than supplying
// a full range of comparison operators (<, >, <=, >=).

template<typename T>
  struct less< SharedPtr<T> >
    : binary_function<SharedPtr<T>, SharedPtr<T>, bool>
  {
    bool operator()(const SharedPtr<T>& a,
        const SharedPtr<T>& b) const
      { return less<T*>()(a.get(),b.get()); }
  };

template<typename T>
  struct less< SharedArray<T> >
    : binary_function<SharedArray<T>, SharedArray<T>, bool>
  {
    bool operator()(const SharedArray<T>& a,
        const SharedArray<T>& b) const
      { return less<T*>()(a.get(),b.get()); }
  };

} // namespace std

#endif  // ifndef BOOST_NO_TEMPLATE_PARTIAL_SPECIALIZATION

#endif  // BOOST_SMART_PTR_HPP

