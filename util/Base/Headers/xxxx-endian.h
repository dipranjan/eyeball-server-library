#ifndef SV_ENDIAN_H
#define SV_ENDIAN_H

// Figure out endianism based on defines available to us
#if defined(sun)
#include <sys/isa_defs.h>		// _BIG_ENDIAN
#elif defined(__linux__)
#include <endian.h>				// __BYTE_ORDER
#endif

#if defined(__BYTE_ORDER)
	#if (__BYTE_ORDER == __BIG_ENDIAN)
	#define _BIG_ENDIAN
	#endif
#endif

// Swap bytes as necessary - must be a better way to do this!
#define FIX_ENDIAN16(X) \
	(((0xff00 & X) >> 8) | ((0x00ff & X) << 8))

#define FIX_ENDIAN32(X) \
	(((0xff000000 & X) >> 24) | \
	((0x00ff0000 & X) >> 8) | \
	((0x0000ff00 & X) << 8) | \
	((0x000000ff & X) << 24))

#ifdef _BIG_ENDIAN
	#define LITTLE16(X) FIX_ENDIAN16(X)
	#define LITTLE32(X) FIX_ENDIAN32(X)

	#define BIG16(X) (X)
	#define BIG32(X) (X)
#else
	#define LITTLE16(X) (X)
	#define LITTLE32(X) (X)

	#define BIG16(X) FIX_ENDIAN16(X)
	#define BIG32(X) FIX_ENDIAN32(X)
#endif

#endif // SV_ENDIAN_H

