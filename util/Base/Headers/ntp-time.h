// ---------------------------------------------------------------------------
// File:       ntp-time.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Inline time access functions.
// 
// Change Log:
// ---------------------------------------------------------------------------

#ifndef NTP_TIME_H
#define NTP_TIME_H

#include "sys-time.h"
#include "config.h"
#include "RtpBase.h"


inline double gettimeofday()
{
	timeval tv;
	::gettimeofday(&tv, 0);
	return (tv.tv_sec + 1e-6 * tv.tv_usec);
}

// In Windows, simulate gettimeofday() with _ftime()
#ifdef WIN32
inline int gettimeofday(struct timeval *tv, struct timezone *tz)
{
	struct _timeb tb;
	_ftime(&tb);

	tv->tv_sec = tb.time;
	tv->tv_usec = 1000 * tb.millitm;

	return 0;
}
#endif

/*
 * convert microseconds to fraction of second * 2^32 (i.e., the lsw of
 * a 64-bit ntp timestamp).  This routine uses the factorization
 * 2^32/10^6 = 4096 + 256 - 1825/32 which results in a max conversion
 * error of 3 * 10^-7 and an average error of half that.
 */
inline u_int usec2ntp(u_int usec)
{
	u_int t = (usec * 1825) >> 5;
	return ((usec << 12) + (usec << 8) - t);
}

/*
 * Number of seconds between 1-Jan-1900 and 1-Jan-1970
 */
#define GETTIMEOFDAY_TO_NTP_OFFSET 2208988800

/*
 * Return a 64-bit ntp timestamp (UTC time relative to Jan 1, 1970).
 * gettimeofday conveniently gives us the correct reference -- we just
 * need to convert sec+usec to a 64-bit fixed point (with binary point
 * at bit 32).
 */
inline ntp64 ntp64time(timeval tv)
{
	ntp64 n;
	n.upper = (u_int)tv.tv_sec + GETTIMEOFDAY_TO_NTP_OFFSET;
	n.lower = usec2ntp((u_int)tv.tv_usec);
	return (n);
}

inline u_int32_t ntptime(timeval t)
{
	u_int s = (u_int)t.tv_sec + GETTIMEOFDAY_TO_NTP_OFFSET;
	return (s << 16 | usec2ntp((u_int)t.tv_usec) >> 16);
}

inline u_int32_t ntptime()
{
	timeval tv;
	::gettimeofday(&tv, NULL);
	return (ntptime(tv));
}

inline ntp64 ntp64time()
{
	timeval tv;
	::gettimeofday(&tv, NULL);
	return (ntp64time(tv));
}

inline timeval unixtime()
{
	timeval tv;
	::gettimeofday(&tv, NULL);
	return (tv);
}

#endif // NTP_TIME_H
