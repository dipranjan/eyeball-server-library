//==============================================================================
// Source: http://www.codeproject.com/useritems/multidisp.asp
//
// FILE:       multidisp.h
// DATE:       9/01/00
// AUTHOR:     Franklin Colbert
// REVISIONS:
//    12/03/00 - <OBJECT> tag limitation discovered.  Documented as limitation
//    08/29/01 - Applied DISPID encoding to solution.  This fixed
//               OBJECT tag limitation.
//
// DESCRIPTION:
//    Implements macros and templates which allows pure automation (IDispatch
//    only) clients access to all IDispatchImpl (dual) interfaces offered by a
//    given object. This effectively gets around the 1 default interface per
//    object limitation by offering up a union of user-selected duals thru
//    IDispatch.  Now you can do COM design right and use QI without polluting
//    your design for the clove cigarette crowd.
//
// IMPL NOTES:
//    Ultimately relies on IDispatch invocation using ITypeInfo::Invoke via
//    ATL's CComTypeInfoHolder
//
// LIMITATIONS:
// 1. You must derive from IDispatchImpl for your duals. You cannot overload
//    method names in the union of your duals. Method names must be
//    unique across all interfaces.
//
// 2. Your DISPIDs must fit within the size of a word. In other words,
//    nothing greater than 65,535
//
// CREDITS:
//    To my knowledge this is an original implementation, however the core of
//    the idea was motivated by Dr. Richard Grimes work in "Professional ATL COM
//    Programming".  See DISPID Encoding pg. 168.
//
// USAGE:
//    Note the following usage in this sample header snip -
//
// 1) --> #include "multidisp.h"
//
//     class ATL_NO_VTABLE CAbundantFeast : 
//        public CComObjectRootEx<CComSingleThreadModel>,
//        public IDispatchImpl<IAbundantFeast, &IID_IAbundantFeast,
//                             &LIBID_GODOFDISPATCHLib>,
//        public IDispatchImpl<IBulemia, &IID_IBulemia,
//                             &LIBID_GODOFDISPATCHLib>,
//        public CComCoClass<CAbundantFeast, &CLSID_AbundantFeast>,
// 2) --> public CMultiDispatch<CAbundantFeast>  // ADD THIS
//     {
//     public:
//
// 3) --> DECLARE_MULTI_DISPATCH(CAbundantFeast)  // ADD THIS
//
//    typedef IDispatchImpl<IAbundantFeast,
//                          &IID_IAbundantFeast, 
//                          &LIBID_GODOFDISPATCHLib> dispBase1;
//    typedef IDispatchImpl<IBulemia,
//                          &IID_IBulemia, 
//                          &LIBID_GODOFDISPATCHLib> dispBase2;
//
//    // ADD THIS MAP - select the IDispatchImpl derived classes you want
// 
// 4) --> BEGIN_MULTI_DISPATCH_MAP(CAbundantFeast)
//           MULTI_DISPATCH_ENTRY(dispBase1)
//           MULTI_DISPATCH_ENTRY(dispBase2)
//        END_MULTI_DISPATCH_MAP()         
//
//==============================================================================

#ifndef _MULTIDISP_H_
#define _MULTIDISP_H_

#define INTERFACE_MASK 0xFFFF0000UL
#define DISPID_MASK    0x0000FFFFUL

// 2) - (see USAGE block in file header comment) 
template <class T>
class ATL_NO_VTABLE CMultiDispatch
{
public:
   STDMETHOD(GetIDsOfNames)(REFIID riid, LPOLESTR* rgszNames,
                            UINT cNames, LCID lcid, DISPID* rgdispid)
	{
      setDispatchMapCount();
      
      T* pT = static_cast<T*> (this);
      _TIH_ENTRY* pEntry = pT->GetTypeInfoHolder();
      while (pEntry->ptih != NULL)
      {
         HRESULT hr = pEntry->ptih->GetIDsOfNames(riid, rgszNames,
                                                  cNames, lcid, rgdispid);
         if (SUCCEEDED(hr))
         {
            //
            // Return the DISPID with the encoding OR'd in.
            //
            rgdispid[0] |= pEntry->dispEncode;
            return hr;
         }
         pEntry++;
      }
      return DISP_E_UNKNOWNNAME;
	}

   STDMETHOD(Invoke)(DISPID dispidMember, REFIID riid,
                     LCID lcid, WORD wFlags, DISPPARAMS* pdispparams,
                     VARIANT* pvarResult, EXCEPINFO* pexcepinfo,
                     UINT* puArgErr)
	{
      HRESULT hrError = DISP_E_MEMBERNOTFOUND; // added
      T* pT = static_cast<T*> (this);
      _TIH_ENTRY* pEntry = pT->GetTypeInfoHolder();
      while (pEntry->ptih != NULL)
      {
         //
         // Did we set the bit in GetIDsOfNames?  If so, we can invoke
         // with the correct IDispatch.
         //
         if (pEntry->dispEncode == (dispidMember & INTERFACE_MASK))
         {
            //
            // Offset from User class to IDispatchImpl is calculated and stored
            // in the static array.  Using this offset allows us to disambiguate
            // the cast to the IDispatch base pointer.
            //
            HRESULT hr = pEntry->ptih->Invoke((IDispatch*)(((DWORD)pT)+pEntry->offset),
                                              (dispidMember & DISPID_MASK), riid, lcid,
                                              wFlags, pdispparams, pvarResult,
                                              pexcepinfo, puArgErr);
            if (SUCCEEDED(hr))
			{
               return hr;
			}
			else
			{
				hrError = hr; // added
			}
         }
         pEntry++;
      }
	  //return DISP_E_MEMBERNOTFOUND; // removed
      return hrError; // added
	}
protected:
   //
   // Encode each entry in the array.  Use the HIWORD for this.
   //
   void setDispatchMapCount()
   {
      static bool counted = false;
      if (!counted)
      {
         counted = true;
         DWORD count = 1;
   
         T* pT = static_cast<T*> (this);
         _TIH_ENTRY* pEntry = pT->GetTypeInfoHolder();
         while (pEntry->ptih != NULL)
         {
            pEntry->dispEncode = count << 16;
            count++;
            pEntry++;
         }
      }
   }
};

// 3) - (see USAGE block in file header comment) 
#define DECLARE_MULTI_DISPATCH(T) \
   STDMETHOD(GetIDsOfNames)(REFIID riid, LPOLESTR* rgszNames, UINT cNames, \
                            LCID lcid, DISPID* rgdispid) \
	{ \
      return CMultiDispatch<T>::GetIDsOfNames(riid, rgszNames, cNames, \
                                              lcid, rgdispid); \
	} \
   STDMETHOD(Invoke)(DISPID dispidMember, REFIID riid, \
                     LCID lcid, WORD wFlags, DISPPARAMS* pdispparams, VARIANT* pvarResult, \
                     EXCEPINFO* pexcepinfo, UINT* puArgErr) \
	{ \
      return CMultiDispatch<T>::Invoke(dispidMember, riid, lcid, wFlags, \
                                       pdispparams, pvarResult, \
                                       pexcepinfo, puArgErr); \
	}

struct _TIH_ENTRY {
   CComTypeInfoHolder *ptih;  // Delegate IDispatch calls to this
   DWORD dispEncode;          // Encoding bit(s)
   DWORD offset;              // offset from derived to IDispatchImpl
};

// 4) - (see USAGE block in file header comment) 
#define BEGIN_MULTI_DISPATCH_MAP(CLS) \
   typedef CLS theDerived; \
   static struct _TIH_ENTRY* GetTypeInfoHolder() { \
      static struct _TIH_ENTRY pDispEntries[] = {

#define MULTI_DISPATCH_ENTRY(theBase) \
      { &theBase::_tih, 0UL, offsetofclass(theBase, theDerived) },

#define END_MULTI_DISPATCH_MAP() \
   { NULL, 0UL, 0UL } }; \
   return(pDispEntries); }


#endif // sentry