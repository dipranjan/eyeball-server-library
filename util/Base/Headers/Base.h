// ---------------------------------------------------------------------------
// File:       Base.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Pre-compiled header file - reduces compilation time
//
// Change Log:
// ---------------------------------------------------------------------------

#ifndef _EC_BASE_H_
#define _EC_BASE_H_

// #ifdef _UNICODE
#if defined(_UNICODE) && !defined(UNICODE)
#define UNICODE
#endif

#if 1 //for Linux yamada
//LinuxÅÍWindowsCE(PocketPC)Åðx[XÆ·é
#define _WIN32_WCE
#endif //for Linux 
#undef AUDIO_RECEIVE_ENABLE
#define  AUDIO_SEND_ENABLE
#define SLEEP_DISABLE
// ---------------------------------------------------------------------------
// Disabled warnings
// ---------------------------------------------------------------------------

#pragma warning(disable:4127) // conditional expression is constant
#pragma warning(disable:4284) // disable auto_ptr UDP warning
#pragma warning(disable:4710) // function not inlined
#pragma warning(disable:4786) // disable length warnings from STL
#pragma warning(disable:4018) // disable signed/unsigned mismatch from STL
//#pragma warning(disable:4531) // disable C++ exception handling warning for CE

// ---------------------------------------------------------------------------
// Standard System files
// ---------------------------------------------------------------------------

#if 0 //for Linux yamada
  
#ifndef _WIN32_WCE
#define _WINSOCKAPI_  // must be defined before windows.h
#include <winsock2.h>
#else
#include <winsock.h>
#endif
#include <windows.h>

#endif //for Linux yamada

// ---------------------------------------------------------------------------
// Common C++ Standard Library files
// ---------------------------------------------------------------------------
#if 1 //for Linux yamada
#include <list>
#include <map>
#include <string>
#include <cstring>
#include <queue>
#include <set>
/* for XPCOM */
//#include "nscore.h" //Farhan commented it out
#include <iostream>
#include <unistd.h>
#else //for Linux yamada

#ifndef _WIN32_WCE
//#if 1
#include <list>
#include <map>
#include <string>
#include <queue>
#include <set>
#else
#include "StringCE.h"
#include "PairCE.h"
#include "QueueCE.h"
#include "MapCE.h"
#include "ListCE.h"
#include "SetCE.h"
#endif

#endif //for Linux yamada

#include "SmartPtr.h"	//mazhar_test
//using namespace std;  //mazhar_test


// ---------------------------------------------------------------------------
// Unicode
// ---------------------------------------------------------------------------
#if 1 //for Linux yamada
typedef char TCHAR;
#else
#include <tchar.h>
#endif //for Linux yamada
#include "tstring.h"


// ---------------------------------------------------------------------------
// Commonly used, infrequently modified files.
// ---------------------------------------------------------------------------

#include "debug.h"
#include "EUtil.h"

/*				//This block is commented by Farhan
#ifndef _WIN32_WCE
#include "SmartPtr.h"
#else
#include "SmartPtrCE.h"
#endif
*/
//#include "SmartPtr.h"		//Farhan added this line after commenting out the previous block

/* Sawamura for Debug fonction*/
/* dummy function macro*/
#define MakeDummyFunction(type,fn) \
type fn() \
{		\
	fprintf(stderr,#fn "() is not supported\n"); \
}
struct ec_opaque_ {
	unsigned int dummy;
};
typedef struct ec_opaque_ *ec_opaque;
typedef ec_opaque TECData; // any data type
typedef ec_opaque TECWindow; //HWND
typedef ec_opaque TECDrawContext; //HDC
typedef ec_opaque TECWindowMenu; //HMENU
struct TECRect_ {
	unsigned int top;
	unsigned int left;
	unsigned int bottom;
	unsigned int right;
};
typedef struct TECRect_ TECRect; //RECT
typedef ec_opaque TECPane;
typedef ec_opaque TECColor;
typedef ec_opaque TECSize;
typedef ec_opaque TECWindowProc;
typedef ec_opaque TECInstancePtr;
typedef ec_opaque TECWparam;
typedef ec_opaque TECLparam;
#define TPM_LEFTALIGN 0
#define TPM_TOPALIGN 0
// Sawamura this Macro is MessageBox's 
#define WM_APP 100
#define MB_OK 0
#define MB_YESNO 1
#define MB_ICONEXCLAMATION 2
#define MB_ICONQUESTION 3
#define IDYES 1
#define IDNO 0
#define S_OK 0
#define SUCCEEDED(stat) ((HRESULT)(stat)>=0)
#define FAILED(stat)   ((HRESULT)(stat)<0)

// Sawamura Windows special Macro
#define MAX_PATH 1024
//Sawamura for sleep function
#include <unistd.h>
// mask for REG_NETWORK_OPTIONS registry entry
#define NETWORK_DYNAMIC_PORT    1 // when set, use dynamic port
#define NETWORK_SECURE_SOCKET   2 // when set, use secure socket
#define NETWORK_UDP_TEST        4 // when set, enable udp test
#define NETWORK_FIREWALL_SERVER 8 // when set, use gloable firewall server

#define HRESULT PRUint32
//typedef PRUint32  LRESULT;
typedef BYTE  *PBYTE;
typedef BYTE  *LPBYTE;
typedef WORD  *PWORD;
typedef WORD  *LPWORD;
typedef DWORD *DWORD_PTR;
// Error table
#define E_BASE 100
#define E_FAIL E_BASE + 1
#define E_NOTIMPL E_BASE + 2
#define E_INVALIDARG  E_BASE + 3
#define INVALID_HANDLE_VALUE E_BASE + 4
#define REGDB_E_CLASSNOTREG  E_BASE + 100
extern "C"{
extern int PostMessageLinux(TECWindow  hWnd,UINT Msg,TECWparam wParam,TECLparam lParam);
}
#define PostMessage(A,B,C,D) PostMessageLinux(A,B,(TECWparam)C,(TECLparam)D) 

//Sawamura define bitmap info
typedef DWORD RGBQUAD;
typedef struct
{
    DWORD       biSize;
    LONG        biWidth;
    LONG        biHeight;
    WORD        biPlanes;
    WORD        biBitCount;
    DWORD       biCompression;
    DWORD       biSizeImage;
    LONG        biXPelsPerMeter;
    LONG        biYPelsPerMeter;
    DWORD       biClrUsed;
    DWORD       biClrImportant;
} BITMAPINFOHEADER, *PBITMAPINFOHEADER, *LPBITMAPINFOHEADER;
                                                                                 
typedef struct {
        BITMAPINFOHEADER bmiHeader;
        RGBQUAD bmiColors[1];
} BITMAPINFO, *PBITMAPINFO, *LPBITMAPINFO;
typedef struct {
    char *      lpData;
    DWORD      dwBufferLength;
    DWORD      dwBytesRecorded;
    DWORD      dwUser;
    DWORD      dwFlags;
    DWORD      dwLoops;
    int * lpNext;
    DWORD_PTR reserved;
} WAVEHDR;
typedef struct {
    WORD  wFormatTag;
    WORD  nChannels;
    DWORD nSamplesPerSec;
    DWORD nAvgBytesPerSec;
    WORD  nBlockAlign;
    WORD  wBitsPerSample;
    WORD  cbSize;
} WAVEFORMATEX;


#define BI_RGB  16
#define LINUX_REG_NETWORK_PORT_MIN 5500
#define LINUX_REG_NETWORK_PORT_MAX 5560
#define LINUX_REG_NETWORK_PORT_LAST 5548
#define LINUX_REG_NETWORK_OPTIONS 3
#define LINUX_REG_FIREWALL_TTL 2
#define LINUX_REG_DETECT_UDP 0
#endif // _EC_BASE_H_
