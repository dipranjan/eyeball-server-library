// ---------------------------------------------------------------------------
// File:       registry.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Registry access functions
// 
// Change Log:
// ---------------------------------------------------------------------------

#ifndef GETRGVAL_H
#define GETRGVAL_H

#if 0 //for Linux yamada
#define _WINSOCKAPI_
#include <windows.h>		// DWORD
#endif //for Linux yamada

#include "tstring.h"
#include "EAlertEvent.h"

enum EcRegistryItem
{
	REG_NULL,

	// User-specific settings
	REG_MY_EYEBALL_ID,
	REG_PASSWORD,
	REG_SAVE_PASSWORD,
	REG_AUTO_LOGIN,
	REG_RECENT_IM,
	REG_AWAY_TIMEOUT,
    REG_FIRST_RUN,
	REG_TEXTMSG_FONT_BOLD,
	REG_TEXTMSG_FONT_ITALIC,
	REG_TEXTMSG_FONT_COLOR,
	REG_RECENT_FILE,
	REG_AUTO_RUN_PATH,

	// privacy:
	REG_AUTO_ACCEPT_CALL,
	REG_AUTO_ADD_CONTACT,
	REG_BLOCK_UNAUTHORIZED_TEXT,
	REG_DND_DURING_CHAT,
	REG_AUTO_ACCEPT_FILES,

	// Top-level
	REG_RECENT_EYEBALL_ID,
	REG_RECENT_SESSION,
	REG_HIDE_ON_CLOSE,
	REG_CLIENT_TYPE,

    // FileTransfer
	REG_TRANSFER_INCOME_PATH,

	// Audio
	REG_AUDIO_BUFFER_SIZE_INIT,
	REG_AUDIO_BUFFER_SIZE,
	REG_AUDIO_CAPTURE_MODE,
	REG_AUDIO_CAPTURE_DEVICE,
	REG_AUDIO_PLAYBACK_DEVICE,
	REG_AUDIO_MIC_VOLUME,

	// Video
	REG_VIDEO_CAPTURE_DEVICE,
	REG_VIDEO_CAPTURE_INPUT,
	REG_VIDEO_CAPTURE_MODE,
    REG_NOTIFY_INVITE,
	REG_VIDEO_CODEC,
	REG_QUALITY_PROFILE,
	REG_VIDEO_BRIGHTNESS,
	REG_VIDEO_SATURATION,
	REG_VIDEO_CONTRAST,
	REG_VIDEO_HUE,

	// Windows
	REG_CLIST_WND_POS,
	REG_INCOMINGCALL_WND_POS,
	REG_LOGIN_WND_POS,
	REG_MAIN_WND_POS,
	REG_MEDIA_WND_POS,
	REG_PEER1_WND_POS,
	REG_PEER2_WND_POS,
	REG_PEER3_WND_POS,
	REG_PEER4_WND_POS,
	REG_PEER5_WND_POS,
	REG_PEER6_WND_POS,
	REG_PEER7_WND_POS,
	REG_PEER8_WND_POS,
	REG_PQP_WND_POS,
	REG_PREVIEW_WND_POS,

	// Display
	REG_TEXT_SIZE,
	REG_DOCKING,
	REG_SHOW_CONTACTS,
	REG_SHOW_ONTOP,
	REG_SHOW_PREVIEW,
	REG_SHOW_STATUS,

	// Skins
	REG_SKIN_DIR,

	// Alerts
	REG_ALERT_SOUND,

	REG_ALERT_FIRST,
	REG_ALERT_LAST = (REG_ALERT_FIRST + 100),

	// Sounds
	REG_SOUND_APP_NAME,

	REG_SOUND_ALIAS_FIRST,
	REG_SOUND_RECEIVE_TEXT = REG_SOUND_ALIAS_FIRST,
	REG_SOUND_USER_ONLINE,
	//REG_SOUND_USER_OFFLINE,
	REG_SOUND_ALIAS_LAST = (REG_SOUND_ALIAS_FIRST + 100),

	// Network
	REG_NETWORK_OPTIONS,
	REG_PEER_PORT,
	REG_SERVICE_ADDRESS,
	REG_SERVICE_PORT,
	REG_CHATROOM_ADDRESS,
	REG_CHATROOM_PORT,
	REG_WEB_ADDRESS,
	REG_CONN_SPEED,
	REG_TRANSFER_PORT,
	REG_FIREWALL_TTL,
	REG_DOMAIN,
	REG_FIREWALL_PORT,
	REG_FIREWALL_SERVER,
	REG_NETWORK_PORT,
	REG_NETWORK_PORT_MIN,
	REG_NETWORK_PORT_MAX,
	REG_NETWORK_PORT_LAST,

	// DebugLog
	REG_LOG_QUALITY_ADAPTATION,
	REG_LOG_AUDIO_RECEIVER,
	REG_LOG_IM_CONNECTION,
	REG_LOG_VS_CONNECTION,
	REG_LOG_HTTP,

	// Debug buffer sizes for Http
	REG_HTTP_SEND_BUFFER_SIZE,
	REG_HTTP_AUDIO_BUFFER_SIZE,
	REG_HTTP_VIDEO_BUFFER_SIZE,

	// Http
	REG_HTTP_PROXY_ADDRESS,
	REG_HTTP_PROXY_PORT,
	REG_DETECT_UDP,
	REG_FORCE_HTTP,
	REG_HTTP_PORT,
	REG_HTTPS_PORT,

	REG_FORCE_P2PREFLECTOR
};

// mask for REG_NETWORK_OPTIONS registry entry
#define NETWORK_DYNAMIC_PORT    1 // when set, use dynamic port
#define NETWORK_SECURE_SOCKET   2 // when set, use secure socket
#define NETWORK_UDP_TEST        4 // when set, enable udp test
#define NETWORK_FIREWALL_SERVER 8 // when set, use gloable firewall server

#if 0 //for linux
// Functions
bool EcInitializeRegistry(
	const tstring& sCompany = "", 
	const tstring& sAppName = "",
	const tstring& sVersion = "");

bool EcGetRegistryEntry(EcRegistryItem regEntrytItem, DWORD& _value);
bool EcGetRegistryEntry(EcRegistryItem regEntrytItem, tstring& _value);

bool EcSetRegistryEntry(EcRegistryItem regEntrytItem, DWORD _value);
bool EcSetRegistryEntry(EcRegistryItem regEntrytItem, const tstring& _value);

bool EcDeleteRegistryEntry(EcRegistryItem regEntrytItem);

// Sound Alias access functions
tstring         EcGetEventName(EcEventType nId);
EcEventType     EcGetEventId(const tstring& _alert);
UINT            EcGetEventFlags(const tstring& _alert);

bool            EcGetSoundRegistryEntry(const tstring& _alert, tstring& _value);
bool            EcSetSoundRegistryEntry(const tstring& _alert, const tstring& _value);
bool            EcGetAlertRegistryEntry(const tstring& _alert, DWORD& _value);
bool            EcSetAlertRegistryEntry(const tstring& _alert, DWORD _value);
#else
#define EcGetRegistryEntry(reg,val) TRUE
#define EcSetRegistryEntry(reg,val) TRUE

#define EcDeleteRegistryEntry(regEntrytItem) TRUE
#define EcGetEventId(_alert)        0
#define EcGetEventName(nid)         tstring("")
#define EcGetEventFlags(_alert)     0
#define EcGetSoundRegistryEntry(_alert,_value) FALSE
#define EcSetSoundRegistryEntry(_alert,_value) FALSE
#define EcGetAlertRegistryEntry(_alert,_value) FALSE
#define EcSetAlertRegistryEntry(_alert,_value) FALSE
#endif
#if 0 //for Linux yamada
// Direct access functions
bool GetRegistryEntry(
	HKEY _hKey, 
	const tstring& _basekey, 
	const tstring& sValueName, 
	DWORD& dwValueData);

bool GetRegistryEntry(
	HKEY _hKey, 
	const tstring& _basekey, 
	const tstring& sValueName, 
	tstring& sValueData);

bool SetRegistryEntry(
	HKEY _hKey, 
	const tstring& _basekey, 
	const tstring& sValueName, 
	DWORD dwValueData);

bool SetRegistryEntry(
	HKEY _hKey, 
	const tstring& _basekey, 
	const tstring& sValueName, 
	const tstring& sValueData);

bool DeleteRegistryEntry(
	HKEY _hKey, 
	const tstring& _basekey, 
	const tstring& sValueName);
#endif //for Linux yamada

#endif // GETRGVAL_H
