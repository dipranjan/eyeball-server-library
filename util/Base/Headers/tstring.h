#ifndef TSTRING_H
#define TSTRING_H


#if 1 //for Linux yamada
#include <string>
#include <iostream>
#else
#ifndef _WIN32_WCE
// STL
#include <string>
#include <iostream>
#endif
#endif //for Linux yamada


using namespace std;

typedef std::string ustring;


// define to allow compile - temporary and dangerous
//#define AUTO_STRING_CONVERT
//#define AUTO_POINTER_CONVERT

class tstring : public ustring
{
public:
	// Constructors
	tstring();
	tstring(const ustring& s);
	tstring(const char* p);

#ifdef AUTO_POINTER_CONVERT
	// conversion operators
	operator LPCTSTR(void) const;
	operator LPTSTR(void);
#endif //AUTO_POINTER_CONVERT

#ifdef AUTO_STRING_CONVERT
	operator std::string(void) const;
#endif //AUTO_STRING_CONVERT

	std::string mbs(void) const;
	tstring ToLower();

#ifndef _WIN32_WCE
	friend ostream& operator<<(ostream& s, const tstring& v);
#endif
};

#endif //TSTRING_H
