// ---------------------------------------------------------------------------
// File:       crypt.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Simple encryption routines, i.e. for writing to registry
// 
// Change Log:
// ---------------------------------------------------------------------------

#ifndef EC_CRYPT_H
#define EC_CRYPT_H

namespace Crypt
{
	std::string simple_crypt(const std::string& _str);
	std::string simple_decrypt(const std::string& _str);
}

#endif // EC_CRYPT_H
