
/********************************************************************
 * 
 * File: GUIProfile.h
 * Author: Eyeball Networks Inc.
 * Copyright: Eyeball Networks Inc. 2001
 *
 * Purpose: GUI Quality Profile/Details
 * 
 * Change Log: 
 *
 *******************************************************************/

#ifndef _GUI_PROFILE_H_
#define _GUI_PROFILE_H_


#define MAXIMUM_LEVEL 40


struct GUIQualityProfile
{
	GUIQualityProfile()
	{
		iCurrentLevel = 0;
		iMaximumLevel = 0;
		iAudioQuality = 0;
		for (int i = 0; i < MAXIMUM_LEVEL; i++)
		{
			aiFrameRate[i] = 0;
			aiQFactor[i] = 0;
		}
	}

	int iCurrentLevel;
	int iMaximumLevel;

	int aiFrameRate[MAXIMUM_LEVEL];
	int aiQFactor[MAXIMUM_LEVEL];
	int iAudioQuality;
};



struct GUIConferenceInfo
{
	GUIConferenceInfo()
	{
		bChatController       = TRUE;     //is chat controller
		bPeerAdaptation       = TRUE;  //is the peer's adaptation enabled or disabled
		bAudioDeviceAvail     = TRUE;  //is audio device available
		bVideoDeviceAvail     = TRUE;  //is video device available
		bIsConnected          = FALSE;  //is in a conference
		bMultiPartyState      = FALSE;  //is in multi party state
		iMemberNumber         = 0;      //how many members in the group chat
	}


	BOOL bAudioDeviceAvail;
	BOOL bVideoDeviceAvail;
	BOOL bChatController;
	BOOL bPeerAdaptation;
	BOOL bIsConnected; 
	BOOL bMultiPartyState;
	int  iMemberNumber;
};


#endif  //_GUI_PROFILE_H_

