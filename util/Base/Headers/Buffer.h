/********************************************************************
 * 
 * File: Buffer.h
 * Author: Eyeball Networks Inc.
 * Copyright: Eyeball Networks Inc. 2001
 *
 * Purpose: Provides the base class Buffer
 * 
 * Change Log: 
 *
 *******************************************************************/

#ifndef EYECONTACT_BUFFER_H
#define EYECONTACT_BUFFER_H

#undef EXPORT_KEY
#define EXPORT_KEY BASE_EXPORT_KEY
#include "export.h"

#include "utypes.h"   //added by shakhawat

#if 0 //for Linux yamada
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif //for Linux yamada

enum BufferType
{
	UNKNOWN_BUFFER = 0,
	VIDEO_CAPTURE_BUFFER,
	VIDEO_ENCODER_BUFFER,
	RTP_MESSAGE_BUFFER,
	VIDEO_DECODER_BUFFER,
	VIDEO_RENDER_BUFFER
};

/**
 * Base class for buffers.  Buffers store data.
 */
class CLASS_DECL_EXPORT Buffer {
public:
	Buffer():m_bufferType(UNKNOWN_BUFFER) {}
	Buffer(BufferType bt):m_bufferType(bt) {}
	virtual ~Buffer(){}
	BufferType GetBufferType();

private:
	BufferType m_bufferType;
};

class FrameBuffer {
public:
	FrameBuffer();
	FrameBuffer(int size);
	virtual ~FrameBuffer();
	const FrameBuffer& operator=(const FrameBuffer& b);
	void DataCopy(BYTE* FrameBuffer, int size);
	void iDataCopy(BYTE* FrameBuffer, int size);
	BYTE* GetBufferPtr() {
		return m_bp;
	}
	void SetBufferSize(int size);
	int GetBufferSize() {return m_size;}

private:	
	int m_size;			// valid data size
	int m_alloc_size;	// allocated buffer size
	BYTE *m_bp;			// buffer pointer	
	void Copy(const FrameBuffer& b);	
	void Free();
};


#endif
