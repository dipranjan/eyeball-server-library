#ifndef DATA_STAT_H
#define DATA_STAT_H

//The following three lines have been added by Farhan
#include <list>
using namespace std;
//using std::list;

struct DataEntry
{
	DataEntry()
	{
		iData = 0;
		dwTime = 0;
	}

	int iData;
	DWORD dwTime;
};

class Mutex;


// this class is used for calculating bitrate and framerate purpose
// rate = m_iData/iPeriod
// m_iData is the accumulated value for a certain period (iPeriod)

class DataStat
{
public:
	DataStat();
	~DataStat();

public:
	void Reset();
	void SetPeriod(int iPeriod);
	void SetUnit(double rUnit);
	void AddData(int iData);
	double GetStat();

private:
	std::list<DataEntry> m_dataQ;
	Mutex*           m_pMutex;
	int              m_iPeriod;
	double           m_rUnit;
	double           m_rStat;
	int              m_iData;
};


struct DataEntry2
{
	DataEntry2()
	{
		dwFailed = 0;
		dwSucceeded = 0;
		dwTime = 0;
	}

	DWORD dwFailed;
	DWORD dwSucceeded;
	DWORD dwTime;
};

// this class is used for calculating rate purpose based on 
// accumulated or instant Failed and Succeeded value.

// iMode = 0
// m_dwFailed: accumulated failed value on the first input parameter 
//             of the method AddDta() in the specified period;
// m_dwSucceeded: accumulated succeeded value on the second input parameter
//             of the method AddData() in the specified period;

// rate = m_dwFailed / (m_dwFailed + m_dwSucceeded) 

// iMode = 1

// Calculating the loss rate -  
// dwFailed2 = value of dwFailed in the last elememt of the queue
// dwFailed1 = value of dwFailed in the first element of the queue
// dwSucceeded2 = value of dwSucceeded in the last element of the queue
// dwSucceeded1 = value of dwSucceeded in the first element of the queue

// rate = (dwFailed2 - dwFailed1) / (dwSucceeded2 - dwSucceeded1 + 
//         dwFailed2 - dwFailed1)


class DataStat2
{
public:
	DataStat2();
	~DataStat2();

public:
	void Reset();
	void SetPeriod(int iPeriod);
	void SetUnit(double rUnit);
	void SetMode(int iMode);
	void AddData(DWORD dwFailed, DWORD dwSuccessed);
	double GetStat();

private:
	void UpdateStatMode0(DWORD dwFailed, DWORD dwSucceeded);
	void UpdateStatMode1(DWORD dwFailed, DWORD dwSucceeded);

private:
	std::list<DataEntry2> m_dataQ;
	Mutex*           m_pMutex;
	int              m_iPeriod;
	double           m_rUnit;
	double           m_rStat;
	DWORD            m_dwFailed;
	DWORD            m_dwSucceeded;
	int              m_iMode;
};


#endif // DATA_STAT_H
