// ---------------------------------------------------------------------------
// File:       Event.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Customized Event
// 
// Change Log:
// ---------------------------------------------------------------------------

#ifndef _EC_EVENT_H_
#define _EC_EVENT_H_

#if 1 //for Linux yamada
#include <queue>
#else
#define _WINSOCKAPI_  // must be defined before windows.h
#include <windows.h>
#endif //for Linux yamada

#include "critsec.h"


#if 1 //for Linux yamada
#define INFINITE ((DWORD)0xFFFFFFFF)
#define WAIT_OBJECT_0 0L
#define WAIT_FAILED ((DWORD)0xFFFFFFFF)
#define WAIT_TIMEOUT  258L 

/*
 CreateEvent
*/
HANDLE CreateEvent(void* EventAttributes, BOOL ManualReset, BOOL InitialState, void* Name);


/*
 CloseEvent(HANDLE hEvent);
*/
BOOL CloseEvent(HANDLE hEvent);


/*
 SetEvent(HANDLE hEvent);
*/
BOOL SetEvent(HANDLE hEvent);


/*
 ResetEvent
*/
BOOL ResetEvent(HANDLE hEvent);


/*
 WaitForSingleObject 
*/
DWORD WaitForSingleObject(HANDLE hHandle, DWORD dwMilliseconds);


/*
 WaitForMultipleObjects 
*/
DWORD WaitForMultipleObjects(DWORD  nCount, HANDLE *pHandles, BOOL   bWaitAll, DWORD  dwMilliseconds);

#endif //for Linux yamada



// Wrapper for a handle to an event.
class SimpleEvent : noncopyable
{
public:
	SimpleEvent() 
	: 
		m_hEvent(NULL) 
	{
	}
	
	virtual ~SimpleEvent() 
	{ 
		if (NULL != m_hEvent)
		{
#if 1 //for Linux yamada
			CloseEvent(m_hEvent);
#else
			CloseHandle(m_hEvent); 
#endif //for Linux 		
			m_hEvent = NULL;
		}
	}

	virtual HANDLE GetHandle(void) const
	{
		return m_hEvent;
	}

	virtual void SetHandle(HANDLE h)
	{
		if (NULL != m_hEvent)
#if 1 //for Linux yamada
		{
			CloseEvent(m_hEvent);
			m_hEvent = NULL;
		}

		/*(void *)*/m_hEvent = h; //void * commented out by Farhan
#else
		{
			CloseHandle(m_hEvent);
			m_hEvent = NULL;
		}
		m_hEvent = h;
#endif //for Linux 
		
	}

protected:
	HANDLE m_hEvent;
};

// Thread-aware event-queue implementation.
template<class T> class Event : public SimpleEvent
{
public:
	Event(
#if 1 //for Linux yamada
		void *lpEventAttributes = NULL,
		BOOL bManualReset = FALSE, 
		BOOL bInitialState = FALSE,
		void *lpName = NULL);
#else
		LPSECURITY_ATTRIBUTES lpEventAttributes = NULL,
		BOOL bManualReset = FALSE, 
		BOOL bInitialState = FALSE,
		LPCTSTR lpName = NULL);
#endif //for linux yamada
	virtual ~Event(void);

	virtual void SetEvent(const T& var); // push event
	virtual BOOL ResetEvent(void); // resets the event and clears the queue

	virtual BOOL GetVar(T& var); // pop event

	virtual BOOL Empty(void); // returns TRUE iff queue is empty
	virtual int Size(void); // returns size of queue

protected:
	std::queue<T> m_varQueue;
	Mutex m_accessMutex;
};


// ---------------------------------------------------------------------------
// Implementation
// ---------------------------------------------------------------------------

template<class T> 
Event<T>::Event(
#if 1 //for Linux yamada
	void *lpEventAttributes,
	BOOL bManualReset, 
	BOOL bInitialState,
	void *lpName)
#else
	LPSECURITY_ATTRIBUTES lpEventAttributes,
	BOOL bManualReset, 
	BOOL bInitialState,
	LPCTSTR lpName)
#endif //for Linux yamada
:
	m_varQueue(),
	m_accessMutex()
{
	m_hEvent = CreateEvent(
		lpEventAttributes,
		bManualReset,
		bInitialState,
		lpName);
}

template<class T> 
Event<T>::~Event(void)
{
}

template<class T> 
void Event<T>::SetEvent(const T& var)
{
	{
		Critical_Section cs(m_accessMutex);
		m_varQueue.push(var);
	}

	// Leave the critical section before setting the event, else the signaled
	// thread is often blocked entering the critical section.
	::SetEvent(m_hEvent);
}

template<class T> 
BOOL Event<T>::ResetEvent(void)
{
	Critical_Section cs(m_accessMutex);

	while(!Empty())
		m_varQueue.pop();

	return ::ResetEvent(m_hEvent);
}

template<class T> 
BOOL Event<T>::GetVar(T& var)
{
	BOOL bEmpty = false;

	{
		Critical_Section cs(m_accessMutex);

		if (Empty())
			return FALSE;

		var = m_varQueue.front();
		m_varQueue.pop();

		bEmpty = Empty();
	}

	if (!bEmpty)
	{
		::SetEvent(m_hEvent);
	}

	return TRUE;
}

template<class T> 
BOOL Event<T>::Empty(void)
{
	Critical_Section cs(m_accessMutex);
		
	return m_varQueue.empty();
}

template<class T> 
int Event<T>::Size(void)
{
	Critical_Section cs(m_accessMutex);
		
	return m_varQueue.size();
}


#endif  //_EC_EVENT_H_
