// ---------------------------------------------------------------------------
// File:       UserStates.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Author:     Jozsef Vass
//
// Purpose:    Eyeball Chat user states
//
// Change Log: 07/19/2001 - Created
// ---------------------------------------------------------------------------

#ifndef __USER_STATES__
#define __USER_STATES__

// Note: these values must match those used by ChatMessage.h

// Client states
#define ECS_USER_STATE_NOT_FOUND    128
#define ECS_USER_STATE_OFFLINE      129
#define ECS_USER_STATE_ONLINE       130
#define ECS_USER_STATE_BUSY         131
#define ECS_USER_STATE_NO_RESPONSE  132
#define ECS_USER_STATE_PENDING      137

// User states (user modes)
#define ECS_USER_STATE_AVAILABLE    256
#define ECS_USER_STATE_AWAY         257
#define ECS_USER_STATE_DND          258


// ---------------------------------------------------------------------------
// Helper macros for contact states.
// ---------------------------------------------------------------------------

#define IS_CONTACT_STATE(X) \
	(((X) == ECS_USER_STATE_OFFLINE) \
		|| ((X) == ECS_USER_STATE_ONLINE) \
		|| ((X) == ECS_USER_STATE_BUSY))

#define IS_USER_MODE(X) \
	(((X) == ECS_USER_STATE_AVAILABLE) \
		|| ((X) == ECS_USER_STATE_AWAY) \
		|| ((X) == ECS_USER_STATE_DND))

#define IS_CONTACT_ONLINE(X) \
	(((X) == ECS_USER_STATE_ONLINE) \
		|| ((X) == ECS_USER_STATE_BUSY))

#define IS_CONTACT_OFFLINE(X) \
	((X) == ECS_USER_STATE_OFFLINE)


#endif // __USER_STATES__
