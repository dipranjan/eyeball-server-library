// ---------------------------------------------------------------------------
// File:       EAtlUtil.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Helper functions specific to ATL
//
// Change Log:
// ---------------------------------------------------------------------------

#ifndef _EC_ATL_UTIL_H_
#define _EC_ATL_UTIL_H_

#include <atlbase.h>

// ---------------------------------------------------------------------------
// Helper Templates
// ---------------------------------------------------------------------------

// Use this just to make the Lock/Unlock members virtual
class CComEyeballModule : public CComModule
{
public:
    virtual LONG Lock() { return CComModule::Lock(); }
	virtual LONG Unlock() { return CComModule::Unlock(); }
};

#endif // _EC_ATL_UTIL_H_

