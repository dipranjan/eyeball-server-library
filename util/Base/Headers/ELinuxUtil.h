// ---------------------------------------------------------------------------
// File:       ELinuxUtil.h
//
// Copyright:  ITAccess Inc. 
//
// Purpose:    Helper functions specific to Linux
//
// Change Log:
// ---------------------------------------------------------------------------

//注：Base/EWinUtil.cの代わりとなるBase/ELinuxUtil.cを新規に作成する事

#ifndef _EC_LINUX_UTIL_H_
#define _EC_LINUX_UTIL_H_


// ---------------------------------------------------------------------------
// Utility functions
// ---------------------------------------------------------------------------

namespace eyeball
{
	DWORD CustomWaitForSingleObject(
		HANDLE hHandle, 
		DWORD dwMilliseconds);
}

#endif // _EC_LINUX_UTIL_H_

