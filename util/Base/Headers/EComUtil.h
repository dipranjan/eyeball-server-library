// ---------------------------------------------------------------------------
// File:       EComUtil.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    ATL / COM Utilities
// 
// Change Log:
// ---------------------------------------------------------------------------

#ifndef EC_COM_UTIL_H_
#define EC_COM_UTIL_H_

#include "atlbase.h"
#include "ComSafeArray.h"
#include "tstring.h"

// Declares a standard property
#define STDPROPERTY(X, Y) \
	STDMETHOD(get_##X)(Y *pVal); \
	STDMETHOD(put_##X)(Y newVal);

// Implements string properties
// Example usage:
//     IMPLEMENT_BSTR_PROPERTY(CChatCtl, EyeballID)
#define IMPLEMENT_BSTR_PROPERTY(X, Y) \
	STDMETHODIMP X::get_##Y(BSTR* pVal) \
	{ \
		return eyeball::CopyBstr(pVal, m_bstr##Y); \
	} \
	STDMETHODIMP X::put_##Y(BSTR newVal) \
	{ \
		m_bstr##Y = newVal; \
		return S_OK; \
	}

// Handle failed COM calls
#define RET_IF_FAILED(X, Y) \
	if (FAILED((X))) \
		return Error(_T((Y)));

// Check for null pointers
#define CHECK_PTR(X) \
	{ if (NULL == (X)) return E_POINTER; }

// Automatically calls CoInitialize() and CoUninitialize()
class CoInit
{
public:
	CoInit();
	virtual ~CoInit();

private:
	bool m_bOk;
	DWORD m_dwThreadId;
};


// ---------------------------------------------------------------------------
// ActiveX Control Helper Functions
// ---------------------------------------------------------------------------

namespace eyeball
{
	/* Returns the URL of the given client site.
	 *
	 * The IOleClientSite pointer can be obtained by overriding the 
	 * following function of the IOleObject interface:
	 *
	 * STDMETHOD(SetClientSite)(IOleClientSite *pClientSite);
	 */
	CComBSTR GetUrlFromClientSite(IOleClientSite* pClientSite);

	// BSTR helper functions
	void StringFromBstr(tstring& sOut, const BSTR bstrIn);
	void StringFromBstr(std::string& sOut, const BSTR bstrIn);
	const tstring ToString(const BSTR bstr);
	const std::string ToStringA(const BSTR bstr);
	int BstrFromString(BSTR* pbstrOut, const tstring& sIn);
	int BstrFromString(CComBSTR& bstrOut, const tstring& sIn);
	const CComBSTR ToBstr(const tstring& sIn);
	HRESULT CopyBstr(BSTR* pbstrOut, const BSTR bstrIn);

	VARIANT_BOOL ToVB(BOOL b);
	VARIANT_BOOL ToVB(bool b);
	BOOL ToBOOL(VARIANT_BOOL b);
	bool Tobool(VARIANT_BOOL b);

	bool CheckActiveXControl(
		const tstring& sProgId,
		const tstring& sFileName);
}

/**
 * Simplifies CComSafeArray.
 * While CComSafeArray is a flexible third party class, in most cases we
 * do not need all that flexibility.  CustomSafeArray provides a simpler
 * interface, exposing only the most commonly required features.
 */
class CustomSafeArray
{
public:
	CustomSafeArray() {}
	virtual ~CustomSafeArray() {}

	void Attach(VARIANT* pVar)
	{
		if (NULL == pVar)
			return;

		if (!(pVar->vt & VT_ARRAY))
			return;

		m_sa.Attach(*pVar);
	}

	void Attach(SAFEARRAY* psa)
	{
		CComVariant var;
		var.vt = VT_ARRAY | VT_VARIANT;
		var.parray = psa;
		m_sa.Attach(var);
	}

	void Detach(VARIANT* pVar)
	{
		m_sa.Detach(pVar);
	}

	void Detach(void)
	{
		m_sa.Detach();
	}

	void Detach(SAFEARRAY** ppsa)
	{
		m_sa.Detach(ppsa);
	}

	DWORD GetSize(void)
	{
		return m_sa.GetSize();
	}

	void GetElement(long nIndex, void* pvData)
	{
		m_sa.GetElement(nIndex, pvData);
	}

	void PutElement(long nIndex, void* pvData)
	{
		m_sa.PutElement(nIndex, pvData);
	}

	SAFEARRAY* GetSafeArrayPtr(void) { return m_sa.parray; }
	VARIANT& GetVariantRef(void) { return m_sa; }

protected:
	CComSafeArray m_sa;
};

// Note: this class encapsulates a safe-array of string variants
class BstrArray : public CustomSafeArray
{
public:
	BstrArray(DWORD nSize = 0)
	{
		m_sa.CreateOneDim(VT_VARIANT, nSize);
	}

	CComBSTR GetBstr(DWORD nIndex)
	{
		CComVariant var;
		GetElement(nIndex, &var);
		return CComBSTR(var.bstrVal);
	}

	void PutBstr(DWORD nIndex, CComBSTR& bstrVal)
	{
		CComVariant var(bstrVal);
		PutElement(nIndex, &var);
	}

	tstring GetString(DWORD nIndex)
	{
		return eyeball::ToString(GetBstr(nIndex));
	}

	void PutString(DWORD nIndex, const tstring& sVal)
	{
		CComBSTR bstr(sVal.c_str());
		PutBstr(nIndex, bstr);
	}
};

class GeneralSafeArray : public BstrArray
{
public:
	GeneralSafeArray(DWORD nSize = 0):
	  BstrArray(nSize)
	{
	}

	void PutNumber(DWORD nIndex, long nValue)
	{
		CComVariant var(nValue);
		PutElement(nIndex, &var);
	}

	void PutBool(DWORD nIndex, BOOL bValue)
	{
		bool b = bValue == TRUE;
		CComVariant var(b);
		PutElement(nIndex, &var);
	}
	long GetNumber(DWORD nIndex)
	{
		CComVariant var;
		GetElement(nIndex, &var);
		return var.lVal;
	}

	BOOL GetBool(DWORD nIndex)
	{
		CComVariant var;
		GetElement(nIndex, &var);
		return var.boolVal;
	}
};




class ByteArray : public CustomSafeArray
{
public:
	ByteArray()
	{
	}

	// this is a byte array - do not use tstring
	void SetData(const std::string& sBuf)
	{
		SetData(sBuf.data(), sBuf.size());
	}

	void SetData(const char* pBuf, int iLen)
	{
		m_sa.CreateOneDim(VT_UI1, iLen);
		if (0 == iLen)
			return;
		
		void* pData = NULL;
		m_sa.AccessData(&pData);
		if (NULL != pData)
		{
			memcpy(pData, pBuf, iLen);
		}
		m_sa.UnaccessData();
	}

	void GetData(std::string& sBuf)
	{
		if (0 == m_sa.GetSize())
			return;

		void* pData = NULL;
		m_sa.AccessData(&pData);
		
		sBuf.assign((char*)pData, m_sa.GetSize());
		
		m_sa.UnaccessData();
	}

	void GetData(SharedArray<char>& pBuf, unsigned int& nSize)
	{
		if (0 == m_sa.GetSize())
			return;

		void* pData = NULL;
		m_sa.AccessData(&pData);
		
		nSize = m_sa.GetSize();
		pBuf.reset(new char[nSize]);
		memcpy(&pBuf[0], pData, nSize);

		m_sa.UnaccessData();
	}
	
	void Attach(SAFEARRAY* psa)
	{
		CComVariant var;
		var.vt = VT_ARRAY | VT_UI1;
		var.parray = psa;
		m_sa.Attach(var);
	}
};

#endif // EC_COM_UTIL_H_
