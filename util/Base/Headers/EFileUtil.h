// ---------------------------------------------------------------------------
// File:       EFileUtil.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Helper functions for handling files, modules, and directories
//
// Change Log:
// ---------------------------------------------------------------------------

#ifndef _EFILEUTIL_H_
#define _EFILEUTIL_H_
#include "Base.h"
#include "tstring.h"


// ---------------------------------------------------------------------------
// Required libraries
// ---------------------------------------------------------------------------

#ifndef _WIN32_WCE
#ifndef _EC_NO_VERSION
#pragma comment(lib, "version.lib")
#endif // _EC_NO_VERSION
#endif

// ---------------------------------------------------------------------------
// Functions
// ---------------------------------------------------------------------------

bool GetVersionOfFile (const tstring& sFilename, tstring& sVersion);
tstring GetVersionOfCurrent(void);

// Combines a directory and file name into a full path name.
tstring GetPathName(
	const tstring& sDirName, 
	const tstring& sFileName);

// Breaks up a full path name into the directory and file name.
tstring GetDirName(const tstring& sPathName);
tstring GetFileName(const tstring& sPathName);
tstring GetFileExtension(const tstring& sPathName);
DWORD GetFileSize(const tstring& sFileName);
DWORD GetFileSize(const HANDLE hFile);
HANDLE GetFileHandle(const tstring& sFileName);
tstring GetModuleDir(TECInstancePtr hInst);

/**
 * Creates all directories in the specified directory path.
 * If the directories already exist, the function succeeds.
 *
 * @param sDirName  directory name, with or without a trailing backslash
 *
 * @return true on success, false on failure
 */
bool CreateAllDirectories(const tstring& sDirName);

bool DirectoryExists(const tstring& sDirName);
bool FileExists(const tstring& sPathName);

/**
 * Checks if the file is executable, based on the extension.
 * Extensions considered executables: exe, com, bat, pif, vbs
 *
 * @return true if it matches an executable extension
 */
bool IsExecutable(const tstring& sPathName);

/**
* Checks if the image or video clip format is supported as of Internet Explorer 5
*/
bool IsSupportedImageFormat(const tstring& sFileName);

tstring GenerateUniquePathName(const tstring& sPathName);

/**
 * Retrieves the path of the directory designated for temporary Internet
 * files.  If this path cannot be found, the GetTempPath API is used instead.
 *
 * @return directory name
 */
tstring GetTempInternetDir(void);

/**
 * Retrieves the path of the directory designated for temporary files.
 *
 * @return directory name
 */
tstring GetTempDir(void);

#endif // _EFILEUTIL_H_
