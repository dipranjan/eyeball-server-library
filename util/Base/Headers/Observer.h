// ---------------------------------------------------------------------------
// File:       Observer.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Classes for Observer Design Pattern
//
// Change Log:
// ---------------------------------------------------------------------------

#ifndef _OBSERVER_H_
#define _OBSERVER_H_

#if 1 //for Linux yamada
class Subject;

// SubjectHandles are pointers used as ID's.  It is unsafe to deference them.
typedef Subject* SubjectHandle;

/**
 * Observer class
 */
class Observer : noncopyable
{
public:
	virtual ~Observer() {}
	virtual void Update(SubjectHandle hChangedSubject) = 0;
};

#if 0
class WindowObserver : public Observer
{
public:
	WindowObserver()
	:
		m_hWnd(NULL),
		m_uMsg(0),
		m_wParam(0)
	{
	}

	virtual void Init(HWND hWnd, UINT uMsg, WPARAM wParam = 0)
	{
		m_hWnd = hWnd;
		m_uMsg = uMsg;
		m_wParam = wParam;
	}

	virtual void Update(SubjectHandle hChangedSubject)
	{
		PostMessage(m_hWnd, m_uMsg, m_wParam, (LPARAM)hChangedSubject);
	}

protected:
	HWND m_hWnd;
	UINT m_uMsg;
	WPARAM m_wParam;
};
#endif

/**
 * Subject class
 */ 
typedef std::list<Observer*> ObserverList;

class Subject : noncopyable
{
public:
	virtual ~Subject() {}

	virtual void Attach(Observer* po) { m_Observers.push_back(po); }
	
	virtual void Detach(Observer* po) { m_Observers.remove(po); }
    virtual void DetachAll(void) { m_Observers.clear(); }

	virtual void Notify(void)
	{
		ObserverList::iterator iter = m_Observers.begin();
		for (; iter != m_Observers.end(); iter++) 
		{
			(*iter)->Update(this);
		}
	}

	virtual SubjectHandle GetHandle(void) { return this; }
	
private:
	ObserverList m_Observers;
};
#else
class Subject;

// SubjectHandles are pointers used as ID's.  It is unsafe to deference them.
typedef Subject* SubjectHandle;

/**
 * Observer class
 */
class Observer : noncopyable
{
public:
	virtual ~Observer() {}
	virtual void Update(SubjectHandle hChangedSubject) = 0;
};

class WindowObserver : public Observer
{
public:
	WindowObserver()
	:
		m_hWnd(NULL),
		m_uMsg(0),
		m_wParam(0)
	{
	}

	virtual void Init(HWND hWnd, UINT uMsg, WPARAM wParam = 0)
	{
		m_hWnd = hWnd;
		m_uMsg = uMsg;
		m_wParam = wParam;
	}

	virtual void Update(SubjectHandle hChangedSubject)
	{
		PostMessage(m_hWnd, m_uMsg, m_wParam, (LPARAM)hChangedSubject);
	}

protected:
	HWND m_hWnd;
	UINT m_uMsg;
	WPARAM m_wParam;
};

/**
 * Subject class
 */ 
typedef std::list<Observer*> ObserverList;

class Subject : noncopyable
{
public:
	virtual ~Subject() {}

	virtual void Attach(Observer* po) { m_Observers.push_back(po); }
	
	virtual void Detach(Observer* po) { m_Observers.remove(po); }
    virtual void DetachAll(void) { m_Observers.clear(); }

	virtual void Notify(void)
	{
		ObserverList::iterator iter = m_Observers.begin();
		for (; iter != m_Observers.end(); iter++) 
		{
			(*iter)->Update(this);
		}
	}

	virtual SubjectHandle GetHandle(void) { return this; }
	
private:
	ObserverList m_Observers;
};
#endif //for Linux yamada

#endif // _OBSERVER_H_
