// randomizing_crypt.h
//
// Intended for randomizing encryption that is hard to guess
// Security by obscurity
// Not intended for highly secure applications, but rather
//   to waste a little more time of potential hackers
// Can be reverse engineered
// No key required
// Algorithm must be kept secret
//
// Zion Kwok
// Eyeball Networks
//
// May 4, 2006
//////////////////////////////////////////////////////////////////

#ifndef RANDOMIZING_CRYPT_H_
#define RANDOMIZING_CRYPT_H_

#include <string>
using std::string;

string randomizing_encrypt(const string& sInput);
string randomizing_decrypt(const string& sInput);

#endif //RANDOMIZING_CRYPT_H_
