// ---------------------------------------------------------------------------
// File:       poll.cc
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Polling assist functions for large servers
//
// Change Log: 10/25/2001 - Added maximum used poll size
// ---------------------------------------------------------------------------

#include <sys/types.h>
#include <sys/socket.h>			// shutdown
#include <unistd.h>				// close

#include "poll.h"

#include "file.h"				// File::set_max_fds()
#include "util.h"

PollArray::PollArray() :
	m_iMaxUsedFd(0)
{
	//DB_AND_LOG_STAT(m_iMaxFds);

	// this set the current limit equal to max, theoretical limit
	// also fixes a linux bug for poll 
	m_iMaxFds = File::set_max_fds();

	// open as much file as possible to double check
	m_iMaxFds = File::get_max_files();

	// allocate poll array
	m_aPollFds = new pollfd[m_iMaxFds];

	open();
}

PollArray::~PollArray()
{
	close();

	DELETE_ARRAY(m_aPollFds);
}

void PollArray::open(void)
{
	// Initialize poll array
	for (int i=0; i < m_iMaxFds; i++)
	{
		pollfd& thisPollFd = m_aPollFds[i];

		thisPollFd.fd = -1;
		thisPollFd.events = 0;
		thisPollFd.revents = 0;
	}
}

void PollArray::close(void)
{
	for (int i=0; i < m_iMaxFds; i++)
	{
		pollfd& thisPollFd = m_aPollFds[i];

		if (thisPollFd.fd != -1)
		{
			// Assume it's a socket
			CLOSE_SOCKET(thisPollFd.fd);

			thisPollFd.fd = -1;
		}

		thisPollFd.events = 0;
		thisPollFd.revents = 0;
	}
}

int PollArray::add(int _iFd, short _iEvents)
{
	if( _iFd < 0 || _iFd >= m_iMaxFds )
	{
		printf("add poll problem: %i\n", _iFd);
		return -1;
	}

	pollfd& thisPollFd = m_aPollFds[_iFd];

   if( thisPollFd.fd != -1 )
      return -1;

	thisPollFd.fd      = _iFd;
	thisPollFd.events  = _iEvents;
	thisPollFd.revents = 0;

	if( _iFd > m_iMaxUsedFd )
		m_iMaxUsedFd = _iFd;

	return 0;
}

int PollArray::modify(int _iFd, short _iEvents)
{
	if( _iFd < 0 || _iFd >= m_iMaxFds )
	{
		printf("add poll problem: %i\n", _iFd);
		return -1;
	}

	pollfd& thisPollFd = m_aPollFds[_iFd];

	if( thisPollFd.fd != _iFd )
	{
		printf("modify poll problem: %i\n", _iFd);
		return -1;
	}

	thisPollFd.events  = _iEvents;
	thisPollFd.revents = 0;

	if( _iFd > m_iMaxUsedFd )
		m_iMaxUsedFd = _iFd;

	return 0;
}

int PollArray::remove(int _iFd)
{
	if( _iFd < 0 || _iFd >= m_iMaxFds )
	{
		printf("remove poll problem: %i\n", _iFd);
		return -1;
	}

	pollfd& thisPollFd = m_aPollFds[_iFd];

   if( thisPollFd.fd == -1 )
      return -1;

	thisPollFd.fd      = -1;
	thisPollFd.events  = 0;
	thisPollFd.revents = 0;

	if( _iFd == m_iMaxUsedFd )
	{
		// remove the largest file descriptor, wee need to rescan

		int fdSearch = m_iMaxUsedFd;
		for( ; fdSearch; fdSearch-- )
		{
			if( m_aPollFds[fdSearch].fd != -1 )
				break;
		}

		m_iMaxUsedFd = fdSearch;
	}

	return 0;
}

ostream& operator<<(ostream& s, const PollArray& v)
{
	s << "[ ";

	for (int i=0; i < v.m_iMaxFds; i++)
	{
		if (v.m_aPollFds[i].fd != -1)
			s << i << " ";
	}

	s << "]";

	return s;
}

