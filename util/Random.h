#if !defined(_Random_h)
#define _Random_h

//#include "Mutex.h"
#include "critsec.h"
#include "rand.h"

class Random
{
 public:
  /**
   * Returns the next number in the random series.  0 <= the return
   * < max.  All callers to Random use the same series (do not
   * instantiate this object).  The seed of the sequence is based on
   * the gettimeofday().tv_usec of the first call to getNextRandom().  
   * This is only 1 million different values rather than the 2^256 
   * values the generator supports, far less than the 52!~=2^225.  
   * The function is thread-safe.
   *
   * This function assumes that:
   *   if a series is pseudo-random then removing all the numbers above
   *   some limit produces a pseudo-random series.
   * I don't have great evidence for this.  I think it's what Numerical
   * Recipes in C calls the rejection method (Section 7.3), but they 
   * get too far into the math for me.
   * The function might also assume (but I don't think so) that:
   *   if a series is psuedo-random then the low-order bits of the
   *   series is pseudo-random.
   * I'm pretty sure this is dependant on the generator.  NRinC says
   * to use the high-order bits in a linear congruential generator (which
   * is not what isaac is).
   *
   * The function works by finding the highest multiple of max that is
   * less than the maximum the generator can produce (1<<sizeof(ub4)-1).
   * If the random number from the generator is more than this number
   * then it is discarded.  Otherwise, I use a traditional mod to map
   * the random range into 0..max.  There is no bias towards the lower
   * numbers.
   */
  static unsigned long getNextRandom(unsigned long max);


 protected:
  /**
   * Seeds the generator.  Not thread-safe (grab lock before calling this).
   */
  static void init();

  /**
   * False until init() is called.  Set in init().
   */
  static bool didInit;


  static randctx randomContext;


  static Mutex lock;
};

#endif
