// checkip.h

#ifndef CHECK_IP_H_
#define CHECK_IP_H_

#include <string>
#include <stdio.h>
using std::string;

//Use this function
extern bool isIPAddress(const string& sIP);

//Calls for ddd.ddd.ddd.ddd notation
extern bool isIPv4Address(const string& sIP);
extern bool isIPv6Address(const string& sIP);

extern bool isValidPort(unsigned int x);
extern bool isValidPort(const std::string& x);
extern std::string retriveAmazaonRealIPv4();

#endif //CHECK_IP_H_

