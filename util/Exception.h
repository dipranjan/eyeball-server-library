#if !defined(_Exception_h)
#define _Exception_h

#include <assert.h>
#include <stdarg.h>
#include <stdio.h>

/// A generic exception class.
/**
 * Intended to be derived.
 */
class Exception
{
        public:
                /// Constructor
                Exception() { details[0] = 0; }

                /*
                Exception(const char* newDetails, ...)
                {
                        va_list ap;
                        va_start(ap, newDetails);
                        vsnprintf(details, 500, newDetails, ap);
                        va_end(ap);
                }
                */
                /// Function to set detailed description
                /**
                 * Takes variable length argument list.
                 * \sa GetDetails
                 */
                void SetDetails(const char* p_NewDetails, va_list ap)
                {
                        vsnprintf(details, 500, p_NewDetails, ap);
                }
                /// Function to get the detailed description
                /**
                 * \return a const char* to the detailed description.
                 * \sa SetDetails
                 */
                const char* GetDetails() { return details; }

        protected:
                /// A character array which holds the detailed description.
                char details[500];
};

#endif
