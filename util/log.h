// $Id: log.h,v 1.11 2006/07/27 00:11:05 zionk Exp $

#ifndef AS_LOG_H
#define AS_LOG_H

#include <iostream>
#include <sstream>
#include <string>
#include "IntMT.h"

#include <syslog.h>

#include <pthread.h>


using namespace std;

extern bool g_bUseSyslog;
extern CIntMT g_iVerboseLevel;
extern pthread_mutex_t logMutex;

#define LOG_OPEN(X) \
        Log::open(X)

#define DEFAULT_MAX_LOG_FILE_SIZE 10000000 //10 MB
#define DEFAULT_MAX_LOG_FILE_COUNT 100     


#define LOG_MAX_FILE_SIZE(X) \
        {\
	  if (!g_bUseSyslog) {\
	    pthread_mutex_lock(&logMutex);   \
	    Log::m_ui32MaxFileSize = (X);    \
	    pthread_mutex_unlock(&logMutex); \
	  }\
	}

//Must be between 1 and 9999999
#define LOG_MAX_FILE_COUNT(X) \
        {\
	  if (!g_bUseSyslog) {\
            pthread_mutex_lock(&logMutex); \
            Log::m_ui32MaxFileCount = (X);		    \
            if (Log::m_ui32MaxFileCount < 1) {              \
              Log::m_ui32MaxFileCount = 1;                  \
            }						    \
            else if (Log::m_ui32MaxFileCount > 9999999) {   \
              Log::m_ui32MaxFileCount = 9999999;	    \
            }						    \
            pthread_mutex_unlock(&logMutex);		    \
	  }\
	}

#define LOG(X)					\
  {						\
    bool bLoggedMessage = false;		\
    if (Log::log_out&&!g_bUseSyslog)				\
      {									\
	pthread_mutex_lock(&logMutex);					\
	*Log::log_out << Log::localtime() << " " << pthread_self() << " " << X << endl; \
	bLoggedMessage = true;						\
	if (bLoggedMessage) {						\
	  string sFilename = Log::autorotate();				\
	  if (sFilename != "") {					\
	    Log::rotate(sFilename);					\
	  }								\
	}								\
   	pthread_mutex_unlock(&logMutex);				\
      }									\
    if (g_bUseSyslog)							\
      {									\
	ostringstream ostr;					\
	ostr << Log::localtime() << " " << pthread_self() << " " << X << endl; \
	syslog(LOG_USER|LOG_NOTICE,"%s",ostr.str().c_str());		\
      }									\
  }

#define LOGL(Y,X)							\
  {									\
    bool bLoggedMessage = false;					\
    if (g_iVerboseLevel >= Y) {						\
      if (Log::log_out&&!g_bUseSyslog)					\
	{								\
	  pthread_mutex_lock(&logMutex);				\
	  *Log::log_out << Log::localtime() << " " << pthread_self() << " " << "[" << Y << "] " << X << endl; \
	  bLoggedMessage = true;					\
       if (bLoggedMessage) {						\
	    string sFilename = Log::autorotate();			\
	    if (sFilename != "") {					\
	      Log::rotate(sFilename);					\
	    }								\
	  }								\
  	  pthread_mutex_unlock(&logMutex);				\    
	}								\
      if (g_bUseSyslog)							\
	{								\
	  ostringstream ostr;						\
	  ostr << Log::localtime() << " " << pthread_self() << " " << X << endl; \
									\
	  syslog(LOG_USER|LOG_NOTICE,"%s",ostr.str().c_str());		\
	}								\
    }									\
  }

#define LOG_CLOSE() \
        Log::close()

#define DB_AND_LOG(X) \
        { DB(X); LOG(X); }

#define DB_AND_LOGL(Y,X) \
        { DB(X); LOGL(Y,X); }

#define DB_AND_LOG_STAT(X) \
        DB_AND_LOG(#X << " = " << X)

#define DB_AND_LOGL_STAT(Y,X) \
        DB_AND_LOGL(Y,#X << " = " << X)

#define LOG_STAT(X) \
        LOG(#X << " = " << X)

#define LOGL_STAT(Y,X) \
        LOGL(Y,#X << " = " << X)

/// The Log class
/**
 * All the members are static, so that they can be called from and they only have one instance.
 */
class Log
{
 public:
  
  /// The logfile stream.
  static ostream *log_out;

  /// Function to open logfile.
  /**
   * \param _filename a reference to std::string containing the filename.
   * \sa close
   */
  static bool open(const string& _filename);

  /// Function to close logfile.
  /**
   * \sa open
   */
  static void close(void);

  /// Generates logfile name.
  /**
   * Generates logfile name using pid.
   * \param _name a reference to std::string containing the prefix.
   * \return a std::string containing logfile name.
   */
  static string generate_filename(const string& _name = "output");

  /// Function to get localtime.
  /**
   * \return a std::string containing the localtime as mm/dd/yyyy HH:MM:SS.
   */
  static string localtime(void);

  /// rotate logfile, append number to filename
  /**
   * \return true if successful, false otherwise
   */
  static bool rotate(const string& _filename);

  /// auto rotate logfile, append number to filename
  /**
   * \return true if successful, false otherwise
   */
  static string autorotate();

  static unsigned int m_ui32MaxFileSize;
  static unsigned int m_ui32MaxFileCount;

private:
  static string m_sFilename;
  static int m_iAutoRotateCount;

};

#endif // AS_LOG_H


/*
  $Log: log.h,v $
  Revision 1.11  2006/07/27 00:11:05  zionk
  Set g_iVerboseLevel to CIntMT instead of int

  Revision 1.10  2006/07/20 22:26:54  zionk
  Allow number of log files to be configurable

  Revision 1.9  2006/05/17 00:33:15  zionk
  Fix autorotate so it doesn't deadlock

  Revision 1.8  2006/05/16 19:57:50  zionk
  Maximum log file size (before rotation) is configurable

  Revision 1.7  2006/05/16 18:06:43  zionk
  Added log auto-rotate

  Revision 1.6  2006/03/07 18:14:24  zionk
  Add bool return value to Log::open

  Revision 1.5  2005/10/11 23:29:29  larsb
  added thread-safe version of hashtable

  Revision 1.4  2005/08/19 21:10:19  larsb
  *** empty log message ***

  Revision 1.3  2005/08/18 18:10:10  larsb
  added int g_iVerboseLevel to log.h/log.cc

  Revision 1.2  2005/08/16 21:08:50  larsb
  added function to rotate log files

*/

