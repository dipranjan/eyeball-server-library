//Encrypt and decrypt password

#include <stdio.h>
#include "BinHex.h"
#include <string.h>
#include "Password3DES.h"


const char szUsage[] = 
    "Usage: pass3des <key1-hex> <key2-hex> <key3-hex> <username> <password> [decrypt]\n"
    "  OR   pass3des <three-keys-hex> <username> <password> [decrypt]\n";

int main(int argc, char** argv)
{
  if (argc < 4) {
    printf("Not enough arguments, only %d\n", argc);
    printf(szUsage);
    return -1;
  }

  bool bEncrypt = (argc == 4) || (argc == 6);
  bool bThreeKeys = (argc >= 6);

  //Check keys
  if (bThreeKeys) {
    unsigned char binKey[8];
    if (!check_3des_key_hex_and_parity(argv[1], binKey)) {
      printf("key1-hex %s is bad key\n", argv[1]);
      printf(szUsage);
      return -2;
    }

    if (!check_3des_key_hex_and_parity(argv[2], binKey)) {
      printf("key2-hex %s is bad key\n", argv[2]);
      printf(szUsage);
      return -3;
    }

    if (!check_3des_key_hex_and_parity(argv[3], binKey)) {
      printf("key3-hex %s is bad key\n", argv[3]);
      printf(szUsage);
      return -4;
    }
  }
  else {
    if (strlen(argv[1]) != 48) {
      printf("three-keys-hex %s is bad key\n", argv[1]);
      printf(szUsage);
      return -5;
    }
  }

  if (bEncrypt) {
    string sOutput;
    if (bThreeKeys) {
      if (!password_3des(argv[1], argv[2], argv[3], argv[4], argv[5], sOutput, true)) {
        printf("Failed to encrypt password\n");
        return -6;
      }
    }
    else {
      if (!password_3des(argv[1], argv[2], argv[3], sOutput, true)) {
        printf("Failed to encrypt password\n");
        return -7;
      }
    }

    string sOutputHex;
    sOutputHex.resize(sOutput.size() << 1);
    convertBinToHex((unsigned char*)sOutput.data(), sOutput.size(), 
                    (unsigned char*)sOutputHex.data());
    printf("%s\n", sOutputHex.c_str());
  }
  else {
    string sHexInput;
    if (bThreeKeys) {
      sHexInput = argv[5];
    }
    else {
      sHexInput = argv[3];
    }
    string sBinInput;
    sBinInput.resize(sHexInput.size() >> 1);

    if (!convertHexToBin((unsigned char*)sHexInput.data(), sHexInput.size(),
                         (unsigned char*)sBinInput.data()))
    {
      printf("Bad hexadecimal password\n");
      return -8;
    }
    else {
      string sOutput;
      if (bThreeKeys) {
        if (!password_3des(argv[1], argv[2], argv[3], argv[4], sBinInput, sOutput, false)) {
          printf("Failed to decrypt password\n");
          return -9;
        }
        else {
          printf("%s\n", sOutput.c_str());
        }
      }
      else {
        if (!password_3des(argv[1], argv[2], sBinInput, sOutput, false)) {
          printf("Failed to decrypt password\n");
          return -10;
        }
        else {
          printf("%s\n", sOutput.c_str());
        }
      }
    }
  }

  return 0;
}

