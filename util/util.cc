
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


#include "util.h"

/**
 * Runs the current process in the background as a daemon.
 * 
 * @param stdoutname If not NULL, stdout and stderr are redirected
 * to this file.
 * @returns -1 if stdoutname could not be opened.  -2 if the fork() fails.
 * Otherwise, 0.
 */
int RunAsDaemon(const char* stdoutname)
{
	int stdoutfd = -1;
	if (stdoutname != NULL)
	{
#ifdef __FreeBSD__
		stdoutfd = open(stdoutname, O_CREAT | O_WRONLY | O_FSYNC, 0777);
#else
		stdoutfd = open(stdoutname, O_CREAT | O_WRONLY | O_SYNC, 0777);
#endif
		if (stdoutfd < 0)
			return -1;
	}

	int pid = fork();

	if (pid == -1)
		return -2;

	close(0);
	close(1);
	close(2);
  
	// parent
	if (pid != 0)
	{
		if (stdoutfd != -1)
			close(stdoutfd);
		return 0;
//		exit(0);
	}

	// child
	if (stdoutfd != -1)
	{
		dup2(stdoutfd, 1);
		dup2(stdoutfd, 2);
  	
		close(stdoutfd);
	}

	return getpid();
//	return 0;
}

int write_pid(const char * sFileName)
{
	FILE *fp;

	if( (fp=fopen(sFileName,"w")) == 0 )
	{
		return -1;
	}
	if( fprintf(fp,"%i\n",getpid()) <=0  )
	{
		fclose(fp);
		return -1;
	}

	fclose(fp);

	return 0;
}

bool file_exist(const char * sFileName)
{
	struct stat buf;

	return (stat(sFileName, &buf) == 0);
}
