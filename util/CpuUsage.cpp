#include "CpuUsage.h"

CpuUsage::CpuUsage(int timeoutSec) : CPeriodicTask(CPU_USAGE_ITERAION * 1000)
{
    LOG("Initializing CpuUsage ...");
    timeoutSec = max(timeoutSec,CPU_USAGE_ITERAION);
    LOG("Check timeout: "<<timeoutSec<<"s");
    checkTimeout = timeoutSec;
    currentState = checkTimeout;
    currentCpuUsage=0;
    calculator = new CpuUsageCalculator();
}

void CpuUsage::DoTask()
{
    if (currentState<=CPU_USAGE_ITERAION)
    {
        calculator->Stop();
        currentCpuUsage = calculator->getCurrentCpuUsage();
        currentState = checkTimeout;
        LOGL(5, "CpuUsage: Stoping CpuUsage Calculator");
        LOGL(4, "Cpuusage: "<<currentCpuUsage<<"%");
        return ;
    }
    else if (currentState<=2*CPU_USAGE_ITERAION) //start calculation
    {
        LOGL(5, "CpuUsage: Start CpuUsage Calculator");
        calculator->Start();
    }

    currentState-=CPU_USAGE_ITERAION;
}

double CpuUsage::getCurrentCpuUsage()
{
    return currentCpuUsage;
}

CpuUsage::~CpuUsage()
{
    if (calculator)
    {
        delete calculator;
    }
    calculator = NULL;
}


CpuUsageCalculator::CpuUsageCalculator() : CPeriodicTask(1000,false)
{
    totalUsage=0;
    prev_idle=0;
    prev_total=0;
}

void CpuUsageCalculator::DoTask()
{
    ifstream proc("/proc/stat");
    if (!proc.is_open())
    {
        LOGL(5,"CpuUsage: failed to open /proc/stat");
        return ;
    }

    string line;
    while (proc.good())
    {
        getline(proc,line);
        string s;
        stringstream sin(line);
        sin>>s;
        if (s=="cpu")
        {
            double usage;
            double idle=0;
            double total=0;
            int i=0;
            while (sin>>usage)
            {
                if (i==3) idle = usage;
                total += usage;
                i++;
            }
            double diff_idle = idle-prev_idle;
            double diff_total = total-prev_total;
            totalUsage = ((diff_total-diff_idle)/diff_total*100);
            prev_idle=idle;
            prev_total=total;
            break;
        }
    }
    proc.close();
}


double CpuUsageCalculator::getCurrentCpuUsage()
{
    return totalUsage;
}
