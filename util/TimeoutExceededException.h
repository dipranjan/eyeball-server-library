#if !defined(_TimeoutExceededException_h)
#define _TimeoutExceededException_h

#include "Exception.h"
#include <assert.h>
#include <stdarg.h>
#include <strings.h>

/// Exception class thrown when timeout occurs
class TimeoutExceededException : public Exception
{
        public:
                /// Constructor
                TimeoutExceededException() : Exception() {}
                /// Overloaded constructor with variable length argument list
                TimeoutExceededException(const char* newDetails, ...)
                {
                        va_list ap;
                        va_start(ap, newDetails);
                        SetDetails(newDetails, ap);
                        va_end(ap);
                }
                /// Overloaded constructor
                TimeoutExceededException(const char* newDetails, va_list ap)
                {
                        SetDetails(newDetails, ap);
                }
};

#endif
