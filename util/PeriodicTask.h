//! PeriodicTask.h
//! Interface for CPeriodicTask class

#ifndef PERIODIC_TASK_H_
#define PERIODIC_TASK_H_

#include <pthread.h>
#include <unistd.h>
 
class CPeriodicTask
{
public:
	//! Constructor
	//! intervalMs is the interval in milliseconds
	CPeriodicTask(int intervalMs, bool bStartThread = true);

	//! Destructor
	virtual ~CPeriodicTask();

	//! Start thread
	void Start();

	//! Do Task
	//! Override this
	virtual void DoTask();

	//! Stop
	void Stop();

protected:
	//! Loop Thread
	static void* LoopThread(void* arg);	

	int m_iIntervalMs;

	int m_iReadFd;
	int m_iWriteFd;

	pthread_t m_tLoopThread;
	pthread_mutex_t m_StopMutex;
};

#endif //PERIODIC_TASK_H_

