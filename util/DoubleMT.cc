// DoubleMT.cc
// Implementation for the thread-safe double-precision floating-podouble number class
//
// Zion Kwok
// July 26, 2006
//
// (C) Eyeball Networks
//////////////////////////////////////////////

#include "DoubleMT.h"
#include "debug.h"

//! Default Constructor
CDoubleMT::CDoubleMT()
: m_fValue(0.0)
{
  DB_ENTER(CDoubleMT::CDoubleMT);

  pthread_mutex_init(&m_Mutex, NULL);
}

//! Copy Constructor
CDoubleMT::CDoubleMT(CDoubleMT& doubleMT)
: m_fValue(doubleMT)
{
  DB_ENTER(CDoubleMT::CDoubleMT2);

  pthread_mutex_init(&m_Mutex, NULL);
}

//! Constructor
CDoubleMT::CDoubleMT(double value)
: m_fValue(value)
{
  DB_ENTER(CDoubleMT::CDoubleMT3);

  pthread_mutex_init(&m_Mutex, NULL);
}

//! Destructor
CDoubleMT::~CDoubleMT()
{
  DB_ENTER(CDoubleMT::~CDoubleMT);

  pthread_mutex_destroy(&m_Mutex);
}

//! Assignment operator
CDoubleMT& CDoubleMT::operator=(CDoubleMT& doubleMT)
{
  DB_ENTER(CDoubleMT::operator=);

  pthread_mutex_lock(&m_Mutex);
  m_fValue = doubleMT;
  pthread_mutex_unlock(&m_Mutex);

  return *this;
}

CDoubleMT& CDoubleMT::operator=(double x)
{
  DB_ENTER(CDoubleMT::operator=2);

  pthread_mutex_lock(&m_Mutex);
  m_fValue = x;
  pthread_mutex_unlock(&m_Mutex);

  return *this;
}

//! Addition
CDoubleMT& CDoubleMT::operator+=(double x)
{
  DB_ENTER(CDoubleMT::operator+=);

  pthread_mutex_lock(&m_Mutex);
  m_fValue += x;
  pthread_mutex_unlock(&m_Mutex);

  return *this;
}

//! Substraction
CDoubleMT& CDoubleMT::operator-=(double x)
{
  DB_ENTER(CDoubleMT::operator-=);

  pthread_mutex_lock(&m_Mutex);
  m_fValue -= x;
  pthread_mutex_unlock(&m_Mutex);

  return *this;
}

//! Multiplication
CDoubleMT& CDoubleMT::operator*=(double x)
{
  DB_ENTER(CDoubleMT::operator*=);

  pthread_mutex_lock(&m_Mutex);
  m_fValue *= x;
  pthread_mutex_unlock(&m_Mutex);

  return *this;
}

//! Division 
CDoubleMT& CDoubleMT::operator/=(double x)
{
  DB_ENTER(CDoubleMT::operator/=);

  pthread_mutex_lock(&m_Mutex);
  m_fValue /= x;
  pthread_mutex_unlock(&m_Mutex);

  return *this;
}

//! Get doubleeger value
CDoubleMT::operator double()
{
  DB_ENTER(CDoubleMT::operator double);

  pthread_mutex_lock(&m_Mutex);
  double fValue = m_fValue;
  pthread_mutex_unlock(&m_Mutex);

  return fValue;
}



