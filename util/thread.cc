/** All files of the DNDS Prototype are Copyright (C) 1999 by Andre Schoorl
 *  unless otherwise noted.
 *
 *  The DNDS Prototype is distributed in the public domain in the hope that it
 *  will be useful, but is provided `as is' and is subject to change without
 *  notice.  No warranty of any kind is made with regard to the software or
 *  documentation.  The author shall not be liable for for any error for
 *  incidental or consequential damages in connection with the software and
 *  the documentation.
 *
 *  Permission to copy the whole, unmodified DNDS package is granted provided
 *  that the copies are not made or distributed for resale (excepting nominal
 *  copying fees).  All redistributions in either source or binary form must
 *  include this disclaimer.
 */

#include "thread.h"

#include "debug.h"
#include "log.h"
#include "util.h"

#include "errno.h"

Thread::Thread(void * (*start_routine)(void *), void * arg)
{
	//DB("Thread::Thread");

#ifdef WIN32
	unsigned long lpThreadId;
	ASSERT(thread_id = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)start_routine, arg, 0, &lpThreadId));
#else
	ASSERT(pthread_create(&thread_id, NULL, start_routine, arg) == 0);
#endif
}

Thread::~Thread()
{
	//DB("Thread::~Thread");

#ifdef WIN32
	ASSERT(TerminateThread(thread_id, 0));
	WARN(CloseHandle(thread_id));
#else
	// Just warn if cancel fails since thread may already have ended
	// Mainly for Solaris
	WARN(pthread_cancel(thread_id) == 0);
#endif
}

#ifdef WIN32
void Thread::set_priority(int priority)
{
	//DB("Thread::set_priority");

	ASSERT(SetThreadPriority(thread_id, priority)); 
}
#endif

void Thread::join(void)
{
	DB("Thread::join");

#ifdef WIN32
	WARN(WaitForSingleObject(thread_id, INFINITE) != WAIT_FAILED);
#else
	WARN(pthread_join(thread_id, NULL) == 0);
#endif
}

void Thread::create(void * (*start_routine)(void *), void *arg)
{
	//DB("Thread::create");

#ifdef WIN32
	unsigned long lpThreadId;
	ASSERT(CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)start_routine, arg, 0, &lpThreadId));
#else
	pthread_t thread_id;
	int iRet;
	if( (iRet = pthread_create(&thread_id, NULL, start_routine, arg)) != 0 )
	{
		LOG("Thread create: " << iRet);
		ASSERT(0 && "Thread create");
	}
#endif
}

void Thread::CreateDetached(void * (*start_routine)(void *), void *arg)
{
	pthread_t thread_id;
	int iRet;
	pthread_attr_t threadAttr;
	if( pthread_attr_init(&threadAttr) != 0 )
		LOG("Error in pthread_attr_init");

	if( pthread_attr_setdetachstate(&threadAttr, PTHREAD_CREATE_DETACHED) != 0 )
		LOG("Error in pthread_attr_setdetachstate");
	
	if( (iRet = pthread_create(&thread_id, &threadAttr, start_routine, arg)) != 0 )
	{
		LOG("Thread create: " << iRet);
		ASSERT(0 && "Thread create");
	}
	if( pthread_attr_destroy(&threadAttr) != 0 )
		LOG("Error in pthread_attr_destroy");
}
