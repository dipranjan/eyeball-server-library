// $Id: timer.cc,v 1.1 2005/06/10 22:53:02 jozsef Exp $
//
// File:       timer.cc
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Author:     Jozsef Vass
//
// Purpose:    Timer class
//
// ---------------------------------------------------------------------------


#include "timer.h"
#include "debug.h"
#include "log.h"

StopWatch::StopWatch():
	m_lTotalTime(0),
	m_iCount(0),
	m_bRunning(false)
{ }

void StopWatch::Reset()
{
	m_lTotalTime = 0;
	m_iCount     = 0;
	m_bRunning   = false;
}

int StopWatch::Start()
{
	int iRet(0);
	if( m_bRunning )
	{
//		DB_AND_LOG("StopWatch::Start(): Timer is running");
		iRet = -1;
	}
	gettimeofday(&m_StartTime,NULL);
	m_bRunning = true;

	return iRet;
}

int StopWatch::Stop()
{
	int iRet(0);

	if( !m_bRunning )
	{
//		DB_AND_LOG("StopWatch::Stop(): Timer is not running");
		iRet = -1;
	}
	gettimeofday(&m_StopTime,NULL);
	m_lTotalTime += 1000*(m_StopTime.tv_sec-m_StartTime.tv_sec)
		+ long(0.5f + 0.001f*(m_StopTime.tv_usec-m_StartTime.tv_usec));
	m_iCount++;
	m_bRunning = false;

	return iRet;
}

float StopWatch::GetAverageTime(int iInterval) const
{
	if( m_iCount>0 && (m_iCount % iInterval == 0) )
	{
		return float(m_lTotalTime)/float(m_iCount);
	}
	else
		return 0.0f;
}

float StopWatch::GetAverageTime() const
{
	if( m_iCount>0 )
	{
		return float(m_lTotalTime)/float(m_iCount);
	}
	else
		return 0.0f;
}
