// ---------------------------------------------------------------------------
// File:       Serializable.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2002
//
// Purpose:    Base class for network messages
//
// Change Log:
// ---------------------------------------------------------------------------

#if defined(WIN32) || defined (_WIN32_WCE)
#include "Base.h"  // automatic use of precompiled headers
#else
#include "debug.h"
#include "util.h"
#include <netinet/in.h>
#endif

#include "Serializable.h"
#include <string.h>

using std::string;


// ---------------------------------------------------------------------------
// Serialization/deserialization helper functions
// ---------------------------------------------------------------------------

inline void InlineSerializeBytes(
	const void* p, 
	unsigned int uSize, 
	string& sBuf)
{
	sBuf.append((const char*)p, uSize);
}

void SerializeBytes(const void* p, unsigned int uSize, string& sBuf)
{
	InlineSerializeBytes(p, uSize, sBuf);
}

void Serialize8(u_int8_t u8, string& sBuf)
{
	InlineSerializeBytes(&u8, sizeof(u_int8_t), sBuf);
}

void Serialize16(u_int16_t u16, string& sBuf)
{
	u_int16_t t = htons(u16);
	InlineSerializeBytes(&t, sizeof(u_int16_t), sBuf);
}

void Serialize32(u_int32_t u32, string& sBuf)
{
	u_int32_t t = htonl(u32);
	InlineSerializeBytes(&t, sizeof(u_int32_t), sBuf);
}

void Serialize64(u_int64_t u64, string& sBuf)
{
	u_int64_t t = htonl(u64);
	InlineSerializeBytes(&t, sizeof(u_int64_t), sBuf);
}

void SerializeCString(const char* p, unsigned int uSize, string& sBuf)
{
	InlineSerializeBytes(p, uSize, sBuf);
}

void SerializeString8(const string& s, string& sBuf)
{
	DB_ASSERT(s.size() <= 255);
	u_int8_t uSize = int_min(255, s.size());

	Serialize8(uSize, sBuf);
	InlineSerializeBytes(s.data(), uSize, sBuf);
}

void SerializeString16(const string& s, string& sBuf)
{
	DB_ASSERT(s.size() <= 65535);
	u_int16_t uSize = int_min(65535, s.size());

	Serialize16(uSize, sBuf);
	InlineSerializeBytes(s.data(), uSize, sBuf);
}

void SerializeString32(const string& s, string& sBuf)
{
	u_int32_t uSize = s.size();

	Serialize32(uSize, sBuf);
	InlineSerializeBytes(s.data(), uSize, sBuf);
}

void SerializeStringVector16(const vector<std::string>& s, std::string& sBuf)
{
	DB_ASSERT(s.size() <= 65535);
	u_int16_t uSize = int_min(65535, s.size());
        int iSize = (int)uSize;

	Serialize16(uSize, sBuf);
	for (int i = 0; i < iSize; i++) {
		SerializeString16(s[i], sBuf);
        }
}

int GetSerializeStringVector16Length(const vector<std::string>& s)
{
	DB_ASSERT(s.size() <= 65535);
	u_int16_t uSize = int_min(65535, s.size());
        int iSize = (int)uSize;

	int iLength = sizeof(u_int16_t);
	for (int i = 0; i < iSize; i++) {
		const string& s2 = s[i];
		DB_ASSERT(s2.size() <= 65535);
		u_int16_t uSize2 = int_min(65535, s2.size());
		iLength += sizeof(u_int16_t) + uSize2;
        }

	return iLength;
}

void Serialize8Vector16(const vector<u_int8_t>& s, string& sBuf)
{
        DB_ASSERT(s.size() <= 65535);
        u_int16_t uSize = int_min(65535, s.size());
        int iSize = (int)uSize;

        Serialize16(uSize, sBuf);
        for (int i = 0; i < iSize; i++) {
                Serialize8(s[i], sBuf);
        }
}

int GetSerialize8Vector16Length(const vector<u_int8_t>& s)
{
        DB_ASSERT(s.size() <= 65535);
        u_int16_t uSize = int_min(65535, s.size());
        int iSize = (int)uSize;

        int iLength = sizeof(u_int16_t) + iSize;

        return iLength;
}

inline bool InlineDeserializeBytes(
	void* p,
	unsigned int uSize,
	const string& sBuf,
	int& iOffset)
{
	if ((int)sBuf.size() < (int)(iOffset + uSize))
	{
		DB_ASSERT(0 && "Deserialization failed");
		return false;
	}

	memcpy(p, &sBuf.data()[iOffset], uSize);
	iOffset += uSize;

	return true;
}

bool DeserializeBytes(
	void* p, 
	unsigned int uSize, 
	const string& sBuf,
	int& iOffset)
{
	return InlineDeserializeBytes(p, uSize, sBuf, iOffset);
}

bool Deserialize8(u_int8_t& u8, const string& sBuf, int& iOffset)
{
	return InlineDeserializeBytes(&u8, sizeof(u_int8_t), sBuf, iOffset);
}

bool Deserialize16(u_int16_t& u16, const string& sBuf, int& iOffset)
{
	if (!InlineDeserializeBytes(&u16, sizeof(u_int16_t), sBuf, iOffset))
		return false;

	u16 = ntohs(u16);

	return true;
}

bool Deserialize32(u_int32_t& u32, const string& sBuf, int& iOffset)
{
	if (!InlineDeserializeBytes(&u32, sizeof(u_int32_t), sBuf, iOffset))
		return false;

	u32 = ntohl(u32);

	return true;
}



bool Deserialize64(u_int64_t& u64, const string& sBuf, int& iOffset)
{
	if (!InlineDeserializeBytes(&u64, sizeof(u_int64_t), sBuf, iOffset))
		return false;

	u64 = ntohl(u64);

	return true;
}


bool DeserializeCString(
	char* p, 
	unsigned int uSize, 
	const string& sBuf, 
	int& iOffset)
{
	if (!InlineDeserializeBytes(p, uSize, sBuf, iOffset))
		return false;

	p[uSize - 1] = '\0';

	return true;
}

bool DeserializeString8(string& s, const string& sBuf, int& iOffset)
{
	u_int8_t uSize = 0;
	if (!Deserialize8(uSize, sBuf, iOffset))
		return false;

	if ((int)sBuf.size() < iOffset + uSize)
	{
		DB_ASSERT(0 && "Deserialization failed");
		return false;
	}

	s.assign(sBuf, iOffset, uSize);
	iOffset += uSize;

	return true;
}

bool DeserializeString16(string& s, const string& sBuf, int& iOffset)
{
	u_int16_t uSize = 0;
	if (!Deserialize16(uSize, sBuf, iOffset))
		return false;

	if ((int)sBuf.size() < iOffset + uSize)
	{
		DB_ASSERT(0 && "Deserialization failed");
		return false;
	}

	s.assign(sBuf, iOffset, uSize);
	iOffset += uSize;

	return true;
}

bool DeserializeString32(string& s, const string& sBuf, int& iOffset)
{
	u_int32_t uSize = 0;
	if (!Deserialize32(uSize, sBuf, iOffset))
		return false;

	if ((int)sBuf.size() < (int)(iOffset + uSize))
	{
		DB_ASSERT(0 && "Deserialization failed");
		return false;
	}

	s.assign(sBuf, iOffset, uSize);
	iOffset += uSize;

	return true;
}

bool DeserializeStringVector16(
        vector<std::string>& s,
        const std::string& sBuf,
        int& iOffset)
{
	u_int16_t uSize = 0;
	if (!Deserialize16(uSize, sBuf, iOffset))
		return false;

	int iSize = (int)uSize;
	s.resize(iSize);
	for (int i = 0; i < iSize; i++) {
		if (!DeserializeString16(s[i], sBuf, iOffset))
			return false;
	}

	return true;
}

bool Deserialize8Vector16(
        vector<u_int8_t>& s,
        const string& sBuf,
        int& iOffset)
{
        u_int16_t uSize = 0;
        if (!Deserialize16(uSize, sBuf, iOffset))
                return false;

        int iSize = (int)uSize;
        s.resize(iSize);
        for (int i = 0; i < iSize; i++) {
                if (!Deserialize8(s[i], sBuf, iOffset))
                        return false;
        }

        return true;
}


