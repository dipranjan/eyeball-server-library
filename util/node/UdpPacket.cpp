#include "UdpPacket.h"
#include <stdio.h>


//int UdpPacket::sendTo(int __fd, char *__buf, size_t __n, int __flags, sockaddr *__addr, socklen_t __addr_len);
UdpPacket::UdpPacket()
{

}

UdpPacket::~UdpPacket()
{

}

int UdpPacket::encodeAndSend(int fd, const char *ibuf, size_t n, int flags, sockaddr *addr, socklen_t addr_len)
{
    int toSend = n;
    string header;

    //cout<<"ENCODE AND SEND("<<toSend<<")"<<endl;

    unsigned uiSeqNo;
    memcpy(&uiSeqNo, &(ibuf[8]), 4);
    uiSeqNo = ntohl(uiSeqNo);

   // cout<<"-------squence no ###### "<<uiSeqNo<<endl;


    while( toSend > 0 )
    {
        int szPacket = MAX_UDP_PACKET_SIZE;
        if ( toSend <= MAX_UDP_PACKET_SIZE )
        {
            header = "1";
            szPacket = toSend;

        }
        else header = "0";

        char data[20];
        sprintf(data,"%010d",szPacket);
        //cout<<"UdpPacket "<<string(data,data+10)<<" should equal "<<szPacket<<endl;

        header.append(data,data+10);

        sprintf(data,"%010u",uiSeqNo);
        header.append(data,data+SQ_LEN);

        header.append(string(1,'.')); // only for padding

        //cout<<" header size = "<<header.size()<<endl;

        //cout<<"sending packet("<<szPacket<<")"<<endl;
        buf.clear();
        buf.assign(header.begin(),header.end());
        buf.append(ibuf,ibuf+szPacket);

        int ret;
        if( (ret=sendto(fd,buf.data(),buf.size(),flags,addr,addr_len) < 0 ) )
        {
            return ret;
        }
        toSend -= szPacket;
        ibuf += szPacket;
    }

    return n;
}

int UdpPacket::sendTo(int fd,const char *buf, size_t n, int flags, sockaddr *addr, socklen_t addr_len)
{
    UdpPacket packet;
    return packet.encodeAndSend(fd,buf,n,flags,addr,addr_len);
}

string UdpPacket::getData()
{
    return buf.substr(HEADER_SIZE,this->getLength());
}

void UdpPacket::decode(const char *ibuf, int bufLen)
{
    if (bufLen<HEADER_SIZE)
    {
        LOGL(4, "UDPPACKET: cant decode buffer: buflen:"<<bufLen);
        return ;
    }

    buf.clear();
    buf.assign(ibuf,ibuf+bufLen);
    //cout<<" ---------------- buf length = "<<buf.size()<<endl;
//    getData(ibuf);
}

bool UdpPacket::isLastPacket()
{
    //cout<<"UdpPacket:isLastPacet "<<buf[0]<<" "<<(buf[0] == '1')<<endl;
    return (buf[0] == '1');
}

int UdpPacket::getLength()
{
    int len;
    sscanf(buf.substr(1,10).data(),"%d",&len);

    //cout<<"UdpPacket:getLength "<<buf.substr(1,10).data()<<" should equal "<<len<<endl;
    return len;
}

u_int32_t UdpPacket::getSeqNo()
{
    u_int32_t len;
    sscanf(buf.substr(SQ_START,SQ_LEN).data(),"%u",&len);
    return len;
}
