#ifndef UDPPACKET_H
#define UDPPACKET_H

#include <sys/socket.h>
#include <string.h>
#include "log.h"
#include <arpa/inet.h>
#include <sys/poll.h>
#include <sys/time.h>
#define MAX_UDP_PACKET_SIZE (1<<14)

#define HEADER_SIZE     22
#define PACKET_START 1
#define PACKET_SIZE 10

#define SQ_START 11
#define SQ_LEN 10


class UdpPacket
{
    string buf;
public:
    UdpPacket();
    ~UdpPacket();

    int encodeAndSend(int fd, const char *ibuf, size_t n, int flags, sockaddr *addr, socklen_t addr_len);

    static int sendTo(int fd, const char *buf, size_t n, int flags, sockaddr *addr, socklen_t addr_len);

    void decode(const char *ibuf, int bufLen);
    string getData();

    bool isLastPacket();
    int getLength();
    uint32_t getSeqNo();
};

#endif // UDPPACKET_H
