// ---------------------------------------------------------------------------
// File:       QueryMessage.cc 
//
// Copyright:  Eyeball Networks Inc. 1999-2002
//
// Purpose:    Base class for network messages
//
// Change Log:
// ---------------------------------------------------------------------------

#if defined(WIN32) || defined(_WIN32_WCE)
#include "Base.h"  // automatic use of precompiled headers
#endif

#include "QueryMessage.h"

using std::string;


// ---------------------------------------------------------------------------
// QueryMessage struct
// ---------------------------------------------------------------------------

QueryMessage::QueryMessage()
: CommandMessage(EYEBALL_SERVER_QUERY),
  m_u32SeqNo(0)
{
}

QueryMessage::QueryMessage(int iMessageType)
: CommandMessage(iMessageType),
  m_u32SeqNo(0)
{
}

QueryMessage::~QueryMessage()
{
}

void QueryMessage::serialize(string& sBuf) const
{
	CommandMessage::serialize(sBuf);

	SERIALIZE32(m_u32SeqNo);
}

int QueryMessage::deserialize(const string& sBuf)
{
	if (sBuf.size() < QM_LENGTH)
		return -1;

	int iOffset = 0;

	DESERIALIZE_BASE_CLASS(CommandMessage);
	DESERIALIZE32(m_u32SeqNo);

	return iOffset;
}

#ifndef _WIN32_WCE
std::ostream& operator<<(std::ostream& s, const QueryMessage& v)
{
	s << (CommandMessage)v << " " << v.m_u32SeqNo;

	return s;
}
#endif
