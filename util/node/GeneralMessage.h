// ---------------------------------------------------------------------------
// File:       GeneralMessage.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Author:     Jozsef Vass
//
// Purpose:    SSL message class
//
// Change Log: 07/31/2001 - Created
// ---------------------------------------------------------------------------

#ifndef __GENERAL_MESSAGE__
#define __GENERAL_MESSAGE__

#include <string>

#include "Message.h"

class GeneralMessage
{
public:
	virtual ~GeneralMessage() {};

	virtual int OpenServer(
		int fdSocket,
		const std::string& sCertFile = "",
		const std::string& sPvkFile = "") = 0;

	virtual int OpenClient(
		const std::string& sHost,
		int iPort,
		int iTimeout) = 0;
	
	virtual void Close() = 0;

	virtual int GetFd() = 0;

	virtual int RecvMsg(std::string& sBuf) = 0;

	virtual int SendMsg(const struct ISerializable& Msg) = 0;
	virtual int SendMsg(const std::string& sBuf) = 0;
	virtual int SendMsg(const char *pBuf, int iLen) = 0;

	virtual int SendBufferedData() = 0;
};
#endif  // __GENERAL_MESSAGE__
