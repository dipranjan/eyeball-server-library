// ---------------------------------------------------------------------------
// File:       TcpMessage.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Author:     Jozsef Vass
//
// Purpose:    TCP message class
//
// Change Log: 08/01/2001 - Created
// ---------------------------------------------------------------------------

#ifndef __TCP_MESSAGE__
#define __TCP_MESSAGE__

#include <string>

#include "Message.h"
#include "GeneralMessage.h"
#include "SendBuffer.h"

#include "SmartPtr.h"

class TcpMessage : public GeneralMessage
{
public:
	TcpMessage();
	virtual ~TcpMessage();

	/**
	* Opens TCP server connection. Empty method, only used for SSL sockets.
	* Always suceeds.
	*
	* @param fdSocket  Accepted socket file descriptor
	* @param sCertFile Not used
	* @param sPvkFle   Not used
	*
	* @out Always 0 (success)
	*/
	virtual int OpenServer(int fdSocket,
		const std::string& sCertFile = "",
		const std::string& sPvkFile = "");

	/**
	* Connects to server. Also sets the socket to non-blocking and
	* non-lingering. Connection timeout currently not implemented.
	*
	* @param sHost IP address of server
	* @param iPort Listen port of server
	* @param iTimeout Connection timeout in msec
	*
	* @out ES_SUCCESS on success. ES_NOT_CONNECTED on failure.
	*/
	virtual int OpenClient(
		const std::string& sHost,
		int iPort,
		int iTimeout);

	virtual int OpenClient(
		u_int32_t uHost,
		int iPort);

	virtual void Close();

	/**
	* Returns the file descriptor of the connection.
	*
	* @out File descriptor of the connection. -1 for invalid socket.
	*/
	virtual int GetFd() { return m_fdSocket; }

	virtual int Read(char* pBuf, int iLen);

	virtual int RecvMsg(std::string& sBuf);

	/**
	* Send message.
	* 
	* @param Msg message to be sent. If the data cannot be sent right away
	* (not sufficient network buffers), it will be buffered. In this case, 
	* if the socket is writeable (use select or poll), then the buffered
	* data can be sent by calling SendBufferedData() method.
	*
	* @out ES return code
	* @see retval.h
	*/	
	virtual int SendMsg(const struct ISerializable& Msg);

	virtual int SendMsg(const std::string& sBuf);

	virtual int SendMsg(const char *pBuf, int iLen);

	/**
	 * Sends buffered message.  It is possible that data was buffered by the
	 * object. After a while, this function needs to be called to send the
	 * data out from the buffer.
	 *
	 * @out ES return code
	 */
	virtual int SendBufferedData();

	/**
	 * Returns how many bytes are actually buffered in send buffer.
	 */
	virtual int GetSendBufferSize();

private:
	int CheckHeader(const struct CommandMessage& msgHdr);
	int GetHeader(struct CommandMessage& msgHdr);
	TcpMessage (TcpMessage& v){}  // cannot copy-construct

private:
	int m_fdSocket;
	std::string m_sRecvBuffer;
	SharedPtr<SendBuffer> m_pSendBuffer;
};
#endif // __TCP_MESSAGE__
