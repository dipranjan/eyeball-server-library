// ---------------------------------------------------------------------------
// File:       TcpMessage.cc
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Author:     Jozsef Vass
//
// Purpose:    TCP message class
//
// Change Log: 08/01/2001 - Created
// ---------------------------------------------------------------------------

#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#include "TcpMessage.h"

#include "debug.h"
#include "log.h"
#include "retval.h"
#include "util.h"
#include "inet.h"

using std::string;

#define USE_SEND_BUFFER 1

TcpMessage::TcpMessage() :
	m_fdSocket(INVALID_SOCKET),
	m_sRecvBuffer(),
	m_pSendBuffer(new SendBuffer)
{
}

TcpMessage::~TcpMessage()
{
}

int TcpMessage::OpenServer(
	int fdSocket,
	const std::string& sCertFile,
	const std::string& sPvkFile)
{
	m_fdSocket = fdSocket;

	return 0;
}

int TcpMessage::OpenClient(
	const string& sHost,
	int iPort,
	int iTimeout)
{
	u_int32_t uHost = LookupHostAddr(sHost.c_str());

	if( uHost == 0 )
		return ES_NOT_CONNECTED;

	return OpenClient(uHost, iPort);
}

int TcpMessage::OpenClient(
	u_int32_t uHost,
	int iPort)
{
   int fd = socket(AF_INET, SOCK_STREAM, 0);
   if (fd < 0)
      return ES_NOT_CONNECTED;

   struct sockaddr_in sin;
   memset((char *)&sin, 0, sizeof(sin));
   sin.sin_family = AF_INET;
   sin.sin_port = htons(iPort);
   sin.sin_addr.s_addr = uHost;

   if (connect(fd, (struct sockaddr *)&sin, sizeof(sin)) < 0)
      return ES_NOT_CONNECTED;

   // Do we need blocking of nonblocking for receive?
   nonblock(fd);

   // Avoid blocking on close
   nolinger(fd);

	recvbufsize(fd);
	sendbufsize(fd);

   // Success
   m_fdSocket = fd;

   return ES_SUCCESS;
}

#if USE_SEND_BUFFER == 0
int TcpMessage::SendMsg(
	const char* pBuf,
	int iLen)
{
	int iRet = ::send(m_fdSocket, pBuf, iLen, 0);

	if( iRet <= 0 )
	{
		LOG("TCP send problem on fd " << m_fdSocket << ": " << strerror(errno));
		return ES_NOT_CONNECTED;
	}

	if( iRet != iLen )
		LOG("TCP send problem on fd " << m_fdSocket << ": partial data");

	return ES_SUCCESS;
}
#else
int TcpMessage::SendMsg(
	const char* pBuf,
	int iLen)
{
	if( m_fdSocket == INVALID_SOCKET )
		return ES_NOT_CONNECTED;

	m_pSendBuffer->AppendPacket(string(pBuf, iLen));

	return SendBufferedData();
}
#endif

int TcpMessage::SendBufferedData()
{
	while( !m_pSendBuffer->Empty() )
	{
		string sData;
		if( !m_pSendBuffer->GetPacket(sData) )
		{
			INTERNAL_ERROR()
			return ES_NOT_CONNECTED;
		}

		int iRet = ::send(m_fdSocket, sData.data(), sData.size(), 0);

		if( iRet > 0 )
		{
			m_pSendBuffer->RemoveBytes(iRet);
		}
		else
		{
			if( iRet == 0 )
			{
				LOG("TCP send problem on fd " << m_fdSocket << ": closed");
				return ES_NOT_CONNECTED;
			}

			LOG("TCP send problem on fd " << m_fdSocket << ": " << strerror(errno));

			if( errno == EAGAIN || errno == EWOULDBLOCK )
				return ES_AGAIN;

			// other error
			return ES_NOT_CONNECTED;
		}
	}

	return ES_SUCCESS;
}

int TcpMessage::Read(char* pBuf, int iLen)
{
 	int iRet = ::recv(m_fdSocket, pBuf, iLen, 0);
 
 	return iRet;
}

int TcpMessage::SendMsg(const struct ISerializable& Msg)
{
	string sBuf;
	Msg.serialize(sBuf);

	return SendMsg(sBuf.data(), sBuf.size());
}

int TcpMessage::SendMsg(const string& sBuf)
{
	return SendMsg(sBuf.data(), sBuf.size());
}

void TcpMessage::Close()
{ 
//	DB_ENTER(TcpMessage::Close);
//	DB_STAT(m_fdSocket);

	if( m_fdSocket >= 0 )
	{
		::shutdown(m_fdSocket, 2);
		::close(m_fdSocket);

		m_fdSocket = -1;
	}
}

int TcpMessage::CheckHeader(const struct CommandMessage& msgHdr)
{
	switch( msgHdr.m_u8Version )
	{
		case EYEBALL_CHAT_LE_09:
		case EYEBALL_CHAT_LE_10:
		case EYEBALL_CHAT_LE_11:
		case EYEBALL_CHAT_LE_12:
		case EYEBALL_CHAT_20:
		case EYEBALL_CHAT_22:
		case EYEBALL_CHAT_23:
			break;

		case EYEBALL_VIDEOMESSAGE_01:
			break;

		case EYEBALL_GROUPCHAT_02:
			break;

		case EYEBALL_CHATROOM:
		case EYEBALL_CHATROOM_UNICODE:
			break;

		case EYEBALL_IM:
			break;

		default:
			return ES_NOT_CONNECTED;
	}

	return ES_SUCCESS;
}

int TcpMessage::RecvMsg(string& sBuf)
{
   // Access the message header
   CommandMessage msgHdr;
   int iRet = GetHeader(msgHdr);
   if (iRet != ES_SUCCESS)
      return iRet; // message header is not yet available

	iRet = CheckHeader(msgHdr);
	if( iRet != ES_SUCCESS )
		return iRet; 

   // Allocate memory for the payload
   u_int16_t uPayloadLen = msgHdr.m_u16PayloadLength;
   u_int32_t cbTotal = CM_LENGTH + uPayloadLen;

   if (uPayloadLen > 0)
   {
      // Perform TCP receive
      int len = cbTotal - m_sRecvBuffer.size();  // requested length
		if( len < 0 )
			return ES_NOT_CONNECTED;

		string sBuf;
		sBuf.resize(len);

		int iRet = Read(&sBuf[0], len);

      if (iRet == 0)
		{
			LOG("Client closed connection on fd " << m_fdSocket);
         return ES_NOT_CONNECTED;
		}

		if( iRet < 0 )
		{
			LOG("Error during TCP read on fd " << m_fdSocket << ": " << strerror(errno));
			return ES_NOT_CONNECTED;
		}

      m_sRecvBuffer.append(sBuf.data(), iRet); // add bytes received

      if (m_sRecvBuffer.size() < cbTotal)
      {
         return ES_FAILURE; // payload not yet available
      }
   }

	if( m_sRecvBuffer.size() != cbTotal )
		return ES_NOT_CONNECTED;

   // Success
   sBuf = m_sRecvBuffer;

   // Clean-up
   m_sRecvBuffer.erase();

   return ES_SUCCESS;
}

int TcpMessage::GetHeader(CommandMessage& msgHdr)
{
   // Return message header, if it is buffered
   if (m_sRecvBuffer.size() >= CM_LENGTH)
   {
      msgHdr.deserialize(m_sRecvBuffer);
      return ES_SUCCESS;
   }

   int len = CM_LENGTH - m_sRecvBuffer.size();
	if( len < 0 )
		return ES_NOT_CONNECTED;

	string sBuf;
	sBuf.resize(len);

	int iRet = Read(&sBuf[0], len);

   if (iRet == 0)
	{
		LOG("Client closed connection on fd " << m_fdSocket);
      return ES_NOT_CONNECTED;
	}

	if( iRet < 0 )
	{
		LOG("Error during TCP read on fd " << m_fdSocket << ": " << strerror(errno));
		return ES_NOT_CONNECTED;
	}

   m_sRecvBuffer.append(sBuf.data(), iRet); // add bytes received

   if (m_sRecvBuffer.size() >= CM_LENGTH)
   {
      msgHdr.deserialize(m_sRecvBuffer);
      return ES_SUCCESS;
   }

   return ES_FAILURE;
}

int TcpMessage::GetSendBufferSize()
{
	return m_pSendBuffer->Size();
}
