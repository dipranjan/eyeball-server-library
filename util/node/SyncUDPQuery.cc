// SyncUDPQuery.cc
// Implementation of the CSyncUDPQuery class

#include "SyncUDPQuery.h"
#include "QueryMessage.h"
#include <arpa/inet.h>
#include <sys/poll.h>
#include <sys/time.h>
#include <pthread.h>
#include <errno.h>
#include <time.h>
#include <stdio.h>
#include <string.h>

//! Constructor
//! Don't bind to address:port if
//!   1) sAddress is empty 
//!   2) sAddress is "any"
//!   3) iPort is 0
CSyncUDPQuery::CSyncUDPQuery(const string& sAddress, int iPort, int iMaxThreads)
: m_pSenders(NULL),
  m_iNumSenders(0),
  m_iFirstUnusedSenderInfo(0),
  m_iSeqNo(0),
  m_sAddress(sAddress),
  m_usPort(iPort),
  m_iMaxThreads(iMaxThreads),
  m_PollThread((pthread_t)-1),
  m_bStopPollThread(false),
  m_iStopReadFd(-1),
  m_iStopWriteFd(-1),
  m_iSocketFd(-1)
{
	if (m_iMaxThreads < 1) {
		m_iMaxThreads = 1;
	}

	m_pSenders = new SenderInfo[m_iMaxThreads];
	for (int i = 0; i < m_iMaxThreads; i++) {
		pthread_mutex_init(&(m_pSenders[i].mutex), NULL);
		pthread_cond_init(&(m_pSenders[i].cond), NULL);
		m_pSenders[i].iNextUnusedSenderInfo = i + 1;
	}

	m_pSenders[m_iMaxThreads - 1].iNextUnusedSenderInfo = -1;
	
	m_HashTable.init(m_iMaxThreads);

	pthread_mutex_init(&m_Mutex, NULL);
}
	
//! Destructor
CSyncUDPQuery::~CSyncUDPQuery()
{
	Stop();

	for (int i = 0; i < m_iMaxThreads; i++) {
		pthread_mutex_lock(&(m_pSenders[i].mutex));
		m_pSenders[i].iResponseLen = -1;
		pthread_mutex_unlock(&(m_pSenders[i].mutex));

		pthread_cond_signal(&(m_pSenders[i].cond));

		pthread_cond_destroy(&(m_pSenders[i].cond));
		pthread_mutex_destroy(&(m_pSenders[i].mutex));
	}
	delete [] m_pSenders;
	pthread_mutex_destroy(&m_Mutex);
}

//! Start
void CSyncUDPQuery::Start()
{
	if (m_PollThread == (pthread_t)-1) {
		m_bStopPollThread = false;

		m_iSocketFd = ::socket(AF_INET, SOCK_DGRAM, 0);
		if (m_iSocketFd < 0) {
			return;
		}

		if ((m_sAddress.length() > 0) &&
		    (m_sAddress != (string)"any") &&
		    (m_usPort != 0))
		{
			//don't bind if address is empty or "any", or port is 0
			struct sockaddr_in addr;
			bzero((char*) &addr, sizeof(addr));
			addr.sin_family = AF_INET;
		
			if (!inet_pton(AF_INET, m_sAddress.c_str(), &(addr.sin_addr))) {
				printf("error inet_pton\n");
				return;
			}

			addr.sin_port = htons(m_usPort);

			if ( bind(m_iSocketFd, (struct sockaddr*) &addr, sizeof (addr)) <0) {
				::close(m_iSocketFd);
				m_iSocketFd = -1;
				printf("** bind failed\n");
				return;
			}
		}

		int iSocketPairFd[2];
                int result =
                        socketpair(AF_UNIX, SOCK_DGRAM, 0, iSocketPairFd);

                if (result == 0) {
                        m_iStopReadFd = iSocketPairFd[0];
                        m_iStopWriteFd = iSocketPairFd[1];
                }
		else {
			::close(m_iSocketFd);
			m_iSocketFd = -1;
			printf("** socketpair error\n");
			return;
		}

		pthread_create(&m_PollThread, NULL, PollFunction, this);
	}
}

//! Stop
void CSyncUDPQuery::Stop()
{
	if (m_PollThread != (pthread_t)-1) {
		m_bStopPollThread = true;

		//Trigger poll
		char dummy;
		::send(m_iStopWriteFd, &dummy, 1, 0);

		//Wait for poll loop to stop
		void* ret;
		pthread_join(m_PollThread, &ret);

		//Close sockets
		::close(m_iStopReadFd);
		::close(m_iStopWriteFd);
		m_iStopReadFd = -1;
		m_iStopWriteFd = -1;

		::close(m_iSocketFd);
		m_iSocketFd = -1;
		m_PollThread = (pthread_t)-1;
	}
}

//! Returns length of Response message, 0 for timeout, -1 for error
//! The sequence number in pcMessage is changed
int CSyncUDPQuery::SendTo(const string& sToAddress, int iToPort, 
	char* pcMessage, int iLen, char** pcResponse, 
	int timeout_sec, int timeout_nsec)
{
	if (m_iSocketFd == -1) {
		printf("ERROR: socket is invalid\n");
		return -1;
	}

	if (NULL == pcResponse)
	{
		printf("ERROR: pcResponse is NULL\n");
		return -2;
	}

	if (iLen < QM_LENGTH) {
		printf("ERROR: message length too short\n");
		return -3;
	}

	SenderInfo* pSender = NULL;
	int iInfoId = -1;

	bool bError = false;

	pthread_mutex_lock(&m_Mutex);

	if (m_iFirstUnusedSenderInfo == -1) {
		bError = true;
	}
	else {
		iInfoId = m_iFirstUnusedSenderInfo;
		pSender = &(m_pSenders[iInfoId]);
		pSender->iSeqNo = m_iSeqNo++;
		m_iFirstUnusedSenderInfo = pSender->iNextUnusedSenderInfo;
	}

	pthread_mutex_unlock(&m_Mutex);
	
	if (bError) {
		printf("error: first unused sender invalid\n");
		return -4;
	}

	u_int32_t uiSeqNo = htonl(pSender->iSeqNo);
	memcpy(&(pcMessage[QM_LENGTH-4]), &uiSeqNo, 4);

        struct sockaddr_in addr;
        bzero((char*) &addr, sizeof(addr));
        addr.sin_family = AF_INET;

        if(sToAddress!=(string)"any")
        {
		if (!inet_pton(AF_INET, sToAddress.c_str(), &(addr.sin_addr))) {
			printf("failed to lookup address\n");
			return -5;
		}
        }
        else
        {
		printf("any not supported\n");
        	return -6;
        }

        addr.sin_port = htons(iToPort);  // use the given dest. port!

	pSender->pcResponse = NULL;
	pSender->iResponseLen = 0;
		
	SenderInfo*** pValue = new SenderInfo**;
	*pValue = &pSender;

	m_HashTable.addToTable(pSender->iSeqNo, pValue);

	struct timespec timeout;
	struct timeval now;

	gettimeofday(&now, NULL);

	timeout.tv_sec = now.tv_sec + timeout_sec;
	timeout.tv_nsec = (now.tv_usec * 1000) + timeout_nsec;

	if (timeout.tv_nsec >= 1000000000) {
		timeout.tv_nsec -= 1000000000;
		timeout.tv_sec++;
	}

	int r = 0;
	r = ::sendto(m_iSocketFd, pcMessage, iLen, 0, (sockaddr*)&addr, sizeof(addr));
	if (r >= 0)
	{
		pthread_mutex_lock(&(pSender->mutex));
		if (pSender->iResponseLen == 0) {
			pthread_cond_timedwait(&(pSender->cond), &(pSender->mutex), &timeout);
		}
		pthread_mutex_unlock(&(pSender->mutex));

                if (pSender->iResponseLen == 0) {
                        //timeout
                        if (!m_HashTable.delFromTable(pSender->iSeqNo)) {
				//Processed
				pthread_mutex_lock(&(pSender->mutex));
				if (pSender->iResponseLen == 0) {
					pthread_cond_wait(&(pSender->cond), &(pSender->mutex));
				}
				pthread_mutex_unlock(&(pSender->mutex));
			}
                }
	}
	else
	{
		int x = errno;
		printf("warning: sendto returned %d\n", r);
		printf("error number: %d (%s)\n", x, strerror(x));
		printf("socket: %d\n", m_iSocketFd);
		printf("pc %ld len %d\n", (long)pcMessage, iLen);
		printf("sockaddr len: %ld\n", sizeof(addr));

		m_HashTable.delFromTable(pSender->iSeqNo);
		pSender->iResponseLen = -7;
	}

	*pcResponse = pSender->pcResponse;
	int iResponseLen = pSender->iResponseLen;

	//Fix linked list
	pthread_mutex_lock(&m_Mutex);

	pSender->iNextUnusedSenderInfo = m_iFirstUnusedSenderInfo;
	m_iFirstUnusedSenderInfo = iInfoId; 

	pthread_mutex_unlock(&m_Mutex);

#ifdef PRINT_TIMING
	struct timeval done;
	gettimeofday(&done, NULL);
	int microseconds = (done.tv_sec - now.tv_sec) * 1000000 +
                           (done.tv_usec - now.tv_usec);
	if (microseconds > 10000) {
		printf("SendTo done: %d us\n", microseconds);
	}
#endif //PRINT_TIMING

	return iResponseLen;	
}

//! Poll function
void* CSyncUDPQuery::PollFunction(void* arg)
{
	CSyncUDPQuery* pThis = (CSyncUDPQuery*)arg;

	struct pollfd pPollFDs[2];

	//UDP Socket
	pPollFDs[0].fd = pThis->m_iSocketFd;
	pPollFDs[0].events = POLLIN;
	//stop socket 
	pPollFDs[1].fd = pThis->m_iStopReadFd;
	pPollFDs[1].events = POLLIN;

	while (!pThis->m_bStopPollThread) {
		::poll(pPollFDs, 2, -1);

		if (pPollFDs[0].revents) {
			if (pPollFDs[0].revents & POLLIN) {
				//Read datagram
				char buf[65536];
				int recv_result 
				  = ::recv(pPollFDs[0].fd, buf, 65536, 0);

				if (recv_result < QM_LENGTH) {
                                        if (recv_result > 0) {
  						printf("Packet too short\n");
					}
					else {
						printf("Sync UDP Query recv returned %d\n", recv_result);
					}
				}
				else {
					//payload length
					unsigned short payload_length;
					memcpy(&payload_length, &(buf[2]), 2);
					payload_length = ntohs(payload_length);
					int size = CM_LENGTH + payload_length;

					if (recv_result < size) {
						printf("Packet too small\n");	
					}
					else {
						char* pcResponse = new char[size];
						memcpy(pcResponse, buf, size);

						u_int32_t uiSeqNo;
						memcpy(&uiSeqNo, &(pcResponse[QM_LENGTH-4]), 4);
						uiSeqNo = ntohl(uiSeqNo);

						SenderInfo** pValue = NULL;

                                                (pThis->m_HashTable).getFromTable(uiSeqNo, pValue);

						if (!(pThis->m_HashTable).delFromTable(uiSeqNo)) {
							//printf("Failed to match sequence number to sender\n");
							delete [] pcResponse;
						}
						else {
							SenderInfo* pSender = *pValue;

							if (recv_result == size) {
								pSender->pcResponse = pcResponse;

								pthread_mutex_lock(&(pSender->mutex));
								pSender->iResponseLen = size;
								pthread_mutex_unlock(&(pSender->mutex));
							}
							else {
								delete [] pcResponse;

								pthread_mutex_lock(&(pSender->mutex));
								pSender->iResponseLen = -8; //error
								pthread_mutex_unlock(&(pSender->mutex));
							}

							pthread_cond_signal(&(pSender->cond));
						}
					}
				}
			}
		}

		if (pPollFDs[1].revents) {
			if (pPollFDs[1].revents & POLLIN) {
				pThis->m_bStopPollThread = true;
			}
		}
	} //while

	return NULL;
}
	

