// ---------------------------------------------------------------------------
// File:       QueryMessage.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Base class for network messages.  Be very careful here
//             since alignment and padding are platform dependent!
//
// Change Log:
// ---------------------------------------------------------------------------

#ifndef QUERY_MESSAGE_H
#define QUERY_MESSAGE_H

#include "Message.h"

#define QM_LENGTH (CM_LENGTH + 4)
#define S_CMD_GET 1
#define S_CMD_PUT 2

#define S_VAL_ERROR   0
#define S_VAL_REQUEST 50
#define S_VAL_SUCCESS 100
#define S_VAL_USER_NOT_FOUND 204

class QueryMessage : public CommandMessage
{
public:
	QueryMessage();
	QueryMessage(int iMessageType);
	virtual ~QueryMessage();

	// Convert to/from network byte order
	virtual void serialize(std::string& sBuf) const;
	virtual int  deserialize(const std::string& sBuf);

	virtual u_int16_t payload_length(void) const { return 4; }

#ifndef _WIN32_WCE
	friend std::ostream& operator<<(std::ostream& s, const CommandMessage& v);
#endif

	u_int32_t m_u32SeqNo;
};


#endif // QUERY_MESSAGE_H
