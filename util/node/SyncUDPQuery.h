// SyncUDPQuery.h
// Interface for the CSyncUDPQuery class

#ifndef _SYNC_UDP_QUERY_H_
#define _SYNC_UDP_QUERY_H_

#include <pthread.h>
#include <string>
#include "inthashtable.h"
#include <unistd.h>

using namespace std;

class CSyncUDPQuery
{
public:
	//! Constructor
	//! Don't bind to address:port if
	//!   1) sAddress is empty
	//!   2) sAddress is "any"
	//!   3) iPort is 0
	CSyncUDPQuery(const string& sAddress, int iPort, int iMaxThreads);
	
	//! Destructor
	~CSyncUDPQuery();

	//! Start
	void Start();

	//! Stop
	void Stop();

	//! Returns length of Response message, or 0 for timeout, -1 for error
	//! The sequence number in pcMessage is changed
	int SendTo(const string& sToAddress, int iToPort, 
		char* pcMessage, int iLen, char** pcResponse, 
		int timeout_sec, int timeout_nsec);

protected:
	//! Poll function
	static void* PollFunction(void* arg);
	
	struct SenderInfo {
		int iSeqNo;
		char* pcResponse;
		int iResponseLen;
		pthread_mutex_t mutex;
		pthread_cond_t cond;
		int iNextUnusedSenderInfo;
	};

	SenderInfo* m_pSenders;
	int m_iNumSenders;
	int m_iFirstUnusedSenderInfo;

	IntHashTableMT<SenderInfo**> m_HashTable;

	pthread_mutex_t m_Mutex;
	int m_iSeqNo;

	string m_sAddress;
	unsigned short m_usPort;
	int m_iMaxThreads;

	pthread_t m_PollThread;
	bool m_bStopPollThread;
	int m_iStopReadFd;
	int m_iStopWriteFd;

	int m_iSocketFd;
};

#endif //_SYNC_UDP_QUERY_H_

