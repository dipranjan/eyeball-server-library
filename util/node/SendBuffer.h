// ---------------------------------------------------------------------------
// File:       SendBuffer.h
//
// Copyright:  Eyeball Networks Inc. 2003
//
// Author:     Jozsef Vass
//
// Purpose:    Send Buffer class - from Windows, separated from TCP message
//
// Change Log: 08/01/2001 - Created
// ---------------------------------------------------------------------------

#ifndef __SENDBUFFER__
#define __SENDBUFFER__

#include <string>
#include <queue>

class SendBuffer
{
public:
	SendBuffer();

	void AppendPacket(const std::string& sBuf);
	void Clear(void);
	bool Empty(void) const;
	bool GetPacket(std::string& sBuf) const;
	void RemoveBytes(unsigned int nLen);
	unsigned int Size(void) const;
	unsigned int PacketNumber(void) const;

private:
	unsigned int m_nSize;
	std::queue<std::string> m_DataQ;
};
#endif // __SENDBUFFER__
