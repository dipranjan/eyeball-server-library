// ---------------------------------------------------------------------------
// File:       License.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Provides the License class
//
// Change Log: 06/13/2001
// ---------------------------------------------------------------------------

#ifndef __LICENSE__
#define __LICENSE__

#include "LicenseServerStub.h"

#include <string>
#include <iostream>
#include <vector>

/**
 * Interface class to send messages to parent class
 */
struct ILicenseParent
{
	/**
	 * Destuctor.
	 */
	virtual ~ILicenseParent() {};

	/**
	 * Fired on severe license violation.
	*/
	virtual void OnLicenseExpired() = 0;
};

enum LicenseConfKey
{
	LICENSE_NULL_KEY = 0,
	LICENSE_EXPIRATION,
	LICENSE_SERVER_MAC,
	LICENSE_NAME,

	// specfic to videosessiond
	LICENSE_EB_SERVER_VERSION,
	LICENSE_EB_MAX_CONCURRENT_USERS,
	LICENSE_EB_CONCURRENT_USERS_HARD_LIMIT,
	LICENSE_EB_MAX_TOTAL_USERS,
	LICENSE_EB_TOTAL_USERS_HARD_LIMIT,
	LICENSE_EB_WHITEBOARD,
	LICENSE_EB_MULTIPARTY,
	LICENSE_EB_PEERTOPEER,
	LICENSE_EB_CLIENTSERVER,

	// specific to instantmsgd
	LICENSE_EC_SERVER_VERSION,
	LICENSE_EC_MAX_CONCURRENT_USERS,
	LICENSE_EC_CONCURRENT_USERS_HARD_LIMIT,
	LICENSE_EC_MAX_TOTAL_USERS,
	LICENSE_EC_TOTAL_USERS_HARD_LIMIT,
	LICENSE_EC_MULTIPARTY,
	LICENSE_EC_IM_INTEROPERABILITY,
	LICENSE_EC_VIDEOMESSAGING,

	// specific to videomaild
	LICENSE_VM_SERVER_VERSION,
	LICENSE_VM_MAX_TOTAL_USERS,
	LICENSE_VM_TOTAL_USERS_HARD_LIMIT,

	// specific to chatroomd
	LICENSE_CR_SERVER_VERSION,
};

/**
 * License configuration parameters in file.
 */
struct LicenseConfigParameters
{
	LicenseConfigParameters();

	int m_iExpiration;
	std::string m_sServerMac;
	std::string m_sLicenseName;

	// specific videosessiond
	std::string m_sEbServerVersion;
	int m_iEbMaxConcurrentUsers;
	int m_iEbConcurrentUsersHardLimit;
	int m_iEbMaxTotalUsers;
	int m_iEbTotalUsersHardLimit;
	bool m_bEbWhiteboard;
	bool m_bEbMultiparty;
	bool m_bEbPeerToPeer;
	bool m_bEbClientServer;

	// specific to instantmsgd
	std::string m_sEcServerVersion;
	int m_iEcMaxConcurrentUsers;
	int m_iEcConcurrentUsersHardLimit;
	int m_iEcMaxTotalUsers;
	int m_iEcTotalUsersHardLimit;
	bool m_bEcMultiparty;
	bool m_bEcImInteroperability;
	bool m_bEcVideoMessaging;

	//specific to videomaild
	std::string m_sVmServerVersion;
	int m_iVmMaxTotalUsers;
	int m_iVmTotalUsersHardLimit;

	// specific to chatroomd
	std::string m_sCrServerVersion;
};

/**
 * Read and validate license configuration from a file.  This is simlar to
 * server configuration.
 */
class LicenseConfig
{
public:
	LicenseConfig();

	virtual ~LicenseConfig() {}

	bool Read(const std::string& sLicenseFile);

	LicenseConfigParameters Get() const { return m_LicenseConfigParameters; }

	bool Validate(const std::string& sLicenseName, const std::string& sVersion);

private:
   LicenseConfKey look_for_key(const std::string& sKey);
   void assign_value(LicenseConfKey _current_key, const std::string& _value);
	std::vector<std::string> parse_array(void);
	bool Decrypt(const std::string& sFileName, std::string& sFile);
	std::vector<std::string> read_tokens_from_string(const std::string& sString);
	bool read_line(std::string& sString, std::string& sLine);

private:
	std::vector<std::string> tokens;
	std::vector<std::string>::const_iterator racer;
	LicenseConfigParameters m_LicenseConfigParameters;
};

/**
 * License class.
 */
class LicenseInterface
{
public:
	/**
	 * Constructor.
	 */
	LicenseInterface();

	/**
	 * Destructor.
	 */
	~LicenseInterface();

	/**
	 * License initialization. License is not requested, only parameters
	 * set.
	 *
	 * @param sLicenseName License name
	 * @param sLicenseCert Client private key/certificate file
	 * @param sEyeballCert Eyeball Network's certificate
	 * @param iDuration    Lease duration
	 * @param iRenewTime   Lease renewal period
	 * @param sLicenseFile Name of licese file
	 * @param sVersion     Server version and build date
	 */
	void Init(
		const std::string& sLicenseName,
		const std::string& sLicenseCert,
		const std::string& sEyeballCert,
		int iDuration,
		int iRenewTime,
		const std::string& sLicenseFile,
		const std::string& sVersion);

	/**
	 * Set parent of interface.
	 *
	 * parent Pointer to parent.
	 */
	void SetParent(ILicenseParent* parent) {m_pParent = parent;}

	bool IsExpired() { return ( m_iFailedAttempt <= 0 ); };

	/**
	 * Obtain license. Fails if license aready leased or cannot be leased.
	 *
	 * @out 0 on success, -1 on failure.
	 */
	int GetLicense();

	/**
	 * Check whether license is valid.  License is leased and released.
	 * Fails if license does not exist.
	 *
	 * @out 0 on success, -1 on failure.
	 */
	int CheckLicense();

	/**
	 * Release license. Fails if license not leased.
	 *
	 * @out 0 on success, -1 on failure.
	 */
	int ReleaseLicense();

	/**
	 * Sends message to license server.  Messages are buffered and sent along
	 * with a license renewal.
	 *
	 * @param sMessage Message to be sent to license server
	 */
	void SendMessage(const std::string& sMessage) const;

	/**
	 * Checks whether the license is leased.
	 *
	 * @out true if license is leased, false otherwise.
	 */
	bool IsOpen() const { return m_bOpen;}
	
	/**
	 * Checks whether the license server is connecting.
	 * 
	 * @out true is license server is connecting, false otherwise.
	 */
    bool IsLicenseConnected();              	

	/**
	 * Check whether offline license
	 */
	bool IsOffline() const { return !m_sLicenseFile.empty(); }

	/**
	 * Read the license file. If file cannot be read, it will print warning
	 * and calls LicenseExpiredCallback() after a given number of failed
	 * attempts.
	 */
	void ReReadOfflineLicense();

private:
   static void LicenseExpiredCallback(void* arg);

	static void LicenseLogCallback(LSC_LogLevel level, const char* mess);

	/**
	 * Opens online license. Connects to Eyeball Monitoring Service.
	 */
   int OpenLicense(bool bForce, bool bAscyn);

	/**
	 * Opens offline license. Reads the license file and checks license
	 * valididty by server version, expiration data and MAC address.
	 */
	int OpenLicense();

protected:
	LicenseServerStub* m_pLicense;
	LicenseConfigParameters m_LicenseConfigParameters;
	std::string m_sLicenseName;

private:
	std::string m_sLicenseFile;
	std::string m_sVersion;

	std::string m_sLicenseCert;
	std::string m_sEyeballCert;

	int m_iDuration;
	int m_iRenewTime;

	bool m_bOpen;

	ILicenseParent *m_pParent;

	int m_iFailedAttempt;
};
#endif
