// ---------------------------------------------------------------------------
// File:       SendBuffer.cc
//
// Copyright:  Eyeball Networks Inc. 2003
//
// Author:     Jozsef Vass
//
// Purpose:    SendBuffer message class, separated from TcpMessage
//
// Change Log: 07/18/2003 - Created
// ---------------------------------------------------------------------------

#include "SendBuffer.h"

#include "util.h"

using std::string;
using std::queue;

SendBuffer::SendBuffer() :
	m_nSize(0),
	m_DataQ()
{}

void SendBuffer::AppendPacket(const string& sBuf)
{
	m_DataQ.push(sBuf);
	m_nSize += sBuf.size();
}

void SendBuffer::Clear()
{
	while (!m_DataQ.empty())
	{
		m_DataQ.pop();
	}

	m_nSize = 0;
}

bool SendBuffer::Empty() const
{
	return (0 == m_nSize);
}

bool SendBuffer::GetPacket(string& sBuf) const
{
	if (m_DataQ.empty())
		return false;

	sBuf = m_DataQ.front();

	return true;
}

void SendBuffer::RemoveBytes(unsigned int nLen)
{
	int iBytesRemaining = nLen;
	while (iBytesRemaining > 0)
	{
		string& sBuf = m_DataQ.front();
		int nErase = MIN((int)sBuf.size(), iBytesRemaining);
		
		string sTemp = sBuf.substr(nErase);
		sBuf = sTemp;
		
		if (sBuf.empty())
		{
			m_DataQ.pop();
		}
		iBytesRemaining -= nErase;
		m_nSize -= nErase;
	}
}

unsigned int SendBuffer::Size(void) const
{
	return m_nSize;
}

unsigned int SendBuffer::PacketNumber(void) const
{
	return m_DataQ.size();
}
