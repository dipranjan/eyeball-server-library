// ---------------------------------------------------------------------------
// File:       Message.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Base class for network messages.  Be very careful here
//             since alignment and padding are platform dependent!
//
// Change Log:
// ---------------------------------------------------------------------------

#ifndef EC_MESSAGE_H
#define EC_MESSAGE_H

#include <string>
using std::string; 

#include <iostream>
using std::ostream;

#include "utypes.h"
#include <netinet/in.h>

#include "Serializable.h"

// ---------------------------------------------------------------------------
// Defines
// ---------------------------------------------------------------------------

#define EYEBALL_CHAT_LE_09      4
#define EYEBALL_CHAT_LE_10      6
#define EYEBALL_CHAT_LE_11      7
#define EYEBALL_CHAT_LE_12      8
#define EYEBALL_CHAT_20         9
#define EYEBALL_CHAT_22         10
#define EYEBALL_CHAT_23         11


#define EYEBALL_GROUPCHAT_01    128 
#define EYEBALL_GROUPCHAT_02    129 
 

#define EYEBALL_VIDEOMESSAGE_01 196	  

#define EYEBALL_CHATROOM		152
#define EYEBALL_CHATROOM_UNICODE	153


#define EYEBALL_IM              160

#define EYEBALL_SERVER_STOP             170
#define EYEBALL_SERVER_CLOSE_CONN       171
#define EYEBALL_SERVER_SEND_TO          172
#define EYEBALL_SERVER_QUERY            173
#define EYEBALL_SERVER_STATE            174
#define EYEBALL_SERVER_FORWARD_EDGE     175
#define EYEBALL_SERVER_PASSWORD         176
#define EYEBALL_SERVER_XMPP_RESOURCE    177
#define EYEBALL_SERVER_XMPP_RESOURCE_LIST 178
#define EYEBALL_SERVER_XMPP_CONTACTS    179
#define EYEBALL_SERVER_XMPP_BLOCK       180
#define EYEBALL_SERVER_XMPP_BLOCK_LIST  181

#define MESSAGE_VERSION         EYEBALL_CHAT_23

// This field indicates the client's product version.
#define PRODUCT_VERSION  (210)


// ---------------------------------------------------------------------------
// Serializable interface
// ---------------------------------------------------------------------------

// Note: use CM_LENGTH instead of sizeof(CommandMessage), as sizeof takes
// into account the v-table.
#define CM_LENGTH					8		// bytes

#define CM_GET_VERSION(BUF)			((u_int8_t *)BUF)[0]
#define CM_GET_MESSAGE_TYPE(BUF)	((u_int8_t *)BUF)[1]
#define CM_GET_PAYLOAD_LENGTH(BUF)	ntohs(((u_int16_t *)BUF)[1])

struct CommandMessage : public ISerializable
{
	CommandMessage();
	CommandMessage(u_int8_t _u8MessageType);
	CommandMessage(
		u_int8_t _u8MessageType,
		u_int16_t _u16Command,
		u_int16_t _u16Value);
	virtual ~CommandMessage();

	// Convert to/from network byte order
	virtual void serialize(string& sBuf) const;
	virtual int  deserialize(const string& sBuf);

	virtual u_int16_t payload_length(void) const { return 0; }
	virtual unsigned int size(void) const;

	friend ostream& operator<<(ostream& s, const CommandMessage& v);

	static u_int8_t MessageType(const string& sBuf)
	{
		if (sBuf.size() < CM_LENGTH)
			return 0;

		return sBuf[1];
	}

	u_int8_t  m_u8Version;
	u_int8_t  m_u8MessageType;
	u_int16_t m_u16PayloadLength;

	u_int16_t m_u16Command;
	u_int16_t m_u16Value;
};


#endif // EC_MESSAGE_H
