// ---------------------------------------------------------------------------
// File:       Serializable.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Base class for serializable objects
//
// Change Log:
// ---------------------------------------------------------------------------

#ifndef SERIALIZABLE_H
#define SERIALIZABLE_H

#include "utypes.h"
#include <string>
using std::string;
#include <vector>
using std::vector;


// ---------------------------------------------------------------------------
// Serializable interface
// ---------------------------------------------------------------------------

class ISerializable
{
public:
	virtual ~ISerializable() {}

	virtual void serialize(std::string& sBuf) const = 0;
	virtual int deserialize(const std::string& sBuf) = 0;
};



// ---------------------------------------------------------------------------
// Serialization/Deserialization functions
// ---------------------------------------------------------------------------

void SerializeBytes(const void* p, unsigned int uSize, std::string& sBuf);
void Serialize8(u_int8_t u8, std::string& sBuf);
void Serialize16(u_int16_t u16, std::string& sBuf);
void Serialize32(u_int32_t u32, std::string& sBuf);
void Serialize64(u_int64_t u64, std::string& sBuf);
void SerializeCString(const char* p, unsigned int uSize, std::string& sBuf);
void SerializeString8(const std::string& s, std::string& sBuf);
void SerializeString16(const std::string& s, std::string& sBuf);
void SerializeString32(const std::string& s, std::string& sBuf);
void SerializeStringVector16(const vector<std::string>& s, std::string& sBuf);
int GetSerializeStringVector16Length(const vector<std::string>& s);
void Serialize8Vector16(const vector<u_int8_t>& s, string& sBuf);
int GetSerialize8Vector16Length(const vector<u_int8_t>& s);


bool DeserializeBytes(
	void* p, 
	unsigned int uSize, 
	const std::string& sBuf, 
	int& iOffset);
bool Deserialize8(u_int8_t& u8, const std::string& sBuf, int& iOffset);
bool Deserialize16(u_int16_t& u16, const std::string& sBuf, int& iOffset);
bool Deserialize32(u_int32_t& u32, const std::string& sBuf, int& iOffset);
bool Deserialize64(u_int64_t& u64, const std::string& sBuf, int& iOffset);
bool DeserializeCString(
	char* p, 
	unsigned int uSize, 
	const std::string& sBuf, 
	int& iOffset);
bool DeserializeString8(
	std::string& s, 
	const std::string& sBuf, 
	int& iOffset);
bool DeserializeString16(
	std::string& s, 
	const std::string& sBuf, 
	int& iOffset);
bool DeserializeString32(
	std::string& s, 
	const std::string& sBuf, 
	int& iOffset);
bool DeserializeStringVector16(
        vector<std::string>& s, 
        const std::string& sBuf,
        int& iOffset);
bool Deserialize8Vector16(
        vector<u_int8_t>& s,
        const string& sBuf,
        int& iOffset);


// ---------------------------------------------------------------------------
// Helper macros
// ---------------------------------------------------------------------------

#define SERIALIZE_BYTES(X, Y) \	SerializeBytes(X, Y, sBuf)

#define SERIALIZE8(X) \	Serialize8(X, sBuf)

#define SERIALIZE16(X) \	Serialize16(X, sBuf)

#define SERIALIZE32(X) \	Serialize32(X, sBuf)

#define SERIALIZE_C_STR(X) \	SerializeCString(X, sizeof(X), sBuf)

#define SERIALIZE_BASE_CLASS(X) \	X::serialize(sBuf)

#define SERIALIZE_STR8(X) \	SerializeString8(X, sBuf)

#define SERIALIZE_STR16(X) \	SerializeString16(X, sBuf)

#define SERIALIZE_STR32(X) \
	SerializeString32(X, sBuf)

#define SERIALIZE_VAR255_STR(X) \
	SERIALIZE_STR8(X)

#define SERIALIZE_STR(X) \
	SERIALIZE_STR16(X)

#define DESERIALIZE_BYTES(X, Y) \
	{ if (!DeserializeBytes(X, Y, sBuf, iOffset)) return -1; }

#define DESERIALIZE8(X) \
	{ if (!Deserialize8(X, sBuf, iOffset)) return -1; }

#define DESERIALIZE16(X) \
	{ if (!Deserialize16(X, sBuf, iOffset)) return -1; }

#define DESERIALIZE32(X) \
	{ if (!Deserialize32(X, sBuf, iOffset)) return -1; }

#define DESERIALIZE64(X) \
	{ if (!Deserialize64(X, sBuf, iOffset)) return -1; }

#define DESERIALIZE_C_STR(X) \
	{ if (!DeserializeCString(X, sizeof(X), sBuf, iOffset)) return -1; }

#define DESERIALIZE_BASE_CLASS(X) \
	{ \
		int iTemp = X::deserialize(sBuf.substr(iOffset)); \
		if (iTemp < 0) \
			return iTemp; \
		iOffset += iTemp; \
	}

#define DESERIALIZE_STR8(X) \
	{ if (!DeserializeString8(X, sBuf, iOffset)) return -1; }

#define DESERIALIZE_STR16(X) \
	{ if (!DeserializeString16(X, sBuf, iOffset)) return -1; }

#define DESERIALIZE_STR32(X) \
	{ if (!DeserializeString32(X, sBuf, iOffset)) return -1; }

#define DESERIALIZE_VAR255_STR(X) \
	DESERIALIZE_STR8(X)

#define DESERIALIZE_STR(X) \
	DESERIALIZE_STR16(X)


#endif //SERIALIZABLE_H
