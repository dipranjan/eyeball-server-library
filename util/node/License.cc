// ---------------------------------------------------------------------------
// File:       License.cc
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Provides the License class
//
// Change Log: 11/05/2001 - CheckLicense() and logging
// ---------------------------------------------------------------------------

#include "License.h"

#include "log.h"
#include "debug.h"
#include "SslMessage.h"
#include "TcpMessage.h"
#include "poll.h"
#include "util.h"
#include "file.h"
#include "StringTokenizer.h"
#include "inet.h"
#include "simple_crypt.h"

#include <time.h>
#include <string.h>

using std::string;
using std::vector;
using namespace::std;

#define FAILED_ATTEMPT 168

LicenseInterface::LicenseInterface() :
	m_pLicense(new LicenseServerStub),
	m_LicenseConfigParameters(),
	m_sLicenseName(),
	m_sLicenseFile(),
	m_sVersion(),
	m_sLicenseCert(),
	m_sEyeballCert(),
	m_iDuration(0),
	m_iRenewTime(0),
	m_bOpen(false),
	m_pParent(NULL),
	m_iFailedAttempt(FAILED_ATTEMPT)
{}

LicenseInterface::~LicenseInterface()
{
	SAFE_DELETE(m_pLicense);
}

void LicenseInterface::LicenseExpiredCallback(void *arg)
{
	LOG("Unable to validate license");
	cerr << "Unable to validate license" << endl;

        if (arg != NULL) {
		LicenseInterface *pThis = (LicenseInterface*) arg;
		if (pThis->m_pParent != NULL) {
			pThis->m_pParent->OnLicenseExpired();
		}
	}
}

void LicenseInterface::LicenseLogCallback(
	LSC_LogLevel level,
	const char* mess)
{
	switch (level)
	{
		case LSC_Error:
			LOG("License error: " << mess);
			cerr << "License error: " << mess << endl;
			break;

		case LSC_Info:
			LOGL(2, "License info: " << mess);
			break;

		case LSC_Debug:
			LOGL(4, "License debug info:" << mess);
			break;
	}
}

void LicenseInterface::Init(
	const string& sLicenseName,
	const string& sLicenseCert,
	const string& sEyeballCert,
	int iDuration,
	int iRenewTime,
	const string& sLicenseFile,
	const string& sVersion)
{
	m_sLicenseName = sLicenseName;
	m_sLicenseCert = sLicenseCert;
	m_sEyeballCert = sEyeballCert;
	m_iDuration    = iDuration;
	m_iRenewTime   = iRenewTime;
	m_sLicenseFile = sLicenseFile;
	m_sVersion = sVersion;

	LSC_setLogCallback(&LicenseLogCallback);
}

int LicenseInterface::GetLicense()
{
	if( IsOpen() )
		return -1;

	if( IsOffline() )
	{
		if( OpenLicense() < 0 )
			return -1;
	}
	else
	{
		if( OpenLicense(true, true) < 0 )
			return -1;
	}

	m_bOpen = true;

	return 0;
}

int LicenseInterface::CheckLicense()
{
	if( IsOpen() )
		return -1;

	if( IsOffline() )
	{
		if( OpenLicense() < 0 )
			return -1;
	}
	else
	{
		if( OpenLicense(true, false) < 0 )
			return -1;

		m_pLicense->close();
	}

	return 0;
}

int LicenseInterface::ReleaseLicense()
{
	if( !IsOpen() )
		return -1;

	if( !IsOffline() )
		m_pLicense->close();

	m_bOpen = false;

	return 0;
}

int LicenseInterface::OpenLicense(bool bForce, bool bAsync)
{
	if( IsOffline() )
		return -1;

	if( bAsync )
		m_pLicense->setAsyncRenewal(true, LicenseExpiredCallback, this);
	else
		m_pLicense->setAsyncRenewal(false);

	m_pLicense->setLeaseDuration(m_iDuration);
	m_pLicense->setMaxRenewPeriod(m_iRenewTime);
	m_pLicense->setForcedLease(bForce);
	if (!m_pLicense->setCertificateFile2(m_sLicenseCert.c_str()))
		return -1;
	if (!m_pLicense->setCAFile2(m_sEyeballCert.c_str()))
		return -1;
	m_pLicense->setServerHosts("ls1.eyeball.com;ls2.eyeball.com;ls3.eyeball.com");//ls4.eyeball.com");ls1internal.eyeball.com;ls2internal.eyeball.com;ls3internal.eyeball.com");

	try 
	{
		m_pLicense->open(m_sLicenseName, "");
	} 
	catch (LSC_IOException ex) 
	{
		LOG("IOException: " << ex.getDetails());
		cerr << "IOException: " << ex.getDetails() << endl;
		return -1;
	} 
	catch (LSC_LicenseUnavailableException ex) 
	{
		LOG("LicenseUnavailableException: " << ex.getDetails());
		cerr << "LicenseUnavailableException: " << ex.getDetails() << endl;
		return -1;
	}

	return 0;
}

bool LicenseInterface::IsLicenseConnected()
{
 return m_pLicense->isLicenseConnected();
}

int LicenseInterface::OpenLicense()
{
	if( !IsOffline() )
		return -1;

	LicenseConfig lc;

	if( !lc.Read(m_sLicenseFile) )
	{
		LOG("Cannot read license file " << m_sLicenseFile);
		cerr << "Cannot read license file " << m_sLicenseFile << endl;
		return -1;
	}

	if( !lc.Validate(m_sLicenseName, m_sVersion) )
	{
		LOG("Cannot validate license");
		cerr << "Cannot validate license" << endl;
		return -1;
	}

	m_LicenseConfigParameters = lc.Get();

	return 0;
}

void LicenseInterface::SendMessage(const string& sMessage) const
{
	if( !IsOffline() )
		m_pLicense->remoteLog(sMessage.c_str());
}

void LicenseInterface::ReReadOfflineLicense()
{

	if( !IsOpen() ){
		return;
	}

	if( !IsOffline() ){
    	return;
	}

	if( OpenLicense() < 0 )
	{
		LOG("Critical error.  Please contact Eyeball Networks Inc. immediately.  You have " << m_iFailedAttempt << " hour(s) to fix the problem.  If the problem is not fixed, the server will shut down!");
		cerr << "Critical error.  Please contact Eyeball Networks Inc. immediately.  You have " << m_iFailedAttempt << " hour(s) to fix the problem.  If the problem is not fixed, the server will shut down!" << endl;

		m_iFailedAttempt--;

		if( m_iFailedAttempt <= 0 ) {
			if (m_pParent != NULL) {
				m_pParent->OnLicenseExpired();
			}
		}
	}
	else{
		m_iFailedAttempt = FAILED_ATTEMPT;
	}
}

LicenseConfigParameters::LicenseConfigParameters() :
	m_iExpiration(0),
	m_sServerMac(),
	m_sLicenseName(),
	m_sEbServerVersion(),
	m_iEbMaxConcurrentUsers(2),
	m_iEbConcurrentUsersHardLimit(3),
	m_iEbMaxTotalUsers(100),
	m_iEbTotalUsersHardLimit(200),
	m_bEbWhiteboard(false),
	m_bEbMultiparty(false),
	m_bEbPeerToPeer(false),
	m_bEbClientServer(false),
	m_sEcServerVersion(),
	m_iEcMaxConcurrentUsers(2),
	m_iEcConcurrentUsersHardLimit(3),
	m_iEcMaxTotalUsers(100),
	m_iEcTotalUsersHardLimit(200),
	m_bEcMultiparty(false),
	m_bEcImInteroperability(false),
	m_bEcVideoMessaging(false),
	m_sVmServerVersion(),
	m_iVmMaxTotalUsers(100),
	m_iVmTotalUsersHardLimit(200),
	m_sCrServerVersion()
{}

LicenseConfig::LicenseConfig() :
	tokens(),
	racer(),
	m_LicenseConfigParameters()
{}

#define LOOK(X, Y) \
	if( sKey == X ) \
		return Y;

LicenseConfKey LicenseConfig::look_for_key(const string& sKey)
{
	LOOK("Expiration", LICENSE_EXPIRATION);
	LOOK("ServerMAC", LICENSE_SERVER_MAC);
	LOOK("LicenseName", LICENSE_NAME);

	LOOK("EbServerVersion", LICENSE_EB_SERVER_VERSION);
	LOOK("EbMaxConcurrentUsers", LICENSE_EB_MAX_CONCURRENT_USERS);
	LOOK("EbConcurrentUsersHardLimit", LICENSE_EB_CONCURRENT_USERS_HARD_LIMIT);
	LOOK("EbMaxTotalUsers", LICENSE_EB_MAX_TOTAL_USERS);
	LOOK("EbTotalUsersHardLimit", LICENSE_EB_TOTAL_USERS_HARD_LIMIT);
	LOOK("EbWhiteboard", LICENSE_EB_WHITEBOARD);
	LOOK("EbMultiparty", LICENSE_EB_MULTIPARTY);
	LOOK("EbPeerToPeer", LICENSE_EB_PEERTOPEER);
	LOOK("EbClientServer", LICENSE_EB_CLIENTSERVER);

	LOOK("EcServerVersion", LICENSE_EC_SERVER_VERSION);
	LOOK("EcMaxConcurrentUsers", LICENSE_EC_MAX_CONCURRENT_USERS);
	LOOK("EcConcurrentUsersHardLimit", LICENSE_EC_CONCURRENT_USERS_HARD_LIMIT);
	LOOK("EcMaxTotalUsers", LICENSE_EC_MAX_TOTAL_USERS);
	LOOK("EcTotalUsersHardLimit", LICENSE_EC_TOTAL_USERS_HARD_LIMIT);
	LOOK("EcMultiparty", LICENSE_EC_MULTIPARTY);
	LOOK("EcImInteroperability", LICENSE_EC_IM_INTEROPERABILITY);
	LOOK("EcVideoMessaging", LICENSE_EC_VIDEOMESSAGING);
	
	LOOK("VmServerVersion", LICENSE_VM_SERVER_VERSION);
	LOOK("VmMaxTotalUsers", LICENSE_VM_MAX_TOTAL_USERS);
	LOOK("VmTotalUsersHardLimit", LICENSE_VM_TOTAL_USERS_HARD_LIMIT);

	LOOK("CrServerVersion", LICENSE_CR_SERVER_VERSION);

	cerr << "license error: unknown key \"" << sKey << "\"" << endl;
   return LICENSE_NULL_KEY;
}

#define ASSIGN_BOOL(X, Y)\
	case X: \
		m_LicenseConfigParameters.Y = !strcmp("yes", tolower(_value).c_str()); \
		break;

#define ASSIGN_INT(X, Y) \
	case X: \
		m_LicenseConfigParameters.Y = atoi(_value.c_str()); \
		break;

#define ASSIGN_STR(X, Y) \
	case X: \
		m_LicenseConfigParameters.Y = _value; \
		break;


void LicenseConfig::assign_value(
	LicenseConfKey _current_key, 
	const string& _value)
{
	switch (_current_key)
	{
		ASSIGN_INT(LICENSE_EXPIRATION, m_iExpiration);
		ASSIGN_STR(LICENSE_SERVER_MAC, m_sServerMac);
		ASSIGN_STR(LICENSE_NAME, m_sLicenseName);

		ASSIGN_STR(LICENSE_EB_SERVER_VERSION, m_sEbServerVersion);
		ASSIGN_INT(LICENSE_EB_MAX_CONCURRENT_USERS, m_iEbMaxConcurrentUsers);
		ASSIGN_INT(LICENSE_EB_CONCURRENT_USERS_HARD_LIMIT, m_iEbConcurrentUsersHardLimit);
		ASSIGN_INT(LICENSE_EB_MAX_TOTAL_USERS, m_iEbMaxTotalUsers);
		ASSIGN_INT(LICENSE_EB_TOTAL_USERS_HARD_LIMIT, m_iEbTotalUsersHardLimit);
		ASSIGN_BOOL(LICENSE_EB_WHITEBOARD, m_bEbWhiteboard);
		ASSIGN_BOOL(LICENSE_EB_MULTIPARTY, m_bEbMultiparty);
		ASSIGN_BOOL(LICENSE_EB_PEERTOPEER, m_bEbPeerToPeer);
		ASSIGN_BOOL(LICENSE_EB_CLIENTSERVER, m_bEbClientServer);
	
		ASSIGN_STR(LICENSE_EC_SERVER_VERSION, m_sEcServerVersion);
		ASSIGN_INT(LICENSE_EC_MAX_CONCURRENT_USERS, m_iEcMaxConcurrentUsers);
		ASSIGN_INT(LICENSE_EC_CONCURRENT_USERS_HARD_LIMIT, m_iEcConcurrentUsersHardLimit);
		ASSIGN_INT(LICENSE_EC_MAX_TOTAL_USERS, m_iEcMaxTotalUsers);
		ASSIGN_INT(LICENSE_EC_TOTAL_USERS_HARD_LIMIT, m_iEcTotalUsersHardLimit);
		ASSIGN_BOOL(LICENSE_EC_MULTIPARTY, m_bEcMultiparty);
		ASSIGN_BOOL(LICENSE_EC_IM_INTEROPERABILITY, m_bEcImInteroperability);
		ASSIGN_BOOL(LICENSE_EC_VIDEOMESSAGING, m_bEcVideoMessaging);

		ASSIGN_STR(LICENSE_VM_SERVER_VERSION, m_sVmServerVersion);
		ASSIGN_INT(LICENSE_VM_MAX_TOTAL_USERS, m_iVmMaxTotalUsers);
		ASSIGN_INT(LICENSE_VM_TOTAL_USERS_HARD_LIMIT, m_iVmTotalUsersHardLimit);

		ASSIGN_STR(LICENSE_CR_SERVER_VERSION, m_sCrServerVersion);

		default:
			break;
	}
}

vector<string> LicenseConfig::parse_array(void)
{
	vector<string> rc;

	// Should be on opening brace
	if (*racer != "[")
	{
		cerr << "Expected \"[\", got \"" << *racer << "\"" << endl;
		return rc;
	}

	// Skip it and continue parsing
	racer++;

	// Add tokens to return array until close brace or end of file
	while ((*racer != "]") && (racer != tokens.end()))
	{
		rc.push_back(*racer);
		racer++;
	}

	return rc;
}

bool LicenseConfig::Read(const string& sFileName)
{
	string sFile;
	//printf("before Decrypt\n"); //znn
	if( !Decrypt(sFileName, sFile) )
		return false;

	printf("After decrypt\n");	//znn
	tokens = read_tokens_from_string(sFile);

	if (!tokens.size())
	{
		// No tokens read from file.  Nothing in file?
		if (!File::is_regular(sFileName))
		{
			// If it's not a regular file, or does not exist, then return false
			return false;
		}

		// Otherwise keep going (empty file), and fall back on defaults
	}

	enum
	{
		KEY_STATE,
		EQUAL_STATE,
		VALUE_STATE
	};

	int state(KEY_STATE);
	LicenseConfKey current_key(LICENSE_NULL_KEY);

	for (racer = tokens.begin(); racer != tokens.end(); racer++)
	{
		const string& token = *racer;

		switch(state)
		{
			case KEY_STATE:
				if ((current_key = look_for_key(token)) != LICENSE_NULL_KEY)
					state = EQUAL_STATE;
				break;

			case EQUAL_STATE:
				if (token == "=")
				{
					state = VALUE_STATE;
				}
				else
				{
					cerr << "Expected \"=\", got \"" << token << "\"" << endl;

					// Back to looking for keys
					state = KEY_STATE;

					if ((current_key = look_for_key(token)) != LICENSE_NULL_KEY)
						state = EQUAL_STATE;
				}
				break;

			case VALUE_STATE:
				assign_value(current_key, token);
				state = KEY_STATE;
				break;

			default:
				ASSERT(0 && "Unknown internal state");
				break;
		}
	}

	return true;
}

bool LicenseConfig::Validate(
	const string& sLicenseName,
	const string& sVersion)
{
        string sLicense;
        if (sLicenseName.size() > 3) {
  	  sLicense = sLicenseName.substr(0, sLicenseName.size() - 3);
        }

	if( tolower(sLicense) != tolower(m_LicenseConfigParameters.m_sLicenseName))
	{
		LOG("License name " << sLicense << " does not match license name " << m_LicenseConfigParameters.m_sLicenseName << ".");
		cerr << "License name " << sLicense << " does not match license name " << m_LicenseConfigParameters.m_sLicenseName << "." << endl;
		return false;
	}

	string sServerVersion;
	if( sLicenseName.find("-eb") != string::npos )
		sServerVersion = m_LicenseConfigParameters.m_sEbServerVersion;
	else if( sLicenseName.find("-ec") != string::npos )
		sServerVersion = m_LicenseConfigParameters.m_sEcServerVersion;
	else if( sLicenseName.find("-cr") != string::npos )
		sServerVersion = m_LicenseConfigParameters.m_sCrServerVersion;
	else
		sServerVersion = m_LicenseConfigParameters.m_sVmServerVersion;

	if( skip_whitespace(sVersion) != skip_whitespace(sServerVersion) )
	{
		LOG("Server version " << sVersion << " does not match license version " << sServerVersion << ".");
		cerr << "Server version " << sVersion << " does not match license version " << sServerVersion  << "." << endl;
		return false;
	}

	size_t pos = sVersion.find_last_of("on");
	if( pos + 14 >= sVersion.size() )
	{
		LOG("Incorrect server version " << sVersion);
		cerr << "Incorrevt server version " << sVersion << endl;
		return false;
	}

	string sMonth, sDay, sYear;
	sMonth.assign(sVersion, pos + 2, 3);
	sDay.assign(sVersion, pos + 6, 2);
	sYear.assign(sVersion, pos + 9, 4);

	tm tmServer = {0, 0, 0, 0, 0, 0, 0, 0, 0,};
	tmServer.tm_mday = atoi(sDay.c_str());
	if( sMonth == "Jan" )
		tmServer.tm_mon = 0;
	else if( sMonth == "Feb" )
		tmServer.tm_mon = 1;
	else if( sMonth == "Mar" )
		tmServer.tm_mon = 2;
	else if( sMonth == "Apr" )
		tmServer.tm_mon = 3;
	else if( sMonth == "May" )
		tmServer.tm_mon = 4;
	else if( sMonth == "Jun" )
		tmServer.tm_mon = 5;
	else if( sMonth == "Jul" )
		tmServer.tm_mon = 6;
	else if( sMonth == "Aug" )
		tmServer.tm_mon = 7;
	else if( sMonth == "Sep" )
		tmServer.tm_mon = 8;
	else if( sMonth == "Oct" )
		tmServer.tm_mon = 9;
	else if( sMonth == "Nov" )
		tmServer.tm_mon = 10;
	else if( sMonth == "Dec" )
		tmServer.tm_mon = 11;
	else 
		tmServer.tm_mon = 0;
	tmServer.tm_year = atoi(sYear.c_str()) - 1900;

	time_t tCurrent = time(NULL);
	time_t tServer = mktime(&tmServer);

	if( tCurrent < tServer )
	{
		char szCurrent[32];
		char szServer[32];
                memset(szCurrent, 0, sizeof(szCurrent));
                memset(szServer, 0, sizeof(szServer));
		strftime(szCurrent, 31, "%Y-%m-%d", localtime(&tCurrent));
		strftime(szServer, 31, "%Y-%m-%d", localtime(&tServer));

                string sCurrent = szCurrent;
                string sServer = szServer;
		LOG("Current time " << sCurrent << " is before server build time " << sServer << ".");
		cerr << "Current time " << sCurrent << " is before server build time " << sServer << "." << endl;
		return false;
	}

	if( tCurrent >= tServer + m_LicenseConfigParameters.m_iExpiration * 86400 )
	{
		char szCurrent[32];
		char szServer[32];
                memset(szCurrent, 0, sizeof(szCurrent));
                memset(szServer, 0, sizeof(szServer));
		strftime(szCurrent, 31, "%Y-%m-%d", localtime(&tCurrent));
		strftime(szServer, 31, "%Y-%m-%d", localtime(&tServer));

                string sCurrent = szCurrent;
                string sServer = szServer;
		LOG("Server build time is " << sServer << " and current time is " << sCurrent << " which is more than " << m_LicenseConfigParameters.m_iExpiration << " days apart.");
		cerr << "Server build time is " << sServer << " and current time is " << sCurrent << " which is more than " << m_LicenseConfigParameters.m_iExpiration << " days apart." << endl;
		return false;
	}

	char cMac[32];

	if( !get_mac(cMac) )
	{
		LOG("Cannot determine MAC address");
		cerr << "Cannot determine MAC address" << endl;
		return false;
	}

	


	unsigned int uTmp[6];	
	sscanf(m_LicenseConfigParameters.m_sServerMac.c_str(), "%x %x %x %x %x %x", uTmp + 0, uTmp + 1, uTmp + 2, uTmp + 3, uTmp + 4, uTmp + 5);

	if( (((unsigned int)cMac[0]) & 255) != uTmp[0] || (((unsigned int)cMac[1]) & 255) != uTmp[1] || (((unsigned int)cMac[2]) & 255) != uTmp[2] || (((unsigned int)cMac[3]) & 255) != uTmp[3] || (((unsigned int)cMac[4]) & 255) != uTmp[4] || (((unsigned int)cMac[5]) & 255) != uTmp[5] )
	{
		LOG("MAC address is " << hex << (((unsigned int)cMac[0]) & 255) << ":" << (((unsigned int)cMac[1]) & 255) << ":" << (((unsigned int)cMac[2]) & 255) << ":" << (((unsigned int)cMac[3]) & 255) << ":" << (((unsigned int)cMac[4]) & 255) << ":" << (((unsigned int)cMac[5]) & 255) << " and license MAC address is " << uTmp[0] << ":" << uTmp[1] << ":" << uTmp[2] << ":" << uTmp[3] << ":" << uTmp[4] << ":" << uTmp[5] << "."); 
		cerr << "MAC address is " << hex << (((unsigned int)cMac[0]) & 255) << ":" << (((unsigned int)cMac[1]) & 255) << ":" << (((unsigned int)cMac[2]) & 255) << ":" << (((unsigned int)cMac[3]) & 255) << ":" << (((unsigned int)cMac[4]) & 255) << ":" << (((unsigned int)cMac[5]) & 255) << " and license MAC address is " << uTmp[0] << ":" << uTmp[1] << ":" << uTmp[2] << ":" << uTmp[3] << ":" << uTmp[4] << ":" << uTmp[5] << "." << endl;
		return false;
	}

	return true;
}

vector<string> LicenseConfig::read_tokens_from_string(const string& sString)
{
	vector<string> rc;

	StringTokenizer st;
	string token, line;
	string sCopy = sString;

	while (read_line(sCopy, line))
	{
		st.setString(line);

		while (st.nextToken(token))
			rc.push_back(token);
	}
	return rc;
}

bool LicenseConfig::read_line(string& sString, string& sLine)
{
	const string ws("\r\n");
	string::size_type sep = sString.find_first_of(ws);
	if( sep == string::npos )
		return false;
	sLine = sString.substr(0, sep);
	sString = sString.substr(sep + 1);
	return true;
}

bool LicenseConfig::Decrypt(const string& sFileName, string& sFile)
{
	if( !File::is_regular(sFileName) )
		return false;

	int iSize = File::num_bytes(sFileName);
	
	File file(sFileName);
	if( !file.open_read() )
		return false;

	char *cTmp;
	if( !file.read_bytes(cTmp, iSize) )
		return false;

	string sData;
	sData.assign(cTmp, iSize);
	string sData2 = skip_whitespace(sData);

	iSize = sData2.size();

	string sCrypt = sData2.substr(2, iSize - 4);

	unsigned long long uSum = 0;
        int crypt_size = sCrypt.size();
	for( int i = 0; i < crypt_size; i++ )
	{
		if( isdigit(sCrypt[i]) )
			uSum += sCrypt[i] - '0';
		else
			uSum += sCrypt[i] - 'a' + 10;
	}

	unsigned int u1 = (uSum & 0xf000) >> 12;
	unsigned int u2 = (uSum & 0x0f00) >> 8;
	unsigned int u3 = (uSum & 0x00f0) >> 4;
	unsigned int u4 = (uSum & 0x000f);

	char c1, c2, c3, c4;
	if( u1 < 10 )
		c1 = u1 + '0';
	else
		c1 = u1 + 'a' - 10;
	
	if( u2 < 10 )
		c2 = u2 + '0';
	else
		c2 = u2 + 'a' - 10;

	if( u3 < 10 )
		c3 = u3 + '0';
	else
		c3 = u3 + 'a' - 10;

	if( u4 < 10 )
		c4 = u4 + '0';
	else
		c4 = u4 + 'a' - 10;

	if( c1 != sData2[0] || c2 != sData2[1] || c3 != sData2[iSize - 2] || c4 != sData2[iSize - 1] )
		return false;

	sFile = Crypt::simple_decrypt(sCrypt);

	return true;
}
