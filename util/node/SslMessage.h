// ---------------------------------------------------------------------------
// File:       SslMessage.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Author:     Jozsef Vass
//
// Purpose:    SSL message class
//
// Change Log: 07/31/2001 - Created
// ---------------------------------------------------------------------------

#ifndef __SSL_MESSAGE__
#define __SSL_MESSAGE__

#include "GeneralMessage.h"

#include "Message.h"

#include "SmartPtr.h"

#include <string>

class SslSocket;

class SslMessage : public GeneralMessage
{
public:
	SslMessage();

	virtual ~SslMessage();

	virtual int OpenServer(
		int fdSocket,
		const std::string& sCertFile = "",
		const std::string& sPvkFile = "");

	virtual int OpenClient(
		const std::string& sHost,
		int iPort,
		int iTimeout);

	virtual void Close();

	virtual int GetFd();

	virtual int RecvMsg(std::string& sBuf);

	virtual int SendMsg(const struct ISerializable& Msg);
	virtual int SendMsg(const std::string& sBuf);
	virtual int SendMsg(const char *pBuf, int iLen);

	virtual int SendBufferedData();

	bool IsReadable();

private:
	int GetHeader(struct CommandMessage& msgHdr);
	int CheckHeader(const struct CommandMessage& msgHdr);

private:
	SharedPtr<SslSocket> m_pSslSocket;
	std::string m_sMsgBuf;
};
#endif  // __SSL_MESSAGE__
