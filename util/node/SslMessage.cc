// ---------------------------------------------------------------------------
// File:       SslMessage.cc
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Author:     Jozsef Vass
//
// Purpose:    SSL message class
//
// Change Log: 07/31/2001 - Created
// ---------------------------------------------------------------------------

#include "SslMessage.h"
#include "SslSocket.h"

#include "debug.h"
#include "log.h"
#include "retval.h"

using std::string;

// ---------------------------------------------------------------------------
// SslMessage class implementation
// ---------------------------------------------------------------------------
SslMessage::SslMessage() :
	m_pSslSocket(new SslSocket),
	m_sMsgBuf()
{
}

SslMessage::~SslMessage()
{
//	LOG("SSLMESSAGE_DESTROY");
	m_pSslSocket.reset();
}

int SslMessage::OpenServer(
	int fdSocket,
	const string& sCertFile,
	const string& sPvkFile
)
{
//	DB_ENTER(SslMessage::OpenServer);

	try 
	{
		if( !sCertFile.empty() || !sPvkFile.empty() )
			m_pSslSocket->setCertificate(sCertFile.c_str(), sPvkFile.c_str());
	} 
	catch (IOException ex) 
	{
		LOG("Could not read SSL file on fd " << fdSocket << ": " << ex.GetDetails());
		return ES_NOT_CONNECTED;
	}

	try 
	{
		m_pSslSocket->accept(fdSocket);
	} 
	catch (IOException ex) 
	{
		LOG("Could not accept SSL on fd " << fdSocket << ": " << ex.GetDetails());
		return ES_NOT_CONNECTED;
	}

/*
	try
	{
		m_pSslSocket->finishNegotiation(10*1000);
	}
	catch (IOException ex) 
	{
		LOG("Error during SSL negotiation on fd " << fdSocket << ": " << ex.GetDetails());
		return ES_NOT_CONNECTED;
	}
	catch(TimeoutExceededException ex)
	{
		LOG("Timeout during SSL negotiation on fd " << fdSocket << ": " << ex.GetDetails());
		return ES_NOT_CONNECTED;
	}
*/

	return ES_SUCCESS;
}

int SslMessage::OpenClient(
	const string& sHost,
	int iPort,
	int iTimeout
)
{
//	DB_ENTER(SslMessage::OpenClient);
	try
	{
		m_pSslSocket->connect(sHost.c_str(), iPort, iTimeout);
		m_pSslSocket->finishNegotiation(30*1000);
	}
	catch( IOException ex )
	{
		LOG("Could not connect " << ex.GetDetails());
		return ES_NOT_CONNECTED;
	}
	catch(TimeoutExceededException ex)
	{
		LOG("Timeout while negotiation SSL connection.  " << ex.GetDetails());
		// socket connected but cannot negotiate
		m_pSslSocket->close();
		return ES_NOT_CONNECTED;
	}

	return ES_SUCCESS;
}

int SslMessage::SendMsg(
	const char* pBuf,
	int iLen
)
{
	try
	{
		m_pSslSocket->write(pBuf, iLen);
	}
	catch( IOException ex )
	{
		LOG("Error during SSL write on fd " << m_pSslSocket->getFd() << ": " << ex.GetDetails());
		return ES_NOT_CONNECTED;
	}
	catch( TimeoutExceededException ex )
	{
		LOG("Timeout during SSL write on fd " << m_pSslSocket->getFd() << ": " << ex.GetDetails());
		return ES_NOT_CONNECTED;
	}
	return ES_SUCCESS;
}

int SslMessage::SendBufferedData()
{
	return ES_SUCCESS;
}

int SslMessage::SendMsg(const string& sBuf)
{
	return SendMsg(sBuf.data(), sBuf.size());
}

int SslMessage::SendMsg(const struct ISerializable& Msg)
{
	string sBuf;
	Msg.serialize(sBuf);

	return SendMsg(sBuf.data(), sBuf.size());
}

void SslMessage::Close()
{ 
//	DB_ENTER(SslMessage::Close);
//	LOG("SSLMESSAGE_CLOSE");
	m_pSslSocket->close(); 
}

int SslMessage::GetFd()
{ 
	return m_pSslSocket->getFd();
} 

int SslMessage::CheckHeader(const struct CommandMessage& msgHdr)
{
	switch( msgHdr.m_u8Version )
	{
		case EYEBALL_CHAT_LE_09:
		case EYEBALL_CHAT_LE_10:
		case EYEBALL_CHAT_LE_11:
		case EYEBALL_CHAT_LE_12:
		case EYEBALL_CHAT_20:
		case EYEBALL_CHAT_22:
		case EYEBALL_CHAT_23:
			break;

		case EYEBALL_VIDEOMESSAGE_01:
			break;

		case EYEBALL_GROUPCHAT_02:
			break;

		case EYEBALL_CHATROOM:
		case EYEBALL_CHATROOM_UNICODE:
			break;

		case EYEBALL_IM:
			break;

		default:
			return ES_NOT_CONNECTED;
	}

	return ES_SUCCESS;
}

int SslMessage::RecvMsg(string& sBuf)
{
//	DB_ENTER(SslMessage::RecvMsg);
//	DB_STAT(m_sMsgBuf.size());

   // Access the message header
   CommandMessage msgHdr;
   int iRet = GetHeader(msgHdr);
   if (iRet != ES_SUCCESS)
      return iRet; // message header is not yet available

	iRet = CheckHeader(msgHdr);
	if( iRet != ES_SUCCESS )
		return iRet; 

   // Allocate memory for the payload
   u_int16_t uPayloadLen = msgHdr.m_u16PayloadLength;
   u_int32_t cbTotal = CM_LENGTH + uPayloadLen;

   if (uPayloadLen > 0)
   {
      // Perform TCP receive
      int len = cbTotal - m_sMsgBuf.size();  // requested length
		if( len < 0 )
			return ES_NOT_CONNECTED;

		string sBuf;
		sBuf.resize(len);

		int iRet = 0;
		try
		{
      	iRet = m_pSslSocket->readWithTimeout(&sBuf[0], len, 0);
			if( iRet == 0 )
				return ES_NOT_CONNECTED;
		}
		catch( IOException ex )
		{
			LOG("Error during SSL read on fd " << m_pSslSocket->getFd() << ": " << ex.GetDetails());
			return ES_NOT_CONNECTED;
		}
		catch( TimeoutExceededException )
		{
			iRet = 0;
		}

      m_sMsgBuf.append(sBuf.data(), iRet); // add bytes received

      if (m_sMsgBuf.size() < cbTotal)
      {
         return ES_FAILURE; // payload not yet available
      }
   }

	if( m_sMsgBuf.size() != cbTotal )
		return ES_NOT_CONNECTED;

   // Success
   sBuf = m_sMsgBuf;

   // Clean-up
   m_sMsgBuf.erase();

   return ES_SUCCESS;
}

int SslMessage::GetHeader(CommandMessage& msgHdr)
{
//	DB_ENTER(SslMessage::GetHeader);

   // Return message header, if it is buffered
   if (m_sMsgBuf.size() >= CM_LENGTH)
   {
      msgHdr.deserialize(m_sMsgBuf);
      return ES_SUCCESS;
   }

   int len = CM_LENGTH - m_sMsgBuf.size();
	if( len < 0 )
		return ES_NOT_CONNECTED;

	string sBuf;
	sBuf.resize(len);

	int iRet = 0;
	try
	{
   	iRet = m_pSslSocket->readWithTimeout(&sBuf[0], len, 0);
		if( iRet == 0 )
			return ES_NOT_CONNECTED;
	}
	catch( IOException ex )
	{
		LOG("Error during SSL read on fd " << m_pSslSocket->getFd() << ": " << ex.GetDetails());
		return ES_NOT_CONNECTED;
	}
	catch( TimeoutExceededException )
	{
		iRet = 0;
	}

   m_sMsgBuf.append(sBuf.data(), iRet); // add bytes received

   if (m_sMsgBuf.size() >= CM_LENGTH)
   {
      msgHdr.deserialize(m_sMsgBuf);
      return ES_SUCCESS;
   }

   return ES_FAILURE;
}

bool SslMessage::IsReadable()
{
	return m_pSslSocket->isReadable();
}
