// IntMT.h
// Interface for the thread-safe integer class
//
// Zion Kwok
// July 14, 2006
//
// (C) Eyeball Networks
//////////////////////////////////////////////

#ifndef EB_INT_MT_H_
#define EB_INT_MT_H_

#include <pthread.h>

class CIntMT 
{
public:
	//! Default Constructor
	CIntMT();

	//! Copy Constructor
	CIntMT(CIntMT& intMT);

	//! Constructor
	CIntMT(int value);

	//! Destructor
	~CIntMT();

	//! Assignment operator
	CIntMT& operator=(CIntMT& intMT);
	CIntMT& operator=(int x);

	//! Addition
	CIntMT& operator+=(int x);

	//! Substraction
	CIntMT& operator-=(int x);

	//! Multiplication
	CIntMT& operator*=(int x);

	//! Division 
	CIntMT& operator/=(int x);

	//! Modulo 
	CIntMT& operator%=(int x);

	//! Prefix Increment
	CIntMT& operator++();

	//! Prefix Decrement
	CIntMT& operator--();

	//! Shift right
	CIntMT& operator>>=(int x);

	//! Shift left
	CIntMT& operator<<=(int x);

	//! And
	CIntMT& operator&=(int x);

	//! Or 
	CIntMT& operator|=(int x);

	//! Xor 
	CIntMT& operator^=(int x);

	//! Get integer value
	operator int();	

private:
	//! Postfix Increment -- very inefficient
	CIntMT operator++(int);

	//! Postfix Decrement -- very inefficient
	CIntMT operator--(int);

	int m_iValue;
	pthread_mutex_t m_Mutex;
};


#endif //EB_INT_MT_H_

