/** All files of the DNDS Prototype are Copyright (C) 1999 by Andre Schoorl
 *  unless otherwise noted.
 *
 *  The DNDS Prototype is distributed in the public domain in the hope that it
 *  will be useful, but is provided `as is' and is subject to change without
 *  notice.  No warranty of any kind is made with regard to the software or
 *  documentation.  The author shall not be liable for for any error for
 *  incidental or consequential damages in connection with the software and
 *  the documentation.
 *
 *  Permission to copy the whole, unmodified DNDS package is granted provided
 *  that the copies are not made or distributed for resale (excepting nominal
 *  copying fees).  All redistributions in either source or binary form must
 *  include this disclaimer.
 */

#ifndef AS_THREAD_H
#define AS_THREAD_H

#ifdef WIN32
#include <windows.h>
#else
#include <pthread.h>
#endif

#ifdef WIN32
	#ifdef DLL_VERSION
		#define DllImport __declspec(dllimport)
		#define DllExport __declspec(dllexport)
	#else
		#define DllImport
		#define DllExport
	#endif

	#ifdef SVVIDEO_EXPORTS
		#define CLASS_DECL_THREAD DllExport
	#else
		#define CLASS_DECL_THREAD DllImport
	#endif
#else
	#define CLASS_DECL_THREAD
#endif

class CLASS_DECL_THREAD Thread
{
	private:
#ifdef WIN32
		HANDLE thread_id;
#else
		pthread_t thread_id;
#endif

	public:
		Thread(void * (*start_routine)(void *), void *arg = NULL);
		~Thread();

#ifdef WIN32
		void set_priority(int priority);
#endif
		void join(void);

		static void create(void * (*start_routine)(void *), void * arg = NULL);

		static void CreateDetached(void * (*start_routine)(void *), void * arg = NULL);
};

#endif // AS_THREAD_H

