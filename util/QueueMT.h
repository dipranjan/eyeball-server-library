// QueueMT.h
// Implementation of the CQueueMT class

#ifndef EB_QUEUE_MT_H_
#define EB_QUEUE_MT_H_

#include <queue>
using std::queue;

#include <pthread.h>

template<class T>
class CQueueMT
{
public:
  //! Constructor
  CQueueMT<T>();

  //! Destructor
  ~CQueueMT<T>();

  //! Get size
  unsigned int GetSize();

  //! Get front
  //! Returns false if queue is empty
  bool Front(T& element);

  //! Pop front
  //! Returns false if queue is empty
  bool Pop(T& element);

  //! Pop front
  void Pop();

  //! Push back
  void Push(const T& element);

private:
  queue<T> m_Queue;
  pthread_mutex_t m_Mutex;

};

//! Constructor
template<class T>
CQueueMT<T>::CQueueMT<T>()
{
  pthread_mutex_init(&m_Mutex, NULL);
}

//! Destructor
template<class T>
CQueueMT<T>::~CQueueMT<T>()
{
  pthread_mutex_destroy(&m_Mutex);
}

//! Get size
template<class T>
unsigned int CQueueMT<T>::GetSize()
{
  unsigned int uiSize = 0;

  pthread_mutex_lock(&m_Mutex);
  uiSize = m_Queue.size();
  pthread_mutex_unlock(&m_Mutex);

  return uiSize;
}

//! Get front
//! Returns false if queue is empty
template<class T>
bool CQueueMT<T>::Front(T& element)
{
  bool bReturnedData = false;

  pthread_mutex_lock(&m_Mutex);
  if (m_Queue.size() > 0) {
    element = m_Queue.front(); 
    bReturnedData = true;
  }
  pthread_mutex_unlock(&m_Mutex);

  return bReturnedData;
}

//! Pop front
//! Returns false if queue is empty
template<class T>
bool CQueueMT<T>::Pop(T& element)
{
  bool bReturnedData = false;

  pthread_mutex_lock(&m_Mutex);
  if (m_Queue.size() > 0) {
    element = m_Queue.front(); 
    m_Queue.pop();
    bReturnedData = true;
  }
  pthread_mutex_unlock(&m_Mutex);

  return bReturnedData;
}

//! Pop front
template<class T>
void CQueueMT<T>::Pop()
{
  pthread_mutex_lock(&m_Mutex);
  if (m_Queue.size() > 0) {
    m_Queue.pop();
  }
  pthread_mutex_unlock(&m_Mutex);
}

//! Push back
template<class T>
void CQueueMT<T>::Push(const T& element)
{
  pthread_mutex_lock(&m_Mutex);
  m_Queue.push(element);
  pthread_mutex_unlock(&m_Mutex);
}

#endif //EB_QUEUE_H_
