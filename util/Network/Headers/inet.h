// ---------------------------------------------------------------------------
// File:       inet.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    
// 
// Change Log:
// ---------------------------------------------------------------------------

#ifndef EYEBALL_INET_H
#define EYEBALL_INET_H

#if 1 //for Linux yamada
//#include <endian.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#else
#ifndef _WIN32_WCE

#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include <winsock2.h>
#endif
#if defined(__osf__)
#include <machine/endian.h>
#endif
#if defined(__linux__)
#include <endian.h>
#endif
#ifndef IPPROTO_IP
#if defined(__ultrix__) && defined(__cplusplus)
extern "C" {
#include <netinet/in.h>
}
#else
#include <netinet/in.h>
#endif
#endif
#ifndef WIN32
#include <arpa/inet.h>
#endif

#else // _WIN32_WCE
//#define WIN32_LEAN_AND_MEAN   // Is good for Windows CE??
// for Linux yamada #include <winsock.h>
#endif // _WIN32_WCE
#endif //for Linux yamada


//#include <string>
#include "../../Base/Headers/tstring.h"
#include "../../Base/Headers/config.h"
#include "../../Base/Headers/utypes.h"


// ---------------------------------------------------------------------------
// Required libraries
// ---------------------------------------------------------------------------

#ifndef _WIN32_WCE
#ifndef _EC_NO_WS2_32
#pragma comment(lib, "ws2_32.lib")
#endif // _EC_NO_WS2_32
#endif

// ---------------------------------------------------------------------------
// Type definitions
// ---------------------------------------------------------------------------

#if defined(sun) || defined(WIN32)
typedef int socklen_t;
#endif

#if defined(WIN32)
struct msghdr {
	caddr_t	msg_name;			/* optional address */
	u_int	msg_namelen;		/* size of address */
	struct	iovec *msg_iov;		/* scatter/gather array */
	u_int	msg_iovlen;			/* # elements in msg_iov */
	caddr_t	msg_control;		/* ancillary data, see below */
	u_int	msg_controllen;		/* ancillary data buffer len */
	int	msg_flags;				/* flags on received message */
};
#endif


// ---------------------------------------------------------------------------
// Function prototypes
// ---------------------------------------------------------------------------

// The IP addresses in this file are all in network byte order.
std::string intoa(u_int32_t addr);
u_int32_t	LookupHostAddr(const std::string& s);
bool		LookupHostName(u_int32_t addr, char* name, int len);
std::string LookupHostName(u_int32_t addr);
u_int32_t	LookupLocalAddr(void);
void        SetLocalAddr(u_int32_t addr);
bool        IsLocalAddress(u_int32_t addr);
DWORD		GetInterfaceSpeed();
bool		IsInSameSubnet(u_int32_t uLocalAddr, u_int32_t uRemoteAddr);

#define SEND_BUFFER_SIZE (80 * 1024)
#define RECV_BUFFER_SIZE (80 * 1024)
                                                                                                                            

void nonblock(int fd);
void reuse(int fd);
bool sendbufsize(int fd, int size = SEND_BUFFER_SIZE);
bool recvbufsize(int fd, int size = RECV_BUFFER_SIZE);
void nolinger(int fd);
void nodelay(int fd);




// ---------------------------------------------------------------------------
// Macros
// ---------------------------------------------------------------------------

#ifndef NTOHL
#if BYTE_ORDER==LITTLE_ENDIAN
#define NTOHL(d) ((d) = ntohl((d)))
#define NTOHS(d) ((d) = ntohs((d)))
#define HTONL(d) ((d) = htonl((d)))
#define HTONS(d) ((d) = htons((d)))
#elif BYTE_ORDER==BIG_ENDIAN
#define NTOHL(d)
#define NTOHS(d)
#define HTONL(d)
#define HTONS(d)
#else
#error error - BYTE_ORDER not defined.
#endif
#endif

#ifndef INADDR_LOOPBACK
#define INADDR_LOOPBACK (u_int32_t)0x7F000001
#endif

/* XXX winsock2.h should have these !! */
#if defined(WIN32) && !defined(IN_CLASSD)
	#define	IN_CLASSD(i) \
		(((u_long)(i) & ((u_long)0xf0000000)) == ((u_long)0xe0000000))

	#define	IN_CLASSD_NET		((u_long)0xf0000000)  // These aren't really
	#define	IN_CLASSD_NSHIFT	28                    // net and host fields,but
	#define	IN_CLASSD_HOST		((u_long)0x0fffffff)  // routing needn't know

	#define	IN_MULTICAST(i)		IN_CLASSD(i)
#endif

#ifdef _WIN32_WCE

/*
 * WinSock 2 extension -- data type for WSAEnumNetworkEvents()
 */
#define FD_MAX_EVENTS    10
#define FD_CONNECT_BIT   4

struct WSANETWORKEVENTS 
{
	long lNetworkEvents;
    int iErrorCode[FD_MAX_EVENTS];
};


int WSAEnumNetworkEvents(
	SOCKET s,
    HANDLE hEventObject,
	WSANETWORKEVENTS* lpNetworkEvents
);


int WSAEventSelect(
	SOCKET s,
	HANDLE hEventObject,
	long lNetworkEvents
);

void CloseSocketThread();
void CreateSocketThread();

#endif // _WIN32_WCE

#if 1 //for Linux yamada
#define INVALID_SOCKET (-1)
#define SOCKET_ERROR (-1)

#define FD_READ         0x01
#define FD_WRITE        0x02
#define FD_OOB          0x04
#define FD_ACCEPT       0x08
#define FD_CONNECT      0x10
#define FD_CLOSE        0x20

#define FD_MAX_EVENTS    10
#define FD_CONNECT_BIT   4

int WSAEnumNetworkEvents(
	SOCKET s,
    HANDLE hEventObject,
	struct WSANETWORKEVENTS* lpNetworkEvents
);


int WSAEventSelect(
	SOCKET s,
	HANDLE hEventObject,
	long lNetworkEvents
);

void CloseSocketThread();
void CreateSocketThread();


#endif //for Linux yamada

#endif // EYEBALL_INET_H
