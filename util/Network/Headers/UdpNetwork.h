// ---------------------------------------------------------------------------
// File:       UdpNetwork.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    
// 
// Change Log:
// ---------------------------------------------------------------------------

#ifndef UDP_NETWORK_H
#define UDP_NETWORK_H

#include "ENetwork.h"
#include "GeneralNetwork.h"
#include "ESocket.h"
#include <errno.h>		//Inserted by Farhan, 3:45 pm, 29/10/2004
#include <iostream>

//#include "SmartPtr.h"

// Note: IP addresses and ports in this file are in network byte order.

class pktbuf;

class UdpNetwork : 
	public ISocketContainer,
	public GeneralNetwork
{
public:
	UdpNetwork();
	~UdpNetwork();

	// Full specified open, use zero for undesired ports
	virtual bool Open(
		u_int32_t uRemoteAddr, 
		u_int16_t uRemotePort, 
		u_int16_t uLocalPort, 
		HANDLE hNotifyEvent);

	virtual void Close(void);

	virtual int SendTo(const char* buf, int len, u_int32_t addr, u_int16_t port);
	virtual int SendTo(const std::string& sBuf, AddressPortPair addr);
	virtual int Send(const char *buf, int len);
	virtual int Send(const pktbuf*);
	virtual int Recv(char* pBuf, int iLen);
	virtual int RecvFrom(
		char* pBuf, 
		int iLen, 
		u_int32_t& uFrom,
		u_int16_t& uFromPort);

	// ISocketContainer
	virtual BOOL GetSharedSocket(SharedPtr<ESocket>& pSocket);
	virtual BOOL SetSharedSocket(SharedPtr<ESocket>& pSocket);

	virtual u_int32_t GetRemoteAddr(void) const { return m_uRemoteAddr; }
	virtual u_int16_t GetRemotePort(void) const { return m_uRemotePort; }
	virtual SOCKET GetSocket(void);
	virtual void SetSocket(SOCKET sock);

	/**
	 * Auto-port function for PAT-firewall compatibility.
	 */
	virtual bool CheckRemotePort(u_int32_t uAddr, u_int16_t uPort);

	virtual u_int16_t GetDetectedPort(void) const;

private:
	void OpenSender(void);
	void OpenReceiver(void);

	int BindSendSocket(void);

	void SetBufferSize(void);
	void SetRecvBufferSize(void);
	void SetSendBufferSize(void);

private:
	SharedPtr<ESocket> m_pSocket;
	HANDLE m_hNotifyEvent;
	u_int32_t m_uRemoteAddr;
	u_int16_t m_uRemotePort;
	u_int16_t m_uLocalPort;
	u_int16_t m_uDetectedPort;  // overrides remote port to help with PAT
	bool m_bPortConfirmed;

	friend struct lt_UdpNetwork;
};

struct lt_UdpNetwork
{
	bool operator()(const UdpNetwork& s1, const UdpNetwork& s2) const;
};

#endif // UDP_NETWORK_H
