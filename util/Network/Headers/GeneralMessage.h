// ---------------------------------------------------------------------------
// File:       GeneralMessage.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Author:     Jozsef Vass
//
// Purpose:    SSL message class
//
// Change Log: 07/31/2001 - Created
// ---------------------------------------------------------------------------

#ifndef __GENERAL_MESSAGE__
#define __GENERAL_MESSAGE__

#if 1 //for Linux yamada
#include <string>
typedef  void* HANDLE;
#endif //for Linux yamada

//#include <string>

class GeneralMessage
{
public:
	virtual ~GeneralMessage() {};

	virtual bool Open(
		u_int32_t uRemoteAddr,
		u_int16_t uRemotePort,
		u_int16_t uLocalPort,
		HANDLE uNotificationEvent) = 0;	
	
	virtual void Close(void) = 0;

	virtual int RecvMsg(std::string& _sBuf) = 0;

	virtual int SendMsg(const class ISerializable& Msg) = 0;
	virtual int SendMsg(const std::string& sBuf) = 0;
	virtual int SendMsg(const char *pBuf, int iLen) = 0;
	virtual void * CreateRecvThread(void * arg) = 0;
	virtual void SetGlobalLocalAddr() = 0;

};

#endif  // __GENERAL_MESSAGE__
