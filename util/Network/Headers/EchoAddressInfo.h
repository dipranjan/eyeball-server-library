// ---------------------------------------------------------------------------
// File:       EchoAddressInfo.h
//
// Copyright:  Eyeball Networks Inc. 1999-2002
//
// Purpose:    Echo address info for firewall port discovery
//
// Change Log: 01/25/2002 - Created
// ---------------------------------------------------------------------------

#ifndef ECHO_ADDRESS_INFO_H
#define ECHO_ADDRESS_INFO_H

#include "ENetwork.h"
#include "Message.h"

enum EchoConnectionType
{
	ECT_Unknown = 0,
	ECT_VideoChat,
	ECT_File,
	ECT_WhiteBoard
};

class EchoAddressInfo : public ISerializable
{
public:
	EchoAddressInfo()
	:
		m_uConnectionType(0),
		m_uConnectionId(0),
		m_EchoAddr(0, 0)
	{}

	virtual void serialize(std::string& sBuf) const
	{
		SERIALIZE16(m_uConnectionType);
		SERIALIZE32(m_uConnectionId);
		SERIALIZE_ADDR(m_EchoAddr);
	}
	
	virtual int deserialize(const std::string& sBuf)
	{
		int iOffset = 0;

		DESERIALIZE16(m_uConnectionType);
		DESERIALIZE32(m_uConnectionId);
		DESERIALIZE_ADDR(m_EchoAddr);

		return iOffset;
	}

public:
	u_int16_t m_uConnectionType;
	u_int32_t m_uConnectionId;
	AddressPortPair m_EchoAddr;
};

#endif //ECHO_ADDRESS_INFO_H
