/**
 * By default, the server provides the client with a certificate and
 * the client does not provide a certificate.  To have the client provide
 * a certificate, the server should call loadClientCAFile() before
 * accept() and the client should call setCertificate() before
 * connect()...  continue docs later...
 *
 * If this class is deallocated before it does a close(), does it leak?
 * It should close.
 */
#ifndef _SslSocket_h_
#define _SslSocket_h_

#if 0 //for Linux yamada
#ifdef DLL_VERSION
#define DllImport __declspec(dllimport)
#define DllExport __declspec(dllexport)
#else
#define DllImport
#define DllExport
#endif

#ifdef TOOLS_EXPORTS
	#define CLASS_DECL_SslSocket DllExport
#else
	#define CLASS_DECL_SslSocket DllImport
#endif
#endif //for Linux yamada

#include "Socket.h"
#include "IOException.h"
#include "TimeoutExceededException.h"

#ifdef boolean
 // boolean defined, most likely in the oracle includes. Undefine it, as it wreaks havoc in ssl
 #undef boolean
#endif

#include <stdio.h>
#include "openssl/ssl.h"
#include "openssl/err.h"


#if 1 //for Linux yamada
class SslSocket: public Socket
#else //for Linux yamada
class CLASS_DECL_SslSocket SslSocket: public Socket
#endif //for Linux yamada

{
 public:
  SslSocket();
  virtual ~SslSocket();

  
  /**
   * Sets the private key and certificate sent to the peer.  Must
   * be called before accept/connect() if at all, but the call is required
   * only for the server.  Handles the 1024 FILE* limit on Linux
   * and Solaris.
   *
   * @param certificateFile The file from which to retrieve the
   * certificate.
   * @param privateKeyFile A file containing the matching private key.
   * Often, this is the same as the certificateFile.
   * @throws IOException If either file cannot be opened.  This
   * object is unchanged, except the private key may or may not
   * be changed.
   */
  void setCertificate(const char* certificateFile, const char* privateKeyFile);

  
  /**
   * Sets the private key and certificate from an open file.  Parameters
   * and exceptions are the same as setCertificate(const char*, const char*).
   */
  void setCertificate(FILE* certificateFile, FILE* privateKeyFile);


  /**
   * Connects using an existing socket.  The socket becomes "owned"
   * by this class and will be closed on close().
   * connect() does block, but negotiation is delayed until the next
   * read, write, or finishNegotiation().  If negotiation fails
   * (eg. no common cyphers), the read/write will fail.
   *
   * @throws IOException on failure (SslSocket already open, could
   * not set non-blocking mode).  This SslSocket remains unopened
   * and connect() can be called again.
   */
  void connect(int fd);


  /**
   * Same as SslSocket::connect(fd), but connects to a named remote host.
   *
   * @throws IOException if the host does not exist, if the underlying
   * connect fails, or if the SSL connection cannot be created.
   * This SslSocket remains unopened and connect() can be called again.
   */
  void connect(const char* host, int port);


  /**
   * Given a connected socket (the return from an accept(2)),
   * starts negotiation of an SSL connection.  Never fails and
   * never blocks, but delays negotiation until the next call to
   * read, write, or finishNegotiation().
   *
   * @param fd The underlying socket.  fd becomes "owned"
   * this SslSocket.  It will be closed by this class on a close().
   * @throws IOException on failure (SslSocket already open).
   * This object is unchanged.
   */
  void accept(int fd);


  /**
   * This function will complete any outstanding SSL negotiation.
   * It can be called after a connect() or accept() to complete
   * their initializations (eg. to discover any errors).  It will
   * block and read and write to the underlying socket.  If negotiation
   * is not needed, returns silently.
   *
   * @param timeoutMillis Number of milliseconds to allow for negotiation.
   * -1 to allow unlimited time.  Experiments show a negotiation takes
   * 120ms on a LAN.
   * @throws IOException if the connection could not be negotiated
   * due to an error.
   * @throws TimeoutExceededException if the timeout is exceed before the
   * negotiation completes.
   */
  void finishNegotiation(long timeoutMillis);


  /**
   * The semantics are intended to be similar to read(2) with a 
   * non-blocking socket.  read() will block until data is read
   * or an error occurs.  read() can cause a
   * write on the underlying socket (during the completion of a
   * negotiation).
   *
   * @return On success, the number of bytes read is returned, 0 on
   * eof.
   * @throws IOException if there is an error while reading.
   */
  int read(char* buf, int len);


  /**
   * Semantics are the same as read(), but readWithTimeout() will
   * block only for timeoutMillis milliseconds.
   *
   * @param timeoutMillis Milliseconds to wait for data.  -1 for
   * forever (in which ase TimeoutExceededException will never be thrown).
   * @return On success, the number of bytes read is returned, 0 on
   * eof.
   * @throws IOException if there is an error while reading.
   * @throws TimeoutExceededException if the timeout period expires 
   * before any data is read.
   */
  int readWithTimeout(char* buf, int len, long timeoutMillis);


  /**
   * Same as readWithTimeout(), except it will not return until len
   * bytes or EOF are read.  If fewer than len bytes are read when
   * an EOF is reached, the number of bytes read is returned.  If fewer
   * than len bytes are read before the timeout, the number of bytes
   * read is returned.
   *
   * This function is erroneously implemented to throw a
   * TimeoutExceededException if 0 < #bytes < len bytes are read.
   * It should return #bytes in this case.  Fix it.
   */
  int readFullyWithTimeout(char* buf, int len, long timeoutMillis);
  

  /** 
   * Semantics are the same as readWithTimeout().  buf will be
   * \0-terminated within len bytes (buf will contain at most
   * len-1 characters followed by a \0).  The string is never
   * over 255 bytes.  It's good to avoid writing strings of length
   * 0 as the return value of 0 from this function is ambiguous with
   * an EOF.
   *
   * @return The number of bytes stored in buf (not counting 
   * the \0) is returned.  If there are more than len bytes in
   * the string, they are silently read and discarded.
   */
  int readPascalStringWithTimeout(char* buf, long len, long timeoutMillis);


  /**
   * Writes buf on the socket.  If the data cannot be written to the
   * socket within timeoutMillis, write fails.  You should not expect
   * a write to succeed immediately since a renegotion takes time and
   * is normal.  Can change this SslSocket from being readable to
   * unreadable (due to the completion of a negotiation).  All the
   * bytes are written unless an exception is thrown.
   *
   * @throws IOException if there is an error writing to the socket.
   * @throws TimeoutExceededException if timeoutMillis elapses before
   * the write can complete.  
   */
  void write(const char* buf, int len, long timeoutMillis=10000);

  /**
   * Attempts to write buf to the socket.  
   * Similar to above write() with timeout = 0.  If the data is partially
   * written, then the number of bytes is returned.
   *
   * @return number of bytes written to the socket, -1 on error
   * @throws IOException if there is an error writing to the socket.
   */
  int writePartial(const char* buf, int len);

  /**
   * Writes str on the socket.  Calls above 
   * write(const char* buf, int len, long timeoutMillis) as has
   * the same semantics.
   */
  void writeStr(const char* str, long timeoutMillis=10000);


  /**
   * Same semantics as write().
   *
   * @param buf A string to write.  Must be \0-terminated and less
   * than 255 characters.
   */
  void writePascalString(char* buf, long timeoutMillis=10000);


  /**
   * Checks if a socket is readable.  The answer may not be the same
   * as select(2) as the SslSocket can buffer data internally.
   *
   * @return Usually, returns false if a read() would not return data
   * and true if a read() would.  However, a false return can happen
   * when the socket is readable (a race between returning from the 
   * function and data arriving on the socket; the next isReadable()
   * would return true) and a true return can
   * happen when the read() would block or return no data (if the
   * outstanding data is part of a socket renegotiation; once read()
   * is called, isReadable will return false).
   */
  bool isReadable();


  /**
   *
   */
  bool isOpen() const {return ssl != NULL;}


  /**
   * Returns true iff the socket has thrown an IOException.  If so,
   * the socket cannot be used, other than getFd(), getSSL(), and
   * close().
   */
  bool isError() const {return isInError;}


  /**
   * Returns the file descriptor of the underlying socket.  Use this
   * only for select(), etc., not for read(2) or write(2).
   */
  int getFd() const;
  

  /**
   * The return can be used for reading and writing.  A call to
   * SslSocket.close() invalidates this pointer.
   *
   * @return The underlying SSL used by this object, or null if the
   * object is closed.
   */
  SSL* getSSL() {return ssl;}


  /**
   * Closes the socket, deallocating the SSL and SSL_CTX, and closing
   * the underlying socket.  Must be called even if the socket has
   * an error.  After this call, no other functions can be called.
   * This call blocks until the peer acknowledges the close and
   * completes a normal shutdown, or for 10 seconds max.
   */
  void close();


  /**
   * This need never be called as it is called on the first connect()
   * or accept().  It does one-time initialization on SSL by loading the
   * crypto algorithms and such.  Calling it repeatedly is safe
   * (but not concurrently).  W2K has a bug such that this must be called
   * before DirectX is initialized.
   */
  static void initLibrary();


  /**
   * Sets the certificate authorities that will be accepted by the 
   * server when the client authenticates.  These are sent to the
   * client.  Must be called before accept and only by the server.
   *
   * @param caFile A file containing a set of CA certificates in PEM
   * format.  Multiple certificates is acceptable, in which case
   * they should be concatenated in the file.
   */
  void setClientCAList(const char* caFile);

  
  /**
   * Sets the "verify" mode to cause automatic verification of the
   * peer's certificate on connection.  Must be called before
   * accept/connect.  
   *
   * @param mode A bit set containing either SSL_VERIFY_NONE (no
   * verification) or SSL_VERIFY_PEER (verifies and closes the 
   * connection on failure).  Additionally,
   * can contain SSL_VERIFY_FAIL_IF_NO_PEER_CERT to cause the server
   * to close the connection if the client doesn't provide a 
   * certificate, or SSL_VERIFY_CLIENT_ONCE to prevent further 
   * certificate requests on renegotiation.
   */
  void setVerify(int mode, const char* caFile, const char* caPath);


  /**
   * Returns the certificate of the peer (the lowest certificate in
   * the certificate chain).  Must be called after finishNegotiation(),
   * or a successful write or read (which implicitly finishes negotiation),
   * and before close().
   * 
   * @return The peer's certificate, or NULL if the peer did not send
   * one.  Caller must X509_free() the return.
   */
  X509* getPeerCertificate() {return SSL_get_peer_certificate(ssl);}


  /**
   * These functions are all identical to their counterparts that
   * do not take the ssl parameter, but can be used by code that
   * doesn't want to use SslSocket but does want all the benefits.
   * 
   * @param ssl must be a connected SSL (although it need not have
   * undergone any negotiations; they will be done as needed).
   */
  static int readWithTimeout(SSL* ssl, char* buf, int len, long timeoutMillis);
  static int readFullyWithTimeout(SSL* ssl, char* buf, int len, long timeoutMillis);
  static void write(SSL* ssl, const char* buf, int len, long timeoutMillis=10000);

  /**
   * Sets str to a string containing the line number of the last error
   * and the error description.  Returns str.
   */
  static char* getErrorString(char* str, long len);


 protected:
  static bool isLibraryInited;
  // to prevent starting init more than once (reading /dev/random can take time!)
  static bool initInProgress;


  /**
   * Finds out the random state file to use (generates if needed),
   * and read random byts to initialize the random state.
   * Called by initLibrary().
   *
   * 1) Finds out the file name to use for random state file:
   *   - if RANDFILE env. var is set, use that
   *   - if /dev/random is available, use that
   *   - use the default supplied by RAND_file_name() (~/.rnd)
   * 2) Read random bytes from the file.
   *   If failed (no such file, or not enough numbers) generate them
   *   and read again.
   *
   * @return the number of random bytes read.
   */
  static int initRandomState(void);


  /**
   * If called after an error is returned from an SSL function, returns
   * the text of the #define for that error.
   */
  static const char* getErrorText(SSL* ssl, int err);


  /**
   * Initializes the sslContext.  Called for each instance by connect()
   * or accept().
   *
   * @throws FileNotFoundException if any of the named files don't exist
   * (initSslContextAsServer only).
   * @throws IOException if any file cannot be read.
   * (initSslContextAsServer only).
   */
  //  void initSslContextAsClient();
  //  void initSslContextAsServer(FILE * dhParamsF, FILE * privateKeyF, FILE * certificateF);


  SSL_CTX* sslContext;
  SSL* ssl;

  /**
   * Set when the socket throws an IOException.  The socket can
   * no longer be used (it will throw another IOException).
   */
  bool isInError;


  /**
   * Sets the error state.
   */
  void setError() {isInError = true;}


  // Can't copy-construct it.
  SslSocket(SslSocket& ssl) {}


  /**
   * Blocks until either the expiryTime is reached or the underlying
   * fd has pollFdEvents set.
   *
   * @param fd The fd to test.
   * @param pollFdEvents A bit set used in the pollfd.events field.
   * POLLIN and POLLOUT are normal, but see poll(2).
   * @param expiryTime The time at which a TimeoutExceeded is thrown.
   * 0 to wait forever.  If the poll succeeds immediately, 
   * TimeoutExceeded is never thrown.
   * @throws IOException if there is an error from the poll().
   * @throws TimeoutExceededException if the system time becomes
   * greater than expiryTime.
   */

#if 1 //for Linux yamada
  static void waitOnFd(int fd, int pollFdEvents, struct timeval expiryTime);
#else //for Linux yamada
  static void waitOnFd(int fd, int pollFdEvents, DWORD expiryTime);
#endif //for Linux yamada

};


/*
 This is in Socket now.
long operator-(const timeval& t1, const timeval& t2);
timeval operator+(timeval t1, long timeDiffMicros);
*/


#endif  // SslSocket_h
