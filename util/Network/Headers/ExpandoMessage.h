// ---------------------------------------------------------------------------
// File:       ExpandoMessage.h
//
// Copyright:  Eyeball Networks Inc. 1999-2002
//
// Purpose:    Echo message for firewall port discovery
//
// Change Log: 01/16/2002 - Created
// ---------------------------------------------------------------------------

#ifndef EXPANDO_MESSAGE_H
#define EXPANDO_MESSAGE_H

#include "Message.h"
#include "tstring.h"
#include <map>

typedef std::map<std::string, std::string> ExpandoMap;

class ExpandoMessage : public CommandMessage
{
public:
	ExpandoMessage(u_int8_t u8MessageType = 0);

	// Accessors
	bool Get(const tstring& sKey, tstring& sValue) const;
	void Set(const tstring& sKey, const tstring& sValue);

	bool GetA(const tstring& sKey, std::string& sValue) const;
	void SetA(const tstring& sKey, const std::string& sValue);

	bool Get(const tstring& sKey, int& nValue) const;
	void Set(const tstring& sKey, int nValue);

	tstring GetString(const std::string& sKey) const;
	int GetInt(const std::string& sKey) const;

	// Convert to/from network byte order
	virtual void serialize(std::string& sBuf) const;
	virtual int deserialize(const std::string& sBuf);

	virtual u_int16_t payload_length(void) const;

#ifndef _WIN32_WCE
	friend ostream& operator<<(ostream& s, const ExpandoMessage& v);
#endif

	// Self-test function
	static void SelfTest(void);

protected:
	bool InternalGet(const std::string& sKey, std::string& sValue) const;
	void InternalSet(const std::string& sKey, const std::string& sValue);

protected:
	ExpandoMap m_map;
};

#endif //EXPANDO_MESSAGE_H
