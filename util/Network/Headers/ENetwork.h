// ---------------------------------------------------------------------------
// File:       Network.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Network utilities
// 
// Change Log:
// ---------------------------------------------------------------------------

#ifndef EYEBALL_NETWORK_H
#define EYEBALL_NETWORK_H

#if 1 //for Linux yamada
#include <map>
#include <string>
#endif //for Linux yamada

//#include <map>
//#include <string>
#include "inet.h"

// IP address & port pair
// Note: address and port should always be in network byte order
typedef std::pair<u_int32_t, u_int16_t> AddressPortPair;

namespace Network
{
	void Nonblock(SOCKET sock);
	void Reuse(SOCKET sock);
	
	int GetSendBufferSize(SOCKET sock);
	bool SetSendBufferSize(SOCKET sock, int nBufSize);
	int GetRecvBufferSize(SOCKET sock);
	bool SetRecvBufferSize(SOCKET sock, int nBufSize);
	
	// Set both send and receive buffers to the given value.
	bool SetBufferSize(SOCKET sock, int nBufSize);

	AddressPortPair GetSockName(SOCKET sock);
	AddressPortPair GetPeerName(SOCKET sock);
	bool IsConnected(SOCKET sock);
	int  GetSocketType(SOCKET sock);
	
	std::string ToString(const AddressPortPair& a);
	AddressPortPair FromString(const std::string& s);

	void SerializeAddress(const AddressPortPair& a, std::string& sBuf);
	bool DeserializeAddress(AddressPortPair& a, const std::string& sBuf, int& iOffset);
}

#define SERIALIZE_ADDR(X) \
	Network::SerializeAddress(X, sBuf)

#define DESERIALIZE_ADDR(X) \
	{ if (!Network::DeserializeAddress(X, sBuf, iOffset)) return -1; }

template <class T>
class AddressPortPairTMap : public std::map<AddressPortPair, T>
{
public:
	AddressPortPairTMap() {}
	virtual ~AddressPortPairTMap() {}
	
	bool Insert(u_int32_t uAddr, u_int16_t uPort, const T& t)
	{
		return insert(value_type(AddressPortPair(uAddr, uPort), t)).second;
	}

#if 1 //仮処置(コンパイルエラー回避) yamada

#else
	bool Find(u_int32_t uAddr, u_int16_t uPort, iterator& iter)
	{
		iter = find(AddressPortPair(uAddr, uPort));
		return (iter != end());
	}
#endif
};

#endif // EYEBALL_NETWORK_H
