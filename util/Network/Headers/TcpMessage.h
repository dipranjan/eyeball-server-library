// ---------------------------------------------------------------------------
// File:		TcpMessage.h
//
// Copyright:	Eyeball Networks Inc. 1999-2001
//
// Purpose:		Extends TCP Network - Adds message support
//
// ---------------------------------------------------------------------------   z

#ifndef EC_TCP_MESSAGE_H
#define EC_TCP_MESSAGE_H

//#include "SmartPtr.h"
#include "TcpNetwork.h"
#include "GeneralMessage.h"

#ifndef _WIN32_WCE
namespace Winterdom { struct WinsockOverlapped; }
#endif

// Note: IP addresses and ports in this file are in network byte order.

class TcpMessage : 
	public GeneralMessage,
	public TcpNetwork
{
public:
	TcpMessage();
	virtual ~TcpMessage();

	virtual bool Open(
		u_int32_t uAddr, 
		u_int16_t uRemotePort, 
		u_int16_t uLocalPort,
		HANDLE hNotifyEvent);

	virtual void Close(void);

	virtual int RecvMsg(std::string& sBuf);

	/**
	 * Receive message with timeout.
	 *
	 * @return ES code.  ES_SUCCESS indicates a message was received.
	 */
	virtual int RecvMsg(std::string& sBuf, unsigned int uTimeout);

	virtual int SendMsg(const ISerializable& msg);
	virtual int SendMsg(const std::string& sBuf);
	virtual int SendMsg(const char *pBuf, int iLen);

	virtual void SetGlobalLocalAddr(void);

	virtual SOCKET GetSocket(void) { return TcpNetwork::GetSocket(); }
	virtual void SetSocket(SOCKET sock) { TcpNetwork::SetSocket(sock); }
        virtual void * CreateRecvThread(void * arg) { return TcpNetwork::CreateRecvThread(arg); }


	virtual int GetReceiveBufferSize(void) const;

private:
	int GetHeader(class CommandMessage& msg);

protected:
	std::string m_sMsgBuf;
};


#ifndef _WIN32_WCE

// Extended version of TcpMessage that supports I/O completion ports
class TcpMessageEx : public TcpMessage
{
public:
	TcpMessageEx(bool bIocpEnabled = true);
	virtual ~TcpMessageEx();

	// I/O completion port functions
	virtual int IoRequestComplete(Winterdom::WinsockOverlapped* pOV);
	virtual int ReceiveComplete(void);
	virtual int SendComplete(void);
	virtual int OverlappedReceive(void);
	virtual void DeleteReceiveBuffer(void);

protected:
	virtual int DoRecv(char* pBuf, int iLen);
	virtual bool SendBufferedData(void);

private:
	bool m_bIocpEnabled;
	std::string m_sReadyBytes;
	bool m_bReceiving;
	bool m_bSending;
	Winterdom::WinsockOverlapped* m_pOVRecv;
	Winterdom::WinsockOverlapped* m_pOVSend;
};
#endif // _WIN32_WCE

#endif // EC_TCP_MESSAGE_H
