#ifndef STUN_MESSAGE_H
#define STUN_MESSAGE_H


#include "ENetwork.h"
#include <sys/timeb.h>
#include <cstdio>

// return address/port in network byte order

class CStunMessage
{
public:
	CStunMessage();
	~CStunMessage();


	std::string CreateBindingRequest();

	// public ip address and port
	bool GetMappedAddr(const std::string& sResponse, AddressPortPair& addr);

	// second STUN server
	bool GetChangedAddr(const std::string& sResponse, AddressPortPair& addr);

private:
	std::string CreateRequest();
	bool ParseResponse(const std::string& sResponse);
	AddressPortPair ParseAddress(const std::string& sAddress);
	AddressPortPair GetAddress(const std::string& sResponse, unsigned short iMessageType);

private:
	typedef std::map<unsigned short, std::string> MessageAttribuiteMap;
	MessageAttribuiteMap m_attrMap;
	unsigned short m_iMessageType;
	char m_szTransactionID[17];
};

#endif
