// ---------------------------------------------------------------------------
// File:       EchoMessage.h
//
// Copyright:  Eyeball Networks Inc. 1999-2002
//
// Purpose:    Echo message for firewall port discovery
//
// Change Log: 01/16/2002 - Created
// ---------------------------------------------------------------------------

#ifndef __ECHO_MESSAGE__
#define __ECHO_MESSAGE__

#include "../../Base/Headers/Message.h"

struct AddressEchoMessage : public CommandMessage
{
	AddressEchoMessage();

	// Convert to/from network byte order
	virtual void serialize(std::string& sBuf) const;
	virtual int  deserialize(const std::string& sBuf);

	virtual u_int16_t payload_length(void) const;

	u_int32_t m_u32IpAddress;
	u_int16_t m_u16IpPort;

#ifndef _WIN32_WCE
	friend ostream& operator<<(ostream& s, const AddressEchoMessage& v);
#endif

};
#endif 
