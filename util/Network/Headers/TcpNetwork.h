// ---------------------------------------------------------------------------
// File:		TcpNetwork.h
//
// Copyright:	Eyeball Networks Inc. 1999-2001
//
// Purpose:		Declares the TcpNetwork class.
// 
// Change Log:
//
// Notes:		This class provides client-side support only.
//              See TcpServer for server support.
//              This class provides support for non-blocking, event-based 
//              sockets only.
// ---------------------------------------------------------------------------

#ifndef TCP_NETWORK_H
#define TCP_NETWORK_H

#include "GeneralNetwork.h"
#include "inet.h"
#include "utypes.h"
//#include "SmartPtr.h"
#include<queue>

class Mutex;

typedef std::pair<u_int32_t, u_int16_t> AddressPortPair;//sazzad 22_12
// ---------------------------------------------------------------------------
// Send Buffer class
// ---------------------------------------------------------------------------

class SendBuffer
{
public:
	SendBuffer();

	void AppendPacket(const std::string& sBuf);
	void Clear(void);
	bool Empty(void) const;
	bool GetPacket(std::string& sBuf) const;
	void RemoveBytes(unsigned int nLen);
	unsigned int Size(void) const;

private:
	unsigned int m_nSize;
	std::queue<std::string> m_DataQ;
};

// Note: IP addresses and ports in this file are in network byte order.

class TcpNetwork : GeneralNetwork
{
public:
	TcpNetwork();	
	virtual ~TcpNetwork();

	static void nodelay(SOCKET fd);


	static std::string GetConnectionError(const std::string& sAddress, u_int16_t uPort);
	
	/**
	 * Opens a connection to the given remote host.
	 * This function is non-blocking.  Later calls to Recv may be used to
	 * determine the status of the connection.
	 *
	 * @param uAddr         remote address
	 * @param uRemotePort   remote port
	 * @param uLocalPort    local port to be used for the connection
	 * @param hNotifyEvent  windows event object to be used for notifications
	 *
	 * @return true on success, false otherwise
	 */
	virtual bool Open(
		u_int32_t uRemoteAddr, 
		u_int16_t uRemotePort,
		u_int16_t uLocalPort,
		HANDLE hNotifyEvent);

	/**
	 * Opens a connection to the given remote host.
	 * This function is blocking.
	 *
	 * @param uAddr         remote address
	 * @param uRemotePort   remote port
	 * @param uLocalPort    local port to be used for the connection
	 * @param hNotifyEvent  windows event object to be used for notifications
	 * @param uTimeOut      time-out period, in milliseconds
	 *
	 * @return true on success, false otherwise
	 */
	virtual bool OpenWait(
		u_int32_t uRemoteAddr, 
		u_int16_t uRemotePort,
		u_int16_t uLocalPort,
		HANDLE hNotifyEvent,
		UINT uTimeOut);

	/**
	 * Closes the network connection.
	 */
	virtual void Close(void);		

	/**
	 * Sends the given data to the network socket.
	 * If the socket buffer is full, the data may be buffered rather than
	 * immediately sent to the socket.  In order for this buffered data 
	 * to be eventually sent, TcpNetwork::Recv() must be called in response 
	 * to the notification event passed to the open function.
	 *
	 * @param pBuf  pointer to buffer containing data to be sent
	 * @param iLen  size of buffer pointed to by pBuf
	 *
	 * @note Function succeeds even if data was buffered.
	 *
	 * @return ES return code
	 */
	virtual int	Send(const char* pBuf, int iLen);

	/**
	 * Wrapper for socket recv call.
	 * This function should be called in response to the notification event
	 * passed to the open function.  This function may also send buffered
	 * data or disconnect the socket.
	 * 
	 * @note For message-handling functionality, see the TcpMessage class.
	 *
	 * @return number of bytes received or ES return code
	 */
	virtual int	Recv(char* pBuf, int iLen);

	/**
	 * Handles a connect event.
	 * Automatically closes the object if the connection fails.
	 *
	 * @return true on success, false on failure
	 */
	virtual bool HandleConnectEvent(void);
	virtual bool HandleConnectEvent(const WSANETWORKEVENTS& netEvents);

	u_int32_t get_from_addr(void) { return m_uRemoteAddr; }
	u_int16_t get_from_port(void) { return m_uRemotePort; }

	u_int32_t GetRemoteAddr(void) { return m_uRemoteAddr; }
	void SetRemoteAddr(u_int32_t uAddr) { m_uRemoteAddr = uAddr; }
	u_int16_t GetRemotePort(void) { return m_uRemotePort; }
	void SetRemotePort(u_int16_t uPort) { m_uRemotePort = uPort; }
	virtual void * CreateRecvThread(void * arg);

	virtual u_int16_t GetLocalPort(void);
	virtual void SetGlobalLocalAddr(void);

	virtual SOCKET GetSocket(void);
	virtual void SetSocket(SOCKET sock);

	virtual bool IsConnected(void);

	virtual int GetSendBufferSize(void) const;

	virtual bool SetSendBufferSize(int iSize);
	virtual bool IsSocketWritable(int iTimeout);
	
	/**
	 * Indicates whether or not this object is to be used for sending and
	 * receiving data.  If on, the code is optimized for improved throughput.
	 * If off, the code is optimized for reduced memory usage and delay.
	 *
	 * @param bDataChannel  set to true to turn on data optimizations
	 */
	virtual void SetDataChannel(bool bDataChannel);

	/**
	 * Sets the notification event.
	 * This event is notified of the following events: FD_READ, FD_WRITE, and 
	 * FD_CLOSE.  The network object should be open before calling this 
	 * function.
	 *
	 * @param hEvent  handle to notification event
	 */
	virtual void SetNotifyEvent(HANDLE hEvent);

	/**
	 * Sets the socket's send and receive buffer sizes to the given value.
	 * This is equivalent to using setsockopt with SO_SNDBUF and SO_RCVBUF 
	 * flags.  Note that this is not related to the object's own send buffer.
	 */
	static bool SetSocketBufferSize(SOCKET sock);

//for testing sazzad.
/******************TURN Support********************************************/
        int GetPortFromTurnServer(AddressPortPair& addrServer);
/*        int TurnSendRequest(AddressPortPair& addrServer,AddressPortPair& addrDest);
        int TurnSetActiveDestRequest(AddressPortPair& addrServer,AddressPortPair& addrDest);
        void SetTurnParams(const std::string& sUserName,
                const std::string& sPassword,
                const std::string& sNonce,
                const std::string& sRealm
                );
*/

/**************************************************************/


protected:
	virtual bool DoConnect(u_int32_t uRemoteAddr, u_int16_t uRemotePort);
	virtual int DoRecv(char* pBuf, int iLen);

	/**
	 * Low-level wrapper for socket send function.
	 *
	 * @param pBuf  pointer to data to be sent
	 * @param iLen  len of data to be sent
	 *
	 * @return bytes sent or ES code
	 */
	virtual int DoSend(const char* pBuf, int iLen);

	// Send Buffer methods
	virtual void AppendToSendBuffer(const std::string& sBuf);
	virtual bool IsSendBufferEmpty(void);
	virtual bool GetSendBufferPacket(std::string& sBuf);
	virtual void RemoveSendBufferBytes(unsigned int nLen);

	/**
	 * Sends buffered data.
	 * The function succeeds even if it does not send all of the buffered
	 * data.  To check that the buffer has been completely sent, see
	 * GetSendBufferSize().
	 *
	 * @return true on success, false on failure
	 */
	virtual bool SendBufferedData(void);

	/**
	 * Sets the socket's send and receive buffer sizes to the given value.
	 * This is equivalent to using setsockopt with SO_SNDBUF and SO_RCVBUF 
	 * flags.  Note that this is not related to the object's own send buffer.
	 */
	virtual bool SetSocketBufferSize(void);

	/**
	 * Called after connection succeeds, before any data is read.
	 */
	virtual bool OnConnect(void);

	/**
	 * Wrapper for WSAEnumNetworkEvents
	 *
	 * @return true on success, false on failure
	 */
	virtual bool EnumNetworkEvents(WSANETWORKEVENTS& netEvents);

protected:
	SOCKET m_ConnSock;
	u_int32_t m_uRemoteAddr;
	u_int16_t m_uRemotePort;	
	HANDLE m_hNotifyEvent;
	SharedPtr<Mutex> m_pMutex;
	SharedPtr<SendBuffer> m_pSendBuffer;
	bool m_bDataChannel;
	DWORD m_dwConnectTime;

	/**
	 * A TCP close has been received from the remote.  Continue calling
	 * Recv() until ES_NOT_CONNECTED is returned.
	 */
	bool m_bCloseReceived;
private:
/**************** Turn Support ****************/
        std::string m_sTurnUserName;
        std::string m_sTurnPassword;
        std::string m_sTurnNonce;
        std::string m_sTurnRealm;
        AddressPortPair m_TurnDetectedAddr;
/**************** Turn Support ****************/

};

#endif // TCP_NETWORK_H
