// ---------------------------------------------------------------------------
// File:		HttpMessage.h
//
// Copyright:	Eyeball Networks Inc. 1999-2001
//
// Purpose:		Extends TCP Network - Adds HTTP message support
//
// ---------------------------------------------------------------------------


#ifndef __HTTP_MESSAGE__
#define __HTTP_MESSAGE__

#include "TcpMessage.h"


#define NUM_PRIORITY 4		// can be at most 256

enum
{
	METHOD_GET = 122,
	METHOD_POST,
	METHOD_OK,
	METHOD_CONNECTED,
	METHOD_MULTIPART_FIRST,
	METHOD_MULTIPART
};

//#define HTTP_RECV_HEADER_SIZE 108		// approximate size of receiving HTTP header (long)
#define HTTP_LONG_RECV_HEADER_SIZE 135	// approximate size of receiving HTTP header (long)
#define HTTP_LONG_SEND_HEADER_SIZE 135	// approximate size of sending HTTP header (long)
#define HTTP_RECV_HEADER_SIZE 63		// approximate size of receiving HTTP header
#define HTTP_SEND_HEADER_SIZE 63		// approximate size of sending HTTP header

#define HTTP_CTRL_PRIORITY 0
#define HTTP_AUDIO_PRIORITY 1
#define HTTP_VIDEO_PRIORITY 3
#define HTTP_WHITEBOARD_PRIORITY 2

class Mutex;

class HttpMessage : public TcpMessage
{
public:
	HttpMessage (std::string m_sProxyAddr = "", WORD m_sProxyPort = 0, std::string sLogTail = "");
	virtual ~HttpMessage ();

	// wrap HTTP header to message
	int Pack(
		std::string& sBuf, 
		const std::string& sHostName,	 // used for GET or POST
		u_int16_t uPort,				 // used for GET or POST
		int nSendType);

	// extract message from HTTP message
	static int Unpack(std::string& sBuf);

	// get the HTTP message type given the header of the message
	static int GetHeaderMethod(const std::string& sHeader);

	// try to locate the HTTP header method and trim the leading junk
	// @return true if this method changed the header
	static bool TrimToHeaderMethod(std::string& sHeader);

	// make a connection to the proxy server
	bool ConnectProxy(
		SOCKET sock,
		const std::string& sProxyServerAddr,
		WORD uProxyPort,					// host order
		const std::string& sServerAddr,
		WORD uPort,							// host order
		bool& bProxyError);

	// connect to the proxy and tunnel to the target server
	bool ConnectProxyHttps(
		SOCKET sock,
		const std::string& sProxyServerAddr,
		WORD uProxyPort,					// host order
		const std::string& sServerAddr,
		WORD uPort,							// host order
		bool& bProxyError);

	void SetSendMethod(int nSendType);

	// set the address and port of the proxy server
	void SetProxyProperties(const std::string& sAddr = "", WORD uPort = 0);
	std::string GetProxyAddr();
	u_int16_t GetProxyPort();

	// set the address and port of the server to be connected to
	void SetServerProperties(const std::string& sAddr = "", WORD uPort = 0);
	std::string GetServerAddr();
	u_int16_t GetServerPort();

	bool IsProxyConnectionError();

	// whether HTTPS CONNECT is used
	void SetHttpsUsage(bool bUseHttps = true);
	bool GetHttpsUsage();

	// determine whether HTTP header should be wrapped on send/recv
	void SetSendHttpHeader(bool bSendHttpHeader = true);
	bool GetSendHttpHeader();
	void SetRecvHttpHeader(bool bRecvHttpHeader = true);
	bool GetRecvHttpHeader();
	void SetLongHttpHeader(bool bLongHttpHeader = true);
	bool GetLongHttpHeader();

	// Receive HTTP message
	// @ returns ES_SUCCESS if a msg is recevied
	// @ returns ES_FAILURE if no data or incomplete msg is received
	// @ returns ES_AGAIN if more care is needed to be taken for the msg
	// @ returns ES_NOT_CONNECTED if the connection is broken
	virtual int RecvMsg(std::string& sBuf);

	virtual bool Open(
		u_int32_t uAddr, 
		u_int16_t uRemotePort,	// host order
		u_int16_t uLocalPort,
		HANDLE hNotifyEvent);
	virtual void Close();

	virtual int SendMsg(const ISerializable& msg);
	virtual int SendMsg(const std::string& sBuf);
	virtual int SendMsg(const char *pBuf, int iLen);

	virtual int SendMsg(const ISerializable& msg, int nPriority);
	virtual int SendMsg(const std::string& sBuf, int nPriority);

	virtual int GetSendBufferSize(void) const;

	virtual void SetQueueBufferSize(int iQueue, int iBufSize);
	virtual int GetQueueBufferSize(int iQueue);

	// whether the video buffer can accept more data
	virtual bool AcceptData();

protected:
	virtual bool DoConnect(u_int32_t uRemoteAddr, u_int16_t uRemotePort);

	// Send Buffer methods

	// append message to send buffer at message queue[0]
	virtual void AppendToSendBuffer(const std::string& sBuf);

	// append message to message queue iQueue
	// @ returns true if the message is enqueued
	// @ returns false if the message is discarded because buffer is full
	virtual bool AppendToSendBuffer(int iQueue, const std::string& sBuf);

	// check whether all message queue is empty
	virtual bool IsSendBufferEmpty(void);

	// obtain the send buffer packet at queue[0]
	virtual bool GetSendBufferPacket(std::string& sBuf);

	// obtain the send buffer packet at iQueue
	virtual bool GetSendBufferPacket(int iQueue, std::string& sBuf);
	
	// remove send buffer bytes from queue[0]
	virtual void RemoveSendBufferBytes(unsigned int nLen);
	
	// remove send buffer bytes from iQueue
	virtual void RemoveSendBufferBytes(int iQueue, unsigned int nLen);

	// used in place of the Send function as in TcpNetwork;
	// do send for a message with priority property
	int SendPriorityMsg(int nPriority, const std::string& sBuf);

	// override TcpNetwork's SendBufferedData
	virtual bool SendBufferedData(void);

private:

	// obtain the total buffer size used
	int GetTotalBufferSize();

	// obtain the next priority in list
	// @ index is modified to be the index from the priority list where the priority is found
	// @ return -1 if the priority list is empty or there is an error in the list
	int GetCurPriority(int& index);

	// remove the first priority in the list
	// @ return true if the removal was successful
	// @ return false otherwise
	bool RemovePriority(int nIndex = 0);

	// obtain the next msg from the buffer
	bool GetPriorityPacket(std::string& sBuf);

	// flush the buffer of the given priority
	void FlushBuffer(int nPriority);

	// whether the buffer nPriority can accept nSize more bytes of data
	bool AcceptBufferData(int nPriority, int nSize);

	SharedPtr<Mutex> m_pMutexLog;

	int m_nLogId;
	int m_nSendType;

	SendBuffer m_MsgQ[NUM_PRIORITY];		// message queues for different priorities
	int m_nQueueBufLimit[NUM_PRIORITY];		// message queue buffer size limit
	std::string m_sPriorityList;			// string storing the priority indices
	std::string m_sBufMsg;					// storing buffered data

	std::string m_sProxyAddr;
	WORD m_uProxyPort;			// host order
	std::string m_sServerAddr;
	WORD m_uServerPort;			// host order
	bool m_bProxyError;

	bool m_bLongHeader;
	bool m_bUseHttps;
	bool m_bSendHttpHeader;
	bool m_bRecvHttpHeader;
};

#endif  // __HTTP_MESSAGE__
