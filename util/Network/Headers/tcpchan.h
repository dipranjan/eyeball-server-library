// $Id: tcpchan.h,v 1.1 2005/12/29 07:19:54 sazzad Exp $

// This software includes software developed by the Network Research
// Group at Lawrence Berkeley National Laboratory.
// Copyright (c) 1996 The Regents of the University of California.
// All rights reserved.

#ifndef sv_tcpchan_h
#define sv_tcpchan_h

#include <sys/types.h>
#include <netinet/in.h>   // INADDR_ANY

#include "utypes.h"

typedef unsigned char u_char;

class TCPServerChannel
{
	public:
		TCPServerChannel();
		~TCPServerChannel();

		static int open(u_int16_t _port, u_int32_t uLocalAddr = INADDR_ANY);
		static void close(int _fdConnSocket);

		static int accept(
			int _iListenSocket,
			u_int32_t& _fromaddr,
			u_int16_t& _fromport
		);

		static ssize_t recv(
			int _fdConnSocket,
			char *_pBuf, size_t _len,
			u_int32_t&, u_int16_t&
		);
		static int send(int _fdConnSocket, const char *_pBuf, size_t _len);
};

class TCPClientChannel
{
	public:
		TCPClientChannel();

		int open(u_int32_t addr, u_int16_t port, u_int32_t uLocalAddr = 0);
		void close(void);

		ssize_t recv(char *_pBuf, size_t _len);
		int send(const char *_pBuf, size_t _len);
		
		int get_fd(){ return m_iClientSocket;}

	private:
		int m_iClientSocket;
};

#endif // sv_tcpchan_h

