#ifndef TURN_MESSAGE_H
#define TURN_MESSAGE_H


//p #include "ENetwork.h"
#include<string>
#include<cstring>
#include<map>
// return address/port in network byte order

typedef bool BOOL;
typedef std::pair<u_int32_t,u_int16_t> AddressPortPair;


namespace eyeballTurn
{

class CTurnMessage
{
public:
	CTurnMessage();
	~CTurnMessage();

	void SetAccount(const std::string& sUserName, const std::string& sPassword);
	void SetAuthAccount(const std::string& sUserName, const std::string& sPassword);

	std::string CreateBindingRequest(BOOL bNoAttribute = true);
	std::string CreateAllocateRequest(BOOL bNoAttribute = true);
	std::string CreateSendRequest(AddressPortPair& addrDestination);
	std::string CreateSetActiveDestinationRequest(AddressPortPair& addrDestination);
	std::string CreateSharedSecretRequest();
	bool ParseSharedSecretResponse(const std::string& sResponse);
	
	bool ParseSendResponse(const std::string& sResponse);
	std::string DataIndication(const std::string& sResponse);
	bool ParseSetActiveDestinationResponse(const std::string& sResponse);
	int  ParseErrorCode(const std::string& sErrorCode);
	void SetNonceRealm(const std::string& sNonce, const std::string& sRealm);
	std::string GetUserName();
	std::string GetPassword();
	std::string GetNonce();
	std::string GetRealm();
	int GetLastErrorCode();
	int GetLastResponseType();
	


	bool ParseMessage(const std::string& sMessage);
	bool ParseMessage(char* pData, int iSize);

	BOOL IsResponse();
	BOOL IsRequest();

	// public ip address and port
	bool GetMappedAddr(const std::string& sResponse, AddressPortPair& addr);

	// second STUN server
	bool GetChangedAddr(const std::string& sResponse, AddressPortPair& addr);

	// return true if the request is valid, response is created if valid
	bool CheckRequestAndCreateResponse(
		AddressPortPair& addrSource,
		AddressPortPair& addrMapped,
		std::string& sResponse);

	
	void SetTurnServer(AddressPortPair& addr) {m_addrStunServer = addr;}
	AddressPortPair GetStunServer() {return m_addrStunServer;}

	void SetDerivedTransportAddr(AddressPortPair& addr) {m_addrDerivedTransport = addr;}
	AddressPortPair GetDerivedTransportAddr() {return m_addrDerivedTransport;}
	
	bool IsTurnMsg(const std::string& s);


private:
	std::string CreateMessage(bool bAddMessageIntegrity = true);
	AddressPortPair ParseAddress(const std::string& sAddress);
	std::string CreateAddressValue(AddressPortPair& addr);
	AddressPortPair GetAddress(const std::string& sResponse, unsigned short iMessageType);
	std::string ComputeHmacOutput(bool bUseMd5);

private:
	typedef std::map<unsigned short, std::string> MessageAttribuiteMap;
	MessageAttribuiteMap m_attrMap;
	unsigned short m_iMessageType;
	char m_szTransactionID[17];
	char m_szMagicCookie[4];
	std::string m_sUserName;
	std::string m_sPassword;

	std::string m_sNonce;
	std::string m_sRealm;
	int m_iLastErrorCode;
	std::string m_sErrorPhrase;


	std::string m_sAuthUserName;
	std::string m_sAuthPassword;
	AddressPortPair m_addrStunServer;
	AddressPortPair m_addrDerivedTransport;
};

void
computeHmac(char* hmac, const char* input, int length, const char* key, int sizeKey);
std::string computeMD5(const std::string& sMd5Input);
std::string hex_string(const std::string& _str);
std::string unhex_string(const std::string& _str);

}//end of namespace


#endif
