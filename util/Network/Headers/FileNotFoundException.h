#ifndef _FILENOTFOUNDEXCEPTION_H__
#define _FILENOTFOUNDEXCEPTION_H__

#include "Exception.h"


class FileNotFoundException : public Exception
{
 public:
  FileNotFoundException() : Exception() {}

  FileNotFoundException(const char* details, ...)
    {
      va_list ap;
      va_start(ap, details);
      setDetails(details, ap);
      va_end(ap);
    }
};

#endif
