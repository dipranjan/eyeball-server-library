#if !defined(_TimeoutExceededException_h)
#define _TimeoutExceededException_h

#include "Exception.h"
#include <assert.h>
#include <stdarg.h>
#include <string.h>


class TimeoutExceededException : public Exception
{
public:
  TimeoutExceededException() : Exception() {}


  TimeoutExceededException(const char* newDetails, ...)
    {
      va_list ap;
      va_start(ap, newDetails);
      setDetails(newDetails, ap);
      va_end(ap);
    }


  TimeoutExceededException(const char* newDetails, va_list ap)
    {
      setDetails(newDetails, ap);
    }
};

#endif
