
// ---------------------------------------------------------------------------
// File:       ESocket.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Thread-aware wrapper class for sockets
// 
// Change Log:
// ---------------------------------------------------------------------------

#ifndef EYEBALL_ESOCKET_H
#define EYEBALL_ESOCKET_H

#if 1 //for Linux yamada
#include <sys/socket.h>
#include <unistd.h>
#endif //for Linux yamada


#include "inet.h"
#include "EchoMessage.h"
#include "ENetwork.h"
#include "SmartPtr.h" //Farhan uncommented this line to help find noncopyable
#include "RudpNetwork.h"



class Mutex;

//_____________________p
/*
enum NetworkFirewallType
{
	FIREWALL_TYPE_UNKNOWN = 0,
	FIREWALL_TYPE_NONE,
	FIREWALL_TYPE_NAT,
	FIREWALL_TYPE_PAT,
	FIREWALL_TYPE_PAT_PLUS,
	FIREWALL_TYPE_IPCHAINS,
	FIREWALL_TYPE_SYMETRIC,

};
*/

//_____________________


#define UDP_PT_ACKNOWLEDGEMENT          107
#define UDP_PT_FIREWALL                 108
#define UDP_PT_P2PPROXY_ACK		109


// Note: IP Addresses and ports in this file are in network byte order.

class ESocket //: noncopyable
{
public:
	ESocket(int af = AF_INET, int type = SOCK_DGRAM, int protocol = 0);
	virtual ~ESocket();

	// Direct access to the socket
	virtual void AttachSocket(SOCKET sock);
	virtual void DetachSocket(void);
	virtual SOCKET GetSocket(void);
	virtual Mutex& GetMutex(void);

	// Connection
	virtual int Bind(u_int32_t uAddr, u_int16_t uPort);
	virtual int SetRemote(u_int32_t uAddr, u_int16_t uPort);
	void SetDetectedAddr(AddressPortPair& addr);
	virtual AddressPortPair GetDetectedAddr();

/******************TURN Support********************************************/
	int GetPortFromTurnServer(AddressPortPair& addrServer);
        int TurnSendRequest(AddressPortPair& addrServer,AddressPortPair& addrDest);
        int TurnSetActiveDestRequest(AddressPortPair& addrServer,AddressPortPair& addrDest);
	void SetTurnParams(const std::string& sUserName,
	        const std::string& sPassword,
                const std::string& sNonce,
                const std::string& sRealm
                );
	AddressPortPair GetTurnDetectedAddrPort();
	int ReleasePortFromTurnServer(AddressPortPair& addrServer);

/**************************************************************/

	
	/**
	 *@param nTempTtl  ttl to use for the conduit packets (set to 0 to
	 *                 use the current socket setting)
	 */
	virtual int OpenConduit(
		u_int32_t uAddr, 
		u_int16_t uPort,
		int nTempTtl);


	virtual int OpenConduit(
		u_int32_t uRemoteAddr, 
		u_int16_t uRemotePort,
		u_int32_t uLocalAddr,
		u_int16_t uLocalPort,
		int nTempTtl);
		
		
	virtual int OpenConduit(   //new
		u_int32_t uAddr, 
		u_int16_t uPort,
		int nTempTtl,
		const std::string& sAppData);
	
	
	
	// Echo-based local address detection
	void SendKeepAlive(AddressPortPair& addrServer);
	int DetectFirewall(AddressPortPair& addrServer);

	// also return the second stun server
	BOOL DetectAddressPort(AddressPortPair& addrServer, AddressPortPair& addrSecondServer);
	BOOL DetectAddressPort(AddressPortPair& addrServer);

	// Echo-based local address detection

	virtual bool DetectLocalAddress(
		const AddressPortPair& addrEchoServer,
		AddressPortPair& addrLocal,
		unsigned int uTimeout);

	// Data Exchange
	virtual int Send(const char *pBuf, int iLen);
	virtual int SendTo(const char *pBuf, int iLen, u_int32_t uAddr, u_int16_t uPort);
	virtual int SendTo(const char *pBuf, int iLen, AddressPortPair& addr);
	virtual int SendTo(const std::string& sBuf, AddressPortPair addr);	
	virtual int Recv(char *pBuf, int iLen);
	virtual int RecvFrom(char *pBuf, int iLen, u_int32_t& uAddr, u_int16_t& uPort);
//
	// Socket Properties
	virtual u_int32_t GetLocalAddr(void);
	virtual u_int16_t GetLocalPort(void);
	virtual AddressPortPair GetLocalAddrPort(void);
	virtual u_int16_t GetDetectedLocalPort(void);
	virtual u_int32_t GetDetectedLocalAddr(void);
	virtual u_int32_t GetRemoteAddr(void);
	virtual u_int16_t GetRemotePort(void);
	virtual AddressPortPair GetRemoteAddrPort(void);
	virtual std::string ToString(void);
	
	virtual int GetTtl(void);
	virtual bool SetTtl(int nTtl);

	virtual void CreateSocket();
	virtual void CloseSocket();
	
	int InternalDetectFirewall(AddressPortPair& addrServer);
	int InternalDetectFirewall(AddressPortPair& addrServer, AddressPortPair& addrSecondServer);

//	int DetectFirewall(AddressPortPair& addrServer);
	BOOL SelectEvent(HANDLE hEvent, long uEvent);




private:
	SOCKET m_sock;
	SharedPtr<Mutex> m_pMutex;
	u_int32_t m_uRemoteAddr;
	u_int16_t m_uRemotePort;
	AddressPortPair m_DetectedLocalAddr;

	bool m_bSendDisabled;

#ifdef _DEBUG
	int m_nDebugCounter;
#endif //_DEBUG

private:
	int m_iAf;
	int m_iType;
	int m_iProtocol;
/**************** Turn Support ****************/
	std::string m_sTurnUserName;
        std::string m_sTurnPassword;
        std::string m_sTurnNonce;
        std::string m_sTurnRealm;
        AddressPortPair m_TurnDetectedAddr;
/**************** Turn Support ****************/

};

/*
 * Interface for objects that have an ESocket member.
 * The ESocket can be shared between multiple objects.  For example, an
 * AudioSender and a AudioReceiver object can share the same ESocket.
 */
struct ISocketContainer
{
	/*
	 * Retrieves the container's active ESocket.
	 * The public functions of the ESocket may be used to directly access the 
	 * socket.  The ESocket may also be used in a call to the SetSharedSocket 
	 * function.
	 *
	 * @param pSocket : shared pointer to an ESocket; may be empty
	 *
	 * @see SetSharedSocket
	 *
	 * @return TRUE on success, FALSE on failure
	 */
	virtual BOOL GetSharedSocket(SharedPtr<ESocket>& pSocket) = 0;

	/*
	 * Changes the container's active ESocket.
	 * By calling this function with the ESocket retrieved from another
	 * ISocketContainer, an ESocket can be shared between objects. For
	 * example, this makes it possible for an AudioSender and an AudioReceiver
	 * to share an ESocket.
	 *
	 * Any previously existing ESocket is discarded.
	 *
	 * @param pSocket : shared pointer to an ESocket
	 *
	 * @see GetSharedSocket
	 *
	 * @return TRUE on success, FALSE on failure
	 */
	virtual BOOL SetSharedSocket(SharedPtr<ESocket>& pSocket) = 0;
};


class DisposableSocket
{
public:
	DisposableSocket():
	  m_socket(INVALID_SOCKET){}
	  ~DisposableSocket() {}

	void Create()
	{
		if (m_socket != INVALID_SOCKET)
			return;
		m_socket = socket(AF_INET, SOCK_DGRAM, 0);
	}

	void Destroy()
	{
		//CLOSE_SOCKET(m_socket);	//Farhan commented it
	}

	SOCKET GetSocket()
	{
		return m_socket;
	}
private:
	SOCKET m_socket;
	
};


#endif // EYEBALL_ESOCKET_H
