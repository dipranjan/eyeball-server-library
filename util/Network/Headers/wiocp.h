// File: wiocp.h - header file for an I/O Completion Port template
// Downloaded from http://www.mvps.org/windev/network/wiocp.html

// Revisions: 	12/01/1999 - created
//             12/31/1999 - added MsgQueue template
//             01/01/2000 - fixed a performance problem in MsgQueue
//                          by changing retval in Push()/Pop() to BOOL
//             04/26/2000 - fixed a couple of bugs in Stop()
//             10/14/2000 - improved the thread termination code
//                          by allowing the derived class to
//                          provide the termination message itself
//             08/01/2001 - Made a small change to GetIoRequestStatus()
//                          Now you can use virtual functions in your TOV!
//                          Thanks to Andrew Maclean for providing the fix!
//
//
// This file is part of the Winterdom library
// Copyright (C) 2000-2001, Tomas Restrepo (tomasr@mvps.org)
//            
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef WIOCP_H__INCLUDED
#define WIOCP_H__INCLUDED

#pragma warning(disable: 4786)  // 255 identifier limit
#pragma warning(disable: 4800)  // bool performance warning
#pragma warning(disable: 4512)  // assignment operator


//
// helper macro to detect invalid handles
//
#define IS_BAD_HANDLE(x) (((x)==NULL) || ((x)==INVALID_HANDLE_VALUE))

// how much time should we wait 
// for threads to exit?
#define WIOCP_ENDWAIT (20000)

// Winsock overlapped send/receive buffer size
#define WSOV_BUFFER_SIZE (16 * 1024)


//////////////////////////////////////////////////////////////////////////
//
// wctypes.h: This header declares the basic types
//            
// This file is part of the Winterdom library
// Copyright (C) 2000-2001, Tomas Restrepo (tomasr@mvps.org)
//            
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//

//#ifndef WCTYPES_H__INCLUDED
//#define WCTYPES_H__INCLUDED

#include <windows.h>
#include <cstdio>
#include <tchar.h>
#include <exception>
#include <cassert>
#include <process.h>
#include <map>
#include <string>
#include <stdexcept>

//
// these definitions work for a
// 32-bit system 
//
#ifdef _32BIT_MACHINE
typedef unsigned char    byte;
typedef unsigned short   word;
typedef unsigned long    dword;

#ifdef _MSC_VER
typedef unsigned __int64 qword;
typedef __int64 long64;
#else
typedef unsigned long long qword;
typedef long long long64;
#endif // _MSC_VER


#endif // _32BIT_MACHINE

//#endif // WCTYPES_H__INCLUDED
//////////////////////////////////////////////////////////////////////////

//
// wsync.h: a set of C++ classes and templates that make using Win32
//          syncronization objects easier
//
// Contents:    
//              * CriticalSection - a CRITICAL_SECTION wrapper
//              * Event           - an EVENT wrapper
//              * Mutex           - a MUTEX wrapper
//              * Semaphore       - a SEMAPHORE wrapper
//              * QueueSemaphore  - a H-P semaphore for queues
//              * SyncLock        - template for using the objects
//
//              The QueueSemaphore class was inspired by a post from
//		          Stefan Gustafsson in microsoft.public.win32.programmer.kernel
//
// Revisions: 	10/26/1999 - created 
//             12/31/1999 - Added Semaphore and QueueSemaphore
//             01/01/2000 - Added Mutex
//             02/25/2001 - fixed namespaces
//
// This file is part of the Winterdom library
// Copyright (C) 2000-2001, Tomas Restrepo (tomasr@mvps.org)
//            
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//#ifndef WSYNC_H__INCLUDED
//#define WSYNC_H__INCLUDED

//
// All classes are in namespace Winterdom
//
namespace Winterdom 
{ 
   // CLASS SIGNATURE
   // all syncronization wrapper objects should have this
   // layout so that SyncLock can use them. We don't 
   // use inheritance to keep the classes lightweight
   // class SyncObject 
   // {
   //  public:
   //      void Acquire ( );
   //      void Release ( );
   // }
   //

   //
   // CRITICAL_SECTION wrapper class
   //
   class CriticalSection
   {
   public:
      CriticalSection ( ) 
      { 
         InitializeCriticalSection ( &m_CritSec ); 
      }
      ~CriticalSection ( ) 
      { 
         DeleteCriticalSection ( &m_CritSec );
      }
      void Acquire ( ) 
      { 
         EnterCriticalSection ( &m_CritSec ); 
      }
      void Release ( ) 
      { 
         LeaveCriticalSection ( &m_CritSec ); 
      }
   private:
      CriticalSection ( const CriticalSection& c );
      CriticalSection& operator= ( const CriticalSection& c );
      CRITICAL_SECTION m_CritSec;
   };

   //
   // Event wrapper class
   //
   class Event
   {
   public:
      Event ( ) 
         : m_hEvent ( NULL ) 
      { 
      }
      ~Event ( ) 
      { 
         if ( m_hEvent != NULL )
            CloseHandle ( m_hEvent ); 
      }
      bool Create ( BOOL IsManualReset = TRUE, BOOL InitialState = FALSE,
                    LPCTSTR Name = NULL ) 
      {  
         m_hEvent = CreateEvent ( NULL, IsManualReset, InitialState, Name );
         return (m_hEvent != NULL);
      }
      void Acquire() 
      { 
         WaitForSingleObject ( m_hEvent, INFINITE ); 
      }
      void Release()
      { 
         SetEvent ( m_hEvent );
      }
      void Pulse()
      { 
         PulseEvent ( m_hEvent );
      }
      void Reset()
      {
         ResetEvent ( m_hEvent );
      }
      HANDLE Handle ( ) 
      { 
         return m_hEvent; 
      }
   private:
      HANDLE m_hEvent;
   };

   //
   // Mutex wrapper class
   //
   class Mutex
   {
   public:
      Mutex ( ) 
         : m_hMutex(NULL), m_AlreadyExists(false) 
      { 
      }
      ~Mutex ( ) 
      { 
         if ( m_hMutex != NULL )
            CloseHandle ( m_hMutex ); 
      }
      bool Create ( BOOL GetOwnership = FALSE, 
                    LPCTSTR Name = NULL ) 
      {  
         m_hMutex = CreateMutex ( NULL, GetOwnership, Name );
         if ( GetLastError() == ERROR_ALREADY_EXISTS )
            m_AlreadyExists = true;
         return (m_hMutex != NULL);
      }
      void Acquire ( ) 
      { 
         WaitForSingleObject ( m_hMutex, INFINITE ); 
      }
      void Release ( )
      { 
         ReleaseMutex ( m_hMutex );
      }
      HANDLE Handle ( ) 
      { 
         return m_hMutex; 
      }
      // did the object already exist when we opened it?
      bool AlreadyExists() const 
      {
         return m_AlreadyExists;
      }
   private:
      HANDLE m_hMutex;
      bool m_AlreadyExists;
   };

   // 
   // this is your basic semaphore
   // not suitable for queues, because the wait
   // is too long at the begining
   //
   class Semaphore
   {
   public:
      Semaphore ( ) 
         : m_hSemaphore ( NULL )
      { 
      }
      ~Semaphore ( ) 
      { 
         if ( m_hSemaphore != NULL )
            CloseHandle ( m_hSemaphore ); 
      }
      bool Create ( long    InitialCount, 
                    long    MaxCount, 
                    LPCTSTR Name = NULL ) 
      {
         m_hSemaphore = CreateSemaphore ( NULL, InitialCount, MaxCount, Name );
         return (m_hSemaphore != NULL);
      }
      void Acquire ( ) 
      {
         WaitForSingleObject ( m_hSemaphore, INFINITE );
      }
      void Release ( )
      {
         ReleaseSemaphore ( m_hSemaphore, 1, NULL );
      }
      HANDLE Handle ( ) 
      { 
         return m_hSemaphore; 
      }
   private:
      HANDLE m_hSemaphore;
   };


   //
   // This is a much faster semaphore for use in 
   // high-performance queues, since it lets the 
   // queue fill up very quickly.
   // Performance increases because we don't block
   // threads if we haven't reached MaxCount Acquisitions
   // of the Semaphore. The semaphore itself allows us
   // to block threads once we've reached MaxCount
   //
   class QueueSemaphore
   {
   public:
      QueueSemaphore ( ) 
         : m_hSemaphore(NULL), m_Count(0)
      { 
      }
      ~QueueSemaphore ( ) 
      { 
         if ( m_hSemaphore != NULL )
            CloseHandle ( m_hSemaphore ); 
      }
      bool Create ( long MaxCount ) 
      {
         m_Count = MaxCount;
         m_hSemaphore = CreateSemaphore ( NULL, 0, 0x7FFFFFFF, NULL );
         return (m_hSemaphore != NULL);
      }
      void Acquire ( ) 
      {
         if ( ::InterlockedDecrement ( &m_Count ) < 0 )
         {
            WaitForSingleObject ( m_hSemaphore, INFINITE );
            //
            // In some contexts, we'll want to sleep to
            // avoid the priority boost the system gives the 
            // current thread after the wait
            //
#ifdef WQS_ACQUIRESLEEP
            Sleep ( 0 );
#endif
         }
      }
      void Release ( )
      {
         if ( ::InterlockedIncrement ( &m_Count ) <= 0 )
            ReleaseSemaphore ( m_hSemaphore, 1, NULL );
      }
      HANDLE Handle ( ) 
      { 
         return m_hSemaphore; 
      }
   private:
      HANDLE m_hSemaphore;
      long   m_Count;
   };

   template <class T>
   class SyncLock
   {
   public:
      SyncLock ( T& obj ) 
         : m_SyncObject ( obj )
      { 
         m_SyncObject.Acquire ( );
      }

      ~SyncLock()
      {
         m_SyncObject.Release ( );
      }

   private:
      T&  m_SyncObject;
   };

   // common typedefs
   typedef SyncLock<CriticalSection> CriticalSectionLock;
   typedef SyncLock<Event> EventLock;
   typedef SyncLock<Semaphore> SemaphoreLock;

//#endif // WSYNC_H__INCLUDED
//////////////////////////////////////////////////////////////////////////

   // 
   // default TOV class you can use if you
   // are not interested in per-request info
   //
   struct DefOverlapped : public OVERLAPPED
   {
      DWORD NumBytes;
   };

   // used for Winsock
   struct WinsockOverlapped : public DefOverlapped
   {
	  // Winsock overlapped receive buffer
	  WSABUF wsabuf;
      char buf[WSOV_BUFFER_SIZE];
   };

   //
   // IOCompPort class:
   // Represents an IO Completion port class 
   // that wraps all code needed to create and use an IOCP
   //
   // derived class T should defined as follows:
   //  class T
   //  {
   //    void ProcessIoRequest ( TOV * pState, TCK CompletionKey );
   //    void CleanUpIoRequest ( TOV * pState, TCK CompletionKey );
   //    void FinalCleanUp  ( );
   //    // used to notify the derived class of internal errors
   //    void OnIocpError ( iocp_error error, DWORD Win32Err ); 
   //    // notification of a new thread created
   //    // runs on the context of the new thread
   //    void OnThreadEntry ( );
   //    // notification just before a thread gets destroyed
   //    void OnThreadExit ( );
   //    // get a termination message
   //    void GetTerminationMsg ( TCK & CompletionKey );
   //    // is this a termination message
   //    bool IsTerminationMsg ( TCK CompletionKey );
   //  };
   //
   // The TOV only has three restrictions:
   // 1- It should be publicly derived from OVERLAPPED
   // 2- NO virtual functions whatsoever
   // 3- It must contain a NumBytes member
   // 
   // There are no restrictions on TCK, you can pass whatever you
   // want through it. Just remember to leave a way
   // to specify termintation messages. Unless you
   // are passing simple cookies through TCK, you'll want to
   // specify a pointer to your own class/struct here, not just
   // the type as in TOV...
   //
   // The reason for allowing both items is that tipically
   // you'll use your own TOV class if you are only interested
   // in per-request info, and TCK if you are interested in 
   // keeping per-client/connection info.
   //
   template <
      class T,          // derived class
      class TOV,        // overlapped class (you can use DefOverlapped)
      class TCK         // completion key class
   > class IoCompPort
   {
   public:
      // typedefs
      typedef IoCompPort<T, TOV, TCK> IoCompPort_;
      typedef std::map<DWORD, HANDLE> ThreadMap_;
      typedef unsigned (__stdcall *PTHREADPROC)(void*);

      // enums
      enum iocp_error {
         err_create_thread =0,      // failed to create new thread
      };


      //
      // Construction/Destruction
      //
      // We have no destructor to avoid having to 
      // make it virtual, so make sure you call Stop()
      //
      IoCompPort ( ) 
         : m_hIOCP ( INVALID_HANDLE_VALUE ),
           m_hStopEvent ( INVALID_HANDLE_VALUE ),
           m_MaxThreads ( 0 ),
           m_ConcurrentThreads ( 0 ),
           m_RunningThreads ( 0 ),
           m_NumThreads ( 0 ),
           m_Timeout ( INFINITE )
      {
      }

      //
      // CreateIocp()
      //
      // Initialize the IOCompletion port
      //
      bool CreateIocp ( ULONG ConcurrentThreads, 
                        ULONG InitialMaxThreads, 
                        DWORD timeout = INFINITE )  
      {
         //
         // save settings
         //
         m_MaxThreads        = InitialMaxThreads;
         m_ConcurrentThreads = ConcurrentThreads;
         m_Timeout           = timeout;

         //
         // create stop event
         //
         m_hStopEvent = CreateEvent ( NULL, TRUE, FALSE, NULL );
         if ( m_hStopEvent == NULL )
            return false;

         //
         // create iocp with no devices attached
         //
         m_hIOCP = CreateIoCompletionPort ( 
                     INVALID_HANDLE_VALUE,                 
                     NULL,
                     NULL,
                     ConcurrentThreads
                  );
         if ( m_hIOCP != NULL )
         {
            //
            // start all worker threads
            //
            for ( UINT i = 0; i < m_MaxThreads; ++i )
               StartThread ( );                
            return true;
         }
         return false;
      } // InternalInit()



      //
      // AttachDevice()
      //
      // Add an io device to completion port.
      // Once attached, you should not destroy the device
      // or try to use it independently
      //
      bool AttachDevice ( HANDLE hDevice, TCK CompletionKey )
      {
         //
         // make sure device and iocp are valid
         //
         if ( IS_BAD_HANDLE(hDevice) || (m_hIOCP == NULL) )
            return false;

         // 
         // associate device handle with IOCP
         //
         HANDLE iocp = CreateIoCompletionPort ( 
                           hDevice,
                           m_hIOCP,
                           (ULONG_PTR)CompletionKey,
                           m_ConcurrentThreads
                        );

         return (iocp != NULL);
      } // AttachDevice()

      bool AttachDevice ( SOCKET Socket, TCK CompletionKey )
      {
         return AttachDevice ( (HANDLE)Socket, CompletionKey );
      }



      //
      // Stop()
      //
      // Stop all processing, terminate all threads.
      // Do not call this from one of the threads attached 
      // to the IOCP, or you'll have nasty surprises...
      //
      bool Stop ( )
      {
         T* pT = static_cast<T*>(this);
         //
         // flag all threads to terminate
         //
         ThreadMap_::iterator it;
         m_ThreadMapLock.Acquire ( );
         int numthreads = m_ThreadMap.size ( );
         m_ThreadMapLock.Release ( );

         for ( ; numthreads > 0; numthreads-- )
         {
            TCK ck; pT->GetTerminationMsg ( ck );
            QueueIoRequest ( NULL, ck );
         }
         //
         // wait for the shutdown event. If
         // some threads have not died before
         // the wait, kill them (ughh!)
         //
         if ( WaitForSingleObject(m_hStopEvent,WIOCP_ENDWAIT)!=WAIT_OBJECT_0 )
         {
            CriticalSectionLock lock ( m_ThreadMapLock );
            for ( it = m_ThreadMap.begin( ); it != m_ThreadMap.end( ); it++ )
            {
               TerminateThread ( it->second, 0 );
               CloseHandle ( it->second );
            }
         }                

         //
         // destroy the IOCP before asking
         // T to clean up after itself
         //
         CloseHandle ( m_hIOCP );
         m_hIOCP = INVALID_HANDLE_VALUE;
         CloseHandle ( m_hStopEvent );
         m_hStopEvent = INVALID_HANDLE_VALUE;

         pT->FinalCleanUp ( );

         return true;
      } // Stop()



      //
      // StartThread()
      //
      // Starts a new thread for the IOCP
      //
      bool StartThread ( )
      {
         HANDLE hThread  = NULL;
         DWORD  ThreadId = 0;

         hThread = (HANDLE)_beginthreadex (
                              NULL, 0,
                              (PTHREADPROC)ThreadProc,
                              (void*)this, 0,
                              (UINT*)&ThreadId
                           );
         if ( hThread != NULL )
         {
            CriticalSectionLock lock ( m_ThreadMapLock );
            m_ThreadMap.insert ( ThreadMap_::value_type(ThreadId, hThread) );
            ::InterlockedIncrement ( (long*)&m_NumThreads );
            ::InterlockedIncrement ( (long*)&m_RunningThreads );
            return true;
         }
         // error
         T * pT = static_cast<T*>(this);
         pT->OnIocpError ( err_create_thread, GetLastError ( ) );
         return false;
      } // StartThread()



      //
      // QueueIoRequest()
      //
      // Queue an io request
      // This is useful for simulating IO operations...
      //
      bool QueueIoRequest ( TOV* pOV, TCK CompletionKey ) 
      {
         //
         // put request in iocp
         //
         return PostQueuedCompletionStatus ( 
                     m_hIOCP,
                     (pOV != NULL) ? pOV->NumBytes : 0,
                     (ULONG_PTR)CompletionKey,
                     (OVERLAPPED*)pOV
                  );
      } // QueueRequest()



      //
      // GetIoRequestStatus()
      //
      // Get a request from the IOCP
      //
      TOV* GetIoRequestStatus ( TCK* pCompletionKey )
      {
         OVERLAPPED* pOV = NULL;
         BOOL  bRet      = FALSE;
         DWORD NumBytes  = 0;

         assert ( pCompletionKey != NULL );

         // 
         // Get the status, with a timeout value.
         // If we timeout, this thread has been idle for too
         // long, so commit suicide
         //
         ::InterlockedDecrement ( (long*)&m_RunningThreads );
         bRet = GetQueuedCompletionStatus (
                     m_hIOCP,
                     &NumBytes,
                     (ULONG_PTR*)pCompletionKey,
                     &pOV,
                     m_Timeout
                  );
         // get real TOV
         TOV*  pTOV = static_cast<TOV*>(pOV);

         ::InterlockedIncrement ( (long*)&m_RunningThreads );
         if ( bRet == FALSE )
         {
            //
            // did we timeout?
            //
            if ( (GetLastError ( ) == WAIT_TIMEOUT) && 
                 (m_NumThreads > m_MaxThreads)  )
            {
               ThreadCleanUp ( );
               _endthreadex ( 0 );
            }
            // no!
            if ( pTOV != NULL )
            {
               // we have a failed io request
               // ask T what to do with it...
               T * pT = static_cast<T*>(this);
               pT->CleanUpIoRequest ( pTOV, *pCompletionKey );
            }
            return NULL;
         }
         //
         // pState might be NULL, even if
         // GetQueuedCompletionStatus()
         // succeeded...
         //
         if ( pTOV != NULL )
            pTOV->NumBytes = NumBytes;

         return pTOV;
      }

      //== accessors ==


      //
      // Timeout()
      //
      // Get/Set the timeout value in miliseconds
      // if you don't want the thread wait time to expire
      // pass INFINITE here
      //
      DWORD Timeout ( )
      {
         return m_Timeout;
      }
      void SetTimeout ( DWORD timeout )
      {
         ::InterlockedExchange ( &m_Timeout, timeout );
      }

      //
      // NumThreads()
      //
      // Get the number of threads attached to the iocp
      //
      ULONG NumThreads ( )
      {
         return m_NumThreads;
      }

      //
      // ConcurrentThreads()
      //
      // Get the number of concurrent threads allowed
      // in the iocp
      //
      ULONG ConcurrentThreads ( )
      {
         return m_ConcurrentThreads;
      }

      //
      // RunningThreads()
      //
      // Get the number of running threads 
      //
      ULONG RunningThreads ( )
      {
         return m_RunningThreads;
      }

      //
      // MaxThreads()
      //
      // Get/Set the maximum number of threads allowed
      // in the iocp. If the MaxThreads > NumThreads,
      // threads will be trimmed down eventually if the
      // timeout value is less than INFINITE and the 
      // server is more or less idle
      //
      ULONG MaxThreads ( )
      {
         return m_MaxThreads;
      }
      void SetMaxThreads ( ULONG maxthreads )
      {
         ::InterlockedExchange ( &m_MaxThreads, maxthreads );
      }


      //== private methods ==
   private:
      //
      // ThreadProc()
      //
      // internal thread procedure of iocp
      //
      static UINT __stdcall ThreadProc ( void * param )
      {
         IoCompPort_ * self = static_cast<IoCompPort_*>(param);
         T * pT = static_cast<T*>(self);

         // notify of the new thread
         pT->OnThreadEntry ( );

         while ( true )
         {
            //
            // wait for a request and process it
            //
            TCK CompletionKey;        
            TOV * pOV = self->GetIoRequestStatus ( &CompletionKey );

            // 
            // commit suicide?
            //
            if ( pT->IsTerminationMsg ( CompletionKey ) )
               break;
            pT->ProcessIoRequest ( pOV, CompletionKey );
         }

         self->ThreadCleanUp ( );
         return 0;
      } // ThreadProc()



      //
      // ThreadCleanUp()
      //
      // Clean up after the calling thread
      //
      void ThreadCleanUp ( )
      {
         m_ThreadMapLock.Acquire ( );
         ThreadMap_::iterator it = m_ThreadMap.find(::GetCurrentThreadId());
         CloseHandle ( it->second );
         m_ThreadMap.erase ( it );
         m_ThreadMapLock.Release ( );

         //
         // notify of the thread exit
         //
         T * pT = static_cast<T*>(this);
         pT->OnThreadExit ( );

         ::InterlockedDecrement ( (long*)&m_RunningThreads );
         if ( ::InterlockedDecrement ( (long*)&m_NumThreads ) == 0 )
         {
            //
            // if this was the last thread,
            // signal the iocp that all threads have died
            //
            ::SetEvent ( m_hStopEvent );
         }
      } // ThreadCleanUp()

      // data members
   private:
      // io completion port handle
      HANDLE     m_hIOCP;         
      // private list of threads
      ThreadMap_ m_ThreadMap;
      // thread list lock
      CriticalSection  m_ThreadMapLock;
      // stop event
      HANDLE     m_hStopEvent;

      // == thread info ==
      // number of threads currently created
      ULONG      m_NumThreads;
      // number of threads to run simultaniously
      ULONG      m_ConcurrentThreads;
      // number of threads running, doing something useful
      ULONG      m_RunningThreads;
      // maximum number of threads allowed to be in the iocp
      ULONG      m_MaxThreads;
      // number of miliseconds to wait before a thread expires
      DWORD      m_Timeout;

   }; // class IoCompPort






   //
   // MsgQueue is an interthread, high-performance 
   // FIFO message queue 
   //
   template <
      class T,                  // type of struct/class passed 
      long  MaxQueueDepth       // max number of elements allowed
   > class MsgQueue
   {
   public:
      MsgQueue ( ) 
         : m_hIOCP ( INVALID_HANDLE_VALUE ),
           m_ElementCount ( 0 )
      {
      }
      ~MsgQueue ( ) 
      {
      }

      bool Create ( )
      {
         if ( !m_QueueLock.Create ( MaxQueueDepth ) )
            return false;
         m_hIOCP = CreateIoCompletionPort ( INVALID_HANDLE_VALUE, 0, 0, 0 );
         return (m_hIOCP != NULL);
      }

      BOOL Push ( T Element )
      {
         m_QueueLock.Acquire ( );
         BOOL b = PostQueuedCompletionStatus ( 
                     m_hIOCP, 0, 
                     (ULONG_PTR)Element, 
                     (OVERLAPPED*)NULL
                  );
         if ( b ) 
            ::InterlockedIncrement ( &m_ElementCount );
         else
            m_QueueLock.Release ( );
         return b;
      }

      BOOL Pop ( T& Element )
      {
         DWORD       NumBytes;
         OVERLAPPED* pOverlapped;

         BOOL b = GetQueuedCompletionStatus ( 
                     m_hIOCP, &NumBytes,
                     (ULONG_PTR*)&Element,
                     &pOverlapped, INFINITE
                  );
         if ( b )
         {
            ::InterlockedDecrement ( &m_ElementCount );
            m_QueueLock.Release ( );
         }
         return b;
      }

      long ElementCount ( ) const
      {
         return m_ElementCount;
      }
   private:
      HANDLE m_hIOCP;
      long   m_ElementCount;
      // the semaphore prevents us from pushing more
      // than MaxQueueDepth messages on the queue
      QueueSemaphore m_QueueLock;  
   }; // class MsgQueue

} // namespace Winterdom

// reenable warnings

#pragma warning(default: 4800)  // bool performance warning
#pragma warning(default: 4512)  // assingment operator

#endif // _WIOCP_H__INCLUDED
