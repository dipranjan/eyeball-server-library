// ---------------------------------------------------------------------------
// File:		TcpServer.h
//
// Copyright:	Eyeball Networks Inc. 1999-2001
//
// Purpose:		Declares the TcpServer class
// 
// Change Log:
// ---------------------------------------------------------------------------

#ifndef TCP_SERVER_H
#define TCP_SERVER_H

#include <string>

#include "Event.h"
#include "Message.h"
#include "ThreadServer.h"
#include "utypes.h"
#if 0 //for Linux yamada
#include "wiocp.h"
#endif //for Linux yamada

typedef std::vector<HANDLE> EventVector;

enum TcpServerEventId
{
	TSE_UNKNOWN = 0,
	TSE_NO_EVENT,
	TSE_CONNECT,
	TSE_DISCONNECT,
	TSE_RECV,
	TSE_SEND
};

typedef std::pair<u_int32_t, u_int16_t> AddressPortPair;

class TcpServerEventInfo
{
public:
	TcpServerEventInfo();

public:
	TcpServerEventId m_EventId;
	u_int32_t m_uAddr;
	u_int16_t m_uPort;
	std::string m_sData;
};

typedef Event<TcpServerEventInfo> TcpServerNotifyEvent;

class TcpServer;
class ClientContext;

// IOCP completion port type definitions
typedef ClientContext* TcpServerCK;

#if 0 //for Linux yamada
typedef Winterdom::WinsockOverlapped TcpServerOV;
typedef Winterdom::IoCompPort<TcpServer, TcpServerOV, TcpServerCK> Iocp;
#endif //for Linux yamada

/** 
 * TcpServer class.
 *
 * Note: The "object owner" refers to the code that creates and uses objects 
 * of the TcpServer class.
 *
 * Note: For simplicity, each TcpServer object should only be accessed
 * from a single thread.  This saves the object from having to contain an
 * extra mutex object, and reduces chances for deadlock.
 *
 * Note: IP addresses and ports in this file are in network byte order.
 */
class TcpServer : 
	public Iocp,
	public ThreadServer
{
public:
	TcpServer();	
	virtual ~TcpServer();

	/**
	 * Initializes use of the TcpServer object.
	 * This function should called by the object owner before calling any
	 * of the other methods.  This function initiates a listening network 
	 * socket.
	 *
	 * @param uListenPort   the local port that the server will listen on
	 */
	virtual bool Open(u_int16_t uListenPort);

	/**
	 * Terminates use of the TcpServer object.
	 * This function indicates that this object is no longer in use.  The
	 * object must be re-opened (using the Open function) before any of the
	 * other methods can be called.
	 */
	virtual void Close(void);

	/**
	 * Retrieves a shared pointer to the TcpServer's notify event object.
	 * The owner object will receive events from TcpServer via this event 
	 * object.
	 *
	 * @param pNotifyEvent  shared pointer to receive the notify event
	 */
	virtual void GetEventPtr(SharedPtr<TcpServerNotifyEvent>& pNotifyEvent);

	/**
	 * Sends data to the specified IP address and port.  If the remote
	 * host given is not connected, the function fails.
	 *
	 * @param uRemoteAddr  destination address on a remote host
	 * @param uRemotePort  destination port on a remote host
	 * @param sBuf         buffer containing the data to be sent
	 * 
	 * @return true on success, false on failure
	 */
	virtual bool SendData(
		u_int32_t uRemoteAddr,
		u_int16_t uRemotePort,
		const std::string& sBuf);

	/**
	 * Sends a message to the specified IP address and port.  If the remote
	 * host given is not connected, the function fails.
	 *
	 * @param uRemoteAddr  destination address on a remote host
	 * @param uRemotePort  destination port on a remote host
	 * @param msg          a serializable message
	 *
	 * @return true on success, false on failure
	 */
	virtual bool SendMsg(
		u_int32_t uRemoteAddr,
		u_int16_t uRemotePort,
		const ISerializable& msg);

	/**
	 * Retrieves the number of buffered bytes received from the given host.
	 *
	 * @param uRemoteAddr  address on a remote host
	 * @param uRemotePort  port on a remote host
	 *
	 * @return number of bytes buffered
	 */
	virtual int GetReceiveBufferSize(
		u_int32_t uRemoteAddr,
		u_int16_t uRemotePort);

	/**
	 * Retrieves the number of buffered bytes to be sent to the given host.
	 *
	 * @param uRemoteAddr  address on a remote host
	 * @param uRemotePort  port on a remote host
	 *
	 * @return number of bytes buffered to be sent
	 */
	virtual int GetSendBufferSize(
		u_int32_t uRemoteAddr,
		u_int16_t uRemotePort);

	/**
	 * Disconnects the specified IP address and port.
	 * 
	 * @param uRemoteAddr  address of host to be disconnected
	 * @param uRemotePort  port of host to be disconnected
	 */
	virtual void Disconnect(
		u_int32_t uRemoteAddr,
		u_int16_t uRemotePort);

	/**
	 * Returns the local port, in network byte order.
	 *
	 * @return number of local port, if available
	 */
	virtual u_int16_t GetLocalPort(void);

public:
	// I/O Completion Port Callbacks
	void ProcessIoRequest(TcpServerOV* pState, TcpServerCK CompletionKey);
	void CleanUpIoRequest(TcpServerOV* pState, TcpServerCK CompletionKey);
	void FinalCleanUp(void);
	
	// used to notify the derived class of internal errors
	void OnIocpError(iocp_error error, DWORD dwWin32Err); 
	
	// notification of a new thread created
	// runs on the context of the new thread
	void OnThreadEntry(void);
	
	// notification just before a thread gets destroyed
	void OnThreadExit(void);
	
	// get a termination message
	void GetTerminationMsg(TcpServerCK& CompletionKey);
	
	// is this a termination message
	bool IsTerminationMsg(TcpServerCK CompletionKey);
	
protected:
	/**
	 * Called by internal thread to set a notification event.
	 */
	virtual void SetNotifyEvent(TcpServerEventInfo& tsei);
	
private:
	virtual void InternalThreadImpl(void);

	// Private helper functions
	void HandleAcceptEvent(void);
	void HandleSocketEvent(HANDLE hEvent);
	void HandleThreadEvent(bool& bStop);

	// Internal event handlers - call from internal thread only
	bool InternalOpen(void);
	bool InternalClose(void);
	
	void UpdateEventArray(void);

protected:
	u_int16_t m_uListenPort;
	SOCKET m_ListenSock;
	SharedPtr<TcpServerNotifyEvent> m_pNotifyEvent;
	HANDLE m_hAcceptEvent;
	SharedPtr<class Mutex> m_pMutex;
	SharedPtr<class ConnectionMap> m_pConnectionMap;
	HANDLE m_aEvents[64];
	int m_iNumEvents;
	bool m_bIocpEnabled;
};

#endif // TCP_SERVER_H
