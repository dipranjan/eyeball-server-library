// ---------------------------------------------------------------------------
// File:       pktbuf.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    
// 
// Change Log:
// ---------------------------------------------------------------------------

#ifndef PKTBUF_H
#define PKTBUF_H

#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#else
#include <sys/param.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/uio.h>
#endif 

#include "../../Base/Headers/Buffer.h"
#include "../../Base/Headers/config.h"
#include "../../RTP/Headers/PacketSize.h"		 // PKTBUF_SIZE


class pktbuf;

/*
 * The base object for performing the outbound path of
 * the application level protocol.
 */
class BufferPool
{
public:
	BufferPool();
	~BufferPool();
	
	void release(pktbuf*);
	/*
	 * Buffer allocation hooks.
	 */
	pktbuf* alloc(int layer = 0);

private:
	pktbuf* m_pFreeBufs;
};

class pktbuf : public Buffer
{
public:
	unsigned short len;
	u_int8_t data[PKTBUF_SIZE];

	pktbuf* next;
	int layer; // do we need this?
	int ref;
	u_int8_t* dp;

	BufferPool* manager;
	inline void release() { ref--; if (!ref) manager->release(this); }
	inline void attach()  { ref++; }
	Buffer* copy();
};

#endif // PKTBUF_H
