// CoSslSocket.h : Declaration of the CoSslSocket

#ifndef __COSSLSOCKET_H_
#define __COSSLSOCKET_H_

#define USE_MY_SSL

#ifndef USE_MY_SSL
	#include "SslSocket.h"
#else
	#include "SslSocketTest.h"
#endif

// CoSslSocket
class  CoSslSocket
{
public:
	CoSslSocket();

public:
	int Open(
		unsigned long uRemoteAddr, 
		unsigned short uRemotePort, 
		unsigned short uLocalPort, 
		unsigned long hNotifyEvent);
	int Close(void);
	int Send(const std::string& sBuf);
	int Recv(long nBytesToRecv, std::string& sBuf);
	int GetSocket(long* pSocket);

private:
#ifndef USE_MY_SSL
	SslSocket m_SslSocket;
#else
	SslSocketTest m_SslSocket;
#endif
};

#endif //__COSSLSOCKET_H_
