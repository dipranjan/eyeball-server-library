#ifndef _SslSocketTest_h_
#define _SslSocketTest_h_
                                                                                                                            
#include <unistd.h>

#include "IOException.h"
#include "TimeoutExceededException.h"

#include "openssl/ssl.h"
#include "openssl/err.h"


class SslSocketTest
{
 public:

 SslSocketTest();
 virtual ~SslSocketTest();

 void initLibrary();
 int connect(int fd);
 int read(char* buf, int len);
 void write(const char* buf, int len);
 void close();
 int getFd() const;

private:
  SSL_CTX* m_pSslContext;
  SSL* m_pSsl;
  bool m_bInit;
  int m_iFd;


};
#endif //_SslSocketTest_h_
