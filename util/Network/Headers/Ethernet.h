// ---------------------------------------------------------------------------
// File:       Ethernet.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Ethernet helper routines
// 
// Change Log:
// ---------------------------------------------------------------------------

#ifndef EYEBALL_NETWORK_ETHERNET_H
#define EYEBALL_NETWORK_ETHERNET_H

//#include <list>

#if 1 //for Linux yamada
#include <list>
#include <string>
#endif //for Linux yamada

struct MACAddress
{
	MACAddress();

	std::string to_str(void) const;
	bool from_str(const std::string& _str);

	unsigned char address[6];
};

typedef std::list<MACAddress> MACAddressList;


namespace Ethernet
{
	// Generates 64-bits extracted from a GUID
	bool generate_mac_addr(MACAddress& _MACAddress);

	// Get real MAC addresses, if available
	bool get_mac_addr(MACAddressList& _lsMACAddress);
};


#endif // EYEBALL_NETWORK_ETHERNET_H
