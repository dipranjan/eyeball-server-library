typedef int SOCKET;
typedef void* HANDLE;
typedef std::pair<u_int32_t, u_int16_t> AddressPortPair;//sazzad 22_12
class CTcpNetworkTest
{
public:
	CTcpNetworkTest();
        virtual ~CTcpNetworkTest();

	bool Open(
                u_int32_t uRemoteAddr,
                u_int16_t uRemotePort,
                u_int16_t uLocalPort,
                HANDLE hNotifyEvent);

	void Close(void);
	int  Send(const char* pBuf, int iLen);
	int  Recv(char* pBuf, int iLen);
	SOCKET GetSocket(void);



	int GetPortFromTurnServer();
	int TurnSendRequest(AddressPortPair& addrDest);
	int TurnSetActiveDestRequest(AddressPortPair& addrDest);
	int TurnSetActiveDestRequestNB(AddressPortPair& addrDest);

	AddressPortPair GetTurnDetectedAddrPort();
	AddressPortPair GetLocalAddrPort();

	void SetTurnParams(const std::string& sUserName,
                const std::string& sPassword,
                const std::string& sNonce,
                const std::string& sRealm
                );

	int ReleasePortFromTurnServer();



private:
	SOCKET m_ConnSock;
        u_int32_t m_uRemoteAddr;
        u_int16_t m_uRemotePort;
	SharedPtr<Mutex> m_pMutex;

/**************** Turn Support ****************/
        std::string m_sTurnUserName;
        std::string m_sTurnPassword;
        std::string m_sTurnNonce;
        std::string m_sTurnRealm;
        AddressPortPair m_TurnDetectedAddr;
/**************** Turn Support ****************/


};
