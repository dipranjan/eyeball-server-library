// File: UdpTest.h

#include "ThreadServer.h"
#include "UdpNetwork.h"

class UdpTest : public ThreadServer
{
public:
	UdpTest();
	virtual ~UdpTest();

	bool StartTest(
		AddressPortPair addrEcho, 
		HANDLE hNotifyEvent);
	void StopTest(void);

	enum
	{
		RESULT_UNKNOWN = 0,
		RESULT_OK = 1,
		RESULT_FAIL = 2
	};

	DWORD GetResult(void);

protected:
	virtual void InternalThreadImpl(void);

private:
	void HandleNetEvent(void);
	void SetResult(DWORD dwResult);

private:
	HANDLE m_hNetEvent;
	HANDLE m_hNotifyEvent;
	AddressPortPair m_addrEcho;
	SharedPtr<UdpNetwork> m_pUdpNetwork;
	bool m_bStop;
	int m_nDetectedPort1;
	int m_nDetectedPort2;

	volatile DWORD m_dwResult;
};
