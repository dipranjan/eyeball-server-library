// ---------------------------------------------------------------------------
// File:		RudpNetwork.h
//
// Copyright:	Eyeball Networks Inc. 1999-2001
//
// Purpose:		Declares the RudpNetwork class
// 
// ---------------------------------------------------------------------------

#ifndef RUDP_NETWORK_H
#define RUDP_NETWORK_H

#include "ENetwork.h"
#include "inet.h"
#include "utypes.h"
//#include "SmartPtr.h"
#include "ReturnCodes.h"

class Thread;
class Mutex;

class EchoAddressInfo;
class ISerializable;
class UdpNetwork;
class RUDPPacket;
class RUDPPacketList;

// RUDP Events
#define RE_READ         0x0001
#define RE_SEND         0x0002
#define RE_CONNECT      0x0004
#define RE_DISCONNECT   0x0008
#define RE_DEBUGINFO    0x0010
#define RE_ECHO_ADDRESS 0x0020

enum RS_STATE
{
	RS_CLOSED = 0,
	RS_BOUND,
	RS_OPEN,
	RS_ESTABLISHED
};

enum NetworkFirewallType
{
	FIREWALL_TYPE_UNKNOWN = 0,
	FIREWALL_TYPE_NONE,
	FIREWALL_TYPE_NAT,
	FIREWALL_TYPE_PAT,
	FIREWALL_TYPE_PAT_PLUS,
	FIREWALL_TYPE_IPCHAINS,
	FIREWALL_TYPE_SYMMETRIC
};


// Note: IP Addresses and ports in this file are in network byte order.

class RudpNetwork : noncopyable
{
public:
	RudpNetwork();
	virtual ~RudpNetwork();
	
	virtual int Bind(u_int16_t& uLocalPort);
	virtual u_int16_t GetLocalPort(void);
	virtual AddressPortPair GetLocalAddress(void);

	// Creates the network thread and opens the send/recv sockets.
	virtual int Open(
		u_int32_t uRemoteAddr,
		u_int16_t uRemotePort,
		u_int16_t uLocalPort,
		HANDLE hNotifyEvent);

	virtual int OpenWait(
		u_int32_t uRemoteAddr1,
		u_int32_t uRemoteAddr2,
		u_int16_t uRemotePort,
		u_int16_t uLocalPort,
		HANDLE hNotifyEvent,
		u_int32_t uTimeout = 0);

	/**
	 * Works similarly to the OpenWait function, but returns immediately.
	 *
	 * The Send/SendMsg functions may be used to determine if the connection
	 * fails.  If Send is used while the connection is in progress, the Send
	 * function succeeds, and the data is queued.
	 *
	 * @return ES return code
	 */
	virtual int OpenAsync(
		u_int32_t uRemoteAddr1,
		u_int32_t uRemoteAddr2,
		u_int16_t uRemotePort,
		u_int16_t uLocalPort,
		HANDLE hNotifyEvent,
		u_int32_t uTimeout = 0);

	/**
	 * Terminates the RUDP connection.
	 * @param iReason, default value -1004, ES_NOT_CONNECTED
	 * @param bKeepSocket  if true, the internal socket is not closed
	 *    This is useful to avoid having to re-bind, etc. when retrying
	 *    connections.
	 */
	virtual int Close(int iReason = ES_NOT_CONNECTED, bool bKeepSocket = false);

	virtual int Send(const std::string& sBuf);
	virtual int SendMsg(const ISerializable& msg);
	
	virtual int Recv(std::string& sBuf);

	/**
	 * Possible states:
	 *
	 * RS_CLOSED - the initial idle state
	 * RS_BOUND - the object's socket is bound to a port
	 * RS_OPEN - open has been called, and a connection attempt is in progress
	 * RS_ESTABLISHED - the connection has been established, and data can be
	 *                  transferred.
	 */
	virtual RS_STATE GetConnectionState(void);
	
	// return true if state is RS_OPEN or RS_ESTABLISHED
	virtual bool IsOpen(void);

	virtual u_int32_t GetRemoteAddr(void);
	virtual u_int16_t GetRemotePort(void);

	// Enable or disable notification of specified events.
	virtual void EnableEvent(u_int16_t uEvent, bool bEnable);
	virtual u_int16_t GetEventFlags(void);

	// Debug info/statistics
	// The following functions are mostly only useful for stats/debugging
	virtual void GetDebugInfo(std::string& sText);
	virtual u_int32_t GetTotalTimeouts(void);
	virtual u_int32_t GetTimeoutPeriod(void);
	virtual u_int16_t GetWindowSize(void);

	// Returns number of bytes in the send-queue (not yet sent).
	// Note that if the RudpNetwork is disconnected, this data
	// will not be sent.
	// The return value is signed to make subtraction more convenient.
	virtual int GetSendQueueSize(void);

	virtual u_int32_t GetAverageRtt(void);

	// Echo-server based address detection functions
	virtual void SendEchoAddressRequest(AddressPortPair addrEcho, int nTtl = 0);
	virtual AddressPortPair GetDetectedAddress(void);
	virtual u_int32_t GetDetectedLocalAddress(void);
	virtual int GetFirewallType();
	
	/** 
	 * Returns if port is translated, based on echo'd local address.
	 * If the answer is unknown, bUnknown is returned.  The answer should
	 * be known after a connection is established.
	 */
	virtual bool IsPortTranslated(bool bUnknown = false);

	/** 
	 * Returns if IP address is translated, based on echo'd local address.
	 * If the answer is unknown, bUnknown is returned.  The answer should
	 * be known after a connection is established.
	 */
	virtual bool IsAddressTranslated(bool bUnknown = false);

	/**
	 * Returns true if the remote address is translated.
	 * If the answer is unknown, bUnknown is returned.
	 * SetRemoteAddressTranslated() affects the return value if the
	 * answer is unknown.
	 */
	virtual bool IsRemoteAddressTranslated(bool bUnknown);

	/**
	 * Gives the RUDP network a hint as to whether the remote address is
	 * translated or not.  It is originally assumed to be true.
	 */
	virtual void SetRemoteAddressTranslated(bool bTranslated);

	/**
	 * Sets the detected remote address.
	 * This should be called when the echo address is received from the remote.
	 */
	virtual void SetDetectedRemoteAddress(const AddressPortPair& addr);
	virtual AddressPortPair GetDetectedRemoteAddress(void);

	/**
	 * check if port incrementing PAT firewall is in use
	 * returns false if unknown
	 */
	bool IsPortIncremented(void) const;

	/**
	 * true if connected to another peer on the same LAN, as determined by
	 * echo'd IP addresses.
	 */
	bool IsInSameLAN() const;

	void FlushSendQueue(void);

	void SetRemoteBehindFirewall(bool bFirewall);
	void SetLocalBehindFirewall(bool bFirewall);
	void UseFirewallServer();

private:
	static void *RudpNetworkThread(void *arg);
	void RudpNetworkThreadImpl(void);

	bool AttemptConnection(void);

	void ProcessRecvEvent(void);
	void ProcessSendEvent(void);
	void ProcessTimeoutEvent(void);

	// sends a packet, automatically keeping track of the number of times sent
	void SendPacket(
		RUDPPacket& packet, 
		u_int32_t uAddr = 0, 
		u_int16_t uPort = 0);
	
	void IncrSN(u_int32_t& x);

	void SetTimeout(void);
	void UpdateDelayValues(const RUDPPacket& packet);
	void UpdateWindowSize(DWORD dwDelay);
	void QueueForSending(RUDPPacket& packet);

	void SendAckPacket(u_int32_t uSeqNo);
	void SendSynPacket(u_int32_t uAddr, u_int16_t uPort);
	void SendFinPacket(void);

	void QueueSynPacket(void);

	void ResetConnectionState(void);

	void CompleteConnection(void);
	bool IsConnecting(void) const;
	void SetDebugInfoImpl(const std::string& sText);

	void NotifyEvent(u_int16_t uEvent);

	void RestoreDefaultTtl(void);

	void NotifyEchoResponse(void);
	void SetSendEvent();

private:
	SharedPtr<Mutex> m_pMutex;
	RS_STATE m_rsState;
	SharedPtr<RUDPPacketList> m_pSendQ;
	SharedPtr<RUDPPacketList> m_pRecvQ;
	u_int32_t m_uMaxSeqNo;
	u_int32_t m_uSentSeqNo;  // last packet sent and unack'd
	u_int16_t m_uSentPacketCount;  // number of sent, unack'd packets
	u_int16_t m_uWindowSize;
	u_int16_t m_uWindowIncCounter;

	u_int32_t m_uReceivedSeqNo;

	u_int32_t m_uRemoteAddr; // currently active remote IP
	u_int16_t m_uRemotePort;	

	u_int32_t m_uRemoteAddr1; // possible remote IP's
	u_int32_t m_uRemoteAddr2;
	AddressPortPair m_DetectedRemoteAddr;
	bool m_bRemoteAddressTranslated;
	u_int32_t m_uOpenStartTime;
	u_int32_t m_uOpenExpiryTime;

	SharedPtr<Thread> m_pThread;
	SharedPtr<UdpNetwork> m_pUdpNetwork;
	HANDLE m_hNotifyEvent; // created by user
	u_int16_t m_uEventFlags;
	u_int16_t m_uEventFilter;

	HANDLE m_hQuitEvent;
	HANDLE m_hRecvEvent;
	HANDLE m_hSendEvent;
	HANDLE m_hConnectEvent;

	u_int32_t m_uTimeout;
	u_int32_t m_uLastTimeout;
	u_int32_t m_uAverageRTT;
	u_int32_t m_uAverageDev;
	u_int32_t m_uSeqNo;

	std::string m_sDebugInfo;

	AddressPortPair m_EchoServerAddr;
	AddressPortPair m_DetectedLocalAddr;
	AddressPortPair m_EchoServerAddr2;
	AddressPortPair m_DetectedLocalAddr2;
	bool m_bPortIncremented;
	bool m_bRemoteBehindFirewall;
	bool m_bLocalBehindFirewall;
	
	/**
	 * The time of the last echo request sent.
	 * Set to zero if an echo response is no longer required.
	 */
	DWORD m_dwSentEchoRequest;

	u_int32_t m_uTotalTimeouts;
	bool m_bSlowStart;

	/**
	 * Custom TTL to help with firewall compatibility.
	 */
	int m_nTtl;

	/**
	 * TTL of a default Winsock socket.
	 */
	int m_nDefaultTtl;

	DWORD m_dwTtlSetTime;

	int  m_iCloseReason;
	bool m_bUseFirewallServer;
	SharedPtr<class DisposableSocket> m_pDisposableSocket;
};

#endif // RUDP_NETWORK_H
