#if !defined(_IOException_h)
#define _IOException_h

#include "Exception.h"
#include <assert.h>
#include <stdarg.h>
#include <string.h>


class IOException : public Exception
{
public:
    IOException() : Exception() {}


    IOException(const char* newDetails, ...)
    {
        va_list ap;
        va_start(ap, newDetails);
        setDetails(newDetails, ap);
        va_end(ap);
    }

    IOException(long errValue)
    {
        setValue(errValue);
    }

    IOException(long errValue, const char* newDetails, ...)
    {
        setValue(errValue);
        va_list ap;
        va_start(ap, newDetails);
        setDetails(newDetails, ap);
        va_end(ap);
    }
};

#endif
