#if !defined(_Exception_h)
#define _Exception_h

#include <assert.h>
#include <stdarg.h>
#include <stdio.h>

class Exception
{
public:
    Exception() { details[0] = 0; }


    Exception(const char* newDetails, ...)
    {
        va_list ap;
        va_start(ap, newDetails);
        setDetails(newDetails, ap);
        va_end(ap);
    }

    Exception(long errValue)
    {
        setValue(errValue);
    }

    Exception(long errValue, const char* newDetails, ...)
    {
        setValue(errValue);
        va_list ap;
        va_start(ap, newDetails);
        setDetails(newDetails, ap);
        va_end(ap);
    }

    const char *getDetails() { return details; }

    long getValue(void)
    {
        return errorValue;
    }

protected:
    void setDetails(const char* newDetails, va_list ap)
    {
#if 1 //for Linux yamada
        vsnprintf(details, 500, newDetails, ap);
#else //for Linux yamada
        _vsnprintf(details, 500, newDetails, ap);
#endif //for Linux yamada
    }

    void setValue(long value)
    {
        errorValue = value;
    }

    char details[500];
    long errorValue;
};

#endif
