// ---------------------------------------------------------------------------
// File:       CoSslMessage.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    COM-based SSL message class
// ---------------------------------------------------------------------------

#ifndef _CO_SSL_MESSAGE_
#define _CO_SSL_MESSAGE_

#include "GeneralMessage.h"

class CommandMessage;

class CoSslMessage : public GeneralMessage
{
public:
	CoSslMessage();
	virtual ~CoSslMessage();

	virtual bool Open(
		u_int32_t uRemoteAddr,
		u_int16_t uRemotePort,
		u_int16_t uLocalPort,
		HANDLE hNotifyEvent);	
	
	virtual void Close(void);

	virtual int RecvMsg(std::string& _sBuf);

	virtual int SendMsg(const ISerializable& msg);
	virtual int SendMsg(const std::string& sBuf);
	virtual int SendMsg(const char *pBuf, int iLen);

	virtual void SetGlobalLocalAddr(void);

	virtual bool SetSendBufferSize(int iSize);
	virtual bool IsSocketWritable(int iTimeout);

private:
	virtual int GetHeader(CommandMessage& msgHdr);
	virtual int DoRecv(std::string& sBuf, int iLen);

private:
	SharedPtr<class CoSslMessageInternal> m_spInternal;
	SharedPtr<class Mutex>	m_pMutex;
};

#endif // _CO_SSL_MESSAGE_
