// ---------------------------------------------------------------------------
// File:       GeneralNetwork.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Base class for network objects.
//
// ---------------------------------------------------------------------------

#ifndef _GENERAL_NETWORK_
#define _GENERAL_NETWORK_

/**
 * Common base class for network objects.
 */
#if 1 //for Linux yamada
class GeneralNetwork
#else
class GeneralNetwork : noncopyable
#endif //for Linux yamada
{
public:
	virtual ~GeneralNetwork() {};

	/**
	 * Opens a connection to the specified remote host.
	 *
	 * @param uRemoteAddr  address of remote host, in network byte order
	 * @param uRemotePort  port of remote host, in network byte order
	 * @param uLocalPort   port of local host, in network byte order.
	 *                     Set to 0 to use dynamic binding.
	 * @param hEvent  (optional) handle to windows event
	 *
	 * @return true on success, false on failure
	 */
	virtual bool Open(
		u_int32_t uRemoteAddr,
		u_int16_t uRemotePort,
		u_int16_t uLocalPort,
		HANDLE hEvent) = 0;	
	
	/**
	 * Closes the connection.
	 */
	virtual void Close(void) = 0;

	/**
	 * Sends the given bytes to the remote host.
	 *
	 * @param pBuf  buffer containing bytes to send
	 * @param iLen  length of buffer
	 *
	 * @return number of bytes sent on success, or an error code on failure:
	 *   ES_FAILURE       - unknown error
	 *   ES_NOT_CONNECTED - a connection was not established, or the connection
	 *                      has been lost.
	 */
	virtual int Send(const char* pBuf, int iLen) = 0;

	/**
	 * Receives bytes from the remote host.
	 *
	 * @param pBuf  buffer to receive bytes from network
	 * @param iLen  length of buffer
	 *
	 * @return number of bytes received on success, or an error code on failure:
	 * failure:
	 *   ES_FAILURE       - unknown error
	 *   ES_NOT_CONNECTED - a connection was not established, or the connection
	 *                      has been lost.
	 */
	virtual int Recv(char* pBuf, int iLen) = 0;

	/**
	 * Retrieves the socket.  In Unix, this is equivalent to a file descriptor.
	 *
	 * @return socket in use by network object
	 */
	virtual SOCKET GetSocket(void) = 0;

	/**
	 * Gives the object ownership of the given socket.  The object will close
	 * this socket automatically upon object destruction.
	 *
	 * @param sock  socket to be attached to this object
	 */
	virtual void SetSocket(SOCKET sock) = 0;
};

#endif  // _GENERAL_NETWORK_
