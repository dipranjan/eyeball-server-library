// ---------------------------------------------------------------------------
// File:       SslMessage.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Author:     Jozsef Vass
//
// Purpose:    SSL message class
//
// Change Log: 07/31/2001 - Created
// ---------------------------------------------------------------------------

#ifndef __SSL_MESSAGE__
#define __SSL_MESSAGE__

#include "SslSocket.h"
#include "TcpMessage.h"

class SslMessage : public TcpMessage
{
public:
	SslMessage();
	virtual ~SslMessage();
        virtual bool Open(
                u_int32_t uAddr,
                u_int16_t uRemotePort,
                u_int16_t uLocalPort,
                HANDLE hNotifyEvent);
                                                                           

	virtual void Close(void);
	virtual bool OnConnect(void);
	virtual int DoRecv(char* pBuf, int iLen);
protected:
	virtual int DoSend(const char *pBuf, int iLen);

	virtual bool EnumNetworkEvents(WSANETWORKEVENTS& netEvents);

private:
	SslSocket m_SslSocket;
};

#endif  // __SSL_MESSAGE__
