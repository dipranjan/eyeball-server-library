// ---------------------------------------------------------------------------
// File:       EchoMessage.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2002
//
// Purpose:    Echo Server port discovering message
//
// Change Log: 01/16/2002
// ---------------------------------------------------------------------------

#include "Base.h"  // automatic use of precompiled headers

#include "EchoMessage.h"
#include "inet.h"

#if 0 //foe Linux yamada
#ifdef WIN32
#include "TextUtil.h"
#else
#include "StringTokenizer.h"
#endif
#endif //for Linux yamada

using std::string;

AddressEchoMessage::AddressEchoMessage()
:
	CommandMessage(),

	m_u32IpAddress(0),
	m_u16IpPort(0)
{}

void AddressEchoMessage::serialize(string& sBuf) const
{
	SERIALIZE_BASE_CLASS(CommandMessage);

	SERIALIZE32(m_u32IpAddress);
	SERIALIZE16(m_u16IpPort);
}

int AddressEchoMessage::deserialize(const string& sBuf)
{
	int iOffset = 0;

	DESERIALIZE_BASE_CLASS(CommandMessage);

	DESERIALIZE32(m_u32IpAddress);
	DESERIALIZE16(m_u16IpPort);

	return iOffset;
}

u_int16_t AddressEchoMessage::payload_length(void) const
{
	return sizeof(m_u32IpAddress) + sizeof(m_u16IpPort);
}

#ifndef _WIN32_WCE
ostream& operator<<(ostream& s, const AddressEchoMessage& v)
{
	s << (CommandMessage)v
		<< " " << string(intoa(htonl(v.m_u32IpAddress))).c_str()
		<< ":" << v.m_u16IpPort;

	return s;
}
#endif

