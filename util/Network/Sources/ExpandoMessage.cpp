// ---------------------------------------------------------------------------
// File:       EchoMessage.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2002
//
// Purpose:    Echo Server port discovering message
//
// Change Log: 01/16/2002
// ---------------------------------------------------------------------------

#include "Base.h"  // automatic use of precompiled headers
using std::string;
using std::map;

#include "ExpandoMessage.h"
#include "inet.h"

#if 1 //for Linux yamada
#include "TextUtil.h"

#else
#ifndef _WIN32_WCE
#include <time.h>
#endif

#ifdef WIN32
#include "TextUtil.h"
#else
#include "StringTokenizer.h"
#endif

#endif //for Linux yamada


ExpandoMessage::ExpandoMessage(u_int8_t u8MessageType)
:
	CommandMessage(u8MessageType),
	m_map()
{}

bool ExpandoMessage::Get(const tstring& sKey, tstring& sValue) const
{
	std::string sKeyA = EncodeUcn(sKey);
	std::string sValueA;
	if (!InternalGet(sKeyA, sValueA))
		return false;

	sValue = DecodeUcn(sValueA);

	return true;
}

void ExpandoMessage::Set(const tstring& sKey, const tstring& sValue)
{
	InternalSet(EncodeUcn(sKey), EncodeUcn(sValue));
}

bool ExpandoMessage::GetA(const tstring& sKey, string& sValue) const
{
	std::string sKeyA = EncodeUcn(sKey);
	return InternalGet(sKeyA, sValue);
}

void ExpandoMessage::SetA(const tstring& sKey, const string& sValue)
{
	InternalSet(EncodeUcn(sKey), sValue);
}

bool ExpandoMessage::Get(const tstring& sKey, int& nValue) const
{
	std::string sKeyA = EncodeUcn(sKey);
	std::string sValueA;
	if (!InternalGet(sKeyA, sValueA))
		return false;

	nValue = atoi(sValueA.c_str());

	return true;
}

void ExpandoMessage::Set(const tstring& sKey, int nValue)
{
	std::string sValue = itoa(nValue);
	InternalSet(EncodeUcn(sKey), sValue);
}

void ExpandoMessage::serialize(string& sBuf) const
{
	SERIALIZE_BASE_CLASS(CommandMessage);

	u_int16_t uSize = (u_int16_t)m_map.size();
	SERIALIZE16(uSize);

	ExpandoMap::const_iterator iter = m_map.begin();
	for (; iter != m_map.end(); iter++)
	{
		SERIALIZE_STR(iter->first);
		SERIALIZE_STR(iter->second);
	}
}

int ExpandoMessage::deserialize(const string& sBuf)
{
	int iOffset = 0;

	DESERIALIZE_BASE_CLASS(CommandMessage);

	u_int16_t uSize = 0;
	DESERIALIZE16(uSize);

	m_map.clear();
	for (unsigned int i = 0; i < uSize; i++)
	{
		std::string sKey;
		DESERIALIZE_STR(sKey);
		std::string sValue;
		DESERIALIZE_STR(sValue);

		m_map.insert(ExpandoMap::value_type(sKey, sValue));
	}

	return iOffset;
}

u_int16_t ExpandoMessage::payload_length(void) const
{
	u_int16_t uSize = 0;

	uSize += sizeof(u_int16_t); // map size field

	ExpandoMap::const_iterator iter = m_map.begin();
	for (; iter != m_map.end(); iter++)
	{
		uSize += (iter->first).size() + sizeof(u_int16_t);
		uSize += (iter->second).size() + sizeof(u_int16_t);
	}

	return uSize;
}

#ifndef _WIN32_WCE
ostream& operator<<(ostream& s, const ExpandoMessage& v)
{
	s << (CommandMessage)v;
	
	s << " [ ";
	ExpandoMap::const_iterator iter = v.m_map.begin();
	for (; iter != v.m_map.end(); iter++)
	{
		s << iter->first.c_str() << "=" << iter->second.c_str() << " ";
	}
	s << "]";

	return s;
}
#endif

tstring ExpandoMessage::GetString(const string& sKey) const
{
	tstring sValue;
	bool bOk = Get(sKey, sValue);
	if( !bOk )
		return "";
	return sValue;
}

int ExpandoMessage::GetInt(const string& sKey) const
{
	tstring sValue;
	bool bOk = Get(sKey, sValue);
	if( !bOk )
		return 0;
	return atoi(sValue.mbs().c_str());
}

// Protected Functions
bool ExpandoMessage::InternalGet(const std::string& sKey, std::string& sValue) const
{
	ExpandoMap::const_iterator iter = m_map.find(sKey);
	if (m_map.end() == iter)
		return false;

	sValue = iter->second;

	return true;
}

void ExpandoMessage::InternalSet(const std::string& sKey, const std::string& sValue)
{
	m_map[sKey] = sValue;
}

// Self-test function
void ExpandoMessage::SelfTest(void)
{
#ifdef _DEBUG
	DB("Testing ExpandoMessage");

	tstring keya = "key_a";
	tstring keyb = "key_b";
	tstring valuea = "value_a";
	tstring valueb = "value_b";

	tstring keyc = "numeric_key";
	int valuec = 123;

	ExpandoMessage msg;
	msg.Set(keya, valuea);
	msg.Set(keyb, valueb);
	msg.Set(keyc, valuec);

	DB_STAT(msg);
	DB_STAT(msg.payload_length() + CM_LENGTH);

	std::string sBuf;
	msg.serialize(sBuf);
	DB_STAT(sBuf.size());

	ExpandoMessage msg2;
	int iRet = msg2.deserialize(sBuf);
	
	DB_STAT(msg2);

	tstring tmp;
	msg2.Get(keya, tmp);
	DB_ASSERT(tmp == valuea);

	msg2.Get(keyb, tmp);
	DB_ASSERT(tmp == valueb);

	int n = 0;
	msg2.Get(keyc, n);
	DB_ASSERT(n == valuec);

	DB("Performing randomized test cases:");
	srand(time(NULL));

	int nIter = 1000;
	int nMaxSizePerIter = 10;
	int nMaxLengthPerString = 100;
	for (int i = 0; i < nIter; i++)
	{
		ExpandoMessage m1;

		int nSize = rand() % nMaxSizePerIter;
		for (int j = 0; j < nSize; j++)
		{
			tstring sKey;
			int nKeyLength = rand() % nMaxLengthPerString;
			for (int k = 0; k < nKeyLength; k++)
			{
				//sKey += (TCHAR)rand();
				sKey += (char)rand();
			}

			tstring sValue;
			int nValueLength = rand() % nMaxLengthPerString;
			for (int v = 0; v < nValueLength; v++)
			{
				sValue += (TCHAR)rand();
			}

			m1.Set(sKey, sValue);

			tstring sValue2;
			m1.Get(sKey, sValue2);
			DB_ASSERT(sValue == sValue2);
		}

		DB("Iteration " << i);

		DB_STAT(m1.m_map.size());
		DB_STAT(m1);

		m1.serialize(sBuf);
		DB_STAT(sBuf.size());

		ExpandoMessage m2;
		m2.deserialize(sBuf);

		DB_STAT(m2.m_map.size());
		DB_STAT(m2);

		DB_ASSERT(m1.m_map == m2.m_map);
	}

	DB("Test complete");
#endif //_DEBUG
}
