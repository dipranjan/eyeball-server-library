// ---------------------------------------------------------------------------
// File:       TcpServer.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    
// 
// Change Log:
// ---------------------------------------------------------------------------

#include "Base.h"  // automatic use of precompiled headers 

#include "critsec.h"
#include "ENetwork.h"
#include "Event.h"
#if 1 //for Linux yamada
#include "ELinuxUtil.h"
#else
#include "EWinUtil.h"
#endif //for linux yamada
#include "ReturnCodes.h"
#include "TcpMessage.h"
#include "TcpServer.h"
#include "Thread.h"
#include "TextUtil.h"

using std::string;
using namespace eyeball;

#define CPU_COUNT  (1)

// I/O completion port parameters
#define IOCP_CONCURRENT_THREADS   (0)
#define IOCP_INITIAL_MAX_THREADS  (CPU_COUNT * 2)
#define IOCP_TIMEOUT  (INFINITE)

//#define DISABLE_IOCP


// ---------------------------------------------------------------------------
// ClientContext class
// ---------------------------------------------------------------------------

class ClientContext : public OVERLAPPED
{
public:
	ClientContext() 
	:
		m_uAddr(0),
		m_uPort(0)
	{ 
		// Zero the overlapped structure
		memset(this, 0, sizeof(OVERLAPPED)); 
	}

public:
	u_int32_t m_uAddr;
	u_int16_t m_uPort;
};


// ---------------------------------------------------------------------------
// TcpServerEventInfo class
// ---------------------------------------------------------------------------

TcpServerEventInfo::TcpServerEventInfo()
: 
	m_EventId(TSE_UNKNOWN),
	m_uAddr(0),
	m_uPort(0),
	m_sData()
{
}


// ---------------------------------------------------------------------------
// Connection class - connection socket and related information
// ---------------------------------------------------------------------------

class Connection
{
public:
	Connection(bool bIocpEnabled);

	void SetSocket(SOCKET sock);

	int RecvMsg(std::string& sBuf);
	int SendMsg(const std::string& sBuf);

	void SetAddr(u_int32_t uAddr);
	u_int32_t GetAddr(void) const;
	void SetPort(u_int16_t uPort);
	u_int16_t GetPort(void) const;
	
	void SetEventHandle(HANDLE hEvent);

	void CleanUp(void);

public:
	SharedPtr<TcpMessageEx> m_pNetwork;
	TcpServerCK m_ck;
	SharedPtr<SimpleEvent> m_pEvent;
};

Connection::Connection(bool bIocpEnabled)
:
	m_pNetwork(new TcpMessageEx(bIocpEnabled)),
	m_ck(new ClientContext),
	m_pEvent(new SimpleEvent)
{
}

void Connection::SetSocket(SOCKET sock)
{
	m_pNetwork->SetSocket(sock);
}

int Connection::RecvMsg(string& sBuf)
{
	return m_pNetwork->RecvMsg(sBuf);
}

int Connection::SendMsg(const string& sBuf)
{
	return m_pNetwork->SendMsg(sBuf);
}

void Connection::SetAddr(u_int32_t uAddr)
{
	m_ck->m_uAddr = uAddr;
	m_pNetwork->SetRemoteAddr(uAddr);
}

u_int32_t Connection::GetAddr(void) const
{
	return m_pNetwork->GetRemoteAddr();
}

void Connection::SetPort(u_int16_t uPort)
{
	m_ck->m_uPort = uPort;
	m_pNetwork->SetRemotePort(uPort);
}

u_int16_t Connection::GetPort(void) const
{
	return m_pNetwork->GetRemotePort();
}

void Connection::SetEventHandle(HANDLE hEvent)
{
	m_pEvent->SetHandle(hEvent);
	m_pNetwork->SetNotifyEvent(hEvent);
}

void Connection::CleanUp(void)
{
	m_pNetwork->DeleteReceiveBuffer();
	SAFE_DELETE(m_ck);
}


// ---------------------------------------------------------------------------
// ConnectionMap class
// ---------------------------------------------------------------------------

class ConnectionMap : public AddressPortPairTMap<Connection>
{
public:
	ConnectionMap() {}

	bool FindEvent(HANDLE hEvent, iterator& iter)
	{
		if (NULL == hEvent)
			return false;

		for (iter = begin(); iter != end(); iter++)
		{
			Connection& con = iter->second;
			if (con.m_pEvent->GetHandle() == hEvent)
				return true;
		}

		return false;
	}
};

typedef ConnectionMap::iterator ConnectionMapIter;


// ---------------------------------------------------------------------------
// TcpServer class
// ---------------------------------------------------------------------------

TcpServer::TcpServer()
:
	m_uListenPort(0),
	m_ListenSock(INVALID_SOCKET),
	m_pNotifyEvent(new TcpServerNotifyEvent),
	m_hAcceptEvent(NULL),
	m_pMutex(new Mutex),
	m_pConnectionMap(new ConnectionMap),
	m_aEvents(),
	m_iNumEvents(0),
	m_bIocpEnabled(true)
{
	ASSERT(m_hAcceptEvent = CreateEvent(NULL, FALSE, FALSE, NULL));
	StartInternalThread();
}

TcpServer::~TcpServer()
{
	if (m_pConnectionMap.get() != NULL)
	{
		if (!m_pConnectionMap->empty())
		{
			DB("Warning: connection map not empty");
		}
	}

	if ((0 != m_uListenPort) || (INVALID_SOCKET != m_ListenSock))
	{
		DB("Warning: object not closed");
	}

	// Finish all these before object goes out of scope
	StopInternalThread();

	CLOSE_HANDLE(m_hAcceptEvent);
}

bool TcpServer::Open(u_int16_t uListenPort)
{
	DB_ENTER(TcpServer::Open);

	m_uListenPort = uListenPort;
	return SetEventAndWait(TSTE_INTERNAL_OPEN);
}

void TcpServer::Close(void)
{
	DB_ENTER(TcpServer::Close);

	SetEventAndWait(TSTE_INTERNAL_CLOSE);
	m_uListenPort = 0;
}

void TcpServer::GetEventPtr(SharedPtr<TcpServerNotifyEvent>& pNotifyEvent)
{
	DB_ASSERT(m_pNotifyEvent.get() != NULL);
	pNotifyEvent = m_pNotifyEvent;
}

bool TcpServer::SendData(
	u_int32_t uRemoteAddr,
	u_int16_t uRemotePort,
	const string& sBuf)
{
	Critical_Section cs(*m_pMutex);

	ConnectionMapIter iter;
	if (!m_pConnectionMap->Find(uRemoteAddr, uRemotePort, iter))
		return false;

	Connection& con = iter->second;
	int iRet = con.SendMsg(sBuf);
	if (iRet < 0)
		return false;

	return true;
}

bool TcpServer::SendMsg(
	u_int32_t uRemoteAddr,
	u_int16_t uRemotePort,
	const ISerializable& msg)
{
	string sBuf;
	msg.serialize(sBuf);
	if (sBuf.empty())
		return false;

	return SendData(uRemoteAddr, uRemotePort, sBuf);
}

int TcpServer::GetReceiveBufferSize(
	u_int32_t uRemoteAddr,
	u_int16_t uRemotePort)
{
	Critical_Section cs(*m_pMutex);

	ConnectionMapIter iter;
	if (!m_pConnectionMap->Find(uRemoteAddr, uRemotePort, iter))
		return 0;
	
	Connection& con = iter->second;
	return con.m_pNetwork->GetReceiveBufferSize();
}

int TcpServer::GetSendBufferSize(
	u_int32_t uRemoteAddr,
	u_int16_t uRemotePort)
{
	Critical_Section cs(*m_pMutex);

	ConnectionMapIter iter;
	if (!m_pConnectionMap->Find(uRemoteAddr, uRemotePort, iter))
		return 0;
	
	Connection& con = iter->second;
	return con.m_pNetwork->GetSendBufferSize();
}

void TcpServer::Disconnect(u_int32_t uRemoteAddr, u_int16_t uRemotePort)
{
	Critical_Section cs(*m_pMutex);

	ConnectionMapIter iter;
	if (!m_pConnectionMap->Find(uRemoteAddr, uRemotePort, iter))
		return;
	
	Connection& con = iter->second;
	con.m_pNetwork->Close();
}

u_int16_t TcpServer::GetLocalPort(void)
{
	Critical_Section cs(*m_pMutex);

	return Network::GetSockName(m_ListenSock).second;
}	


// ---------------------------------------------------------------------------
// I/O Completion Ports
// ---------------------------------------------------------------------------

void TcpServer::ProcessIoRequest(TcpServerOV* pState, TcpServerCK ck)
{
	//DB_ENTER(TcpServer::ProcessIoRequest);

	Critical_Section cs(*m_pMutex);

	ConnectionMapIter iter;
	if (!m_pConnectionMap->Find(ck->m_uAddr, ck->m_uPort, iter))
		return;

	Connection& con = iter->second;

	//DB_STAT(intoa(con.GetAddr()).c_str());
	//DB_STAT(itoa(ntohs(con.GetPort())).c_str());

	bool bDisconnected = false;  // set to true to disconnect client

	int iInitialSendBufSize = con.m_pNetwork->GetSendBufferSize();

	if (NULL != pState)
	{
		int iRet = con.m_pNetwork->IoRequestComplete(pState);
		pState = NULL;
		if (ES_NOT_CONNECTED == iRet)
		{
			bDisconnected = true;
		}
	}

	TcpServerEventInfo tsei;
	tsei.m_uAddr = con.GetAddr();
	tsei.m_uPort = con.GetPort();

	if (bDisconnected)
	{
		// clean up socket and other data structures.
		con.CleanUp();
		m_pConnectionMap->erase(iter);
			
		// Notify owner of disconnection
		tsei.m_EventId = TSE_DISCONNECT;
		SetNotifyEvent(tsei);
		return;
	}

	int iRet = 0;
	while (iRet >= 0)
	{
		string sBuf;
		iRet = con.RecvMsg(sBuf);
		
		if (sBuf.empty())
			continue;

		// Notify owner of new data received
		tsei.m_EventId = TSE_RECV;
		tsei.m_sData = sBuf;
		SetNotifyEvent(tsei);
	}

	if (con.m_pNetwork->GetSendBufferSize() < iInitialSendBufSize)
	{
		tsei.m_EventId = TSE_SEND;
		tsei.m_sData.erase();
		SetNotifyEvent(tsei);
	}
}

void TcpServer::CleanUpIoRequest(TcpServerOV* pState, TcpServerCK ck)
{
	//DB_ENTER(TcpServer::CleanUpIoRequest);
	Critical_Section cs(*m_pMutex);

	ConnectionMapIter iter;
	if (!m_pConnectionMap->Find(ck->m_uAddr, ck->m_uPort, iter))
		return;

	Connection& con = iter->second;
	//DB_STAT(intoa(con.GetAddr()).c_str());
	//DB_STAT(itoa(ntohs(con.GetPort())).c_str());

	// Notify owner of disconnection
	TcpServerEventInfo tsei;
	tsei.m_uAddr = con.GetAddr();
	tsei.m_uPort = con.GetPort();
	tsei.m_EventId = TSE_DISCONNECT;
	SetNotifyEvent(tsei);
	
	pState = NULL;

	con.CleanUp();
	m_pConnectionMap->erase(iter);
}

void TcpServer::FinalCleanUp(void)
{
	//DB_ENTER(TcpServer::FinalCleanUp);
	Critical_Section cs(*m_pMutex);

	ConnectionMapIter iter = m_pConnectionMap->begin();
	for (; iter != m_pConnectionMap->end(); iter++)
	{
		Connection& con = iter->second;
		con.CleanUp();
	}
}

// used to notify the derived class of internal errors
void TcpServer::OnIocpError(iocp_error error, DWORD dwWin32Err)
{
	DB_ENTER(TcpServer::OnIocpError);
	Critical_Section cs(*m_pMutex);
	DB_STAT(dwWin32Err);
}

// notification of a new thread created
// runs on the context of the new thread
void TcpServer::OnThreadEntry(void)
{
	//DB_ENTER(TcpServer::OnThreadEntry);
	//Critical_Section cs(*m_pMutex);
	//DB_STAT(GetCurrentThreadId());
}

// notification just before a thread gets destroyed
void TcpServer::OnThreadExit(void)
{
	//DB_ENTER(TcpServer::OnThreadExit);
	//Critical_Section cs(*m_pMutex);
	//DB_STAT(GetCurrentThreadId());
}

// get a termination message
void TcpServer::GetTerminationMsg(TcpServerCK& ck)
{
	//DB_ENTER(TcpServer::GetTerminationMsg);
	//Critical_Section cs(*m_pMutex);
	ck = NULL;
}

// is this a termination message
bool TcpServer::IsTerminationMsg(TcpServerCK ck)
{
	//DB_ENTER(TcpServer::IsTerminationMsg);
	//Critical_Section cs(*m_pMutex);
	return (NULL == ck);
}


// ---------------------------------------------------------------------------
// Protected methods
// ---------------------------------------------------------------------------

void TcpServer::SetNotifyEvent(TcpServerEventInfo& tsei)
{
	m_pNotifyEvent->SetEvent(tsei);
	
	// allow the other thread to handle the event
	Sleep(0);
}


// ---------------------------------------------------------------------------
// Private methods
// ---------------------------------------------------------------------------

void TcpServer::InternalThreadImpl(void)
{
	// Initialize the IOCompletion port
#ifdef DISABLE_IOCP
	// simulate IOCP failure
	BOOL bRet = FALSE;

#pragma _ebWarning("debug build option - IOCP disabled")

#else
	BOOL bRet = Iocp::CreateIocp(
		IOCP_CONCURRENT_THREADS, 
		IOCP_INITIAL_MAX_THREADS,
		IOCP_TIMEOUT);
#endif

	if (!bRet)
	{
		DB("Failed to initialize IO completion port.");

		// Revert to Windows 9x compatibility mode.
		m_bIocpEnabled = false;
	}

	UpdateEventArray();

	DWORD dwWait;  // holds return value of the wait
    bool bStop = false;
	while (!bStop)
	{
		if (!m_bIocpEnabled)
		{
			UpdateEventArray();
		}

        dwWait = WaitForMultipleObjects(
			m_iNumEvents,
			m_aEvents, 
			FALSE,
			INFINITE);

		switch (dwWait)
		{
		case WAIT_TIMEOUT:
			break;

		case WAIT_OBJECT_0:  // inter-thread event
			HandleThreadEvent(bStop);
			break;

		case WAIT_OBJECT_0 + 1:  // accept event
			HandleAcceptEvent();
			break;

		default:
			if ((dwWait >= WAIT_OBJECT_0 + 2) && (dwWait < WAIT_OBJECT_0 + 64))
			{
				int iIndex = dwWait - WAIT_OBJECT_0;
				HANDLE hEvent = m_aEvents[iIndex];
				HandleSocketEvent(hEvent);
			}
			else
			{
				DB_STAT(WSAGetLastError());
				DB_ASSERT(0 && "Unexpected wait result");
				bStop = true;
			}
			break;
		} // switch
    } // while

	if (m_bIocpEnabled)
	{
		WARN(Iocp::Stop());
	}
	else
	{
		FinalCleanUp();
	}
}

void TcpServer::HandleAcceptEvent(void)
{
	DB_ENTER(TcpServer::HandleAcceptEvent);

	Critical_Section cs(*m_pMutex);

	if (INVALID_SOCKET == m_ListenSock)
		return;

	sockaddr_in sinFrom;
	socklen_t lenSinFrom = sizeof(sinFrom);

	// wait for connect request
	SOCKET fd = ::accept(
		m_ListenSock, 
		(sockaddr*)&sinFrom, 
		(socklen_t*)&lenSinFrom);			

	if (fd == INVALID_SOCKET)
	{
		string sErrMessage = "TCP server: accept failure, error " + itoa(WSAGetLastError()) + ".";
		MSG_BOX(sErrMessage);
		return;
	}

	u_int32_t uAddr = sinFrom.sin_addr.s_addr;
	u_int16_t uPort = sinFrom.sin_port;

	DB_STAT(intoa(uAddr).c_str());
	DB_STAT(ntohs(uPort));

	// a new connection has been established by this point
	Connection con(m_bIocpEnabled);
	con.SetSocket(fd);
	con.SetAddr(uAddr);
	con.SetPort(uPort);

	if (m_bIocpEnabled)
	{
		// Set socket to non-blocking
		Network::Nonblock(fd);

		// Attach connection to I/O completion port
		TcpServerCK ck = con.m_ck;
		DB_ASSERT(ck->hEvent == NULL);
		DB_ASSERT(ck->m_uAddr == uAddr);
		DB_ASSERT(ck->m_uPort == uPort);
		BOOL bRet = Iocp::AttachDevice(fd, ck);
		DB_ASSERT(bRet);

		// Start receiving
		con.m_pNetwork->OverlappedReceive();
	}
	else
	{
		HANDLE hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
		con.SetEventHandle(hEvent);
	}

	m_pConnectionMap->Insert(uAddr, uPort, con);

	// Notify owner of new connection
	TcpServerEventInfo tsei;
	tsei.m_EventId = TSE_CONNECT;
	tsei.m_uAddr = uAddr;
	tsei.m_uPort = uPort;
	SetNotifyEvent(tsei);
}

void TcpServer::HandleSocketEvent(HANDLE hEvent)
{
	DB_ENTER(TcpServer::HandleSocketEvent);

	Critical_Section cs(*m_pMutex);

	if (m_bIocpEnabled)
	{
		DB_ASSERT(0 && "Unexpected case");
		return;
	}

	ConnectionMapIter iter;
	if (!m_pConnectionMap->FindEvent(hEvent, iter))
		return;

	Connection& con = iter->second;
	DB("Remote: " << intoa(con.GetAddr()).c_str() << ":" 
		<< itoa(ntohs(con.GetPort())).c_str());

	int iInitialSendBufSize = con.m_pNetwork->GetSendBufferSize();

	string sBuf;
	int iRet = con.RecvMsg(sBuf);

	TcpServerEventInfo tsei;
	tsei.m_uAddr = con.GetAddr();
	tsei.m_uPort = con.GetPort();
	
	if (con.m_pNetwork->GetSendBufferSize() < iInitialSendBufSize)
	{
		tsei.m_EventId = TSE_SEND;
		tsei.m_sData.erase();
		SetNotifyEvent(tsei);
	}
	
	if (ES_SUCCESS == iRet)
	{
		if (sBuf.empty())
			return;

		// Notify owner of new data received
		tsei.m_EventId = TSE_RECV;
		tsei.m_sData = sBuf;
	}
	else if (ES_NOT_CONNECTED == iRet)
	{
		// clean up socket and other data structures.
		con.CleanUp();
		m_pConnectionMap->erase(iter);

		// Notify owner of disconnection
		tsei.m_EventId = TSE_DISCONNECT;
	}
	else
	{
		DB_STAT(iRet);
		return;
	}

	SetNotifyEvent(tsei);
}

void TcpServer::HandleThreadEvent(bool& bStop)
{
	DB_ENTER(TcpServer::HandleThreadEvent);

	u_int16_t uEvent = 0;
	if (!m_pThreadEvent->GetVar(uEvent))
		return;

	bool bRet = false;

	switch (uEvent)
	{
	case TSTE_INTERNAL_OPEN:
		bRet = InternalOpen();
		break;

	case TSTE_INTERNAL_CLOSE:
		bRet = InternalClose();
		break;

	case TSTE_INTERNAL_QUIT:
		bStop = true;
		return;

	default:
		DB_ASSERT(0);
		return;
	}

	m_bResponseValue = bRet;
	m_dwLastError = GetLastError();
	SetEvent(m_hResponseEvent);
}

bool TcpServer::InternalOpen(void)
{
	DB_ENTER(TcpServer::InternalOpen);

	Critical_Section cs(*m_pMutex);

	SOCKET fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (fd == INVALID_SOCKET)
	{
//p		DB(_strerror("socket"));
		return false;
	}

	// Set socket buffer size
	TcpNetwork::SetSocketBufferSize(fd);

	// Set socket to non-blocking
	Network::Nonblock(fd);

	// Ensure we can open this port again if the program is restarted
	Network::Reuse(fd);

	// bind the socket to listen_port
	struct sockaddr_in sin;
	memset((char *)&sin, 0, sizeof(sin));
	sin.sin_family = AF_INET;
	sin.sin_port = m_uListenPort;
	sin.sin_addr.s_addr = INADDR_ANY;

	if (::bind(fd, (struct sockaddr *)&sin, sizeof(sin)) != 0)
	{
		DB("bind error: " << WSAGetLastError());
		CLOSE_SOCKET(fd);

		string sErrMessage = "TCP server: bind failure, port " + 
			itoa(ntohs(m_uListenPort)) + ".";
		MSG_BOX(sErrMessage);

		return false;
	}	

	if (::listen(fd, SOMAXCONN) != 0)
	{
		int iErrNo = WSAGetLastError();

//p		DB(_strerror("listen"));
		CLOSE_SOCKET(fd);
		MSG_BOX("Warning: failed to create a TCP server.  Error code = " + itoa(iErrNo));
		return false;
	}

	if (WSAEventSelect(fd, m_hAcceptEvent, FD_ACCEPT) != 0)
	{
		DB("WSAEventSelect m_hAcceptEvent failed");
		CLOSE_SOCKET(fd);
		return false;
	}

	m_ListenSock = fd;
	
	return true;
}

bool TcpServer::InternalClose(void)
{
	DB_ENTER(TcpServer::InternalClose);

	Critical_Section cs(*m_pMutex);

	WARN(closesocket(m_ListenSock) == 0);
	m_ListenSock = INVALID_SOCKET;

	// Close any remaining connections
	if (NULL == m_pConnectionMap.get())
	{
		DB_ASSERT(0 && "connection map is null");
		return false;
	}

	ConnectionMapIter iter = m_pConnectionMap->begin();
	for (; iter != m_pConnectionMap->end(); iter++)
	{
		DB("TcpServer - closing connection");
		Connection& con = iter->second;
		con.m_pNetwork->Close();
	}
	
	return true;
}

void TcpServer::UpdateEventArray(void)
{
	DB_ENTER(TcpServer::UpdateEventArray);

	Critical_Section cs(*m_pMutex);

	m_aEvents[0] = m_pThreadEvent->GetHandle();
	m_aEvents[1] = m_hAcceptEvent;
	
	m_iNumEvents = 2;

	// Add connection sockets.
	ConnectionMapIter iter = m_pConnectionMap->begin();
	for (; iter != m_pConnectionMap->end(); iter++)
	{
		Connection& con = iter->second;
		m_aEvents[m_iNumEvents++] = con.m_pEvent->GetHandle();

		if (m_iNumEvents >= 64)
		{
			DB("Max events reached (64)");
			return;
		}
	}

	//DB_STAT(m_iNumEvents);
}
