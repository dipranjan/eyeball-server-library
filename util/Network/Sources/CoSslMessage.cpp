// ---------------------------------------------------------------------------
// File:       CoSslMessage.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    SSL message class
// ---------------------------------------------------------------------------

#include "Base.h"  // automatic use of precompiled headers 
#include "critsec.h"
#if 0 // for linux yamada
#include "AtlBase.h"
#endif

#include "CoSslMessage.h"
#include "EComUtil.h"
#include "inet.h"
#include "Message.h"
#include "ReturnCodes.h"
#include "Serializable.h"
#include "ENetwork.h"

// SslNetwork COM object
#include <initguid.h>
#include "SslNetwork.h"
#include "SslNetwork_i.c"

typedef CComQIPtr<ICoSslSocket> CoSslSocketPtr;


// ---------------------------------------------------------------------------
// Helper class
// ---------------------------------------------------------------------------

// Prevent users of CoSslMessage from having to include the relevant
// COM header files, GUIDs, etc.
class CoSslMessageInternal
{
public:
	std::string m_sMsgBuf;
	CoSslSocketPtr m_spSocket;
	DWORD m_dwThreadId;

	void CheckThread(void)
	{
		if (m_dwThreadId != GetCurrentThreadId())
		{
			//DB("Warning: incorrect thread used to access object.");
			//DB_STAT(m_dwThreadId);
			//DB_STAT(GetCurrentThreadId());
		}
	}
};


// ---------------------------------------------------------------------------
// Class implementation
// ---------------------------------------------------------------------------

CoSslMessage::CoSslMessage()
:
	m_spInternal(new CoSslMessageInternal),
	m_pMutex(new Mutex)
{
}

CoSslMessage::~CoSslMessage()
{
	if (NULL != m_spInternal->m_spSocket.p)
	{
		DB("Warning: object not closed");
	}
	m_pMutex.reset();
}

bool CoSslMessage::Open(
	u_int32_t uRemoteAddr,
	u_int16_t uRemotePort,
	u_int16_t uLocalPort,
	HANDLE hNotifyEvent)
{
	Critical_Section cs(*m_pMutex);

	HRESULT hr = 0;
	if (NULL == m_spInternal->m_spSocket.p)
	{
		m_spInternal->m_dwThreadId = GetCurrentThreadId();

		hr = m_spInternal->m_spSocket.CoCreateInstance(__uuidof(CoSslSocket));
		if (FAILED(hr))
			return false;

		if (NULL == m_spInternal->m_spSocket.p)
			return false;
	}

	m_spInternal->CheckThread();

	hr = m_spInternal->m_spSocket->Open(
		uRemoteAddr, 
		uRemotePort, 
		uLocalPort,
		(unsigned long)hNotifyEvent);
	if (FAILED(hr))
		return false;

	return true;
}

void CoSslMessage::Close()
{
	Critical_Section cs(*m_pMutex);
	
	m_spInternal->CheckThread();

	if (NULL == m_spInternal->m_spSocket.p)
		return;

	HRESULT hr = m_spInternal->m_spSocket->Close();
	DB_ASSERT(SUCCEEDED(hr));

	m_spInternal->m_spSocket.Release();
	DB_ASSERT(NULL == m_spInternal->m_spSocket.p);
}

int CoSslMessage::SendMsg(const ISerializable& msg)
{
	Critical_Section cs(*m_pMutex);
	m_spInternal->CheckThread();

	std::string sBuf;
	msg.serialize(sBuf);

	return SendMsg(sBuf);
}

int CoSslMessage::SendMsg(const std::string& sBuf)
{
	Critical_Section cs(*m_pMutex);
	m_spInternal->CheckThread();

	return SendMsg(sBuf.data(), sBuf.size());
}

int CoSslMessage::SendMsg(const char* pBuf, int iLen)
{
	Critical_Section cs(*m_pMutex);
	m_spInternal->CheckThread();

	if (NULL == m_spInternal->m_spSocket.p)
		return ES_FAILURE;

	ByteArray ba;
	ba.SetData(pBuf, iLen);

	HRESULT hr = m_spInternal->m_spSocket->Send(ba.GetSafeArrayPtr());
	if (FAILED(hr))
		return ES_NOT_CONNECTED;

	return ES_SUCCESS;
}

int CoSslMessage::RecvMsg(std::string& _sBuf)
{
	Critical_Section cs(*m_pMutex);
	m_spInternal->CheckThread();

	std::string& sMsgBuf = m_spInternal->m_sMsgBuf;
	
	// Access the message header
	CommandMessage msgHdr;
	int iRet = GetHeader(msgHdr);
	if (iRet != ES_SUCCESS)
		return iRet; // message header is not yet available
	
	// Allocate memory for the payload
	u_int16_t uPayloadLen = msgHdr.m_u16PayloadLength;
	u_int16_t cbTotal = CM_LENGTH + uPayloadLen;
	
	if (uPayloadLen > 0)
	{
		// Perform TCP receive
		int len = cbTotal - sMsgBuf.size();  // requested length
		DB_ASSERT(len > 0);
		
		std::string sBuf;
		int iRet = DoRecv(sBuf, len);
		if (iRet < 0)
			return iRet;
		
		sMsgBuf.append(sBuf.data(), iRet); // add bytes received
		
		if (sMsgBuf.size() < cbTotal)
		{
			return ES_FAILURE; // payload not yet available
		}
	}
	
	DB_ASSERT(sMsgBuf.size() == cbTotal);
	
	// Success
	_sBuf = sMsgBuf;
	
	// Clean-up
	sMsgBuf.erase();
	
	return ES_SUCCESS;
}

void CoSslMessage::SetGlobalLocalAddr(void)
{
	Critical_Section cs(*m_pMutex);
	m_spInternal->CheckThread();

	// If there is a connected socket, use it to set the local address
	sockaddr_in local;
	memset(&local, 0, sizeof(local));

	local.sin_family = AF_INET;
	int len = sizeof(local);

	long lVal = -1;
	if (NULL == m_spInternal->m_spSocket.p)
		return;
	
	HRESULT hr = m_spInternal->m_spSocket->GetSocket(&lVal);
	if (FAILED(hr))
		return;

	SOCKET fdSocket = (SOCKET)lVal;
	if (fdSocket == INVALID_SOCKET)
		return;

	if (getsockname(fdSocket, (struct sockaddr *)(&local), (socklen_t *)&len) != 0)
	{
		DB_ASSERT(0 && "getsockname failed"); 
		return;
	}

	SetLocalAddr(local.sin_addr.s_addr);
}




bool CoSslMessage::SetSendBufferSize(int iSize)
{
	Critical_Section cs(*m_pMutex);
	m_spInternal->CheckThread();

	if (NULL == m_spInternal->m_spSocket.p)
		return false;

	SOCKET s;
	m_spInternal->m_spSocket->GetSocket((long*)&s);

	return Network::SetSendBufferSize(s, iSize);
}

bool CoSslMessage::IsSocketWritable(int iTimeout)
{
	Critical_Section cs(*m_pMutex);
	m_spInternal->CheckThread();

	if (NULL == m_spInternal->m_spSocket.p)
		return true;

	SOCKET s;
	m_spInternal->m_spSocket->GetSocket((long*)&s);

	fd_set set;
	FD_ZERO(&set);
	FD_SET(s, &set);

	// waiting for specified seconds
	timeval tv;
	tv.tv_sec = iTimeout;
	tv.tv_usec = 0;

	bool b =  select(0, NULL, &set, NULL, &tv) == 1; 

	FD_CLR(s, &set);

	return b;
}


// Private methods
int CoSslMessage::GetHeader(CommandMessage& msgHdr)
{
	m_spInternal->CheckThread();

	std::string& sMsgBuf = m_spInternal->m_sMsgBuf;

	// Return message header, if it is buffered
	if (sMsgBuf.size() >= CM_LENGTH)
	{
		msgHdr.deserialize(sMsgBuf);
		return ES_SUCCESS;
	}
	
	int len = CM_LENGTH - sMsgBuf.size();
	DB_ASSERT(len > 0);
	
	std::string sBuf;
	int iRet = DoRecv(sBuf, len);
	if (iRet < 0)
		return iRet;

	sMsgBuf.append(sBuf.data(), iRet); // add bytes received
	
	if (sMsgBuf.size() >= CM_LENGTH)
	{
		msgHdr.deserialize(sMsgBuf);
		return ES_SUCCESS;
	}
	
	return ES_FAILURE;
}

int CoSslMessage::DoRecv(std::string& sBuf, int iLen)
{
	m_spInternal->CheckThread();

	if (NULL == m_spInternal->m_spSocket.p)
		return ES_FAILURE;

	SAFEARRAY* psa = NULL;
	HRESULT hr = m_spInternal->m_spSocket->Recv(iLen, &psa);
	if (FAILED(hr))
		return ES_NOT_CONNECTED;

	ByteArray ba;
	ba.Attach(psa);
	
	ba.GetData(sBuf);
	DB_ASSERT((int)sBuf.size() <= iLen);

	return sBuf.size();
}
