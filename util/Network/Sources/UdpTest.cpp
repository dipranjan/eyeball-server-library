// File: UdpTest.cpp

#include "Base.h"  // automatic use of precompiled headers 
#include "registry.h"
#include "UdpTest.h"

#define DISABLED_TTL_VALUE (-1)
#define LOW_TTL_VALUE (2)
#define HIGH_TTL_VALUE (32)
#define MAX_WAIT_TIMEOUT (20000)


//----------------------------------------------------------------------------
// UdpTest Class 
//----------------------------------------------------------------------------

UdpTest::UdpTest()
:
	m_hNetEvent(NULL),
	m_hNotifyEvent(NULL),
	m_addrEcho(0, 0),
	m_pUdpNetwork(),
	m_bStop(false),
	m_nDetectedPort1(0),
	m_nDetectedPort2(0),
	m_dwResult(RESULT_UNKNOWN)
{
}

UdpTest::~UdpTest()
{
	StopTest();
}

bool UdpTest::StartTest(AddressPortPair addrEcho, HANDLE hNotifyEvent)
{
	StopTest();  // automatically stop previous tests

	m_addrEcho = addrEcho;
	m_hNotifyEvent = hNotifyEvent;

	m_hNetEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	m_pUdpNetwork.reset(new UdpNetwork);

	StartInternalThread();

	return true;
}

void UdpTest::StopTest(void)
{
	StopInternalThread();

	m_pUdpNetwork.reset();
	
#if 1 //for Linux yamada
	CLOSE_EVENT(m_hNetEvent);
#else
	CLOSE_HANDLE(m_hNetEvent);
#endif

	m_hNotifyEvent = NULL;
}

DWORD UdpTest::GetResult(void)
{
	return m_dwResult;
}


//----------------------------------------------------------------------------
// Protected Methods
//----------------------------------------------------------------------------

void UdpTest::InternalThreadImpl(void)
{
	m_dwResult = RESULT_UNKNOWN;

	HANDLE events[2];
	events[0] = m_pThreadEvent->GetHandle();
	events[1] = m_hNetEvent;

	if (!m_pUdpNetwork->Open(0, 0, 0, m_hNetEvent))
	{
		DB_ASSERT(0);
		return;
	}

	AddressPortPair addr = m_addrEcho;

	AddressEchoMessage msg;
	std::string sBuf;
	msg.serialize(sBuf);

	SharedPtr<ESocket> pSocket;
	if (!m_pUdpNetwork->GetSharedSocket(pSocket))
		return;

	pSocket->SetTtl(HIGH_TTL_VALUE);
	int iRet = m_pUdpNetwork->SendTo(sBuf, addr);
	if (iRet < (int)sBuf.size())
		return;

#if 1 //for Linux yamada
	usleep(50*1000);  // UDP to firewall delay
#else
	Sleep(50);  // UDP to firewall delay
#endif //for Linux yamada

	// increment port number
	addr.second = htons(ntohs(addr.second) + 1);

	pSocket->SetTtl(LOW_TTL_VALUE);
	iRet = m_pUdpNetwork->SendTo(sBuf, addr);
	if (iRet < (int)sBuf.size())
		return;

#if 1 //for Linux yamada
	usleep(50*1000);  // UDP to firewall delay
#else
	Sleep(50);  // UDP to firewall delay
#endif //for Linux yamada

	// increment port number
	addr.second = htons(ntohs(addr.second) + 1);

	pSocket->SetTtl(HIGH_TTL_VALUE);
	iRet = m_pUdpNetwork->SendTo(sBuf, addr);
	if (iRet < (int)sBuf.size())
		return;

	m_bStop = false;
	while (!m_bStop)
	{
		DWORD dwWait = WaitForMultipleObjects(
			sizeof(events) / sizeof(events[0]), 
			events, 
			FALSE, 
			MAX_WAIT_TIMEOUT);

		switch (dwWait)
		{
		case WAIT_OBJECT_0:  // m_pThreadEvent->GetHandle()
			{
				u_int16_t uEvent = 0;
				if (!m_pThreadEvent->GetVar(uEvent))
					continue;

				if (TSTE_INTERNAL_QUIT == uEvent)
				{
					// exit thread
					m_bStop = true;
				}
			}
			break;

		case WAIT_OBJECT_0 + 1:  // m_hNetEvent
			HandleNetEvent();
			break;

		case WAIT_TIMEOUT:
			DB("UdpTest - timed out");
			SetResult(RESULT_FAIL);
			break;

		default:
			DB_ASSERT(0 && "unexpected wait result");

			// handle error by stopping test
			SetResult(RESULT_FAIL);
			break;
		}
	}
}

void UdpTest::HandleNetEvent(void)
{
	std::string sBuf;
	sBuf.resize(256);

	u_int32_t uFromAddr;
	u_int16_t uFromPort;

	int iLen = m_pUdpNetwork->RecvFrom(
		&sBuf[0],
		sBuf.size(),
		uFromAddr,
		uFromPort);
	if (iLen < 0)
		return;

	if (uFromAddr != m_addrEcho.first)
		return;

	AddressEchoMessage msg;
	msg.deserialize(sBuf);

	int nIndex = (ntohs(uFromPort) - ntohs(m_addrEcho.second));
	switch (nIndex)
	{
	case 0:
		m_nDetectedPort1 = msg.m_u16IpPort;
		break;

	case 1:
		// very close to the echo server
		DB("Echo response received for low-ttl packet.");
		SetResult(RESULT_OK);
		return;

	case 2:
		m_nDetectedPort2 = msg.m_u16IpPort;
		break;

	default:
		DB_ASSERT(0);
		return;
	}

	if ((0 != m_nDetectedPort1) && (0 != m_nDetectedPort2))
	{
		DWORD dwTtl = LOW_TTL_VALUE;

		int nDiff = m_nDetectedPort2 - m_nDetectedPort1;
		switch (nDiff)
		{
		case 0:
			DB("Merged ports detected");
			break;

		case 1:
			DB("PAT++ detected");
			break;

		case 2:
			DB("PAT+2 detected");
			dwTtl = DISABLED_TTL_VALUE;
			break;

		default:
			return;
		}

		// set the registry key
		EcSetRegistryEntry(REG_FIREWALL_TTL, dwTtl);
		DB("Firewall TTL registry set to " << dwTtl);

		// test complete
		SetResult(RESULT_OK);
		return;
	}
}

void UdpTest::SetResult(DWORD dwResult)
{
	m_dwResult = dwResult;
	SetEvent(m_hNotifyEvent);
}
