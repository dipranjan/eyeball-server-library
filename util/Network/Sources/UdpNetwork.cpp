// ---------------------------------------------------------------------------
// File:       UdpNetwork.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    
// 
// Change Log:
// ---------------------------------------------------------------------------

//#include "../../Base/Headers/Base.h"  // automatic use of precompiled headers 
#include "Base.h"  // automatic use of precompiled 
#if 0 //for Linux yamada
#ifndef _WIN32_WCE
#include <ws2tcpip.h>		// Needed for IP_ADD_MEMBERSHIP define
#include <io.h>
#endif
#endif //for Linux yamada

#include "debug.h"
#include "ESocket.h"
#include "ReturnCodes.h"
#include "pktbuf.h"
#include "TextUtil.h"
#include "UdpNetwork.h"
#include "EUtil.h"
#include <errno.h>
#include <cstdio>
#if 0 //for Linux yamada
#include "DebugLog.h"
#endif //for Linux yamada

using std::string;


// ---------------------------------------------------------------------------
// Constants and Defines
// ---------------------------------------------------------------------------

#define UDP_TTL 5


// ---------------------------------------------------------------------------
// UdpNetwork class definition
// ---------------------------------------------------------------------------

UdpNetwork::UdpNetwork()
:
	m_pSocket(NULL),
	m_hNotifyEvent(NULL),
	m_uRemoteAddr(0),
	m_uRemotePort(0),
	m_uLocalPort(0),
	m_uDetectedPort(0),
	m_bPortConfirmed(false)
{
}

UdpNetwork::~UdpNetwork()
{
	Close();
}

bool lt_UdpNetwork::operator()(const UdpNetwork& s1, const UdpNetwork& s2) const
{
	LT_CHECK(s1.m_pSocket.get(), s2.m_pSocket.get());
	LT_CHECK(s1.m_hNotifyEvent, s2.m_hNotifyEvent);

	return false;
}

bool UdpNetwork::Open(
	u_int32_t uRemoteAddr, 
	u_int16_t uRemotePort,
	u_int16_t uLocalPort,
	HANDLE hNotifyEvent)
{
	m_uRemoteAddr = uRemoteAddr;
	m_hNotifyEvent = hNotifyEvent;
	m_bPortConfirmed = false;
	m_uDetectedPort = 0;

	//DB("UdpNetwork::open");

	// Open the receiver, if required
	if ((GetSocket() == INVALID_SOCKET) || (uLocalPort != 0))
	{
		m_uLocalPort = uLocalPort;
		OpenReceiver();
		if (GetSocket() == INVALID_SOCKET)
		{
			return false;
		}

	}

	if (uRemotePort != 0)
	{
		m_uRemotePort = uRemotePort;
		OpenSender();
		if (GetSocket() == INVALID_SOCKET)
		{
			return false;
		}

		/*
		string sLocal = m_pSocket->ToString();
		string sRemote = Network::ToString(AddressPortPair(m_uRemoteAddr, m_uRemotePort));
		ERR_MSG("Opening sender from " + sLocal + " to " + sRemote);
		*/
	}

	if (m_hNotifyEvent != NULL)
	{
		SOCKET sock = m_pSocket->GetSocket();
		if (WSAEventSelect(sock, m_hNotifyEvent, FD_READ | FD_CLOSE) != 0)
		{
			Close();
			return false;
		}
	}
	
	return true;
}

void UdpNetwork::Close(void)
{
	// closing the socket will cancel the association and selection of network
	// events automatically. 

	if (m_hNotifyEvent != NULL)
	{
		SOCKET sock = GetSocket();
		WSAEventSelect(sock, m_hNotifyEvent, 0);
		m_hNotifyEvent = NULL;
	}

	m_uDetectedPort = 0;
	m_bPortConfirmed = false;

	// Shared sockets are closed automatically
	m_pSocket.reset();
}

int UdpNetwork::SendTo(const char* buf, int len, u_int32_t addr, u_int16_t port)
{
	if (NULL == m_pSocket.get())
	{
		DB_ASSERT(0);
		return 0;
	}

	return m_pSocket->SendTo(buf, len, addr, port);
}

int UdpNetwork::SendTo(const string& sBuf, AddressPortPair addr)
{
	if (NULL == m_pSocket.get())
	{
		DB_ASSERT(0);
		return 0;
	}

	return m_pSocket->SendTo(sBuf, addr);
}

int UdpNetwork::Send(const char *pBuf, int iLen)
{
	if (!m_pSocket.get())
		return ES_FAILURE;

	
	u_int16_t uPort = GetDetectedPort();

	m_pSocket->SendTo(pBuf, iLen, m_uRemoteAddr, uPort);

	return ES_SUCCESS;
}

int UdpNetwork::Send(const pktbuf *pb)
{
	return UdpNetwork::Send((char *)pb->dp, pb->len);
}

int UdpNetwork::Recv(char* pBuf, int iLen)
{
	return m_pSocket->Recv(pBuf, iLen);
}

int UdpNetwork::RecvFrom(char *buf, int len, u_int32_t& from, u_int16_t& fromport)
{
	RZIN(m_pSocket.get());

	return m_pSocket->RecvFrom(buf, len, from, fromport);
}

SOCKET UdpNetwork::GetSocket(void)
{
	if (!m_pSocket.get())
		return INVALID_SOCKET;

	return m_pSocket->GetSocket();
}

void UdpNetwork::SetSocket(SOCKET sock)
{
	if (NULL == m_pSocket.get())
	{
		DB_ASSERT(0);
		return;
	}

	m_bPortConfirmed = false;

	m_pSocket->AttachSocket(sock);
}

bool UdpNetwork::CheckRemotePort(u_int32_t uAddr, u_int16_t uPort)
{
	if (0 == m_uRemoteAddr)
		return false;

	if (uAddr != m_uRemoteAddr)
		return false;

	if ((0 == uPort) || (0 == m_uRemotePort))
		return false;

	if (uPort == m_uRemotePort)
	{
		// target port confirmed
		m_bPortConfirmed = true;
		m_uDetectedPort = 0;
		return true;
	}

	if (m_bPortConfirmed)
		return false;

	if (uPort != m_uRemotePort)
	{
		// automatically change remote port for PAT firewall compatibility
		DB("Automatically changing port: " << 
			ntohs(m_uRemotePort) << "->" << ntohs(uPort));

		DB_ASSERT(0 && "UdpNetwork: remote port is not correctly detected.");
#if 0 //for Linux yamada
		bool bLogEnabled = GetLogEnabled(LOG_FIREWALL);
		if (bLogEnabled)
		{
			char sz[100];
			sprintf(sz, "UdpNetwork: Remote PAT port is translated from %d to %d", ntohs(m_uRemotePort), ntohs(uPort));
			MSG_BOX(sz);
		}
#endif //for Linux yamada
		m_uDetectedPort = uPort;
	}

	m_bPortConfirmed = true;

	return true;
}


// ---------------------------------------------------------------------------
// Private methods
// ---------------------------------------------------------------------------

void UdpNetwork::OpenReceiver(void)
{
	// Check if a shared socket is available
	if (m_pSocket.get() != NULL)
	{
		// Check if the socket is bound to the correct port
		u_int16_t uBoundPort = m_pSocket->GetLocalPort();
		if (uBoundPort != m_uLocalPort)
		{
			//DB("Warning: ports do not match - this may be due to PAT");
			//cout<<"Warning: ports do not match - this may be due to PAT";
		}

		SetBufferSize();
		return;
	}

	// Create new socket
	m_pSocket = SharedPtr<ESocket>(new ESocket(AF_INET, SOCK_DGRAM, 0));


#ifdef IP_ADD_MEMBERSHIP
	if (IN_CLASSD(ntohl(m_uRemoteAddr)))
	{
		// Try to bind the multicast address as the socket
		// dest address.  On many systems this won't work
		// so fall back to a destination of INADDR_ANY if
		// the first bind fails.
		if (m_pSocket->Bind(m_uRemoteAddr, m_uRemotePort) != ES_SUCCESS)
		{
			if (m_pSocket->Bind(INADDR_ANY, m_uRemotePort) != ES_SUCCESS)
			{
				//MSG_BOX("UDP receiver: bind failure, port "); // + itoa(m_uRemotePort) + ".");
				//The previous line was commented by Farhan
				perror("UDP receiver: bind failure, port ");
				return;
			}
		}

		// XXX This is bogus multicast setup that really
		// shouldn't have to be done (group membership should be
		// implicit in the IP class D address, route should contain
		// ttl & no loopback flag, etc.).  Steve Deering has promised
		// to fix this for the 4.4bsd release.  We're all waiting
		// with bated breath.
		struct ip_mreq mr;

		mr.imr_multiaddr.s_addr = m_uRemotePort;
		mr.imr_interface.s_addr = INADDR_ANY;
		if (setsockopt(m_pSocket->GetSocket(), IPPROTO_IP, IP_ADD_MEMBERSHIP,
			(char *)&mr, sizeof(mr)) != 0)
		{
//p			DB(_strerror("IP_ADD_MEMBERSHIP"));
			return;
		}
	} 
	else
#endif
	{
		if (m_pSocket->Bind(INADDR_ANY, m_uLocalPort) != ES_SUCCESS)
		{
			int iError = WSAGetLastError();
#ifndef _WIN32_WCE
			string sErrMsg = "Network Error: UDP receiver bind failed.";
			sErrMsg += " Port: " + itoa(m_uLocalPort);
			sErrMsg += " (Error code: " + itoa(iError) + ").";
			MSG_BOX(sErrMsg);
#endif

			DB_ASSERT(0 && "Receiver bind failed");

			return;
		}
	}

	SetBufferSize();
}

void UdpNetwork::OpenSender(void)
{
	if (m_pSocket.get() == NULL)
	{
		// Create new socket
		m_pSocket = SharedPtr<ESocket>(new ESocket(AF_INET, SOCK_DGRAM, 0));

		if (BindSendSocket() != ES_SUCCESS)
			return;
	}

	if (m_pSocket->SetRemote(m_uRemoteAddr, m_uRemotePort) != ES_SUCCESS)
		return;

	if (IN_CLASSD(ntohl(m_uRemoteAddr)))
	{
#ifdef IP_ADD_MEMBERSHIP
		char c;

		// Turn off loopback by default.
		// Note that some network stacks don't allow
		// loopback to be disabled, but that's okay
		// because looped-back packets are filtered out
		// on the recv path anyway.
		c = 0;
		// SK turns it on for testing
		//c = 1;
		SOCKET fd = m_pSocket->GetSocket();
		(void)setsockopt(fd, IPPROTO_IP, IP_MULTICAST_LOOP, &c, 1);

		// set the multicast TTL
#ifdef WIN32
		u_int t;
#else
		u_char t;
#endif
		t = UDP_TTL;
		if (setsockopt(fd, IPPROTO_IP, IP_MULTICAST_TTL,
			(char*)&t, sizeof(t)) < 0)
		{
//p			DB(_strerror("IP_MULTICAST_TTL"));
			//exit(1);
			ASSERT(0);
		}
#else
		MSG_BOX("UDP sender: Not compiled with support for IP multicast.");

		//exit(1);
		ASSERT(0);
#endif
	}

	SetBufferSize();
}

BOOL UdpNetwork::GetSharedSocket(SharedPtr<ESocket>& pSocket)
{
	pSocket = m_pSocket;

	return (pSocket.get() != NULL);
}

BOOL UdpNetwork::SetSharedSocket(SharedPtr<ESocket>& pSocket)
{
	m_pSocket = pSocket;
	m_bPortConfirmed = false;
	return (m_pSocket.get() != NULL);
}


int UdpNetwork::BindSendSocket(void)
{
	if (m_uLocalPort)
	{
		// socket should already be bound
		return ES_SUCCESS;
	}

	if (m_pSocket->Bind(INADDR_ANY, 0) != ES_SUCCESS)
	{
		int iError = WSAGetLastError();
//		MSG_BOX("System error: an error has occurred trying to send data.\n");
//			"Details: UDP network sender bind failure, error:" + itoa(iError) + ".");
		//The previous two lines was commented by Farhan
		perror("System error: an error has occurred trying to send data.\n");
		return ES_FAILURE;
	}

	return ES_SUCCESS;
}

void UdpNetwork::SetBufferSize(void)
{
	SetRecvBufferSize();
	SetSendBufferSize();
}

void UdpNetwork::SetRecvBufferSize(void)
{
#ifndef _WIN32_WCE
	SOCKET fd = m_pSocket->GetSocket();

	if (!Network::SetRecvBufferSize(fd, 128 * 1024))
	{
		if (!Network::SetRecvBufferSize(fd, 32 * 1024))
		{
			MSG_BOX("Error: failed to set UDP receiver buffer size.");
		}
	}

	DB_ASSERT(Network::GetRecvBufferSize(fd) >= (32 * 1024));
#endif
}

void UdpNetwork::SetSendBufferSize(void)
{
#ifndef _WIN32_WCE
	SOCKET fd = m_pSocket->GetSocket();

	if (!Network::SetSendBufferSize(fd, 128 * 1024))
	{
		if (!Network::SetSendBufferSize(fd, 32 * 1024))
		{
			MSG_BOX("Error: failed to set UDP sender buffer size.");
		}
	}

	DB_ASSERT(Network::GetSendBufferSize(fd) >= (32 * 1024));
#endif
}

u_int16_t UdpNetwork::GetDetectedPort(void) const
{
	return (0 != m_uDetectedPort) ? m_uDetectedPort : m_uRemotePort;
}
