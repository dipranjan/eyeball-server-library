// ---------------------------------------------------------------------------
// File:       Ethernet.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Ethernet helper routines
// 
// Change Log:
// ---------------------------------------------------------------------------

#include "Base.h"  // automatic use of precompiled headers 

#if 1 //for Linux yamada
#include <stdio.h>
#include "Timer.h" //use timeGetTime()
#else
#ifndef _WIN32_WCE
#include <snmp.h>
#endif
#endif //for Linux yamada

#include "Ethernet.h"
#include "inet.h"
#include "ReturnCodes.h"

using std::string;


MACAddress::MACAddress()
:
	address()
{
	memset(address, 0, sizeof(address));
}

string MACAddress::to_str(void) const
{
	char szAddress[13];

	sprintf(
		szAddress,
		"%02x%02x%02x%02x%02x%02x",
		address[0], address[1], address[2], address[3], address[4], address[5]
	);

	return szAddress;
}

bool MACAddress::from_str(const string& _str)
{
	RFUT(_str.size() == 12);

	unsigned int val;

	for (int i=0; i < 6; i++)
	{
		RFUT(sscanf(&_str.c_str()[i*2], "%02x", &val) == 1);
		address[i] = (unsigned char)val;
	}

	return true;
}

bool Ethernet::generate_mac_addr(MACAddress& _MACAddress)
{

#if 1 //for Linux yamada
	srand(timeGetTime());
#else
	srand(GetSystemTime() ^ LookupLocalAddr());
#endif //for Linux

	for (int i = 0; i < sizeof(_MACAddress); i++)
	{
		_MACAddress.address[i] = (unsigned char)(rand());
	}

	return true;
}

#ifndef _WIN32_WCE
bool Ethernet::get_mac_addr(MACAddressList& _lsMACAddress)
{
	typedef BOOL(WINAPI * pSnmpExtensionInit) (
			IN DWORD dwTimeZeroReference,
			OUT HANDLE * hPollForTrapEvent,
			OUT AsnObjectIdentifier * supportedView);

	typedef BOOL(WINAPI * pSnmpExtensionTrap) (
				OUT AsnObjectIdentifier * enterprise,
				OUT AsnInteger * genericTrap,
				OUT AsnInteger * specificTrap,
				OUT AsnTimeticks * timeStamp,
				OUT RFC1157VarBindList * variableBindings);

	typedef BOOL(WINAPI * pSnmpExtensionQuery) (
				IN BYTE requestType,
				IN OUT RFC1157VarBindList * variableBindings,
				OUT AsnInteger * errorStatus,
				OUT AsnInteger * errorIndex);

	typedef BOOL(WINAPI * pSnmpExtensionInitEx) (
				OUT AsnObjectIdentifier * supportedView);

	HINSTANCE m_hInst;
	pSnmpExtensionInit m_Init;
	pSnmpExtensionInitEx m_InitEx;
	pSnmpExtensionQuery m_Query;
	pSnmpExtensionTrap m_Trap;
	HANDLE PollForTrapEvent;
	AsnObjectIdentifier SupportedView;

	UINT OID_ifEntryType[]  = { 1, 3, 6, 1, 2, 1, 2, 2, 1, 3 };
	UINT OID_ifEntryNum[]   = { 1, 3, 6, 1, 2, 1, 2, 1 };
	UINT OID_ipMACEntAddr[] = { 1, 3, 6, 1, 2, 1, 2, 2, 1, 6 };

	AsnObjectIdentifier MIB_ifMACEntAddr = { sizeof(OID_ipMACEntAddr) / sizeof(UINT), OID_ipMACEntAddr };
	AsnObjectIdentifier MIB_ifEntryType  = { sizeof(OID_ifEntryType) / sizeof(UINT), OID_ifEntryType };
	AsnObjectIdentifier MIB_ifEntryNum   = { sizeof(OID_ifEntryNum) / sizeof(UINT), OID_ifEntryNum };

	RFC1157VarBindList varBindList;
	RFC1157VarBind varBind[2];
	AsnInteger errorStatus;
	AsnInteger errorIndex;
	AsnObjectIdentifier MIB_NULL = { 0, 0 };

	int ret;
	int dtmp;
	int i = 0, j = 0;
	BOOL found = FALSE;
	m_Init = NULL;
	m_InitEx = NULL;
	m_Query = NULL;
	m_Trap = NULL;

	/* Load the SNMP dll and get the addresses of the functions necessary */
	m_hInst = LoadLibrary(_T("inetmib1.dll"));
	if (m_hInst < (HINSTANCE) HINSTANCE_ERROR)
	{
		m_hInst = NULL;
		return false;
	}

	m_Init   = (pSnmpExtensionInit)   GetProcAddress(m_hInst, "SnmpExtensionInit");
	m_InitEx = (pSnmpExtensionInitEx) GetProcAddress(m_hInst, "SnmpExtensionInitEx");
	m_Query  = (pSnmpExtensionQuery)  GetProcAddress(m_hInst, "SnmpExtensionQuery");
	m_Trap   = (pSnmpExtensionTrap)   GetProcAddress(m_hInst, "SnmpExtensionTrap");

	m_Init(GetTickCount(), &PollForTrapEvent, &SupportedView);

	/* Initialize the variable list to be retrieved by m_Query */
	varBindList.list = varBind;
	varBind[0].name = MIB_NULL;
	varBind[1].name = MIB_NULL;

	/* Copy in the OID to find the number of entries in the Interface table */
	varBindList.len = 1;		 /* Only retrieving one item */
	SNMP_oidcpy(&varBind[0].name, &MIB_ifEntryNum);

	ret = m_Query(ASN_RFC1157_GETNEXTREQUEST, &varBindList, &errorStatus, &errorIndex);
	//DB("# of adapters in this system : " << varBind[0].value.asnValue.number);

	varBindList.len = 2;

	/* Copy in the OID of ifType, the type of interface */
	SNMP_oidcpy(&varBind[0].name, &MIB_ifEntryType);

	/* Copy in the OID of ifPhysAddress, the address */
	SNMP_oidcpy(&varBind[1].name, &MIB_ifMACEntAddr);

	_lsMACAddress.clear();

	do
	{
		// Submit the query.  Responses will be loaded into varBindList.
		// We can expect this call to succeed a # of times corresponding
		// to the # of adapters reported to be in the system

		ret = m_Query(ASN_RFC1157_GETNEXTREQUEST, &varBindList, &errorStatus,
			&errorIndex);

		if (!ret)
			ret = 1;
		else
			/* Confirm that the proper type has been returned */
			ret = SNMP_oidncmp(&varBind[0].name, &MIB_ifEntryType, MIB_ifEntryType.idLength);

		if (!ret)
		{
			j++;
			dtmp = varBind[0].value.asnValue.number;
			//DB("Interface #" << j << " type : " << dtmp);

			/* Type 6 describes ethernet interfaces */
			if (dtmp == 6)
			{
				/* Confirm that we have an address here */
				ret = SNMP_oidncmp(&varBind[1].name, &MIB_ifMACEntAddr, MIB_ifMACEntAddr.idLength);
				if ((!ret)
					&& (varBind[1].value.asnValue.address.stream != NULL))
				{
					if (
						(varBind[1].value.asnValue.address.stream[0] == 0x44)
						&& (varBind[1].value.asnValue.address.stream[1] == 0x45)
						&& (varBind[1].value.asnValue.address.stream[2] == 0x53)
						&& (varBind[1].value.asnValue.address.stream[3] == 0x54)
						&& (varBind[1].value.asnValue.address.stream[4] == 0x00))
					{

						/* Ignore all dial-up networking adapters */
						//DB("Interface #" << j << " is a DUN adapter");
						continue;
					}

					if (
						(varBind[1].value.asnValue.address.stream[0] == 0x00)
						&& (varBind[1].value.asnValue.address.stream[1] == 0x00)
						&& (varBind[1].value.asnValue.address.stream[2] == 0x00)
						&& (varBind[1].value.asnValue.address.stream[3] == 0x00)
						&& (varBind[1].value.asnValue.address.stream[4] == 0x00)
						&& (varBind[1].value.asnValue.address.stream[5] == 0x00))
					{
						/* Ignore NULL addresses returned by other network interfaces */
						//DB("Interface #" << j << " is a NULL address");
						continue;
					}

					MACAddress macAddress;
					memcpy(
						&macAddress.address[0],
						varBind[1].value.asnValue.address.stream,
						sizeof(macAddress.address)
					);
					_lsMACAddress.push_back(macAddress);
				}
			}
		}
		// Stop only on an error.  An error will occur when we go
		// exhaust the list of interfaces to be examined
	} while (!ret);

	/* Free the bindings */
	SNMP_FreeVarBind(&varBind[0]);
	SNMP_FreeVarBind(&varBind[1]);

	FreeLibrary(m_hInst);

	return true;
}

#else
bool Ethernet::get_mac_addr(MACAddressList& _lsMACAddress)
{
	DB_ASSERT(0 && "Method not supported under Windows CE.");
	return false;
}
#endif
