// ---------------------------------------------------------------------------
// File:       Network.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    
// 
// Change Log:
// ---------------------------------------------------------------------------

#include "Base.h"  // automatic use of precompiled headers 
#include <stdio.h>		// sprintf

#if 1 //for Linux yamada
#include <fcntl.h>
#endif //for Linux yamada

#include "debug.h"
#include "ENetwork.h"
#include "pktbuf.h"
#include "EUtil.h"
#include "Serializable.h"
#include "TextUtil.h"

using std::string;

void Network::Nonblock(SOCKET sock)
{
#ifdef WIN32
	u_long flag = 1;
	if (ioctlsocket(sock, FIONBIO, &flag) == -1)
	{
		char buf[256];
		sprintf(buf, "Network error ioctlsocket: FIONBIO: %lu", GetLastError());
		MSG_BOX(buf);
		//exit(1);
		ASSERT(0);
	}
#else
	int flags = fcntl(sock, F_GETFL, 0);
#if defined(hpux) || defined(__hpux)
	flags |= O_NONBLOCK;
#else
	flags |= O_NONBLOCK|O_NDELAY;
#endif
	if (fcntl(sock, F_SETFL, flags) == -1)
	{
		//p DB(_strerror("fcntl: F_SETFL"));
		//exit(1);
		ASSERT(0);
	}
#endif
}

void Network::Reuse(SOCKET sock)
{
	// Set socket for re-use
	BOOL on = TRUE;
	setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char *)&on, sizeof(on));

#ifdef SO_REUSEPORT
	on = TRUE;
	setsockopt(sock, SOL_SOCKET, SO_REUSEPORT, (char *)&on, sizeof(on));
#endif
}

int Network::GetSendBufferSize(SOCKET sock)
{
	int iVal = -1;
	int iLen = sizeof(iVal);

	int iRet = getsockopt(
		sock, 
		SOL_SOCKET, 
		SO_SNDBUF,
		(char*)&iVal,
#if 1 //for Linux yamada
		(socklen_t*)&iLen);
#else
		&iLen);
#endif //for Linux yamada
	
	DB_ASSERT(0 == iRet);

	return iVal;
}

bool Network::SetSendBufferSize(SOCKET sock, int nBufSize)
{
	int iRet = setsockopt(
		sock, 
		SOL_SOCKET, 
		SO_SNDBUF,
		(char*)&nBufSize,
		sizeof(nBufSize));
	
	DB_ASSERT(0 == iRet);

	return (0 == iRet);
}

int Network::GetRecvBufferSize(SOCKET sock)
{
	int iVal = -1;
	int iLen = sizeof(iVal);

	int iRet = getsockopt(
		sock, 
		SOL_SOCKET, 
		SO_RCVBUF,
		(char*)&iVal,
#if 1 //for Linux yamada
		(socklen_t*)&iLen);
#else
		&iLen);
#endif //for Linux yamada
	
	DB_ASSERT(0 == iRet);

	return iVal;
}

bool Network::SetRecvBufferSize(SOCKET sock, int nBufSize)
{
	int iRet = setsockopt(
		sock, 
		SOL_SOCKET, 
		SO_RCVBUF, 
		(char*)&nBufSize, 
		sizeof(nBufSize));

	DB_ASSERT(0 == iRet);

	return (0 == iRet);
}

bool Network::SetBufferSize(SOCKET sock, int nBufSize)
{
	if (!SetSendBufferSize(sock, nBufSize))
		return false;

	if (!SetRecvBufferSize(sock, nBufSize))
		return false;

	return true;
}

AddressPortPair Network::GetSockName(SOCKET sock)
{
	sockaddr_in local;
	memset(&local, 0, sizeof(local));
	local.sin_family = AF_INET;

	int iLen = sizeof(local);

	u_int32_t uAddr = 0;
	u_int16_t uPort = 0;

	if (INVALID_SOCKET != sock)
	{
		getsockname(sock, (sockaddr*)(&local), (socklen_t*)&iLen);
	}

	return AddressPortPair(local.sin_addr.s_addr, local.sin_port);
}

AddressPortPair Network::GetPeerName(SOCKET sock)
{
	sockaddr_in local;
	memset(&local, 0, sizeof(local));
	local.sin_family = AF_INET;

	int iLen = sizeof(local);

	u_int32_t uAddr = 0;
	u_int16_t uPort = 0;

	if (INVALID_SOCKET != sock)
	{
		getpeername(sock, (sockaddr*)(&local), (socklen_t*)&iLen);
	}

	return AddressPortPair(local.sin_addr.s_addr, local.sin_port);
}

bool Network::IsConnected(SOCKET sock)
{
	AddressPortPair name = GetPeerName(sock);
	if (0 == name.first)
		return false;

	if (0 == name.second)
		return false;

	return true;
}

int Network::GetSocketType(SOCKET sock)
{
	if (sock == INVALID_SOCKET)
		return 0; //undefined

	int iType = 0;
	int iSize = sizeof(iType);
#if 1 //for Linux yamada
	if (getsockopt(sock, SOL_SOCKET, SO_TYPE, (char*)&iType, (socklen_t*)&iSize) != 0)
#else
	if (getsockopt(sock, SOL_SOCKET, SO_TYPE, (char*)&iType, &iSize) != 0)
#endif //for Linux yamada

		return 0;

	return iType;
}

	
string Network::ToString(const AddressPortPair& a)
{
	return ""; //itoa(a.first) + ":" + itoa(ntohs(a.second));
	//The above line is edited by Farhan
	//The return statement previously returned "itoa(a.first) ... "
}

AddressPortPair Network::FromString(const string& s)
{
	AddressPortPair addr(0, 0);

	string::size_type pos = s.find_first_of(':');
	// pos might equal npos

	string sAddr = s.substr(0, pos);
	addr.first = LookupHostAddr(sAddr);

	if (pos != string::npos)
	{
		string sPort = s.substr(pos + 1);
		addr.second = htons(atoi(sPort.c_str()));
	}

	return addr;
}

void Network::SerializeAddress(const AddressPortPair& a, string& sBuf)
{
	SerializeBytes(&a.first, sizeof(a.first), sBuf);
	SerializeBytes(&a.second, sizeof(a.second), sBuf);
}

bool Network::DeserializeAddress(AddressPortPair& a, const string& sBuf, int& iOffset)
{
	if (!DeserializeBytes(&a.first, sizeof(a.first), sBuf, iOffset))
		return false;

	if (!DeserializeBytes(&a.second, sizeof(a.second), sBuf, iOffset))
		return false;

	return true;
}
