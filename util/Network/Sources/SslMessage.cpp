// ---------------------------------------------------------------------------
// File:       SslMessage.cc
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Author:     Jozsef Vass
//
// Purpose:    SSL message class
//
// Change Log: 07/31/2001 - Created
// ---------------------------------------------------------------------------

#include "Base.h"  // automatic use of precompiled headers 

#include "inet.h"
#include "critsec.h"
#include "debug.h"
#include "Message.h"
#include "MessageTypes.h"
#include "ReturnCodes.h"
#include "SslMessage.h"
#include "ENetwork.h"
#if 1 //for Linux yamada
#include "Event.h"
#include <sys/socket.h>
#endif //for Linux yamada

using std::string;

#if 1 //for Linux yamada
#define UNREFERENCED_PARAMETER(P)          (P)
#endif //for Linux yamada

// ---------------------------------------------------------------------------
// Class implementation
// ---------------------------------------------------------------------------
SslMessage::SslMessage() :
	m_SslSocket()
{
}

SslMessage::~SslMessage()
{
}
bool SslMessage::Open(
                u_int32_t uAddr,
                u_int16_t uRemotePort,
                u_int16_t uLocalPort,
                HANDLE hNotifyEvent)
{
	TcpMessage::Open(uAddr, uRemotePort, uLocalPort, hNotifyEvent);
	return OnConnect();
}

void SslMessage::Close()
{
	DB("SslMessage::Close");

	Critical_Section cs(*m_pMutex);

#if 0 //for Linux yamada
	if ( INVALID_SOCKET != m_ConnSock)
	{
		if (NULL != m_hNotifyEvent)
		{
			WSAEventSelect(m_ConnSock, m_hNotifyEvent, 0);
			m_hNotifyEvent = NULL;
		}
	}
#endif //for Linux yamada

	// SSL Socket closes the socket.
	m_SslSocket.close();

#if 0 //for Linux yamada
	m_ConnSock = INVALID_SOCKET;
#endif //for Lionux yamada

	TcpMessage::Close();
}




int SslMessage::DoRecv(char* pBuf, int iLen)
{
	Critical_Section cs(*m_pMutex);

	if (iLen == 0)
		return 0;

	int iRet = 0;
	try
	{
		if (m_SslSocket.isOpen())
		{
			iRet = m_SslSocket.readWithTimeout(pBuf, iLen, 0);
		}
	}
	catch (IOException)
	{
		// not connected - handled below
	}
	catch (TimeoutExceededException)
	{
		return 0;
	}
	
	if (iRet > 0)
	{
		// there may be more data to be received
		SetEvent(m_hNotifyEvent);
	}
	else
	{
#if 1 //for Linux yamada
		WSASetLastError(ENOTCONN);
#else
		WSASetLastError(WSAENOTCONN);
#endif //for Linux yamada
		return ES_NOT_CONNECTED;
	}

	return iRet;
}

int SslMessage::DoSend(const char* pBuf, int iLen)
{
	int iRet = 0;
	try
	{
		if (!m_SslSocket.isOpen())
			return ES_NOT_CONNECTED;

		iRet = m_SslSocket.writePartial(pBuf, iLen);
	}
	catch (IOException)
	{
		iRet = ES_NOT_CONNECTED;
	}
	catch (TimeoutExceededException)
	{
		DB_ASSERT(0);
		iRet = ES_NOT_CONNECTED;
	}

	return iRet;
}

bool SslMessage::OnConnect(void)
{
	Critical_Section cs(*m_pMutex);

	try
	{
		m_SslSocket.connect(m_ConnSock);
		
		//m_SslSocket.finishNegotiation(SSL_TIMEOUT);
		// Wait for read and write to transparently finish negotiation
	}
	catch (IOException ex)
	{
		DB("Could not connect " << ex.getDetails());
		UNREFERENCED_PARAMETER(ex);
		CLOSE_SOCKET(m_ConnSock);
		return false;
	}
	catch (TimeoutExceededException ex)
	{
		DB("Timeout while negotiation SSL connection.  " << ex.getDetails());
		UNREFERENCED_PARAMETER(ex);
		CLOSE_SOCKET(m_ConnSock);
		return false;
	}

	return true;
}

bool SslMessage::EnumNetworkEvents(WSANETWORKEVENTS& netEvents)
{
	if (!TcpMessage::EnumNetworkEvents(netEvents))
		return false;

	if (netEvents.lNetworkEvents & FD_CONNECT)
	{
		// SSL reads and writes cannot occur until an SSL connection is
		// established.
		return true;
	}

	// try both reading and writing - this is required since SSL may
	// need to finish negotiation before it can read and write.
	netEvents.lNetworkEvents |= (FD_READ | FD_WRITE);
	return true;
}
