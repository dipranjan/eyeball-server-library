// ---------------------------------------------------------------------------
// File:       TcpMessage.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    
// 
// Change Log:
// ---------------------------------------------------------------------------

#include "Base.h"  // automatic use of precompiled headers 

#include "critsec.h"
#include "debug.h"
#if 1 //for Linux yamada
#include "Event.h"
#include "ELinuxUtil.h"
#else
#include "EWinUtil.h"
#endif //for Linux yamada
#include "Message.h"
#include "MessageTypes.h"
#include "ReturnCodes.h"
#include "TcpMessage.h"
#include "Timer.h"
#include "TextUtil.h"

#ifndef _WIN32_WCE
#include "wiocp.h"
using Winterdom::WinsockOverlapped;
#endif

using std::string;
using namespace eyeball;


// ---------------------------------------------------------------------------
// Local Helper functions
// ---------------------------------------------------------------------------

// This is a test function
static void CheckHeader(const CommandMessage& msgHdr)
{
	switch (msgHdr.m_u8Version)
	{
		case EYEBALL_CHAT_LE_11:
		case EYEBALL_CHAT_LE_12:
		case EYEBALL_CHAT_20:
		case EYEBALL_CHAT_22:
		case EYEBALL_CHAT_23:
			break;

		case EYEBALL_VIDEOMESSAGE_01:
			break;

		case EYEBALL_GROUPCHAT_01:
		case EYEBALL_GROUPCHAT_02:
			break;

		case EYEBALL_CHATROOM:
		case EYEBALL_CHATROOM_UNICODE:
			break;

		case EYEBALL_IM:
			break;

		case 0:
		case 0xff:
			DB_ASSERT(0);
			break;

		default:
			DB("Warning: incorrect message version  from server");
			break;
	}
}


// ---------------------------------------------------------------------------
// Class implementation
// ---------------------------------------------------------------------------

TcpMessage::TcpMessage()
:
	m_sMsgBuf()
{
}

TcpMessage::~TcpMessage()
{
}

bool TcpMessage::Open(
	u_int32_t uAddr, 
	u_int16_t uRemotePort, 
	u_int16_t uLocalPort,
	HANDLE hNotifyEvent)
{
	return TcpNetwork::Open(uAddr, uRemotePort, uLocalPort, hNotifyEvent);
}


void TcpMessage::Close(void)
{
	Critical_Section cs(*m_pMutex);

	TcpNetwork::Close();
	m_sMsgBuf.erase();
}

void TcpMessage::SetGlobalLocalAddr(void)
{
	TcpNetwork::SetGlobalLocalAddr();
}

int TcpMessage::GetReceiveBufferSize(void) const
{
	return m_sMsgBuf.size();
}

// The caller should check the return value
int TcpMessage::RecvMsg(string& _sBuf)
{
	Critical_Section cs(*m_pMutex);
	// Access the message header
	CommandMessage msgHdr;
	int iRet = GetHeader(msgHdr);
	if (iRet != ES_SUCCESS)
		return iRet; // message header is not yet available

	// Allocate memory for the payload
	u_int16_t uPayloadLen = msgHdr.m_u16PayloadLength;
	unsigned int cbTotal = CM_LENGTH + uPayloadLen;
	DB_ASSERT(cbTotal <= 0xffff);

	if (uPayloadLen > 0)
	{
		// Perform TCP receive
		int len = cbTotal - m_sMsgBuf.size();  // requested length
		DB_ASSERT(len > 0);

		SharedArray<char> pBuf(new char[len]);

		int iRet = Recv(pBuf.get(), len);
		if (iRet == ES_NOT_CONNECTED)
			return ES_NOT_CONNECTED;

		if (iRet <= 0)
			return ES_FAILURE;

		m_sMsgBuf.append(pBuf.get(), iRet); // add bytes received

		if (m_sMsgBuf.size() < cbTotal)
		{
			return ES_FAILURE; // payload not yet available
		}
	}

	DB_ASSERT(m_sMsgBuf.size() == cbTotal);

	// Success
	_sBuf = m_sMsgBuf;

	// Clean-up
	m_sMsgBuf.erase();

	return ES_SUCCESS;
}

int TcpMessage::RecvMsg(string& sBuf, unsigned int uTimeout)
{
	DWORD dwTotalTimeout = uTimeout;
	DWORD dwStartTime = timeGetTime();

	for (;;)
	{
		int iRet = RecvMsg(sBuf);
		if (iRet >= 0)
			return ES_SUCCESS;

		if (ES_NOT_CONNECTED == iRet)
			return ES_NOT_CONNECTED;

		DWORD dwElapsed = timeGetTime() - dwStartTime;
		uTimeout = MAX(0, (int)(dwTotalTimeout - dwElapsed));

		// Wait for the event
		DWORD dwWait = CustomWaitForSingleObject(m_hNotifyEvent, uTimeout);
		if (dwWait != WAIT_OBJECT_0)
			return ES_FAILURE;
	}
}

int TcpMessage::SendMsg(const ISerializable& msg)
{
	string sBuf;
	msg.serialize(sBuf);

	return SendMsg(sBuf.data(), sBuf.size());
}

int TcpMessage::SendMsg(const string& sBuf)
{
	return SendMsg(sBuf.data(), sBuf.size());
}

int TcpMessage::SendMsg(const char *pBuf, int iLen)
{
#ifdef DEBUG
	// Check if message length field is consistent with buffer length
	if (iLen < CM_LENGTH)
	{
		DB_ASSERT(0 && "Message too small");
		return ES_FAILURE;
	}

	// Assign into string for possibly early null-termination
	string sBuf;
	sBuf.assign(pBuf, iLen);

	CommandMessage msgHdr;

	if (msgHdr.deserialize(sBuf) < 0)
	{
		DB_ASSERT(0 && "TcpMessage::SendMessage - header incorrect");
		return ES_FAILURE;
	}

	CheckHeader(msgHdr);

	int iMsgLen = msgHdr.m_u16PayloadLength + CM_LENGTH;
	if (iLen != iMsgLen)
	{
		DB_ASSERT(0 && "TcpMessage::SendMessage - length incorrect");
		return ES_FAILURE;
	}
#endif

	return TcpNetwork::Send(pBuf, iLen);
}

int TcpMessage::GetHeader(CommandMessage& msgHdr)
{
	// Return message header, if it is buffered
	if (m_sMsgBuf.size() >= CM_LENGTH)
	{
		msgHdr.deserialize(m_sMsgBuf);
		CheckHeader(msgHdr);
		return ES_SUCCESS;
	}

	int len = CM_LENGTH - m_sMsgBuf.size();
	DB_ASSERT(len > 0);

	SharedArray<char> pBuf = SharedArray<char>(new char[len]);

	int iRet = Recv(pBuf.get(), len);
	if (iRet == ES_NOT_CONNECTED)
		return ES_NOT_CONNECTED;

	if (iRet <= 0)
		return ES_FAILURE;

	m_sMsgBuf.append(pBuf.get(), iRet); // add bytes received

	if (m_sMsgBuf.size() >= CM_LENGTH)
	{
		msgHdr.deserialize(m_sMsgBuf);
		CheckHeader(msgHdr);
		return ES_SUCCESS;
	}

	return ES_FAILURE;
}


// ---------------------------------------------------------------------------
// TcpMessageEx class
// ---------------------------------------------------------------------------

#ifndef _WIN32_WCE

TcpMessageEx::TcpMessageEx(bool bIocpEnabled)
:
	m_bIocpEnabled(bIocpEnabled),
	m_sReadyBytes(),
	m_bReceiving(false),
	m_pOVRecv(NULL),
	m_bSending(false),
	m_pOVSend(NULL)
{
}

TcpMessageEx::~TcpMessageEx()
{
	//DB_ASSERT(NULL == m_pOVRecv);  // check that object was closed
}

int TcpMessageEx::IoRequestComplete(WinsockOverlapped* pOV)
{
	if (pOV == m_pOVRecv)
	{
		return ReceiveComplete();
	} 
	else if (pOV == m_pOVSend)
	{
		return SendComplete();
	}
	else
	{
		DB_ASSERT(0);
		return ES_FAILURE;
	}
}

int TcpMessageEx::SendComplete(void)
{
	if (!m_bSending)
	{
		DB_ASSERT(0 && "Unexpected send completed");
		return ES_FAILURE;
	}

	m_bSending = false;

	SAFE_DELETE(m_pOVSend);

	// Continue sending buffered data
	SendBufferedData();

	return ES_SUCCESS;
}

int TcpMessageEx::ReceiveComplete(void)
{
	if (!m_bReceiving)
	{
		DB_ASSERT(0 && "Unexpected receive completed");
		return ES_FAILURE;
	}

	m_bReceiving = false;

	int iLen = m_pOVRecv->NumBytes;
	if (iLen <= 0)
		return ES_NOT_CONNECTED;

	// Store the received data, to be passed on the next call to DoRecv
	m_sReadyBytes.append(m_pOVRecv->buf, iLen);
	
	// Destroy buffer
	SAFE_DELETE(m_pOVRecv);

	// Continue receiving data
	int iRet = OverlappedReceive();
	if (ES_NOT_CONNECTED == iRet)
		return ES_NOT_CONNECTED;

	return ES_SUCCESS;
}

int TcpMessageEx::OverlappedReceive(void)
{
	if (m_bReceiving)
	{
		// already receiving
		return ES_SUCCESS;
	}

	// Initialize overlapped receive buffer
	DB_ASSERT(NULL == m_pOVRecv);
	m_pOVRecv = new WinsockOverlapped;
	
	memset(m_pOVRecv, 0, sizeof(*m_pOVRecv));
	m_pOVRecv->wsabuf.buf = m_pOVRecv->buf;
	m_pOVRecv->wsabuf.len = sizeof(m_pOVRecv->buf);

	// Receive from network
	DWORD dwBytesReceived = 0;
	DWORD dwFlags = 0;
	int iRet = WSARecv(
		m_ConnSock,
		&m_pOVRecv->wsabuf,
		1,
		&dwBytesReceived,
		&dwFlags,
		m_pOVRecv,
		NULL);

	if (iRet == SOCKET_ERROR)
	{
		int iLastError = WSAGetLastError();
		switch (iLastError)
		{
		case WSAEMSGSIZE:
			DB("recv error (WSAEMSGSIZE)");
			break;

		case WSAEWOULDBLOCK:
			DB("recv error (WSAEWOULDBLOCK)");
			break;

		case WSA_IO_PENDING:
			// this error is normal behaviour
			m_bReceiving = true;
			return ES_SUCCESS;

		default:
			DB("Lost connection - error code: " << iLastError);
			return ES_NOT_CONNECTED;
		}

		return ES_FAILURE;
	}

	m_bReceiving = true;
	return ES_SUCCESS;
}

void TcpMessageEx::DeleteReceiveBuffer(void)
{
#ifdef _DEBUG
	if (NULL != m_pOVRecv)
	{
		memset(m_pOVRecv, (-1), sizeof(*m_pOVRecv));
	}
#endif

	SAFE_DELETE(m_pOVRecv);	
}


// ---------------------------------------------------------------------------
// Protected methods
// ---------------------------------------------------------------------------

int TcpMessageEx::DoRecv(char* pBuf, int iLen)
{
	Critical_Section cs(*m_pMutex);
	if (!m_bIocpEnabled)
	{
		return TcpMessage::DoRecv(pBuf, iLen);
	}

	if (iLen == 0)
		return 0;

	if (m_sReadyBytes.empty())
		return 0;

	string sBuf = m_sReadyBytes;

	int iTotalBytesReady = m_sReadyBytes.size();
	int iBytesToCopy = MIN(iLen, iTotalBytesReady);
	memcpy(pBuf, m_sReadyBytes.data(), iBytesToCopy);

	// Keep remaining bytes (may be empty)
	m_sReadyBytes = sBuf.substr(iBytesToCopy);
	DB_ASSERT((int)m_sReadyBytes.size() == iTotalBytesReady - iBytesToCopy);

	return iBytesToCopy;
}

bool TcpMessageEx::SendBufferedData(void)
{
	Critical_Section cs(*m_pMutex);

	if (!m_bIocpEnabled)
	{
		return TcpMessage::SendBufferedData();
	}

	if (m_pOVSend != NULL)
	{
		// waiting for existing data to finish sending
		DB_ASSERT(m_bSending);
		return true;
	}

	if (IsSendBufferEmpty())
		return true;  // no more data to be sent

	string sData;
	if (!GetSendBufferPacket(sData))
	{
		DB_ASSERT(0 && "send buffer error");
		return false;
	}

	// Initialize overlapped send buffer
	DB_ASSERT(NULL == m_pOVSend);
	m_pOVSend = new WinsockOverlapped;
	memset(m_pOVSend, 0, sizeof(*m_pOVSend));
	
	// Copy as many bytes as we can into this buffer, up to one packet.
	int nLen = MIN(sData.size(), sizeof(m_pOVSend->buf));
	memcpy(m_pOVSend->buf, sData.data(), nLen);

	m_pOVSend->wsabuf.buf = m_pOVSend->buf;
	m_pOVSend->wsabuf.len = nLen;
	DB_ASSERT(m_pOVSend->wsabuf.len <= sizeof(m_pOVSend->buf));

	// Remove these bytes from our send buffer.
	RemoveSendBufferBytes(nLen);

	// Send to network using overlapped socket
	DWORD dwBytesSent = 0;
	DWORD dwFlags = 0;
	int iRet = WSASend(
		m_ConnSock,
		&m_pOVSend->wsabuf,
		1,
		&dwBytesSent,
		dwFlags,
		m_pOVSend,
		NULL);

	if (iRet == SOCKET_ERROR)
	{
		int iLastError = WSAGetLastError();
		switch (iLastError)
		{
		case WSAEWOULDBLOCK:
			DB("send error (WSAEWOULDBLOCK)");
			break;

		case WSA_IO_PENDING:
			// this error is normal behaviour
			m_bSending = true;
			return true;

		default:
			DB("Lost connection - error code: " << iLastError);
			break;
		}

		SAFE_DELETE(m_pOVSend);
		return false;
	}

	m_bSending = true;
	return true;
}
#endif
