#

.SUFFIXES: .cc $(.SUFFIXES)

ALL = \
	ecserver.o \
	ecserver-conf.o \
	ebserver.o \
	ebserver-conf.o \
	crserver.o \
	crserver-conf.o \
	vmserver.o \
	vmserver-conf.o \
	ecpasswd.o \
	echoserver.o \
	testclient.o \
	qa_client.o
#	ebsip.o \
#	sipConfig.o \
#	ebtestclient.o
#	udp_sender.o \
#	udp_receiver.o \
#	CR_testclient.o \
#	dmallocc.o
#	archive.o \
#	reflector.o\
#	echomain.o \
#	improxy.o \
#	imserver-conf.o \
#	vmtestclient.o \
#	tcpfwd.o \
#	nullserver.o \
#	testyahoo.o \
#	gcserver.o \
#	mpagent-conf.o \

all: depend $(ALL)

.cc.o:
	$(C++) -o $@ -c $(PROF) $(CFLAGS) $*.cc
.c.o:
	$(CC) -o $@ -c $(PROF) $(CFLAGS) $*.c

top_srcdir = ../..

CC = gcc
C++ = g++
CCOPT = -O2 -Wall -g
PERL = perl
PROF = 
DEPEND = $(PERL) $(top_srcdir)/scripts/update_dependencies
LS_INCLUDE = ../$(top_srcdir)/ls2/client
SSL_INCLUDE = /usr/local/ssl/include
DMALLOC_INCLUDE = /mnt/nfshome/vass/dmalloc-5.2.1
#DMALLOC_INCLUDE = /usr/local/include
#RESIP_INCLUDE = /home/farhad/RESIP/sip
RESIP_INCLUDE = /home/vass/resiprocate-0.4.0/
PROF =

STATIC = -static

LIBS =

INCLUDE = \
	-I$(top_srcdir)/inc \
	-I../net -I../node -I../util -I../main\
	-I../ecserver \
	-I../mpagent \
	-I../vmserver \
	-I../crserver \
	-I../ebserver \
	-I../reflector \
	-I../imserver \
	-I../improxy \
	-I../imserver_yahoo \
	-I../imlibs/libyahoo \
	-I../imserver_msn \
	-I../imserver_toc \
	-I../testclient \
	-I../CR_testclient \
	-I../vmtestclient \
	-I../ebtestclient \
	-I../qa_client \
	-I../archive \
	-I../esip \
	-I$(LS_INCLUDE) -I$(SSL_INCLUDE) -I$(DMALLOC_INCLUDE) -I$(RESIP_INCLUDE)


#XXX NLAYER define / MB_DEBUG
DEFINE =  -DDEBUG -fsigned-char -fno-inline \
	-D_REENTRANT -D_POSIX_PTHREAD_SEMANTICS \
	-D_POSIX_PER_PROCESS_TIMER_SOURCE -D_PTHREADS -DUNICODE

SHLIB_SUFFIX   = .so
SHLIB_LD       = gcc -shared
SHLIB_LD_LIBS  = 
SHLIB_CFLAGS   = -fPIC

BFLAGS = $(DEFINE) $(INCLUDE)
CFLAGS = $(CCOPT) $(BFLAGS)

OBJ_C =

OBJ_CC = \
	ecserver.o \
	ecserver-conf.o \
	echoserver.o \
	ebserver.o \
	ebserver-conf.o \
	crserver.o \
	crserver-conf.o \
	vmserver.o \
	vmserver-conf.o \
	ecpasswd.o \
	ebsip.o \
	sipConfig.o
#	improxy.o \
#	echomain.o \
#	reflector.o\
#	gcserver.o \
#	mpagent-conf.o \
#	imserver-conf.o \
#	testclient.o \
#	ebtestclient.o \
#	CR_testclient.o \
#	vmtestclient.o \
#	qa_client.o \
#	tcpfwd.o \
#	nullserver.o \
#	testyahoo.o \
#	udp_sender.o \
#	udp_receiver.o \
#	dmallocc.o


OBJS = $(OBJ_C) $(OBJ_CC) 

SRCS = $(OBJ_C:.o=.c) $(OBJ_CC:.o=.cc)

clean:	
	$(RM) *.o .depend* core
	$(RM) $(ALL)

depend: Makefile
	$(DEPEND) $(INCLUDE) .depend $(SRCS)

-include .depend

