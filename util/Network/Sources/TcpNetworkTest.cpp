#include<string>
#include <sys/socket.h> /* for socket(), connect(), send(), and recv() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_addr() */

#include "SmartPtr.h"
#include "debug.h"
#include "critsec.h"
#include "EUtil.h"
#include "inet.h"
#include "TcpNetworkTest.h"
#include "TurnMessage.h"
#include "ENetwork.h"

using namespace eyeballTurn;

CTcpNetworkTest::CTcpNetworkTest():
	m_ConnSock(INVALID_SOCKET),
        m_uRemoteAddr(0),
        m_uRemotePort(0),
        m_pMutex(new Mutex)

{

}
CTcpNetworkTest::~CTcpNetworkTest()
{
	Close();
}

bool CTcpNetworkTest::Open(
                u_int32_t uRemoteAddr,
                u_int16_t uRemotePort,
                u_int16_t uLocalPort,
                HANDLE hNotifyEvent)
{

	SOCKET fd = socket(AF_INET, SOCK_STREAM, 0);
        if (fd == INVALID_SOCKET)
        {
                DB("invalid socket");
                return false;
        }
                                                                                                                            
	Critical_Section cs(*m_pMutex);
                                                                                                                            
	m_uRemoteAddr = uRemoteAddr;
	m_uRemotePort = uRemotePort;
                                                                                                                            
	m_ConnSock = fd;
	fd = INVALID_SOCKET;
                                                                                                                            
	struct sockaddr_in sin;
        memset((char *)&sin, 0, sizeof(sin));
        sin.sin_family = AF_INET;
        sin.sin_port = uRemotePort;
        sin.sin_addr.s_addr = uRemoteAddr;
                                                                                                                            
        // Ignore return value of connect since we wait for FD_CONNECT event
        if (::connect(m_ConnSock, (struct sockaddr *)&sin, sizeof(sin)) != 0)
        {
		DB("connect failed");
		CLOSE_SOCKET(m_ConnSock);
		return false;
	}

	//set tcp nodelay to inactivate nagyles algorithm.
	nodelay(m_ConnSock);


	return true;
}
                                                                                                                            
void CTcpNetworkTest::Close(void)
{
	Critical_Section cs(*m_pMutex);
	if (m_ConnSock != INVALID_SOCKET)
        {
                CLOSE_SOCKET(m_ConnSock);
        }


}
int  CTcpNetworkTest::Send(const char* pBuf, int iLen)
{
	Critical_Section cs(*m_pMutex);
	if (m_ConnSock == INVALID_SOCKET)
        {
                DB("socket is invalid");
                return -1; 
        }

	int iRet = ::send(m_ConnSock, pBuf, iLen, 0);
        if (iRet < 0)
		return -1;
	return iRet;
}

int  CTcpNetworkTest::Recv(char* pBuf, int iLen)
{
	 Critical_Section cs(*m_pMutex);
        int iRet = ::recv(m_ConnSock, pBuf, iLen, 0);
        if (iRet < 0)
        {
		 DB("TcpNetworkTest::recv error : iRet = "<<iRet);
                 CLOSE_SOCKET(m_ConnSock); //added 19_1
	}

	return iRet;
}

//#define USE_ECHO_SERVER
int CTcpNetworkTest::GetPortFromTurnServer()
{
        CTurnMessage msg;
        if( !m_sTurnUserName.empty() )
        {
                msg.SetAccount(m_sTurnUserName,m_sTurnPassword);
                msg.SetNonceRealm(m_sTurnNonce,m_sTurnRealm);
        }
        else
        {
                //DB_ASSERT( 0 && "UserName, Password, Nonce,.. not set properly");
        }
#ifdef USE_ECHO_SERVER
        std::string sBuf = "hello world--";
#else
        std::string sBuf = msg.CreateAllocateRequest(false);
#endif
                                                                                                                            
        Send(sBuf.c_str(), sBuf.size() );
        //blocking recv
                                                                                                                            
        char szBuf[2048];
        int iRet = Recv(szBuf,2048);
        DB("Recv: iRet = "<<iRet);
        if(iRet <= 0)
        {
                DB("received failed");
                //ASSERT(0);
                return -1;
        }
        std::string s;
        s.assign(szBuf,iRet);
#ifdef USE_ECHO_SERVER
        DB("Recv: Data =  "<< s.c_str());
#else
        if (!msg.GetMappedAddr(s, m_TurnDetectedAddr/*m_DetectedLocalAddr*/))
        {
                //DB("TURN ALLOCATE REQUEST parse failed");
                return -2;
        }
                                                                                                                            
        DB("Allocated addr: "<< LookupHostName(m_TurnDetectedAddr.first/*m_DetectedLocalAddr.first*/));
        DB("Allocated port: "<< ntohs(m_TurnDetectedAddr.second));
#endif
                                                                                                                            
        return 0;
                                                                                                                            
}

int CTcpNetworkTest::TurnSendRequest(AddressPortPair& addrDest)
{
                                                                                                                            
        CTurnMessage msg;
        if( !m_sTurnUserName.empty() )
        {
                msg.SetAccount(m_sTurnUserName,m_sTurnPassword);
                msg.SetNonceRealm(m_sTurnNonce,m_sTurnRealm);
        }
        else
        {
                //DB_ASSERT( 0 && "UserName, Password, Nonce,.. not set properly");
        }
        std::string sBuf = msg.CreateSendRequest(addrDest);
                                                                                                                            
        Send(sBuf.c_str(), sBuf.size());
        DB("TCP: SEND Request is sent to TURN Server.");

	//test
	//return 0;

	//blocking recv
                                                                                                                            
        char szBuf[2048];
        int iRet = Recv(szBuf,2048);
        DB("Recv: iRet = "<<iRet);
        if(iRet <= 0)
        {
                DB("received failed");
                //ASSERT(0);
                return -1;
        }
        std::string s;
        s.assign(szBuf,iRet);
	if (!msg.ParseSendResponse(s))
        {
		DB("ParseSendResponse ERROR.");
		return -1;
		//DB_ASSERT(0);
	}

	DB("Got SendRequest Response Successfully.");
	return 0;
}

void CTcpNetworkTest::SetTurnParams(const std::string& sUserName,
                const std::string& sPassword,
                const std::string& sNonce,
                const std::string& sRealm
                )
{
                                                                                                                            
        m_sTurnUserName = sUserName;
        m_sTurnPassword = sPassword;
        m_sTurnNonce = sNonce;
        m_sTurnRealm = sRealm;
                                                                                                                            
}

int CTcpNetworkTest::TurnSetActiveDestRequest(AddressPortPair& addrDest)
{
	CTurnMessage msg;
        if( !m_sTurnUserName.empty() )
        {
                msg.SetAccount(m_sTurnUserName,m_sTurnPassword);
                msg.SetNonceRealm(m_sTurnNonce,m_sTurnRealm);
        }
        else
        {
                //DB_ASSERT( 0 && "UserName, Password, Nonce,.. not set properly");
        }
        std::string sBuf = msg.CreateSetActiveDestinationRequest(addrDest);

	Send(sBuf.c_str(), sBuf.size() );
        DB("TCP: SETACTIVEDESTINATION Request is sent to TURN Server.");
        //blocking recv
	//test
	//return 0;
                                                                                                                            
        char szBuf[2048];
        int iRet = Recv(szBuf,2048);
        DB("Recv: iRet = "<<iRet);
        if(iRet <= 0)
        {
                DB("received failed");
                //ASSERT(0);
                return -1;
        }
        std::string s;
        s.assign(szBuf,iRet);

	if (!msg.ParseSetActiveDestinationResponse(s))
	{
		DB("ParseSetActiveDestinationResponse ERROR.");
		return -1;
	}

	DB("Got SetActiveRequest Response Successfully.");
	return 0;
}

AddressPortPair CTcpNetworkTest::GetTurnDetectedAddrPort()
{
        AddressPortPair addrZero(0,0);
        if( m_TurnDetectedAddr != addrZero )
                return m_TurnDetectedAddr;

        //ASSERT( m_TurnDetectedAddr != addrZero );
        return addrZero;
}

SOCKET CTcpNetworkTest::GetSocket(void)
{
        Critical_Section cs(*m_pMutex);
        return m_ConnSock;
}

AddressPortPair CTcpNetworkTest::GetLocalAddrPort()
{
	Critical_Section cs(*m_pMutex);
        return Network::GetSockName(m_ConnSock);

}
int CTcpNetworkTest::ReleasePortFromTurnServer()
{
        AddressPortPair addrSecondServer;
                                                                                                                            
        CTurnMessage msg;
        if( !m_sTurnUserName.empty() )
        {
                msg.SetAccount(m_sTurnUserName,m_sTurnPassword);
                msg.SetNonceRealm(m_sTurnNonce,m_sTurnRealm);
        }
        else
        {
                //DB_ASSERT( 0 && "UserName, Password, Nonce,.. not set properly");
        }
        std::string sBuf = msg.CreateAllocateRequest(true);
                                                                                                                            
        Send(sBuf.c_str(), sBuf.size());
        return 0;
}

int CTcpNetworkTest::TurnSetActiveDestRequestNB(AddressPortPair& addrDest)
{
        CTurnMessage msg;
        if( !m_sTurnUserName.empty() )
        {
                msg.SetAccount(m_sTurnUserName,m_sTurnPassword);
                msg.SetNonceRealm(m_sTurnNonce,m_sTurnRealm);
        }
        else
        {
                //DB_ASSERT( 0 && "UserName, Password, Nonce,.. not set properly");
        }
        std::string sBuf = msg.CreateSetActiveDestinationRequest(addrDest);
                                                                                                                            
        Send(sBuf.c_str(), sBuf.size() );
        DB("TCP: SETACTIVEDESTINATION Request is sent to TURN Server.");
        //blocking recv
        //test
        return 0;
}
