
#include "SslSocketTest.h"

#include "debug.h"

#define CHK_ERR(err,s) if ((err)==-1) { perror(s); return;}
#define CHK_SSL(err) if ((err)==-1) { ERR_print_errors_fp(stderr); return; }


SslSocketTest::SslSocketTest()
	:
	m_bInit(false),
	m_iFd(-1)
{
	initLibrary();
}

SslSocketTest::~SslSocketTest()
{
	close();
}

int SslSocketTest::connect(int fd)
{
	SSL_METHOD const *meth = NULL;
        meth = SSLv3_client_method();
        m_pSslContext = SSL_CTX_new (meth);
        if( !m_pSslContext )
        {
                m_bInit = false;
                return -1;
	}

	m_pSsl = SSL_new (m_pSslContext);                         
        if( !m_pSsl )
        {
                m_bInit = false;
                return -1;
	}

	m_iFd = fd; 

	SSL_set_fd (m_pSsl, m_iFd);
	int iRet = SSL_connect (m_pSsl); 
	if( iRet == -1)
	{
		ERR_print_errors_fp(stderr);
                m_bInit = false;
		return -1;
	}

	m_bInit = true;
	//printf ("SSL connection using %s\n", SSL_get_cipher (m_pSsl));
	return 0;
}

int SslSocketTest::read(char* buf, int len)
{
	if( !m_bInit )
		return -1;
                                                                                
  int iRet = SSL_read (m_pSsl, buf, len-1);                     
  if(iRet == -1)
	  ERR_print_errors_fp(stderr);
  buf[iRet] = '\0';
  //printf ("Got %d\n", iRet);

  return iRet;
}
void SslSocketTest::write(const char* buf, int len)
{
	if( !m_bInit )
		return ;
	int iRet = SSL_write (m_pSsl, buf, len);  
	CHK_SSL(iRet);
}
void SslSocketTest::close()
{
  if(!m_bInit)
	return;
  
//p  ERR_free_strings() ;

  if(m_pSsl)
	  SSL_shutdown (m_pSsl);
  ::close (m_iFd);
  if(m_pSsl)
  {
	  SSL_free (m_pSsl);
	  m_pSsl = NULL;
  }
  if(m_pSslContext)
  {
	  SSL_CTX_free (m_pSslContext);
	  m_pSslContext = NULL;
  }
  m_bInit = false;
}

void SslSocketTest::initLibrary()
{
	SSLeay_add_ssl_algorithms();
//p	SSL_load_error_strings();
}

int SslSocketTest::getFd() const
{
  if (!m_bInit)
    return -1;
                                                                                                                            
  return SSL_get_fd(m_pSsl);
}


