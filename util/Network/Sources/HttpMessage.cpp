// ---------------------------------------------------------------------------
// File:       HttpMessage.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    
// 
// Change Log:
// ---------------------------------------------------------------------------

#include "Base.h"  // automatic use of precompiled headers 

#include "critsec.h"
#include "debug.h"
#if 1 //for Linux yamada
#include <sys/socket.h>
#include <unistd.h>
#include <stdio.h>
#include "config.h"
#include "Event.h"
#include "ELinuxUtil.h"
#else
#include "EWinUtil.h"
#endif //for Linux yamada

#include "Message.h"
#include "MessageTypes.h"
#include "ReturnCodes.h"
#include "TcpMessage.h"
#include "Timer.h"
#include "TextUtil.h"

#if 0 //for Linux yamada
#include "wiocp.h"
#endif //for Linux yamada

#include "HttpMessage.h"

using std::string;
using namespace eyeball;

#define MULTIPART_SEPARATOR "--EBMultipart--Separator--"
#define MULTIPART_HEADER "Content-type: image/jpeg"

#define HTTP_CONNECT_TIMEOUT  30000  // ms

#define PROXY_TIMEOUT 2000

#define HTTP_HEADER_LENGTH 16

#define DEFAULT_BUFFER_SIZE 10*1024
#define VIDEO_BUFFER_EXTRA 4*1024		// extra space for video buffer to contain an extra frame
										// so that video capturing stops instead of overflowing the buffer

#define FLUSH_BUFFER_PRIORITY 4		// priorities >= this value have their buffers flushed when full

// define to enable logging when registry is set
#if 0 //for Linux yamada
#define LOG_HTTP_MESSAGE
#endif //for Linux yamada

#ifdef LOG_HTTP_MESSAGE
#include "DebugLog.h"

#if 1 //for Linux yamada
#define LOGP(X)
#else
#define LOGP(X) { \
					Critical_Section cs(*m_pMutexLog); \
					LOG("[id=" << m_nLogId << "] -- " << X) \
				}
#endif //for Linux yamada

LOG_FILE(LOG_FILENAME_HTTP);

static int s_nLogId = 0;
static int s_nRef = 0;
static int s_nSession = 0;

#else
#define LOGP(X)
#endif

// ---------------------------------------------------------------------------
// Class implementation
// ---------------------------------------------------------------------------

HttpMessage::HttpMessage(string m_sProxyAddr, WORD m_sProxyPort, string sLogTail)
:
	TcpMessage(),
	m_pMutexLog(new Mutex),
	m_bUseHttps(false),
	m_bSendHttpHeader(false),
	m_bRecvHttpHeader(false),
	m_bLongHeader(false),
	m_bProxyError(false),
	m_nSendType(METHOD_POST),
	m_sProxyAddr(""),
	m_nLogId(0),
	m_uProxyPort(0),
	m_sBufMsg("")
{
#ifdef LOG_HTTP_MESSAGE
	Critical_Section cs(*m_pMutexLog);

	m_nLogId = s_nLogId;
	s_nLogId++;
	bool bEnable = GetLogEnabled(LOG_HTTP);

	// whether this is the first reference
	if (s_nRef == 0)
	{
		// add the tail string to the log file
		if (sLogTail.length() > 0)
		{
			string sFileName = LOG_FILENAME_HTTP;
			int nFind = sFileName.find(".log");
			if (nFind != string::npos)
			{
				sFileName = sFileName.substr(0, nFind) + sLogTail + ".log";
			}
			else
			{
				sFileName += sLogTail;
			}		
			LOG_SETFILE(sFileName.c_str());
		}

		// open the log file
		LOG_ENABLE(bEnable);
		LOG_OPEN();
	}

	// increment the reference
	s_nRef++;
#endif

	int i;
	for (i=0; i<NUM_PRIORITY; i++)
	{
		m_MsgQ[i].Clear();
		m_nQueueBufLimit[i] = DEFAULT_BUFFER_SIZE;
	}
	m_sPriorityList = "";
}

HttpMessage::~HttpMessage()
{
#ifdef LOG_HTTP_MESSAGE
	{
		Critical_Section cs(*m_pMutexLog);

		s_nRef--;
		if (s_nRef == 0)
		{
			LOGP("End log");
			LOG_CLOSE();
#if 0
			// increment the next log filename
			s_nSession++;
			string sFileName = LOG_FILENAME_HTTP;
			int nFind = sFileName.find(".log");
			if (nFind != string::npos)
			{
				sFileName = sFileName.substr(0, nFind) + itoa(s_nSession) + ".log";
			}
			else
			{
				sFileName += itoa(s_nSession);
			}
			
			LOG_SETFILE(sFileName.c_str());
#endif
		}
	}
#endif
	m_pMutexLog.reset();
}

// ---------------------------------------------------------------------------
// Static functions
// ---------------------------------------------------------------------------

int HttpMessage::Pack(
	string& sBuf, 
	const string& sHostName, // used for GET or POST
	u_int16_t uPort,		 // used for GET or POST
	int nSendType)
{
	string sHdr;
	string sHttpTail("/ HTTP/1.1\r\n");
	string sNoCache("Pragma: no-cache no-store\r\nPragma: no-cache\r\n");
	string sSeparator(MULTIPART_SEPARATOR);
	switch (nSendType) 
	{
	case METHOD_GET:
		sHdr = "GET http://";
		sHdr.append(sHostName);
		sHdr.append(":");
		sHdr.append(itoa(uPort));
		sHdr.append(sHttpTail);
		sHdr.append(sNoCache);
		sHdr.append("\r\n");
		break;
	case METHOD_POST:
		{
			sHdr = "POST http://";
			sHdr.append(sHostName);
			sHdr.append(":");
			sHdr.append(itoa(uPort));
			sHdr.append(sHttpTail);
			if (m_bLongHeader)
			{
				sHdr.append(sNoCache);
				sHdr.append("Connection: Keep-Alive\r\n");
				sHdr.append("Content-Type: image/gif\r\n");
			}
			sHdr.append("Content-Length: ");
			sHdr.append(itoa(sBuf.size()));
			sHdr.append("\r\n\r\n");
			int iOldLength = sHdr.length();
			sHdr.resize(sHdr.size() + sBuf.size());
			memcpy(&sHdr[iOldLength], sBuf.data(), sBuf.size());
		}
		break;
	case METHOD_OK:
		if (sBuf.size() > 0)
		{
			sHdr = "HTTP/1.1 200 OK\r\n";
			if (m_bLongHeader)
			{
				sHdr.append("Pragma: no-cache no-store\r\n");
				sHdr.append("Connection: Keep-Alive\r\n");
				sHdr.append("Content-Type: image/gif\r\n");
			}
			sHdr.append("Content-Length: ");
			sHdr.append(itoa(sBuf.size()));
			sHdr.append("\r\n\r\n");
			int iOldLength = sHdr.length();
			sHdr.resize(sHdr.size() + sBuf.size());
			memcpy(&sHdr[iOldLength], sBuf.data(), sBuf.size());
		}
		else
		{
			sHdr.append("\r\n");
		}
		break;
	case METHOD_MULTIPART_FIRST:
		sHdr = "HTTP/1.1 206 Partial Content\r\n\r\n";
		sHdr.append("Content-type: multipart/mixed; boundary=");
		sHdr.append(sSeparator);
		sHdr.append("\r\n\r\n");
		sHdr.append(sSeparator);
		sHdr.append("\r\n");
	case METHOD_MULTIPART:
		sHdr.append(MULTIPART_HEADER);
		sHdr.append("\r\n\r\n");
		sHdr.append(sBuf);
		sHdr.append("\r\n\r\n");
		sHdr.append(sSeparator);
		sHdr.append("\r\n");
		break;
	default:
		return ES_FAILURE;
	}
	sBuf.assign(sHdr);
	return ES_SUCCESS;
}

int HttpMessage::Unpack(string& sBuf)
{
	int nFindPos = sBuf.find(" ");
	if (nFindPos == string::npos || sBuf.size() == 0)
	{
		return ES_FAILURE;
	}
	string sFirst = sBuf.substr(0, nFindPos);
	if (sFirst.compare("get") == 0)
	{
		// GET method
		sBuf.erase();
		return METHOD_GET;
	}
	if (sFirst.compare("post") == 0)
	{
		// POST method
		string sContent("content-length");
		string sLower = sBuf;
		nFindPos = sLower.find(sContent);
		if (nFindPos == string::npos)
		{
			// no content
			sBuf.erase();
		}
		else
		{
			// contains content
			nFindPos = sBuf.find("\r\n\r\n");
			sBuf = sBuf.substr(nFindPos+4);
		}
		return METHOD_POST;
	}

	int nRet = METHOD_MULTIPART;
	if (sFirst.compare("http/1.1") == 0 || sFirst.compare("http/1.0") == 0)
	{
		sBuf = sBuf.substr(nFindPos+1);
		string sLower = sBuf;
		if (sLower.find("200 ok") == 0)
		{
			// OK Response
			string sContent("content-length");
			nFindPos = sLower.find(sContent);
			if (nFindPos == string::npos)
			{
				// no content
				sBuf.erase();
			}
			else
			{
				// contains content
				nFindPos = sBuf.find("\r\n\r\n");				
				string sTmp = sBuf.substr(nFindPos+4);
				sBuf = sTmp;
			}
			return METHOD_OK;
		}
		if (sLower.find("200 connection established") == 0)
		{
			// CONNECTED Response
			sBuf.erase();
			return METHOD_CONNECTED;
		}
		if (sLower.find("206 partial content") != 0)
		{
			// Invalid type
			sBuf.erase();
			return ES_FAILURE;
		}

		// First part of multipart Response
		nRet = METHOD_MULTIPART_FIRST;
	}
	nFindPos = sBuf.find(MULTIPART_HEADER);
	if (nFindPos == string::npos)
	{
		// Invalid message
		return ES_FAILURE;
	}
	sBuf = sBuf.substr(nFindPos);
	nFindPos = sBuf.find("\r\n\r\n");
	if (nFindPos == string::npos)
	{
		// Invalid message
		return ES_FAILURE;
	}
	sBuf = sBuf.substr(nFindPos+4);
	nFindPos = sBuf.find("\r\n\r\n");
	if (nFindPos == string::npos)
	{
		// Invalid message
		return ES_FAILURE;
	}

	// multipart response
	sBuf = sBuf.substr(0, nFindPos);

	return nRet;
}

// determine the HTTP message type given the header of the message
int HttpMessage::GetHeaderMethod(const string& sHeader)
{
	int iRet = ES_FAILURE;
	string sLower = sHeader;
	if (sLower.find("get") != string::npos)
	{
		iRet = METHOD_GET;
	}
	else if (sLower.find("post") != string::npos)
	{
		iRet = METHOD_POST;
	}
	else
	{
		int nFindPos = sHeader.find(" ");
		string sFirst;
		if (nFindPos == string::npos)
		{
			sFirst = sHeader;
		}
		else
		{
			sFirst = sHeader.substr(0, nFindPos);
		}
		if (sFirst.compare("http/1.1") == 0 || sFirst.compare("http/1.0") == 0)
		{
			iRet = METHOD_OK;
			if (nFindPos != string::npos)
			{
#if 1 //for Linux yamada
				sFirst = (char*) sHeader.data()+nFindPos+1;
#else
				sFirst = (char*) &sHeader[nFindPos+1];
#endif
				sLower = sFirst;
				if (sLower.find("200 ok") != string::npos)
				{
					iRet = METHOD_OK;
				}
				else if (sLower.find("200 connection established") != string::npos)
				{
					iRet = METHOD_CONNECTED;
				}
				else
				{
					iRet = ES_FAILURE;
				}
			}
		}
	}

	return iRet;
}

bool HttpMessage::TrimToHeaderMethod(string& sHeader)
{
	string sLower = sHeader;
	int nFind = sLower.find("get");
	if (nFind == 0)
	{
		// header locate at the beginning
		return false;
	}
	if (nFind != string::npos)
	{
		// find the header and it's not at the beginning
		sHeader = sHeader.substr(nFind);
		return true;
	}
	nFind = sLower.find("post");
	if (nFind == 0)
	{
		// header locate at the beginning
		return false;
	}
	if (nFind != string::npos)
	{
		// find the header and it's not at the beginning
		sHeader = sHeader.substr(nFind);
		return true;
	}
	nFind = sLower.find("http");
	if (nFind == 0)
	{
		// header locate at the beginning
		return false;
	}
	if (nFind != string::npos)
	{
		// find the header and it's not at the beginning
		sHeader = sHeader.substr(nFind);
		return true;
	}
	return false;
}

// ---------------------------------------------------------------------------
// Member functions
// ---------------------------------------------------------------------------

// Connect to the target server through a proxy connection
// - returns whether or not the connection was successfully made
// - if an error occured due to the proxy connection, bProxyError is modified to be true
// @ bProxyError is true if there is an error connecting to the proxy server
bool HttpMessage::ConnectProxy(
	SOCKET sock,
	const string& sProxyAddr,
	WORD uProxyPort,			// host order
	const string& sAddr,
	WORD uPort,					// host order
	bool& bProxyError)
{
	bProxyError = false;

	struct sockaddr_in sin;
	memset((char *)&sin, 0, sizeof(sin));
	sin.sin_family = AF_INET;
	sin.sin_port = htons(uProxyPort);
	sin.sin_addr.s_addr = LookupHostAddr(sProxyAddr);
	if (!sin.sin_addr.s_addr)
	{
		bProxyError = true;
		return false;
	}

	HANDLE evt = CreateEvent(NULL, FALSE, FALSE, NULL);
	WSAEventSelect(sock, evt, FD_CONNECT);

	// Connect to the proxy server
	char buf[1024];
	sprintf(buf, "%s", sProxyAddr.c_str());

	LOGP("- Connect to proxy server");

	if (::connect(sock, (struct sockaddr *)&sin, sizeof(sin)) != 0)
	{
		int err = WSAGetLastError();
#if 1 //for Linux yamada
		if (err != EWOULDBLOCK && err != EISCONN)
#else
		if (err != WSAEWOULDBLOCK && err != WSAEISCONN)
#endif //for Linux yamada
		{
			int iErrorCode = WSAGetLastError();

			LOGP("- -> Error connecting to proxy server; error code = " << iErrorCode);

			DB("WSA error code " << iErrorCode);
			CLOSE_SOCKET(sock);
			bProxyError = true;
			return false;
		}
	}

	DWORD dwRet = WaitForSingleObject(evt, PROXY_TIMEOUT);
	if (dwRet != WAIT_OBJECT_0)
	{
		if (dwRet == WAIT_TIMEOUT)
		{
			LOGP("- -> Proxy connection timeout");

			DB("Proxy connection timeout");
		}
		bProxyError = true;
		return false;
	}
	else
	{
		WSANETWORKEVENTS netEvents;	
		if (0 != WSAEnumNetworkEvents(sock, evt, &netEvents))
		{
			LOGP("- -> Error network events");

			bProxyError = true;
			return false;
		}
		else
		{
			int iErrorCode = netEvents.iErrorCode[FD_CONNECT_BIT];

			if (0 != iErrorCode)
			{
				LOGP("- -> Error connection return; error code = " << iErrorCode);

				bProxyError = true;
				return false;
			}
		}
	}

	LOGP("- -> Success");
	return true;
}

// Connect to the target server through a proxy connection
// - returns whether or not the connection was successfully made
// - if an error occured due to the proxy connection, bProxyError is modified to be true
// @ bProxyError is true if the proxy cannot be reached; it is false if it does not support HTTPS
// @ returns false if the connection fails
bool HttpMessage::ConnectProxyHttps(
	SOCKET sock,
	const string& sProxyAddr,
	WORD uProxyPort,		// host order
	const string& sAddr,
	WORD uPort,				// host order
	bool& bProxyError)
{
	bProxyError = false;
	if (!ConnectProxy(sock, sProxyAddr, uProxyPort, sAddr, uPort, bProxyError))
	{
		return false;
	}

	HANDLE evt = CreateEvent(NULL, FALSE, FALSE, NULL);
	WSAEventSelect(sock, evt, FD_READ);

	SharedArray<char> pBuf = SharedArray<char>(new char[1024]);
	sprintf(pBuf.get(), "CONNECT %s:%d HTTP/1.1\r\nHOST: %s:%d\r\n\r\n", 
		sAddr.c_str(), uPort, sAddr.c_str(), uPort);

	string sBuf(pBuf.get());

	LOGP("- Sending HTTPS connection request");

	// Send request to the proxy server for connecting to the destinated server
	if (send(sock, sBuf.c_str(), sBuf.size(), 0) != SOCKET_ERROR)
	{	
		DWORD dwRet = WaitForSingleObject(evt, 2000);
		switch (dwRet) {
		case WAIT_OBJECT_0:
			{
				// Receive confirmation message
				int iLen = recv(sock, pBuf.get(), 1024, 0);
				sBuf = pBuf.get();

				LOGP("- -> Proxy server's reply:");
				LOGP("  ->\n{\n" << sBuf << "\n}");

				int iErrorCode = WSAGetLastError();
#if 1 //for Linux yamada
				if (iLen < 0 && iErrorCode != EWOULDBLOCK)
#else
				if (iLen < 0 && iErrorCode != WSAEWOULDBLOCK)
#endif //for Linux yamada

				{
					LOGP("- -> Error occured; error code = " << iErrorCode);

					return false;
				}

				if (iLen > 0)
				{
					if (Unpack(sBuf) != METHOD_CONNECTED)
					{
						LOGP("- -> Message is not a connection confirmation");

						return false;
					}
				}
				break;
			}
		default:
			LOGP("- -> Proxy server is not responding");
			return false;
		}
	}
	else
	{
		LOGP("- -> Error sending connection request; error code = " << WSAGetLastError());

		bProxyError = true;
		return false;
	}

	LOGP("- -> Success");
	return true;
}

void HttpMessage::SetSendMethod(int nSendType)
{
	m_nSendType = nSendType;
}

void HttpMessage::SetProxyProperties(const std::string& sAddr, WORD uPort)
{
	m_sProxyAddr = sAddr;
	m_uProxyPort = uPort;
}

string HttpMessage::GetProxyAddr()
{
	return m_sProxyAddr;
}

WORD HttpMessage::GetProxyPort()
{
	return m_uProxyPort;
}

void HttpMessage::SetServerProperties(const std::string& sAddr, WORD uPort)
{
	m_sServerAddr = sAddr;
	m_uServerPort = uPort;
}

string HttpMessage::GetServerAddr()
{
	return m_sServerAddr;
}

WORD HttpMessage::GetServerPort()
{
	return m_uServerPort;
}

// ---------------------------------------------------------------------------
// Inherited functions
// ---------------------------------------------------------------------------

bool HttpMessage::Open(
	u_int32_t uAddr, 
	u_int16_t uRemotePort, 
	u_int16_t uLocalPort,	// host order
	HANDLE hNotifyEvent)
{
	LOGP("Opening");

	SetServerProperties(LookupHostName(uAddr), uRemotePort);

	// not converting remote port to network order because DoConnect is overriden
	// and it will handle the port conversion
	return TcpMessage::Open(uAddr, uRemotePort, uLocalPort, hNotifyEvent);
}

void HttpMessage::Close()
{
	LOGP("Closing");

	int i;
	for (i=0; i<NUM_PRIORITY; i++)
	{
		m_MsgQ[i].Clear();
		m_nQueueBufLimit[i] = DEFAULT_BUFFER_SIZE;
	}
	m_sPriorityList = "";

	TcpMessage::Close();
}

int HttpMessage::RecvMsg(string& sBuf)
{
	if (!GetRecvHttpHeader())
	{
		// don't need to wrap HTTP header
		return TcpMessage::RecvMsg(sBuf);
	}

	Critical_Section cs(*m_pMutex);

	// receive HTTP message

	sBuf.erase();

	int iRet;
	int len = HTTP_HEADER_LENGTH;
	if (m_sMsgBuf.size() < len)
	{
		// receive buffer has not contained full header method required yet
		len -= m_sMsgBuf.size();
		SharedArray<char> pBuf = SharedArray<char>(new char[len]);
		iRet = Recv(pBuf.get(), len);
		if (iRet == ES_NOT_CONNECTED)
			return ES_NOT_CONNECTED;

		if (iRet <= 0)
		{
			return ES_FAILURE;
		}

		m_sMsgBuf.append(pBuf.get(), iRet);
		if (m_sMsgBuf.size() < len)
		{
			// header not completed
			return ES_FAILURE;
		}
	}

	// receive buffer contains full header method
	int nType = GetHeaderMethod(m_sMsgBuf);

	if (nType == -1) {
		// Bad HTTP header method, which should only happen if non-HTTP message is received

		LOGP("Bad HTTP header received:");
		LOGP("->\n{\n" << m_sMsgBuf << "\n}");

		// tries to locate a HTTP header method
		// - if found, then remove the leading junk and return
		// - otherwise, erase the buffer and return
		if (!TrimToHeaderMethod(m_sMsgBuf))
		{
			LOGP("Cannot locate HTTP header");

			m_sMsgBuf.erase();
		}
		return ES_AGAIN;
	}

	int iHeaderReadSize = 20;
	string sHeaderTerminator = "\r\n\r\n";
	SharedArray<char> pBuf = SharedArray<char>(new char[iHeaderReadSize]);
	int nFindPos;

	string sTmpBuf = "";		// used for storing extra data read
	bool bReceived = false;

	while (!bReceived)
	{
		nFindPos = m_sMsgBuf.find(sHeaderTerminator);
		if (nFindPos != string::npos)
		{
			// finished reading HTTP header
			// m_sMsgBuf contains the whole HTTP header here

			switch (nType)
			{
			case METHOD_POST:
			case METHOD_OK:
				{
					string sLen;
					string sContent("content-length: ");
					string sLower = m_sMsgBuf;
					int nContent = sLower.find(sContent);
					if (nContent == string::npos)
					{
						LOGP("Message received contains no content");

						// this OK message does not contain content
						bReceived = true;
						m_sMsgBuf = m_sMsgBuf.substr(nFindPos + sHeaderTerminator.length());
						break;
					}
					nContent += sContent.length(); // start of content-length
					sLen = m_sMsgBuf.substr(nContent, nFindPos);
					int nContentEndPos = sLen.find("\r\n");
					sLen = sLen.substr(0, nContentEndPos);
					int iLen = atoi(sLen.c_str());								 // content length
					int iTotal = iLen + nFindPos + sHeaderTerminator.length();   // total message length
					iLen = iTotal - m_sMsgBuf.size();							 // length to obtain

					if (iLen < 0)
					{
						// m_sMsgBuf contains more data than the current message;
						// use sTmpBuf to store the extra data
						sTmpBuf = m_sMsgBuf.substr(iTotal);
						m_sMsgBuf = m_sMsgBuf.substr(0, iTotal);
						bReceived = true;
						break;
					}

					if (iLen == 0)
					{
						// m_sMsgBuf contains the exact number of bytes as the current message
						bReceived = true;
						break;
					}

					pBuf.reset(new char[iLen]);
					iRet = Recv(pBuf.get(), iLen);

					if (iRet == ES_NOT_CONNECTED)
					{
						return ES_NOT_CONNECTED;
					}
					if (iRet <= 0)
					{
						return ES_FAILURE;
					}

					if (iRet > 0)
					{
						// more data is received, append it to the buffer
						m_sMsgBuf.append(pBuf.get(), iRet);
					}
					if (m_sMsgBuf.size() < iTotal)
					{
						return ES_FAILURE; // payload not yet available
					}
				}
				bReceived = true;
				break;
			default:
				bReceived = true;
				break;
			}
		}
		else
		{
			// continue reading HTTP header
			iRet = Recv(pBuf.get(), iHeaderReadSize);
			if (iRet == ES_NOT_CONNECTED)
			{
				return ES_NOT_CONNECTED;
			}
			if (iRet <= 0)
			{
				return ES_FAILURE;
			}

			m_sMsgBuf.append(pBuf.get(), iRet);
		}
	}

	sBuf = m_sMsgBuf;
	m_sMsgBuf.erase();
	m_sMsgBuf.assign(sTmpBuf);
	// sBuf currently stores the whole HTTP message

	iRet = Unpack(sBuf);
	// sBuf now stores the message with HTTP header extracted

	if (iRet == ES_FAILURE)
	{
		return ES_FAILURE;
	}

	return ES_SUCCESS;
}

int HttpMessage::SendMsg(const ISerializable& msg)
{
	string sBuf;
	msg.serialize(sBuf);

	return SendMsg(sBuf, 0);
}

int HttpMessage::SendMsg(const string& sBuf)
{
	return SendMsg(sBuf, 0);
}

int HttpMessage::SendMsg(const char *pBuf, int iLen)
{
	string sBuf;
	sBuf.assign(pBuf, iLen);

	return SendMsg(sBuf, 0);
}

int HttpMessage::SendMsg(const ISerializable& msg, int nPriority)
{
	string sBuf;
	msg.serialize(sBuf);

	return SendMsg(sBuf, nPriority);
}

int HttpMessage::SendMsg(const std::string& sBuf, int nPriority)
{
	if (!GetSendHttpHeader())
	{
		// don't need to wrap HTTP header
		return SendPriorityMsg(nPriority, sBuf);
	}

	string sPackBuf(sBuf);
	if (GetSendHttpHeader())
	{
		// need to pack message
		if (Pack(sPackBuf, m_sServerAddr, m_uServerPort, m_nSendType) == ES_FAILURE)
		{
			// error occured in packing HTTP header
			return ES_FAILURE;
		}
	}
	return SendPriorityMsg(nPriority, sPackBuf);
}

int HttpMessage::GetSendBufferSize() const
{
	Critical_Section cs(*m_pMutex);

#ifdef LOG_HTTP_MESSAGE
	int nBufSize;

	if (m_ConnSock == INVALID_SOCKET)
	{
		nBufSize = -1;
	}
	else
	{
		int s = sizeof(nBufSize);
		getsockopt(m_ConnSock,
				SOL_SOCKET, 
				SO_SNDBUF,
				(char*)&nBufSize,
				&s);
	}
	LOGP("Current buffer sizes:");
	LOGP("- Socket buffer size = " << nBufSize);
	int i;
	for (i=0; i<NUM_PRIORITY; i++)
	{
		LOGP("- Buffer queue[" << i << "] buffer size = " << m_nQueueBufLimit[i]);
	}
#endif

	return m_nQueueBufLimit[0];
}

void HttpMessage::SetQueueBufferSize(int iQueue, int iBufSize)
{
	Critical_Section cs(*m_pMutex);

	if (iBufSize != m_nQueueBufLimit[iQueue])
	{
		FlushBuffer(iQueue);
		m_nQueueBufLimit[iQueue] = iBufSize;
	}

	// add space to the video buffer, so that the buffer does not get overflow but stops video
	// from capturing instead
	if (iQueue == HTTP_VIDEO_PRIORITY)
	{
		m_nQueueBufLimit[iQueue] += VIDEO_BUFFER_EXTRA;
	}
}

int HttpMessage::GetQueueBufferSize(int iQueue)
{
	Critical_Section cs(*m_pMutex);

	return m_nQueueBufLimit[iQueue];
}

bool HttpMessage::AcceptData()
{
	// returns true if video buffer is not too full and audio buffer is less than 1/2 full

	if (!AcceptBufferData(HTTP_VIDEO_PRIORITY, VIDEO_BUFFER_EXTRA))
	{
		LOGP("Video send buffer is too full (" << m_MsgQ[HTTP_VIDEO_PRIORITY].Size() << 
			" bytes) -- Video buffer is not accepting new data");
		return false;
	}
	if (m_nQueueBufLimit[HTTP_AUDIO_PRIORITY] > 0 && 
		!AcceptBufferData(HTTP_AUDIO_PRIORITY, m_nQueueBufLimit[HTTP_AUDIO_PRIORITY]/2))
	{
		LOGP("Audio send buffer is too full (" << m_MsgQ[HTTP_AUDIO_PRIORITY].Size() << 
			" bytes) -- Video buffer is not accepting new data");
		return false;
	}

	return true;
}

bool HttpMessage::DoConnect(u_int32_t uRemoteAddr, u_int16_t uRemotePort)
{
	LOGP("Connect");

	m_bProxyError = false;
	if (m_sProxyAddr.length() == 0 || m_uProxyPort == 0)
	{
		// no proxy server setting
		LOGP("Non-proxy connection");
		if (TcpMessage::DoConnect(uRemoteAddr, (u_int16_t) uRemotePort))
		{
			LOGP("-> Success");
			return true;
		}
		return false;
	}

	LOGP("Proxy properties: [" << m_sProxyAddr << ":" << m_uProxyPort << "]");

	string sServiceAddress = m_sServerAddr;
	if (m_sServerAddr.length() == 0 || m_uServerPort == 0)
	{
		// server info not set
		sServiceAddress = LookupHostName(uRemoteAddr);
		m_sServerAddr = sServiceAddress;
		m_uServerPort = uRemotePort;
		LOGP("Server info not set; using [" << m_sServerAddr << ":" << m_uServerPort << "]");
	}
	else
	{
		// server info already set
		uRemotePort = m_uServerPort;
		LOGP("Server info; using [" << m_sServerAddr << ":" << m_uServerPort << "]");
	}

	bool bConnected = false;
	if (!m_bUseHttps)
	{
		LOGP("non-HTTPS proxy connection");

		// HTTP proxy connection
		bConnected = ConnectProxy(m_ConnSock, m_sProxyAddr, m_uProxyPort, 
			sServiceAddress, uRemotePort, m_bProxyError);
	}
	else
	{
		LOGP("HTTPS proxy connection");

		// HTTPS proxy connection
		bConnected = ConnectProxyHttps(m_ConnSock, m_sProxyAddr, m_uProxyPort, 
			sServiceAddress, uRemotePort, m_bProxyError);	
	}

	if (!bConnected)
	{
		LOGP("-> Not connected");

		return false;
	}

	SetNotifyEvent(m_hNotifyEvent);

	m_bCloseReceived = false;
	m_dwConnectTime = timeGetTime();
	// Waiting for connect event...

	return true;
}

void HttpMessage::AppendToSendBuffer(const std::string& sBuf)
{
	AppendToSendBuffer(0, sBuf);
}

bool HttpMessage::AppendToSendBuffer(int nPriority, const std::string& sBuf)
{
	Critical_Section cs(*m_pMutex);

	if (m_MsgQ[nPriority].Size() + sBuf.size() > m_nQueueBufLimit[nPriority])
	{
		// buffer is full

#if 0
		if (nPriority >= FLUSH_BUFFER_PRIORITY)
		{
			// priority beyond FLUSH_BUFFER_PRIORITY will be flushed if its buffer size is full
			FlushBuffer(nPriority);
		}
#endif

		if (nPriority == HTTP_AUDIO_PRIORITY)
		{
			// drop audio packet if the buffer is full
			LOGP("Packet lost: send buffer is too full (" << m_MsgQ[nPriority].Size() << 
				" bytes) to add msg (" << sBuf.size() << " bytes); priority = " << nPriority);

			if (m_MsgQ[HTTP_VIDEO_PRIORITY].Size() == VIDEO_BUFFER_EXTRA)
			{
				// video buffer is not being used
				FlushBuffer(nPriority);
				LOGP("Flushing audio buffer");
			}
			return false;
		}

		if (nPriority == HTTP_VIDEO_PRIORITY)
		{
			// do not discard video packets even if full
			LOGP("Video buffer is full");
		}

		if (nPriority == HTTP_WHITEBOARD_PRIORITY)
		{
			// do not discard whiteboard packets even if full
			LOGP("Whiteboard buffer is full");
		}
	}

	// new data can be added to the queue
	m_MsgQ[nPriority].AppendPacket(sBuf);

	// update the priority list
	m_sPriorityList += itoa(nPriority);

	return true;
}

bool HttpMessage::IsSendBufferEmpty(void)
{
	Critical_Section cs(*m_pMutex);

	int i;
	for (i=0; i<NUM_PRIORITY; i++)
	{
		if (!m_MsgQ[i].Empty())
		{
			return false;
		}
	}

	return true;
}

bool HttpMessage::GetSendBufferPacket(std::string& sBuf)
{
	return GetSendBufferPacket(0, sBuf);
}

bool HttpMessage::GetSendBufferPacket(int iQueue, std::string& sBuf)
{
	Critical_Section cs(*m_pMutex);

	return m_MsgQ[iQueue].GetPacket(sBuf);
}

void HttpMessage::RemoveSendBufferBytes(unsigned int nLen)
{
	RemoveSendBufferBytes(0, nLen);
}

void HttpMessage::RemoveSendBufferBytes(int iQueue, unsigned int nLen)
{
	Critical_Section cs(*m_pMutex);

	m_MsgQ[iQueue].RemoveBytes(nLen);
}

int	HttpMessage::SendPriorityMsg(int nPriority, const string& sBuf)
{
	Critical_Section cs(*m_pMutex);

	if (m_ConnSock == INVALID_SOCKET)
	{
		DB("socket is invalid");
		return ES_NOT_CONNECTED;
	}

	if (nPriority >= NUM_PRIORITY)
	{
		nPriority = NUM_PRIORITY-1;
	}
	if (nPriority < 0)
	{
		nPriority = 0;
	}

	if (sBuf.size() > m_nQueueBufLimit[nPriority])
	{
		LOGP("Unexpectedly long message to be sent -- rejected; priority=" << nPriority);

		// the msg itself is longer than the buffer size
		return ES_FAILURE;
	}

	bool bAppended = AppendToSendBuffer(nPriority, sBuf);

	if (!SendBufferedData())
	{
		int iErr = WSAGetLastError();
		DB_ASSERT(0 != iErr);
#if 1 //for Linux yamada
		if (EWOULDBLOCK != iErr)
#else
		if (WSAEWOULDBLOCK != iErr)
#endif //for Linux yamada
		{
			DB("socket send error " << iErr);
			return ES_NOT_CONNECTED;
		}
	}

	if (!bAppended)
	{
		// msg was not appended to the queue
		return ES_FAILURE;
	}

	return ES_SUCCESS;
}

bool HttpMessage::SendBufferedData(void)
{
	Critical_Section cs(*m_pMutex);

	// Continue to send data from the buffer queue until either the queue is empty or sending
	// socket blocks
	string sData;
	while (m_sPriorityList.length() > 0 || m_sBufMsg.size() > 0)
	{
		if (m_sBufMsg.size() == 0)
		{
			if (!GetPriorityPacket(m_sBufMsg))
			{
				return false;
			}
		}

		// m_sBufMsg contains the data to be sent

		// manually set the last error to 0, since we rely on this value below
		WSASetLastError(0);

		int iRet = DoSend(m_sBufMsg.data(), m_sBufMsg.size());

		if (iRet > 0)
		{
			int nErase = MIN((int)m_sBufMsg.size(), iRet);
			sData.assign(m_sBufMsg.substr(nErase));
			m_sBufMsg.assign(sData);

			// continue sending
		}
		else
		{
			int iErr = WSAGetLastError();
			if ((iErr == 0) && (iRet == ES_NOT_CONNECTED))
			{
				// map ES return code to winsock error code
#if 1 //for Linux yamada
				iErr = ENOTCONN;
#else
				iErr = WSAENOTCONN;
#endif //for Linux yamada
			}

			DB_ASSERT(iErr != 0);
#if 1 //for Linux yamada 
			if (EWOULDBLOCK == iErr)
#else
			if (WSAEWOULDBLOCK == iErr)
#endif //for Linux yamada
			{
				LOGP("Sending socket is blocked -- priority list remains: " << m_sPriorityList);
				return true;
			}

#if 1 //for Linux yamada
			if ((ENOTCONN == iErr) && 
#else
			if ((WSAENOTCONN == iErr) && 
#endif //for Linux yamada
				(timeGetTime() < m_dwConnectTime + HTTP_CONNECT_TIMEOUT))
			{
				// still waiting for connection
				return true;
			}

			return false;
		}
	}
	
	return true;
}

int HttpMessage::GetTotalBufferSize()
{
	int i;
	int nSum = 0;
	for (i=0; i<NUM_PRIORITY; i++)
	{
		nSum += m_MsgQ[i].Size();
	}

	return nSum;
}

int HttpMessage::GetCurPriority(int& index)
{
	if (m_sPriorityList.length() == 0)
	{
		return -1;
	}

#if 1
	int i;
	int nFound;
	for (i=0; i<NUM_PRIORITY; i++)
	{
		nFound = m_sPriorityList.find(itoa(i));
		if (nFound != string::npos)
		{
			// the current priority i is found in the list;
			index = nFound;
			return i;
		}
	}

	// m_sPriorityList is not valid
	return -1;
#else
	int nCurrPriority = m_sPriorityList[0] - '0';
	if (nCurrPriority < 0)
	{
		return -1;
	}
	return nCurrPriority;
#endif
}

bool HttpMessage::RemovePriority(int nIndex)
{
	if (m_sPriorityList.length() <= nIndex)
	{
		return false;
	}

	if (nIndex == 0)
	{
		m_sPriorityList = m_sPriorityList.substr(1);
		return true;
	}

	if (nIndex == m_sPriorityList.length()-1)
	{
		m_sPriorityList = m_sPriorityList.substr(0, nIndex);
		return true;
	}

	string sTail = m_sPriorityList.substr(nIndex+1);
	string sHead = m_sPriorityList.substr(0, nIndex);
	m_sPriorityList = sHead + sTail;

	return true;
}

bool HttpMessage::GetPriorityPacket(string& sBuf)
{
	int index;
	int nCurrPriority = GetCurPriority(index);
	if (nCurrPriority == -1)
	{
		LOGP("Error: priority list contains an error value");
		return false;
	}

	string sData;

	// obtain data from buffer
	if (!m_MsgQ[nCurrPriority].GetPacket(sData))
	{
		LOGP("Error: Empty message queue (" << nCurrPriority << 
			") to obtain data from; priority list: " << m_sPriorityList);
		return false;
	}
	
	// remove the data from the msg queue
	m_MsgQ[nCurrPriority].RemoveBytes(sData.size());

	// remove the priority from the list
	RemovePriority(index);

	// put the data to the temporary to-send buffer
	sBuf.assign(sData);

	return true;
}

void HttpMessage::FlushBuffer(int nPriority)
{
	LOGP("Buffer of priority " << nPriority << " is flushed");
	m_MsgQ[nPriority].Clear();

	string sPriority = itoa(nPriority);
	int nFind = m_sPriorityList.find(sPriority);
	while (nFind != string::npos)
	{
		if (!RemovePriority(nFind))
		{
			break;
		}
		nFind = m_sPriorityList.find(sPriority);
	}
}

bool HttpMessage::AcceptBufferData(int nPriority, int nSize)
{
	Critical_Section cs(*m_pMutex);

	return (m_MsgQ[nPriority].Size() + nSize < m_nQueueBufLimit[nPriority]);
}

bool HttpMessage::IsProxyConnectionError()
{
	return m_bProxyError;
}

void HttpMessage::SetHttpsUsage(bool bUseHttps)
{
	m_bUseHttps = bUseHttps;
}

bool HttpMessage::GetHttpsUsage()
{
	return m_bUseHttps;
}

void HttpMessage::SetSendHttpHeader(bool bSendHttpHeader)
{
	m_bSendHttpHeader = bSendHttpHeader;
}

bool HttpMessage::GetSendHttpHeader()
{
	return m_bSendHttpHeader;
}

void HttpMessage::SetRecvHttpHeader(bool bRecvHttpHeader)
{
	m_bRecvHttpHeader = bRecvHttpHeader;
}

bool HttpMessage::GetRecvHttpHeader()
{
	return m_bRecvHttpHeader;
}

void HttpMessage::SetLongHttpHeader(bool bLongHttpHeader)
{
	m_bLongHeader = bLongHttpHeader;
}

bool HttpMessage::GetLongHttpHeader()
{
	return m_bLongHeader;
}

