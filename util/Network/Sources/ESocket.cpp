// ---------------------------------------------------------------------------
// File:       ESocket.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Thread-aware wrapper class for sockets
// 
// Change Log:
// ---------------------------------------------------------------------------

#include "Base.h"  // automatic use of precompiled headers 

#if 1 //for Linux yamada
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/time.h>
#include <unistd.h>
#include<poll.h>
#include "critsec.h"
#include "debug.h"
#include "ESocket.h"
#include "ENetwork.h"
#include "Event.h"
#include "ReturnCodes.h"
#include "EUtil.h"
#include "Timer.h"
#include "registry.h"
#include "stunMessage.h"
#include "TurnMessage.h"
#include <cstdio>
#else
#ifndef _WIN32_WCE
#include <ws2tcpip.h> //IP_TTL
#endif

#include "critsec.h"
#include "debug.h"
#include "ESocket.h"
#include "ENetwork.h"
#include "Event.h"
#include "ReturnCodes.h"
#include "EUtil.h"
#include "Timer.h"
#include "Registry.h"

#endif //for Linux

using std::string;
using std::queue;
using namespace eyeballTurn;

// ---------------------------------------------------------------------------
// Definitions and Constants
// ---------------------------------------------------------------------------
#if 1 //for Linux yamada
#define SOCKET_ERROR (-1)
#define WSAGetLastError() (errno)
#endif //for linux yamada


#define DISABLE_SENDING (false)

#ifdef _DEBUG
//#define DEBUG_SOCKET
#endif

// Period to wait after sending a UDP conduit opening packet. 
// For most firewalls, no delay is required.
#define OPEN_CONDUIT_DELAY (50)
//#define OPEN_CONDUIT_DELAY (2) //modified by D


// ---------------------------------------------------------------------------
// ESocket Implementation
// ---------------------------------------------------------------------------

ESocket::ESocket(int af, int type, int protocol)
:
	m_iAf(af),
	m_iType(type),
	m_iProtocol(protocol),
	m_sock(INVALID_SOCKET),
	m_pMutex(new Mutex),
	m_uRemoteAddr(0),
	m_uRemotePort(0),
	m_DetectedLocalAddr(0, 0),
	m_bSendDisabled(DISABLE_SENDING)
{
	CreateSocket();

#ifdef _DEBUG
	m_nDebugCounter = 0;
#endif //_DEBUG
}

ESocket::~ESocket()
{
	CLOSE_SOCKET(m_sock);
}

void ESocket::CreateSocket()
{
	Critical_Section cs(*m_pMutex);
	if (m_sock != INVALID_SOCKET)
		return;

	m_sock = socket(m_iAf, m_iType, m_iProtocol);

	if (m_sock == INVALID_SOCKET)
	{
		DB("Unable to create socket");
		return;
	}

	// Set socket to non-blocking
	Network::Nonblock(m_sock);

	// Ensure we can open this port again if the program is restarted
	//Network::Reuse(m_sock);

}

void ESocket::CloseSocket()
{
	Critical_Section cs(*m_pMutex);

	CLOSE_SOCKET(m_sock);
}

void ESocket::AttachSocket(SOCKET sock)
{
	Critical_Section cs(*m_pMutex);

	CLOSE_SOCKET(m_sock);
	m_sock = sock;
}

void ESocket::DetachSocket(void)
{
	Critical_Section cs(*m_pMutex);

	m_sock = INVALID_SOCKET;
}

SOCKET ESocket::GetSocket(void)
{
	return m_sock;
}

Mutex& ESocket::GetMutex(void)
{
	return *m_pMutex;
}

int ESocket::Bind(u_int32_t uAddr, u_int16_t uPort)
{
	// port in network bytes order

	Critical_Section cs(*m_pMutex);

	DWORD dwPortMin = 0;
	DWORD dwPortMax = 0;
	DWORD dwPortLast = 0;
	bool bStaticPort = false;

	if (uPort == 0)
	{
		DWORD dwNetworkOptions = 0;
#if 0
		EcGetRegistryEntry(REG_NETWORK_PORT_MIN, dwPortMin);
		EcGetRegistryEntry(REG_NETWORK_PORT_MAX, dwPortMax);
		EcGetRegistryEntry(REG_NETWORK_PORT_LAST, dwPortLast);
		EcGetRegistryEntry(REG_NETWORK_OPTIONS, dwNetworkOptions);		
#else
		dwPortMin = LINUX_REG_NETWORK_PORT_MIN;
		dwPortMax = LINUX_REG_NETWORK_PORT_MAX;
		dwPortLast = LINUX_REG_NETWORK_PORT_LAST;
		dwNetworkOptions = LINUX_REG_NETWORK_OPTIONS;
#endif
		bool bDynamic = (dwNetworkOptions & NETWORK_DYNAMIC_PORT) != 0;
		if (dwPortMin != 0 && dwPortMin < dwPortMax && !bDynamic)
		{
			if ((dwPortLast < dwPortMin) || (dwPortLast > dwPortMax))
				dwPortLast = dwPortMin;

			bStaticPort = true;
		}
	}

	struct sockaddr_in sin;
	memset((char*)&sin, 0, sizeof(sin));
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = uAddr;
	//sin.sin_port = uPort;
	
	int iBind = 0;
	const int iMaxBind = 20;
	while (iBind < iMaxBind)
	{
		sin.sin_port = uPort;

		if (iBind == iMaxBind - 1)
		{
			uPort = 0;
			sin.sin_port = 0;
			DB("Using dynamic port...");
			//cout << "Using dynamic port...";
		}
		else if (bStaticPort)
		{
			sin.sin_port = uPort = htons((WORD)dwPortLast);
			dwPortLast++;
			if (dwPortLast > dwPortMax)
				dwPortLast = dwPortMin;
		}

		if (::bind(m_sock, (sockaddr*)&sin, sizeof(sin)) == 0)
		{
			DB("Socket bound to port : " << ntohs(uPort));
			break;
		}
		
		DB("Failed to bind port : " << ntohs(uPort));
		//cout << "Failed to bind port : " << ntohs(uPort);

		iBind++;
		uPort++;
	}

	if (bStaticPort)
	{
		EcSetRegistryEntry(REG_NETWORK_PORT_LAST, dwPortLast);
	}
	//cout<<"\t\t\tPort found: "<<ntohs(uPort)<<endl<<endl;
	return ES_SUCCESS;
}

int ESocket::SetRemote(u_int32_t uAddr, u_int16_t uPort)
{
	Critical_Section cs(*m_pMutex);

	m_uRemoteAddr = uAddr;
	m_uRemotePort = uPort;

	return ES_SUCCESS;
}


// Opens a UDP conduit (hole) through most firewalls
int ESocket::OpenConduit(u_int32_t uAddr, u_int16_t uPort, int nTempTtl, const string& sAppData)
{
	Critical_Section cs(*m_pMutex);

	int iOldTtl = 0;
	if (nTempTtl > 0)
	{
		iOldTtl = GetTtl();
		SetTtl(nTempTtl);
	}

	// one packet should be enough - loss rate to the firewall is near-zero.
	for (int i = 0; i < 2; i++)
	{
		DB("in ESock: OpenCondurit: rem. addr: "<<LookupHostName(htonl(uAddr))<<" port "<<uPort);
		SendTo(sAppData.c_str(), sAppData.size(), uAddr, uPort);
	}

	//string sRemote = Network::ToString(AddressPortPair(uAddr, uPort));
	//ERR_MSG("Opening conduit from " + ToString() + " to " + sRemote);

	if (nTempTtl > 0)
	{
		SetTtl(iOldTtl);
	}

	//sleep(OPEN_CONDUIT_DELAY);
	usleep(OPEN_CONDUIT_DELAY*1000);

	return ES_SUCCESS;
}



// Opens a UDP conduit (hole) through most firewalls
int ESocket::OpenConduit(u_int32_t uAddr, u_int16_t uPort, int nTempTtl)
{
	Critical_Section cs(*m_pMutex);

	int iOldTtl = 0;
	if (nTempTtl > 0)
	{
		iOldTtl = GetTtl();
		SetTtl(nTempTtl);
	}

	char pBuf[4];
	memset(pBuf, 0, sizeof(pBuf));
	pBuf[0] = UDP_PT_FIREWALL;

	// one packet should be enough - loss rate to the firewall is near-zero.
	for (int i = 0; i < 2; i++)
		SendTo(pBuf, sizeof(pBuf), uAddr, uPort);

	//string sRemote = Network::ToString(AddressPortPair(uAddr, uPort));
	//ERR_MSG("Opening conduit from " + ToString() + " to " + sRemote);

	if (nTempTtl > 0)
	{
		SetTtl(iOldTtl);
	}

#if 1 //for Linux yamada 
	#ifndef SLEEP_DISABLE
		usleep(OPEN_CONDUIT_DELAY*1000);
	#else
		usleep(OPEN_CONDUIT_DELAY * 10);
	#endif
#else 
	Sleep(OPEN_CONDUIT_DELAY);
#endif //for Linux yamada

	return ES_SUCCESS;
}


int ESocket::OpenConduit(
	u_int32_t uRemoteAddr,
	u_int16_t uRemotePort,
	u_int32_t uLocalAddr, // use as identity
	u_int16_t uLocalPort, // use as identity
	int nTempTtl)
{
	Critical_Section cs(*m_pMutex);

	int iOldTtl = 0;
	if (nTempTtl > 0)
	{
		iOldTtl = GetTtl();
		SetTtl(nTempTtl);
	}
	
	char sz[8];
	sz[0] = UDP_PT_FIREWALL;
	memcpy(sz+1, &uLocalAddr, sizeof(u_int32_t));
	memcpy(sz+5, &uLocalPort, sizeof(u_int16_t));
	sz[7] = '\0';

	// one packet should be enough - loss rate to the firewall is near-zero.
	for (int i = 0; i < 2; i++)
		SendTo(sz, sizeof(sz), uRemoteAddr, uRemotePort);

	//string sRemote = Network::ToString(AddressPortPair(uRemoteAddr, uRemotePort));
	//ERR_MSG("Opening conduit from " + ToString() + " to " + sRemote);
	//DB("Open conduits from addr: " << (LookupHostName(uLocalAddr)).c_str() << ", port: " << ntohs(uLocalPort));
	//DB("Open conduits to addr: " << (LookupHostName(uRemoteAddr)).c_str() << ", port: " << ntohs(uRemotePort));

	if (nTempTtl > 0)
	{
		SetTtl(iOldTtl);
	}

#if 1 //for Linux yamada
#ifndef SLEEP_DISABLE
	usleep(OPEN_CONDUIT_DELAY*1000);
#else
	usleep(OPEN_CONDUIT_DELAY * 10);
#endif
#else
	Sleep(OPEN_CONDUIT_DELAY);
#endif //for Linux yamada

	return ES_SUCCESS;
}


int ESocket::Send(const char* pBuf, int iLen)
{
	Critical_Section cs(*m_pMutex);

	return SendTo(pBuf, iLen, m_uRemoteAddr, m_uRemotePort);
}

int ESocket::SendTo(const char* pBuf, int iLen, u_int32_t uAddr, u_int16_t uPort)
{
	Critical_Section cs(*m_pMutex);

	//printf ("ESocket::SendTo \n");
	
	if (!uAddr || !uPort)
	{
		//DB_ASSERT(0 && "Invalid destination");
		return 0;
	}

	if (iLen < 0)
		return 0;

	sockaddr_in sin;
	memset((char*)&sin, 0, sizeof(sin));
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = uAddr;
	sin.sin_port = uPort;

#ifdef DEBUG_SOCKET
#pragma _ebWarning("socket debugging is enabled") 
	m_nDebugCounter++;
	if ((m_nDebugCounter < 10) || (m_nDebugCounter % 100 == 0))
	{
		DB("ESocket: Sending packet from " <<
			Network::ToString(Network::GetSockName(m_sock)) <<
			" to " << intoa(uAddr).c_str() << ":" << ntohs(uPort));
	}
#endif

	if (m_bSendDisabled)
	{
		// return success, but don't really send
		return iLen;
	}
	//printf ("ESocket::SendTo \n");
//	if(iLen>1312||iLen<12)
//		printf("\n\n\n\nWe have got a Send to function call with an odd length = %d=====================================================\n\n\n\n\n",iLen);
	if(!pBuf)
		printf("\n\n\n\nWe have got a Send to function call with a null pointer to buffer ==============================================\n\n\n\n\n",iLen);
	
	//free(p);
	
	return ::sendto(m_sock, pBuf, iLen, 0, (sockaddr*)&sin, sizeof(sin));
}


int ESocket::SendTo(const char *pBuf, int iLen, AddressPortPair& addr)
{
	Critical_Section cs(*m_pMutex);

	return SendTo(pBuf, iLen, addr.first, addr.second);
}

int ESocket::SendTo(const std::string& sBuf, AddressPortPair addr)
{
	Critical_Section cs(*m_pMutex);

	return SendTo(sBuf.data(), sBuf.size(), addr.first, addr.second);
}

int ESocket::Recv(char *pBuf, int iLen)
{
	Critical_Section cs(*m_pMutex);

	u_int32_t uAddr = 0;
	u_int16_t uPort = 0;

	int iRet = RecvFrom(pBuf, iLen, uAddr, uPort);

	if (uAddr != m_uRemoteAddr)
	{
		DB("Received packet from unexpected address: " << intoa(uAddr).c_str());
		return -1;
	}

	if (uPort != m_uRemotePort)
	{
		DB("Received packet from unexpected port: " << ntohs(uPort));
		return -1;
	}

	return iRet;
}

int ESocket::RecvFrom(char *pBuf, int iLen, u_int32_t& uAddr, u_int16_t& uPort)
{
	Critical_Section cs(*m_pMutex);

	int ret(0);

	sockaddr_in sFrom;
	memset((void*)&sFrom, 0, sizeof(sFrom));

	int fromlen = sizeof(sFrom);

	ret = ::recvfrom(m_sock, pBuf, iLen, 0, 
		(sockaddr*)&sFrom, (socklen_t *)&fromlen);

	uAddr = sFrom.sin_addr.s_addr;
	uPort = sFrom.sin_port;

	if (ret < 0)
	{
#ifdef DEBUG_SOCKET
		int iErr = WSAGetLastError();
		if (iErr != WSAEWOULDBLOCK)
		{
			// Note: DB inside a critical section can cause deadlock in debug mode.
			DB("ESocket: Receive-error from " << intoa(uAddr).c_str() << ":" << ntohs(uPort) <<
				" on " << Network::ToString(Network::GetSockName(m_sock)));
			DB("ESocket - recvfrom failed, error code: " << iErr);
		}
#endif //DEBUG_SOCKET
		return ret;
	}

#ifdef DEBUG_SOCKET
	m_nDebugCounter++;
	if ((m_nDebugCounter < 10) || (m_nDebugCounter % 100 == 0))
	{
		DB("ESocket: Received packet from " << intoa(uAddr).c_str() << ":" << ntohs(uPort) <<
			" on " << Network::ToString(Network::GetSockName(m_sock)));
	}
#endif

	return ret;
}

u_int32_t ESocket::GetLocalAddr(void)
{
	Critical_Section cs(*m_pMutex);

	return Network::GetSockName(m_sock).first;
}

u_int16_t ESocket::GetLocalPort(void)
{
	Critical_Section cs(*m_pMutex);
	u_int16_t ret = Network::GetSockName(m_sock).second;
	return ret;
}

AddressPortPair ESocket::GetLocalAddrPort(void)
{
	Critical_Section cs(*m_pMutex);

	return Network::GetSockName(m_sock);
}

u_int16_t ESocket::GetDetectedLocalPort(void)
{
//	if( m_DetectedLocalAddr.second !=0)  //added by D
		return m_DetectedLocalAddr.second;

//	return GetLocalPort();  //added by D
}

//added by D
u_int32_t ESocket::GetDetectedLocalAddr(void)
{
DB("in ESocket:GetDetectedLocalAddr: "<<LookupHostName(m_DetectedLocalAddr.first));

//	if( m_DetectedLocalAddr.first !=0)  
		return m_DetectedLocalAddr.first;

//	AddressPortPair add = GetLocalAddrPort();
//	return add.first;

}




u_int32_t ESocket::GetRemoteAddr(void)
{
	Critical_Section cs(*m_pMutex);

	return m_uRemoteAddr;
}

u_int16_t ESocket::GetRemotePort(void)
{
	Critical_Section cs(*m_pMutex);

	return m_uRemotePort;
}

AddressPortPair ESocket::GetRemoteAddrPort(void)
{
	Critical_Section cs(*m_pMutex);

	return AddressPortPair(m_uRemoteAddr, m_uRemotePort);
}

string ESocket::ToString(void)
{
	Critical_Section cs(*m_pMutex);

	return Network::ToString(Network::GetSockName(m_sock));
}

bool ESocket::DetectLocalAddress(
	const AddressPortPair& addrEchoServer,
	AddressPortPair& addrLocal,
	unsigned int uTimeout)
{
	Critical_Section cs(*m_pMutex);

	if (m_DetectedLocalAddr.first != 0)
	{
		// use cached value
		addrLocal = m_DetectedLocalAddr;
		return true;
	}

	SimpleEvent netEvent;
	netEvent.SetHandle(CreateEvent(NULL, FALSE, FALSE, NULL));
	HANDLE hEvent = netEvent.GetHandle();

	if (WSAEventSelect(m_sock, hEvent, FD_READ | FD_CLOSE) != 0)
	{
		DB_ASSERT(0 && "WSAEventSelect failed");
		return false;
	}

	for (int i = 0; i < 2; i++)
	{
		// Send echo request
		AddressEchoMessage msg;
		string sBuf;
		msg.serialize(sBuf);
		SendTo(sBuf, addrEchoServer);
	}

	// Note: don't return without unselecting the event
	bool bRet = false;

	DWORD dwStart = timeGetTime();
	while (true)
	{
		// handle multiple packets - may be data ack's, etc.
		DWORD dwWait = WaitForSingleObject(hEvent, uTimeout);
		if (WAIT_OBJECT_0 == dwWait)
		{
			string sBuf;
			sBuf.resize(1024);
			u_int32_t uAddr = 0;
			u_int16_t uPort = 0;
			if (RecvFrom(&sBuf[0], sBuf.size(), uAddr, uPort) < 0)
			{
				// receive error
				DB("DetectLocalAddress receives error: " << WSAGetLastError());
				bRet = false;
				break;
			}

			AddressEchoMessage msg;
			if ((AddressPortPair(uAddr, uPort) == addrEchoServer) &&
				(msg.deserialize(sBuf) >= 0))
			{
				addrLocal.first = htons(msg.m_u32IpAddress);
				addrLocal.second = htons(msg.m_u16IpPort);

				// save for later use
				m_DetectedLocalAddr = addrLocal;

				bRet = true;
				break;
			}
		}
		
		if (timeGetTime() - dwStart >= uTimeout)
			break;
	}

	WSAEventSelect(m_sock, hEvent, 0);

	return bRet;
}

int ESocket::GetTtl(void)
{
	Critical_Section cs(*m_pMutex);

	int nTtl = 0;
	int nLen = sizeof(nTtl);

	int iRet = getsockopt(
		m_sock, 
		IPPROTO_IP, 
		IP_TTL, 
		(char*)&nTtl,
#if 1 //for Linux yamada
	(socklen_t*)&nLen);
#else
        &nLen);
#endif //for linux yamada
	if (SOCKET_ERROR == iRet)
	{
		int iErr = WSAGetLastError();
		DB("Failed to get TTL: error code " << iErr);
		DB_ASSERT(0);
	}

	return nTtl;
}

bool ESocket::SetTtl(int nTtl)
{
	Critical_Section cs(*m_pMutex);

	// use negative TTL to disable sending
	m_bSendDisabled = (nTtl < 0);

	if (0 == nTtl)
		return false;

	int iRet = setsockopt(
		m_sock, 
		IPPROTO_IP, 
		IP_TTL, 
		(const char*)&nTtl,
        sizeof(nTtl));
	if (SOCKET_ERROR == iRet)
	{
		int iErr = WSAGetLastError();
		DB("Failed to set TTL: error code " << iErr);
		DB_ASSERT(0);
		return false;
	}

	return true;
}



//_______________________added by D_______________________________________

int ESocket::InternalDetectFirewall(AddressPortPair& addrServer)
{
	WORD wLocalPort = GetLocalPort();
	if (wLocalPort == 0)
		return FIREWALL_TYPE_UNKNOWN;

	if (addrServer.first == 0 || addrServer.second == 0)
		return FIREWALL_TYPE_UNKNOWN;

	AddressPortPair addrSecondServer;
	// detected address is stored in m_DetectedLocalAddr

	int i = InternalDetectFirewall(addrServer, addrSecondServer);
//______added 20_11_________________
 DB("InternalDetectFirewall: after first check: i =  "<< i);
        DB("InternalDetectFirewall addr: "<<LookupHostName(addrServer.first)<<"port: "<<ntohs(addrServer.second));
        DB("InternalDetectFirewall addrSecond: "<<LookupHostName(addrSecondServer.first)<<"port: "<<ntohs(addrSecondServer.second));

//_________________________________
	if (i == FIREWALL_TYPE_UNKNOWN || i == FIREWALL_TYPE_NONE ||
		i == FIREWALL_TYPE_NAT)
	{
		return i;
	}


	if (addrSecondServer.first == 0 || addrSecondServer.second == 0)
	{
		DB("Invalid second stun server.");
		return i;
	}

	// save the first detection result

	AddressPortPair addr1 = m_DetectedLocalAddr;

	// start a second test

//_____________added 23_11
//        AddressPortPair temp(	ntohl(LookupHostAddr("216.187.87.83")), 3478);
	
//	int j = InternalDetectFirewall(temp,temp);
//___________________________________________

	int j = InternalDetectFirewall(addrSecondServer, addrSecondServer);
//_____________added 20_11_____________
 DB("InternalDetectFirewall: after second check: j =  "<< j);
        DB("in InternalDetectFirewall addr: "<<LookupHostName(addrServer.first)<<"port: "<<ntohs(addrServer.second));
        DB("in InternalDetectFirewall addrSecond: "<<LookupHostName(addrSecondServer.first)<<"port: "<<ntohs(addrSecondServer.second));

//_____________________________________

	if (j == FIREWALL_TYPE_UNKNOWN || j == FIREWALL_TYPE_NONE || 
		j == FIREWALL_TYPE_NAT)
	{
//byD		DB_ASSERT(0);
//		return i;
		return j;
	}

	if (addr1.second == m_DetectedLocalAddr.second)
	{
		DB("PAT firewall detected.");
		return FIREWALL_TYPE_PAT;
	}

	int iIncr = ntohs(m_DetectedLocalAddr.second) - ntohs(addr1.second);

	if (iIncr == 1)
	{
		m_DetectedLocalAddr.second = htons(ntohs(m_DetectedLocalAddr.second) + 1);
		return FIREWALL_TYPE_PAT_PLUS;
	}

	DB("PAT firewall port is incremented by: " << iIncr);
	return FIREWALL_TYPE_SYMMETRIC;


return 0;

}

/*

int ESocket::InternalDetectFirewall(
	AddressPortPair& addrServer,
	AddressPortPair& addrSecondServer)
{
	CStunMessage msg;
	string sBuf = msg.CreateBindingRequest();

	struct timeval tv;
	tv.tv_sec = 0;
	tv.tv_usec = 100*1000; // 100 ms 

	int iCount = 0;
	bool bErrorContinue = false;
	while (iCount < 7)
	{
		SendTo(sBuf, addrServer);

		if (!bErrorContinue)
		{
			if (iCount == 0)
			{
				tv.tv_usec = 100*1000; // 100 ms 
			}
			else

			{
				tv.tv_usec *= 2;
				if (tv.tv_usec > 1600 * 1000) // 1600 ms
					tv.tv_usec = 1600 * 1000;
			}
		}

		fd_set fdSet; 
		FD_ZERO(&fdSet);
		FD_SET(m_sock, &fdSet);
		int err = select(NULL, &fdSet, NULL, NULL, &tv);
		if (err == SOCKET_ERROR)
		{
			int e = WSAGetLastError();
			DB("Error on select: " << e);
			return FIREWALL_TYPE_UNKNOWN;
		}
		else if (err == 0)
		{
			// timeout occured 
			iCount++;
			DB("Timeout on select: " << iCount);
			bErrorContinue = false;
		}
		else
		{
			if (!FD_ISSET(m_sock, &fdSet))
			{
				DB_ASSERT(0);
				bErrorContinue = true;
				continue;
			}
			char sz[2048];
			unsigned int uAddr = 0;
			unsigned short uPort = 0;

			int iLen = RecvFrom(sz, 2048, uAddr, uPort);

			if (uAddr != addrServer.first || uPort != addrServer.second)
			{
				bErrorContinue = true;
				continue;
			}

			if (iLen == SOCKET_ERROR)
				return FIREWALL_TYPE_UNKNOWN;

			string s(sz, iLen);

			if (!msg.GetMappedAddr(s, m_DetectedLocalAddr))
			{
				DB_ASSERT(0);
				bErrorContinue = true;
				continue;
			}

			msg.GetChangedAddr(s, addrSecondServer);

			if (m_DetectedLocalAddr.first == LookupLocalAddr())
				return FIREWALL_TYPE_NONE;

			if (m_DetectedLocalAddr.second == GetLocalPort())
				return FIREWALL_TYPE_NAT;

			return FIREWALL_TYPE_PAT;

		}

	}

	return FIREWALL_TYPE_UNKNOWN;


return 0;
}
*/
//____________added 20_11____________________

int ESocket::InternalDetectFirewall(
        AddressPortPair& addrServer,
        AddressPortPair& addrSecondServer)
{

	DB_ENTER(ESocket::InternalDetectFirewall);

        int pLen;
        CStunMessage msg;
        string sBuf = msg.CreateBindingRequest();
//_____________________________________________________
                int tval=100;
                struct pollfd p_fd[1];
                p_fd[0].fd=m_sock;
                p_fd[0].events=POLLRDNORM;
//_____________________________________________________
                                                                                                                             
        struct timeval tv;
        tv.tv_sec = 1;
        tv.tv_usec = 100*1000; // 100 ms
                                                                                                                             
        int iCount = 0;
        bool bErrorContinue = false;
	
        while (iCount < 7)
        {
        DB("in ESocket: addr of srv: "<<LookupHostName(addrServer.first)<<"port: "<<ntohs(addrServer.second));
                pLen=SendTo(sBuf, addrServer);
                DB("stun packet sent: len: "<<pLen);
//              DB("stun packet sent: data: "<<sBuf<<"len: "<<sBuf.length());
                                                                                                                             
                if (!bErrorContinue)
                {
                        if (iCount == 0)
                        {
                               tval=400;//added by D
                                                                                                                             
                        }
                        else
                        {
                                        tval *=2;
                                        if(tval> 5000)
                                                tval=5000;
                                                                                                                             
                        }
                }
                                                                                                                             
//______________________________________________
                                                                                                                             
                int err = poll(p_fd,1,tval);
                                                                                                                             
//______________________________________________
                                                                                                                             
                DB_STAT(err);
//p             if (err == SOCKET_ERROR)
                if( p_fd[0].revents & POLLERR )
                {
                        int e = WSAGetLastError();
                       DB("Error on select: " << e);
                        return FIREWALL_TYPE_UNKNOWN;
                }
                else if( p_fd[0].revents & POLLRDNORM)
                {
                        char sz[2048];
                        unsigned int uAddr = 0;
                        unsigned short uPort = 0;
                                                                                                                             
                        DB("before stun pkt recv: data: ");
                                                                                                                             
                        int iLen = RecvFrom(sz, 2048, uAddr, uPort);
                                                                                                                             
                        DB("stun pkt recv: data len: "<<iLen);
                        DB("stun pkt recv: data : "<<sz);

                        if (uAddr != addrServer.first || uPort != addrServer.second)
                        {
                                bErrorContinue = true;
                                continue;

                      }
                                                                                                                             
                        if (iLen == SOCKET_ERROR)
                                return FIREWALL_TYPE_UNKNOWN;
                                                                                                                             
                        string s(sz, iLen);
                                                                                                                             
                        if (!msg.GetMappedAddr(s, m_DetectedLocalAddr))
                        {
                                DB_ASSERT(0);
                                bErrorContinue = true;
                                continue;
                        }
                                                                                                                             
                        msg.GetChangedAddr(s, addrSecondServer);
                                                                                                                             
         DB("in ESocket:m_DetectedLocalAddr: "<<LookupHostName(m_DetectedLocalAddr.first)<<"port: "<<ntohs(m_DetectedLocalAddr.second));
         DB("in ESocket:addrSecondServer: "<<LookupHostName(addrSecondServer.first)<<"port: "<<ntohs(addrSecondServer.second));
                        if (m_DetectedLocalAddr.first == LookupLocalAddr())
                        {
                                return FIREWALL_TYPE_NONE;
                        }
                                                                                                                             
                        if (m_DetectedLocalAddr.second == GetLocalPort())
                                return FIREWALL_TYPE_NAT;
                                                                                                                             
                        return FIREWALL_TYPE_PAT;
                                                                                                                             
                }
                else //added by D
                {
                        // timeout occured
                        iCount++;
                        DB("Timeout on select: " << iCount);
                       bErrorContinue = false;
                                                                                                                             
                }
                                                                                                                             
        }
	
                                                                                                                             
        return FIREWALL_TYPE_UNKNOWN;
                                                                                                                             
}
                                                                                                                            

//__________________________________________
int ESocket::DetectFirewall(AddressPortPair& addrServer)
{
	
	int i;
	
	//WSAEventSelect(m_sock, NULL, NULL);
	
	
	i = InternalDetectFirewall(addrServer);
	
//	SelectEvent(m_hEventObject, m_lNetworkEvent);
	return i;
}

BOOL ESocket::SelectEvent(HANDLE hEvent, long uEvent)
{
	Critical_Section cs(*m_pMutex);
	if (m_sock == INVALID_SOCKET)
		return FALSE;

//	m_hEventObject = hEvent;
//	m_lNetworkEvent = uEvent;
	return WSAEventSelect(m_sock, hEvent, uEvent) == 0;
}



void ESocket::SetDetectedAddr(AddressPortPair& addr)
{
	m_DetectedLocalAddr = addr;

}

AddressPortPair ESocket::GetDetectedAddr()
{
	return m_DetectedLocalAddr ;

}

void ESocket::SendKeepAlive(AddressPortPair& addrServer)
{
	DB("ESocket: sending keepAlive for signaling ");
	CStunMessage msg;
	string sMsg = msg.CreateBindingRequest();
	SendTo(sMsg, addrServer);
}



BOOL ESocket::DetectAddressPort(AddressPortPair& addrServer, AddressPortPair& addrSecondServer)
{
        int i= InternalDetectFirewall(addrServer,addrSecondServer);
                                                                                
        if( addrSecondServer.first==0 || addrSecondServer.second == 0 )
                addrSecondServer = addrServer;
                                                                                
        return i!= FIREWALL_TYPE_UNKNOWN;
}
                                                                                
BOOL ESocket::DetectAddressPort(AddressPortPair& addrServer)
{
AddressPortPair addrSecondServer;
return DetectAddressPort(addrServer, addrSecondServer);
                                                                                
}


/********************* TURN Support**********************/
int ESocket::GetPortFromTurnServer(AddressPortPair& addrServer)
{
        AddressPortPair addrSecondServer;
                                                                                                                            
        CTurnMessage msg;
        if( !m_sTurnUserName.empty() )
        {
                msg.SetAccount(m_sTurnUserName,m_sTurnPassword);
                msg.SetNonceRealm(m_sTurnNonce,m_sTurnRealm);
        }
        else
        {
                //DB_ASSERT( 0 && "UserName, Password, Nonce,.. not set properly");
        }
        std::string sBuf = msg.CreateAllocateRequest(false);

        int tval=100;
        struct pollfd p_fd[1];
        p_fd[0].fd=m_sock;
        p_fd[0].events=POLLRDNORM;
                                                                                                                            
        int iCount = 0;
        bool bErrorContinue = false;
        while (iCount < 7)
        {
                                                                                                                            
                SendTo(sBuf, addrServer);
                DB("Allocate Request is sent to TURN Server.");
		
		if (!bErrorContinue)
                {
                        if (iCount == 0)
                        {
                               tval=400;//added by D
                        }
                        else
                        {
                                        tval *=2;
                                        if(tval> 5000)
                                                tval=5000;
                        }
                }
                int err = poll(p_fd,1,tval);
		
		if( p_fd[0].revents & POLLERR )
                {
                        int e = WSAGetLastError();
                        DB("Error on select: " << e);
                        return -1; 
                }
		else if( p_fd[0].revents & POLLRDNORM)
                {
                        char sz[2048];
                        unsigned int uAddr = 0;
                        unsigned short uPort = 0;
                                                                                                                            
                        int iLen = RecvFrom(sz, 2048, uAddr, uPort);
                                                                                                                            
                        if (uAddr != addrServer.first || uPort != addrServer.second)
                        {
                                bErrorContinue = true;
                                continue;
                      	}
			
			 if (iLen == SOCKET_ERROR)
                                return -1;

		        DB("Got Response Successfully.");
                        std::string s(sz, iLen);
                        if (!msg.GetMappedAddr(s, m_TurnDetectedAddr/*m_DetectedLocalAddr*/))
                        {
                                //DB_ASSERT(0);
                                bErrorContinue = true;
				iCount++;
                                continue;//what if the user send data with wrong user.
                        }
                                                                                                                            
                        DB("allocated addr: "<< LookupHostName(m_TurnDetectedAddr.first/*m_DetectedLocalAddr.first*/));
                        DB("allocated port: "<< ntohs(m_TurnDetectedAddr.second/*m_DetectedLocalAddr.first*/));
                        return 0;
                }
                else
                {
                        // timeout occured
                        iCount++;
                        DB("Timeout on select: " << iCount);
                       bErrorContinue = false;
                                                                                                                            
                }
        }
                                                                                                                            
        return -1;
}

int ESocket::TurnSendRequest(AddressPortPair& addrServer,AddressPortPair& addrDest)
{
                                                                                                                            
        AddressPortPair addrSecondServer;
                                                                                                                            
        CTurnMessage msg;
        if( !m_sTurnUserName.empty() )
        {
                msg.SetAccount(m_sTurnUserName,m_sTurnPassword);
                msg.SetNonceRealm(m_sTurnNonce,m_sTurnRealm);
        }
        else
        {
                //DB_ASSERT( 0 && "UserName, Password, Nonce,.. not set properly");
        }
        std::string sBuf = msg.CreateSendRequest(addrDest);

	int tval=100;
        struct pollfd p_fd[1];
        p_fd[0].fd=m_sock;
        p_fd[0].events=POLLRDNORM;

        int iCount = 0;
        bool bErrorContinue = false;
        while (iCount < 7)
        {
                                                                                                                            
                SendTo(sBuf, addrServer);
                DB("SEND Request is sent to TURN Server.");
                iCount++;
		
		if (!bErrorContinue)
                {
                        if (iCount == 0)
                        {
                               tval=400;//added by D
                        }
                        else
                        {
                                        tval *=2;
                                        if(tval> 5000)
                                                tval=5000;
                        }
                }
                int err = poll(p_fd,1,tval);
                                                                                                                            
                if( p_fd[0].revents & POLLERR )
                {
                        int e = WSAGetLastError();
                        DB("Error on select: " << e);
                        return -1;
                }
                else if( p_fd[0].revents & POLLRDNORM)
                {
			char sz[2048];
                        unsigned int uAddr = 0;
                        unsigned short uPort = 0;
                                                                                                                            
                        int iLen = RecvFrom(sz, 2048, uAddr, uPort);
                                                                                                                            
                        if (uAddr != addrServer.first || uPort != addrServer.second)
                        {
                                bErrorContinue = true;
                                continue;
                        }
                                                                                                                            
                        if (iLen == SOCKET_ERROR)
                                return -1;
                                                                                                                            
                        std::string s(sz, iLen);
  			if (!msg.ParseSendResponse(s))
                        {
                                DB("ParseSendResponse ERROR.");
                                //DB_ASSERT(0);
                                bErrorContinue = true;
                                iCount++;
                                continue;
                        }
                        else
                        {
		                DB("Got SendRequest Response Successfully.");
				return 0;
                        }


		}
		else
                {
                        // timeout occured
                        iCount++;
                        DB("Timeout on select: " << iCount);
                       bErrorContinue = false;
                }

        }
                                                                                                                            
        return 0;
}
                                                                                                                            
void ESocket::SetTurnParams(const std::string& sUserName,
                const std::string& sPassword,
                const std::string& sNonce,
                const std::string& sRealm
                )
{
                                                                                                                            
        m_sTurnUserName = sUserName;
        m_sTurnPassword = sPassword;
        m_sTurnNonce = sNonce;
        m_sTurnRealm = sRealm;
                                                                                                                            
}

int ESocket::TurnSetActiveDestRequest(AddressPortPair& addrServer,AddressPortPair& addrDest)
{
                                                                                                                            
        AddressPortPair addrSecondServer;
                                                                                                                            
        CTurnMessage msg;
        if( !m_sTurnUserName.empty() )
        {
                msg.SetAccount(m_sTurnUserName,m_sTurnPassword);
                msg.SetNonceRealm(m_sTurnNonce,m_sTurnRealm);
        }
        else
        {
                //DB_ASSERT( 0 && "UserName, Password, Nonce,.. not set properly");
        }
        std::string sBuf = msg.CreateSetActiveDestinationRequest(addrDest);
                                                                                                                            
	int tval=100;
        struct pollfd p_fd[1];
        p_fd[0].fd=m_sock;
        p_fd[0].events=POLLRDNORM;
                                                                                                                            
        int iCount = 0;
        bool bErrorContinue = false;
        while (iCount < 7)
        {
                                                                                                                            
                SendTo(sBuf, addrServer);
                DB("SET_ACTIVE_DESTINATION Request is sent to TURN Server.");
                iCount++;

		if (!bErrorContinue)
                {
                        if (iCount == 0)
                        {
                               tval=400;//added by D
                        }
                        else
                        {
                                        tval *=2;
                                        if(tval> 5000)
                                                tval=5000;
                        }
                }
                int err = poll(p_fd,1,tval);
                                                                                                                            
                if( p_fd[0].revents & POLLERR )
                {
                        int e = WSAGetLastError();
                        DB("Error on select: " << e);
                        return -1;
                }
                else if( p_fd[0].revents & POLLRDNORM)
                {
                        char sz[2048];
                        unsigned int uAddr = 0;
                        unsigned short uPort = 0;
                                                                                                                            
                        int iLen = RecvFrom(sz, 2048, uAddr, uPort);
                                                                                                                            
                        if (uAddr != addrServer.first || uPort != addrServer.second)
                        {
                                bErrorContinue = true;
                                continue;
                        }
                                                                                                                            
                        if (iLen == SOCKET_ERROR)
                                return -1;
                                                                                                                            
                        std::string s(sz, iLen);
			if (!msg.ParseSetActiveDestinationResponse(s))
                        {
                                DB("ParseSetActiveDestinationResponse ERROR.");
                                //DB_ASSERT(0);
                                bErrorContinue = true;
                                iCount++;
                                continue;
                        }
			else
			{
                        	DB("Got SetActiveRequest Response Successfully.");
                        	return 0;
			}

		}
                else
                {
                        // timeout occured
                        iCount++;
                        DB("Timeout on select: " << iCount);
                       bErrorContinue = false;
                }
                                                                                                                            
        }
                                                                                                                            
        return 0;
}

AddressPortPair ESocket::GetTurnDetectedAddrPort()
{
	AddressPortPair addrZero(0,0);
	if( m_TurnDetectedAddr != addrZero )
		return m_TurnDetectedAddr;
	//ASSERT( m_TurnDetectedAddr != addrZero );
	return addrZero;
}

int ESocket::ReleasePortFromTurnServer(AddressPortPair& addrServer)
{
        AddressPortPair addrSecondServer;
                                                                                                                            
        CTurnMessage msg;
        if( !m_sTurnUserName.empty() )
        {
                msg.SetAccount(m_sTurnUserName,m_sTurnPassword);
                msg.SetNonceRealm(m_sTurnNonce,m_sTurnRealm);
        }
        else
        {
                //DB_ASSERT( 0 && "UserName, Password, Nonce,.. not set properly");
        }
        std::string sBuf = msg.CreateAllocateRequest(true);
                                                                                                                            
	SendTo(sBuf, addrServer);
	return 0;
}
