// ---------------------------------------------------------------------------
// File:       pktbuf.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    
// 
// Change Log:
// ---------------------------------------------------------------------------

#include "Base.h"  // automatic use of precompiled headers 

#include "debug.h"
#include "pktbuf.h"

BufferPool::BufferPool()
:
	m_pFreeBufs(NULL)
{
}

BufferPool::~BufferPool()
{
	// delete the linked list of pktbuf
	pktbuf* pb;
	while (m_pFreeBufs != NULL)
	{
		pb = m_pFreeBufs;
		m_pFreeBufs = pb->next;
		delete pb;
	}
}


// if freebufs_ is valid pktbuf, use this
// otherwise get a new pktbuf

pktbuf* BufferPool::alloc(int layer)
{
	//printf("in BufferPool::alloc layer %d\n", layer);
	pktbuf* pb = m_pFreeBufs;
	if (pb != NULL)
	{
		m_pFreeBufs = pb->next;
	}
	else
	{
		pb = new pktbuf;
		pb->manager = this;
	}

	pb->len = 0;
	pb->ref = 1;
	pb->layer = layer;
	pb->dp = pb->data;

	return (pb);
}


// releasing a pktbuf makes it available for next alloc

void BufferPool::release(pktbuf* pb)
{
	//DB_ENTER(BufferPool::release);
	//DB("releasing pktbuf====================");
	pb->next = m_pFreeBufs;
	m_pFreeBufs = pb;
}

Buffer* pktbuf::copy()
{
	pktbuf* cp = manager->alloc(layer);
	memcpy(cp->dp, dp, len);
	cp->len = len;

	return (cp);
}

