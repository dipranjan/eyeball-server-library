// ---------------------------------------------------------------------------
// File:       RudpNetwork.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    
// 
// Change Log:
// ---------------------------------------------------------------------------

#include "Base.h"  // automatic use of precompiled headers 

using std::list;
using std::string;

#include "critsec.h"
#include "debug.h"

#if 0 //for Linux yamada
#include "DebugLog.h"
#endif //for Linux yamada

#include "EchoMessage.h"
#include "ESocket.h"

#if 1 //for Linux yamada
#include "ELinuxUtil.h"
#include "Event.h"
#else
#include "EWinUtil.h"
#endif

#include "Message.h"
#include "ReturnCodes.h"
#include "registry.h"
#include "Thread.h"
#include "Timer.h"
#include "TextUtil.h"
#include "RudpNetwork.h"
//#include "SmartPtr.h"
#include "UdpNetwork.h"
#include "EUtil.h"
#include <cmath>
#include <cstdlib>
//using namespace std;
using namespace eyeball;


// ---------------------------------------------------------------------------
// Defines
// ---------------------------------------------------------------------------

#if 1 //for Linux yamada
#define PRINT(X)
#else
#define PRINT(X) \
	OutputDebugString((tstring(X) + "\r\n").c_str());
#endif //for Linux yamada

#define WindowSizeDebug(X) SetDebugInfo(X)
#define TimeoutDebug(X) SetDebugInfo(X)

#ifdef _DEBUG
#define RUDP_OUTPUT_DEBUG 0
#else
#define RUDP_OUTPUT_DEBUG 0
#endif

#if RUDP_OUTPUT_DEBUG >= 4
// this is only used for RUDP debugging - it should not be defined in a release
#define RUDP_LOG_TO_FILE
#endif

#ifdef RUDP_LOG_TO_FILE
LOG_FILE("C:\\RUDP.log");
#endif //RUDP_LOG_TO_FILE

#ifdef _DEBUG
  #define SetDebugInfo(X) SetDebugInfoImpl(X)
#else
  #if RUDP_OUTPUT_DEBUG >= 3
    #define SetDebugInfo(X) SetDebugInfoImpl(X)
  #else
    #define SetDebugInfo(X)
  #endif
#endif

#define RUDP_SIMULATE_SEND_ERRORS 0
#define RUDP_SIMULATE_RECV_ERRORS 0
#define RUDP_SIMULATE_DELAY 0

#define RUDP_MIN_TIMEOUT 50  // ms
#define RUDP_DEFAULT_TIMEOUT 2000  // ms
#define RUDP_MAX_TIMEOUT 5000  // ms
#define RUDP_MAX_RESENDS 10
#define RUDP_UNKNOWN (-1)

// The timeout waiting for ACK's is multiplied by this factor.
// Default: 1
#define RUDP_TIMEOUT_FACTOR 2

#define RUDP_CONNECT_TIMEOUT 20000  // ms

// Period to remain at a low TTL, for firewall (IPTables) compatibility.
// Since the timeout is arbitrary, the following justification is given:
// 1. Forwarding the accept message via the ec-server is assumed to take < 1s.
// 2. The remote is given 500ms to handle the message and open the conduits.
// 3. The time is then doubled to ensure that it works in most cases.
#define RUDP_LOW_TTL_PERIOD (3000)

// Period to wait after sending a UDP conduit opening (SYN) packet. 
// For most firewalls, no delay is required.
#define RUDP_OPEN_DELAY (50)

// Period to wait for echo'd addresses to be exchanged
#define RUDP_ECHO_WAIT_PERIOD (10000)

#define SECOND_ECHO_PORT_OFFSET (4)

#define RUDP_MAX_OPTIMAL_PACKET_SIZE (8 * 1024)
#define RUDP_MAX_PACKET_SIZE (64 * 1024) // bytes - including RUDP header
#define RUDP_MIN_SEQNO 1  // note that 0 is an invalid seq. number

// used with old clients
#define RUDP_DEFAULT_MAX_SEQNO 256

// used with new clients
#define RUDP_LARGE_MAX_SEQNO (1024 * 1024 * 1024)

#define RUDP_MAX_WINDOW 128  // number of unack'ed messages

// RUDP will attempt to limit delay to (at most) this value:
#define RUDP_MAX_DELAY (1000)  // ms

// Flags
#define RUDP_SYN   0x0001
#define RUDP_FIN   0x0002
#define RUDP_NAK   0x0004
#define RUDP_ACK   0x0008
#define RUDP_PSH   0x0010
#define RUDP_URG   0x0020
#define RUDP_RST   0x0040
#define RUDP_LSN   0x0080

#if 1 //for Linux yamada
#define INFINITE            0xFFFFFFFF  // Infinite timeout
#endif //for Linux yamada

static string SFlags(u_int32_t uFlags)
{
	string sFlags("");

	sFlags +=
		((uFlags & RUDP_SYN) ? (string)"S" : "") +
		((uFlags & RUDP_FIN) ? (string)"F" : "") +
		((uFlags & RUDP_NAK) ? (string)"N" : "") +
		((uFlags & RUDP_ACK) ? (string)"A" : "") +
		((uFlags & RUDP_PSH) ? (string)"P" : "") +
		((uFlags & RUDP_URG) ? (string)"U" : "") +
		((uFlags & RUDP_RST) ? (string)"R" : "");

	return sFlags;
}

DWORD GetFirewallTtl(void)
{
	DWORD dwFirewallTtl = 0;
#if 0
	EcGetRegistryEntry(REG_FIREWALL_TTL, dwFirewallTtl);
#else
	dwFirewallTtl = LINUX_REG_FIREWALL_TTL;
#endif	
	return dwFirewallTtl;
}


// ---------------------------------------------------------------------------
// RUDPPacket Class Implementation
// ---------------------------------------------------------------------------

// This header defines the structure sent over the network.
struct RUDPPacketHeader
{
	RUDPPacketHeader() { memset(this, 0, sizeof(*this)); }
	
	u_int32_t uSeqNo;
	u_int32_t uAckNo;
	u_int16_t uOffset;
	u_int16_t uFlags;
};

class RUDPPacket
{
public:
	RUDPPacket();
	
	void Clear(void);

	int Serialize(string& sBuf) const;
	int Deserialize(const string& sBuf);

public:
	u_int32_t m_uSeqNo;
	u_int32_t m_uAckNo;
	u_int16_t m_uFlags;

	string m_sData;

	// The following data is not serialized.
	DWORD m_dwTimeSent;
	u_int16_t m_uSends;
};

RUDPPacket::RUDPPacket()
:
	m_uSeqNo(0),
	m_uAckNo(0),
	m_uFlags(0),
	
	m_sData(),

	m_dwTimeSent(0),
	m_uSends(0)
{
}

void RUDPPacket::Clear(void)
{
	m_uSeqNo = 0;
	m_uAckNo = 0;
	m_uFlags = 0;

	m_sData.erase();

	m_dwTimeSent = 0;
	m_uSends = 0;
}

int RUDPPacket::Serialize(string& sBuf) const
{
	RUDPPacketHeader hdr;
	
	u_int16_t uOffset = sizeof(RUDPPacketHeader);

	hdr.uSeqNo = htonl(m_uSeqNo);
	hdr.uAckNo = htonl(m_uAckNo);
	hdr.uOffset = htons(uOffset);
	hdr.uFlags = htons(m_uFlags);
	
	sBuf.assign((const char*)&hdr, sizeof(hdr));

	sBuf += m_sData;

	return ES_SUCCESS;
}

int RUDPPacket::Deserialize(const string& sBuf)
{
	if (sBuf.length() < sizeof(RUDPPacketHeader))
		return ES_FAILURE;

	RUDPPacketHeader* pHdr = (RUDPPacketHeader*)sBuf.data();

	u_int16_t uOffset = ntohs(pHdr->uOffset);

	if (uOffset < sizeof(RUDPPacketHeader))
	{
		DB_ASSERT(0 && "uOffset < sizeof(RUDPPacketHeader)");
		return ES_FAILURE;
	}

	int iPayloadLen = (int)sBuf.length() - (int)uOffset;

	if (iPayloadLen < 0)
	{
		DB_ASSERT(0 && "iPayloadLen < 0");
		return ES_FAILURE;
	}

	if (iPayloadLen > RUDP_MAX_PACKET_SIZE)
	{
		// this should be impossible
		DB_ASSERT(0);
		return ES_FAILURE;
	}

	m_uSeqNo = ntohl(pHdr->uSeqNo);
	m_uAckNo = ntohl(pHdr->uAckNo);
	m_uFlags = ntohs(pHdr->uFlags);

	m_sData.erase();

	// Allow for zero size messages
	if (iPayloadLen == 0)
		return ES_SUCCESS;

#if 1 //for Linux yamada
	m_sData.assign(sBuf.data()+uOffset, iPayloadLen);
#else
	m_sData.assign(&sBuf[uOffset], iPayloadLen);
#endif //for Linux yamada

	return ES_SUCCESS;
}


// ---------------------------------------------------------------------------
// RUDPPacketList Class Implementation
// ---------------------------------------------------------------------------

class RUDPPacketList : public list<RUDPPacket> 
{
	typedef list<RUDPPacket> BaseClass;

public:
	RUDPPacketList()
	: 
		BaseClass(),
		m_uSizeInBytes(0)
	{
	}

	u_int32_t GetFirstSeqNo(void)
	{
		if (empty())
			return 0;

		return front().m_uSeqNo;
	}

	bool IsSentPacketSeqNo(u_int32_t uSeqNo)
	{
		int i = 0;
		for (iterator iter = begin(); iter != end(); iter++)
		{
			if (iter->m_uSeqNo == uSeqNo)
				return true;

			// the sequence numbers wrap around - so we only want to check
			// the first N packets, which could actually be sent
			if (i++ > (RUDP_MAX_WINDOW + 1))
				return false;
		}

		return false;
	}

	void pop_front(void)
	{
		if (!empty())
		{
			m_uSizeInBytes -= front().m_sData.size();
			BaseClass::pop_front();
		}
	}

	void push_back(const RUDPPacket& pkt)
	{
		BaseClass::push_back(pkt);
		m_uSizeInBytes += pkt.m_sData.size();
	}

	void clear(void)
	{
		BaseClass::clear();
		m_uSizeInBytes = 0;
	}

public:
	unsigned int m_uSizeInBytes;
};


// ---------------------------------------------------------------------------
// RudpNetwork Class Implementation
// ---------------------------------------------------------------------------

// Constructor
RudpNetwork::RudpNetwork()
:
	m_pMutex(new Mutex),
	m_rsState(RS_CLOSED),
	m_pSendQ(new RUDPPacketList),
	m_pRecvQ(new RUDPPacketList),
	m_uMaxSeqNo(RUDP_DEFAULT_MAX_SEQNO),
	m_uSentSeqNo(0),
	m_uSentPacketCount(0),
	m_uWindowSize(1),
	m_uWindowIncCounter(1),

	m_uReceivedSeqNo(0),

	m_uRemoteAddr(0),
	m_uRemotePort(0),
	m_uRemoteAddr1(0),
	m_uRemoteAddr2(0),
	m_DetectedRemoteAddr(0, 0),
	m_bRemoteAddressTranslated(true),
	m_uOpenStartTime(0),
	m_uOpenExpiryTime(0),

	m_pThread(NULL),
	m_pUdpNetwork(new UdpNetwork),
	m_hNotifyEvent(NULL),
	m_uEventFlags(0),
	m_uEventFilter(~0),
	m_hQuitEvent(NULL),
	m_hRecvEvent(NULL),
	m_hSendEvent(NULL),
	m_hConnectEvent(NULL),
	m_uTimeout(INFINITE),
	m_uLastTimeout(RUDP_UNKNOWN),
	m_uAverageRTT(RUDP_UNKNOWN),
	m_uAverageDev(RUDP_UNKNOWN),
	m_uSeqNo(RUDP_MIN_SEQNO),
	m_sDebugInfo(),
	m_EchoServerAddr(0, 0),
	m_DetectedLocalAddr(0, 0),
	m_EchoServerAddr2(0, 0),
	m_DetectedLocalAddr2(0, 0),
	m_bPortIncremented(false),
	m_bRemoteBehindFirewall(false),
	m_bLocalBehindFirewall(false),
	m_dwSentEchoRequest(0),

	m_uTotalTimeouts(0),
	m_bSlowStart(true),

	m_nTtl(0),
	m_nDefaultTtl(0),
	m_dwTtlSetTime(0),

	m_iCloseReason(ES_NOT_CONNECTED),
	m_bUseFirewallServer(false),
	m_pDisposableSocket(new DisposableSocket)
{
	m_hQuitEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	m_hRecvEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	m_hSendEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	m_hConnectEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	// set default event filter
	m_uEventFilter &= (~RE_SEND);

#ifdef RUDP_LOG_TO_FILE
	LOG_ENABLE(TRUE);
	LOG_OPEN();
#endif //RUDP_LOG_TO_FILE
}

RudpNetwork::~RudpNetwork()
{
	RudpNetwork::Close();

	if (m_pThread.get() != NULL)
	{
#ifdef _WIN32_WCE
		m_pDisposableSocket->Destroy();
#endif
		SetEvent(m_hQuitEvent);
		m_pThread->join();
	}

	m_pDisposableSocket.reset();
#if 1 //for Linux yamada
	CLOSE_EVENT(m_hConnectEvent);
	CLOSE_EVENT(m_hSendEvent);
	CLOSE_EVENT(m_hRecvEvent);
	CLOSE_EVENT(m_hQuitEvent);
#else
	CLOSE_HANDLE(m_hConnectEvent);
	CLOSE_HANDLE(m_hSendEvent);
	CLOSE_HANDLE(m_hRecvEvent);
	CLOSE_HANDLE(m_hQuitEvent);
#endif

#ifdef RUDP_LOG_TO_FILE
	LOG_CLOSE();
#endif //RUDP_LOG_TO_FILE
}

int RudpNetwork::Bind(u_int16_t& uLocalPort)
{
	Critical_Section cs(*m_pMutex);

	SharedPtr<ESocket> pSocket(new ESocket);

	pSocket->Bind(INADDR_ANY, uLocalPort);
	uLocalPort = pSocket->GetLocalPort();
	if (0 != uLocalPort)
	{
		DB_ASSERT(RS_CLOSED == m_rsState);
		m_rsState = RS_BOUND;
	}

	BOOL bRet = m_pUdpNetwork->SetSharedSocket(pSocket);
	DB_ASSERT(bRet);

	return bRet ? ES_SUCCESS : ES_FAILURE;
}

u_int16_t RudpNetwork::GetLocalPort(void)
{
	Critical_Section cs(*m_pMutex);

	SharedPtr<ESocket> pSocket;
	if (!m_pUdpNetwork->GetSharedSocket(pSocket))
		return 0;

	return pSocket->GetLocalPort();
}

AddressPortPair RudpNetwork::GetLocalAddress(void)
{
	Critical_Section cs(*m_pMutex);

	SharedPtr<ESocket> pSocket;
	if (!m_pUdpNetwork->GetSharedSocket(pSocket))
		return AddressPortPair(0, 0);

	return pSocket->GetLocalAddrPort();
}


int RudpNetwork::Open(
	u_int32_t uRemoteAddr, 
	u_int16_t uRemotePort,
	u_int16_t uLocalPort,
	HANDLE hNotifyEvent)
{
	Critical_Section cs(*m_pMutex);

	if (m_rsState == RS_ESTABLISHED)
	{
		return ES_SUCCESS; // already connected
	}

	if (m_pThread.get() == NULL)
	{
		// Start the thread on the first connect.
		m_pThread = SharedPtr<Thread>(new Thread(RudpNetworkThread, this));
	}

	if (hNotifyEvent != NULL)
	{
		m_hNotifyEvent = hNotifyEvent;
	}

	if (!IsOpen())
	{
#ifndef _WIN32_WCE
		// Open socket
		bool bRet = m_pUdpNetwork->Open(0, 0, uLocalPort, m_hRecvEvent);
#else
	//	bool bRet = m_pUdpNetwork->Open(0, 0, uLocalPort, NULL);
		bool bRet = m_pUdpNetwork->Open(0, 0, uLocalPort, m_hRecvEvent);
#endif
		if (!bRet)
		{
			return ES_FAILURE;
		}else
		{
		}

		SharedPtr<ESocket> pSocket;
		if (!m_pUdpNetwork->GetSharedSocket(pSocket))
		{
			DB_ASSERT(0);
			return ES_FAILURE;
		}

		if (0 == m_nDefaultTtl)
		{
			// save the default TTL for the socket
			m_nDefaultTtl = pSocket->GetTtl();
		}

		// reduce the TTL to open local conduits without reaching the remote host
		m_nTtl = GetFirewallTtl();
		pSocket->SetTtl(m_nTtl);
		m_dwTtlSetTime = timeGetTime();
	}

	m_rsState = RS_OPEN;

	if ((0 != uRemoteAddr) && (0 != uRemotePort))
	{
		for (int i = 0; i < 1; i++)
		{
			// Send SYN packets to open a channel
			SendSynPacket(uRemoteAddr, uRemotePort);
		}
	}

	return ES_SUCCESS;
}

int RudpNetwork::OpenWait(
	u_int32_t uRemoteAddr1,
	u_int32_t uRemoteAddr2,
	u_int16_t uRemotePort,
	u_int16_t uLocalPort,
	HANDLE hNotifyEvent,
	u_int32_t uTimeout)
{
	if (uTimeout == 0)
	{
		uTimeout = RUDP_CONNECT_TIMEOUT;
	}

	ResetEvent(m_hConnectEvent);

	int iRet = OpenAsync(
		uRemoteAddr1,
		uRemoteAddr2,
		uRemotePort,
		uLocalPort,
		hNotifyEvent,
		uTimeout);

	if (ES_SUCCESS != iRet)
		return iRet;

	DWORD dwResult = CustomWaitForSingleObject(m_hConnectEvent, uTimeout);
	if (dwResult != WAIT_OBJECT_0)
		return ES_FAILURE;

	if (GetConnectionState() != RS_ESTABLISHED)
	{
		SetDebugInfo("Connection failed.");
		Close(ES_CANNOT_CONNECT);
		return ES_NOT_CONNECTED; // failure
	}

	SetDebugInfo("OpenWait - Connection established");

	return ES_SUCCESS;
}

int RudpNetwork::OpenAsync(
	u_int32_t uRemoteAddr1,
	u_int32_t uRemoteAddr2,
	u_int16_t uRemotePort,
	u_int16_t uLocalPort,
	HANDLE hNotifyEvent,
	u_int32_t uTimeout)
{
	DB_ASSERT(uRemotePort != 0);
	DB_ASSERT((uLocalPort != 0) || (GetLocalPort() != 0));

	m_iCloseReason = ES_NOT_CONNECTED;
	if (uTimeout == 0)
	{
		uTimeout = RUDP_CONNECT_TIMEOUT;
	}

	ResetEvent(m_hConnectEvent);

	if (m_rsState == RS_ESTABLISHED)
	{
		return ES_FAILURE;
	}

	if (m_rsState == RS_OPEN)
	{
		Close(ES_FAILURE, true);
	}

	ResetConnectionState();

	m_uRemoteAddr1 = uRemoteAddr1;
	m_uRemoteAddr2 = uRemoteAddr2;

	m_uOpenStartTime = timeGetTime();
	m_uOpenExpiryTime = m_uOpenStartTime + uTimeout;

	// Open Receiver
	if (Open(0, 0, uLocalPort, hNotifyEvent) != ES_SUCCESS)
		return ES_FAILURE;

	m_uRemotePort = uRemotePort;
	if (!AttemptConnection())
		return ES_FAILURE;

	// wake up/unblock the internal thread
	SetSendEvent();


	return ES_SUCCESS;
}

int RudpNetwork::Close(int iReason, bool bKeepSocket)
{
	Critical_Section cs(*m_pMutex);

	if (m_rsState == RS_ESTABLISHED)
	{
		SendFinPacket();

		NotifyEvent(RE_DISCONNECT);
	}

	RS_STATE rsState = m_rsState;
	if (!bKeepSocket)
	{
		m_pUdpNetwork->Close();
		m_rsState = RS_CLOSED;
		m_hNotifyEvent = NULL;
		m_dwSentEchoRequest = 0;
		m_DetectedLocalAddr = AddressPortPair(0, 0);
		m_DetectedLocalAddr2 = AddressPortPair(0, 0);
		m_bPortIncremented = false;

		m_nTtl = 0;
		m_nDefaultTtl = 0;
		m_dwTtlSetTime = 0;
	}
	else
	{
		if (IsOpen())
		{
			m_rsState = RS_BOUND;
		}
	}

	m_iCloseReason = iReason;
	m_bUseFirewallServer = false;
	ResetConnectionState();
	return ES_SUCCESS;
}

int RudpNetwork::Send(const string& sBuf)
{
	Critical_Section cs(*m_pMutex);

	if (!IsOpen())
		return ES_FAILURE;

	// overly large UDP packets will not work well with some networks
	DB_ASSERT(sBuf.size() <= RUDP_MAX_OPTIMAL_PACKET_SIZE);

	RUDPPacket packet;
	packet.m_sData = sBuf;

	// Assign SeqNo, and add to packet queue
	QueueForSending(packet);

	return ES_SUCCESS;
}

int RudpNetwork::SendMsg(const ISerializable& msg)
{
	string sBuf;
	msg.serialize(sBuf);
	
	return Send(sBuf);
}

int RudpNetwork::Recv(string& sBuf)
{
	Critical_Section cs(*m_pMutex);

	sBuf.erase();

	if (!IsOpen())
	{
		return m_iCloseReason;
	}

	if (m_pRecvQ->empty())
		return ES_FAILURE;

	sBuf = m_pRecvQ->front().m_sData;
	m_pRecvQ->pop_front();

	if (!m_pRecvQ->empty())
	{
		NotifyEvent(RE_READ);  // more packets available
	}

	return ES_SUCCESS;
}

RS_STATE RudpNetwork::GetConnectionState(void)
{
	Critical_Section cs(*m_pMutex);

	return m_rsState;
}

bool RudpNetwork::IsOpen(void)
{
	Critical_Section cs(*m_pMutex);

	return ((m_rsState == RS_OPEN) || (m_rsState == RS_ESTABLISHED));
}

int RudpNetwork::GetSendQueueSize(void)
{
	Critical_Section cs(*m_pMutex);

	if (m_rsState == RS_CLOSED)
		return 0;

	return (int)m_pSendQ->m_uSizeInBytes;
}

u_int32_t RudpNetwork::GetRemoteAddr(void)
{
	Critical_Section cs(*m_pMutex);

	if (m_rsState == RS_CLOSED)
		return 0;

	return m_uRemoteAddr;
}

u_int16_t RudpNetwork::GetRemotePort(void)
{
	Critical_Section cs(*m_pMutex);

	if (m_rsState == RS_CLOSED)
		return 0;

	return m_uRemotePort;
}

void RudpNetwork::EnableEvent(u_int16_t uEvent, bool bEnable)
{
	if (bEnable)
	{
		m_uEventFilter |= uEvent;
	}
	else
	{
		m_uEventFilter &= ~(uEvent);
	}
}

u_int16_t RudpNetwork::GetEventFlags(void)
{
	Critical_Section cs(*m_pMutex);

	u_int16_t uFlags = m_uEventFlags;
	m_uEventFlags = 0;

	if (!m_pRecvQ->empty())
	{
		uFlags |= RE_READ;
	}

	return uFlags;
}

void RudpNetwork::GetDebugInfo(string& sText)
{
	Critical_Section cs(*m_pMutex);

	sText = m_sDebugInfo;
	m_sDebugInfo.erase();
}

// Remote address detection functions
void RudpNetwork::SendEchoAddressRequest(AddressPortPair addrEcho, int nTtl)
{
	Critical_Section cs(*m_pMutex);


	if (RS_ESTABLISHED == m_rsState)
	{
		DB("Connection established - no need for address echo");
		return;
	}

	m_EchoServerAddr = addrEcho;
	m_EchoServerAddr2 = addrEcho;
	m_EchoServerAddr2.second = 
		htons((ntohs(addrEcho.second) + SECOND_ECHO_PORT_OFFSET));


	// use the global firewall server to detect the firewall type
	DWORD dwOptions = 0;
#if 0
	EcGetRegistryEntry(REG_NETWORK_OPTIONS, dwOptions);
#else
	dwOptions = LINUX_REG_NETWORK_OPTIONS;
#endif
	if (((dwOptions & NETWORK_FIREWALL_SERVER) != 0) || m_bUseFirewallServer)
	{
		tstring sFirewallServer = "chat0.eyeball.com";
#if 0
		EcGetRegistryEntry(REG_FIREWALL_SERVER, sFirewallServer);
#endif
		m_EchoServerAddr2.first = LookupHostAddr(sFirewallServer.mbs());

		if (m_EchoServerAddr2.first == 0)
		{
			DWORD dwMask = ~NETWORK_FIREWALL_SERVER;
			dwOptions &= dwMask;
			// the global firewall server is unavailable
			EcSetRegistryEntry(REG_NETWORK_OPTIONS, dwOptions);
			m_bUseFirewallServer = false;
			m_EchoServerAddr2.first = addrEcho.first;
		}
		else
		{
			DWORD dwPort = 0;
#if 0
			EcGetRegistryEntry(REG_FIREWALL_PORT, dwPort);
#endif
			if (dwPort == 0)
				dwPort = 5503;

			m_EchoServerAddr2.second = htons(WORD(dwPort));
		}
	}

	if (RS_CLOSED == m_rsState)
	{
		DB_ASSERT(0);
		return;
	}

	// Contact the address detection (echo) server
	AddressEchoMessage msg;
	string sBuf;
	msg.serialize(sBuf);
	
	SharedPtr<ESocket> pSocket;
	if (!m_pUdpNetwork->GetSharedSocket(pSocket))
		return;

	if (0 != nTtl)
	{
		// use given TTL
		pSocket->SetTtl(nTtl);
	}
	else if (m_nDefaultTtl > m_nTtl)
	{
		// temporarily set TTL back to the default, just for this packet
		pSocket->SetTtl(m_nDefaultTtl);
	}

	for (int i = 0; i < 2; i++)
	{
		int iRet = m_pUdpNetwork->SendTo(sBuf, m_EchoServerAddr);
		if (iRet < (int)sBuf.size())
		{
			DB("RudpNetwork::RemoteAddressDetect - SendTo failed");
		}
	}


	for (int j = 0; j < 2; j++)
	{
		// second a packet to the "confirmation port"
		int iRet = m_pUdpNetwork->SendTo(sBuf, m_EchoServerAddr2);
		if (iRet < (int)sBuf.size())
		{
		}
	}
	
	m_dwSentEchoRequest = timeGetTime();
	if (0 == m_dwSentEchoRequest)
	{
		// handle the extremely rare case of timeGetTime() returning zero.
		m_dwSentEchoRequest = 1;
	}

	if (m_nDefaultTtl > m_nTtl)
	{
		// restore TTL
		pSocket->SetTtl(m_nTtl);	
	}

	// wake up/unblock the internal thread
	SetSendEvent();
}

AddressPortPair RudpNetwork::GetDetectedAddress(void)
{
	Critical_Section cs(*m_pMutex);

	if ((0 == m_DetectedLocalAddr.first) ||
		(0 == m_DetectedLocalAddr2.first))
	{
		// address has not yet been detected
		return AddressPortPair(0, 0);
	}

	AddressPortPair addr = m_DetectedLocalAddr;
	if (m_bPortIncremented)
	{
		u_int16_t uPort1 = ntohs(m_DetectedLocalAddr.second);
		u_int16_t uPort2 = ntohs(m_DetectedLocalAddr2.second);

		if (uPort2 != uPort1)
		{
			// incrementing port number by 1 (predicted port)
			addr.second = htons(MAX(uPort1, uPort2) + 1);
		}
	}

	return addr;
}

int RudpNetwork::GetFirewallType()
{
	if ((0 == m_DetectedLocalAddr.first) ||
		(0 == m_DetectedLocalAddr2.first))
	{
		if (m_rsState != RS_ESTABLISHED)
		{
			DB_ASSERT(0);
			// address has not yet been detected
			return FIREWALL_TYPE_UNKNOWN;
		}

		if (m_uRemoteAddr1 == m_uRemoteAddr2)
			return FIREWALL_TYPE_NONE;

		// can only be NAT
		return FIREWALL_TYPE_NAT;
	}
	
	if (IsPortIncremented())
	{
		return FIREWALL_TYPE_PAT_PLUS;
	}

	if (IsPortTranslated())
	{
		return FIREWALL_TYPE_PAT;
	}

	if (IsAddressTranslated())
	{
		return FIREWALL_TYPE_NAT;
	}
	
	return FIREWALL_TYPE_NONE;
}

u_int32_t RudpNetwork::GetDetectedLocalAddress(void)
{
	if (m_DetectedLocalAddr.first)
		return m_DetectedLocalAddr.first;

	if (m_DetectedLocalAddr2.first)
		return m_DetectedLocalAddr2.first;

	return 0;
}
bool RudpNetwork::IsPortTranslated(bool bUnknown)
{
	Critical_Section cs(*m_pMutex);

	if (0 == m_DetectedLocalAddr.second)
		return bUnknown;

	u_int16_t uPort = GetLocalPort();
	if (0 == uPort)
		return false;

	bool bTranslated = uPort != m_DetectedLocalAddr.second;

	return bTranslated;
}

bool RudpNetwork::IsAddressTranslated(bool bUnknown)
{
	Critical_Section cs(*m_pMutex);

	if ((0 == m_DetectedLocalAddr.first) || (0 == m_DetectedLocalAddr.second))
		return bUnknown;

	// check if address is translated
	u_int32_t uAddr = LookupLocalAddr();
	if (uAddr != m_DetectedLocalAddr.first)
	{
		DB("NAT detected");
		return true;
	}

	// check if port is translated
	u_int16_t uPort = GetLocalPort();
	if (0 == uPort)
		return false;

	if (uPort != m_DetectedLocalAddr.second)
	{
		DB("PAT detected");
		return true;
	}

	return false;
}

bool RudpNetwork::IsRemoteAddressTranslated(bool bUnknown)
{
	if (0 == m_uRemoteAddr1)
	{
		// remote address #1 is not set - return default as set by the owner
		return m_bRemoteAddressTranslated;
	}

	if (0 != m_DetectedRemoteAddr.first)
	{
		if (m_DetectedRemoteAddr.first != m_uRemoteAddr1)
			return true;  // definitely translated
	}

	if (m_uRemoteAddr1 == m_uRemoteAddr2)
	{
		// definitely not translated
		return false;
	}

	return bUnknown;
}

void RudpNetwork::SetRemoteAddressTranslated(bool bTranslated)
{
	Critical_Section cs(*m_pMutex);

	m_bRemoteAddressTranslated = bTranslated;
}

void RudpNetwork::SetDetectedRemoteAddress(const AddressPortPair& addr)
{
	Critical_Section cs(*m_pMutex);


	m_DetectedRemoteAddr = addr;

	if ((RS_OPEN == m_rsState) &&
		(timeGetTime() - m_uOpenStartTime <= RUDP_ECHO_WAIT_PERIOD))
	{
		// retry the connection
		AttemptConnection();
	}
}

AddressPortPair RudpNetwork::GetDetectedRemoteAddress()
{
	return m_DetectedRemoteAddr;
}


// Private methods
void* RudpNetwork::RudpNetworkThread(void *arg)
{
	DB_ASSERT(arg != NULL);

	if (NULL == arg)
		return NULL;

	((RudpNetwork*)(arg))->RudpNetworkThreadImpl();

	return NULL;
}

void RudpNetwork::RudpNetworkThreadImpl(void)
{
	const int iNumEvents = 3;
	HANDLE aEvents[iNumEvents];
	aEvents[0] = m_hQuitEvent;
	aEvents[1] = m_hRecvEvent;
	aEvents[2] = m_hSendEvent;

	bool bStop(false);
	while(!bStop)
	{
		DWORD dwTimeLeft = 0;
		if (m_uTimeout == INFINITE)
		{
			if (m_dwSentEchoRequest > 0)
			{
				dwTimeLeft = RUDP_DEFAULT_TIMEOUT;
			}
			else
			{
				dwTimeLeft = INFINITE;
			}
		}
		else if (m_pSendQ->empty())
		{
			dwTimeLeft = 0;
		}
		else
		{
			DWORD dwTimeSent = m_pSendQ->front().m_dwTimeSent;
			// note: dwTimeSent may be 0

			DWORD dwElapsed = timeGetTime() - dwTimeSent;
			if (dwElapsed < m_uTimeout)
			{
				dwTimeLeft = m_uTimeout - dwElapsed;
			}
		}

		SetDebugInfo("RUDP waiting - time-left = " + itoa(dwTimeLeft));

#ifndef _WIN32_WCE
		DWORD dwEvent = WaitForMultipleObjects(
			iNumEvents,
			aEvents,
			FALSE, 
			dwTimeLeft);
#else
		if (m_pUdpNetwork->GetSocket() == INVALID_SOCKET)
		{
#if 0 //for linux yamada
			OutputDebugString(TEXT("RudpNetwork thread exited.\n"));
#endif //for Linux yamada
			return;
		}

		DWORD dwEvent = 0;
		fd_set readfds;
		FD_ZERO(&readfds);
		m_pDisposableSocket->Create();
		FD_SET(m_pDisposableSocket->GetSocket(), &readfds);
		FD_SET(m_pUdpNetwork->GetSocket(), &readfds);
		if (WaitForSingleObject(m_hQuitEvent, 0) == WAIT_OBJECT_0)
			dwEvent = WAIT_OBJECT_0 + 0;
		else if (WaitForSingleObject(m_hSendEvent, 0) == WAIT_OBJECT_0)
			dwEvent = WAIT_OBJECT_0 + 2;
		else
		{
			timeval tv;
			tv.tv_sec = dwTimeLeft/1000;
			tv.tv_usec = dwTimeLeft%1000;
			//int iEvents = select(0, &readfds, NULL, NULL, &tv);
			int iEvents = select(m_pDisposableSocket->GetSocket() + 1, &readfds, NULL, NULL, &tv);
			if (iEvents == 0)
				dwEvent = WAIT_TIMEOUT;
			else
			{
				if (WaitForSingleObject(m_hQuitEvent, 0) == WAIT_OBJECT_0)
					dwEvent = WAIT_OBJECT_0 + 0;
				else if (WaitForSingleObject(m_hSendEvent, 0) == WAIT_OBJECT_0)
					dwEvent = WAIT_OBJECT_0 + 2;
				else
					dwEvent = WAIT_OBJECT_0 + 1;
			}
		}
#endif

		// Determine why we woke up and act accordingly
		switch (dwEvent)
		{
		case WAIT_OBJECT_0 + 0: // m_hQuitEvent
			bStop = true;
			break;

		case WAIT_OBJECT_0 + 1:	// m_hRecvEvent
			ProcessRecvEvent();
			break;

		case WAIT_OBJECT_0 + 2: // m_hSendEvent
			ProcessSendEvent();
			break;

		case WAIT_TIMEOUT:
			ProcessTimeoutEvent();
			break;

		case WAIT_FAILED:
			DB_ASSERT(0 && "RudpNetwork: Wait Failed");
			bStop = true;
			break;

		default:
			DB_ASSERT(0 && "Unknown event in RudpNetworkThread");
			break;
		}
	}
}


// ---------------------------------------------------------------------------
// Private methods
// ---------------------------------------------------------------------------

bool RudpNetwork::AttemptConnection(void)
{
	//DB_ENTER(RudpNetwork::AttemptConnection);


	Critical_Section cs(*m_pMutex);

	DB_ASSERT(RS_OPEN == m_rsState);

	// Queue a SYN packet - should be the first queued packet
	if (m_pSendQ->empty())
	{
		QueueSynPacket();
	}

	// Initialize time-out
	m_uTimeout = RUDP_DEFAULT_TIMEOUT;

	// Try to prevent extra port increments by waiting for the addresses
	// to be detected before connecting.  This may cause compatibility
	// problems with old (v2.0) clients, so we continue anyway if a certain
	// amount of time has passed.
	if (timeGetTime() - m_uOpenStartTime <= RUDP_ECHO_WAIT_PERIOD)
	{
		// even the remote is not behind the firewall, the local port
		// has to be detected first to determine the local firewall type.
		if (m_dwSentEchoRequest > 0 && m_bLocalBehindFirewall)
		{
			if ((0 == m_DetectedLocalAddr.first) ||
				(0 == m_DetectedLocalAddr2.first))
			{
				return true;
			}
		}

		if ((0 != m_uRemoteAddr1) && 
			(0 != m_uRemoteAddr2) &&
			(0 == m_DetectedRemoteAddr.first) &&
			m_bRemoteBehindFirewall)
		{
			return true;
		}
	}

	bool bInternalFirst = false;

	DWORD dwLocalAddr = LookupLocalAddr();
	if (dwLocalAddr != m_DetectedLocalAddr.first && m_uRemoteAddr1 != m_uRemoteAddr2)
	{
		bInternalFirst = IsInSameSubnet(dwLocalAddr, m_uRemoteAddr1);
	}

	if (bInternalFirst)
	{
		UINT uInternalAddr = m_uRemoteAddr1;
		if (m_DetectedRemoteAddr.first == m_uRemoteAddr1)
			uInternalAddr = m_uRemoteAddr2;

		if (0 != uInternalAddr)
		{
			if (Open(uInternalAddr, m_uRemotePort, 0, 0) != ES_SUCCESS)
			{
				return false;
			}
		}
	}


	// Try all remote IP addresses
	if (0 != m_DetectedRemoteAddr.first)
	{
		if (Open(m_DetectedRemoteAddr.first, m_DetectedRemoteAddr.second, 0, 0) != ES_SUCCESS)
		{
			return false;
		}
	}
	
	if (0 != m_uRemoteAddr1)
	{
		if (Open(m_uRemoteAddr1, m_uRemotePort, 0, 0) != ES_SUCCESS)
		{
			return false;
		}
	}

	if ((0 != m_uRemoteAddr2) && (m_uRemoteAddr1 != m_uRemoteAddr2))
	{
		if (Open(m_uRemoteAddr2, m_uRemotePort, 0, 0) != ES_SUCCESS)
		{
			return false;
		}
	}

	return true;
}


// ---------------------------------------------------------------------------
// Event Handlers
// ---------------------------------------------------------------------------

// Receive a packet from the remote host
void RudpNetwork::ProcessRecvEvent(void)
{
	Critical_Section cs(*m_pMutex);

	//DB_STAT(m_rsState);

	if (m_rsState == RS_CLOSED)
		return;

	u_int32_t uFromAddr(0);
	u_int16_t uFromPort(0);
	
	SharedArray<char> pBuf(new char[RUDP_MAX_PACKET_SIZE]);
	int iLen(0);

	iLen = m_pUdpNetwork->RecvFrom(
		&pBuf[0],
		RUDP_MAX_PACKET_SIZE, 
		uFromAddr, 
		uFromPort);
	if (iLen < 0)
	{
		int iError = WSAGetLastError();
		return;
	}

	string sBuf;
	sBuf.assign(&pBuf[0], iLen);

	if (m_EchoServerAddr == AddressPortPair(uFromAddr, uFromPort))
	{
		AddressEchoMessage msg;
		msg.deserialize(sBuf);
		m_DetectedLocalAddr = AddressPortPair(
			htonl(msg.m_u32IpAddress),
			htons(msg.m_u16IpPort));
		NotifyEchoResponse();
		return;
	}

	if (m_EchoServerAddr2 == AddressPortPair(uFromAddr, uFromPort))
	{
		AddressEchoMessage msg;
		msg.deserialize(sBuf);
		m_DetectedLocalAddr2 = AddressPortPair(
			htonl(msg.m_u32IpAddress),
			htons(msg.m_u16IpPort));
		SetDebugInfo("Detected local address (echo server #2): " + 
			Network::ToString(m_DetectedLocalAddr2));

		NotifyEchoResponse();
		return;
	}

	//DB("From address: " << LookupHostName(uFromAddr).c_str());
	
	if (RS_BOUND == m_rsState)
	{
		// open has not yet been called
		return;
	}

	if (m_rsState == RS_ESTABLISHED)
	{
		// Check that we received from the correct sender
		if ((uFromAddr != m_uRemoteAddr) || (uFromPort != m_uRemotePort))
		{
			//DB_ASSERT(0);
			SetDebugInfo("Received from wrong remote addr/port: " +
				intoa(uFromAddr) + ":" + itoa(ntohs(uFromPort)));
			SetDebugInfo("Current remote addr/port: " +
				intoa(m_uRemoteAddr) + ":" + itoa(ntohs(m_uRemotePort)));
			return;
		}
	}

	RUDPPacket packet;
	if (packet.Deserialize(sBuf) < 0)
	{
		SetDebugInfo("Received packet is invalid.");
		return;
	}

	SetDebugInfo("Packet Recv: FL=" + SFlags(packet.m_uFlags) +
		"  AN=" + itoa(packet.m_uAckNo) +
		"  SN=" + itoa(packet.m_uSeqNo));

#if RUDP_SIMULATE_RECV_ERRORS
#pragma _ebWarning("Debug option enabled - simulating receive errors")
	// Simulate errors in the network
	if (rand() < (RAND_MAX * 0.25))
	{
		SetDebugInfo("Ignoring received message.");
		return;
	}
	else
	{
		SetDebugInfo("Allowing received message.");
	}
#endif // RUDP_SIMULATE_RECV_ERROR

	if ((0 == m_uRemoteAddr) && 
		(0 == m_uRemotePort) &&
		(m_DetectedRemoteAddr == AddressPortPair(0, 0)))
	{
		DB("No remote set - RUDP packet ignored.");
		return;
	}

	// Check for SYN packet
	if ((m_rsState == RS_OPEN) && (packet.m_uFlags & RUDP_SYN))
	{
		// Set Remote Addr/Port to match SYN packet sender
		if (!(uFromAddr && uFromPort))
		{
			DB_ASSERT(0 && "Invalid SYN packet");
			return;
		}

		SetDebugInfo("Received SYN from remote at " + 
			intoa(uFromAddr) + ":"	+ itoa(ntohs(uFromPort)));

		RestoreDefaultTtl();

		//DB("Remote address confirmed: " << LookupHostName(uFromAddr).c_str());
		m_uRemoteAddr = uFromAddr;

		m_uRemotePort = uFromPort;

		if (packet.m_uFlags & RUDP_LSN)
		{
			m_uMaxSeqNo = RUDP_LARGE_MAX_SEQNO;
		}

		// send a SYN packet and wait for an ACK, if nothing else is queued
		if (m_pSendQ->empty())
		{
			QueueSynPacket();
		}

		// send a SYN packet right away.
		RUDPPacket pktSyn;
		pktSyn.m_uFlags = RUDP_SYN | RUDP_LSN;
		pktSyn.m_uSeqNo = RUDP_MIN_SEQNO;
		SendPacket(pktSyn, uFromAddr, uFromPort);

		SetSendEvent();
	}

	// Check for FIN packet
	if (packet.m_uFlags & RUDP_FIN)
	{
		if (m_rsState == RS_ESTABLISHED)
		{
			DB("FIN received (remote disconnected) - closing...");

			// Respond to FIN with ACK.
			SendAckPacket(packet.m_uSeqNo);

			RudpNetwork::Close(ES_NOT_CONNECTED);
		}

		return;
	}

	// Check for ACK packet
	if ((packet.m_uFlags & RUDP_ACK) && (packet.m_uAckNo != 0))
	{
		if (m_pSendQ->IsSentPacketSeqNo(packet.m_uAckNo))
		{
			if (m_pSendQ->empty())
			{
				DB_ASSERT(0);
				return;
			}

			while (!m_pSendQ->empty())
			{
				// remove acknowledged packets from queue
				RUDPPacket pktAcked = m_pSendQ->front();
				m_pSendQ->pop_front();

				// An ack'd SYN packet indicates the connection is established
				if (pktAcked.m_uFlags & RUDP_SYN)
				{
					CompleteConnection();
				}

				if (pktAcked.m_uSeqNo == m_uSentSeqNo)
				{
					m_uSentSeqNo = 0;
				}

				if (m_uSentPacketCount > 0)
				{
					m_uSentPacketCount--;
				}

				WindowSizeDebug("Send window: " + 
					itoa(m_uSentPacketCount) + "/" + itoa(m_uWindowSize));

				if (pktAcked.m_uSeqNo == packet.m_uAckNo)
				{
					UpdateDelayValues(pktAcked);
					SetTimeout();
					break;
				}
			}

			NotifyEvent(RE_SEND);
			SetSendEvent(); // send next packet
		}
	}

	if (packet.m_uSeqNo)
	{
		// check packet sequence number
		u_int32_t uExpectedSeqNo = m_uReceivedSeqNo;
		IncrSN(uExpectedSeqNo);

		if (packet.m_uSeqNo != uExpectedSeqNo)
		{
			SetDebugInfo("Received unexpected/duplicate packet SN="
				+ itoa(packet.m_uSeqNo));
			SendAckPacket(m_uReceivedSeqNo);
			return;
		}

		m_uReceivedSeqNo = packet.m_uSeqNo;

		if (RS_OPEN == m_rsState)
		{
			// automatically move from connecting to connected state
			CompleteConnection();
		}

		if (packet.m_sData.length() > 0)
		{
			// Packet has data
			m_pRecvQ->push_back(packet);

			// signal user of RudpNetwork to receive this packet
			NotifyEvent(RE_READ);
		}

		// Reply by sending an ACK to remote host
		SendAckPacket(packet.m_uSeqNo);
	}
}

// Send a packet to the remote host
void RudpNetwork::ProcessSendEvent(void)
{
	//DB("ProcessSendEvent...");

	Critical_Section cs(*m_pMutex);

	if (m_pSendQ->empty())
	{
		m_uTimeout = INFINITE;
		return; // no more packets to send
	}

	if (!(m_uRemoteAddr && m_uRemotePort))
	{
		if (IsConnecting())
		{
			// wait for connection...
			DB_ASSERT(RUDP_DEFAULT_TIMEOUT == m_uTimeout);
			return;
		}

		DB("ProcessSendEvent - No remote addr/port set.");

		m_pSendQ->clear();
		m_uTimeout = INFINITE;
		return;
	}

	RUDPPacketList::iterator iter = m_pSendQ->begin();

	if (m_uSentSeqNo != 0)
	{
		// find last-sent packet
		for (; iter != m_pSendQ->end(); iter++)
		{
			if (iter->m_uSeqNo == m_uSentSeqNo)
			{
				iter++;
				if (iter == m_pSendQ->end())
				{
					// all queued packets have been sent
					return;
				}

				// continue sending from this point
				break;
			}
		}
	}

	if (iter == m_pSendQ->begin())
	{
		if (m_uTimeout == INFINITE)
		{
			SetTimeout();
		}
	}

	for (; iter != m_pSendQ->end(); iter++)
	{
		if (m_uSentPacketCount >= m_uWindowSize)
		{
			WindowSizeDebug("Send window is full: " + 
				itoa(m_uSentPacketCount) + "/" + itoa(m_uWindowSize));
			return;
		}

		iter->m_dwTimeSent = timeGetTime();

		SendPacket(*iter);
		m_uSentSeqNo = iter->m_uSeqNo;
		m_uSentPacketCount++;

		WindowSizeDebug("Send window: " + 
			itoa(m_uSentPacketCount) + "/" + itoa(m_uWindowSize));
	}
}

// Process timeout
void RudpNetwork::ProcessTimeoutEvent(void)
{
	//DB("ProcessTimeoutEvent...");

	Critical_Section cs(*m_pMutex);

	if (m_dwSentEchoRequest > 0)
	{
		if (timeGetTime() - m_dwSentEchoRequest >= RUDP_DEFAULT_TIMEOUT)
		{
			// resend the request
			SendEchoAddressRequest(m_EchoServerAddr);
		}
	}

	if (m_uTimeout == INFINITE)
	{
		// unexpected timeout - with the exception of the SendEchoRequest, above
		return;
	}

	if (m_pSendQ->empty())
	{
		m_uTimeout = INFINITE;
		return;
	}

	// Update time of last send attempt - the packet is sent below.
	// Note: for a connection attempt, this may not correspond to the packet.
	m_pSendQ->front().m_dwTimeSent = timeGetTime();

	if (RS_OPEN == m_rsState)
	{
		if (IsConnecting())
		{
			if (!(m_uRemoteAddr && m_uRemotePort))
			{
				if (m_nTtl < m_nDefaultTtl)
				{
					DWORD dwElapsed = timeGetTime() - m_dwTtlSetTime;
					if (dwElapsed > RUDP_LOW_TTL_PERIOD)
					{
						RestoreDefaultTtl();
					}
				}

				// retry connection attempt
				AttemptConnection();
				return;
			}
		}
		else
		{
			// connection failed
			DB("RudpNetwork - connection failed");
			// indicate a failed connection
			NotifyEvent(RE_CONNECT);
			Close(ES_CANNOT_CONNECT);
			return;
		}
	}

	if (!(m_uRemoteAddr && m_uRemotePort))
		return;

	if (m_rsState == RS_CLOSED)
		return;

	m_uTotalTimeouts++;
	m_bSlowStart = false;

	RUDPPacket& packet = m_pSendQ->front();

	// Resend packet
	if (packet.m_uSends > RUDP_MAX_RESENDS)
	{
		// Remote is probably not responding - give up.
		SetDebugInfo("Max number of resends reached (" + itoa(RUDP_MAX_RESENDS) + ")");
		Close(ES_LOST_CONNECTION);
		return;
	}

	// Double the timeout for each resend
	m_uTimeout = MIN(m_uTimeout * 2, RUDP_MAX_TIMEOUT);

	SetDebugInfo("Resending - Count: " + itoa(packet.m_uSends) 
		+ " Addr: " + intoa(m_uRemoteAddr)
		+ " Timeout: " + itoa(m_uTimeout));

	SendPacket(packet);
	m_uSentSeqNo = packet.m_uSeqNo;
	m_uSentPacketCount = 1;

	m_uWindowSize = MAX(1, m_uWindowSize / 2);
	m_uWindowIncCounter = m_uWindowSize;

	WindowSizeDebug("Timeout - window set to " + itoa(m_uWindowSize));
}

void RudpNetwork::SendPacket(
	RUDPPacket& packet, 
	u_int32_t uAddr, 
	u_int16_t uPort)
{
	Critical_Section cs(*m_pMutex);

	if (uAddr == 0)
	{
		uAddr = m_uRemoteAddr;
	}

	if (uPort == 0)
	{
		uPort = m_uRemotePort;
	}

	if (uAddr == 0 || uPort == 0)
	{
		return;
	}


	/*
#ifdef _DEBUG
	static int iPacketCount = 0;
	WindowSizeDebug("[static] Sent packets: " + itoa(iPacketCount++));
#endif
	*/

#if RUDP_SIMULATE_SEND_ERRORS
#pragma _ebWarning("Debug option enabled - simulating send errors")
	// Simulate errors in the network
	if (rand() < (RAND_MAX * 0.25))
	{
		static int error = 0;
		DB_STAT(error++);
		SetDebugInfo("Simulating Network Error");
		return;
	}
	else
	{
		static int ok = 0;
		DB_STAT(ok++);
	}
#endif

#if RUDP_SIMULATE_DELAY
#pragma _ebWarning("Debug option enabled - simulating network delay")
	static int i = 0;
	if (i++ > 10)
	{
		DWORD dwDelay = (rand() % 200) + 100;
		SetDebugInfo("Simulating Network Delay: " + itoa(dwDelay));
		Sleep(dwDelay);
	}
#endif

	packet.m_uSends++;

	string sBuf;
	packet.Serialize(sBuf);

	int cc = m_pUdpNetwork->SendTo(sBuf.data(), sBuf.length(), uAddr, uPort);
	if (cc < (int)sBuf.length())
	{
		int iError = WSAGetLastError();
	}
}

void RudpNetwork::IncrSN(u_int32_t& x)
{
	x++;
	if (x > m_uMaxSeqNo)
	{
		x = RUDP_MIN_SEQNO;
	}
}

void RudpNetwork::SetTimeout(void)
{
	Critical_Section cs(*m_pMutex);

	if ((m_uAverageRTT != RUDP_UNKNOWN) && (m_uAverageDev != RUDP_UNKNOWN))
	{
		m_uTimeout = RUDP_TIMEOUT_FACTOR * (m_uAverageRTT + (4 * m_uAverageDev));
	}

	if ((m_uTimeout != INFINITE) && (m_uLastTimeout != RUDP_UNKNOWN))
	{
		// This check makes sure the timeout is not decreased by too much.
		m_uTimeout = MAX(m_uTimeout, (m_uLastTimeout * 2) / 3);
	}

	// Ensure timeout is within defined bounds.
	m_uTimeout = MAX(m_uTimeout, RUDP_MIN_TIMEOUT);
	m_uTimeout = MIN(m_uTimeout, RUDP_MAX_TIMEOUT);

	SetDebugInfo("Last Timeout: " + itoa(m_uLastTimeout));
	SetDebugInfo("New Timeout: " + itoa(m_uTimeout));

	m_uLastTimeout = m_uTimeout;
}

void RudpNetwork::UpdateDelayValues(const RUDPPacket& packet)
{
	Critical_Section cs(*m_pMutex);

	if (packet.m_dwTimeSent == 0)
	{
		// not sure when this happens, but it will be ignored for timeout
		// calculations
		return;
	}

	if (packet.m_uSends > 1)
	{
		// This is a special case: we don't know which of the multiple packets
		// was ack'ed.
		return;
	}

	DWORD dwTimeAcked = timeGetTime();
	DWORD dwElapsedTime = dwTimeAcked - packet.m_dwTimeSent;
	
	if (dwElapsedTime > (m_uLastTimeout / RUDP_TIMEOUT_FACTOR))
	{
		TimeoutDebug("Saved-timeout: "
			"SRTT: " + itoa(m_uAverageRTT) + "ms  " +
			"SDev: " + itoa(m_uAverageDev) + "ms  " + 
			"ElapsedTime: " + itoa(dwElapsedTime) + "ms");
	}

	if (dwElapsedTime >= 10000)
	{
		SetDebugInfo("RTT was unexpectedly large");
		dwElapsedTime = 10000;
	}

	if (dwElapsedTime <= 2)
	{
		SetDebugInfo("RTT was unexpectedly small");
		dwElapsedTime = 2;
	}

	if (m_uAverageRTT == RUDP_UNKNOWN)
	{
		m_uAverageRTT = dwElapsedTime;
	}
	else
	{
		const float fX = 0.125;
		m_uAverageRTT = (u_int32_t)(((1.0f - fX) * m_uAverageRTT) + (fX * dwElapsedTime));
	}

	int iDev = fabs(dwElapsedTime - m_uAverageRTT);
	
	if (m_uAverageDev == RUDP_UNKNOWN)
	{
		m_uAverageDev = (u_int32_t)(0.25 * m_uAverageRTT);
	}
	else
	{
		const float fX = 0.25;
		m_uAverageDev = (u_int32_t)(((1.0f - fX) * m_uAverageDev) + (fX * iDev));
	}

	SetDebugInfo("New RTT: " + itoa(dwElapsedTime));
	DB_ASSERT(dwElapsedTime);
	SetDebugInfo("Average Dev: " + itoa(m_uAverageDev));
	SetDebugInfo("Average RTT: " + itoa(m_uAverageRTT));

	UpdateWindowSize(dwElapsedTime);
}

void RudpNetwork::UpdateWindowSize(DWORD dwDelay)
{
	if (dwDelay > (RUDP_MAX_DELAY / 2))
	{
		// disable TCP "slow-start" to avoid excessive delay
		m_bSlowStart = false;
	}

	if (dwDelay <= RUDP_MAX_DELAY)
	{
		// Note: check that window really is utilized.
		if ((m_uSentPacketCount + 1) < (m_uWindowSize / 2))
			return;

		// Linear window size increase
		m_uWindowIncCounter--;
		if (0 == m_uWindowIncCounter)
		{
			m_uWindowSize = MIN(m_uWindowSize + 1, RUDP_MAX_WINDOW);
			m_uWindowIncCounter = m_uWindowSize;
			
			if (m_bSlowStart)
			{
				// perform TCP "slow-start"
				m_uWindowIncCounter = 1;
			}
			
			WindowSizeDebug("Window size increased to " + 
				itoa(m_uWindowSize));
		}
	}
	else
	{
		// (half-speed) linear window size decrease
		m_uWindowIncCounter++;
		if (m_uWindowIncCounter >= (3 * m_uWindowSize))
		{
			if (m_uWindowSize > 1)
			{
				m_uWindowSize--;
			}
			m_uWindowIncCounter = m_uWindowSize;

			WindowSizeDebug("Window size decreased to " + 
				itoa(m_uWindowSize));
		}
	}
}

void RudpNetwork::QueueForSending(RUDPPacket& packet)
{
	Critical_Section cs(*m_pMutex);

	// Assign sequence number.
	packet.m_uSeqNo = m_uSeqNo;
	IncrSN(m_uSeqNo);

	m_pSendQ->push_back(packet);
	//DB_STAT(m_pSendQ->back().m_uSeqNo);

	SetSendEvent();
}

void RudpNetwork::SendAckPacket(u_int32_t uSeqNo)
{
	RUDPPacket pktAck;
	pktAck.m_uAckNo = uSeqNo;
	pktAck.m_uFlags = RUDP_ACK;
	SendPacket(pktAck);
}

void RudpNetwork::SendSynPacket(u_int32_t uAddr, u_int16_t uPort)
{
	RUDPPacket pktSyn;
	pktSyn.m_uFlags = RUDP_SYN | RUDP_LSN;
	SendPacket(pktSyn, uAddr, uPort);

#if 1 //for Linux yamada
#ifndef SLEEP_DISABLE
	usleep(RUDP_OPEN_DELAY*1000);
#else
	usleep(RUDP_OPEN_DELAY);
#endif
#else
	Sleep(RUDP_OPEN_DELAY);
#endif //for Linux yamada
}

void RudpNetwork::SendFinPacket(void)
{
	RUDPPacket pktFin;
	pktFin.m_uFlags = RUDP_FIN;
	SendPacket(pktFin);
}

void RudpNetwork::QueueSynPacket(void)
{
	RUDPPacket pktSyn;
	pktSyn.m_uFlags = RUDP_SYN | RUDP_LSN;
	QueueForSending(pktSyn);
}

void RudpNetwork::ResetConnectionState(void)
{
	Critical_Section cs(*m_pMutex);

	m_pSendQ->clear();
	m_pRecvQ->clear();

	m_uMaxSeqNo = RUDP_DEFAULT_MAX_SEQNO;
	m_uSentSeqNo = 0;
	m_uSentPacketCount = 0;
	m_uWindowSize = 1;
	m_uWindowIncCounter = 1;

	m_uReceivedSeqNo = 0;
	m_uRemoteAddr = 0;
	m_uRemotePort = 0;
	m_uRemoteAddr1 = 0;
	m_uRemoteAddr2 = 0;
	m_DetectedRemoteAddr = AddressPortPair(0, 0);
	m_bRemoteAddressTranslated = true;
	m_uOpenExpiryTime = 0;
	m_uOpenStartTime = 0;

	m_uTimeout = INFINITE;
	m_uLastTimeout = RUDP_UNKNOWN;
	m_uAverageRTT = RUDP_UNKNOWN;
	m_uAverageDev = RUDP_UNKNOWN;
	m_uSeqNo = RUDP_MIN_SEQNO;

	m_uTotalTimeouts = 0;
	m_bSlowStart = true;
}

void RudpNetwork::CompleteConnection(void)
{
	DB_ENTER(RudpNetwork::CompleteConnection);

	m_dwSentEchoRequest = 0;

	if (RS_ESTABLISHED == m_rsState)
	{
		DB("already in established state");
		return;
	}

	if (RS_OPEN != m_rsState)
	{
		DB_ASSERT(0 && "unexpected state");
		return;
	}

	DB("RudpNetwork - connection established.");

	// set connect flag
	m_rsState = RS_ESTABLISHED;

	// Reset certain statistics
	m_uTotalTimeouts = 0;

	NotifyEvent(RE_CONNECT);

	SetEvent(m_hConnectEvent);
}

bool RudpNetwork::IsConnecting(void) const
{
	Critical_Section cs(*m_pMutex);

	if (RS_OPEN != m_rsState)
		return false;

	if (0 == m_uOpenStartTime)
		return false;

	if (0 == m_uOpenExpiryTime)
		return false;

	if (timeGetTime() > m_uOpenExpiryTime)
	{
		// time-out expired
		return false;
	}

	return true;
}

void RudpNetwork::SetDebugInfoImpl(const string& sText)
{
	Critical_Section cs(*m_pMutex);

#if RUDP_OUTPUT_DEBUG >= 2
	string sTime = itoa(timeGetTime() % 100000);
#if RUDP_OUTPUT_DEBUG >= 4
	DB_AND_LOG(sTime + _T(" RUDP: ") + sText);
#elif RUDP_OUTPUT_DEBUG >= 3
	PRINT(sTime + _T(" RUDP: ") + sText);
#else
	DB((sTime + " RUDP: " + sText).c_str());
#endif
#endif

#if RUDP_OUTPUT_DEBUG >= 1
	if (m_sDebugInfo.length() > 2048)
	{
		m_sDebugInfo.erase();
	}

	if (m_sDebugInfo.length() > 0)
	{
		m_sDebugInfo += "\r\n";
	}

	m_sDebugInfo += "DB: " + sText;
	
	NotifyEvent(RE_DEBUGINFO);
#endif
}

u_int32_t RudpNetwork::GetTotalTimeouts(void)
{
	Critical_Section cs(*m_pMutex);

	return m_uTotalTimeouts;
}

u_int32_t RudpNetwork::GetTimeoutPeriod(void)
{
	Critical_Section cs(*m_pMutex);

	// m_uLastTimeout is updated every time m_uTimeout is updated.
	return m_uLastTimeout;
}

u_int16_t RudpNetwork::GetWindowSize(void)
{
	Critical_Section cs(*m_pMutex);

	return m_uWindowSize;
}

u_int32_t RudpNetwork::GetAverageRtt(void)
{
	Critical_Section cs(*m_pMutex);

	return m_uAverageRTT;
}

void RudpNetwork::NotifyEvent(u_int16_t uEvent)
{
	Critical_Section cs(*m_pMutex);

	m_uEventFlags |= uEvent;

	if ((NULL != m_hNotifyEvent) && (m_uEventFilter & uEvent))
	{
		SetEvent(m_hNotifyEvent);
	}
}

void RudpNetwork::RestoreDefaultTtl(void)
{
	// restore default TTL
	if (m_nDefaultTtl <= 0)
	{
		DB_ASSERT(0);
		return;
	}

	if (m_nTtl >= m_nDefaultTtl)
		return;  // already restored

	SharedPtr<ESocket> pSocket;
	if (!m_pUdpNetwork->GetSharedSocket(pSocket))
	{
		DB_ASSERT(0);
		return;
	}

	m_nTtl = m_nDefaultTtl;
	pSocket->SetTtl(m_nTtl);
	m_dwTtlSetTime = 0;
}

bool RudpNetwork::IsPortIncremented(void) const
{
	Critical_Section cs(*m_pMutex);

	return m_bPortIncremented;
}

void RudpNetwork::NotifyEchoResponse(void)
{
	// check that echo response is complete
	if ((0 == m_DetectedLocalAddr.first) || (0 == m_DetectedLocalAddr.second))
		return;

	if ((0 == m_DetectedLocalAddr2.first) || (0 == m_DetectedLocalAddr2.second))
		return;

	if (m_DetectedLocalAddr.second != m_DetectedLocalAddr2.second)
	{
#ifdef _DEBUG
		MSG_BOX("Debug info: merged port property is false.");
#endif
		m_bPortIncremented = true;
	}
	else
	{
		m_bPortIncremented = false;
	}

	if (m_dwSentEchoRequest > 0)
	{
		m_dwSentEchoRequest = 0;  // prevent multiple notifications
		NotifyEvent(RE_ECHO_ADDRESS);
	}
}

bool RudpNetwork::IsInSameLAN() const
{
	if (m_uRemoteAddr1 == m_uRemoteAddr2)
	{
		// remote is not even on a LAN
		return false;
	}
	
	if (0 == m_uRemoteAddr)
		return false;

	//if (0 == m_DetectedRemoteAddr.first)
	//	return false;

	//return (m_uRemoteAddr != m_DetectedRemoteAddr.first);

	return (m_uRemoteAddr != m_uRemoteAddr2);
}

void RudpNetwork::FlushSendQueue(void)
{
	if (RS_ESTABLISHED != m_rsState)
		return;  // only flush if connection is established

	// note: no critical sections should be in this function, as it's waiting
	for (int i = 0; i < 40; i++)
	{
		if (GetSendQueueSize() <= 0)
			break;

#if 0 //for Linux yamada
		OutputDebugString(_T("Waiting for RUDP network to finish sending..."));
#endif //for Linux yamada
		SetSendEvent();
#if 1 //for Linux yamada
#ifndef SLEEP_DISABLE
		usleep(100*1000);
#else
		usleep(100);
#endif
#else
		Sleep(100);
#endif //for Linux yamada
	}
}


void RudpNetwork::SetRemoteBehindFirewall(bool bFirewall)
{
	m_bRemoteBehindFirewall = bFirewall;
}


void RudpNetwork::SetLocalBehindFirewall(bool bFirewall)
{
	m_bLocalBehindFirewall = bFirewall;
}

void RudpNetwork::UseFirewallServer()
{
	m_bUseFirewallServer = true;
}

void RudpNetwork::SetSendEvent()
{
#ifdef _WIN32_WCE
	m_pDisposableSocket->Destroy();
#endif
	SetEvent(m_hSendEvent);
}
