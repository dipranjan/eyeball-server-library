// CoSslSocket.cpp : Implementation of CoSslSocket
#include <sys/types.h>
#include <sys/socket.h>

#include <sys/stat.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/time.h>
#include <unistd.h>
#include<poll.h>

#include "Base.h"

#include "CoSslSocket.h"
#include "debug.h"
#include "SmartPtr.h"

//#define E_FAIL -1
#define S_OK    0
#define INVALID_SOCKET -1

/////////////////////////////////////////////////////////////////////////////
// CoSslSocket

CoSslSocket::CoSslSocket()
:
	m_SslSocket()
{
	DB("CoSslSocket object constructed.");
}

int CoSslSocket::Open(
	unsigned long uRemoteAddr, 
	unsigned short uRemotePort, 
	unsigned short uLocalPort, 
	unsigned long hNotifyEvent)
{
	struct sockaddr_in sin;
	memset((char *)&sin, 0, sizeof(sin));
	sin.sin_family = AF_INET;
	sin.sin_port = uRemotePort;	
	sin.sin_addr.s_addr = uRemoteAddr;

	int fd = ::socket(PF_INET, SOCK_STREAM, 0);
	if (fd == -1)
	{
		DB("can't open socket from system");
		return -1;
	}

	if (::connect(fd, (sockaddr*)&sin, sizeof(sin)) != 0)
	{
		DB("connect to " << ntohl(uRemoteAddr) << ":" << ntohs(uRemotePort) << " failed: " << strerror(errno));
		CLOSE_SOCKET(fd);
		return -1;
	}

#ifndef USE_MY_SSL
	try
	{
		m_SslSocket.connect(fd);
		m_SslSocket.finishNegotiation(30*1000);
	}
	catch (IOException ex)
	{
		DB("Could not connect " << ex.getDetails());
		//UNREFERENCED_PARAMETER(ex);
		CLOSE_SOCKET(fd);
		return E_FAIL;
	}
	catch (TimeoutExceededException ex)
	{
		DB("Timeout while negotiation SSL connection.  " << ex.getDetails());
		//UNREFERENCED_PARAMETER(ex);
		CLOSE_SOCKET(fd);
		return E_FAIL;
	}

	if (hNotifyEvent != NULL)
	{
		#if 0
		if (WSAEventSelect(fd, (HANDLE)hNotifyEvent, FD_READ | FD_CLOSE) != 0)
		{
			DB("WSAEventSelect failed");
			Close();
			CLOSE_SOCKET(fd);
			return E_FAIL;
		}
		#endif
	}
	else
	{
		//do we need to make it blocking explicitly?
		// Set socket to blocking
		//u_long flag = 0;
		//ASSERT(ioctlsocket(fd, FIONBIO, &flag) == 0);
	}
#else
	return m_SslSocket.connect(fd);
#endif

	DB("succeed  " );
	return S_OK;
}

int CoSslSocket::Close()
{
	m_SslSocket.close(); 

	return S_OK;
}
int CoSslSocket::Send(const std::string& sBuf)
{
#ifndef USE_MY_SSL
	try
	{
		m_SslSocket.write(sBuf.c_str(),sBuf.size());
	}
	catch (IOException)
	{
		return E_FAIL;
	}
	catch (TimeoutExceededException)
	{
		return E_FAIL;
	}

#else
		m_SslSocket.write(sBuf.c_str(),sBuf.size());
#endif
	return S_OK;
}

int CoSslSocket::Recv(long nBytesToRecv, std::string& sBuf)
{
	SharedPtr<char> pBuf(new char[nBytesToRecv]);
	
	int nBytesReceived = 0;
#ifndef USE_MY_SSL
	try
	{
		nBytesReceived = m_SslSocket.readWithTimeout(pBuf.get(), nBytesToRecv, 0);
		if (0 == nBytesReceived)
			return E_FAIL;
	}
	catch (IOException)
	{
		return E_FAIL;
	}
	catch (TimeoutExceededException)
	{
		nBytesReceived = 0;
	}
#else
		nBytesReceived = m_SslSocket.read(pBuf.get(), nBytesToRecv);
#endif
	if( nBytesReceived > 0)
		sBuf.assign(pBuf.get(),nBytesReceived);

	return S_OK;
}

int CoSslSocket::GetSocket(long* pSocket)
{
	SOCKET sock = m_SslSocket.getFd();
	if (INVALID_SOCKET == sock)
		return E_FAIL;

	*pSocket = (long)sock;

	return S_OK;
}
