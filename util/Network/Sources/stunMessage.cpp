#include "Base.h"
#include "stunMessage.h"
#include "Serializable.h"
#include "Timer.h"

using std::map;
using std::string;

// ---------------------------------------------------------------------------
// Definition
// ---------------------------------------------------------------------------

// message type definition
#define UNKNOWN_MESSAGE					0
#define BINDING_REQUEST					0x0001
#define BINDING_RESPONSE				0x0101
#define BINDING_ERROR_RESPONSE			0x0111
#define SHARED_SECRET_REQUEST			0x0002
#define SHARED_SECRET_RESPONSE			0x0102
#define SHARED_SECRET_ERROR_RESPONSE	0x0112


// message attribute types
#define MAPPED_ADDRESS					0x0001
#define RESPONSE_ADDRESS				0x0002
#define CHANGE_REQUEST					0x0003
#define SOURCE_ADDRESS					0x0004
#define CHANGED_ADDRESS					0x0005
#define USERNAME						0x0006
#define PASSOWRD						0x0007
#define MESSAGE_INTEGRITY				0x0008
#define ERROR_CODE						0x0009
#define UNKNOWN_ATTRIBUTES				0x000a
#define REFLECTED_FROM					0x000b


// ---------------------------------------------------------------------------
// Constructor/Destructor
// ---------------------------------------------------------------------------
CStunMessage::CStunMessage():
	m_iMessageType(UNKNOWN_MESSAGE),
	m_attrMap()
{
	memset(m_szTransactionID, 0, sizeof(m_szTransactionID));
}

CStunMessage::~CStunMessage()
{
	m_attrMap.clear();
}

// ---------------------------------------------------------------------------
// Interfaces
// ---------------------------------------------------------------------------
std::string CStunMessage::CreateBindingRequest()
{
	m_attrMap.clear();
	m_iMessageType = BINDING_REQUEST;

	// optional change request
	int i = 0;
	string s;
	s.assign((char*)&i, sizeof(int));

	m_attrMap[CHANGE_REQUEST] = s;

	return CreateRequest();
}

bool CStunMessage::GetMappedAddr(const string& sResponse, AddressPortPair& addr)
{
	addr = GetAddress(sResponse, MAPPED_ADDRESS);
	return addr != AddressPortPair(0, 0);
}

bool CStunMessage::GetChangedAddr(const string& sResponse, AddressPortPair& addr)
{
	addr =  GetAddress(sResponse, CHANGED_ADDRESS);
	return addr != AddressPortPair(0, 0);
}

// ---------------------------------------------------------------------------
// Helper
// ---------------------------------------------------------------------------
std::string CStunMessage::CreateRequest()
{
	string sBuf;

	// message type
	Serialize16(m_iMessageType, sBuf);
	
	// calculate the message length
	unsigned short iLen = 0;
	MessageAttribuiteMap::iterator iter = m_attrMap.begin();
	for (; iter != m_attrMap.end(); iter++)
	{
		string& sValue = iter->second;

		iLen += 4; // type + len;
		iLen += sValue.size();
	}

	// message length
	Serialize16(iLen, sBuf);

	// transaction ID


	sprintf(m_szTransactionID, "%016u", timeGetTime());
	SerializeCString(m_szTransactionID, 16, sBuf);

	// payload
	iter = m_attrMap.begin();
	for (; iter != m_attrMap.end(); iter++)
	{
		string& sValue = iter->second;
		Serialize16(iter->first, sBuf);
		Serialize16(sValue.size(), sBuf);
		SerializeCString(sValue.c_str(), sValue.size(), sBuf);
	}

	return sBuf;
}

bool CStunMessage::ParseResponse(const std::string& sResponse)
{
	m_iMessageType = UNKNOWN_MESSAGE;
	m_attrMap.clear();

	if (sResponse.size() < 20)
		return false;

	int iOffset = 0;

	unsigned short iLength = 0;
	Deserialize16(m_iMessageType, sResponse, iOffset);
	Deserialize16(iLength, sResponse, iOffset);
	DeserializeCString(m_szTransactionID, 16, sResponse, iOffset);

	if (iLength == 0)
	{
		DB_ASSERT(sResponse.size() == 20);
		return true;
	}

	string sAttributes = sResponse.substr(20);
	if (sAttributes.size() < iLength)
		return false;

	while (sAttributes.size() >= 4)
	{
		iOffset = 0;
		unsigned short iType = 0;
		Deserialize16(iType, sAttributes, iOffset);

		unsigned short iLen = 0;
		Deserialize16(iLen, sAttributes, iOffset);

		if (sAttributes.size() < 4 + iLen)
			return false;

		char* sz = new char [iLen];
		DeserializeBytes(sz, iLen, sAttributes, iOffset);

		string s;
		s.assign(sz, iLen);
		delete sz;

		m_attrMap[iType] = s;

		sAttributes.erase(0, iLen + 4);
	}

	if (!sAttributes.empty())
	{
		DB_ASSERT(0);
		return false;
	}

	return true;
}


AddressPortPair CStunMessage::ParseAddress(const string& sAddress)
{
	AddressPortPair addr;

	if (sAddress.size() != 8)
	{
		DB_ASSERT(0);
		if (sAddress.size() < 8)
			return addr;
	}

	int iOffset = 0;

	unsigned char uIgnoed = 0;
	Deserialize8(uIgnoed, sAddress, iOffset);

	unsigned char uFamily = 0;
	Deserialize8(uFamily, sAddress, iOffset);
	DB_ASSERT(uFamily == 0x01);

	unsigned short uPort = 0;
	Deserialize16(uPort, sAddress, iOffset);

	unsigned int uAddr = 0;
	Deserialize32(uAddr, sAddress, iOffset);

	// return result in network byte order
	addr.first = htonl(uAddr);
	addr.second = htons(uPort);

	return addr;
}

AddressPortPair CStunMessage::GetAddress(const string& sResponse, unsigned short iMessageType)
{
	AddressPortPair addr;

	if (!ParseResponse(sResponse))
		return addr;

	if (m_attrMap.find(iMessageType) == m_attrMap.end())
		return addr;

	string sAddress = m_attrMap[iMessageType];

	return ParseAddress(sAddress);
}
