// ---------------------------------------------------------------------------
// File:       TcpNetwork.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    
// 
// Change Log:
// ---------------------------------------------------------------------------

#include "Base.h"  // automatic use of precompiled headers 

#if 1 //for Linux yamada
#include <queue>
#include <sys/socket.h>
#include <netinet/tcp.h>
#include <unistd.h>
#include "inet.h"
#include "Event.h"
#endif //for Linux yamada

#include "critsec.h"
#include "debug.h"

#if 1 //for Linux yamada
#include "ELinuxUtil.h"
#else
#include "EWinUtil.h"
#endif //for Linux yamada

#include "ENetwork.h"
#include "ReturnCodes.h"
#include "Thread.h"
#include "TextUtil.h"
#include "Timer.h"
#include "TcpNetwork.h"
#include "EUtil.h"
#include "TurnMessage.h"

using std::string;
using namespace eyeball;
using namespace eyeballTurn;
#define DB_TCPN 

// ---------------------------------------------------------------------------
// Defines
// ---------------------------------------------------------------------------

#define TCP_CONNECT_TIMEOUT  30000  // ms

// Winsock send/recv buffer size to be used for data sockets.  This affects 
// performance and memory usage.
#define TCP_MIN_SOCKET_BUFFER_SIZE  (2 * 1024)
#define TCP_SOCKET_BUFFER_SIZE      (64 * 1024)
#define TCP_MAX_BUFFER_SIZE_MSEC    (500)

#if 1 //for Linux yamada
#define SOCKET_ERROR (-1)
#endif //for linux yamada

SendBuffer::SendBuffer()
:
	m_nSize(0),
	m_DataQ()
{
}

void SendBuffer::AppendPacket(const string& sBuf)
{
	m_DataQ.push(sBuf);
	m_nSize += sBuf.size();
}

void SendBuffer::Clear()
{
	while (!m_DataQ.empty())
	{
		m_DataQ.pop();
	}

	m_nSize = 0;
}

bool SendBuffer::Empty() const
{
	DB_ASSERT((0 == m_nSize) == (m_DataQ.empty()));
	return (0 == m_nSize);
}

bool SendBuffer::GetPacket(string& sBuf) const
{
	if (m_DataQ.empty())
		return false;

	sBuf = m_DataQ.front();
	DB_ASSERT(sBuf.size() > 0);

	return true;
}

void SendBuffer::RemoveBytes(unsigned int nLen)
{
	int iBytesRemaining = nLen;
	while (iBytesRemaining > 0)
	{
		string& sBuf = m_DataQ.front();
		int nErase = MIN((int)sBuf.size(), iBytesRemaining);
		
		string sTemp = sBuf.substr(nErase);
		sBuf = sTemp;
		
		if (sBuf.empty())
		{
			m_DataQ.pop();
		}
		iBytesRemaining -= nErase;
		m_nSize -= nErase;
	}
	DB_ASSERT(0 == iBytesRemaining);
}

unsigned int SendBuffer::Size(void) const
{
	return m_nSize;
}


// ---------------------------------------------------------------------------
// Class implementation
// ---------------------------------------------------------------------------

TcpNetwork::TcpNetwork()
:		
	m_ConnSock(INVALID_SOCKET),		
	m_uRemoteAddr(0),
	m_uRemotePort(0),
	m_hNotifyEvent(NULL),
	m_pMutex(new Mutex),
	m_pSendBuffer(new SendBuffer),
	m_bDataChannel(false),
	m_dwConnectTime(0),
	m_bCloseReceived(false)
{
}

TcpNetwork::~TcpNetwork()
{
	// close the socket
	Close();
}

void TcpNetwork::nodelay(SOCKET fd)
{
	BOOL one = TRUE;
	if (setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, (char *) &one, sizeof(one)) != 0)
	{
//p		DB(_strerror("setsockopt: TCP_NODELAY"));
	}
}

string TcpNetwork::GetConnectionError(const string& sAddress, u_int16_t uPort)
{
	string sError;
	DWORD dwAddr = LookupHostAddr(sAddress);
	if (sAddress.empty())
	{
		sError = "Invalid service address.";
	}
	else if (dwAddr == 0)
	{
		sError = "Failed to resolve address ";
		sError += sAddress;
	}
	else
	{
		sError = "Failed to connect to ";
		sError += sAddress;
		sError += ":";
		sError += itoa(uPort);
	}
	return sError;
}


bool TcpNetwork::Open(
	u_int32_t uRemoteAddr, 
	u_int16_t uRemotePort, 
	u_int16_t uLocalPort,
	HANDLE hNotifyEvent) 
{
	//DB_ENTER(TcpNetwork::Open);

	DB_ASSERT(m_ConnSock == INVALID_SOCKET);

	if (0 == uRemoteAddr)
	{
		DB_ASSERT(0 && "Invalid remote address");
#if 1 //for Linux yamada
		SetLastError(EDESTADDRREQ);
#else
		SetLastError(WSAEDESTADDRREQ);
#endif //for Linux yamada
		return false;
	}

	SOCKET fd = socket(AF_INET, SOCK_STREAM, 0);
	if (fd == INVALID_SOCKET)
	{
	//p	DB(_strerror("socket"));
		return false;
	}

	{
		Critical_Section cs(*m_pMutex);

		// Commit to member variables
		m_uRemoteAddr = uRemoteAddr;
		m_uRemotePort = uRemotePort;
		
		m_ConnSock = fd;
		fd = INVALID_SOCKET;

	//p	DB_ASSERT(hNotifyEvent != NULL);
		m_hNotifyEvent = hNotifyEvent;
	}

	// Set socket to non-blocking
	if(hNotifyEvent == NULL ) //sazzad. 21_12
		Network::Nonblock(m_ConnSock);

	if (!m_bDataChannel)
	{
		// Disable Nagle's algorithm for small command messages.
		nodelay(m_ConnSock);
	}

	bool bRet = SetSocketBufferSize();
	if (!bRet)
	{
#ifndef _WIN32_WCE
		MSG_BOX("Warning: could not set the network socket buffer size.");
#endif
	}

	if (!DoConnect(uRemoteAddr, uRemotePort))
	{
		Close();
		return false;
	}

	return true;
}

bool TcpNetwork::OpenWait(
	u_int32_t uRemoteAddr, 
	u_int16_t uRemotePort,
	u_int16_t uLocalPort,
	HANDLE hNotifyEvent,
	UINT uTimeOut)
{
	bool bRet = Open(
		uRemoteAddr,
		uRemotePort,
		uLocalPort,
		hNotifyEvent);

	if (!bRet)
		return false;

	// Wait up to uTimeOut for connection to complete
#if 1 //for Linux yamada
	DWORD dwWait = CustomWaitForSingleObject(hNotifyEvent, (DWORD)uTimeOut);
	if (dwWait != 0)
#else
	DWORD dwWait = CustomWaitForSingleObject(hNotifyEvent, uTimeOut);
	if (dwWait != WAIT_OBJECT_0)
#endif //for Linux yamada
	{
		Close();
		return false;
	}

	if (!HandleConnectEvent())
	{
		// object already closed
		return false;
	}

	if (!IsConnected())
	{
		Close();
		return false;
	}
	
	return true;
}

void TcpNetwork::Close(void)
{	
	// Since close is often called on failure, preserve the last error number
	int iErr = GetLastError();

	//DB_ENTER(TcpNetwork::Close);
	Critical_Section cs(*m_pMutex);

	m_bCloseReceived = false;
	m_dwConnectTime = 0;

	if (m_ConnSock != INVALID_SOCKET)
	{
		if (NULL != m_hNotifyEvent)
		{
			WSAEventSelect(m_ConnSock, m_hNotifyEvent, 0);
			m_hNotifyEvent = NULL;
		}
		
		CLOSE_SOCKET(m_ConnSock);
	}

	m_pSendBuffer->Clear();
	SetLastError(iErr);

}

// returns negative value for error
//
int TcpNetwork::Send(const char *pBuf, int iLen) 
{
	DB_TCPN("Sawamura TcpNetwork::Send\n");
	//DB_ENTER(TcpNetwork::Send);	

	Critical_Section cs(*m_pMutex);

	if (m_ConnSock == INVALID_SOCKET)
	{
		DB("socket is invalid");
		return ES_NOT_CONNECTED;
	}

	DB_TCPN("Sawamura TcpNetwork::Send1\n");
	AppendToSendBuffer(string(pBuf, iLen));

	if (!SendBufferedData())
	{
		int iErr = WSAGetLastError();
		DB_ASSERT(0 != iErr);
#if 1 //for Linux yamada
		if (EWOULDBLOCK != iErr)
#else
		if (WSAEWOULDBLOCK != iErr)
#endif //for Linux yamada
		{
			DB("socket send error " << iErr);
			return ES_NOT_CONNECTED;
		}
	}

	DB_TCPN("Sawamura TcpNetwork::Send2\n");
	return ES_SUCCESS;
}

int TcpNetwork::Recv(char *pBuf, int iLen)
{	
	DB_TCPN("Sawamura TcpNetwork::Recv\n");

	Critical_Section cs(*m_pMutex);

	if (m_hNotifyEvent == NULL)
	{
	DB_TCPN("Sawamura TcpNetwork::Recv m_hNotifyEvent == NULL\n");
		// not using WSAEventSelect
		return DoRecv(pBuf, iLen);
	}

	if (m_ConnSock == INVALID_SOCKET) 
	{
	DB_TCPN("Sawamura TcpNetwork::Recv ES_NOT_CONNECTED\n");
		return ES_NOT_CONNECTED;
	}

	DB_TCPN("Sawamura TcpNetwork::Recv EnumNetworkEvents IN\n");
	// check what type of event we were woken up for
	WSANETWORKEVENTS netEvents;	
	if (!EnumNetworkEvents(netEvents))
		return ES_FAILURE;

	DB_TCPN("Sawamura TcpNetwork::Recv EnumNetworkEvents OK\n");
	if (netEvents.lNetworkEvents & FD_CONNECT)
	{
		DB_TCPN("Received FD_CONNECT event");
		if (!HandleConnectEvent(netEvents))
		{
			// connection error
			// object already closed
			return ES_NOT_CONNECTED;
		}
	}

	if (netEvents.lNetworkEvents & FD_WRITE)
	{
		SendBufferedData();

		// The function may fail if there is no data to be received.
		// The owner of this object must be ready to handle ES_FAILURE
		// as normal behavior.
	}

	// we received a close event
	if (netEvents.lNetworkEvents & FD_CLOSE)
	{
		DB_TCPN("TcpNetwork::recv() caught FD_CLOSE");
		m_bCloseReceived = true;

		// note: there may be more incoming data...
	}

	if (netEvents.lNetworkEvents & FD_READ)
	{
DB_TCPN("Sawamura TcpNetWork::Recv DoRecv 0\n");
		int iRet = DoRecv(pBuf, iLen);
DB_TCPN("Sawamura TcpNetWork::Recv DoRecv 1\n");
		if (m_bCloseReceived)
		{
			// Recv() should be called again to close the socket
			SetEvent(m_hNotifyEvent);

			// possible bug fix
			if (ES_NOT_CONNECTED == iRet)
			{
				Close();
				return ES_NOT_CONNECTED;
			}
		}

		return iRet;
	}

	if (m_bCloseReceived)
	{
		DB_TCPN("Closing TCP socket in response to received close event");
		Close();
		return ES_NOT_CONNECTED;
	}

	DB_TCPN("Sawamura TcpNetwork::Recv End buf = %s\n",pBuf);
	return ES_FAILURE;

}

bool TcpNetwork::HandleConnectEvent(void)
{
	Critical_Section cs(*m_pMutex);

	WSANETWORKEVENTS netEvents;	
	if (0 != WSAEnumNetworkEvents(m_ConnSock, m_hNotifyEvent, &netEvents))
	{
		DB_ASSERT(0 && "WSAEnumNetworkEvents failed");
		Close();
		return false;
	}

	return HandleConnectEvent(netEvents);
}

bool TcpNetwork::HandleConnectEvent(const WSANETWORKEVENTS& netEvents)
{
	Critical_Section cs(*m_pMutex);

	if (!(netEvents.lNetworkEvents & FD_CONNECT))
	{
		DB_ASSERT(0 && "FD_CONNECT not received");
		return false;
	}

	int iErrorCode = netEvents.iErrorCode[FD_CONNECT_BIT];
	if (0 != iErrorCode)
	{
		Close();
		WSASetLastError(iErrorCode);

		return false;
	}

	DB_ASSERT(NULL != m_hNotifyEvent);
	
	// Call virtual method
	if (!OnConnect())
	{
		DB("virtual OnConnect failed");
		Close();
		return false;
	}

	// Wait for read, write and close events
	long lNetworkEvents = FD_READ | FD_WRITE | FD_CLOSE;
	if (WSAEventSelect(m_ConnSock, m_hNotifyEvent, lNetworkEvents) != 0)
	{
		DB("WSAEventSelect failed");
		Close();
		return false;
	}
	
	return true;
}

u_int16_t TcpNetwork::GetLocalPort(void)
{
	return Network::GetSockName(m_ConnSock).second;
}

void TcpNetwork::SetGlobalLocalAddr(void)
{
	//DB_ENTER(TcpNetwork::SetGlobalLocalAddr);

	Critical_Section cs(*m_pMutex);

	SetLocalAddr(0);

	// If there is a connected socket, use it to set the local address
	if (INVALID_SOCKET == m_ConnSock)
		return;

	u_int32_t uAddr = Network::GetSockName(m_ConnSock).first;
	if (0 == uAddr)
		return;

	SetLocalAddr(uAddr);
}

SOCKET TcpNetwork::GetSocket(void)
{ 
	Critical_Section cs(*m_pMutex);

	return m_ConnSock; 
}

void TcpNetwork::SetSocket(SOCKET sock) 
{ 
	//DB_ENTER(TcpNetwork::SetSocket);

	Critical_Section cs(*m_pMutex);

	m_ConnSock = sock;
}

bool TcpNetwork::IsConnected(void)
{
	Critical_Section cs(*m_pMutex);

	return Network::IsConnected(m_ConnSock);
}

int TcpNetwork::GetSendBufferSize(void) const
{
	Critical_Section cs(*m_pMutex);

	return m_pSendBuffer->Size();
}

bool TcpNetwork::SetSendBufferSize(int iSize)
{
	if (m_ConnSock == INVALID_SOCKET)
		return false;

	return Network::SetSendBufferSize(m_ConnSock, iSize);
}

bool TcpNetwork::IsSocketWritable(int iTimeout)
{
	if (m_ConnSock == INVALID_SOCKET)
		return false;

	fd_set set;
	FD_ZERO(&set);
	FD_SET(m_ConnSock, &set);

	// waiting for specified seconds
	timeval tv;
	tv.tv_sec = iTimeout;
	tv.tv_usec = 0;

	bool b =  select(m_ConnSock + 1, NULL, &set, NULL, &tv) == 1; 

	FD_CLR(m_ConnSock, &set);

	return b;
}


void TcpNetwork::SetDataChannel(bool bDataChannel)
{
	Critical_Section cs(*m_pMutex);

	m_bDataChannel = bDataChannel;
}

void TcpNetwork::SetNotifyEvent(HANDLE hEvent)
{
	Critical_Section cs(*m_pMutex);

	m_hNotifyEvent = hEvent;

	long lNetworkEvents = FD_READ | FD_WRITE | FD_CLOSE;
	if (WSAEventSelect(m_ConnSock, hEvent, lNetworkEvents) != 0)
	{
		DB_ASSERT(0 && "WSAEventSelect failed");
		return;
	}
}

bool TcpNetwork::DoConnect(u_int32_t uRemoteAddr, u_int16_t uRemotePort)
{
//p	DB_ASSERT(NULL != m_hNotifyEvent); // 21_12
	if(NULL != m_hNotifyEvent) //added by sazzad.
		WSAEventSelect(m_ConnSock, m_hNotifyEvent, FD_CONNECT);

	struct sockaddr_in sin;
	memset((char *)&sin, 0, sizeof(sin));
	sin.sin_family = AF_INET;
	sin.sin_port = uRemotePort;	
	sin.sin_addr.s_addr = uRemoteAddr;

	// Ignore return value of connect since we wait for FD_CONNECT event
	if (::connect(m_ConnSock, (struct sockaddr *)&sin, sizeof(sin)) != 0)
	{
#if 1 //for Linux yamada
		if (WSAGetLastError() != EINPROGRESS)
		{
			cout << "Network - connect error " << WSAGetLastError() <<"\n" <<endl;
#else
		if (WSAGetLastError() != WSAEWOULDBLOCK)
		{
			DB("WSA error code " << WSAGetLastError());
#endif //for Linux yamada
			CLOSE_SOCKET(m_ConnSock);
			return false;
		}
	}

	m_bCloseReceived = false;
	m_dwConnectTime = timeGetTime();
	// Waiting for connect event...

	return true;
}

int TcpNetwork::DoRecv(char* pBuf, int iLen)
{
	DB_TCPN("Sawamura TcpNetwork::DoRecv\n");
	//DB_ENTER(TcpNetwork::DoRecv);

	Critical_Section cs(*m_pMutex);

	if (iLen == 0)
		return 0;

	// Receive from network
	int iRet = ::recv(m_ConnSock, pBuf, iLen, 0);
	if (iRet == SOCKET_ERROR)
	{
		//return iRet;
#if 1
		int iLastError = WSAGetLastError();
		switch (iLastError)
		{
#if 1 //for Linux yamada
		case EMSGSIZE:
#else
		case WSAEMSGSIZE:
#endif //for Linux yamada
			DB("TcpNetwork::recv error (WSAEMSGSIZE)");
			break;

#if 1 //for Linux yamada
		case EWOULDBLOCK:
#else
		case WSAEWOULDBLOCK:
#endif //for Linux yamada
			DB("TcpNetwork::recv error (WSAEWOULDBLOCK)");
			break;

		default:
			{
				Close();
			}
			return ES_NOT_CONNECTED;
		}

		return ES_FAILURE;
#endif
	}
	if (0 == iRet)
	{
		DB("Socket has been gracefully closed.");
		return ES_NOT_CONNECTED;
	}

	//DB("TcpNetwork::DoRecv - Socket: " << m_ConnSock << ", Bytes read: " << iRet);

	return iRet;
}

int TcpNetwork::DoSend(const char* pBuf, int iLen)
{
	DB_TCPN("Sawamura TcpNetwork::DoSend\n");
	DB_ENTER(TcpNetwork::DoSend);

	Critical_Section cs(*m_pMutex);

	int iRet = ::send(m_ConnSock, pBuf, iLen, 0);
	DB("in DoSend: iRet = "<<iRet);
	if (iRet < 0)
		return ES_FAILURE;

	return iRet;
}

void TcpNetwork::AppendToSendBuffer(const string& sBuf)
{
	//DB_ENTER(TcpNetwork::AppendToSendBuffer);

	Critical_Section cs(*m_pMutex);

	DB("Buffering " << sBuf.size() << " bytes to be sent.");
	m_pSendBuffer->AppendPacket(sBuf);

	DB("Total bytes buffered: " << m_pSendBuffer->Size());
}

bool TcpNetwork::IsSendBufferEmpty(void)
{
	Critical_Section cs(*m_pMutex);

	return m_pSendBuffer->Empty();
}

bool TcpNetwork::GetSendBufferPacket(string& sBuf)
{
	Critical_Section cs(*m_pMutex);

	return m_pSendBuffer->GetPacket(sBuf);
}

void TcpNetwork::RemoveSendBufferBytes(unsigned int nLen)
{
	Critical_Section cs(*m_pMutex);

	m_pSendBuffer->RemoveBytes(nLen);
}


bool TcpNetwork::SendBufferedData(void)
{
	//DB_ENTER(TcpNetwork::SendBufferedData);

	Critical_Section cs(*m_pMutex);

	// Send as much data to the socket as possible
	while (!m_pSendBuffer->Empty())
	{
		string sData;
		if (!m_pSendBuffer->GetPacket(sData))
		{
			DB_ASSERT(0 && "send buffer error");
			return false;
		}

		// manually set the last error to 0, since we rely on this value below
		WSASetLastError(0);

		int iRet = DoSend(sData.data(), sData.size());

		if (iRet > 0)
		{
			m_pSendBuffer->RemoveBytes(iRet);

			// continue sending
		}
		else
		{
			int iErr = WSAGetLastError();
			if ((iErr == 0) && (iRet == ES_NOT_CONNECTED))
			{
				// map ES return code to winsock error code
#if 1 //for Linux yamada 
				iErr = ENOTCONN;
#else
				iErr = WSAENOTCONN;
#endif
			}

			DB_ASSERT(iErr != 0);
#if 1 //for Linux yamada
			if (EWOULDBLOCK == iErr)
#else
			if (WSAEWOULDBLOCK == iErr)
#endif //for Linux yamada
			{
#ifdef _WIN32_WCE
				long lNetworkEvents = FD_READ | FD_WRITE;
				WSAEventSelect(m_ConnSock, m_hNotifyEvent, lNetworkEvents);
#endif
				return true;
			}

#if 1 //for Linux yamada
			if ((ENOTCONN == iErr) && 
#else
			if ((WSAENOTCONN == iErr) && 
#endif //for Linux yamada
				(timeGetTime() < m_dwConnectTime + TCP_CONNECT_TIMEOUT))
			{
				// still waiting for connection
				return true;
			}

			return false;
		}
	}
	
#ifdef _WIN32_WCE
	// there is not data buffered, FD_WRITE event should be disabled.
	if (WSAEventSelect(m_ConnSock, m_hNotifyEvent, FD_READ) != 0)
	{
	//p	DB_ASSERT(0 && "WSAEventSelect failed.");
	}
#endif

	return true;
}

bool TcpNetwork::SetSocketBufferSize(void)
{
	Critical_Section cs(*m_pMutex);

	return TcpNetwork::SetSocketBufferSize(m_ConnSock);
}

bool TcpNetwork::SetSocketBufferSize(SOCKET sock)
{
	if (INVALID_SOCKET == sock)
	{
		DB_ASSERT(0);
		return false;
	}

	int iBufSize = TCP_SOCKET_BUFFER_SIZE;
	int iInterfaceSpeed = GetInterfaceSpeed() / 8;  // bytes per second
	if (iInterfaceSpeed > 0)
	{
		// calculate optimal buffer size
		const float nTime = TCP_MAX_BUFFER_SIZE_MSEC / 1000.0;  // in seconds
		int nOptBufSize = (int)(nTime * iInterfaceSpeed);

		// keep buffer size small to control delay
		while (iBufSize > nOptBufSize)
		{
			// keep it as a multiple of 2
			iBufSize /= 2;
		}
	}

	if (iBufSize < TCP_MIN_SOCKET_BUFFER_SIZE)
	{
		iBufSize = TCP_MIN_SOCKET_BUFFER_SIZE;
	}

	while (!Network::SetBufferSize(sock, iBufSize))
	{
		DB_ASSERT(0);

		// Handle this error by trying a smaller buffer size
		iBufSize /= 2;
		if (iBufSize < TCP_MIN_SOCKET_BUFFER_SIZE)
			return false;
	}

	return true;
}

bool TcpNetwork::OnConnect(void)
{
	// implemented by derived classes
	return true;
}

bool TcpNetwork::EnumNetworkEvents(WSANETWORKEVENTS& netEvents)
{
	if (0 != WSAEnumNetworkEvents(m_ConnSock, m_hNotifyEvent, &netEvents))
	{
		DB_TCPN("WSAEnumNetworkEvents failed");
		return false;
	}

	return true;
}

void* TcpNetwork::CreateRecvThread(void * arg)
{
DB_TCPN("Sawamura TcpNetwork::CreateRecvThread\n");
	return NULL;
}


#define USE_ECHO_SERVER
//#define USE_SYS_API
int TcpNetwork::GetPortFromTurnServer(AddressPortPair& addrServer)
{
	CTurnMessage msg;
        if( !m_sTurnUserName.empty() )
        {
                msg.SetAccount(m_sTurnUserName,m_sTurnPassword);
                msg.SetNonceRealm(m_sTurnNonce,m_sTurnRealm);
        }
        else
        {
                //DB_ASSERT( 0 && "UserName, Password, Nonce,.. not set properly");
        }
#ifdef USE_ECHO_SERVER 
        std::string sBuf = "hello world--"; 
#else
        std::string sBuf = msg.CreateAllocateRequest(false);
#endif

#ifdef USE_SYS_API
	int iSendRet = ::send(m_ConnSock, sBuf.c_str(), sBuf.size(), 0);
	DB("iSendRet = "<<iSendRet);
#else
	Send(sBuf.c_str(), sBuf.size() );
#endif
        //blocking recv

	char szBuf[2048];
#ifdef USE_SYS_API
	int iRet = ::recv(m_ConnSock, szBuf, 2048, 0);
#else
	int iRet = Recv(szBuf,2048);
#endif
	DB("Recv: iRet = "<<iRet);
	if(iRet <= 0)
	{
		DB("received failed");
		ASSERT(0);
		return -1;
	}
	std::string s;
	s.assign(szBuf,iRet);
	DB("Recv: iRet = "<<iRet);
#ifdef USE_ECHO_SERVER 
	DB("Recv: Data =  "<< s.c_str());
#else
	if (!msg.GetMappedAddr(s, m_TurnDetectedAddr/*m_DetectedLocalAddr*/))
	{
		DB("TURN ALLOCATE REQUEST parse failed");
		return -2;
	}
                                                                                                                            
	DB("Allocated addr: "<< LookupHostName(m_TurnDetectedAddr.first/*m_DetectedLocalAddr.first*/));
	DB("Allocated port: "<< ntohs(m_TurnDetectedAddr.second));
#endif

	return 0;
                                                                                                                
}
