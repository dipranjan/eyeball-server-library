// ---------------------------------------------------------------------------
// File:       inet.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    
// 
// Change Log:-
// --------------------------------------------------------------------------

#include "Base.h"  // automatic use of precompiled headers 

#include "critsec.h"
#include "inet.h"
#include "EUtil.h"

#if 1 //for Linux yamada
#include "Event.h"
#include <net/if.h>
#include <sys/ioctl.h>
                                                                                                                             

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/time.h>
#include <unistd.h>
#else
#ifndef _WIN32_WCE
#include <iprtrmib.h> //network
#include <iphlpapi.h> //network
#endif
#endif //for Linux yamada

#include "ESocket.h"
#include "Thread.h"
#include "ENetwork.h"

#include <cstdio>
#include <netinet/tcp.h>                // TCP_NODELAY

using std::string;


// --------------------------------------------------------------------------
// Static variables
// --------------------------------------------------------------------------

static u_int32_t s_uLocalAddr = 0;
//static Mutex s_AddressMutex;


// --------------------------------------------------------------------------
// Local helper functions
// --------------------------------------------------------------------------

static bool is_dotted_ip_address(const string& s)
{	
	int dotcount = 0;	
	int len = s.length();

	if (len>15)
		return false;
	
	int number = 0;

	for(int i = 0; i < len; i++)	
	{
		if (isdigit(s[i]))
		{
			number = (number * 10) + s[i] - '0';
			if(number > 255) return false;
			continue;
		}
		else if (s[i] == '.') 
		{
			//if(number == 0) return false;
			number = 0;
			dotcount++;
		}
		else
			return false;
	}

	if (dotcount != 3) 
		return false;

	return true;
}


// --------------------------------------------------------------------------
// Exported functions
// --------------------------------------------------------------------------

// A faster replacement for inet_ntoa().
// Extracted from tcpdump 2.1.

string intoa(u_int32_t addr)
{
	register char *cp;
	register u_int byte;
	register int n;
	char buf[sizeof(".xxx.xxx.xxx.xxx")];

	NTOHL(addr);
	cp = &buf[sizeof buf];
	*--cp = '\0';

	n = 4;
	do
	{
		byte = addr & 0xff;
		*--cp = byte % 10 + '0';
		byte /= 10;
		if (byte > 0)
		{
			*--cp = byte % 10 + '0';
			byte /= 10;
			if (byte > 0)
				*--cp = byte + '0';
		}
		*--cp = '.';
		addr >>= 8;
	} while (--n > 0);

	return cp + 1;
}

u_int32_t LookupHostAddr(const string& s)
{
	if (s.empty())
		return 0;

    if (is_dotted_ip_address(s))
		return (u_int32_t)inet_addr(s.c_str());
    else
	{
		struct hostent *hp = gethostbyname(s.c_str());
		if (!hp)
            return 0;

		// Return first IP
		return *((u_int32_t **)hp->h_addr_list)[0];
    }
}

bool LookupHostName(u_int32_t addr, char* name, int len)
{	
	struct in_addr ia;
	
	//memcpy(&ia, &addr, 4);

#if 1 //for Linux yamada
	ia.s_addr = addr;
#else
	ia.S_un.S_addr = addr;
#endif //for Linux yamada

	STRNCPY(name, inet_ntoa(ia), len);

	return true;
}

string LookupHostName(u_int32_t addr)
{	
	struct in_addr ia;
	
#if 1 //for Linux yamada
	ia.s_addr = addr;
#else
	ia.S_un.S_addr = addr;
#endif //for Linux yamada

	return inet_ntoa(ia);
}
/* modified 5_12 
u_int32_t LookupLocalAddr(void)
{
	//Critical_Section cs(s_AddressMutex);

	// Use static to only call gethostname first time
	if (s_uLocalAddr == 0)
	{
#if 1 //for Linux yamada
		char name[256];
#else
		char name[MAXHOSTNAMELEN];
#endif //for Linux yamada

		gethostname(name, sizeof(name));
		s_uLocalAddr = LookupHostAddr(name);
	}

	return s_uLocalAddr;
}
*/
//added by D 5_12

u_int32_t LookupLocalAddr(void)
{
   struct ifreq if_data;
   int sockd;
   struct sockaddr *sinptr;
   if (s_uLocalAddr == 0)
   {
		#if 1 //for Linux yamada
                	char name[256];
		#else
                	char name[MAXHOSTNAMELEN];
		#endif //for Linux yamada
//                gethostname(name, sizeof(name));
//                s_uLocalAddr = LookupHostAddr(name);
   	if ((sockd = socket (AF_INET, SOCK_DGRAM, 0)) < 0)
        {
      		perror("socket");
	        //exit (0);
	}
	strcpy (if_data.ifr_name, "eth0");
	if (ioctl (sockd, SIOCGIFADDR, &if_data) < 0) 
	{
      		perror ("ioctl(); SIOCGIFADDR \n");
		//exit(EXIT_FAILURE);
   	}
        sinptr = (struct sockaddr *)&if_data.ifr_addr;
        if(sinptr !=NULL)
        {
                struct sockaddr_in  *sin = (struct sockaddr_in *) sinptr;
                if (inet_ntop(AF_INET, &sin->sin_addr, name, sizeof(name)) != NULL)
		{
                        //printf("ipAddr: %s\n",name);
                        s_uLocalAddr = LookupHostAddr(name);
		}
        }

   }
	return s_uLocalAddr;

}

//____________________________________

void SetLocalAddr(u_int32_t addr)
{
	//Critical_Section cs(s_AddressMutex);

	s_uLocalAddr = addr;
}

bool IsLocalAddress(u_int32_t addr)
{
	if (addr == 0)
		return false;

#if 1 //for Linux yamada
	char name[256];
#else
	char name[MAXHOSTNAMELEN];
#endif //for Linux yamada

	if (gethostname(name, sizeof(name)) != 0)
		return false;

	hostent* p = gethostbyname(name);
	if (!p)
		return false;

	int num = sizeof(p->h_addr_list)/p->h_length;
	for (int i = 0; i < num; i++)
	{
		u_int32_t add = *(u_int32_t*)(p->h_addr_list[i]);

		if (add == addr)
			return true;
	}

	return false;
}


DWORD GetInterfaceSpeed()
{
#ifndef _WIN32_WCE
	// get number of interface
	DWORD dwNumIf;
	DWORD dwValue = GetNumberOfInterfaces(&dwNumIf);

	if (dwValue != NO_ERROR)
		dwNumIf = 10;

	// get interface table
	MIB_IFTABLE* pIfTable = new MIB_IFTABLE [dwNumIf+1];
	DWORD dwBufferSize  = sizeof(MIB_IFTABLE)*(dwNumIf+1);
	dwValue =  GetIfTable(pIfTable, &dwBufferSize, false);
	

	DWORD dwSpeed = 0;

	if (dwValue == NO_ERROR)
	{
		for (DWORD i = 0; i < pIfTable->dwNumEntries; i++)
		{		
			if (pIfTable->table[i].dwType == MIB_IF_TYPE_LOOPBACK)
				continue;

			if (pIfTable->table[i].dwOperStatus == MIB_IF_OPER_STATUS_DISCONNECTED)
				continue;

			if (pIfTable->table[i].dwOperStatus == MIB_IF_OPER_STATUS_NON_OPERATIONAL)
				continue;

			if (pIfTable->table[i].dwOperStatus == MIB_IF_OPER_STATUS_CONNECTED)
			{
				dwSpeed = pIfTable->table[i].dwSpeed;
				break;
			}

			// not functional
			if (pIfTable->table[i].dwInOctets == 0 || pIfTable->table[i].dwOutOctets == 0)
				continue;

			if (pIfTable->table[i].dwSpeed == 0)
				continue;

			// get the smallest value
			if (dwSpeed == 0 || dwSpeed > pIfTable->table[i].dwSpeed)
				dwSpeed = pIfTable->table[i].dwSpeed;
		}
	}
	
	delete [] pIfTable;

	return dwSpeed;
#else
//p	DB_ASSERT(0 && "Method not supported yet.");
	return 200*1024; // 200kb/s;
#endif
}


bool IsInSameSubnet(u_int32_t uLocalAddr, u_int32_t uRemoteAddr)
{
#ifndef _WIN32_WCE
	DWORD dwBufLen;
	DWORD dwErr = GetAdaptersInfo(NULL, &dwBufLen);
	if (dwErr != ERROR_BUFFER_OVERFLOW)
		return false;

	if (dwBufLen == 0)
		return false;

    PIP_ADAPTER_INFO pAdapterInfo = (PIP_ADAPTER_INFO) new BYTE [dwBufLen];
	if (pAdapterInfo == NULL)
		return false;

	dwErr = GetAdaptersInfo(pAdapterInfo, &dwBufLen);
	if (dwErr != ERROR_SUCCESS)
	{
		delete pAdapterInfo;
		return false;
	}

	bool bSameSubnet = false;

	PIP_ADAPTER_INFO p = pAdapterInfo;
	while (p != NULL)
	{
		PIP_ADDR_STRING pAddr = &p->IpAddressList;
		while (pAddr != NULL)
		{
			if (LookupHostAddr(pAddr->IpAddress.String) == uLocalAddr)
			{
				DWORD dwMask = LookupHostAddr(pAddr->IpMask.String);
				//DB("addr: "  << pAddr->IpAddress.String);
				//DB("mask: " << pAddr->IpMask.String);
				if ((uLocalAddr & dwMask) == (uRemoteAddr & dwMask))
				{
					bSameSubnet = true;
					break;
				}
			}
			pAddr = pAddr->Next;
		}
		if (bSameSubnet)
			break;
		p = p->Next;
	}

	delete pAdapterInfo;
	return bSameSubnet;  
#else
	return false;
#endif
}


#ifdef _WIN32_WCE

struct SocketEvent
{
	SocketEvent():
		m_lSelectedEvent(0),
		m_lOccurredEvent(0),
		m_hEventObject(NULL)
	{
		memset(this, 0, sizeof(SocketEvent));
	}

	SocketEvent& operator=(SocketEvent& x)
	{
		m_lSelectedEvent = x.m_lSelectedEvent;
		m_lOccurredEvent = x.m_lOccurredEvent;
		m_hEventObject = x.m_hEventObject;
		for (int i = 0; i < FD_MAX_EVENTS; i++)
		{
			m_iErrorCodes[i] = x.m_iErrorCodes[i];
		}
		return *this;
	}

	long m_lSelectedEvent;
	long m_lOccurredEvent;
	int  m_iErrorCodes[FD_MAX_EVENTS];
	HANDLE m_hEventObject;
};


class SocketThread
{
public:
	SocketThread()
	{
		m_pAccessMutex = new Mutex;
		m_pDisposableSocket = new DisposableSocket;
		m_pDisposableSocket->Create();
		SocketEvent sv;
		m_SocketEventMap[m_pDisposableSocket->GetSocket()] = sv;

		m_bContinue = true;
		m_pThread = new Thread(SocketEventThread, this);
	}

	~SocketThread()
	{
		m_bContinue = false;
		m_pDisposableSocket->Destroy();
		m_pThread->join();
		SAFE_DELETE(m_pThread);
		SAFE_DELETE(m_pAccessMutex);
		SAFE_DELETE(m_pDisposableSocket);
	}

	void* ThreadImpl();
	
	int EnumEvent(
		SOCKET s,
		HANDLE hEventObject,
		WSANETWORKEVENTS* lpNetworkEvents);

	int SelectEvent(
		SOCKET s,
		HANDLE hEventObject,
		long lNetworkEvents);

private:
	static void* SocketEventThread(void* arg);

private:
	Mutex* m_pAccessMutex;
	Thread* m_pThread;
	volatile bool m_bContinue;
	typedef std::map<SOCKET , SocketEvent> SocketEventMap;
	SocketEventMap m_SocketEventMap;
	DisposableSocket* m_pDisposableSocket;
};

static SocketThread* s_pSocketThread = NULL;

// globale functions
void CreateSocketThread()
{
	if (s_pSocketThread)
		return;

	s_pSocketThread = new SocketThread;
}

void CloseSocketThread()
{
	SAFE_DELETE(s_pSocketThread);
}

// member functions
void* SocketThread::SocketEventThread(void* arg)
{
	SocketThread* pThis = (SocketThread*)arg;
	return pThis->ThreadImpl();
}

void* SocketThread::ThreadImpl()
{
#if 0 //for Linux yamada
	OutputDebugString(L"Starting socket event thread...\n");
#endif //for Linux yamada

	// file descriptor set variables
	fd_set readfds, writefds, exceptfds;
#if 0 //for Linux yamada
	DWORD dwWakeupTime = GetTickCount();
#endif //for Linux yamada
	bool bDebug = false;
	while (m_bContinue)
	{
		// initializes the set to the NULL set. 
		FD_ZERO(&readfds);
		FD_ZERO(&writefds);
		FD_ZERO(&exceptfds);
		SOCKET s;
		// prepare socket sets to be checked for readability, writability and errors
		if (true)
		{
			Critical_Section cs(*m_pAccessMutex);

			int iSize = m_SocketEventMap.size();
			if (iSize > 64)
			{
				return NULL;
			}

			SocketEventMap::iterator iter = m_SocketEventMap.begin();
			for (; iter != m_SocketEventMap.end(); iter++)
			{
				s = iter->first;
				SocketEvent& sv = iter->second;

				if ((sv.m_lSelectedEvent & FD_READ) || (sv.m_lSelectedEvent & FD_CLOSE) ||
					(sv.m_lSelectedEvent & FD_ACCEPT))
					FD_SET(s, &readfds);

				if ((sv.m_lSelectedEvent & FD_WRITE) || (sv.m_lSelectedEvent & FD_CONNECT))
					FD_SET(s, &writefds);

				if (sv.m_lSelectedEvent & FD_CONNECT)
					FD_SET(s, &exceptfds);

				// disposable socket
				if (sv.m_lSelectedEvent == 0)
					FD_SET(s, &readfds);
			}
		}

		// the maximum time select should wait before returning
		timeval tv;
		tv.tv_sec = 20; // in seconds
		tv.tv_usec = 0; // in microseconds

		int iEvents = select(s + 1, &readfds, &writefds, &exceptfds, &tv);

#if 0 //for Linux yamada
		if (bDebug)
		{
			DWORD dwElapsed = GetTickCount() - dwWakeupTime;
			dwWakeupTime += dwElapsed;
			TCHAR sz[100];
			wsprintf(sz, L"%s%u%s", L"Socket thread wakes up interval: ", dwElapsed, L"\n");
			OutputDebugString(sz);
		}
#endif //for Linux yamada
			
		// time limit expires, no event occurs
		if (iEvents == 0)
			continue;

		// error occurred (-1)
		if (iEvents == SOCKET_ERROR)
		{
			// the disaposable socket may be closed
			//DB_ASSERT(WSAGetLastError() == WSAENOTSOCK);
			continue;
		}

		// initializes the set to the NULL set. 
		FD_ZERO(&readfds);
		FD_ZERO(&writefds);
		FD_ZERO(&exceptfds);

		// event occurred
		if (true)
		{
			Critical_Section cs(*m_pAccessMutex);

			// make select return immediately, to poll the state of the selected socket
			tv.tv_sec = 0; // in seconds
			tv.tv_usec = 0;

			SocketEventMap::iterator iter = m_SocketEventMap.begin();
			// determine the status of each socket
			for (; iter != m_SocketEventMap.end(); iter++)
			{
				SOCKET soc = iter->first;
				SocketEvent& sv = iter->second;
				sv.m_lOccurredEvent = 0;
				sv.m_iErrorCodes[FD_CONNECT_BIT] = 0;

				if (sv.m_lSelectedEvent & FD_ACCEPT)
				{
					FD_SET(soc, &readfds);
					if (select(soc + 1, &readfds, NULL, NULL, &tv) > 0)
					{
						sv.m_lOccurredEvent |= FD_ACCEPT;
					}
					FD_CLR(soc, &readfds);
				}

				// cannot differentiate FD_READ and FD_CLOSE
				if (((sv.m_lSelectedEvent & FD_READ) || (sv.m_lSelectedEvent & FD_CLOSE)) &&
					!(sv.m_lSelectedEvent & FD_ACCEPT))
				{
					FD_SET(soc, &readfds);
					if (select(soc + 1, &readfds, NULL, NULL, &tv) > 0)
					{
						if (!(sv.m_lOccurredEvent & FD_ACCEPT))
							sv.m_lOccurredEvent |= FD_READ;
					}
					FD_CLR(soc, &readfds);
				}

		
				if ((sv.m_lSelectedEvent & FD_WRITE) &&
					!(sv.m_lSelectedEvent & FD_CONNECT))
				{
					FD_SET(soc, &writefds);
					if (select(soc + 1, NULL, &writefds, NULL, &tv) > 0)
					{
						sv.m_lOccurredEvent |= FD_WRITE;
					}
					FD_CLR(soc, &readfds);
				}
				
				if (sv.m_lSelectedEvent & FD_CONNECT)
				{
					FD_SET(soc, &writefds);
					if (select(soc + 1, NULL, &writefds, NULL, &tv) > 0)
					{
						sv.m_lOccurredEvent |= FD_CONNECT;
					}
					FD_CLR(soc, &writefds);
				}				
				
				// processing a connect call (nonblocking), connection attempt failed
				if (sv.m_lSelectedEvent & FD_CONNECT)
				{
					FD_SET(soc, &exceptfds);
					if (select(soc + 1, NULL, NULL, &exceptfds, &tv) > 0)
					{
						DB_ASSERT((sv.m_lOccurredEvent & FD_CONNECT) != 0);
						sv.m_iErrorCodes[FD_CONNECT_BIT] = WSAGetLastError();
						if (sv.m_iErrorCodes[FD_CONNECT_BIT] == 0)
#if 1 //for Linux yamada
							sv.m_iErrorCodes[FD_CONNECT_BIT] = ENETUNREACH;
#else
							sv.m_iErrorCodes[FD_CONNECT_BIT] = WSAENETUNREACH;
#endif //for Linux yamada
					}
					FD_CLR(soc, &exceptfds);
				}
				
				if (sv.m_iErrorCodes[FD_CONNECT_BIT] != 0 || sv.m_lOccurredEvent != 0)
				{
					//printf("SocketThread SetEvent\n");
//printf("SocketThread Handle %d\n",sv.m_hEventObject);
					SetEvent(sv.m_hEventObject);
				}
			} // end of for loop
#if 1 //for linux yamada
#else
			Sleep(100);
#endif //for Linux yamada
		}// end of if (true)
#if 1 //for linux yamada
			usleep(100);
//printf("SocketThread sleep \n");
#endif

	} // end of while loop

#if 0 //for Linux yamada
	OutputDebugString(L"Exiting socket event thread...\n");
#endif //for Linux yamada
	return NULL;
}

int SocketThread::EnumEvent(
	SOCKET s,
    HANDLE hEventObject,
	WSANETWORKEVENTS* lpNetworkEvents)
{
	if (s == INVALID_SOCKET)
	{
#if 1 //for Linux yamada
		WSASetLastError(ENOTSOCK);
#else
		WSASetLastError(WSAENOTSOCK);
#endif //for linux yamada
		return SOCKET_ERROR;
	}
	
	if (lpNetworkEvents == NULL)
	{

#if 1 //for linux yamada
		WSASetLastError(EINVAL);
#else
		WSASetLastError(WSAEINVAL);
#endif //for Linux yamada
		return SOCKET_ERROR;
	}

	memset(lpNetworkEvents, 0, sizeof(WSANETWORKEVENTS));
	ResetEvent(hEventObject);

	Critical_Section cs(*m_pAccessMutex);
	
	if (m_SocketEventMap.find(s) == m_SocketEventMap.end())
		return 0;

	// copying reports to the provided memory
	SocketEvent& sv = m_SocketEventMap[s];
	lpNetworkEvents->lNetworkEvents = sv.m_lOccurredEvent;
	if (sv.m_lSelectedEvent & FD_CONNECT)
	{
		lpNetworkEvents->iErrorCode[FD_CONNECT_BIT] = sv.m_iErrorCodes[FD_CONNECT_BIT];
	}

	// clear the internal network event records
	sv.m_lOccurredEvent = 0;
	sv.m_iErrorCodes[FD_CONNECT_BIT] = 0;

	return 0;	
}

int SocketThread::SelectEvent(
	SOCKET s,
	HANDLE hEventObject,
	long lNetworkEvents)
{
	if (s == INVALID_SOCKET)
	{
#if 1 //for Linux yamada
		WSASetLastError(ENOTSOCK);
#else
		WSASetLastError(WSAENOTSOCK);
#endif //for Linux yamada
		return SOCKET_ERROR;
	}

	// Cancel the association and selection of network events on a socket
	if (lNetworkEvents == 0)
	{
		Critical_Section cs(*m_pAccessMutex);
		m_SocketEventMap.erase(s);
		return 0;
	}
	
	if (hEventObject == NULL)
	{
#if 1 //for linux yamada
		WSASetLastError(EINVAL);
#else
		WSASetLastError(WSAEINVAL);
#endif //for Linux yamada
		return SOCKET_ERROR;
	}

	Critical_Section cs(*m_pAccessMutex);

	SOCKET sd = m_pDisposableSocket->GetSocket();
	m_pDisposableSocket->Destroy();
	m_SocketEventMap.erase(sd);
	
	// create a new disposable socket and add it to map
	m_pDisposableSocket->Create();
	sd = m_pDisposableSocket->GetSocket();

	// add it back again
	SocketEvent sv;
	m_SocketEventMap[sd] = sv;

	if (sd == s)
	{
#if 0 //for Linux yamada
		OutputDebugString(L"socket has the same descriptor as the disposable socket.\n");
#endif //for Linux yamada
		return SOCKET_ERROR;
	}


	if (m_SocketEventMap.find(s) != m_SocketEventMap.end())
	{
		SocketEventMap::iterator iter = m_SocketEventMap.find(s);
		SOCKET so = iter->first;

		if (Network::GetSocketType(so) != Network::GetSocketType(s))
		{
#if 0 //for linux yamada
			OutputDebugString(L"Same socket descriptors have different type.");
#endif //for Linux yamada
			return SOCKET_ERROR;
		}
		else
		{
			m_SocketEventMap.erase(s);
		}
	}

	sv.m_hEventObject = hEventObject;
	sv.m_lSelectedEvent = lNetworkEvents;

	m_SocketEventMap[s] = sv;
	return 0;
}

int WSAEnumNetworkEvents(
	SOCKET s,
    HANDLE hEventObject,
	WSANETWORKEVENTS* lpNetworkEvents)
{
	if (s_pSocketThread == NULL)
	{
		return SOCKET_ERROR;
	}
	return s_pSocketThread->EnumEvent(s, hEventObject, lpNetworkEvents);
}


int WSAEventSelect(
	SOCKET s,
	HANDLE hEventObject,
	long lNetworkEvents)
{
	CreateSocketThread();
	return s_pSocketThread->SelectEvent(s, hEventObject, lNetworkEvents);
}


#endif

void nonblock(int fd)
{
        int flags = fcntl(fd, F_GETFL, 0);
        flags |= O_NONBLOCK | O_NDELAY;
                                                                                                                            
        if (fcntl(fd, F_SETFL, flags) == -1)
        {
                perror("fcntl");
        }
}
                                                                                                                            
void reuse(int fd)
{
        int on = 1;
        if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (char *)&on,
                sizeof(on)) != 0)
        {
                perror("SO_REUSEADDR");
        }
                                                                                                                            
#ifdef SO_REUSEPORT
        on = 1;
        if (setsockopt(fd, SOL_SOCKET, SO_REUSEPORT, (char *)&on,
                sizeof(on)) != 0)
        {
                perror("SO_REUSEPORT");
        }
#endif
}
                                                                                                                            
bool sendbufsize(int fd, int size)
{
        // set the send buffer size
        int bufsize = size;
                                                                                                                            
        if( setsockopt(fd, SOL_SOCKET, SO_SNDBUF, (char*)&bufsize, sizeof(bufsize))<0 )
        {
                return false;
        }
        return true;
}
                                                                                                                            
bool recvbufsize(int fd, int size)
{
        // set the send buffer size
        int bufsize = size;
                                                                                                                            
        if( setsockopt(fd, SOL_SOCKET, SO_RCVBUF, (char*)&bufsize, sizeof(bufsize))<0 )
        {
                return false;
        }
        return true;
}
void nolinger(int fd)
{
#ifndef __linux__
        // Linux kernel 2.2.x has a bug which sets the linger time to
        // maximum when it is zero!  For this reason, currently avoid
        // SO_LINGER under Linux.
        struct linger linger;
        linger.l_onoff = 1;
        linger.l_linger = 0;
                                                                                                                            
        if (setsockopt(fd, SOL_SOCKET, SO_LINGER, (char *)&linger,
                sizeof(linger)) != 0)
        {
                perror("SO_LINGER");
        }
#endif
}
                                                                                                                            
void nodelay(int fd)
{
        int on = 1;
        if (setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, (char *)&on,
                sizeof(on)) != 0)
        {
                perror("TCP_NODELAY");
        }
}

