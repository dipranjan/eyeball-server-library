//p #include "Base.h
#include <netinet/in.h>
#include "TurnMessage.h"
#include "Serializable.h"

#ifndef _WIN32_WCE
#include <openssl/hmac.h>
#include <openssl/md5.h>
#endif
//#include "FirewallLog.h"
#include "debug.h"
using std::map;
using std::string;
using namespace eyeballTurn;
// ---------------------------------------------------------------------------
// Definition
// ---------------------------------------------------------------------------

// message type definition
#define UNKNOWN_MESSAGE					0
#define BINDING_REQUEST					0x0001
#define BINDING_RESPONSE				0x0101
#define BINDING_ERROR_RESPONSE			0x0111
#define SHARED_SECRET_REQUEST			0x0002
#define SHARED_SECRET_RESPONSE			0x0102
#define SHARED_SECRET_ERROR_RESPONSE	0x0112

//Three new Message Types for TURN
#define ALLOCATE_REQUEST				0x0003
#define ALLOCATE_RESPONSE				0x0103
#define ALLOCATE_ERROR_RESPONSE			0x0113
#define SEND_REQUEST					0x0004
#define SEND_RESPONSE					0x0104
#define SEND_ERROR_RESPONSE				0x0114
#define DATA_INDICATION					0x0115

//New MessageType According to Draft 8
#define SET_ACTIVE_DESTINATION_REQUEST			0x0006
#define SET_ACTIVE_DESTINATION_RESPONSE			0x0106
#define SET_ACTIVE_DESTINATION_ERROR_RESPONSE	0x0116


// message attribute types
#define MAPPED_ADDRESS					0x0001
#define RESPONSE_ADDRESS				0x0002
#define CHANGE_REQUEST					0x0003
//#define SOURCE_ADDRESS					0x0004
#define CHANGED_ADDRESS					0x0005
#define USERNAME						0x0006
#define PASSWORD						0x0007
#define MESSAGE_INTEGRITY				0x0008
#define ERROR_CODE						0x0009
#define UNKNOWN_ATTRIBUTES				0x000a
#define REFLECTED_FROM						0x000b

//New Attributes for TURN.
#define LIFETIME						0x000d
#define ALTERNATE_SERVER				0x000e
#define	MAGIC_COOKIE					0x000f
#define BANDWIDTH						0x0010
#define DESTINATION_ADDRESS				0x0011
#define SOURCE_ADDRESS					0x0012
#define DATA							0x0013
#define NONCE							0x0014
#define REALM							0x0015 //added by sazzad

#define MAGIC_COOKIE_VALUE				0x72c64bc6//1925598150 //
#define COLON							":"


// ---------------------------------------------------------------------------
// Constructor/Destructor
// ---------------------------------------------------------------------------
CTurnMessage::CTurnMessage():
	m_iMessageType(UNKNOWN_MESSAGE),
	m_attrMap(),
	m_sUserName(),
	m_sPassword(),
	m_sNonce(),
	m_sRealm(),
	m_iLastErrorCode(-1),
	m_sErrorPhrase(),
	m_sAuthUserName(),
	m_sAuthPassword()
{
	memset(m_szTransactionID, 0, sizeof(m_szTransactionID));
}

CTurnMessage::~CTurnMessage()
{
	m_attrMap.clear();
}

// ---------------------------------------------------------------------------
// Interfaces
// ---------------------------------------------------------------------------

void CTurnMessage::SetAccount(const string& sUserName, const string& sPassword)
{
	m_sUserName = sUserName;
	m_sPassword = sPassword;
}

void CTurnMessage::SetAuthAccount(const string& sUserName, const string& sPassword)
{
	m_sAuthUserName = sUserName;
	m_sAuthPassword = sPassword;
}

void CTurnMessage::SetNonceRealm(const string& sNonce, const string& sRealm)
{
	m_sNonce = sNonce;
	m_sRealm = sRealm;
}


std::string CTurnMessage::CreateBindingRequest(BOOL bNoAttribute)
{
	m_attrMap.clear();
	m_iMessageType = BINDING_REQUEST;
	sprintf(m_szTransactionID, "%016u", time(NULL)/*timeGetTime()*/);

	// optional change request
	int i = 0;
	string s;
	s.assign((char*)&i, sizeof(int));

	if (!m_sUserName.empty())
	{
		m_attrMap[USERNAME] = m_sUserName;
		m_attrMap[PASSWORD] = m_sPassword;
	}

	if (!bNoAttribute)
	{
		m_attrMap[CHANGE_REQUEST] = s;
	}
	else
	{
		//DB("Stun binding request is not compatible with Ethereal: no attribute");
	}

	return CreateMessage();
}

std::string CTurnMessage::CreateAllocateRequest(BOOL bClose)
{
	m_attrMap.clear();
	m_iMessageType = ALLOCATE_REQUEST;
	sprintf(m_szTransactionID, "%016u", time(NULL)/*timeGetTime()*/);

	// optional change request
	int i = 0;
	string s;
	s.assign((char*)&i, sizeof(int));

	if (!m_sUserName.empty())
	{
		m_attrMap[USERNAME] = m_sUserName;
		//m_attrMap[PASSWORD] = m_sPassword;
	}

	if (bClose)
	{
		m_attrMap[LIFETIME] = "0000";
	}
	else
	{
		//DB("Stun binding request is not compatible with Ethereal: no attribute");
	}

	string sMagicCookie;
	Serialize32(MAGIC_COOKIE_VALUE, sMagicCookie);
	m_attrMap[MAGIC_COOKIE] = sMagicCookie;

	if( !m_sNonce.empty() )
	{
		m_attrMap[NONCE] = m_sNonce;
		m_attrMap[REALM] = m_sRealm;
		m_attrMap[MESSAGE_INTEGRITY] = ComputeHmacOutput(false);
	}
	else
	{
//		DB("nonce is empty.");
	}
	
	return CreateMessage();
}

std::string CTurnMessage::CreateSendRequest(AddressPortPair& addrDestination)
{
	m_attrMap.clear();
	m_iMessageType = SEND_REQUEST;
	sprintf(m_szTransactionID, "%016u", time(NULL)/*timeGetTime()*/);

	if (!m_sUserName.empty())
	{
		m_attrMap[USERNAME] = m_sUserName;
		//m_attrMap[PASSWORD] = m_sPassword;
	}

	
	string sMagicCookie;
	Serialize32(MAGIC_COOKIE_VALUE, sMagicCookie);
	m_attrMap[MAGIC_COOKIE] = sMagicCookie;

	//now set  DESTINATION attribute.
	m_attrMap[DESTINATION_ADDRESS] = CreateAddressValue(addrDestination);
	
	//now set  DATA attribute. //for now, DATA attribute payload is empty.
	m_attrMap[DATA] = "hello farhan";

	if( !m_sNonce.empty() )
	{
		m_attrMap[NONCE] = m_sNonce;
		m_attrMap[REALM] = m_sRealm;
		m_attrMap[MESSAGE_INTEGRITY] = ComputeHmacOutput(false);
	}
	else
	{
//		DB("nonce is empty.");
	}

	return CreateMessage();
}

std::string CTurnMessage::CreateSharedSecretRequest()
{
	//initial shared secret request will contain only username and password.
	m_attrMap.clear();
	m_iMessageType = SHARED_SECRET_REQUEST;
	sprintf(m_szTransactionID, "%016u", time(NULL)/*timeGetTime()*/);

	string sMagicCookie;
	Serialize32(MAGIC_COOKIE_VALUE, sMagicCookie);
	m_attrMap[MAGIC_COOKIE] = sMagicCookie;

	if (!m_sUserName.empty())
	{
		m_attrMap[USERNAME] = m_sUserName;
		m_attrMap[PASSWORD] = m_sPassword;
	}
	else
	{
		DB_ASSERT(0 && "Username and Password is not provided");
	}

	//Final shared secret request will contain username and password and nonce and 
	//message-integrity.
	if( !m_sNonce.empty() )
	{
		m_attrMap[NONCE] = m_sNonce;
		m_attrMap[REALM] = m_sRealm;
		m_attrMap[MESSAGE_INTEGRITY] = ComputeHmacOutput(true);
		DB("TurnMessage: nonce: "<<m_sNonce);
		DB("TurnMessage: realm: "<<m_sRealm);
	}
	else
	{
//		DB("nonce is empty.");
	}


	return CreateMessage();
}


std::string CTurnMessage::CreateSetActiveDestinationRequest(AddressPortPair& addrDestination)
{
	m_attrMap.clear();
	m_iMessageType = SET_ACTIVE_DESTINATION_REQUEST;
	sprintf(m_szTransactionID, "%016u", time(NULL)/*timeGetTime()*/);

	if (!m_sUserName.empty())
	{
		m_attrMap[USERNAME] = m_sUserName;
		//m_attrMap[PASSWORD] = m_sPassword;
	}

	
	string sMagicCookie;
	Serialize32(MAGIC_COOKIE_VALUE, sMagicCookie);
	m_attrMap[MAGIC_COOKIE] = sMagicCookie;

	//now set  DESTINATION attribute.
	m_attrMap[DESTINATION_ADDRESS] = CreateAddressValue(addrDestination);
	
	m_attrMap[DATA] = "";

	if( !m_sNonce.empty() )
	{
		m_attrMap[NONCE] = m_sNonce;
		m_attrMap[REALM] = m_sRealm;
		m_attrMap[MESSAGE_INTEGRITY] = ComputeHmacOutput(false);
	}
	else
	{
//		DB("nonce is empty.");
	}

	return CreateMessage();
}



bool CTurnMessage::ParseSharedSecretResponse(const string& sResponse)
{

	if (!ParseMessage(sResponse))
	{
		return false;
	}
	
	if( m_iMessageType != SHARED_SECRET_RESPONSE && m_iMessageType != SHARED_SECRET_ERROR_RESPONSE)
	{
		return false;
	}

	m_iLastErrorCode = -1;
	if(m_iMessageType == SHARED_SECRET_ERROR_RESPONSE)
	{
		if( m_attrMap.find(ERROR_CODE) != m_attrMap.end() )
		{
			m_iLastErrorCode = ParseErrorCode( m_attrMap[ERROR_CODE] );
			DB("error code :"<< m_iLastErrorCode);
		}
	}
	

	if( m_attrMap.find(NONCE) == m_attrMap.end() )
	{
//		DB("nonce is not present in SharedSecret response");
		return false;
	}


	if( m_attrMap.find(REALM) == m_attrMap.end() )
	{
//		DB("realm is not present in SharedSecret response");
		return false;
	}

	if( m_attrMap.find(USERNAME) != m_attrMap.end() )
	{
		m_sUserName = m_attrMap[USERNAME];
//		DB("username is present in SharedSecret response:"<<m_sUserName.c_str());
	}

	if( m_attrMap.find(PASSWORD) != m_attrMap.end() )
	{
		m_sPassword = m_attrMap[PASSWORD];
//		DB("PASSWORD is present in SharedSecret response:"<<m_sPassword.c_str());
	}

	m_sNonce = m_attrMap[NONCE];
	m_sRealm = m_attrMap[REALM];

	DB("nonce: "<<m_sNonce.c_str());
	DB("realm: "<<m_sRealm.c_str());

	return true;
}


bool CTurnMessage::ParseSendResponse(const string& sResponse)
{

	if (!ParseMessage(sResponse))
	{
		DB("CTurnMessage: ParseMessage failed.");
		return false;
	}

	if( m_iMessageType != SEND_RESPONSE && m_iMessageType != SEND_ERROR_RESPONSE)
	{
		DB("CTurnMessage: MessageType mismatched.");
		return false;
	}

	m_iLastErrorCode = -1;
	if(m_iMessageType == SEND_ERROR_RESPONSE)
	{
		if( m_attrMap.find(ERROR_CODE) != m_attrMap.end() )
		{
			m_iLastErrorCode = ParseErrorCode( m_attrMap[ERROR_CODE] );
			DB("SEND_ERROR_RESPONSE: error code :"<< m_iLastErrorCode);
			//p FirewallLog("SEND_ERROR_RESPONSE: error code :");//<< m_iLastErrorCode);
		}
	}
	else
	{
			DB("SEND_RESPONSE occured");
			//p FirewallLog("SEND_RESPONSE occured");
	}
	
	return true;
}


bool CTurnMessage::ParseSetActiveDestinationResponse(const string& sResponse)
{

	if (!ParseMessage(sResponse))
	{
		DB("CTurnMessage: ParseMessage failed.");
		return false;
	}

	if( m_iMessageType != SET_ACTIVE_DESTINATION_RESPONSE && 
		m_iMessageType != SET_ACTIVE_DESTINATION_ERROR_RESPONSE)
	{
		DB("CTurnMessage: MessageType mismatched.");
		return false;
	}

	m_iLastErrorCode = -1;
	if(m_iMessageType == SET_ACTIVE_DESTINATION_ERROR_RESPONSE)
	{
		if( m_attrMap.find(ERROR_CODE) != m_attrMap.end() )
		{
			m_iLastErrorCode = ParseErrorCode( m_attrMap[ERROR_CODE] );
			DB("SET_ACTIVE_DESTINATION_ERROR_RESPONSE: error code :"<< m_iLastErrorCode);
			//p FirewallLog("SET_ACTIVE_DESTINATION_ERROR_RESPONSE: error code :");//<< m_iLastErrorCode);
		}
	}
	else
	{
		DB("SET_ACTIVE_DESTINATION_RESPONSE occured");
		//p FirewallLog("SET_ACTIVE_DESTINATION_RESPONSE occured");
	}

	return true;
}


string CTurnMessage::DataIndication(const string& sResponse)
{

	int length = sResponse.length();
	if (!ParseMessage(sResponse))
	{
		DB("CTurnMessage DataIndication: ParseMessage failed.");
		return "";
	}

	m_iLastErrorCode = -1;
	if(m_iMessageType == DATA_INDICATION /*&& length > 64*/)
	{
		if( m_attrMap.find(DATA) != m_attrMap.end() )
		{
			string s = m_attrMap[DATA];
			return s;
		}
	}
	else
	{
			DB("DataIndication occured");
			//p FirewallLog("SEND_RESPONSE occured");
	}
	
	return "";
}

bool CTurnMessage::GetMappedAddr(const string& sResponse, AddressPortPair& addr)
{
	addr = GetAddress(sResponse, MAPPED_ADDRESS);
	return addr != AddressPortPair(0, 0);
}

bool CTurnMessage::GetChangedAddr(const string& sResponse, AddressPortPair& addr)
{
	addr =  GetAddress(sResponse, CHANGED_ADDRESS);
	return addr != AddressPortPair(0, 0);
}

bool CTurnMessage::ParseMessage(char* pData, int iSize)
{
	if (iSize <= 0 || pData == NULL)
	{
		return false;
	}

	string sMsg;
	sMsg.assign(pData, iSize);
	return ParseMessage(sMsg);
}


bool CTurnMessage::ParseMessage(const std::string& sMessage)
{
	m_iMessageType = UNKNOWN_MESSAGE;
	m_attrMap.clear();

	if (sMessage.size() < 20)
	{
		DB("sMessage.size() < 20");
		return false;
	}

	int iOffset = 0;

	unsigned short iLength = 0;
	Deserialize16(m_iMessageType, sMessage, iOffset);
	Deserialize16(iLength, sMessage, iOffset);
	DeserializeCString(m_szTransactionID, 16, sMessage, iOffset);

	if (iLength == 0)
	{
		if (sMessage.size() != 20)
		{
			DB_ASSERT(0);
			return false;
		}

		return true;
	}

	string sAttributes = sMessage.substr(20);
	if (sAttributes.size() < iLength)
	{
		return false;
	}

	while (sAttributes.size() >= 4)
	{
		iOffset = 0;
		unsigned short iType = 0;
		Deserialize16(iType, sAttributes, iOffset);

		unsigned short iLen = 0;
		Deserialize16(iLen, sAttributes, iOffset);

		if (sAttributes.size() < 4 + iLen)
		{
			return false;
		}

		char* sz = new char [iLen];
		DeserializeBytes(sz, iLen, sAttributes, iOffset);

		string s;
		s.assign(sz, iLen);
		delete sz;

		m_attrMap[iType] = s;

		sAttributes.erase(0, iLen + 4);
	}

	if (!sAttributes.empty())
	{
		DB_ASSERT(0);
		return false;
	}

	return true;
}

BOOL CTurnMessage::IsRequest()
{
	return m_iMessageType == ALLOCATE_REQUEST;//BINDING_REQUEST;
}

BOOL CTurnMessage::IsResponse()
{
	return m_iMessageType == ALLOCATE_RESPONSE;//BINDING_RESPONSE;
}

bool CTurnMessage::CheckRequestAndCreateResponse(
	AddressPortPair& addrSource,
	AddressPortPair& addrMapped,
	string& sResponse)
{
	if (!IsRequest())
		return false;

	if (m_sAuthUserName.empty())
	{
		//DB_ASSERT(0);
		return false;
	}

	if (m_sAuthUserName != GetUserName())
		return false;

	if (m_sAuthPassword != GetPassword())
		return false;

	m_attrMap.clear();
	m_iMessageType = BINDING_RESPONSE;
	m_attrMap[SOURCE_ADDRESS] = CreateAddressValue(addrSource);
	m_attrMap[MAPPED_ADDRESS] = CreateAddressValue(addrMapped);

	sResponse = CreateMessage();
	return TRUE;
}

// ---------------------------------------------------------------------------
// Helper
// ---------------------------------------------------------------------------
std::string CTurnMessage::CreateMessage(bool bAddMessageIntegrity) //default true.
{
	string sBuf;

	// message type
	Serialize16(m_iMessageType, sBuf);
	
	// calculate the message length
	unsigned short iLen = 0;
	MessageAttribuiteMap::iterator iter = m_attrMap.begin();
	for (; iter != m_attrMap.end(); iter++)
	{
		string& sValue = iter->second;

		iLen += 4; // type + len;
		iLen += sValue.size();
	}

	// message length
	Serialize16(iLen, sBuf);

	// transaction ID
	SerializeCString(m_szTransactionID, 16, sBuf);

	// payload
	//MAGIC_COOKIE attribute must be the first attribute.
	if( m_attrMap.find( MAGIC_COOKIE ) != m_attrMap.end() )
	{
		iter = m_attrMap.find( MAGIC_COOKIE );
		string& sValue = iter->second;
		Serialize16(iter->first, sBuf);
		Serialize16(sValue.size(), sBuf);
		SerializeCString(sValue.c_str(), sValue.size(), sBuf);
	}

	iter = m_attrMap.begin();
	for (; iter != m_attrMap.end(); iter++)
	{
		if( iter->first == MAGIC_COOKIE )
			continue;
		if( iter->first == MESSAGE_INTEGRITY )
			continue;
		string& sValue = iter->second;
		Serialize16(iter->first, sBuf);
		Serialize16(sValue.size(), sBuf);
		SerializeCString(sValue.c_str(), sValue.size(), sBuf);
	}

	if( bAddMessageIntegrity )
	{
		//MESSAGE_INTEGRITY attribute must be the last attribute.
		if( m_attrMap.find( MESSAGE_INTEGRITY ) != m_attrMap.end() )
		{
			iter = m_attrMap.find( MESSAGE_INTEGRITY );
			string& sValue = iter->second;
			Serialize16(iter->first, sBuf);
			Serialize16(sValue.size(), sBuf);
			SerializeCString(sValue.c_str(), sValue.size(), sBuf);
		}
	}

	

	return sBuf;
}

int CTurnMessage::ParseErrorCode(const string& sErrorCode)
{
	/*
	if (sErrorCode.size() != 8)
	{
		DB_ASSERT(0);
		if (sAddress.size() < 8)
			return addr;
	}
	*/
	int iOffset = 0;

	unsigned short uIgnoed = 0;
	Deserialize16(uIgnoed, sErrorCode, iOffset);

	unsigned char uClass = 0;
	Deserialize8(uClass, sErrorCode, iOffset);

	unsigned char uNumber = 0;
	Deserialize8(uNumber, sErrorCode, iOffset);

	unsigned int uReason = 0;
	Deserialize32(uReason, sErrorCode, iOffset);


//	DB("class: "<<uClass);
//	DB("number: "<<uNumber);
//	DB("reason: "<<sReason.c_str());

	int iErrorCode = uClass*100 + uNumber;
	return iErrorCode;
}


AddressPortPair CTurnMessage::ParseAddress(const string& sAddress)
{
	AddressPortPair addr;

	if (sAddress.size() != 8)
	{
		DB_ASSERT(0);
		if (sAddress.size() < 8)
			return addr;
	}

	int iOffset = 0;

	unsigned char uIgnoed = 0;
	Deserialize8(uIgnoed, sAddress, iOffset);

	unsigned char uFamily = 0;
	Deserialize8(uFamily, sAddress, iOffset);
	DB_ASSERT(uFamily == 0x01);

	unsigned short uPort = 0;
	Deserialize16(uPort, sAddress, iOffset);

	unsigned int uAddr = 0;
	Deserialize32(uAddr, sAddress, iOffset);

	// return result in network byte order
	addr.first = htonl(uAddr);
	addr.second = htons(uPort);

	return addr;
}

string CTurnMessage::CreateAddressValue(AddressPortPair& addr)
{
	string sBuf;
	unsigned char uIgnored = 0;
	Serialize8(uIgnored, sBuf);
	unsigned char uFamily = 0x10;
	Serialize8(uFamily, sBuf);
	Serialize16(ntohs(addr.second), sBuf);
	Serialize32(ntohl(addr.first), sBuf);
	return sBuf;
}

AddressPortPair CTurnMessage::GetAddress(const string& sResponse, unsigned short iMessageType)
{
	AddressPortPair addr;

	if (!ParseMessage(sResponse))
	{
//		DB("11");
		return addr;
	}

	if(m_iMessageType == ALLOCATE_ERROR_RESPONSE)
	{
		if( m_attrMap.find(ERROR_CODE) != m_attrMap.end() )
		{
			m_iLastErrorCode = ParseErrorCode( m_attrMap[ERROR_CODE] );
			DB("error code :"<< m_iLastErrorCode);
		}
		return addr;
	}
	
	
	if (m_iMessageType != ALLOCATE_RESPONSE)
	{
		return addr;
	}

	if (m_attrMap.find(iMessageType) == m_attrMap.end())
		return addr;

	string sAddress = m_attrMap[iMessageType];

	return ParseAddress(sAddress);
}

string CTurnMessage::GetUserName()
{
	if (m_attrMap.find(USERNAME) == m_attrMap.end())
		return "";

	return m_attrMap[USERNAME];
}

string CTurnMessage::GetPassword()
{
	if (m_attrMap.find(PASSWORD) == m_attrMap.end())
		return "";

	return m_attrMap[PASSWORD];
}

string CTurnMessage::GetNonce()
{
	if (m_attrMap.find(NONCE) == m_attrMap.end())
		return "";

	return m_attrMap[NONCE];
}

string CTurnMessage::GetRealm()
{
	if (m_attrMap.find(REALM) == m_attrMap.end())
		return "";

	return m_attrMap[REALM];
}

string CTurnMessage::ComputeHmacOutput(bool bUseMd5)
{
		//Dummy value. This is because to calculate Hmac.
		string sDummy(20,'0');
		m_attrMap[MESSAGE_INTEGRITY] = sDummy;

		string sHmacInput = CreateMessage(false);

		//make hmac input size multiple of 64.
		int iQot = sHmacInput.size()%64;
		int iGap = ( iQot == 0 ) ? 0 : (64 - iQot);

		//DB_STAT(iGap);
		string sTemp(iGap, 0);
		sHmacInput.insert(sHmacInput.size(),sTemp);
		
		string sHex;
		if( bUseMd5 )
		{
			//make hmac key input using md5 hash.
			string sMd5Input = m_sUserName + COLON + m_sRealm + COLON + m_sPassword ;
			sHex = computeMD5(sMd5Input);
		}
		else
		{
			sHex = m_sPassword;
		}

		char hmac[20];
		string sHmac;
	
		computeHmac(hmac, sHmacInput.c_str(), sHmacInput.size(), sHex.c_str(),sHex.size() );
		sHmac.assign(hmac,20);

		return sHmac;

}

int CTurnMessage::GetLastErrorCode()
{
	return m_iLastErrorCode;
}
int CTurnMessage::GetLastResponseType()
{
	return m_iMessageType;
}
bool CTurnMessage::IsTurnMsg(const std::string& s)
{
#define MAGIC_COOKIE_VALUE              0x72c64bc6

	if(s.size() < 28)
                return false;
        u_int32_t ui32Temp;
	const char* p_szMessage = s.c_str();
        memcpy(&ui32Temp, &p_szMessage[24], 4);
//        DB("MAGIC_COOKIE field before ntohl: " << ui32Temp);
        ui32Temp = ntohl(ui32Temp);
//        DB("MAGIC_COOKIE field after ntohl: " << ui32Temp);
        if(MAGIC_COOKIE_VALUE != ui32Temp)
                return false;
        return true;
}

void eyeballTurn::computeHmac(char* hmac, const char* input, int length, const char* key, int sizeKey)
{
   unsigned int resultSize=0;
#ifndef _WIN32_WCE
   HMAC(EVP_sha1(), 
        key, sizeKey, 
        reinterpret_cast<const unsigned char*>(input), length, 
        reinterpret_cast<unsigned char*>(hmac), &resultSize);
   assert(resultSize == 20);
#endif
}


string eyeballTurn::computeMD5(const string& sMd5Input)
{
#ifndef _WIN32_WCE
   char p[16];
   string sMd5Output ;
   MD5(reinterpret_cast<const unsigned char*>(sMd5Input.c_str()), 
					sMd5Input.size(),
					reinterpret_cast<unsigned char*>(p));

   sMd5Output.assign(p,16);
   return sMd5Output;
#else
   return "";
#endif
}



string eyeballTurn::hex_string(const string& _str)
{
	string rc;
	char szHexBuf[5];

	for (string::size_type i = 0; i < _str.length(); i++)
	{
		sprintf(szHexBuf, "%.2hx", (unsigned char)_str[i]);
		rc += szHexBuf;
	}

	return rc;
}

string eyeballTurn::unhex_string(const string& _str)
{
	string rc;
	short int cHex;

	for (string::size_type i = 0; i < _str.length(); i += 2)
	{
		sscanf(_str.substr(i, 2).c_str(), "%hx", &cHex);
		rc += (char)cHex;
	}

	return rc;
}


