#include "Base.h"  // automatic use of precompiled headers 

#if 1 //for Linux yamada

#include "Socket.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/time.h>
#include <unistd.h>
#include <sys/poll.h>
#include <assert.h>
#include <errno.h>
#include <string.h>
#else //for Linux yamada
//#include "Tools_StdAfx.h"
#include "Socket.h"
//#include "dassert.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <winsock2.h>
//#include <sys/socket.h>
//#include <netinet/in.h>
//#include <netdb.h>
#include <io.h>
#include <time.h>
//#include <unistd.h>
//#include <sys/poll.h>
#include <errno.h>
#include <string.h>

#endif //for Linux yamada

Socket::Socket()
{
  socket = -1;
  isInError = false;
}


void Socket::connect(const char * host, int port)
{
  if (isOpen() || isError())
    throw IOException("Socket already open or had an error");

  sockaddr_in addr;
  addr.sin_family=AF_INET;
  addr.sin_port = htons(port);
  struct hostent* hostname = gethostbyname(host);

  if (hostname == NULL)
  {
    throw IOException("No such host %s: %s", host, strerror(errno));
  }

  // To avoid a solaris alignment complaint
#if 0 //for Linux yamada
  ASSERT(sizeof(addr.sin_addr) == sizeof(hostname->h_addr));
#endif //for Linux yamada
  memcpy(&addr.sin_addr, hostname->h_addr, sizeof(hostname->h_addr));
  //  *((long*)&addr.sin_addr) = *(long*)hostname->h_addr;

  int sock = ::socket(PF_INET, SOCK_STREAM, 0);
  if (sock == -1)
    throw IOException("Could not create socket.");

  if (::connect(sock, (sockaddr*)&addr, sizeof(sockaddr_in)) != 0)
  {
    ::close(sock);
    throw IOException("Could not connect to %s: %s", host, strerror(errno));
  }

  socket = sock;
}


int Socket::read(char * buf, int len)
{
  return readWithTimeout(buf, len, -1);
}


int Socket::readWithTimeout(char * buf, int len, long timeoutMillis)
{
  if (!isOpen() || isError())
    throw IOException("Socket is not open or had an error");

  try {
    return readWithTimeout(socket, buf, len, timeoutMillis);
  } catch (IOException) {
    setError();
    throw;
  }
}


int Socket::readFullyWithTimeout(char * buf, int len, long timeoutMillis)
{
  if (!isOpen() || isError())
    throw IOException("Socket is not open or had an error");

  try {
    return readFullyWithTimeout(socket, buf, len, timeoutMillis);
  } catch (IOException) {
    setError();
    throw;
  }
}


int Socket::readPascalStringWithTimeout(char * buf, long len, long timeoutMillis)
{
#if 1 //for Linux yamada
  timeval expiryTime;
  gettimeofday(&expiryTime, NULL);
  expiryTime = expiryTime + timeoutMillis*1000;
#else //for Linux yamada
  DWORD expiryTime = GetTickCount() + timeoutMillis;
#endif //for Linux for yamadaq\

  char stringLen;
  while (true)
  {
    // Can throw IOException, TimeoutExceededException
    int ret = readWithTimeout(&stringLen, 1, timeoutMillis);
    if (ret == 0)
      return 0;
    else
    {
#if 0 //for Linux yamada
      ASSERT(ret > 0);
#endif //for Linux yamada
      break;
    }
  }

  // Read enough data into buf.
  long bytesRead = 0;
  while (true)
  {
#if 1 //for Linux yamada
    timeval now;
    gettimeofday(&now, NULL);

    long timeToBlock = (expiryTime - now)/1000;
#else //for Linux yamada
//    timeval now;
    DWORD timeToBlock = expiryTime - GetTickCount();
//    gettimeofday(&now, NULL);

//    long timeToBlock = (expiryTime - now)/1000;
#endif //for Linux yamada

    // Can throw IOException, TimeoutExceededException
    int ret = readWithTimeout(buf+bytesRead, (stringLen<len?stringLen:len)-bytesRead, timeToBlock);
    if (ret == 0)
    {
      setError();
      throw IOException("Read only %d bytes of a %d-byte string before EOF", bytesRead, stringLen);
    }

#if 0 //for Linux yamada
    ASSERT(ret > 0);
#endif //for Linux yamada

    bytesRead += ret;
    if (bytesRead == len || bytesRead == stringLen)
      break;

    if (timeToBlock <= 0 && timeoutMillis >= 0)
      throw TimeoutExceededException("Timeout while reading string data");
  }

  // Discard remaining data in string.
  while (bytesRead < stringLen)
  {
#if 1 //for Linux yamada
    timeval now;
    gettimeofday(&now, NULL);
#else //for Linux yamada
//    timeval now;
//    gettimeofday(&now, NULL);
#endif //for Linux yamada

    char discard[256];

#if 1 //for Linux yamada
    long timeToBlock = (expiryTime - now)/1000;
#else //for Linux yamada
    long timeToBlock = expiryTime - GetTickCount();
#endif //for Linux yamada

    // Can throw IOException, TimeoutExceededException
    int ret = readWithTimeout(discard, (stringLen-bytesRead)<256?stringLen-bytesRead:256, timeToBlock);
    if (ret == 0)
    {
      setError();
      throw IOException("Read only %d bytes of a %d-byte string before EOF", bytesRead, stringLen);
    }

#if 0 //for Linux yamada
    ASSERT(ret > 0);
#endif //for Linux yamada
    bytesRead += ret;

    if (bytesRead == stringLen)
      break;

    if (timeToBlock < 0 && timeoutMillis >= 0)
      throw TimeoutExceededException("Timeout while reading excess string data");
  }

  if (bytesRead < len)
  {
    buf[bytesRead] = 0;
    return bytesRead;
  }

  buf[len-1] = 0;
  return len;
}


void Socket::writePascalString(char * buf, long timeoutMillis)
{
#if 0 //for Linux yamada
  ASSERT(strlen(buf) < 256);
#endif //for Linux yamada
  char len = strlen(buf);
  // Can throw IOException of TimeoutExceededException

  // This is careless as it can allow timeoutMillis*2 time for the
  // write.  Fix it later.
  write(&len, 1, timeoutMillis);
  write(buf, len, timeoutMillis);
}


void Socket::write(const char* buf, int len, long timeoutMillis)
{
  if (!isOpen() || isError())
    throw IOException("Socket is not open or had an error");

  try {
    write(socket, buf, len, timeoutMillis);
  } catch (IOException) {
    setError();
    throw;
  }
}


bool Socket::isReadable()
{
  if (!isOpen() || isError())
    return false;

#if 1 //for Linux yamada
  struct pollfd pollFd;
  pollFd.events = POLLIN;
  pollFd.fd = socket;

  int ret = poll(&pollFd, 1, 0);
  if (ret == 1)
    return true;

  return false;

#else // for Linux yamada
  return (pollReadable(socket, 0) == 1)?true:false;

#endif //for Linux yamada
}


bool Socket::isOpen() const
{
  if (socket < 0)
    return false;
  return true;
}


int Socket::getFd() const
{
  return socket;
}



void Socket::close()
{
  if (!isOpen())
    // Already closed.
    return;

#if 0 //for Linux yamada
  ASSERT(socket >= 0);
#endif //for Linux yamada

  ::close(socket);
  isInError = false;
  socket = -1;
}


int Socket::readWithTimeout(int socket, char* buf, int len, long timeoutMillis)
{
  // Determine expiry time and normalize.
#if 1 //for Linux yamada
  struct timeval expiryTime;
  gettimeofday(&expiryTime, NULL);
  expiryTime = expiryTime + timeoutMillis*1000;
  struct pollfd pollFd;
  pollFd.fd = socket;
#else //for Linux yamada
  DWORD expiryTime = GetTickCount() + timeoutMillis;
#endif //for Linux yamada

  int ret;

  while (true)
  {

#if 1 //for Linux yamada
    pollFd.events = POLLIN;
    struct timeval now;
    gettimeofday(&now, NULL);
    long timeToBlock = (expiryTime - now)/1000;
    if (timeoutMillis < 0)
      ret = poll(&pollFd, 1, -1);
    else if (timeToBlock < 0)
      ret = poll(&pollFd, 1, 0);
    else
      ret = poll(&pollFd, 1, timeToBlock);

#else //for Linux yamada
//    pollFd.events = POLLIN;
//    struct timeval now;
//    gettimeofday(&now, NULL);
//    long timeToBlock = (expiryTime - now)/1000;
    DWORD timeToBlock = expiryTime - GetTickCount();

    if (timeoutMillis < 0)
      ret = pollReadable(socket, -1);
    else if (timeToBlock < 0)
      ret = pollReadable(socket, 0);
    else
      ret = pollReadable(socket, timeToBlock);
#endif //for Linux yamada

    if (ret == 0)
    {
      // Poll timeout exceeded without error.  If our timeToBlock
      // was already <=0, throw.
      // It's probably ok to throw any time ret == 0, but
      // this code is safer.
      if ((timeToBlock <= 0) && (timeoutMillis >= 0))
        throw TimeoutExceededException("Timeout exceeded in read");
      continue;
    }

    if (ret < 0)
    {
      if (errno == EINTR)
        continue;
      else
      {
        throw IOException("Error in poll()");
      }
    }

    ret = ::read(socket, buf, len);

    if (ret > 0)
      return ret;

    // if ret<0, error
    // if zero bytes read, treat it as error
//    switch (errno)
//    {
//      default:
//        dprintf(1, "ret=%d, errno=%d\n", ret, errno);
        throw IOException("Unknown error in read");
//    }
  }
  // This should not be reachable.
}


int Socket::readFullyWithTimeout(int socket, char* buf, int len, long timeoutMillis)
{
#if 1 //for Linux yamada
  timeval expiryTime;
  gettimeofday(&expiryTime, NULL);
  expiryTime = expiryTime + timeoutMillis*1000;
#else //for Linux yamada
//  timeval expiryTime;
//  gettimeofday(&expiryTime, NULL);
//  expiryTime = expiryTime + timeoutMillis*1000;
  DWORD expiryTime = GetTickCount() + timeoutMillis;
#endif //for Linux yamada

  long totalBytesRead = 0;
  while (totalBytesRead < len)
  {
#if 1 //for Linux yamada
    timeval now;
    gettimeofday(&now, NULL);
    long timeToBlock = (expiryTime-now)/1000;
#else
//    timeval now;
//    gettimeofday(&now, NULL);
    long timeToBlock = expiryTime-GetTickCount();
#endif //for Linux yamada

    long bytesRead = readWithTimeout(socket, buf+totalBytesRead, len-totalBytesRead, timeToBlock);
    if (bytesRead == 0)
      return totalBytesRead;

    totalBytesRead += bytesRead;
    if (totalBytesRead == len)
      return totalBytesRead;

    if ((timeToBlock <= 0) && (timeoutMillis >= 0))
      throw TimeoutExceededException("Read only %d of %d bytes", totalBytesRead, len);
  }

  return totalBytesRead;
}


void Socket::write(int socket, const char* buf, int len, long timeoutMillis)
{
  // Determine expiry time and normalize.

#if 1 //for Linux yamada
  struct timeval expiryTime;
  gettimeofday(&expiryTime, NULL);
  expiryTime = expiryTime + timeoutMillis*1000;
#else //for Linux yamada
//  struct timeval expiryTime;
//  gettimeofday(&expiryTime, NULL);
//  expiryTime = expiryTime + timeoutMillis*1000;
  DWORD expiryTime = GetTickCount() + timeoutMillis;
#endif //for Linux yamada

  long bytesWritten = 0;

  while (true)
  {
    int err = ::write(socket, buf + bytesWritten, len - bytesWritten);

    if (err > 0)
    {
      bytesWritten += err;
      if (len == bytesWritten)
        return;
      else
        // If we haven't written everything, try again.
        continue;
    }

//    switch (errno)
//    {
//      default:
        // All other things are errors.
        throw IOException("Unknown error in write");
//    }
  }
}


#if 0 //for Linux yamada
int Socket::pollReadable(int fd, long timeoutMSec)
{
  timeval timeout;
  fd_set readFDs;

  // Set our timeout
  timeout.tv_sec = timeoutMSec/1000;
  timeout.tv_usec = (timeoutMSec%1000)*1000;

  // Set the file descriptor we're interested in
  FD_ZERO(&readFDs);
  FD_SET((unsigned int)fd, &readFDs);

  // Poll the given file descriptor
  if (timeoutMSec < 0)
    {
        return select(1, &readFDs, NULL, NULL, NULL);
    }
    else
    {
        return select(1, &readFDs, NULL, NULL, &timeout);
    }
}


int Socket::pollWritable(int fd, long timeoutMSec)
{
  timeval timeout = {0, 0};
  fd_set writeFDs;

  // Set our timeout
  timeout.tv_sec = timeoutMSec/1000;
  timeout.tv_usec = (timeoutMSec%1000)*1000;

  // Set the file descriptor we're interested in
  FD_ZERO(&writeFDs);
  FD_SET((unsigned int)fd, &writeFDs);

  // Poll the given file descriptor
  if (timeoutMSec < 0)
    {
        return select(1, &writeFDs, NULL, NULL, NULL);
    }
    else
    {
        return select(1, &writeFDs, NULL, NULL, &timeout);
    }
}
#endif //for Linux yamada


long operator-(const timeval& t1, const timeval& t2)
{
  return (t1.tv_sec-t2.tv_sec)*1000000+(t1.tv_usec - t2.tv_usec);
}


timeval operator+(timeval t1, long timeDiffMicros)
{
  t1.tv_usec += timeDiffMicros;
  t1.tv_sec += t1.tv_usec/1000000;
  t1.tv_usec %= 1000000;
  return t1;
}
