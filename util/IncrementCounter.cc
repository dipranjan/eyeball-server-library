// IncrementCounter.cc
// Implementation for the CIncrementCounter class

#include "IncrementCounter.h"

#include "debug.h"

//! Constructor
CIncrementCounter::CIncrementCounter()
: m_uiHighCount(0),
  m_uiLowCount(0)
{
  DB_ENTER(CIncrementCounter::CIncrementCounter);

  pthread_mutex_init(&m_Mutex, NULL);
}

//! Destructor
CIncrementCounter::~CIncrementCounter()
{
  DB_ENTER(CIncrementCounter::CIncrementCounter);

  pthread_mutex_destroy(&m_Mutex);
}

//! Get next count
unsigned int CIncrementCounter::GetNextCount()
{
  DB_ENTER(CIncrementCounter::GetNextCount1);

  unsigned int uiHighCount, uiLowCount;
  GetNextCount(uiHighCount, uiLowCount);
  return uiLowCount;
}

//! Get next count
void CIncrementCounter::GetNextCount(unsigned int& uiHighCount, unsigned int& uiLowCount)
{
  DB_ENTER(CIncrementCounter::GetNextCount2);

  pthread_mutex_lock(&m_Mutex);

  uiHighCount = m_uiHighCount;
  uiLowCount = m_uiLowCount++;
  if (m_uiLowCount == 0) {
    m_uiHighCount++;
  }
  
  pthread_mutex_unlock(&m_Mutex);
}


