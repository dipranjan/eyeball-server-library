// combo_crypt.h
// Combines simple_crypt and randomizing_crypt
//
// Zion Kwok
// May 8, 2006
// Eyeball Networks
///////////////////////////////////////////////

#ifndef COMBO_CRYPT_H_
#define COMBO_CRYPT_H_

#include <string>
using std::string;

string combo_encrypt(const string& sInput);
string combo_decrypt(const string& sInput);

#endif //COMBO_CRYPT_H_

