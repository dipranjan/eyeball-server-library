// ---------------------------------------------------------------------------
// File:       crypt.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Simple encryption routines, i.e. for writing to registry
// 
// Change Log:
// ---------------------------------------------------------------------------

#ifndef EC_CRYPT_H
#define EC_CRYPT_H

#include <string>

namespace Crypt
{
	std::string simple_crypt(const std::string& _str, bool bHex = true);
	std::string simple_decrypt(const std::string& _str, bool bHex = true);

	std::string get_passwd(
		const std::string& sPasswdFile,
		const std::string& sUser);

	bool set_passwd(
		const std::string& sPasswdFile,
		const std::string& sUser,
		const std::string& sPassword);
}

#endif // EC_CRYPT_H
