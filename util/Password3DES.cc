//Password3DES.cc
//
//Encrypts a password using Triple CBC DES
//
//Zion Kwok
//June 30, 2006
//
//(C) Eyeball Networks

#include "Password3DES.h"
#include "BinHex.h"
#include <openssl/des.h>
#include "debug.h"
#include <string.h>

bool password_3des(const string& sHexKey, const string& sUsername, const string& sInput, string& sOutput, bool bEncrypt)
{
  DB_ENTER(password_3des1);

  if (sHexKey.size() != 48) {
    return false;
  }

  const char* ch = sHexKey.data();
  string sHexKey1, sHexKey2, sHexKey3;
  sHexKey1.assign(ch, 16);
  sHexKey2.assign(&(ch[16]), 16);
  sHexKey3.assign(&(ch[32]), 16);

  return password_3des(sHexKey1, sHexKey2, sHexKey3, sUsername, sInput, sOutput, bEncrypt);
}

bool password_3des(const string& sHexKey1, const string& sHexKey2, const string& sHexKey3,
                   const string& sUsername, const string& sInput, string& sOutput, bool bEncrypt)
{
  DB_ENTER(password_3des2);

  bool bRet = false;

  int iInputLen = sInput.size();
  if (!bEncrypt && ((iInputLen & 7) || (iInputLen < 8))) {
    return false;
  }

  DES_cblock key1, key2, key3;
  if (check_3des_key_hex_and_parity(sHexKey1, key1) &&
      check_3des_key_hex_and_parity(sHexKey2, key2) &&
      check_3des_key_hex_and_parity(sHexKey3, key3))
  {
      DES_key_schedule ks1, ks2, ks3;

      DES_set_key_unchecked(&key1, &ks1);
      DES_set_key_unchecked(&key2, &ks2);
      DES_set_key_unchecked(&key3, &ks3);

      DES_cblock ivec1, ivec2;
      int iUsernameLength = sUsername.size();
      if (iUsernameLength >= 8) {
        memcpy(ivec1, sUsername.data(), 8);
      }
      else {
        memset(ivec1, 0xAB, 8);
        memcpy(ivec1, sUsername.data(), iUsernameLength);
      }

      DES_ecb_encrypt(&ivec1, &ivec2, &ks1, 1);

      if (bEncrypt) {
        int iNumPadding = 8 - (iInputLen & 7);
        int iPaddedLen = iInputLen + iNumPadding;
        unsigned char* pszPaddedInput = new unsigned char[iPaddedLen];
        memcpy(pszPaddedInput, sInput.data(), iInputLen);
        memset(&(pszPaddedInput[iInputLen]), iNumPadding, iNumPadding);

        sOutput.resize(iPaddedLen);
        DES_ede3_cbc_encrypt(pszPaddedInput, (unsigned char*) sOutput.data(),
                             iPaddedLen, &ks1, &ks2, &ks3, &ivec2, 1);
	delete [] pszPaddedInput;
        bRet = true;
      }
      else {
        sOutput.resize(iInputLen);
        DES_ede3_cbc_encrypt((unsigned char*) sInput.data(), (unsigned char*) sOutput.data(),
                             iInputLen, &ks1, &ks2, &ks3, &ivec2, 0);

	
        int iNumPadding = sOutput.at(iInputLen - 1);
        if ((1 <= iNumPadding) && (iNumPadding <= 8) && (iNumPadding <= iInputLen)) {
          sOutput.resize(iInputLen - iNumPadding);
          bRet = true;    
        }
        else {
          sOutput.clear();
        }
      }
  }

  return bRet;
}

//Does not check if the key is weak or not
bool check_3des_key_hex_and_parity(const string& sHexKey, unsigned char cBlock[8])
{
  DB_ENTER(check_3des_key_hex_and_parity);

  if (sHexKey.size() == 16) {
    if (convertHexToBin((unsigned char*)sHexKey.data(), 16, cBlock)) {
      //Check odd parity
      for (int i = 0; i < 8; i++) {
        int oneByte = (int)cBlock[i];
        int oddParity = 0;
        for (int b = 0; b < 8; b++) {
          oddParity ^= oneByte & 1;
          oneByte >>= 1;
        }
        if (oddParity == 0) {
          return false;
        }
      } //for
      return true;
    }
  }
  return false;
}



