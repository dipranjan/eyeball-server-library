#ifndef RLOG_H
#define RLOG_H

#include<sys/types.h>

int rlog_open(u_int32_t uAddress, u_int16_t uPort);  // net byte order

void rlog_close();

void rlog_set_monitor(u_int32_t uAddress, u_int16_t uPort);  // resets monitor address

int rlog(const string& sText); 

#endif //RLOG_H
