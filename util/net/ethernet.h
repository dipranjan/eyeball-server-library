// ---------------------------------------------------------------------------
// File:       Ethernet.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Ethernet helper routines
//
// Change Log:
// ---------------------------------------------------------------------------

#ifndef EYEBALL_NETWORK_ETHERNET_H
#define EYEBALL_NETWORK_ETHERNET_H

#include <string>
using std::string;

struct MACAddress
{
	MACAddress();

	string to_str(void) const;
	bool from_str(string& _str);

	unsigned char address[6];
};

#endif // EYEBALL_NETWORK_ETHERNET_H

