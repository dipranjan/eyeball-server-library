/***************************************************************************
                          csocket.cpp  -  description
                             -------------------
    begin                : Sun Oct 10 2004
    copyright            : (C) 2004 by root
    email                : root@zunnun
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "csocket.h"
#include <typeinfo> // test
#include <iostream>
using namespace std;

CSocket::CSocket()
{
  m_socketfd = 0;
}
//--------------------------------------------------------------------------------
CSocket::~CSocket()
{
  if(m_socketfd)
    close(m_socketfd);
}
//--------------------------------------------------------------------------------
int CSocket::Create(int type ,char* addr/*=NULL*/, unsigned int uiPort/* = SERV_UDP_PORT*/)
{
// type = 0 server
  m_socketfd = socket(AF_INET, SOCK_DGRAM,0);

  if(m_socketfd <0)
    return -1;

  // now binding

  bzero((char *) &m_Serv_Addr, sizeof(m_Serv_Addr));
  m_Serv_Addr.sin_family = AF_INET;
  ////if(type == UDPSERVER)
        if(strcmp(addr, "any"))
                m_Serv_Addr.sin_addr.s_addr = inet_addr(addr);  //htonl(INADDR_ANY);
        else
                m_Serv_Addr.sin_addr.s_addr = htonl(INADDR_ANY);
  ////else
  ////m_Serv_Addr.sin_addr.s_addr =   inet_addr(addr/*"10.1.100.152"*/);  
  
/*  if(type == UDPSERVER)	// commented 8-1-05
    m_Serv_Addr.sin_port = htons(SERV_UDP_PORT);
  else
    m_Serv_Addr.sin_port = htons(uiPort);   
*/

  m_Serv_Addr.sin_port = htons(uiPort);   // added 8-1-05
  
  if(type == UDPSERVER)
  {
    if ( bind(m_socketfd, (struct sockaddr*) &m_Serv_Addr, sizeof (m_Serv_Addr)) <0)
      return - 2;
  }
  else  
  {
    m_Cli_Addr.sin_family = AF_INET;
    m_Cli_Addr.sin_addr.s_addr = htonl(INADDR_ANY);    
    m_Cli_Addr.sin_port = htons(0);    
    if ( bind(m_socketfd, (struct sockaddr*) &m_Cli_Addr, sizeof (m_Cli_Addr)) <0)
      return - 2;
  }

  //printf("[SOCKET OK]\n");
  return 0;   // success
}

//--------------------------------------------------------------------------------
int CSocket::ReceiveFrom(char *buffer, char * clientIP ,unsigned int *uiPort)
{

  struct sockaddr *pCli_addr = (struct sockaddr *) &m_Serv_Addr;
  socklen_t clientlen = sizeof(m_Cli_Addr);

  int iRead = recvfrom(m_socketfd, buffer, MAXMSGSIZE, 0, pCli_addr, &clientlen);

  in_addr a;
  a = m_Serv_Addr.sin_addr;
  *uiPort = ntohs(m_Serv_Addr.sin_port);
  strcpy(clientIP,inet_ntoa(a));

  return iRead;
}

//--------------------------------------------------------------------------------
int CSocket::ReceiveFrom(char *buffer)
{

  struct sockaddr *pCli_addr = (struct sockaddr *) &m_Serv_Addr;
  socklen_t clientlen = sizeof(m_Cli_Addr);

  return recvfrom(m_socketfd,buffer,MAXMSGSIZE,MSG_WAITALL,pCli_addr,&clientlen);
}

//--------------------------------------------------------------------------------
int CSocket::SendTo(char *buffer, int size)
{
  struct sockaddr * pServ_addr;
  socklen_t serverlen;
  pServ_addr = (struct sockaddr*) &m_Serv_Addr;
  serverlen = sizeof(m_Serv_Addr);  
  return sendto(m_socketfd,buffer, size, MSG_DONTWAIT,pServ_addr,serverlen);
}
//--------------------------------------------------------------------------------
void CSocket::NonBlock()
{
	int flags = fcntl(m_socketfd, F_GETFL, 0);
	flags |= O_NONBLOCK | O_NDELAY;

	if (fcntl(m_socketfd, F_SETFL, flags) == -1)
	{
		//perror("fcntl");
    		printf("Error -> fcntl\n");
	}  
}
//--------------------------------------------------------------------------------
int CSocket::Getfd()
{
  return m_socketfd;
}
//--------------------------------------------------------------------------------
int CSocket::SendTo(char* addr, unsigned int iPort,char *buffer, int size)
{
//client.Create(UDPCLIENT,DNSLookUp(addr),iPort);

	struct sockaddr_in _Serv_addr;
	_Serv_addr.sin_family = AF_INET;
    	_Serv_addr.sin_addr.s_addr = inet_addr(addr);
    	_Serv_addr.sin_port = htons(iPort);    
	socklen_t serverlen;
	serverlen = sizeof(_Serv_addr);  
	struct sockaddr * pServ_addr = (struct sockaddr*) & _Serv_addr;
	return sendto(m_socketfd,buffer, size, MSG_DONTWAIT,pServ_addr,serverlen);
}
