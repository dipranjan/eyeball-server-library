// ---------------------------------------------------------------------------
// $Id: tcpchan.cc,v 1.6 2007/07/25 20:19:26 sshelfor Exp $
//
// File:       tcpchan.cc
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    TCP server and client channel classes
//
// ---------------------------------------------------------------------------

#include "tcpchan.h"

#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>

#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/tcp.h>		// TCP_NODELAY

#include "debug.h"
#include "inet.h"
#include "log.h"
#include "util.h"		// PERROR
#include "utypes.h"




// ---------------------------------------------------------------------------
// Classes
// ---------------------------------------------------------------------------

TCPServerChannel::TCPServerChannel()
{}

TCPServerChannel::~TCPServerChannel()
{}

void TCPServerChannel::close(int _fdConnSocket)
{
	//DB("close(_fdConnSocket)");
	::shutdown(_fdConnSocket, 2);
	while (::close(_fdConnSocket) != 0) {
		if (errno != EINTR) break;
	}
}

// Takes NETWORK byte order network
// Takes HOST byte order port
int TCPServerChannel::open(u_int16_t _port, u_int32_t uLocalAddr, bool bReuse)
{
	int fd = socket(AF_INET, SOCK_STREAM, 0);

	if (fd < 0)
	{
		PERROR("socket");
		return INVALID_SOCKET;
	}

	// Set socket to non-blocking.  According to accept(2) this
	// is necessary to ensure accept is non-blocking.
	nonblock(fd);

	// Ensure we can open this port again if the server is restarted
	if(bReuse) 
	{
	        //std::cout << "TCPServerChannel::open: sock reuse" << endl;
	        reuse(fd);
	}

	// Avoid blocking on close
	nolinger(fd);

	// No need to call sendbufsize since we never send on this socket

	struct sockaddr_in sin;
	memset((char *)&sin, 0, sizeof(sin));
	sin.sin_family = AF_INET;
    sin.sin_port = htons(_port);
	sin.sin_addr.s_addr = uLocalAddr;

	if (::bind(fd, (struct sockaddr *)&sin, sizeof(sin)) < 0)
	{
		PERROR("bind");
		close(fd);
		return INVALID_SOCKET;
	}


	int on = 1;
	if (setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, (char *) &on, sizeof(on)) < 0)
	{
		PERROR("setsockopt");
	}

	if (listen(fd, 1000) != 0)
	{
		PERROR("listen");
		close(fd);
		return INVALID_SOCKET;
	}

	// Success
	return fd;
}
//REZAAAAAAAAAAAAAAAAAAAAAAAAAA
int TCPServerChannel::open(sockaddr_storage addr, bool bReuse)
{
    int fd = socket(AF_INET, SOCK_STREAM, 0);

    if (fd < 0)
    {
        PERROR("socket");
        return INVALID_SOCKET;
    }

    // Set socket to non-blocking.  According to accept(2) this
    // is necessary to ensure accept is non-blocking.
    nonblock(fd);

    // Ensure we can open this port again if the server is restarted
    if(bReuse)
    {
            //std::cout << "TCPServerChannel::open: sock reuse" << endl;
            reuse(fd);
    }

    // Avoid blocking on close
    nolinger(fd);

    // No need to call sendbufsize since we never send on this socket

    struct sockaddr* sin=(struct sockaddr*)&addr;


    if (::bind(fd, sin, sizeof(addr)) < 0)
    {
    //    cout<<"ererrere"<<endl;
        PERROR("bind");
        close(fd);
        return INVALID_SOCKET;
    }


    int on = 1;
    if (setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, (char *) &on, sizeof(on)) < 0)
    {
        PERROR("setsockopt");
    }

    if (listen(fd, 1000) != 0)
    {
        PERROR("listen");
        close(fd);
        return INVALID_SOCKET;
    }

    // Success
    return fd;
}

int TCPServerChannel::accept(
	int _iListenSocket, u_int32_t& _fromaddr, u_int16_t& _fromport
)
{
	//DB_ENTER(TCPServerChannel::accept);

	struct sockaddr_in sin;
	memset((char *)&sin, 0, sizeof(sin));
	sin.sin_family = AF_INET;

	socklen_t sin_len = sizeof(sin);

	int ConnSock = ::accept(_iListenSocket, (struct sockaddr*)&sin, &sin_len);

	if (ConnSock < 0)
	{
		PERROR("accept");

		// No need to retry accept() since we go back to poll or select
		// loop and will get another event on the listen socket if
		// necessary.
		return INVALID_SOCKET;
	}

	// Make sure socket is non-blocking in case the OS we are under
	// didn't automatically this property from the corresponding
	// listen socket.
	nonblock(ConnSock);

	// Disable Nagle's algorithm for our command messages
	nodelay(ConnSock);

	// Avoid blocking on close
	nolinger(ConnSock);

	// Since we intend to send on this socket, set the send buffer
	// size to something reasonable.  This also to reduce behavioural
	// differences due to varying defaults in operating system
	// (e.g. Linux vs Solaris)
	sendbufsize(ConnSock);

	// Keep values in network byte order
	_fromaddr  = sin.sin_addr.s_addr;
	_fromport  = sin.sin_port;

	return ConnSock;
}

//REZAAAAAAAAAAAAAAAAAAAAAAAAA
int TCPServerChannel::accept(
    int _iListenSocket, sockaddr_storage& addr
)
{
    printf("Unimplemented TCPServerChannel::accept()\n");
    return -1;
}

ssize_t TCPServerChannel::recv(
	int _fdConnSocket,
	char *_pBuf, size_t _len,
	u_int32_t& _fromaddr, u_int16_t& _fromport
)
{
	//DB_ENTER(TCPServerChannel::recv);

	// Must have already accepted
	if (_fdConnSocket < 0)
	{
		_fromaddr = 0;
		_fromport = 0;

		return -1;
	}

	ssize_t cc = ::recv(_fdConnSocket, _pBuf, _len, 0);

	if (cc < 0)
	{
		_fromaddr = 0;
		_fromport = 0;
		return cc;
	}

	struct sockaddr_in sin;
	memset((char *)&sin, 0, sizeof(sin));
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = INADDR_ANY;

	socklen_t sin_len = sizeof(sin);

	// recvfrom doesn't seem to work under Solaris 8.  Most likely
	// due to IPv6 issues.  Call getpeername instead.
	if (getpeername(_fdConnSocket, (struct sockaddr *)&sin, &sin_len) != 0)
	{
		//PERROR("getpeername");
		_fromaddr = 0;
		_fromport = 0;
	} else {
	    // Keep values in network byte order
	    _fromaddr = sin.sin_addr.s_addr;
	    _fromport = sin.sin_port;
        }

	return cc;
}

ssize_t TCPServerChannel::recv(
    int _fdConnSocket,
    char *_pBuf, size_t _len,
    sockaddr_storage& fromaddr
)
{
    printf("Unimplemented TCPServerChannel::recv()\n");
    return -1;
}

int TCPServerChannel::send(int _fdConnSocket, const char *_pBuf, size_t _len)
{
	//DB_ENTER(TCPServerChannel::send);
	//DB_STAT(_fdConnSocket);

	if (_fdConnSocket < 0)
		return -1;

	int iRet = ::send(_fdConnSocket, _pBuf, _len, 0);
	if( iRet < 0)
	{
//		PERROR("send");
                //std::cerr << "TCPServerChannel::send failed on fd " << _fdConnSocket << ": " << strerror(errno) << std::endl;
	}
	return iRet;
}

TCPClientChannel::TCPClientChannel()
:
	m_iClientSocket(INVALID_SOCKET)
{
}

int TCPClientChannel::open(u_int32_t addr, u_int16_t port, u_int32_t uLocalAddr)
{
//	DB_ENTER(TCPClientChannel::open);
//	DB_STAT(addr);
//	DB_STAT(port);

	int fd = socket(AF_INET, SOCK_STREAM, 0);
	if (fd < 0)
	{
		PERROR("socket");
		return INVALID_SOCKET;
	}

	if( uLocalAddr != 0 )
	{
		struct sockaddr_in sin;
		memset((char *)&sin, 0, sizeof(sin));
		sin.sin_family = AF_INET;
		sin.sin_port = 0;
		sin.sin_addr.s_addr = uLocalAddr;

		if (::bind(fd, (struct sockaddr *)&sin, sizeof(sin)) < 0)
		{
			PERROR("bind");
			while (::close(fd) != 0) {
				if (errno != EINTR) break;
			}
			return INVALID_SOCKET;
		}
	}

	struct sockaddr_in sin;
	memset((char *)&sin, 0, sizeof(sin));
	sin.sin_family = AF_INET;
	sin.sin_port = htons(port);
	sin.sin_addr.s_addr = addr;
        DB("Going to connect to : " << intoa(sin.sin_addr.s_addr) << ':' << ntohs(sin.sin_port) << " from " << intoa(uLocalAddr));
	if (connect(fd, (struct sockaddr *)&sin, sizeof(sin)) < 0)
	{
		PERROR("connect");
		while (::close(fd) != 0) {
			if (errno != EINTR) break;
		}
		return INVALID_SOCKET;
	}

	// Leave socket blocking for receive
	nonblock(fd);

	// Avoid blocking on close
	nolinger(fd);

	// Success
	m_iClientSocket = fd;
	return fd;
}

//REZAAAAAAAAAAAA
int TCPClientChannel::open(sockaddr_storage addr, sockaddr_storage LocalAddr)
{
    printf("Unimplemented TCPClientChannel::open()\n");
    return -1;
}

void TCPClientChannel::close(void)
{
	if (m_iClientSocket > 0)
	{
		::shutdown(m_iClientSocket, 2);
		while (::close(m_iClientSocket) != 0) {
			if (errno != EINTR) break;
		}
	}

	m_iClientSocket = INVALID_SOCKET;
}

int TCPClientChannel::recv(char *_pBuf, size_t _len)
{
	//DB_ENTER(TCPClientChannel::recv);

	//DB_STAT(m_iClientSocket);

	int len = ::recv(m_iClientSocket, _pBuf, _len, 0);
	if (len < 0)
	{
		PERROR("recv");
		return -1;
	}

	return (len);
}

int TCPClientChannel::send(const char *_pBuf, size_t _len)
{
	//DB_ENTER(TCPClientChannel::send);

	if( m_iClientSocket < 0 )
		return -1;

	int len;
	if ( (len=::send(m_iClientSocket, _pBuf, _len, 0)) < 0)
		return -1;

	return len;
}
