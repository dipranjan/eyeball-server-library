// ---------------------------------------------------------------------------
// File:       inet.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Platform dependent Internet includes
//
// ---------------------------------------------------------------------------

#include "inet.h"

#include <ctype.h>
#include <unistd.h>
#include <netdb.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/tcp.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <cstring>
#include <iostream>
#include <cstdlib>
#include "checkip.h"
using std::string;
using std::list;

bool operator == (const sockaddr_storage& left,const sockaddr_storage& right)
{


    //return (memcmp(&left,&right,sizeof(sockaddr_storage))==0);
    return (address_to_string(left)==address_to_string(right));
    
    //REZAAAAA implement
}
bool operator < (const sockaddr_storage& left,const sockaddr_storage& right)
{
    //return (memcmp(&left,&right,sizeof(sockaddr_storage))<0);
    return (address_to_string(left)<address_to_string(right));
    
    //REZAAAAA implement
}

//REZA:@TEST
bool doSeparateIPv4AndPort(const std::string& str,std::string& ip,std::string& port)
{
    unsigned x = str.find_first_of(':');
    if ( x == string::npos ) return 0;

    ip = str.substr(0,x);
    port = str.substr(x+1);

    return isIPv4Address(ip) && isValidPort(port);
}
//REZA:@TEST
bool doSeparateIPv6AndPort(const std::string &str, std::string &ip, std::string &port)
{
    unsigned x = str.find_first_of(']');
    if( x== string::npos) return false;

    ip = str.substr(1,x-1);
    port = str.substr(x+2);
    return isIPv6Address(ip) && isValidPort(port);
}
//REZA:@TEST
bool doSeparateIPAndPort(const std::string& ip_port,std::string& ip,std::string& port)
{
    if ( ip_port.size() == 0 ) return 0;
    if ( ip_port[0] == '[' )
    {
        return doSeparateIPv6AndPort(ip_port,ip,port);
    }
    else
    {
        return doSeparateIPv4AndPort(ip_port,ip,port);
    }
}
//REZA:@TEST
sockaddr_storage getSocketAddress(const char* ipaddress,const unsigned short int port,bool isLocal)
{
    sockaddr_storage laddr;

    struct addrinfo hints, *res;
    int status;
    char port_buffer[6];

    sprintf(port_buffer, "%hu", port);
    //printf("port buffer = %s where port is %u\n",port_buffer,port);

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = 0;
    hints.ai_flags = AI_NUMERICHOST | AI_NUMERICSERV ;
    if ( isLocal ) hints.ai_flags |= AI_PASSIVE;

    if ((status = getaddrinfo(ipaddress, port_buffer, &hints, &res)) != 0){
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(status));
        if ( ipaddress != NULL ) fprintf(stderr,"ipaddress: %s\n",ipaddress);
        return laddr;
    }

    /* Note, we're taking the first valid address, there may be more than one */

    memcpy(&laddr, res->ai_addr, res->ai_addrlen);

    freeaddrinfo(res);
    return laddr;
}
void setPort(sockaddr_storage &addr, int port){
    if(addr.ss_family==AF_INET){
        sockaddr_in* ad = (sockaddr_in*)(&addr);
        ad->sin_port=htons(port);
    }else if(addr.ss_family==AF_INET6){
        sockaddr_in6* ad = (sockaddr_in6*)(&addr);
        ad->sin6_port=htons(port);
    }
}

//depricate
u_int32_t LookupHostAddr(const char *s)
{
	if (isdigit((int)*s))
		return (u_int32_t)inet_addr(s);
	else
	{
		struct hostent *hp = gethostbyname(s);
		if (hp == 0)
			/*XXX*/
			return (0);
		return *((u_int32_t **)hp->h_addr_list)[0];
	}
}

#include "debug.h"
//depricate
bool LookupHostAddr(const string& s, list<u_int32_t>& addr_list)
{
	addr_list.clear();

	if (isdigit((int)*s.c_str()))
	{
		addr_list.push_back((u_int32_t)inet_addr(s.c_str()));
		return true;
	}
	else
	{
		struct hostent *hp = gethostbyname(s.c_str());
		if (hp == 0)
			return false;

		int i = 0;
		while( ((u_int32_t **)hp->h_addr_list)[i] )
		{
			addr_list.push_back(*((u_int32_t **)hp->h_addr_list)[i]);
			i++;
		}
		return true;
	}
}

//depricate
u_int32_t LookupLocalAddr(void)
{
	char myhostname[256];

	gethostname(myhostname, sizeof(myhostname));
	return LookupHostAddr(myhostname);
}

/*
 * A faster replacement for inet_ntoa().
 * Extracted from tcpdump 2.1.
 */
//depricate
string intoa(u_int32_t addr)
{
	register char *cp;
	register u_int byte;
	register int n;
	char buf[sizeof(".xxx.xxx.xxx.xxx")];

	NTOHL(addr);
	cp = &buf[sizeof buf];
	*--cp = '\0';

	n = 4;
	do
	{
		byte = addr & 0xff;
		*--cp = byte % 10 + '0';
		byte /= 10;
		if (byte > 0)
		{
			*--cp = byte % 10 + '0';
			byte /= 10;
			if (byte > 0)
				*--cp = byte + '0';
		}
		*--cp = '.';
		addr >>= 8;
	} while (--n > 0);

	return cp + 1;
}

//REZA:@TEST
std::string intoa(const sockaddr_storage &addr)
{

    socklen_t len = 0;
    char str[INET6_ADDRSTRLEN+10]={0};
    if (addr.ss_family == AF_INET)
    {

        inet_ntop(addr.ss_family,&((sockaddr_in*)&addr)->sin_addr,str,INET_ADDRSTRLEN);
    }
    else if ( addr.ss_family == AF_INET6 )
    {
        inet_ntop(addr.ss_family,&((sockaddr_in6*)&addr)->sin6_addr,str,INET6_ADDRSTRLEN);
    }
    else
        return string("intoa() ERROR: UNKNOWN ADDRESS FAMILY");

    return std::string(str);
}

u_int16_t intop(const sockaddr_storage &addr)
{
    switch ( addr.ss_family )
    {
    case AF_INET:
    {
        sockaddr_in* ad = (sockaddr_in*)(&addr);

        return ntohs(ad->sin_port);
        break;
    }

    case AF_INET6:
    {
        sockaddr_in6* ad = (sockaddr_in6*)(&addr);
     //   printf("HUKKKKAAAA %hu",ad->sin6_port);

        return ntohs(ad->sin6_port);
        break;
    }
    default:
        return 0;
    }

}

void nonblock(int fd)
{
	int flags = fcntl(fd, F_GETFL, 0);
	flags |= O_NONBLOCK | O_NDELAY;

	if (fcntl(fd, F_SETFL, flags) == -1)
	{
		perror("fcntl");
	}
}

void reuse(int fd)
{
	int on = 1;
	if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (char *)&on,
		sizeof(on)) != 0)
	{
		perror("SO_REUSEADDR");
	}

#ifdef SO_REUSEPORT
	on = 1;
	if (setsockopt(fd, SOL_SOCKET, SO_REUSEPORT, (char *)&on,
		sizeof(on)) != 0)
	{
		perror("SO_REUSEPORT");
	}
#endif
}

bool sendbufsize(int fd, int size)
{
	// set the send buffer size
	int bufsize = size;

	if( setsockopt(fd, SOL_SOCKET, SO_SNDBUF, (char*)&bufsize, sizeof(bufsize))<0 )
	{
		return false;
	}
	return true;
}

bool recvbufsize(int fd, int size)
{
	// set the send buffer size
	int bufsize = size;

	if( setsockopt(fd, SOL_SOCKET, SO_RCVBUF, (char*)&bufsize, sizeof(bufsize))<0 )
	{
		return false;
	}
	return true;
}

void nolinger(int fd)
{
#ifndef __linux__
	// Linux kernel 2.2.x has a bug which sets the linger time to
	// maximum when it is zero!  For this reason, currently avoid
	// SO_LINGER under Linux.
	struct linger linger;
	linger.l_onoff = 1;
	linger.l_linger = 0;

	if (setsockopt(fd, SOL_SOCKET, SO_LINGER, (char *)&linger,
		sizeof(linger)) != 0)
	{
		perror("SO_LINGER");
	}
#endif
}

void nodelay(int fd)
{
	int on = 1;
	if (setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, (char *)&on,
		sizeof(on)) != 0)
	{
		perror("TCP_NODELAY");
	}
}

bool get_mac(char cMac[32])
{
	int skfd = socket(AF_INET, SOCK_DGRAM, 0);
	if( skfd == -1 )
		return false;

	struct ifreq ifr;
	char *ifname = "eth0";
	memset(cMac, 0, 32);

	strcpy(ifr.ifr_name, ifname);
	if( ioctl(skfd, SIOCGIFHWADDR, &ifr) < 0 )
		return false;

	memcpy(cMac, ifr.ifr_hwaddr.sa_data, 8);

	return true;
}

//REZA:@TEST
std::string address_to_string(const sockaddr_storage& addr) {
    std::string ret;


    if(addr.ss_family == AF_INET){
        char data[INET_ADDRSTRLEN+10]={0};
        sockaddr_in* sock =(sockaddr_in*) &addr;

        if ( inet_ntop(addr.ss_family,&sock->sin_addr,data,INET_ADDRSTRLEN) == NULL )
            fprintf(stderr,"Error: Failed to convert to string from sockaddr_storage using inet_ntop()\n");



        sprintf(data,"%s:%u",data,ntohs(sock->sin_port));
        ret = data;

    }else {
        sockaddr_in6* sock = (sockaddr_in6*) &addr;

        char data[INET6_ADDRSTRLEN+10]={0};
        if ( inet_ntop(addr.ss_family,&sock->sin6_addr,data,INET6_ADDRSTRLEN) == NULL )
            fprintf(stderr,"Error: Failed to convert to string from sockaddr_storage using inet_ntop()\n");

        char result[100]={0};
        sprintf(result,"[%s]:%u",data,ntohs(sock->sin6_port));
        ret = result;
    }

    return ret;
}

//REZA:@TEST
int getServerSocket(sockaddr_storage& addr,__socket_type socktype )
{
    std::string sIP = intoa(addr);
    int port = intop(addr);
    char sPort[10];
    sprintf(sPort,"%u",port);

    int status;
    struct addrinfo host_info;       // The struct that getaddrinfo() fills up with data.
    struct addrinfo *host_info_list; // Pointer to the to the linked list of host_info's.
    memset(&host_info, 0, sizeof host_info);

    host_info.ai_family = addr.ss_family;
    host_info.ai_socktype = socktype; // Use SOCK_STREAM for TCP or SOCK_DGRAM for UDP.
    host_info.ai_flags = AI_PASSIVE|AI_NUMERICHOST|AI_NUMERICSERV;     // IP Wildcard

    // Now fill up the linked list of host_info structs with google's address information.
    status = getaddrinfo(sIP.data(), sPort, &host_info, &host_info_list);
    // getaddrinfo returns 0 on succes, or some other value when an error occured.
    // (translated into human readable text by the gai_gai_strerror function).
    if (status != 0)  return INVALID_SOCKET;

    int iFdSocket ; // The socket descripter
    std::cout<<"hshshshhshshshshshshshs "<<(addr.ss_family==AF_INET?"AF_INET":"AF_INET6")<<std::endl;
    iFdSocket = socket(host_info_list->ai_family, host_info_list->ai_socktype,host_info_list->ai_protocol);
    return iFdSocket;
}
