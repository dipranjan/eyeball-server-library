// ---------------------------------------------------------------------------
// $Id: inet.h,v 1.1 2005/05/31 03:02:17 farhan Exp $
//
// File:       inet.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Platform dependent Internet includes
//
// ---------------------------------------------------------------------------

#ifndef EC_INET_H
#define EC_INET_H

#include <string>
#include <list>

#include <sys/types.h>
#ifdef WIN32
#include <windows.h>
#include <winsock.h>
#endif
#if defined(__osf__)
#include <machine/endian.h>
#endif
#if defined(__linux__)
#include <endian.h>
#endif
#ifndef IPPROTO_IP
#if defined(__ultrix__) && defined(__cplusplus)
extern "C" {
#include <netinet/in.h>
}
#else
#include <netinet/in.h>
#endif
#endif
#ifndef WIN32
#include <arpa/inet.h>
#endif

#include "utypes.h"

#ifndef WIN32
#define INVALID_SOCKET (-1)
#endif

#ifndef NTOHL
#if BYTE_ORDER==LITTLE_ENDIAN
#define NTOHL(d) ((d) = ntohl((d)))
#define NTOHS(d) ((d) = ntohs((d)))
#define HTONL(d) ((d) = htonl((d)))
#define HTONS(d) ((d) = htons((d)))
#elif BYTE_ORDER==BIG_ENDIAN
#define NTOHL(d)
#define NTOHS(d)
#define HTONL(d)
#define HTONS(d)
#else
#error error - BYTE_ORDER not defined.
#endif
#endif

#define SEND_BUFFER_SIZE (80 * 1024)
#define RECV_BUFFER_SIZE (80 * 1024)

// Uncomment on Solaris 2.6
//typedef int socklen_t;

const sockaddr_storage NULLsockaddr_storage={0};

bool doSeparateIPv4AndPort(const std::string& str,std::string& ip,std::string& port);
bool doSeparateIPv6AndPort(const std::string& str,std::string& ip,std::string& port);
bool doSeparateIPAndPort(const std::string& str,std::string& ip,std::string& port);

extern std::string intoa(u_int32_t addr);
extern std::string intoa(const sockaddr_storage& addr);
extern u_int16_t intop(const sockaddr_storage &addr);
extern int getServerSocket(sockaddr_storage& addr,__socket_type socktype );
sockaddr_storage getSocketAddress(const char* ipaddress, const unsigned short int port, bool isLocal=false);
u_int32_t LookupHostAddr(const char *s);
bool LookupHostAddr(const std::string& s, std::list<u_int32_t>& addr_list);
u_int32_t LookupLocalAddr(void);
void setPort(sockaddr_storage& addr, int port);
void nonblock(int fd);
void reuse(int fd);
bool sendbufsize(int fd, int size = SEND_BUFFER_SIZE);
bool recvbufsize(int fd, int size = RECV_BUFFER_SIZE);
void nolinger(int fd);
void nodelay(int fd);

bool get_mac(char cMac[32]);
extern std::string address_to_string(const sockaddr_storage& addr);
extern bool operator == (const sockaddr_storage& left,const sockaddr_storage& right);
extern bool operator < (const sockaddr_storage& left,const sockaddr_storage& right);
#endif // EC_INET_H

