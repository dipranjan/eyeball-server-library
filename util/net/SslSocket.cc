/**
 * There are switch blocks with only a default: case.  Remove the switch.
 * SslSocket.isReadable does a poll itself, but should probably call
 * the Socket.isReadable().
 * waitOnFd() should move to class Socket.
 */
#include "SslSocket.h"
#include "IOException.h"
#include "FileNotFoundException.h"
#include "TimeoutExceededException.h"
#include "Random.h"
#include "dassert.h"
#include "log.h"
// on Solaris use FopenPool
#ifdef SOLARIS_FOPENPOOLISBROKEN
 #include "FopenPool.h"
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/time.h>
#include <unistd.h>
#if defined(linux)
#include <sys/ioctl.h>  // For FIONBIO
#else
// This is to get the BSD-style FIONBIO symbol on SysV Solaris
#include <sys/filio.h>
#endif
#include <sys/poll.h>
#include <assert.h>
#include <errno.h>
#include <string.h>
#include <openssl/rand.h>
#include "debug.h"


bool SslSocket::isLibraryInited = false;
bool SslSocket::initInProgress = false;


SslSocket::SslSocket()
{
  initLibrary();

  sslContext = SSL_CTX_new(SSLv3_method());
  // I have no idea how the SSL_CTX_new could fail.
  assert(sslContext != NULL);
  ssl = NULL;
  isInError = false;
}

SslSocket::~SslSocket()
{
//	LOG("SSLSOCKET_DESTRUCTOR");
  SSL_CTX_free(sslContext);
  sslContext = NULL;
}

// This will need to call ssl_set_connect_state()
void SslSocket::connect(const char* host, int port)
{
  if (isOpen())
    throw IOException("Socket is already connected.\n");

  sockaddr_in addr;
  addr.sin_family=AF_INET;
  addr.sin_port = htons(port);
  struct hostent* hostname = gethostbyname(host);

  if (hostname == NULL)
    {
      throw IOException("No such host %s: %s", host, strerror(errno));
    }

  // To avoid a solaris alignment complaint
  assert(sizeof(addr.sin_addr) == sizeof(hostname->h_addr));
  memcpy(&addr.sin_addr, hostname->h_addr, sizeof(hostname->h_addr));
  //  *((long*)&addr.sin_addr) = *(long*)hostname->h_addr;

  int sock = ::socket(PF_INET, SOCK_STREAM, 0);
  if (sock == -1)
    throw IOException("Could not create socket.");

  if (::connect(sock, (sockaddr*)&addr, sizeof(sockaddr_in)) != 0)
    {
      ::close(sock);
      throw IOException("Could not connect to %s: %s", host, strerror(errno));
    }

  try {
    connect(sock);
  } catch (...) {
    ::shutdown(sock, 2);
    ::close(sock); // To match the socket() call.
    throw;
  }
}

void SslSocket::connect(const char* host, int port, int timeout)
{
  if (isOpen())
    throw IOException("Socket is already connected.");

  sockaddr_in addr;
  addr.sin_family=AF_INET;
  addr.sin_port = htons(port);
  struct hostent* hostname = gethostbyname(host);

  if (hostname == NULL)
    {
      throw IOException("No such host %s: %s", host, strerror(errno));
    }

  // To avoid a solaris alignment complaint
  assert(sizeof(addr.sin_addr) == sizeof(hostname->h_addr));
  memcpy(&addr.sin_addr, hostname->h_addr, sizeof(hostname->h_addr));
  //  *((long*)&addr.sin_addr) = *(long*)hostname->h_addr;

  int sock = ::socket(PF_INET, SOCK_STREAM, 0);
  if (sock == -1)
    throw IOException("Could not create socket.");

  int iOriginal = fcntl(sock, F_GETFL, 0);
  int iNonBlock = iOriginal | O_NONBLOCK;
  fcntl(sock, F_SETFL, iNonBlock);
	
  if (::connect(sock, (sockaddr*)&addr, sizeof(sockaddr_in)) != 0)
  {
    if( errno != EINPROGRESS )
    {
      ::close(sock);
      throw IOException("Could not connect to %s: %s", host, strerror(errno));
    }
    pollfd fdProgress;
    fdProgress.fd = sock;
    fdProgress.events = POLLOUT;

    int iPoll = poll(&fdProgress, 1, timeout);
    if( iPoll == 0 )
    {
      ::close(sock);
      throw IOException("Timeout during connection to %s", host);
    }
    else if( iPoll == -1 )
    {
      ::close(sock);
      throw IOException("Poll error during connect to %s: %s", host, strerror(errno));
    } 
    if( fdProgress.revents != POLLOUT )
    {
      ::close(sock);
      throw IOException("Could not connect to %s", host);
    }
    fcntl(sock, F_SETFL, iOriginal);
  }

  try {
    connect(sock);
  } catch (...) {
    ::shutdown(sock, 2);
    ::close(sock); // To match the socket() call.
    throw;
  }
}

void SslSocket::connect(int fd)
{
  if (isOpen())
    throw IOException("Socket is already connected.\n");

  //  initSslContextAsClient();

  unsigned long l=1;
  if (BIO_socket_ioctl(fd,FIONBIO,&l) < 0) 
    {
      throw IOException("Could not set BIO to non-blocking.");
    }

  ssl = SSL_new(sslContext); 
  assert(ssl != NULL);
  BIO* sbio = BIO_new_socket(fd,BIO_NOCLOSE);
  assert(sbio != NULL);
  SSL_set_bio(ssl,sbio,sbio);
  SSL_set_connect_state(ssl); 

  // This will not complete immediately, but will eventually cause
  // the socket to become readable.
  SSL_connect(ssl);
}


void SslSocket::setCertificate(const char* certificateFile, const char* privateKeyFile)
{
  if (isOpen())
    throw IOException("Socket is already connected.\n");

  FILE * fp2, * fp3;

  // on Solaris use FopenPool
  #ifdef SOLARIS_FOPENPOOLISBROKEN
   fp2 = FopenPool::Fopen(privateKeyFile, "r");
  #else
   fp2 = fopen(privateKeyFile, "r");
  #endif
  if (NULL == fp2)
  {
    throw IOException("Unable to open file %s to retrieve private key", privateKeyFile);
  }

  #ifdef SOLARIS_FOPENPOOLISBROKEN
   fp3 = FopenPool::Fopen(certificateFile, "r");
  #else
   fp3 = fopen(certificateFile, "r");
  #endif
  if (NULL == fp3)
  {
		int myerrno = errno;
    #ifdef SOLARIS_FOPENPOOLISBROKEN
     FopenPool::Fclose(fp2);
    #else
     fclose(fp2);
    #endif
    throw IOException("Unable to open file %s to retrieve certificate, errno = %d, %s", certificateFile, myerrno, strerror(myerrno));
  }

  try {
    setCertificate(fp2, fp3);
  } catch (Exception) {
    #ifdef SOLARIS_FOPENPOOLISBROKEN
     FopenPool::Fclose(fp3);
     FopenPool::Fclose(fp2);
    #else
     fclose(fp3);
     fclose(fp2);
    #endif
    throw;
  }

  #ifdef Solaris_FOPENPOOLISBROKEN
   FopenPool::Fclose(fp3);
   FopenPool::Fclose(fp2);
  #else
   fclose(fp3);
   fclose(fp2);
  #endif


  // I'm unconvinced this stuff is important.  I don't really know why
  // it's needed or what the impact is.
  /*
  int i;

  for (i=0; i<CRYPTO_NUM_LOCKS; i++)
    {
      lock_count[i]=0;
      // rwlock_init(&(lock_cs[i]),USYNC_THREAD,NULL);
      pthread_mutex_init(&(lock_cs[i]), NULL);
    }

  CRYPTO_set_id_callback(solaris_thread_id);
  CRYPTO_set_locking_callback(solaris_locking_callback);
*/
}


/*
void SslSocket::accept(int fd, const char* dhParamsFile, const char* privateKeyFile, const char* certificateFile)
{
  if (isOpen())
    throw IOException("Socket is already connected.\n");

  FILE * fp1, * fp2, * fp3;

  // on Solaris use FopenPool
  #ifdef SOLARIS_FOPENPOOLISBROKEN
   fp1 = FopenPool::Fopen(dhParamsFile, "r");
  #else
   fp1 = fopen(dhParamsFile, "r");
  #endif
  if (NULL == fp1)
  {
    setError();
    throw IOException("Unable to open file %s to retrieve DH params", dhParamsFile);
  }

  #ifdef SOLARIS_FOPENPOOLISBROKEN
   fp2 = FopenPool::Fopen(privateKeyFile, "r");
  #else
   fp2 = fopen(privateKeyFile, "r");
  #endif
  if (NULL == fp2)
  {
    #ifdef SOLARIS_FOPENPOOLISBROKEN
     FopenPool::Fclose(fp1);
    #else
     fclose(fp1);
    #endif
    setError();
    throw IOException("Unable to open file %s to retrieve private key", privateKeyFile);
  }

  #ifdef SOLARIS_FOPENPOOLISBROKEN
   fp3 = FopenPool::Fopen(certificateFile, "r");
  #else
   fp3 = fopen(certificateFile, "r");
  #endif
  if (NULL == fp3)
  {
    #ifdef SOLARIS_FOPENPOOLISBROKEN
     FopenPool::Fclose(fp2);
     FopenPool::Fclose(fp1);
    #else
     fclose(fp2);
     fclose(fp1);
    #endif
    setError();
    throw IOException("Unable to open file %s to retrieve certificate", certificateFile);
  }

  try {
    accept(fd, fp1, fp2, fp3);
  } catch (Exception ex) {
    #ifdef SOLARIS_FOPENPOOLISBROKEN
     FopenPool::Fclose(fp3);
     FopenPool::Fclose(fp2);
     FopenPool::Fclose(fp1);
    #else
     fclose(fp3);
     fclose(fp2);
     fclose(fp1);
    #endif
    throw ex;
  }

  #ifdef Solaris-FOPENPOOLISBROKEN
   FopenPool::Fclose(fp3);
   FopenPool::Fclose(fp2);
   FopenPool::Fclose(fp1);
  #else
   fclose(fp3);
   fclose(fp2);
   fclose(fp1);
  #endif
}
*/


void SslSocket::accept(int fd)
{
  if (isOpen())
    throw IOException("Socket is already connected.\n");

  /*
  try {
    initSslContextAsServer(dhParamsF, privateKeyF, certificateF);
  } catch (FileNotFoundException ex) {
    throw IOException("%s", ex.getDetails());
  }
  */

  unsigned long l=1;
  if (BIO_socket_ioctl(fd,FIONBIO,&l) < 0)
    throw IOException("Could not set BIO to non-blocking.");

  ssl = SSL_new(sslContext);
  BIO* sbio = BIO_new_socket(fd,BIO_NOCLOSE);
  SSL_set_bio(ssl,sbio,sbio);
  SSL_set_accept_state(ssl);

  // This will not complete immediately, but will eventually cause
  // the socket to become readable.
  SSL_accept(ssl);
}


void SslSocket::finishNegotiation(long timeoutMillis)
{
  char ch;
  this->write(&ch, 0, timeoutMillis);
}


int SslSocket::read(char* buf, int len)
{
  return readWithTimeout(buf, len, -1);
}


int SslSocket::readWithTimeout(char* buf, int len, long timeoutMillis)
{
  if (!isOpen() || isError())
    throw IOException("SslSocket is not open or had a previous error");

  try {
    return SslSocket::readWithTimeout(ssl, buf, len, timeoutMillis);
  } catch (IOException) {
    setError();
    throw;
  }
}


int SslSocket::readFullyWithTimeout(char* buf, int len, long timeoutMillis)
{
  if (!isOpen() || isError())
    throw IOException("SslSocket is not open or had a previous error");

  try {
    return SslSocket::readFullyWithTimeout(ssl, buf, len, timeoutMillis);
  } catch (IOException) {
    setError();
    throw;
  }
}


int SslSocket::readPascalStringWithTimeout(char* buf, long len, long timeoutMillis)
{
  timeval expiryTime;
  gettimeofday(&expiryTime, NULL);
  expiryTime = expiryTime + timeoutMillis*1000;
  if (timeoutMillis == -1)
    expiryTime.tv_sec = expiryTime.tv_usec = 0;

  char stringLen;
  while (true)
    {
      // Can throw IOException, TimeoutExceededException
      int ret = readWithTimeout(&stringLen, 1, timeoutMillis);
      if (ret == 0)
	return 0;
      else
	{
	  assert(ret > 0);
	  break;
	}
    }

  // Read enough data into buf.
  long bytesRead = 0;
  while (true)
    {
      timeval now;
      gettimeofday(&now, NULL);

      long timeToBlock = (expiryTime - now)/1000;
      // Can throw IOException, TimeoutExceededException
      int ret = readWithTimeout(buf+bytesRead, (stringLen<len?stringLen:len)-bytesRead, timeToBlock);
      if (ret == 0) 
	{
	  setError();
	  throw IOException("Read only %d bytes of a %d-byte string before EOF", bytesRead, stringLen);
	}

      assert(ret > 0);

      bytesRead += ret;
      if (bytesRead == len || bytesRead == stringLen)
	break;

      if (timeToBlock <= 0 && timeoutMillis >= 0)
	throw TimeoutExceededException("Timeout while reading string data");
    }

  // Discard remaining data in string.
  while (bytesRead < stringLen)
    {
      timeval now;
      gettimeofday(&now, NULL);

      char discard[256];
      long timeToBlock = (expiryTime - now)/1000;
      // Can throw IOException, TimeoutExceededException
      int ret = readWithTimeout(discard, (stringLen-bytesRead)<256?stringLen-bytesRead:256, timeToBlock);
      if (ret == 0) 
	{
	  setError();
	  throw IOException("Read only %d bytes of a %d-byte string before EOF", bytesRead, stringLen);
	}

      assert(ret > 0);
      bytesRead += ret;

      if (bytesRead == stringLen)
	break;

      if (timeToBlock < 0 && timeoutMillis >= 0)
	throw TimeoutExceededException("Timeout while reading excess string data");
    }

  if (bytesRead < len)
    {
      buf[bytesRead] = 0;
      return bytesRead;
    } 

  buf[len-1] = 0;
  return len;
}


void SslSocket::writePascalString(char* buf, long timeoutMillis)
{
  assert(strlen(buf) < 256);
  char len = strlen(buf);
  // Can throw IOException of TimeoutExceededException
  db_printf(0, "Writing pascal string len %d: %s", len, buf);

  // This is careless as it can allow timeoutMillis*2 time for the
  // write.  Fix it later.
  write(&len, 1, timeoutMillis);
  write(buf, len, timeoutMillis);
}


void SslSocket::writeStr(const char* str, long timeoutMillis)
{
  SslSocket::write(str, strlen(str), timeoutMillis);
}


void SslSocket::write(const char* buf, int len, long timeoutMillis)
{
  if (!isOpen() || isError())
    throw IOException("Socket is not open or had an error");

  try {
    SslSocket::write(ssl, buf, len, timeoutMillis);
  } catch (IOException) {
    setError();
    throw;
  }
}


bool SslSocket::isReadable()
{
  if (!isOpen() || isError())
    return false;

  if (SSL_pending(ssl) > 0)
    return true;

  pollfd pollFd;
  pollFd.events = POLLIN; 
  pollFd.fd = SSL_get_fd(ssl);

  int ret = poll(&pollFd, 1, 0);
  if (ret == 1)
    return true;

  return false;
}


int SslSocket::getFd() const
{
  if (!isOpen())
    return -1;

  return SSL_get_fd(ssl);
}

  

void SslSocket::close()
{
  if (!isOpen())
    // Already closed.
    return;
    
  int fd = getFd();
  assert(fd >= 0);
  //db_printf(1, "SslSocket::close() closing socket %d\n", fd);

  timeval expiryTime;
  gettimeofday(&expiryTime, NULL);
  //  expiryTime = expiryTime + timeoutMillis*1000;
  expiryTime = expiryTime + 10000*1000;
  
  do
    {
      int ret = SSL_shutdown(ssl);

      if (ret > 0)
	break;

		// put this break here because server was spinning
//		printf("Error in SSL shutdown: %s\n", getErrorText(ssl, ret));
		SSL_set_shutdown(ssl, SSL_SENT_SHUTDOWN | SSL_RECEIVED_SHUTDOWN);
		break;

      try {
	switch (SSL_get_error(ssl, ret))
	  {
	  case SSL_ERROR_NONE:
	    // I don't think this can happen.  ret was <= 0, but no
	    // error from SSL_get_error()?
	    assert(false);
	    continue;
	  case SSL_ERROR_SYSCALL:
		SSL_set_shutdown(ssl, SSL_SENT_SHUTDOWN | SSL_RECEIVED_SHUTDOWN);
	      break; // normal eof.  Close enough to a received_shutdown
	  case SSL_ERROR_WANT_READ:
	    waitOnFd(getFd(), POLLIN, expiryTime);
	    break;
	  case SSL_ERROR_WANT_WRITE:
	    waitOnFd(getFd(), POLLOUT, expiryTime);
	    break;
	  default:
		SSL_set_shutdown(ssl, SSL_SENT_SHUTDOWN | SSL_RECEIVED_SHUTDOWN);
	    break;
	  }
      } catch (TimeoutExceededException ex) {
	SSL_set_shutdown(ssl, SSL_SENT_SHUTDOWN | SSL_RECEIVED_SHUTDOWN);
	//DB("Shutdown response took too long.  Shutting down forcefully.  " << ex.getDetails());
	break;
      } catch (IOException ex) {
	SSL_set_shutdown(ssl, SSL_SENT_SHUTDOWN | SSL_RECEIVED_SHUTDOWN);
	//DB("Error from shutdown.  Shutting down forcefully.  " << ex.getDetails());
	break;
      }
    }
  while ((SSL_get_shutdown(ssl) & SSL_RECEIVED_SHUTDOWN) == 0);

  SSL_free(ssl);
  ssl = NULL;
//	LOG("SSLSOCKET_FREE_MEMORY");
  //  SSL_CTX_free(sslContext);
  //  sslContext = NULL;
  ::shutdown(fd, 2);
  ::close(fd);
  fd = -1;

  isInError = false;
}





void SslSocket::initLibrary()
{
  if (isLibraryInited)
    return;
  if (initInProgress)
    return;
  initInProgress = true;

  SSL_load_error_strings();
  SSL_library_init();

  initRandomState();

  isLibraryInited = true;
}


/**
 * 1) Finds out the file name to use for random state file:
 *   - if RANDFILE env. var is set, use that
 *   - if /dev/random is available, use that
 *   - use the default supplied by RAND_file_name() (~/.rnd)
 * 2) Read random bytes from the file.
 *   If failed (no such file, or not enough numbers) generate them
 *   and read again.
 */

#define  RANDFILE_ENV "RANDFILE"
#define  RANDDEV "/dev/random"
#define  RANDCOUNT  256

int SslSocket::initRandomState(void)
{
  char randFile[300];
  int bytesRead;
  int fd;
  int randCount = RANDCOUNT;

  char * randFile_env = getenv(RANDFILE_ENV);
  if (NULL != randFile_env)
  {
    strcpy(randFile, randFile_env);
    //db_printf(1, "Environment variable %s had value '%s'.\n", RANDFILE_ENV, randFile);
  } else {
    //db_printf(1, "Environment variable %s NOT set.\n", RANDFILE_ENV);
    // see if there is a /dev/random
    fd = open(RANDDEV, O_RDONLY);
    if (fd >= 0)
    {
      ::close(fd);
      //db_printf(1, "Device %s available.\n", RANDDEV);
      strcpy(randFile, RANDDEV);
      // read only one byte from /dev/random
      randCount = 1;
    } else {
      //db_printf(1, "Device %s NOT available.\n", RANDDEV);
      // use default file, generate if it does not exists or has not enough numbers
      RAND_file_name(randFile, 300);
      //db_printf(1, "Using default filename %s.\n", RANDFILE_DEFAULT);
    }
  }

  db_printf(1, "Using file '%s' for initial random state.\n", randFile);

  // try to read numbers
  bytesRead = RAND_load_file(randFile, randCount);
  if (bytesRead < (randCount-2))
  {
    db_printf(1, "Could not read enough numbers from random state file %s\n", randFile);
    // not enough (or none)!  Try to generate
    fd = open(randFile, O_WRONLY | O_CREAT | O_APPEND, S_IRWXU);
    if (fd < 0)
    {
      db_printf(1, "ERROR: random state file %s has not enough numbers in it, and "\
        "cannot be written! (errno %d)\n", randFile, errno);
      return 0;
    }
    // write numbers
    unsigned char by;
    int bytesWritten = 0;
    for (int i=0; i<randCount; i++)
    {
      by = Random::getNextRandom(256);
      bytesWritten += ::write(fd, &by, 1);
    }
    ::close(fd);
    db_printf(1, "%d random bytes generated into file %s\n", bytesWritten, randFile);
  }

  // try to read again
  bytesRead = RAND_load_file(randFile, randCount);
  if (bytesRead < (randCount-2))
  {
    db_printf(1, "Could not read enough numbers from generated random state file '%s'.\n",
      randFile);
    return bytesRead;
  }

  db_printf(1, "%d random bytes read from random state file '%s'.\n", bytesRead, randFile);
  return bytesRead;
}


void SslSocket::setCertificate(FILE* certificateFile, FILE* privateKeyFile)
{
  EVP_PKEY * pkey = NULL;
  int err;
  errno=0;
  rewind(privateKeyFile);
  if (errno)
    db_printf(1, "An error occured while doing a rewind in SSL private key file\n");

  pkey = PEM_read_PrivateKey(privateKeyFile, NULL,
    sslContext->default_passwd_callback,
    sslContext->default_passwd_callback_userdata);
  if (pkey == NULL)
  {
    throw IOException("Unable to read private key file");
  }

  err = SSL_CTX_use_PrivateKey(sslContext, pkey);
  if (err != 1)
  {
    setError();
    char buf[5000];
    getErrorString(buf, sizeof(buf));
    throw IOException("Unable to read private key file.  SSL error=%s", buf);
  }
  if (NULL != pkey) EVP_PKEY_free(pkey);

  // instead of the line below, we use two functions, to be able
  // to do without an fopen()
  //err = SSL_CTX_use_certificate_file(sslContext, "crypto.pem", SSL_FILETYPE_PEM);
  X509 * xkey = NULL;
  errno=0;
  rewind(certificateFile);
  if (errno)
    db_printf(1, "An error occured while doing a rewind in SSL certificate file\n");

  xkey = PEM_read_X509(certificateFile, NULL,
    sslContext->default_passwd_callback,
    sslContext->default_passwd_callback_userdata);
  if (xkey == NULL)
  {
    setError();
    throw IOException("Unable to read certificate file");
  }

  err = SSL_CTX_use_certificate(sslContext, xkey);
  if (err != 1)
  {
    char buf[5000];
    getErrorString(buf, sizeof(buf));
    throw IOException("Unable to read certificate file.  SSL error=%s\n", buf);
  }
  if (xkey != NULL) X509_free(xkey);
}


/*
void SslSocket::initSslContextAsClient()
{
  initLibrary();

  //  assert(sslContext == NULL);
  //  sslContext = SSL_CTX_new(SSLv3_client_method());
  //  assert(sslContext != NULL);
}
*/


/*
void SslSocket::initSslContextAsServer(FILE * dhParamsF, FILE * privateKeyF, FILE * certificateF)
{
  //  initLibrary();

  //  assert(sslContext == NULL);
  //  sslContext = SSL_CTX_new(SSLv3_server_method());
  //  assert(sslContext != NULL);

  SSL_CTX_set_options(sslContext, 0);

  // Set DH params in sslContext
  DH* dh=NULL;
  errno=0;
  rewind(dhParamsF);
  if (errno)
    db_printf(1, "An error occured while doing a rewind in SSL DH params file\n");

  dh = PEM_read_DHparams(dhParamsF, NULL, NULL, NULL);
  if (dh == NULL)
  {
    setError();
    throw IOException("Could not retrieve DH params (perhaps malformed)");
  }

  SSL_CTX_set_tmp_dh(sslContext,dh);
  DH_free(dh);

  // Set private key and certificate files in sslContext

  // instead of the line below, we use two functions, to be able
  // to do without an fopen()
  //int err = SSL_CTX_use_PrivateKey_file(sslContext, privateKeyFile, X509_FILETYPE_PEM);
  EVP_PKEY * pkey = NULL;
  int err;
  errno=0;
  rewind(privateKeyF);
  if (errno)
    db_printf(1, "An error occured while doing a rewind in SSL private key file\n");

  pkey = PEM_read_PrivateKey(privateKeyF, NULL,
    sslContext->default_passwd_callback,
    sslContext->default_passwd_callback_userdata);
  if (pkey == NULL)
  {
    setError();
    throw IOException("Unable to read private key file");
  }

  err = SSL_CTX_use_PrivateKey(sslContext, pkey);
  if (err != 1)
  {
    setError();
    char buf[5000];
    getErrorString(buf, sizeof(buf));
    throw IOException("Unable to read private key file.  SSL error=%s", buf);
  }
  if (NULL != pkey) EVP_PKEY_free(pkey);

  // instead of the line below, we use two functions, to be able
  // to do without an fopen()
  //err = SSL_CTX_use_certificate_file(sslContext, "crypto.pem", SSL_FILETYPE_PEM);
  X509 * xkey = NULL;
  errno=0;
  rewind(certificateF);
  if (errno)
    db_printf(1, "An error occured while doing a rewind in SSL certificate file\n");

  xkey = PEM_read_X509(certificateF, NULL,
    sslContext->default_passwd_callback,
    sslContext->default_passwd_callback_userdata);
  if (xkey == NULL)
  {
    setError();
    throw IOException("Unable to read certificate file");
  }

  err = SSL_CTX_use_certificate(sslContext, xkey);
  if (err != 1)
  {
    setError();
    char buf[5000];
    getErrorString(buf, sizeof(buf));
    throw IOException("Unable to read certificate file.  SSL error=%s\n", buf);
  }
  if (xkey != NULL) X509_free(xkey);


  //isLibraryInited = true;  // server
}
*/


const char* SslSocket::getErrorText(SSL* ssl, int err)
{
  assert(ssl != NULL);

  switch (SSL_get_error(ssl, err))
    {
    case SSL_ERROR_NONE:
      return "SSL_ERROR_NONE";
    case SSL_ERROR_WANT_WRITE:
      return "SSL_ERROR_WANT_WRITE"; 
    case SSL_ERROR_WANT_READ: 
      return "SSL_ERROR_WANT_READ"; 
    case SSL_ERROR_WANT_CONNECT: 
      return "SSL_ERROR_WANT_CONNECT"; 
    case SSL_ERROR_WANT_X509_LOOKUP: 
      return "SSL_ERROR_WANT_X509_LOOKUP"; 
    case SSL_ERROR_SYSCALL: 
      return "SSL_ERROR_SYSCALL";
    case SSL_ERROR_SSL: 
      return "SSL_ERROR_SSL"; 
    case SSL_ERROR_ZERO_RETURN: 
      return "SSL_ERROR_ZERO_RETURN"; 
    default: 
      return "unknown"; 
    } 
}


char* SslSocket::getErrorString(char* str, long len)
{
  unsigned long l;
  char buf[200];
  const char *file,*data;
  int line,flags;
  unsigned long es;
  
  str[0] = 0;

  es=CRYPTO_thread_id();
  while ((l=ERR_get_error_line_data(&file,&line,&data,&flags)) != 0)
    {
      snprintf(str, len, "%lu:%s:%s:%d:%s\n",es,ERR_error_string(l,buf),
	      file,line,(flags&ERR_TXT_STRING)?data:"");
    }

  return str;
}


int SslSocket::readWithTimeout(SSL* ssl, char* buf, int len, long timeoutMillis)
{
  // for testing
  /*
  if ((double)rand()/RAND_MAX < 0.1)
    {
      db_printf(1, "readWithTimeout: Frivolous renegotiation for testing.\n");
      SSL_renegotiate(ssl);
    }
  */

  // Determine expiry time and normalize.
  struct timeval expiryTime;
  gettimeofday(&expiryTime, NULL);
  expiryTime = expiryTime + timeoutMillis*1000;
  if (timeoutMillis == -1)
    expiryTime.tv_sec = expiryTime.tv_usec = 0;

  while (true)
    {
      if (!SSL_pending(ssl))
	{
	  // Can throw TimeoutExceededException or IOException
	  //	  printf("SslSocket.readWithTimeout (%ld): started waitOnFd\n", time(NULL));
	  waitOnFd(SSL_get_fd(ssl), POLLIN, expiryTime);
	  //	  printf("SslSocket.readWithTimeout (%ld): done waitOnFd\n", time(NULL));
	  /*
	  pollfd pollFd;
	  pollFd.events = POLLIN; 
	  pollFd.fd = SSL_get_fd(ssl); 
	  struct timeval now;
	  gettimeofday(&now, NULL);
	  long timeToBlock = (expiryTime - now)/1000;
	  int ret;
	  if (timeoutMillis < 0)
	    ret = poll(&pollFd, 1, -1);
	  else if (timeToBlock < 0)
	    ret = poll(&pollFd, 1, 0);
	  else
	    ret = poll(&pollFd, 1, timeToBlock);
	  //db_printf(0, "SslSocket.readWithTimeout: Poll for read unblocked, ret = %d, err=%s.\n", ret, strerror(errno)); 

	  if (ret == 0)
	    {
	      // Poll timeout exceeded without error.  If our timeToBlock
	      // was already <=0, throw.
	      // It's probably ok to throw any time ret == 0, but
	      // this code is safer.
	      if (timeToBlock <= 0 && timeoutMillis >= 0)
		throw TimeoutExceededException("Timeout exceeded in read");
	      continue;
	    }

	  if (ret < 0)
	    {
	      if (errno == EINTR)
		continue;
	      else
		{
		  throw IOException("Error on poll()");
		}
	    }
	  */
	}

      int ret = SSL_read(ssl, buf, len);

      if (ret > 0)
	return ret;

      if (ret == 0 && SSL_get_error(ssl, ret) == SSL_ERROR_ZERO_RETURN)
	// no data available from underlying layer.  However, socket
	// is readable (POLLIN), so must be eof.
	return 0;

      //      printf("readWithTimeout: ret=%d\n", ret);
      switch (SSL_get_error(ssl, ret))
	{
	case SSL_ERROR_NONE:
	  // I don't think this can happen.  ret was <= 0, but no
	  // error from SSL_get_error()?
	  assert(false);
	  continue;
	case SSL_ERROR_SYSCALL:
	  if (ret == 0)
	    return 0; // normal eof
	  else
	    {
	      //	      setError();
	      /*
	      char buf[1000];
	      strncpy(buf, strerror(errno), 1000);
	      buf[999] = 0;
	      db_printf(0, "SslSocket.readWithTimeout: Error in underlying system call: %s\n", strerror(errno));
	      */
              if (EBADFD == errno)
                throw IOException("Error in underlying system call: Bad filedescriptor");
              else
              {
                db_printf(0, "Underlying syscall error: %s %d\n", strerror(errno), errno);
	        throw IOException("Error in underlying system call during read");
              }
	    }
	case SSL_ERROR_WANT_READ:
	  {
//			printf("SSL_ERROR_WANT_READ: %i\n", SSL_pending(ssl));
	    // Can throw TimeoutExceededException or IOException
	    waitOnFd(SSL_get_fd(ssl), POLLIN, expiryTime);
	    /*
	    assert(SSL_is_init_finished(ssl) == 0);
	    
	    pollfd pollFd; 
	    pollFd.events = POLLIN; 
	    pollFd.fd = SSL_get_fd(ssl); 
	    struct timeval now;
	    gettimeofday(&now, NULL);
	    long timeToBlock = (expiryTime-now)/1000;
	    int ret;
	    if (timeoutMillis < 0)
	      ret = poll(&pollFd, 1, -1);
	    else if (timeToBlock < 0)
	      ret = poll(&pollFd, 1, 0);
	    else
	      ret = poll(&pollFd, 1, timeToBlock);
	    db_printf(0, "SslSocket.write: Poll for read unblocked, ret = %d.\n", ret); 

	    if (ret == 0)
	      {
		// if there's nothing to read and the timeout is exceeded,
		// throw.  Otherwise, try writing.
		if (timeToBlock <= 0 && timeoutMillis >= 0)
		  throw TimeoutExceededException("Timeout exceeded waiting for readable socket during readWithTimeout().");
		continue;
	      }

	    // If it's readable, try the read again.
	    if (ret > 0)
	      continue;
	    
	    if (ret < 0)
	      {
		if (errno == EINTR)
		  continue;
		else
		  {
		    setError();
		    throw IOException("Error on poll()");
		  }
	      }
	    */
	    // We read a renegotiation.  Try again to read data.
	    continue;
	  }
	case SSL_ERROR_WANT_WRITE:
	  {
	    //
	    // We're in a negotiation and the socket isn't currently 
	    // writable (perhaps backpressure).

//			printf("SSL_ERROR_WANT_WRITE: %i\n", SSL_pending(ssl));
	    // Can throw TimeoutExceededException or IOException
	    waitOnFd(SSL_get_fd(ssl), POLLOUT, expiryTime);
	    /*
	    pollfd pollFd; 
	    pollFd.events = POLLOUT; 
	    pollFd.fd = SSL_get_fd(ssl); 
	    struct timeval now;
	    gettimeofday(&now, NULL);
	    long timeToBlock = (expiryTime-now)/1000;
	    int ret;
	    if (timeoutMillis < 0)
	      ret = poll(&pollFd, 1, -1);
	    else if (timeToBlock < 0)
	      ret = poll(&pollFd, 1, 0);
	    else
	      ret = poll(&pollFd, 1, timeToBlock);
	    //db_printf(0, "SslSocket.read: Poll for write unblocked, ret = %d.\n", ret); 

	    if (ret == 0)
	      {
		// The socket was never writable and the timeout
		// expired.  Throw.  Otherwise, try writing.
		if (timeToBlock <= 0 && timeoutMillis >= 0)
		  throw TimeoutExceededException("Timeout exceeded waiting for writable socket during read()");
		continue;
	      }

	    if (ret < 0)
	      {
		if (errno == EINTR)
		  continue;
		else
		  {
		    //		    setError();
		    throw IOException("Error on poll()");
		  }
	      }
	    */
	    
	    // The socket is writable.  Try again.
	    continue;
	  }
	default:
	  //	  setError();
	  throw IOException("Unknown error: %s", getErrorText(ssl, ret));
	}
    }

  // This should not be reachable.
}


int SslSocket::readFullyWithTimeout(SSL* ssl, char* buf, int len, long timeoutMillis)
{
  timeval expiryTime;
  gettimeofday(&expiryTime, NULL);
  expiryTime = expiryTime + timeoutMillis*1000;
  if (timeoutMillis == -1)
    expiryTime.tv_sec = expiryTime.tv_usec = 0;

  long totalBytesRead = 0;
  while (totalBytesRead < len)
    {
      timeval now;
      gettimeofday(&now, NULL);
      long timeToBlock = (expiryTime-now)/1000;
      long bytesRead = readWithTimeout(ssl, buf+totalBytesRead, len-totalBytesRead, timeToBlock);
      if (bytesRead == 0)
	return totalBytesRead;

      totalBytesRead += bytesRead;
      if (totalBytesRead == len)
	return totalBytesRead;

      if (timeToBlock <= 0 && timeoutMillis >= 0)
	throw TimeoutExceededException("Read only %d of %d bytes", totalBytesRead, len);
    }

  return totalBytesRead;
}


void SslSocket::write(SSL* ssl, const char* buf, int len, long timeoutMillis)
{
  // for testing
  /*
  if ((double)rand()/RAND_MAX < 0.1)
    {
      db_printf(0, "readWithTimeout: Frivolous renegotiation for testing.\n");
      SSL_renegotiate(ssl);
    }
  */

  // Determine expiry time and normalize.
  struct timeval expiryTime;
  gettimeofday(&expiryTime, NULL);
  expiryTime = expiryTime + timeoutMillis*1000;
  if (timeoutMillis == -1)
    expiryTime.tv_sec = expiryTime.tv_usec = 0;

  long bytesWritten = 0;

  while (true)
    {
      int err = SSL_write(ssl, buf + bytesWritten, len - bytesWritten);

      if (err > 0)
	{
	  bytesWritten += err;
	  if (len == bytesWritten)
	    return;
	  else
	    // If we haven't written everything, try again.
	    continue;
	}

      switch (SSL_get_error(ssl, err))
	{
	case SSL_ERROR_WANT_WRITE:
	  {
	    // The socket isn't currently writable (perhaps backpressure).

	    // Can throw TimeoutExceededException or IOException
	    waitOnFd(SSL_get_fd(ssl), POLLOUT, expiryTime);
	    /*
	    pollfd pollFd; 
	    pollFd.events = POLLOUT; 
	    pollFd.fd = SSL_get_fd(ssl); 
	    struct timeval now;
	    gettimeofday(&now, NULL);
	    long timeToBlock = (expiryTime-now)/1000;
	    int ret;
	    if (timeoutMillis < 0)
	      ret = poll(&pollFd, 1, -1);
	    else if (timeToBlock < 0)
	      ret = poll(&pollFd, 1, 0);
	    else
	      ret = poll(&pollFd, 1, timeToBlock);
	    //db_printf(0, "SslSocket.write: Poll for write unblocked, ret = %d.\n", ret);

	    if (ret == 0)
	      {
		// The socket was never writable and the timeout
		// expired.  Throw.  Otherwise, try writing.
		if (timeToBlock <= 0 && timeoutMillis >= 0)
		  throw TimeoutExceededException("Timeout exceeded waiting for writable socket during write()");
		continue;
	      }

	    if (ret < 0)
	      {
		if (errno == EINTR)
		  continue;
		else
		  {
		    //		    setError();
		    throw IOException("Error on poll()");
		  }
	      }
	    */

	    // The socket is writable.  Try again.
	    continue;
	  }
	  
	case SSL_ERROR_WANT_READ:
	  {
	    //
	    // We're in the midst of an init (ie. renegotiation).
	    // We're waiting for the other side to send his part of
	    // the negotiation.  Sleep until we can read.

	    // Assert we're renegotiating as I don't know how else this can
	    // happen.
	    assert(SSL_is_init_finished(ssl) == 0);
	    
	    // Can throw TimeoutExceededException or IOException
	    waitOnFd(SSL_get_fd(ssl), POLLIN, expiryTime);
	    // The socket is readable -- try the write again
	    continue;

	    /*
	    pollfd pollFd; 
	    pollFd.events = POLLIN; 
	    pollFd.fd = SSL_get_fd(ssl); 
	    struct timeval now;
	    gettimeofday(&now, NULL);
	    long timeToBlock = (expiryTime-now)/1000;
	    int ret;
	    if (timeoutMillis < 0)
	      ret = poll(&pollFd, 1, -1);
	    else if (timeToBlock < 0)
	      ret = poll(&pollFd, 1, 0);
	    else
	      ret = poll(&pollFd, 1, timeToBlock);
	    //db_printf(0, "SslSocket.write: Poll for read unblocked, ret = %d.\n", ret); 

	    if (ret == 0)
	      {
		// Poll timeout exceeded without error.  If our timeToBlock
		// was already <=0, throw.
		// It's probably ok to throw any time ret == 0, but
		// this code is safer.
		if (timeToBlock <= 0 && timeoutMillis >= 0)
		  throw TimeoutExceededException("Timeout exceeded in read");
		continue;
	      }

	    // If it's readable, try the write again.
	    if (ret > 0)
	      continue;
	    
	    if (ret < 0)
	      {
		if (errno == EINTR)
		  continue;
		else
		  {
		    //		    setError();
		    throw IOException("Error on poll(): %s", strerror(errno));
		  }
	      }
	    */
	  }

	case SSL_ERROR_SYSCALL:
	  {
	    if (len == 0 && errno == 0)
	      // This is the case for write(len=0).  The underlying
	      // write() return 0 and SSL thinks it's an error, but errno
	      // is Success.
	      return;

	    // Attempted to write on a closed socket, perhaps.  Don't do that.
	    //	    setError();
	    /*
	    char buf[1000];
	    strncpy(buf, strerror(errno), 1000);
	    buf[999] = 0;
	    */

            if (EBADFD == errno)
              throw IOException("Error in underlying system call: Bad filedescriptor");
            else
            {
              db_printf(0, "Underlying syscall error: %s %d\n", strerror(errno), errno);
	      throw IOException("Error in underlying system call during write");
            }
	  }
	default:
	  // All other things are errors.
	  //	  setError();
	  char errorStr[500];
	  throw IOException("Unknown error: %s: %s", getErrorText(ssl, err), getErrorString(errorStr, 500));
	}
    }
}


// Should throw IOException if can't find file.
void SslSocket::setClientCAList(const char* caFile)
{
  // I believe this doesn't leak.
  STACK_OF(X509_NAME)* list = SSL_load_client_CA_file(caFile);
  if (list == NULL)
    throw IOException("Could not read certificate list from CA file '%s'", caFile);

  SSL_CTX_set_client_CA_list(sslContext, list);
}


// This is just for testing.  It isn't normally used.
int verifyCallback(int preverify_ok, X509_STORE_CTX* ctx)
{
  X509* peerCert = X509_STORE_CTX_get_current_cert(ctx);
  char buf[5000];
  X509_NAME_oneline(X509_get_issuer_name(peerCert), buf, 5000);
  buf[4999] = 0;
  DB("verify: issuer name=" << buf);
  X509_NAME_oneline(X509_get_subject_name(peerCert), buf, 5000);
  buf[4999] = 0;
  DB("verify: subject name=" << buf);
  return preverify_ok;
}


void SslSocket::setVerify(int mode, const char* caFile, const char* caPath)
{
  SSL_CTX_set_verify(sslContext, mode, NULL);//verifyCallback);
  SSL_CTX_load_verify_locations(sslContext, caFile, caPath);
}


void SslSocket::waitOnFd(int fd, int pollFdEvents, struct timeval expiryTime)
{
  //  printf("waitOnFd: fd=%d, events=%d, expiry=%ld,%ld, now=%ld,%ld\n", fd, pollFdEvents, expiryTime.tv_sec, expiryTime.tv_usec, now.tv_sec, now.tv_usec);

  while (true)
    {
      pollfd pollFd; 
      pollFd.events = pollFdEvents; 
      pollFd.fd = fd;
      struct timeval now;
      gettimeofday(&now, NULL);

      int ret;
      long timeToBlock = (expiryTime-now)/1000;
      if (expiryTime.tv_sec == 0 && expiryTime.tv_usec == 0)
	ret = poll(&pollFd, 1, -1);
      else
	{
	  if (timeToBlock < 0)
	    ret = poll(&pollFd, 1, 0);
	  else
	    {
	      //	      printf("SslSocket.waitOnFd (%ld): started poll, timeToBlock=%ld.\n", time(NULL), timeToBlock);
	      ret = poll(&pollFd, 1, timeToBlock);
	      //	      printf("SslSocket.waitOnFd (%ld): done poll.\n", time(NULL));
	    }
	  //db_printf(0, "SslSocket.write: Poll for read unblocked, ret = %d.\n", ret); 
	}
  
      if (ret == 0)
	{
	  // Poll timeout exceeded without error.  If our timeToBlock
	  // was already <=0, throw.
	  // It's probably ok to throw any time ret == 0, but
	  // this code is safer.
	  if (timeToBlock <= 0 && (expiryTime.tv_sec != 0 || expiryTime.tv_usec != 0))
	    {
	      //	      printf("waitOnFd: timeout exceeded.\n");
	      throw TimeoutExceededException("Timeout exceeded in read");
	    }
	  continue;
	}
  
      // We got the desired event.  Return.
      if (ret > 0)
	{
	  //	  printf("waitOnFd: got event.\n");
	  return;
	}
  
      if (ret < 0)
	{
	  if (errno == EINTR)
	    continue;
	  else
	    {
	      //	      printf("waitOnFd: io exception.\n");
	      throw IOException("Error on poll(): %s", strerror(errno));
	    }
	}
    }
}
