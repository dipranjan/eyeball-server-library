// ---------------------------------------------------------------------------
// File:       Ethernet.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Ethernet helper routines
//
// Change Log:
// ---------------------------------------------------------------------------

#include "ethernet.h"

#include <stdio.h>

#include "retval.h"


MACAddress::MACAddress()
:
	address()
{
	memset(address, 0, sizeof(address));
}

string MACAddress::to_str(void) const
{
	char szAddress[13];

	sprintf(
		szAddress,
		"%02x%02x%02x%02x%02x%02x",
		address[0], address[1], address[2], address[3], address[4], address[5]
	);

	return szAddress;
}

bool MACAddress::from_str(string& _str)
{
	RFUT(_str.size() == 12);

	unsigned int val;

	for (int i=0; i < 6; i++)
	{
		RFUT(sscanf(&_str.c_str()[i*2], "%02x", &val) == 1);
		address[i] = val;
	}

	return true;
}

