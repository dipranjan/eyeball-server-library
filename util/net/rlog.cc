#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>

#include <string>
using std::string;

#include "rlog.h"
#include "inet.h"

static int s_fd = -1;  // socket used for logging

static u_int32_t s_uAddr = 0;
static u_int16_t s_uPort = 0;

int rlog_open(u_int32_t uAddr, u_int16_t uPort)
{
  if (s_fd != -1)
    return -1;

  s_fd = socket(PF_INET, SOCK_DGRAM, 0);

  struct sockaddr_in sin;
  memset((char*)&sin, 0, sizeof(sin));
  sin.sin_family = PF_INET;
  sin.sin_port = 0;
  
  int val = bind(s_fd, (sockaddr*)&sin, sizeof(sin));
  if (val < 0)
  {
    printf("Bind error");
    return -1;
  }

  rlog_set_monitor(uAddr, uPort);
  
  return 0;  // OK
}

void rlog_close()
{
  if (-1 != s_fd)
  {
    close(s_fd);
    s_fd = -1;
  }
}

void rlog_set_monitor(u_int32_t uAddr, u_int16_t uPort)
{
	s_uAddr = uAddr;
	s_uPort = uPort;
}

int rlog(const string& sText)
{
  if ((0 == s_uAddr) || (0 == s_uPort))
    return 0;  // no log monitor

  sockaddr_in sin;
  memset((char*)&sin, 0, sizeof(sin));
  sin.sin_family = PF_INET;
  sin.sin_addr.s_addr = s_uAddr;
  sin.sin_port = s_uPort;

  int ret = sendto(s_fd, sText.data(), sText.size(), 0,
    (sockaddr*)&sin, sizeof(sin));

  if (ret < (int)sText.size())
  {
    printf("rlog error");
    return -1;
  }

  return 0;
}
