/**
 * Socket
 * Wrapper class around a socket.
 */
#ifndef _Socket_h_
#define _Socket_h_

#include "IOException.h"
#include "TimeoutExceededException.h"

#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>


class Socket
{
 public:
  Socket();


  /**
   * Same as connect(fd), but connects to a named remote host.
   *
   * @throws IOException if the host does not exist, or if the underlying
   * connect fails.
   * This SslSocket remains unopened and connect() can be called again.
   */
  virtual void connect(const char * host, int port);


  /**
   * The semantics are intended to be similar to read(2) with a
   * non-blocking socket.  read() will block until data is read
   * or an error occurs.
   *
   * @return On success, the number of bytes read is returned, 0 on
   * eof.
   * @throws IOException if there is an error while reading.
   */
  virtual int read(char* buf, int len);


  /**
   * Semantics are the same as read(), but readWithTimeout() will
   * block only for timeoutMillis milliseconds.
   *
   * @param timeoutMillis Milliseconds to wait for data.  -1 for
   * forever (in which ase TimeoutExceededException will never be thrown).
   * @return On success, the number of bytes read is returned, 0 on
   * eof.
   * @throws IOException if there is an error while reading.
   * @throws TimeoutExceededException if the timeout period expires
   * before any data is read.
   */
  virtual int readWithTimeout(char* buf, int len, long timeoutMillis);


  /**
   * Same as readWithTimeout(), except it will not return until len
   * bytes or EOF are read.  If fewer than len bytes are read when
   * an EOF is reached, the number of bytes read is returned.  If fewer
   * than len bytes are read before the timeout, the number of bytes
   * read is returned.
   *
   * This function is erroneously implemented to throw a
   * TimeoutExceededException if 0 < #bytes < len bytes are read.
   * It should return #bytes in this case.  Fix it.
   */
  virtual int readFullyWithTimeout(char* buf, int len, long timeoutMillis);


  /**
   * Semantics are the same as readWithTimeout().  buf will be
   * \0-terminated within len bytes (buf will contain at most
   * len-1 characters followed by a \0).  The string is never
   * over 255 bytes.  It's good to avoid writing strings of length
   * 0 as the return value of 0 from this function is ambiguous with
   * an EOF.
   *
   * @return The number of bytes stored in buf (not counting
   * the \0) is returned.  If there are more than len bytes in
   * the string, they are silently read and discarded.
   */
  int readPascalStringWithTimeout(char* buf, long len, long timeoutMillis);


  /**
   * Writes buf on the socket.  If the data cannot be written to the
   * socket within timeoutMillis, write fails.  You should not expect
   * a write to succeed immediately since a renegotion takes time and
   * is normal.  Can change this SslSocket from being readable to
   * unreadable (due to the completion of a negotiation).  All the
   * bytes are written unless an exception is thrown.
   *
   * @throws IOException if there is an error writing to the socket.
   * @throws TimeoutExceededException if timeoutMillis elapses before
   * the write can complete.
   */
  virtual void write(const char* buf, int len, long timeoutMillis=10000);


  /**
   * Same semantics as write().
   *
   * @param buf A string to write.  Must be \0-terminated and less
   * than 255 characters.
   */
  void writePascalString(char* buf, long timeoutMillis=10000);


  /**
   * Checks if a socket is readable.
   *
   * @return Usually, returns false if a read() would not return data
   * and true if a read() would.  However, a false return can happen
   * when the socket is readable (a race between returning from the
   * function and data arriving on the socket; the next isReadable()
   * would return true) and a true return can
   * happen when the read() would block or return no data (if the
   * outstanding data is part of a socket renegotiation; once read()
   * is called, isReadable will return false).
   */
  virtual bool isReadable();


  /**
   * Returns true if the socket is open.
   */
  virtual bool isOpen() const;


  /**
   * Returns true iff the socket has thrown an IOException.  If so,
   * the socket cannot be used, other than getFd(), getSSL(), and
   * close().
   */
  virtual bool isError() const {return isInError;}


  /**
   * Returns the file descriptor of the underlying socket.  Use this
   * only for select(), etc., not for read(2) or write(2).
   */
  virtual int getFd() const;


  /**
   * Closes the socket.  Must be called even if the socket has
   * an error.  After this call, no other functions can be called.
   */
  virtual void close();


  /**
   * These functions are all identical to their counterparts that
   * do not take the socket parameter, but can be used by code that
   * doesn't want to use a Socket instance.
   *
   * @param socket must be a connected socket.
   */
  static int readWithTimeout(int socket, char* buf, int len, long timeoutMillis);
  static int readFullyWithTimeout(int socket, char* buf, int len, long timeoutMillis);
  static void write(int socket, const char* buf, int len, long timeoutMillis=10000);


 protected:
  /// The socket
  int socket;

  /**
   * Set when the socket throws an IOException.  The socket can
   * no longer be used (it will throw another IOException).
   */
  bool isInError;


  /**
   * Sets the error state.
   */
  void setError() {isInError = true;}


  /// Can't copy-construct it.
  Socket(Socket& soc) {}
};


long operator-(const timeval& t1, const timeval& t2);
timeval operator+(timeval t1, long timeDiffMicros);


#endif

