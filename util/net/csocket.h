/***************************************************************************
                          csocket.h  -  description
                             -------------------
    begin                : Sun Oct 10 2004
    copyright            : (C) 2004 by root
    email                : root@zunnun
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CSOCKET_H
#define CSOCKET_H


/**
  *@author root
  */

#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>

  
#define   SERV_UDP_PORT 5060
#ifndef MAXMSGSIZE
#define MAXMSGSIZE  4000
#endif
#define   UDPSERVER  0
#define   UDPCLIENT  1

class CSocket
{
public: 
	CSocket();
	~CSocket();
  int Create(int type ,char* addr=NULL,unsigned int uiPort = SERV_UDP_PORT);
  int ReceiveFrom(char *buffer);
  int ReceiveFrom(char *buffer, char * clientIP ,unsigned int *uiPort);
  int SendTo(char *buffer, int size);
  int SendTo(char* addr, unsigned int iPort,char *buffer, int size);
  void NonBlock();
  int Getfd();
  
  private:
  int m_socketfd;
  struct sockaddr_in m_Serv_Addr, m_Cli_Addr;
  
};

#endif
