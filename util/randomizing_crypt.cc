// randomizing_crypt.cc
//
// Intended for randomizing encryption that is hard to guess
// Security by obscurity
// Not intended for highly secure applications, but rather
//   to waste a little more time of potential hackers
// Can be reverse engineered
// No key required
// Algorithm must be kept secret
//
// Zion Kwok
// Eyeball Networks
//
// May 4, 2006
//////////////////////////////////////////////////////////////////

#include "randomizing_crypt.h"
#include <fcntl.h>
#include <time.h>
#include <stdlib.h>

static int srand_init = 0;

static string randomizing_encrypt_small(const char* pcInput, int iLen);
static string randomizing_decrypt_small(const char* pcInput, int& iLen);

static string generateRandomString(int iLen);

string randomizing_encrypt(const string& sInput)
{
	if (sInput.size() == 0) return "";

	const char* pcInput = sInput.data();
	int iLen = sInput.size();

	string sOutput;

	if (!srand_init) {
	  //		srand(rand() ^ (rand() >> 3) ^ (int)pcInput ^ time(NULL));
		srand(rand() ^ (rand() >> 3) ^ time(NULL));
		srand_init = 1;
	}

	//Break up string
	int iOffset = 0;
	//Need number between 0 and 255;
	while ((iLen - iOffset) > 255) {
		int iCurrSize = rand() & 0xFF;
		if (iCurrSize > 0) {
			sOutput += randomizing_encrypt_small(
					&(pcInput[iOffset]),
					iCurrSize);
			iOffset += iCurrSize;
		}
	}
	if (iLen > iOffset) {
		sOutput += randomizing_encrypt_small(
				&(pcInput[iOffset]),
				iLen - iOffset);
	}
	return sOutput;
}

string randomizing_decrypt(const string& sInput)
{
	const char* pcInput = sInput.data();
	int iLen = sInput.size();

	string sOutput;
	int iOffset = 0;
	while (iLen > iOffset) {
		int iTempLen = iLen - iOffset;
		sOutput += randomizing_decrypt_small(
				&(pcInput[iOffset]),
				iTempLen);

		if (iTempLen == (iLen - iOffset)) {
			if (!srand_init) {
			  //				srand(rand() ^ (rand() >> 3) ^ (int)(sInput.data()) ^ time(NULL));
  				srand(rand() ^ (rand() >> 3) ^ time(NULL));
				srand_init = 1;
			}
			int iNewLen = iLen - 5 - (rand() & 3);
			return generateRandomString(iNewLen);
		}

		iOffset = (iLen - iTempLen);	
	}

	return sOutput;	
}


string randomizing_encrypt_small(const char* pcInput, int iLen)
{
	int rand1 = (rand() >> 4) & 0xFFFFFF;

	//4 bytes;
	string sOutput;
	sOutput += char(rand1 >> 16);
	sOutput += char(iLen ^ (rand1 >> 3) ^ (rand1 << 3));
	sOutput += char(rand1);
	sOutput += char(rand1 >>  8);
	
	int checksum = 0x37;
	for (int i = 0; i < iLen; i++) {
		checksum ^= (pcInput[i]) | i;
		char ch = (pcInput[i] ^ (rand1 >> ((i + iLen) & 0xF)));
		sOutput += ch;
		rand1 ^= (rand1 << (ch & 0xF)) ^ pcInput[i];
		if (((rand1 + ch) & 0x4010) == 0x4010) {
			sOutput += char(rand());
		}
	}		
	sOutput += (char)(checksum ^ rand1);

	return sOutput;	
}

string randomizing_decrypt_small(const char* pcInput, int& iLen)
{
	if (iLen < 4) {
		return "";
	}

	string sOutput;
	int rand1 = ((pcInput[0] & 0xFF) << 16) |
	            ((pcInput[2] & 0xFF)) |
	            ((pcInput[3] & 0xFF) << 8);

	int iOutputLen = (pcInput[1] ^ (rand1 >> 3) ^ (rand1 << 3)) & 0xFF;

	int i = 0;
	int x = 4;	
	int checksum = 0x37;
	for (; (i < iOutputLen) && (x < iLen); i++, x++) {
		char ch = pcInput[x];
		char outputch = (ch ^ (rand1 >> ((i + iOutputLen) & 0xF)));
		sOutput += outputch;
		checksum ^= outputch | i;
		rand1 ^= (rand1 << (ch & 0xF)) ^ outputch;
		if (((rand1 + ch) & 0x4010) == 0x4010) {
			x++;
		}
	}		

	if ((int)sOutput.size() != iOutputLen) {
		return "";
	}

	if ((x + 1) > iLen) {
		return "";
	}

	if (pcInput[x] != (char)(checksum ^ rand1)) {
		return "";
	}

	iLen -= (x + 1);
	return sOutput;
}

string generateRandomString(int iLen)
{
	if (iLen <= 0) {
		return 0;
	}

	string s;
	s.resize(iLen);
	char* data = (char*)s.data();
	for (int i = 0; i < iLen; i++) {
		data[i] = (char)((rand() >> 7) ^ i);
	}	
	return s;
}

