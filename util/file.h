/** All files of the DNDS Prototype are Copyright (C) 1999 by Andre Schoorl
 *  unless otherwise noted.
 *
 *  The DNDS Prototype is distributed in the public domain in the hope that it
 *  will be useful, but is provided `as is' and is subject to change without
 *  notice.  No warranty of any kind is made with regard to the software or
 *  documentation.  The author shall not be liable for for any error for
 *  incidental or consequential damages in connection with the software and
 *  the documentation.
 *
 *  Permission to copy the whole, unmodified DNDS package is granted provided
 *  that the copies are not made or distributed for resale (excepting nominal
 *  copying fees).  All redistributions in either source or binary form must
 *  include this disclaimer.
 */

#ifndef AS_FILE_H
#define AS_FILE_H

#include <dirent.h>
#include <stdio.h>

#include <string>
#include <vector>

class File
{
public:
	File(const std::string& _filename);
	~File();

	static bool is_absolute(const std::string& _filename);
	static bool is_regular(const std::string& _filename);
	static bool is_dir(const std::string& _filename);
	static bool remove(const std::string& _filename);
	static bool rename(const std::string& _source, const std::string& _dest);
	static int num_bytes(const std::string& _filename);
	static int num_lines(const std::string& _filename);
	static std::string get_temporary_name(const std::string& _prefix);

	// Set available fds to maximum
	static long set_max_fds(void);
	static long get_max_files(long lTryAtMost = 64000);

	static std::vector<std::string> read_tokens_from_file(const std::string& _filename);

	FILE *get_fp(void) { return fp; }

	bool open_append(void);
	bool open_read(void);
	bool open_write(void);

	bool open_pipe_read(void);
	bool open_pipe_write(void);

	bool append_bytes(char *_val, int _len);
	bool read_bytes(char*& _val, int& _len);
	bool read_line(std::string& _val);
	bool seek(long _offset);

private:
	std::string filename;
	FILE *fp;
	bool is_pipe;
};

class Directory
{
public:
	static DIR *read(const std::string& _filename);
	static int num_entries(const std::string& _filename);
};

#endif // AS_FILE_H

