//Encrypt and decrypt password

#include <stdio.h>
#include "BinHex.h"
#include <openssl/des.h>

int main(int argc, char** argv)
{
  DES_cblock block;
  DES_key_schedule ks;

  for (int i = 0; i < 10; i++) {
    DES_random_key(&block);
    int iRet = DES_set_key_checked(&block, &ks);
    if (iRet == 0) break;
  }

  //Check keys
  unsigned char hexKey[17];
  hexKey[16] = '\0';
  convertBinToHex((unsigned char*)block, 8, hexKey);
  
  printf("%s\n", hexKey);

  return 0;
}

