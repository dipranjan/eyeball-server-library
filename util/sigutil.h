// ---------------------------------------------------------------------------
// File:       signal.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:
//
// Change Log:
// ---------------------------------------------------------------------------

#ifndef SIGNAL_H
#define SIGNAL_H

extern int g_fildes[2];

class SignalHandler
{
        private:
                static void signal_handler(int sig);

        public:
                SignalHandler();
                ~SignalHandler();

                int get_pipe_output(void) { return g_fildes[0]; }

                static void ignore_all(void);
};

#endif // SIGNAL_H

