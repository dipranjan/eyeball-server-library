// ---------------------------------------------------------------------------
// File:       signal.cc
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Warnings:   CANNOT USE DB STATEMENTS WITHIN THIS FILE.  This is because
//             DB() prints the time using strftime() which locks a mutex
//             under Solaris 8!  Because the signal handler can be called
//             at any point within the main thread, this can result in a
//             deadlock if the main thread is also doing a DB().
//
// Purpose:
// 
// Change Log:
// ---------------------------------------------------------------------------

#include <signal.h>
#include <unistd.h>

#include "sigutil.h"

#include "debug.h"			// For ASSERT() only!
#include "log.h"
#include "thread.h"

// ---------------------------------------------------------------------------
// Exported global variables
// ---------------------------------------------------------------------------

int g_fildes[2];


// ---------------------------------------------------------------------------
// Class implementation
// ---------------------------------------------------------------------------

SignalHandler::SignalHandler()
{
        ASSERT(pipe(g_fildes) == 0);

        struct sigaction act;
        act.sa_handler = signal_handler;
        ASSERT(sigemptyset(&act.sa_mask) == 0);
        act.sa_flags = 0;

        ASSERT(sigaction(SIGINT,  &act, NULL) == 0);
        ASSERT(sigaction(SIGTERM, &act, NULL) == 0);
        ASSERT(sigaction(SIGQUIT, &act, NULL) == 0);
        ASSERT(sigaction(SIGALRM, &act, NULL) == 0);
        ASSERT(sigaction(SIGXFSZ, &act, NULL) == 0);

        ASSERT(sigaction(SIGHUP,  &act, NULL) == 0);

        // Ignored signals
        ASSERT(signal(SIGPIPE, SIG_IGN) != SIG_ERR);
        ASSERT(signal(SIGCHLD, SIG_IGN) != SIG_ERR);
}

SignalHandler::~SignalHandler()
{
        // Output may already have been closed by caller
        close(g_fildes[0]);
        close(g_fildes[1]);
}

void SignalHandler::ignore_all(void)
{
        sigset_t set;
        ASSERT(sigfillset(&set) == 0);

        ASSERT(pthread_sigmask(SIG_BLOCK, &set, NULL) == 0);
}

void SignalHandler::signal_handler(int sig)
{
#define CASE_NOTIFY_PIPE(X) \
        case X: \
                cerr << "Caught " << #X << ", sending notice\n"; \
                write(g_fildes[1], &sig, sizeof(sig)); \
                break

        switch(sig)
        {
                // Notify server, typically a reason to quit
                CASE_NOTIFY_PIPE(SIGINT);
                CASE_NOTIFY_PIPE(SIGTERM);
                CASE_NOTIFY_PIPE(SIGQUIT);
                CASE_NOTIFY_PIPE(SIGALRM);
                CASE_NOTIFY_PIPE(SIGXFSZ);

                // Server handles this specially
                CASE_NOTIFY_PIPE(SIGHUP);

                default:
                        cerr << "caught unknown signal " << sig;
                        break;
        }
}

