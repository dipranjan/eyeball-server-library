//! PeriodicTask.cc
//! Implementation for CPeriodicTask class

#include "PeriodicTask.h"
#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/time.h>

#include "debug.h"
#include "log.h"

//! Constructor
//! intervalMs is the interval in milliseconds
CPeriodicTask::CPeriodicTask(int intervalMs, bool bStartThread)
: m_iIntervalMs(intervalMs),
  m_iReadFd(-1),
  m_iWriteFd(-1),
  m_tLoopThread((pthread_t)-1)
{
	DB_ENTER(CPeriodicTask::CPeriodicTask);

	pthread_mutex_init(&m_StopMutex, NULL);

	if (bStartThread) {
		Start();
	}
}

//! Destructor
CPeriodicTask::~CPeriodicTask()
{
	DB_ENTER(CPeriodicTask::~CPeriodicTask);

	Stop();

	pthread_mutex_destroy(&m_StopMutex);
}

//! Start thread
void CPeriodicTask::Start()
{
	DB_ENTER(CPeriodicTask::Start);

	if (m_tLoopThread != (pthread_t)-1) return;

	if (m_iIntervalMs > 0) {
		int iSocketPairFd[2];

                int result = socketpair(AF_UNIX, SOCK_DGRAM, 0, iSocketPairFd);

		if (result == 0) {
			m_iReadFd = iSocketPairFd[0];
			m_iWriteFd = iSocketPairFd[1];

			pthread_create(&m_tLoopThread, NULL, LoopThread, this);
		}
		else {
                        DB_AND_LOG("Error: Cannot create socket pair.");
                        DB_AND_LOG("Period Task not started.");
                }
	}
	else {
                DB_AND_LOG("Error: Interval " << m_iIntervalMs << " is invalid.");
                DB_AND_LOG("Period Task not started.");
	}
}


//! Do Task
//! Override this
void CPeriodicTask::DoTask()
{
	DB_ENTER(CPeriodicTask::DoTask);
}

//! Stop
//! Only one thread should call this
void CPeriodicTask::Stop()
{
	DB_ENTER(CPeriodicTask::Stop);

	pthread_mutex_lock(&m_StopMutex);

	if (m_iIntervalMs > 0) {
		if (m_iWriteFd != -1) {
			if (m_tLoopThread != (pthread_t)-1) {
				//Stop Loop Thread
				char dummy = 'a';
				::send(m_iWriteFd, &dummy, 1, 0);
			
				void* ptr;	
				pthread_join(m_tLoopThread, &ptr);
				m_tLoopThread = (pthread_t)-1;
			}
		}

		if (m_iReadFd != -1) {
			::close(m_iReadFd);
			m_iReadFd = -1;
		}
		if (m_iWriteFd != -1) {
			::close(m_iWriteFd);
			m_iWriteFd = -1;
		}
	}

	pthread_mutex_unlock(&m_StopMutex);
}

//! Loop Thread
void* CPeriodicTask::LoopThread(void* arg)
{
	DB_ENTER(CPeriodicTask::LoopThread);

	CPeriodicTask* pThis = (CPeriodicTask*)arg;

	struct pollfd myPollFd;
	myPollFd.fd = pThis->m_iReadFd;
	myPollFd.events = POLLIN;	

	struct timeval tv;
	gettimeofday(&tv, NULL);

	//Infinite Loop
	for (;;) {
		struct timeval tv2;
		gettimeofday(&tv2, NULL);

		int iWaitMs = pThis->m_iIntervalMs - 
                              ((tv2.tv_sec - tv.tv_sec) * 1000) -
                              ((tv2.tv_usec - tv.tv_usec) / 1000);

		if (iWaitMs >= pThis->m_iIntervalMs) {
			iWaitMs = pThis->m_iIntervalMs;
		}
		else if (iWaitMs < 0) {
			iWaitMs = 0;
		}

		int ret = ::poll(&myPollFd, 1, iWaitMs);

		if (ret == 0) {
			gettimeofday(&tv, NULL);
			pThis->DoTask();
		}
                else if (ret > 0) {
			DB_AND_LOG("Poll returned " << ret << ". Leaving CPeriodicTask.");
			break;
		}
		else {
			DB_AND_LOG("Poll error " << ret << " in CPeriodicTask.");

			gettimeofday(&tv2, NULL);

			int iWaitMs = pThis->m_iIntervalMs - 
                	              ((tv2.tv_sec - tv.tv_sec) * 1000) -
                        	      ((tv2.tv_usec - tv.tv_usec) / 1000);

			if (iWaitMs >= pThis->m_iIntervalMs) {
				iWaitMs = pThis->m_iIntervalMs;
			}
			else if (iWaitMs < 0) {
				iWaitMs = 0;
			}

			ret = ::poll(&myPollFd, 1, iWaitMs);

			if (ret == -1) {
				int iForcedSleep = iWaitMs;
				if (iForcedSleep > 1000) {
                                  iForcedSleep = 1000;
                                }
				::poll(NULL, 0, iForcedSleep);
			}
		}
	}

	return NULL;
}

