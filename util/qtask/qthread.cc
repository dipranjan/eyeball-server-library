#include <string.h>
#include "qthread.h"
#include <fcntl.h>

QThread::QThread( qtaskBase* pTask )
{
	memset( &m_stArg, 0x00, sizeof( m_stArg ));
	memset( &m_stSock, 0x00, sizeof( m_stSock ));
	m_pTask = pTask;
}

QThread::~QThread()
{
	// write stop to the queue
	close( m_stSock.tsd );
	pthread_join( m_tid, NULL );

	if( m_pTask )
		delete m_pTask;
}

// signals the task (thread) to stop 
bool QThread::stopThread()
{
	// write stop to the queue
	close( m_stSock.tsd );
	pthread_join( m_tid, NULL );
	return true;
}

// send a message to the task (thread) via the socket queue
int QThread::submit( const char* pBuf, int nLen )
{
	int nSent=0;
	if(( nSent = send( m_stSock.tsd, pBuf, nLen, 0 )) < 0 ) {
		printf( "send() : %s\n", strerror(errno) );
		return 0;
	}
	return nSent;
}

int QThread::init( int nPort, char* szInterface, struct timeval tvTimeout )
{
  int on=1, r=0, buflen;

	// we will make a hostent for the thread to use to connect to us
	if(( sHost = (struct hostent*)gethostbyname( szInterface )) == NULL ){
		printf( "QThread::init() - Invalid host : %s\n", szInterface );
		return 0;
	}
	
	m_stArg.sAddr.sin_family = AF_INET;
	m_stArg.sAddr.sin_addr.s_addr = ((struct in_addr*)(sHost->h_addr))->s_addr;
	m_stArg.sAddr.sin_port = htons(nPort);
	m_stArg.tvTimeout = tvTimeout;
	m_stArg.pTaskClass = m_pTask;

	// setup listen socket for thread to call back on
    m_stSock.saddr.sin_family = AF_INET;
    m_stSock.saddr.sin_port = htons( nPort );
    m_stSock.saddr.sin_addr.s_addr = htonl( INADDR_ANY );

	if(( m_stSock.lsd = socket( AF_INET, SOCK_STREAM, 0 )) < 0 ) {
		printf( "setsockopt(): %s\n", strerror(errno) );
		return 0;
	}

    if( setsockopt( m_stSock.lsd, SOL_SOCKET,SO_REUSEADDR, (char*)&on, sizeof(on)) < 0 ) {
		printf( "setsockopt() : %s\n", strerror(errno) );
		return 0;
    }

	buflen = m_pTask->getReadSize();                                              
	socklen_t rr = sizeof( buflen );                                              
	if( setsockopt( m_stSock.lsd, SOL_SOCKET, SO_SNDBUF, (char*)&buflen, rr ) < 0 ) {
		printf( "setsockopt() : %s\n", strerror(errno) );                         
		return 0;                                                                 
	}
	m_stArg.nRcvBufSiz = buflen;
	
	// create our listening socket
	if( bind( m_stSock.lsd, (struct sockaddr*)&m_stSock.saddr, sizeof(m_stSock.saddr)) < 0 ) {
		printf( "bind() : %s\n", strerror(errno) );
		return 0;
	}

	if( listen( m_stSock.lsd, 1 ) < 0 ) {
		printf( "listen() : %s\n", strerror(errno) );
		return 0;
	}

	// pop the thread
	if(( r = pthread_create( &m_tid, NULL, qthreadMain, &m_stArg )) > 0 ){
		printf( "**** pthread_create() failed\n" );
		return 0;
	}
   
	// wait for thread to connect
    unsigned len = sizeof( m_stSock.saddr );
    if(( m_stSock.tsd = accept( m_stSock.lsd, (struct sockaddr*)&m_stSock.saddr, &len )) < 0 ) {
        printf( "accept() : %s\n", strerror(errno) );
        return 0;
    }

	// set non-blocking to avoid main process blocking
	if( int n = fcntl( m_stSock.tsd, F_GETFL, 0 ))
		fcntl( m_stSock.tsd, F_SETFL, n|O_NONBLOCK );
	else
        printf( "fcntl() F_GETFL : %s\n", strerror(errno) );

	// stop listening
	close( m_stSock.lsd );	

	// return opened descriptor
	return m_stSock.tsd;
}

