#ifndef _QTHREAD_H_
#define _QTHREAD_H_

#include <pthread.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    void* pTaskClass;           // whatever task you want in the thread
    struct sockaddr_in sAddr;   // the connect back info for the thread
    int nRcvBufSiz;             // the SO_RECVBUF size
    struct timeval tvTimeout;   // the timeout interval for select(),
                                // processTimeout() of your task will execute
} ARG;

// the main thread proc for qthread
//
void *qthreadMain( void* arg );


// the base class for tasks
// derive your task from qtaskBase
// -- overridables --
//   processMessage()	- receives data from QThread::submit()
//   processTimeout()	- called on timeout
//   respond()		- writes back to owner of QThread
//   getReadSize()	- sets the socket buffer and read sizes
// -- no need to override --
//   setRespondFd()
//
class qtaskBase
{
    public:
        qtaskBase(){m_respondFd=0;};
        virtual ~qtaskBase() {};
        virtual bool processMessage( const char* pBuf, int nLen ){return true;};
        virtual bool processTimeout(){return false;};
        virtual inline void setRespondFd( int nFd ){m_respondFd=nFd;};
        virtual int respond( const char* pBuf, int* nLen ){return 0;};
		virtual int getReadSize() { return 0; };
        int m_respondFd;
};


// the main class
// usage: construct with pointer to a task class
//		x = QThread( new qtaskMyTask );
// call init(), add the returned sd to your readset
// submit() data, data arrives in your task's processMessage()
//
class QThread
{
	public:
		QThread( qtaskBase* pTask );
		virtual ~QThread();
		virtual bool stopThread();
		int submit( const char* pBuf, int nLen );
		
		// nPort:		listen port, thread will call back on this port
		// szInterface:	listen on interface
		// tvTimeout:	timeout for select(), invokes processTimeout()
		int init( int nPort, char* szInterface, struct timeval tvTimeout );

	private:
		struct hostent* sHost;
		pthread_t m_tid;
		qtaskBase* m_pTask;

		struct {
			struct sockaddr_in saddr;
			int port;
			int lsd;	// listen sd
			int tsd;	// thread sd
		} m_stSock;
		
		ARG m_stArg;
};

#endif

