#include <fcntl.h>
#include <netdb.h>
#include <string.h>
#include <stdio.h>
#include "qthread.h"

void *qthreadMain( void* arg )
{
  ARG* pArg = (ARG*)arg;
  int sd, rcvd, nBufSiz=pArg->nRcvBufSiz;
  struct timeval tvTimeout, tv;
  char* pBuf;
  fd_set readset;

	// attach the task
	qtaskBase* pTask = (qtaskBase*)pArg->pTaskClass;
	tvTimeout = tv = pArg->tvTimeout;

	if(( sd = socket( AF_INET, SOCK_STREAM, 0 )) < 0 ) {
		printf( "-- socket() return - %s\n", strerror(errno));
		return 0;
	}

    // create a recv buffer same size as socket buffer
	if(( pBuf = (char*)malloc( pArg->nRcvBufSiz )) == NULL ) {
		printf( "-- malloc() error\n" );
		return 0 ;
	}

	if( connect( sd, (struct sockaddr*)&pArg->sAddr, sizeof(pArg->sAddr)) < 0 ) {
		printf( "-- connect() error - %s\n", strerror(errno));
		free( pBuf );
		return 0;
    }

	// tell the task where to respond to
	pTask->setRespondFd( sd );

	for(;;) {
		FD_ZERO( &readset );
		FD_SET( sd, &readset );
		
		// attempt to keep timeouts on roughly even intervals
		// todo: use time spent in processMessage() in timeout calc
		if(( tv.tv_sec == 0 )&&( tv.tv_usec == 0 ))
			tv = tvTimeout;

		if(( rcvd = select( sd+1, &readset, NULL, NULL, &tv )) < 0 ) {
			printf( "select() : %s\n", strerror(errno));

			if( errno == EINTR ) continue;
			else break;
		}

        if( FD_ISSET( sd, &readset ) && sd > 0 ) { // data from source
			if(( rcvd = recv( sd, pBuf, nBufSiz, 0 )) < 0 ) {
				printf( "-- recv() error -%s\n", strerror(errno));
				break;
			}
			
			if( rcvd == 0 ) { // indicates owner wishes the thread to stop
				printf( "-- disconnection recvd stopping thread\n" );
				break;
			}

        	// send data to task
			pTask->processMessage( pBuf, rcvd );
			continue;
		}

		pTask->processTimeout();
	}

	printf( "-- thread exiting\n" );
	close(sd);
	free(pBuf);
	return (void*)NULL;
}

