
#include "tstring.h"
#include "debug.h"



using namespace std;


tstring from2bs(const string& in)
{
	if( in.empty() )
		return "";

	tstring out;
	int iSize = in.size();
	out.reserve(iSize/2);
	for( int i = 0; i < iSize/2; i++ )
	{
#ifdef UNICODE
#ifdef USE_WCHAR
		unsigned char cT1 = (in[2 * i + 1]);
		unsigned char cT2 = in[2 * i];
		wchar_t value = cT1 * 256 + cT2;
#else
		unsigned short value = (unsigned char)(in[2 * i + 1]) << 8 | (unsigned char)in[2 * i];
#endif
#else
		char value = in[2 * i];
#endif
		out.push_back(value);
	}
	return out;
}

tstring::tstring() { }

tstring::tstring(const string& s)
{
#ifdef UNICODE
	int iSize = s.size();
	reserve(iSize);
	for( int i = 0; i < iSize; i++ )
#ifdef USE_WCHAR
		push_back((wchar_t)s[i]);
#else
		push_back((unsigned short)s[i]);
#endif
#else
	assign(s.c_str());
#endif
}

tstring::tstring(const my_wstring& s)
{
#ifdef UNICODE
	assign(s.data());
#else
	int iSize = s.size();
	reserve(iSize);
	for( int i = 0; i < iSize; i++ )
		push_back((char)s[i]);
#endif
}

tstring::tstring(const char* p)
{
#ifdef UNICODE
	if( p == NULL || *p == 0 )
	{
		resize(0);
		return;
	}
	do
	{
#ifdef USE_WCHAR
		wchar_t c = *p;
#else
		unsigned short c = *p;
#endif
		push_back(c);
	} while( *(++p) != 0 );
#else
	assign(p);
#endif
}

tstring::tstring(const unsigned short* p)
{
	if( p == NULL || *p == 0 )
	{
		resize(0);
		return;
	}
	do
	{
#ifdef UNICODE
#ifdef USE_WCHAR
		wchar_t c = *p;
#else
		unsigned short c = *p;
#endif
#else
		char c = *p;
#endif
		push_back(c);
	} while( *(++p) != 0 );
}

tstring::tstring(const wchar_t* p)
{
	if( p == NULL || *p == 0 )
	{
		resize(0);
		return;
	}
	do
	{
#ifdef UNICODE
#ifdef USE_WCHAR
		wchar_t c = *p;
#else
		unsigned short c = *p;
#endif
#else
		char c = *p;
#endif
		push_back(c);
	} while( *(++p) != 0 );
}

string tstring::mbs(void) const
{
	if( empty() )
		return "";
	string s;
#ifdef UNICODE
	int iSize = size();
	s.reserve(iSize);
	for( int i = 0; i < iSize; i++ )
		s.push_back(char(at(i)));
#else
	s.assign(c_str());
#endif
	return s;
}

string tstring::to2bs(void) const
{
	if( empty() )
		return "";
	string s;
	int iSize = size();
	s.reserve(2*iSize);
	for( int i = 0; i < iSize; i++ )
	{
		s.push_back(at(i) & 0x00ff);
#ifdef UNICODE
#ifdef USE_WCHAR
		s.push_back((at(i) & 0xff00) >> 8);
#else
		s.push_back((at(i) & 0xff00) >> 8);
#endif
#else
		s.push_back(0);
#endif
	}
	return s;
}

ostream& operator<<(ostream& s, const tstring& v)
{
#ifdef UNICODE
	int iSize = v.size();
	for( int i = 0; i < iSize; i++ )
		s << (char) v.at(i);
#else
	s << v.c_str();
#endif
	return s;
}
