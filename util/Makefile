#

.SUFFIXES: .cc $(.SUFFIXES)

ALL = libutil.a ebpasswd ebdecrypt pass3des pass3desf gendeskey gen3deskey ebhash

SUBDIRS=DnsLookup resipAuth

all: subdirs depend $(ALL)

.cc.o:
	$(C++) -o $@ -c $(PROF) $(CFLAGS) $*.cc
.c.o:
	$(CC) -o $@ -c $(PROF) $(CFLAGS) $*.c

top_srcdir = ..

CC = gcc
C++ = g++
CCOPT = -Wall -O2  -fstack-protector 
#CCOPT = -Wall -g -O0
PROF =
#PROF = -pg 
PERL = perl
DEPEND = $(PERL) $(top_srcdir)/scripts/update_dependencies

STATIC = -static

LIBS =

INCLUDE = -I./ -L/usr/lib/x86_64-linux-gnu

DEFINE =  -fsigned-char -fno-inline \
	-D_REENTRANT -D_POSIX_PTHREAD_SEMANTICS \
	-D_POSIX_PER_PROCESS_TIMER_SOURCE -D_PTHREADS -DUNICODE \
#	-DDEBUG

SHLIB_SUFFIX   = .so
SHLIB_LD       = gcc -shared
SHLIB_LD_LIBS  = 
SHLIB_CFLAGS   = -fPIC

BFLAGS = $(DEFINE) $(INCLUDE)
CFLAGS = $(CCOPT) $(BFLAGS)

# andaleeb changed for fc10. linked .so in case .a does not exist

SSL_LIBS = /usr/lib/libssl.a \
        /usr/lib/libcrypto.a \
#        /usr/lib/libkrb5.a \
        /usr/lib/libcom_err.a \
        /usr/lib/libresolv.a \
#        /usr/lib/libk5crypto.a

# Lars: override SSL_LIBS to be more generic
SSL_LIBS=-lssl -lcrypto -lresolv -ldl

#KRB_LIBS = $(if $(wildcard /usr/lib/libkrb5.a), /usr/lib/libkrb5.a, /usr/lib/libkrb5.so)

#KRB_LIBS = -lkrb5
#SSL_LIBS+= $(KRB_LIBS)

#KRB_LIBS = $(if $(wildcard /usr/lib/libk5crypto.a), /usr/lib/libk5crypto.a, /usr/lib/libk5crypto.so)
#KRB_LIBS = -lk5crypto
#SSL_LIBS+= $(KRB_LIBS)


OBJ_C = md5.o

OBJ_CC = \
	debug.o \
	critsec.o \
	options.o \
	log.o \
	poll.o \
	sigutil.o \
	thread.o \
	StringTokenizer.o \
	timer.o \
	simple_crypt.o \
	randomizing_crypt.o \
	combo_crypt.o \
	Random.o \
	dassert.o \
	rand.o \
	util.o \
	file.o \
	tstring.o \
	base64.o \
	hashtable.o \
	PeriodicTask.o \
	PeriodicTask2.o \
	checkip.o \
	string_utils.o \
	IncrementCounter.o \
	BinHex.o \
	Password3DES.o \
	IntMT.o \
	LongLongMT.o \
	DoubleMT.o \
	CPollArray.o \
	EPollArray.o \
	CpuUsage.o\
	
OBJS = $(OBJ_C) $(OBJ_CC) 

SRCS = $(OBJ_C:.o=.c) $(OBJ_CC:.o=.cc)

libutil.a: $(OBJS)
	ar crsu $@ $(OBJS)

pass3des: libutil.a pass3des_main.cc
	$(C++) $(CFLAGS) -o pass3des pass3des_main.cc libutil.a $(SSL_LIBS)

pass3desf: libutil.a pass3desf_main.cc
	$(C++) $(CFLAGS) -o pass3desf pass3desf_main.cc libutil.a $(SSL_LIBS)

gendeskey: libutil.a gendeskey_main.cc
	$(C++) $(CFLAGS) -o gendeskey gendeskey_main.cc libutil.a $(SSL_LIBS)

gen3deskey: libutil.a gen3deskey_main.cc
	$(C++) $(CFLAGS) -o gen3deskey gen3deskey_main.cc libutil.a $(SSL_LIBS)

ebpasswd: libutil.a ebpasswd.cc
	$(C++) $(CFLAGS) -o ebpasswd ebpasswd.cc libutil.a $(SSL_LIBS)

ebdecrypt: libutil.a ebdecrypt.cc
	$(C++) $(CFLAGS) -o ebdecrypt ebdecrypt.cc libutil.a $(SSL_LIBS)

ebhash: libutil.a ebhash.cc
	$(C++) $(CFLAGS) -o ebhash ebhash.cc libutil.a $(SSL_LIBS)

.PHONY: subdirs $(SUBDIRS)

subdirs: $(SUBDIRS)

$(SUBDIRS):
	$(MAKE) -C $@

clean:	
	@for T in $(SUBDIRS); do make -C $$T $@; done
	$(RM) *.o .depend* core
	$(RM) $(ALL)

depend: Makefile
	$(DEPEND) $(INCLUDE) .depend $(SRCS)

-include .depend

