// ---------------------------------------------------------------------------
// $Id: timer.h,v 1.1 2005/06/10 22:53:02 jozsef Exp $
//
// File:       timer.h
//
// Copyright:  Eyeball Networks Inc. 2001
//
// Author:     Jozsef Vass
//
// Purpose:    StopWatch class
//
// Change Log: 03/27/2001 - Created
// ---------------------------------------------------------------------------

#ifndef __TIMER__
#define __TIMER__

#include <sys/time.h>

class StopWatch
{
public:
	StopWatch();

	void Reset();
	int Start();
	int Stop();

	float GetAverageTime(int iInterval) const;
	float GetAverageTime() const;
	long GetTotalTime() const { return m_lTotalTime; };
	int GetCount() const { return m_iCount; };

private:
	struct timeval m_StartTime;
	struct timeval m_StopTime;
	long m_lTotalTime;			// in ms
	int m_iCount;
	bool m_bRunning;
};

#endif  // __TIMER__
