// checkip.cc

#include "checkip.h"
#include <arpa/inet.h>

//Use this function
bool isIPAddress(const string& sIP)
{
    sockaddr_storage addr;
    bool isIPv4 = inet_pton(AF_INET,sIP.data(),&addr) == 1;
    bool isIPv6 = inet_pton(AF_INET6,sIP.data(),&addr) == 1;

    return isIPv4 | isIPv6;
}

//Calls for ddd.ddd.ddd.ddd notation
bool isIPv4Address(const string& sIP)
{
  int len = sIP.size();

  if (len >= 7) {
    int iNumDots = 0;
    int iLastDot = -1;
    int iCurrNum = 0;
    int i;

    const char* szIP = sIP.data();

    for (i = 0; i < len; i++) {
      char ch = szIP[i];
      if (('0' <= ch) && (ch <= '9')) {
        iCurrNum = (iCurrNum * 10) + (int)(ch - '0');
        if (iCurrNum > 255) break;
      }
      else if (ch == '.') {
        if ((iLastDot + 1) == i) break;
        iLastDot = i;
        iNumDots++;
        if (iNumDots > 3) break;
        iCurrNum = 0;
      }
      else {
        break;
      }
    }

    if ((i == len) && (iNumDots == 3) && ((iLastDot + 1) < len)) {
      return true;
    }
  }

  return false;
}

bool isIPv6Address(const string& sIP)
{
    sockaddr_storage addr;
    return inet_pton(AF_INET6,sIP.data(),&addr) == 1;
}

bool isValidPort(unsigned int x)
{
    return (x>=0 && x<65536);
}

bool isValidPort(const std::string &x)
{
    unsigned int _port;
    sscanf(x.data(),"%u",&_port);
    return isValidPort(_port);
}

std::string retriveAmazaonRealIPv4() {
    std::string cmd("wget -qO- http://instance-data/latest/meta-data/public-ipv4");
    FILE* pipe = popen(cmd.data(), "r");
    if (!pipe) return "";

    char buffer[128]={0};
    std::string result = "";
    while(!feof(pipe)) {
        if(fgets(buffer, 128, pipe) != NULL)
            result += buffer;
    }
    pclose(pipe);

    if ( isIPv4Address(result)) return result;
    return "";
}
