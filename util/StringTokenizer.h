// $Id: StringTokenizer.h,v 1.1 2005/05/31 03:05:18 farhan Exp $

/** All files of the DNDS Prototype are Copyright (C) 1999 by Andre Schoorl
 *  unless otherwise noted.
 *
 *  The DNDS Prototype is distributed in the public domain in the hope that it
 *  will be useful, but is provided `as is' and is subject to change without
 *  notice.  No warranty of any kind is made with regard to the software or
 *  documentation.  The author shall not be liable for for any error for
 *  incidental or consequential damages in connection with the software and
 *  the documentation.
 *
 *  Permission to copy the whole, unmodified DNDS package is granted provided
 *  that the copies are not made or distributed for resale (excepting nominal
 *  copying fees).  All redistributions in either source or binary form must
 *  include this disclaimer.
 */

#ifndef AS_TOKEN_H
#define AS_TOKEN_H

#include "tstring.h"

std::string ToSingleByteStr(const std::string& _str);
std::string ToMultiByteStr(const std::string& _str);
std::string tolower(std::string _str);
tstring tolower(tstring _str);
bool compare_i(const std::string& _s1, const std::string& _s2);
std::string trim_whitespace(const std::string& _str);
tstring trim_whitespace(const tstring& _str);
std::string skip_whitespace(const std::string& _str);
bool IsWhitespaceOnly(const std::string& _str);
std::string escape_quotes(const std::string& _str);
std::string itoa(const int _x);
std::string utoa(const unsigned int _x);
std::string itoh(const int _x);
std::string utoh(const unsigned int _x);
std::string escape_html(const std::string& str);
std::string unescape_html(const std::string& str);

std::string EncodeUcn(const tstring& sPlainText);
tstring DecodeUcn(const std::string& sUcnText);

#define LT_CHECK(A, B) \
	if ((A) != (B)) \
	return (A) < (B)

struct lt_lowerstring
{
	bool operator()(const std::string& s1, const std::string& s2) const;
};

struct lt_lowertstring
{
	bool operator()(const tstring& s1, const tstring& s2) const;
};

class StringTokenizer
{
	private:
		std::string str;
		std::string::size_type pos;

	public:
		StringTokenizer();
		StringTokenizer(const std::string& _str);

		void setString(const std::string& _str);
		bool nextToken(std::string& _token);

		static std::string ltoa(const long _x);
};

#endif // AS_TOKEN_H

