#ifndef _EPOLLARRAY_H_
#define _EPOLLARRAY_H_

#include <sys/epoll.h>
#include <sys/resource.h>

namespace NEB{
        /// A class containing a poll array
        /**
        * This class does some required management.
        */
        class CEPollArray
        {
                public:
                        /// The constructor.
                        CEPollArray();
                        /// Get a pointer to pollfd structure array.
                        struct epoll_event* Get() const { return m_ptrEventFds; }
                        /// Get the useful size of the pollfd array.
                        unsigned int Size() const { return m_iNumEvents + 1; }
			int poll(int ms);
                        /// Add an Fd.
                        /**
                        * \param p_iFd is an fd.
                        * \param p_sEvents is the requested event of interest, default value is POLLIN.
                        * \return true if sucessful, otherwise false.
                        * \sa Add
                        */
                        bool Add(int p_iFd, int p_iEvents = EPOLLIN);
                        /// Modify an Fd.
                        /**
                        * \param p_iFd is an fd.
                        * \return true if sucessful, otherwise false.
                        * \sa Modify 
                        */
                        bool Modify(int p_iFd, int p_iEvents);
                        /// Remove an Fd.
                        /**
                        * \param p_iFd is an fd.
                        * \return true if sucessful, otherwise false.
                        * \sa Remove 
                        */
                        bool Remove(int p_iFd);
                        /// The destructor
                        /**
                        * Closes array
                        */
                        ~CEPollArray();

                protected:
                        /// Set maximum fd.
                        /**
                        * Set the maximum number of fd.
                        * \return maximum fd.
                        */
                        rlim_t SetMaxFds();
                        /// Get maximum file.
                        /**
                        * Get the number of actually available fds.
                        * \param p_iTryAtMost is the number of files to be tried at most.
                        * \return maximum number of files.
                        */
                        int GetMaxFiles(int p_iTryAtMost = 64000);

                private:
			///Epoll file descriptor
			int m_iEpollFd;
                        /// The array size.
                        int m_iMaxFd;
                        /// Num Events
                        int m_iNumEvents;
                        /// A pointer to pollfd structure
                        struct epoll_event* m_ptrEventFds;
        };
}
#endif
