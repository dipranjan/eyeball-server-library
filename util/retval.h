/** All files of the DNDS Prototype are Copyright (C) 1999 by Andre Schoorl
 *  unless otherwise noted.
 *
 *  The DNDS Prototype is distributed in the public domain in the hope that it
 *  will be useful, but is provided `as is' and is subject to change without
 *  notice.  No warranty of any kind is made with regard to the software or
 *  documentation.  The author shall not be liable for for any error for
 *  incidental or consequential damages in connection with the software and
 *  the documentation.
 *
 *  Permission to copy the whole, unmodified DNDS package is granted provided
 *  that the copies are not made or distributed for resale (excepting nominal
 *  copying fees).  All redistributions in either source or binary form must
 *  include this disclaimer.
 */

#ifndef AS_RETVAL_H
#define AS_RETVAL_H

#include "debug.h"

#define ES_SUCCESS 0
#define ES_FAILURE -1
#define ES_AGAIN   -2
#define ES_REMOTE  -3
#define ES_NOT_CONNECTED -1004

#define RET_IF(S, R) \
	{ \
		if (S) \
		{ \
			DB(__FILE__ << ":" << __LINE__ << ": `" << #S << "' failed."); \
			return R; \
		} \
	}

#define RFI(S) RET_IF(S, false)
#define RTI(S) RET_IF(S, true)
#define RNI(S) RET_IF(S, NULL)
#define RZI(S) RET_IF(S, 0)

// false
#define RFIF(X) RFI((X) == false)
#define RFIT(X) RFI((X) == true)
#define RFIN(X) RFI((X) == NULL)
#define RFIZ(X) RFI((X) == 0)

#define RFUF(X) RFI((X) != false)
#define RFUT(X) RFI((X) != true)
#define RFUN(X) RFI((X) != NULL)
#define RFUZ(X) RFI((X) != 0)

// true
#define RTIF(X) RTI((X) == false)
#define RTIT(X) RTI((X) == true)
#define RTIN(X) RTI((X) == NULL)
#define RTIZ(X) RTI((X) == 0)

#define RTUF(X) RTI((X) != false)
#define RTUT(X) RTI((X) != true)
#define RTUN(X) RTI((X) != NULL)
#define RTUZ(X) RTI((X) != 0)

// NULL
#define RNIF(X) RNI((X) == false)
#define RNIT(X) RNI((X) == true)
#define RNIN(X) RNI((X) == NULL)
#define RNIZ(X) RNI((X) == 0)

#define RNUF(X) RNI((X) != false)
#define RNUT(X) RNI((X) != true)
#define RNUN(X) RNI((X) != NULL)
#define RNUZ(X) RNI((X) != 0)

// 0
#define RZIF(X) RZI((X) == false)
#define RZIT(X) RZI((X) == true)
#define RZIN(X) RZI((X) == NULL)
#define RZIZ(X) RZI((X) == 0)

#define RZUF(X) RZI((X) != false)
#define RZUT(X) RZI((X) != true)
#define RZUN(X) RZI((X) != NULL)
#define RZUZ(X) RZI((X) != 0)

#endif // AS_RETVAL_H

