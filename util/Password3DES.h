//Password3DES.h
//
//Encrypts a password using Triple CBC DES
//
//Zion Kwok
//June 30, 2006
//
//(C) Eyeball Networks

#ifndef EB_PASSWORD_3DES_H_
#define EB_PASSWORD_3DES_H_

#include <string>
using std::string;

bool password_3des(const string& sHexKey, const string& sUsername, const string& sInput, string& sOutput, bool bEncrypt);

bool password_3des(const string& sHexKey1, const string& sHexKey2, const string& sHexKey3,
                   const string& sUsername, const string& sInput, string& sOutput, bool bEncrypt);

//Does not check if the key is weak or not
bool check_3des_key_hex_and_parity(const string& sHexKey, unsigned char cBlock[8]);

#endif //EB_PASSWORD_3DES_H_

