//! PeriodicTask2.cc
//! Implementation for CPeriodicTask2 class

#include "PeriodicTask2.h"
#include <sys/poll.h>
#include <sys/socket.h>

#include "debug.h"
#include "log.h"

//! Constructor
//! intervalMs is the interval in milliseconds
CPeriodicTask2::CPeriodicTask2(int intervalMs, bool bStartThread)
: m_iIntervalMs(intervalMs),
  m_iReadFd(-1),
  m_iWriteFd(-1),
  m_tLoopThread((pthread_t)-1)
{
	DB_ENTER(CPeriodicTask2::CPeriodicTask2);

	pthread_mutex_init(&m_StopMutex, NULL);

	if (bStartThread) {
		Start();
	}
}

//! Destructor
CPeriodicTask2::~CPeriodicTask2()
{
	DB_ENTER(CPeriodicTask2::~CPeriodicTask2);

	Stop();

	pthread_mutex_destroy(&m_StopMutex);
}

//! Start thread
void CPeriodicTask2::Start()
{
	DB_ENTER(CPeriodicTask2::Start);

	if (m_tLoopThread != (pthread_t)-1) return;

	if (m_iIntervalMs > 0) {
		int iSocketPairFd[2];

                int result = socketpair(AF_UNIX, SOCK_DGRAM, 0, iSocketPairFd);

		if (result == 0) {
			m_iReadFd = iSocketPairFd[0];
			m_iWriteFd = iSocketPairFd[1];

			pthread_create(&m_tLoopThread, NULL, LoopThread, this);
		}
		else {
                        DB_AND_LOG("Error: Cannot create socket pair.");
                        DB_AND_LOG("Period Task 2 not started.");
                }
	}
	else {
                DB_AND_LOG("Error: Interval " << m_iIntervalMs << " is invalid.");
                DB_AND_LOG("Period Task 2 not started.");
	}
}


//! Do Task
//! Override this
void CPeriodicTask2::DoTask()
{
	DB_ENTER(CPeriodicTask2::DoTask);
}

//! Stop
//! Only one thread should call this
void CPeriodicTask2::Stop()
{
	DB_ENTER(CPeriodicTask2::Stop);

	pthread_mutex_lock(&m_StopMutex);

	if (m_iIntervalMs > 0) {
		if (m_iWriteFd != -1) {
			if (m_tLoopThread != (pthread_t)-1) {
				//Stop Loop Thread
				char dummy = 'a';
				::send(m_iWriteFd, &dummy, 1, 0);
			
				void* ptr;	
				pthread_join(m_tLoopThread, &ptr);
				m_tLoopThread = (pthread_t)-1;
			}
		}

		if (m_iReadFd != -1) {
			::close(m_iReadFd);
			m_iReadFd = -1;
		}
		if (m_iWriteFd != -1) {
			::close(m_iWriteFd);
			m_iWriteFd = -1;
		}
	}

	pthread_mutex_unlock(&m_StopMutex);
}

//! Loop Thread
void* CPeriodicTask2::LoopThread(void* arg)
{
	DB_ENTER(CPeriodicTask2::LoopThread);

	CPeriodicTask2* pThis = (CPeriodicTask2*)arg;

	struct pollfd myPollFd;
	myPollFd.fd = pThis->m_iReadFd;
	myPollFd.events = POLLIN;	
	
	int iPollPeriod= 1800000;
	int iTimeoutInterval=0;
	int ret=0;
	//int i=0;

	//Infinite Loop
	for (;;) {
		iPollPeriod= 1800000;
		iTimeoutInterval= pThis->m_iIntervalMs;
		ret=0;
		//i=0;
		while(iTimeoutInterval>0){ 
			if (iTimeoutInterval< iPollPeriod){
				iPollPeriod= iTimeoutInterval;
			}
			iTimeoutInterval = iTimeoutInterval - iPollPeriod;
			
			//cout<<(int)pthread_self() << "Calling poll with timeout interval: "<< iPollPeriod <<"i= "<<i<<endl;

			//DB_AND_LOG("Calling poll with timeout interval "<< iPollPeriod);

			ret = ::poll(&myPollFd, 1, iPollPeriod);
			if ((ret>0) || (ret <0)) break;
			//i++;
		}

		if (ret == 0) {
			//cout<<"Poll returned 0"<<endl;
			//DB_AND_LOG("Poll is calling DoTask");
			pThis->DoTask();
			//cout<<"After DoTask...."<<endl;	
		}
                else if (ret > 0) {
			//cout<< "Poll returned +";
			DB_AND_LOG("Poll returned " << ret << ". Leaving CPeriodicTask2.");
			break;
		}
		else {
			//cout<<"Poll returned -";
			DB_AND_LOG("Poll error " << ret << " in CPeriodicTask2.");
			ret = ::poll(&myPollFd, 1, 1000);
			if (ret == -1) {
				int iForcedSleep = 1000;
				if (iForcedSleep > 1000) {
                                  	iForcedSleep = 1000;
                                }
				::poll(NULL, 0, iForcedSleep);
			}
		}// end else
		
	}// end for

	return NULL;
}

