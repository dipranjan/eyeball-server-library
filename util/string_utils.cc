

/*
  $Id: string_utils.cc,v 1.6 2009/04/15 16:42:15 andaleeb Exp $
*/




#include "string_utils.h"
#include <sys/time.h>
#include <stdlib.h>
#include <netdb.h>
#include <arpa/inet.h>

#include "debug.h"
#include "log.h"
#include "base64.h"
#include "memory.h"

using std::string;

string& string_tolower(string& s)
{
    int iLen = s.size();
    char* pBuf = (char*)s.data();
    for (int i = 0; i < iLen; i++) {
        char& ch = pBuf[i];
        if (('A' <= ch) && (ch <= 'Z')) {
            ch = ch + ('a' - 'A');
        }
    }
    return s;
}
string& string_toupper(string& s)
{
    int iLen = s.size();
    char* pBuf = (char*)s.data();
    for (int i = 0; i < iLen; i++) {
        char& ch = pBuf[i];
        if (('a' <= ch) && (ch <= 'z')) {
            ch = ch + ('A' - 'a');
        }
    }
    return s;
}

bool string_case_equal(const string& s1, const string& s2)
{
    bool bEqual = true;
    int iLen = s1.size();

    if (iLen == (int)s2.size()) {
        const char* p1 = s1.data();
        const char* p2 = s2.data();
        for (int i = 0; i < iLen; i++) {
            const char& ch1 = p1[i];

            int diff = ch1 - p2[i];
            if (diff) {
                if (diff == ('A' - 'a')) {
                    if (!(('A' <= ch1) && (ch1 <= 'Z'))) {
                        bEqual = false;
                        break;
                    }
                }
                else if (diff == ('a' - 'A')) {
                    if (!(('a' <= ch1) && (ch1 <= 'z'))) {
                        bEqual = false;
                        break;
                    }
                }
                else {
                    bEqual = false;
                    break;
                }
            }
        }
    }
    else {
        bEqual = false;
    }

    return bEqual;
}

std::string& string_time_random16(std::string& s)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);

    long iData = (long)s.data();
    int seed = rand() ^ (tv.tv_sec << 12) ^ (tv.tv_usec >> 7) ^
            (long)(&tv) ^ (int)iData;

    int rand1[3];
    rand1[0] = rand_r((unsigned int*)&seed);
    rand1[1] = tv.tv_sec ^ (iData & 0x3456) ^ rand1[0];
    rand1[2] = (long)(&tv) ^ tv.tv_usec ;

    char* szBase64 = NULL;
    int iBase64Len;
    Base64Encode((const char*)rand1, 12, szBase64, iBase64Len);
    s.assign(szBase64, 16);

    delete [] szBase64;

    return s;
}  

char *findSubstring(char *string/* String to search. */, char *substring/* Substring to find in string */)
{
    char *a, *b, *pointer;

    /*
   * First scan quickly through the two strings looking for a single-
   * character match.  When it's found, then compare the rest of the
   * substring.
   */

    pointer = string;

    for (b = substring; *pointer != 0; pointer += 1) {
        if (*pointer != *b)
            continue;
        a = pointer;
        for (;;) {
            if (*b == 0)
                return(pointer);
            if (*a++ != *b++)
                break;
        }
        b = substring;
    }
    return((char *) NULL);
}


int GetServerPort(const string& sAddressPort)
{
    // find colon and take numeric value after the colon
    int iPos = sAddressPort.find(':');
    if (iPos < 0)
        return 0;

    string s = sAddressPort.substr(iPos + 1);

    return atoi(s.c_str());
}

string GetServerAddrString(const string& sAddressPort)
{
    // find colon and take substring before the colon
    int iPos = sAddressPort.find(':');

    return sAddressPort.substr(0, iPos);
}

string GetIpByHostnameOrIp(string sAddress)
{
    string sIp;

    struct addrinfo hints;
    struct addrinfo *result, *rp;
    int sfd, s;

    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_UNSPEC;    /* Allow IPv4 or IPv6 */
    //hints.ai_socktype = SOCK_DGRAM; /* Datagram socket */
    //hints.ai_flags = AI_PASSIVE;    /* For wildcard IP address */
    hints.ai_protocol = 0;          /* Any protocol */
    hints.ai_canonname = NULL;
    hints.ai_addr = NULL;
    hints.ai_next = NULL;

    s = getaddrinfo( sAddress.c_str(),0, &hints, &result);
    if (s != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
        exit(EXIT_FAILURE);
    }

    /* getaddrinfo() returns a list of address structures.
                Try each address until we successfully bind(2).
                If socket(2) (or bind(2)) fails, we (close the socket
                and) try the next address. */

    /*for (rp = result; rp != NULL; rp = rp->ai_next) {
                 sfd = socket(rp->ai_family, rp->ai_socktype,
                         rp->ai_protocol);
                 if (sfd == -1)
                     continue;

                 if (bind(sfd, rp->ai_addr, rp->ai_addrlen) == 0)
                     break;

                 close(sfd);
             }

             if (rp == NULL) {
                 fprintf(stderr, "Could not bind\n");
                 exit(EXIT_FAILURE);
             }*/
    struct sockaddr *sa = result->ai_addr;    /* input */
    socklen_t len = result->ai_addrlen;         /* input */
    char hbuf[NI_MAXHOST], sbuf[NI_MAXSERV];

    if (getnameinfo(sa, len, hbuf, sizeof(hbuf), sbuf,
                    sizeof(sbuf), NI_NUMERICHOST | NI_NUMERICSERV) == 0)
    {
        sIp = hbuf;
    }

    freeaddrinfo(result);             /* No longer needed */

    return sIp;
}

int GetServerAddr(const string& sAddressPort)
{
    string sAddress = GetServerAddrString(sAddressPort);
    //sAddress = GetIpByHostnameOrIp(sAddress);
    int iAddress = 0;
    struct in_addr addr;

    int iResult = inet_pton(AF_INET, sAddress.c_str(), &addr);
    if (iResult > 0) {
        iAddress = ntohl(addr.s_addr);
    }
    else {
        DB_AND_LOGL(3, "Cannot convert IP address to integer: " << sAddress.c_str());
        return false;
    }

    //  return iAddress;
}


/*
  $Log: string_utils.cc,v $
  Revision 1.6  2009/04/15 16:42:15  andaleeb
  Changed for building in Fedora 10.

  Revision 1.5  2006/08/21 22:55:55  zionk
  Fix string_case_equal()

  Revision 1.4  2006/08/21 16:28:39  zionk
  added string_time_random16

  Revision 1.3  2006/08/16 18:14:23  zionk
  Add lower/uppercase functions

  Revision 1.2  2006/07/04 23:57:35  zionk
  Add address string when IP cannot be converted to integer

  Revision 1.1  2006/05/12 22:08:36  zionk
  Move servercore/utils to string_utils

  Revision 1.2  2006/04/11 00:59:09  michaelc
  updated remote-logging to use database for server address/port

  Revision 1.1  2006/03/07 18:12:00  zionk
  Multi-threaded server core

  Revision 1.2  2005/10/19 22:05:18  larsb
  removed memory leak in send buffers

  Revision 1.1  2005/07/21 19:05:49  larsb
  *** empty log message ***

*/


