// combo_crypt.cc
// Combines simple_crypt and randomzing_crypt
//
// Zion Kwok
// May 8, 2006
// Eyeball Networks
///////////////////////////////////////////////

#include "combo_crypt.h"
#include "simple_crypt.h"
#include "randomizing_crypt.h"

string combo_encrypt(const string& sInput)
{
	if (sInput.size() == 0) return "";

	string sCode1 = Crypt::simple_crypt(sInput, false);
	string sCode2 = randomizing_encrypt(sCode1);
	return Crypt::simple_crypt(sCode2, true);
}

string combo_decrypt(const string& sInput)
{
	if (sInput.size() == 0) return "";

	string sCode2 = Crypt::simple_decrypt(sInput, true);
	string sCode1 = randomizing_decrypt(sCode2);
	return Crypt::simple_decrypt(sCode1, false);
}

