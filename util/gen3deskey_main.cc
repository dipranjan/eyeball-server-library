//Encrypt and decrypt password

#include <stdio.h>
#include "BinHex.h"
#include <openssl/des.h>

int main(int argc, char** argv)
{
  unsigned char hexKey[49];
  hexKey[48] = '\0';

  for (int k = 0; k < 3; k++) {
    DES_cblock block;
    DES_key_schedule ks;

    for (int i = 0; i < 10; i++) {
      DES_random_key(&block);
      int iRet = DES_set_key_checked(&block, &ks);
      if (iRet == 0) break;
    }

    convertBinToHex((unsigned char*)block, 8, &(hexKey[k << 4]));
  }
  
  printf("%s\n", hexKey);

  return 0;
}

