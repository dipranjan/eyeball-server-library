#include "vmd5.h"
#include "ResiprocateAuth.h"
#include "Data.h"
#include "MD5Stream.h"
#include <iostream>
#include <iomanip>

#include<string>

using std::string;
using std::setw;
using std::setfill;

using namespace resip;

#define BOOL bool

#define COLON ":"
#define AUTH  "auth"

#define TRUE true
#define FALSE false

Data makeResponseMD5(
	const char* szUserName,
	const char* szPassword,
	const char* szRealm,
	const char* szMethod,
	const char* szDigestUri,
	const char* szNonce,
	const Data& qop = Data::Empty,
	const Data& cnonce = Data::Empty, 
    const Data& cnonceCount = Data::Empty)
{
   MD5Stream a1;
   a1 << szUserName
      << COLON
      << szRealm
      << COLON
      << szPassword;

   MD5Stream a2;
   a2 << szMethod
      << COLON
      << szDigestUri;
   
   MD5Stream r;
   r << a1.getHex()
     << COLON
     << szNonce
     << COLON;

	if (!qop.empty()) 
    {
		r << cnonceCount
        << COLON
        << cnonce
        << COLON
        << qop
        << COLON;
	}
	
	r << a2.getHex();

	return r.getHex();
}

string RemoveQuote(const string& s)
{
	string sout = s.substr(1, string::npos);
	sout = sout.substr(0, sout.size() - 1);
	return sout;
}

string AddQuote(const string& s)
{
	string sout = "\"";
	sout += s;
	sout += "\"";
	return sout;
}

void updateNonceCount(unsigned int& nonceCount, Data& nonceCountString)
{
   if (!nonceCountString.empty())
   {
      return;
   }
   nonceCount++;
   {
      DataStream s(nonceCountString);
      
      s << std::setw(8) << std::setfill('0') << std::hex << nonceCount;
	  //s << setw(8) << setfill('0') << std::hex << nonceCount;
   }
   //DebugLog(<< "nonceCount is now: [" << nonceCountString << "]");
}


void AddChallengeResponseAuth(
			      osip_message_t* response,
			      osip_www_authenticate_t* authenticate,
			      const char* szUserName,
			      const char* szPassword,
			      BOOL bWWWAuthenticate)
{
  osip_authorization_t* auth = NULL;
  if (bWWWAuthenticate) {
    osip_authorization_init(&auth);
  }
  else {
    osip_proxy_authorization_init(&auth);
  }

  // user name
  osip_authorization_set_username(auth, osip_strdup(AddQuote(szUserName).c_str()));

  // realm
  char* szRealm = osip_www_authenticate_get_realm(authenticate);
  osip_authorization_set_realm(auth, osip_strdup(szRealm));

  // nonce
  char* szNonce = osip_www_authenticate_get_nonce(authenticate);
  osip_authorization_set_nonce(auth, osip_strdup(szNonce));

  // authtype = "digest"
  char* szAuthType = osip_www_authenticate_get_auth_type(authenticate);
  if (szAuthType)
    {
      osip_authorization_set_auth_type(auth, osip_strdup(szAuthType));
    }
	
  // opaque
  char* szOpaque = osip_www_authenticate_get_opaque(authenticate);
  if (szOpaque)
    {
      osip_authorization_set_opaque(auth, osip_strdup(szOpaque));
    }
	
  // message qop
  char* szQop = osip_www_authenticate_get_qop_options(authenticate);
  string sQop;
  if (szQop)
    sQop = RemoveQuote(szQop);

  // algorithm
  char* szAlgorithm = osip_www_authenticate_get_algorithm(authenticate);
  if (szAlgorithm)
    {
      osip_authorization_set_algorithm(auth, osip_strdup(szAlgorithm));
    }
  /*else
    {
    osip_authorization_set_algorithm(auth, osip_strdup("MD5"));
    }*/
	
  // mrk
  // digest uri
  char* szDigestUri = NULL;
  osip_uri_to_str(response->req_uri, &szDigestUri);
  osip_authorization_set_uri(auth, osip_strdup(AddQuote(szDigestUri).c_str()));

  // method
  char* szMethod = osip_message_get_method(response);

  Data rsp;
  Data nonceCountString = Data::Empty;
  const Data cnonce = Data::Empty;
  unsigned int nonceCount=0;

  if (strcmp(sQop.c_str(), AUTH) == 0 && szOpaque)
    {
      updateNonceCount(nonceCount, nonceCountString);
      rsp = makeResponseMD5(
			    szUserName, 
			    szPassword, 
			    RemoveQuote(szRealm).c_str(),
			    szMethod,
			    szDigestUri,
			    RemoveQuote(szNonce).c_str(),
			    AUTH,
			    cnonce,
			    nonceCountString);

      osip_authorization_set_cnonce(auth, osip_strdup(AddQuote(cnonce.c_str()).c_str()));
      osip_authorization_set_message_qop(auth, szQop);
      osip_authorization_set_nonce_count(auth, osip_strdup(AddQuote(nonceCountString.c_str()).c_str()));
    }
  else
    {
      rsp = makeResponseMD5(
			    szUserName, 
			    szPassword, 
			    RemoveQuote(szRealm).c_str(),
			    szMethod,
			    szDigestUri,
			    RemoveQuote(szNonce).c_str());
    }

  osip_authorization_set_response(auth, osip_strdup(AddQuote(rsp.c_str()).c_str()));

  free(szDigestUri);

  char* szAuth = NULL;
  osip_authorization_to_str(auth, &szAuth);
  osip_authorization_free(auth);
  if (bWWWAuthenticate)
    osip_message_set_authorization(response, szAuth);
  else
    osip_message_set_proxy_authorization(response, szAuth);
  free(szAuth);
}


int addAuthorization(
		     osip_message_t* response,
		     osip_message_t* challenge,
		     const char* szUserName,
		     const char* szPassword)
{
  int iPos = 0;
  osip_www_authenticate_t* auth = NULL;
  while (osip_message_get_www_authenticate(challenge, iPos, &auth) >= 0)
    {
      AddChallengeResponseAuth(response, auth, szUserName, szPassword, TRUE);
      iPos++;
    }
	
  iPos = 0;
  while (osip_message_get_proxy_authenticate(challenge, iPos, &auth) >= 0)
    {
      AddChallengeResponseAuth(response, auth, szUserName, szPassword, FALSE);
      iPos++;
    }
	
  return 0;
}



#define HASHLEN 16
typedef char HASH[HASHLEN];
#define HASHHEXLEN 32
typedef char HASHHEX[HASHHEXLEN+1];
#define IN
#define OUT


void CvtHex(
    IN HASH Bin,
    OUT HASHHEX Hex)
{
    unsigned short i;
    unsigned char j;

    for (i = 0; i < HASHLEN; i++) {
        j = (Bin[i] >> 4) & 0xf;
        if (j <= 9)
            Hex[i*2] = (j + '0');
         else
            Hex[i*2] = (j + 'a' - 10);
        j = Bin[i] & 0xf;
        if (j <= 9)
            Hex[i*2+1] = (j + '0');
         else
            Hex[i*2+1] = (j + 'a' - 10);
    };
    Hex[HASHHEXLEN] = '\0';
}




void CvtBin(IN HASH Hex, OUT HASH Bin)
{
  unsigned short i;
  unsigned char j;

  for (i=0;i<HASHLEN;i++)
    {
      j = Hex[i*2];
      if ((j >= 'a')&&(j<='f'))
        Bin[i] = (j - 97 + 10)*16;
      else
        Bin[i] = (j-48)*16;

      j = Hex[i*2+1];
      if ((j >= 'a')&&(j<='f'))
        Bin[i] += (j - 97)+10;
      else
        Bin[i] += (j-48);
    }

}



/* calculate H(A1) as per spec */
void DigestCalcHA1(
    IN const char * pszAlg,
    IN const char * pszUserName,
    IN const char * pszRealm,
    IN const char * pszPassword,
    IN const char * pszNonce,
    IN const char * pszCNonce,
    OUT HASHHEX SessionKey)
{
      MD5Context Md5Ctx;
      HASH HA1;

      /* lars: changed the password storage, the first part user:realm:pass now comes from the database
	 the next block of code is therefore obsolete
      MD5Init(&Md5Ctx);
      MD5Update(&Md5Ctx, pszUserName, strlen(pszUserName));
      MD5Update(&Md5Ctx, ":", 1);
      MD5Update(&Md5Ctx, pszRealm, strlen(pszRealm));
      MD5Update(&Md5Ctx, ":", 1);
      MD5Update(&Md5Ctx, pszPassword, strlen(pszPassword));
      MD5Final(HA1, &Md5Ctx);
      */

      //if (stricmp(pszAlg, "md5-sess") == 0) { mrk
      if (strcasecmp(pszAlg, "md5-sess") == 0) {

            MD5Init(&Md5Ctx);
	    // changed to reflect the password storage change
	    // old: MD5Update(&Md5Ctx, HA1, HASHLEN);
	    MD5Update(&Md5Ctx, pszPassword, strlen(pszPassword));
            MD5Update(&Md5Ctx, ":", 1);
            MD5Update(&Md5Ctx, pszNonce, strlen(pszNonce));
            MD5Update(&Md5Ctx, ":", 1);
            MD5Update(&Md5Ctx, pszCNonce, strlen(pszCNonce));
            MD5Final(HA1, &Md5Ctx);
      };

      CvtHex(HA1, SessionKey);
};

/* calculate request-digest/response-digest as per HTTP Digest spec */
void DigestCalcResponse(
    IN HASHHEX HA1,           /* H(A1) */
    IN const char * pszNonce,       /* nonce from server */
    IN const char * pszNonceCount,  /* 8 hex digits */
    IN const char * pszCNonce,      /* client nonce */
    IN const char * pszQop,         /* qop-value: "", "auth", "auth-int" */
    IN const char * pszMethod,      /* method from the request */
    IN const char * pszDigestUri,   /* requested URL */
    IN HASHHEX HEntity,       /* H(entity body) if qop="auth-int" */
    OUT HASHHEX Response)      /* request-digest or response-digest */
{
      MD5Context Md5Ctx;
      HASH HA2;
      HASH RespHash;
      HASHHEX HA2Hex;

      // calculate H(A2)
      MD5Init(&Md5Ctx);
      MD5Update(&Md5Ctx, pszMethod, strlen(pszMethod));
      MD5Update(&Md5Ctx, ":", 1);
      MD5Update(&Md5Ctx, pszDigestUri, strlen(pszDigestUri));
      //if (stricmp(pszQop, "auth-int") == 0) { mrk
      if (strcasecmp(pszQop, "auth-int") == 0) {
            MD5Update(&Md5Ctx, ":", 1);
            MD5Update(&Md5Ctx, HEntity, HASHHEXLEN);
      };
      MD5Final(HA2, &Md5Ctx);
      CvtHex(HA2, HA2Hex);

      // calculate response
      MD5Init(&Md5Ctx);
      MD5Update(&Md5Ctx, HA1, HASHHEXLEN);
      MD5Update(&Md5Ctx, ":", 1);
      MD5Update(&Md5Ctx, pszNonce, strlen(pszNonce));
      MD5Update(&Md5Ctx, ":", 1);
      if (*pszQop) {

          MD5Update(&Md5Ctx, pszNonceCount, strlen(pszNonceCount));
          MD5Update(&Md5Ctx, ":", 1);
          MD5Update(&Md5Ctx, pszCNonce, strlen(pszCNonce));
          MD5Update(&Md5Ctx, ":", 1);
          MD5Update(&Md5Ctx, pszQop, strlen(pszQop));
          MD5Update(&Md5Ctx, ":", 1);
      };
      MD5Update(&Md5Ctx, HA2Hex, HASHHEXLEN);
      MD5Final(RespHash, &Md5Ctx);
      CvtHex(RespHash, Response);
};





