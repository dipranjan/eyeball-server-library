#include <osip2/osip.h>
#include <osipparser2/osip_parser.h>
#include <osipparser2/sdp_message.h>
#include <osip2/osip_dialog.h>


#ifdef __cplusplus
extern "C"
{
#endif

extern int addAuthorization(
	osip_message_t* response,
	osip_message_t* challenge,
	const char* szUserName,
	const char* szPassword);



#ifdef __cplusplus
}
#endif
