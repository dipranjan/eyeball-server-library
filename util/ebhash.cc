// ---------------------------------------------------------------------------
// File:       ebhash.cc
//
// Copyright:  Eyeball Networks Inc. 2007
//
// Author:     Zion Kwok
//
// Purpose:    Generate 30-bit integer hash value for a string
//
// ---------------------------------------------------------------------------

#include "hashtable.h"
#include "string_utils.h"
#include <iostream>
#include <string>

int main(int argc, const char *argv[])
{
	if (argc < 2)
        {
		std::cerr << "Usage: " << argv[0] << " <string>" << std::endl;
		return 1;
	}

	std::string sHash = itos(FastGoodStringHash30(argv[1]));
	std::cout << sHash.c_str() << std::endl;
	return 0;
}
