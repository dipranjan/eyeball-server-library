#ifndef CPUUSAGE_H
#define CPUUSAGE_H

#include "PeriodicTask.h"
#include <iostream>
#include <stdio.h>
#include "log.h"
#include <fstream>

using namespace std;

#define CPU_USAGE_ITERAION 6

class CpuUsageCalculator : public CPeriodicTask
{
    double totalUsage;
    double prev_idle;
    double prev_total;

    int currentState;
public:
    CpuUsageCalculator();
    void DoTask();

    double getCurrentCpuUsage();
};

/**
 * @brief Running each 1 sec. If timeout passed then staring calculator. after 5 sec getting value from calculator
 */
class CpuUsage : public CPeriodicTask
{

    int checkTimeout;
    int currentState;
    double currentCpuUsage;
    CpuUsageCalculator *calculator;


public:
    CpuUsage(int timeoutSec);
    void DoTask();

    double getCurrentCpuUsage();
    ~CpuUsage();
};

extern CpuUsage *g_pCpuUsage;
#endif // CPUUSAGE_H
