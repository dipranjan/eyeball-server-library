// $Id: log.cc,v 1.15 2006/07/27 00:11:05 zionk Exp $

#include <fstream>

#include <unistd.h>		// getpid()
#include <stdio.h>		// sprintf(), rename()
#include <time.h>		// cftime()
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "log.h"
#include "debug.h"



// Instantiate static variables
ostream *Log::log_out;
string Log::m_sFilename = "";
int Log::m_iAutoRotateCount = 0;
unsigned int Log::m_ui32MaxFileSize = DEFAULT_MAX_LOG_FILE_SIZE;
unsigned int Log::m_ui32MaxFileCount = DEFAULT_MAX_LOG_FILE_COUNT;

CIntMT g_iVerboseLevel;
bool g_bUseSyslog;


pthread_mutex_t logMutex = PTHREAD_MUTEX_INITIALIZER;


bool Log::open(const string& _filename)
{
  if (!g_bUseSyslog) {
    ASSERT(!log_out);
    WARN(Log::log_out = new ofstream(_filename.c_str(), ios::out |ios::app));
    
    if (log_out != 0) {
      m_sFilename = _filename;
      m_iAutoRotateCount = 0;
      return true;
    }
    m_sFilename = "";
    m_iAutoRotateCount = 0;
  }
  else {
    openlog("AFS",LOG_PID,LOG_USER);
    return true;
  }
  return false;
}


void Log::close(void)
{
  if (g_bUseSyslog) {
    closelog();
  }
  else {
    if (!Log::log_out)
      return;
    
//   pthread_mutex_lock(&logMutex);
    *Log::log_out << flush;
    
    delete Log::log_out;
    Log::log_out = NULL;
    m_sFilename = "";
    m_iAutoRotateCount = 0;
//  pthread_mutex_unlock(&logMutex);
  }
}


string Log::generate_filename(const string& _name)
{
  char pid[5];
#ifdef __linux__
  sprintf(pid, "%d", getpid());
#else
  sprintf(pid, "%ld", getpid());
#endif

  return _name + pid + ".log";
}


string Log::localtime(void)
{
  // mm/dd/yyyy HH:MM:SS
  // 1234567891123456789
  char buf[25];

  time_t tNow = time(NULL);
  if (tNow==-1) return "time() failed";

  strftime(buf, sizeof(buf), "%Y-%m-%d %T", ::localtime(&tNow));

  return buf;
}


bool Log::rotate(const string& _filename)
{
  bool result = true;
  char buf[8];
  int pos = 1;

  if (!g_bUseSyslog) {
  //  pthread_mutex_lock(&logMutex);
    buf[7] = '\0';
    sprintf(buf,"%07d",pos);
    
    std::string sNewFilename = _filename + (string)"." + buf;
    
  //  pthread_mutex_unlock(&logMutex);
    Log::close();
  //  pthread_mutex_lock(&logMutex);
    
    pos = 0;
    while ((access(sNewFilename.c_str(),F_OK)==F_OK)&&(pos<=(int)m_ui32MaxFileCount))
      {
	pos++;
	sprintf(buf,"%07d",pos);
	sNewFilename = _filename + (string)"." + buf;
      }
    
    if (pos > (int)m_ui32MaxFileCount)
      {
	pos = 1;
	sprintf(buf,"%07d",pos);
	sNewFilename = _filename + (string)"." + buf;
      }
    
    if (pos <= ((int)m_ui32MaxFileCount - 1))
      {
	//Delete "next" file
	sprintf(buf,"%07d",pos + 1);
	string sNextFilename = _filename + (string)"." + buf;
	unlink(sNextFilename.c_str());
      }
    
    if (rename(_filename.c_str(),sNewFilename.c_str())<0)
      {
	DB("could not rotate log file "<<_filename<<" : "<<strerror(errno));
	result = false;
      }
    
    Log::open(_filename);
  //  pthread_mutex_unlock(&logMutex);
  }
  return result;

}


string Log::autorotate()
{
  string sFilename = "";

  if (!g_bUseSyslog) {
//    pthread_mutex_lock(&logMutex);
    m_iAutoRotateCount++;
    if ((m_iAutoRotateCount & 0x3FF) == 1) {
      if (Log::log_out) {
	unsigned int ui32FileSize = Log::log_out->tellp();
          if (ui32FileSize > m_ui32MaxFileSize) {
	    sFilename = m_sFilename;
          }
      }
    }
//    pthread_mutex_unlock(&logMutex);
  }
  return sFilename;
}


/*
  $Log: log.cc,v $
  Revision 1.15  2006/07/27 00:11:05  zionk
  Set g_iVerboseLevel to CIntMT instead of int

  Revision 1.14  2006/07/20 22:26:54  zionk
  Allow number of log files to be configurable

  Revision 1.13  2006/07/05 15:54:25  zionk
  Make buffer bigger so it can hold 4 characters.
  Use %03d instead of just %d for the log number

  Revision 1.12  2006/07/04 23:44:36  zionk
  Allow 1000 log files. Make wrap-around work by deleting "next" log file

  Revision 1.11  2006/07/04 22:19:40  zionk
  When rotating logs, and reach 99, go back to 1.

  Revision 1.10  2006/05/17 00:33:15  zionk
  Fix autorotate so it doesn't deadlock

  Revision 1.9  2006/05/16 19:57:50  zionk
  Maximum log file size (before rotation) is configurable

  Revision 1.8  2006/05/16 18:06:43  zionk
  Added log auto-rotate

  Revision 1.7  2006/03/07 18:14:24  zionk
  Add bool return value to Log::open

  Revision 1.6  2006/01/28 10:12:12  farhan
  Fedora Core 4

  Revision 1.5  2005/10/11 23:29:29  larsb
  added thread-safe version of hashtable

  Revision 1.4  2005/08/19 21:10:19  larsb
  *** empty log message ***

  Revision 1.3  2005/08/18 18:10:10  larsb
  added int g_iVerboseLevel to log.h/log.cc

  Revision 1.2  2005/08/16 21:08:50  larsb
  added function to rotate log files

*/
