// ---------------------------------------------------------------------------
// File:       util.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:
//
// Change Log:
// ---------------------------------------------------------------------------

#ifndef EC_UTIL_H
#define EC_UTIL_H

// ---------------------------------------------------------------------------
// Macros
// ---------------------------------------------------------------------------

#define SAFE_DELETE(X) {if (X) { delete (X); (X) = NULL; } }

#define DELETE_ARRAY(X) \
	{ if (X) { delete [] (X);  (X) = NULL; } }

#define CLOSE_SOCKET(X) \
	{ \
		if (X != -1) \
		{ \
				::shutdown(X, 2); \
				::close(X); \
				(X) = -1; \
		} \
	}

#define PERROR(X) \
	LOG(X << ": " << strerror(errno));

#define PERRORL(Y,X) \
	LOGL(Y,X << ": " << strerror(errno));

#define INTERNAL_ERROR() \
	LOG("WARNING: Server state error " << __FILE__ << ": " << __LINE__);

#define INTERNAL_ERROR2() \
	LOGL(2,"WARNING: Server state error " << __FILE__ << ": " << __LINE__);

#define DB_INTERNAL_ERROR() \
	DB_AND_LOG("WARNING: Server state error " << __FILE__ << ": " << __LINE__);

#define STRNCPY(DEST, SOURCE, COUNT) \
	{ strncpy(DEST, SOURCE, (COUNT)-1); (DEST)[(COUNT)-1] = '\0'; }

#define STRCPY(DEST, SOURCE) \
	STRNCPY(DEST, SOURCE, sizeof(DEST))

#define NULL_TERMINATE(X) \
		X[sizeof(X)-1] = '\0'

#define NULL_STRING(X) \
		X[0] = '\0'

#define MIN(X, Y) \
	((X) < (Y) ? (X) : (Y))

#define MAX(X, Y) \
	((X) > (Y) ? (X) : (Y))

#define FIND(ITER, CONTAINER, ARG, ERROR_HANDLER) \
	ITER = CONTAINER.find(ARG); \
	if (ITER == CONTAINER.end()) \
	{ \
		INTERNAL_ERROR(); \
		ERROR_HANDLER; \
	}

int RunAsDaemon(const char* stdoutname);
int write_pid(const char* sFileName);
bool file_exist(const char* sFileName);

inline int int_max(int a, int b) {
    b = a-b;
    a -= b & (b>>31);
    return a;
}

inline int int_min(int a, int b) {
    b = b-a;
    a += b & (b>>31);
    return a;
}

#endif // EC_UTIL_H

