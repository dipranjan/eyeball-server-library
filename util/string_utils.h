

/*
  $Id: string_utils.h,v 1.5 2006/08/21 16:28:39 zionk Exp $
*/

#ifndef _UTILS_H
#define _UTILS_H

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

#include <stdio.h>


// Convert an int to a string
inline std::string itos(const int i)
{
  std::string s;
  char buffer[16];//std::stringstream s;
  sprintf(buffer,"%d",i);
  s = buffer;
  return s;
}

// Convert a long long to a string
inline std::string lltos(const long long i)
{
  std::string s;
  char buffer[32];//std::stringstream s;
  sprintf(buffer,"%lld",i);
  s = buffer;
  return s;
}


// Convert a double to a string
inline std::string dtos(const double i)
{
  std::stringstream s;
  s << i;
  return s.str();
}

// Convert an int to a string
inline std::string& string_append_int(std::string& s, const int i)
{
  char buffer[16];//std::stringstream s;
  sprintf(buffer,"%d",i);
  s += buffer;
  return s;
}

// Convert a long long to a string
inline std::string& string_append_long_long(std::string& s, const long long i)
{
  char buffer[32];//std::stringstream s;
  sprintf(buffer,"%lld",i);
  s += buffer;
  return s;
}


// Convert a double to a string
inline std::string& string_append_double(std::string& s, const double i)
{
  std::stringstream ss;
  ss << i;
  s += ss.str();
  return s;
}

std::string& string_tolower(std::string& s);
std::string& string_toupper(std::string& s);
bool string_case_equal(const std::string& s1, const std::string& s2);

std::string& string_time_random16(std::string& s);


// check if a string contains a particular substring.
char* findSubstring(char *string/* string to search. */, char *substring/* substring to find */);

// address handling helper functions
int GetServerPort(const std::string& sAddressPort);
std::string GetServerAddrString(const std::string& sAddressPort);
int GetServerAddr(const std::string& sAddressPort);
std::string GetIpByHostnameOrIp(std::string sAddress);

#endif

/*
  $Log:
*/

