//Encrypt and decrypt password

#include "Password3DES.h"
#include <stdio.h>
#include "BinHex.h"

#include <string>
using std::string;

#define BUF_SIZE 512

//#define DEBUG

void read_keys_from_file(const char* szFile,
	string& sKey1, string& sKey2, string& sKey3)
{
  sKey1.resize(BUF_SIZE);
  sKey2.resize(BUF_SIZE);
  sKey3.resize(BUF_SIZE);

  // access key file - note: does not handle errors
  FILE* key_file = fopen(szFile, "r");
  if (NULL == key_file)
    return;

  string sBuf;
  sBuf.resize(BUF_SIZE);
  string file_data;

  while (!feof(key_file))
  {
    int nBytes = fread(&sBuf[0], sizeof(char), sBuf.size(), key_file);
    file_data.append(sBuf.data(), nBytes);
  }
  file_data += '\0';  // make sure string is null-terminated
#ifdef DEBUG
  printf("file read:\n%s\n---\n\n", file_data.c_str());
#endif //DEBUG

  string sKeyData = file_data.substr(file_data.find("pass_3des_key1"));
  sscanf(sKeyData.c_str(), "pass_3des_key1 = %s", &sKey1[0]);
  sKeyData = file_data.substr(file_data.find("pass_3des_key2"));
  sscanf(sKeyData.c_str(), "pass_3des_key2 = %s", &sKey2[0]);
  sKeyData = file_data.substr(file_data.find("pass_3des_key3"));
  sscanf(sKeyData.c_str(), "pass_3des_key3 = %s", &sKey3[0]);

  fclose(key_file);
}

int main(int argc, char** argv)
{
  if (argc < 4) {
    printf("Not enough arguments, only %d\n", argc);
    printf("Usage: pass3des <key123-file> <username> <password> [decrypt]\n");
    return -1;
  }

  bool bEncrypt = (argc == 4);

  // read keys
  string sKey[3];

  read_keys_from_file(argv[1], sKey[0], sKey[1], sKey[2]);

  // for convenience...
  const char* szKey1 = sKey[0].c_str();
  const char* szKey2 = sKey[1].c_str();
  const char* szKey3 = sKey[2].c_str();

#ifdef DEBUG
  printf("Key: %s %s %s\n", szKey1, szKey2, szKey3);
#endif //DEBUG

  //Check keys
  unsigned char binKey[8];
  if (!check_3des_key_hex_and_parity(szKey1, binKey)) {
    printf("key1-hex %s is bad key\n", szKey1);
    printf("Usage: pass3des <key123-file> <username> <password> [decrypt]\n");
    return -2;
  }

  if (!check_3des_key_hex_and_parity(szKey2, binKey)) {
    printf("key2-hex %s is bad key\n", szKey2);
    printf("Usage: pass3des <key123-file> <username> <password> [decrypt]\n");
    return -3;
  }

  if (!check_3des_key_hex_and_parity(szKey3, binKey)) {
    printf("key3-hex %s is bad key\n", szKey2);
    printf("Usage: pass3des <key123-file> <username> <password> [decrypt]\n");
    return -4;
  }

  if (bEncrypt) {
    string sOutput;
    if (!password_3des(szKey1, szKey2, szKey3, argv[2], argv[3], sOutput, true)) {
      printf("Failed to encrypt password\n");
      return -5;
    }

    string sOutputHex;
    sOutputHex.resize(sOutput.size() << 1);
    convertBinToHex((unsigned char*)sOutput.data(), sOutput.size(), 
                    (unsigned char*)sOutputHex.data());
    printf("%s\n", sOutputHex.c_str());
  }
  else {
    string sHexInput(argv[3]);
    string sBinInput;
    sBinInput.resize(sHexInput.size() >> 1);

    if (!convertHexToBin((unsigned char*)sHexInput.data(), sHexInput.size(),
                         (unsigned char*)sBinInput.data()))
    {
      printf("Bad hexadecimal password\n");
      return -6;
    }
    else {
      string sOutput;
      if (!password_3des(szKey1, szKey2, szKey3, argv[2], sBinInput, sOutput, false)) {
        printf("Failed to decrypt password\n");
        return -7;
      }
      else {
        printf("%s\n", sOutput.c_str());
      }
    }
  }

  return 0;
}

