// ---------------------------------------------------------------------------
// File:       poll.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Polling assist functions for large servers
//
// ---------------------------------------------------------------------------

#ifndef EC_POLL_H
#define EC_POLL_H

#include <iostream>
#include <sys/poll.h>

using std::ostream;

class PollArray
{
	public:
		PollArray();
		~PollArray();

		struct pollfd *get(void) { return m_aPollFds; }
		int size(void) { return m_iMaxFds; }
		int GetUsedSize() { return m_iMaxUsedFd; }

		void open(void);
		void close(void);

		int add(int _iFd, short _iEvents = POLLIN);
		int modify(int _iFd, short _iEvents);
		int remove(int _iFd);

		friend ostream& operator<<(ostream& s, const PollArray& v);

	private:
		int m_iMaxFds;
		int m_iMaxUsedFd;
		struct pollfd *m_aPollFds;
};

#endif // EC_POLL_H

