//! PeriodicTask2.h
//! Interface for CPeriodicTask2 class

#ifndef PERIODIC_TASK2_H_
#define PERIODIC_TASK2_H_

#include <pthread.h>
#include <unistd.h>

class CPeriodicTask2
{
public:
	//! Constructor
	//! intervalMs is the interval in milliseconds
	CPeriodicTask2(int intervalMs, bool bStartThread = true);

	//! Destructor
	virtual ~CPeriodicTask2();

	//! Start thread
	void Start();

	//! Do Task
	//! Override this
	virtual void DoTask();

	//! Stop
	void Stop();

private:
	//! Loop Thread
	static void* LoopThread(void* arg);	

	int m_iIntervalMs;

	int m_iReadFd;
	int m_iWriteFd;

	pthread_t m_tLoopThread;
	pthread_mutex_t m_StopMutex;
};

#endif //PERIODIC_TASK2_H_

