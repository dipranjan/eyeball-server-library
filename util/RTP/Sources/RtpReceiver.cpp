// ---------------------------------------------------------------------------
// File:       RtpReceiver.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Receive RTP packets
// 
// Change Log:
// ---------------------------------------------------------------------------

#include "Base.h"  // automatic use of precompiled headers 

#include "debug.h"
#include "UdpNetwork.h"
#include "pktbuf.h"
#include "RtpReceiver.h"
#include "Thread.h"
#include "PayloadTypes.h"
#include "EUtil.h"

using std::string;

// ---------------------------------------------------------------------------
// Defines
// ---------------------------------------------------------------------------

// #define DEBUG_RTP


// ---------------------------------------------------------------------------
// RtpReceiver class definition
// ---------------------------------------------------------------------------

RtpReceiver::RtpReceiver()
:
	m_bInitialized(false),

	m_pUdpNetwork(new UdpNetwork),
	m_pPktBuf(new pktbuf),
	m_hNotificationEvent(NULL),

	m_ulSenderAddress(INADDR_NONE),
	m_iSenderPort(0)
{
}

RtpReceiver::~RtpReceiver()
{
	Close();

	SAFE_DELETE(m_pUdpNetwork);
	SAFE_DELETE(m_pPktBuf);
}

BOOL RtpReceiver::Open(HANDLE _notification_event, u_int32_t uAddress, u_int16_t _port)
{
	if (m_bInitialized)
		return false;

	u_int32_t addr = htonl(uAddress);

	if (addr == INADDR_NONE)
	{
		DB("Invalid IP address");
		return false;
	}

	if (!m_pUdpNetwork->Open(addr, 0, htons(_port), _notification_event))
	{
		Close();
		return false;
	}
	//cout<<"RtpReceiver::Open, bound to _port: "<<_port<<endl;
	DB("RtpReceiver::Open address " << intoa(htonl(uAddress)).c_str() << ":" << _port );
	m_ulSenderAddress = addr;
	m_iSenderPort = htons(_port);
	m_bInitialized = true;
	m_hNotificationEvent = _notification_event;

	return true;
}

BOOL RtpReceiver::Close(void)
{
	m_pUdpNetwork->Close();

	m_ulSenderAddress = INADDR_NONE;
	m_iSenderPort = 0;

	m_bInitialized = false;

	return true;
}

BOOL RtpReceiver::GetSharedSocket(SharedPtr<ESocket>& pSocket)
{
	return m_pUdpNetwork->GetSharedSocket(pSocket);
}

BOOL RtpReceiver::SetSharedSocket(SharedPtr<ESocket>& pSocket)
{
	return m_pUdpNetwork->SetSharedSocket(pSocket);
}


bool RtpReceiver::check_format(int fmt) const
{
	switch (fmt)
	{
		case RTCP_PT_SR:
		case RTCP_PT_RR:
		case RTCP_PT_SDES:
		case RTCP_PT_BYE:
		case RTCP_PT_APP:
			// Control packets not allowed on data port
			return false;
	}

	// Otherwise assume OK
	return true;
}

// client side: receive packet from network
BOOL RtpReceiver::ReceivePacket(
	char& _payload_type, 
	u_int16_t& _seqno, 
	u_int32_t& _ts,
	u_int32_t& _ssrc,
	char *_buf, 
	int& _len, 
	const int _size)
{
	u_int32_t from;
	u_int16_t fromport;

	int cc = m_pUdpNetwork->RecvFrom(
		(char *)m_pPktBuf->data, 
		sizeof(m_pPktBuf->data), 
		from, 
		fromport);
	if (cc < 0)
	{
		DB("RtpReceiver - recv error: " << WSAGetLastError());
		return false;
	}

#ifdef DEBUG_RTP
	// for debugging
	DB("RtpReceiver - packet from " << intoa(from).c_str() << ":" << fromport);
#endif

	// Check that the packet came from our intended destination.  Check is disabled
	// for multicast reception.
/*	if (!IN_MULTICAST(ntohl(m_ulSenderAddress)))
	{
		if (from != m_ulSenderAddress)
		{
			DB("RtpReceiver - Received packet from unexpected address");
			return false;
		}
	}
*/
	m_iSenderPort = fromport;

	int payload_size = cc - sizeof(rtphdr);

	if (payload_size < 0)
	{
		DB("  ==> packet too small");
		//++nrunt_;
		// Runt packets are used to open UDP conduits through firewalls
		return false;
	}

	if (payload_size > _size)
	{
		DB("  ==> packet too big for given buffer");
		return false;
	}

	int version = m_pPktBuf->data[0] >> 6;
	if (version != 2)
	{
		DB("  ==> bad version");
		//++badversion_;
		return false;
	}

	m_pPktBuf->len = cc;

#ifndef _WIN32_WCE
	const rtphdr *rh = (rtphdr *)m_pPktBuf->data;

	//u_int32_t srcid = ntohl(rh->rh_ssrc);
	int flags = ntohs(rh->rh_flags);
	
	if ((flags & RTP_X) != 0)
	{
		 // The minimal-control audio/video profile explicitly forbids extensions
		 DB("  ==> bad extension");
		//++badext_;
		return false;
	}

	// Check for illegal payload types.  Most likely this is a session packet arriving
	// on the data port.
	int fmt = flags & 0x7f;
	if (!check_format(fmt))
	{
		DB("  ==> bad format");
		//++badfmt_;
		return false;
	}

	_payload_type = flags & 0x7f;			// media payload type
	_seqno = ntohs(rh->rh_seqno);			// packet sequence number

	_ts = ntohl(rh->rh_ts);					// media-specific time stamp
	_ssrc = ntohl(rh->rh_ssrc);

	int nCC = (flags & 0x0F00) >> 8;		// the CSRC count
	payload_size -= nCC*sizeof(u_int32_t);	// modify the payload to exclude the CSRCs
	if (payload_size < 0)
	{
		DB("  ==> packet too small");
		//++nrunt_;
		// Runt packets are used to open UDP conduits through firewalls
		return false;
	}

	// obtain CSRC from the rtp header and put it in the queue
	u_int32_t* pCSRC = (u_int32_t *) (rh + 1);
	for (int i=0; i<nCC; i++)
	{
		m_CSRC.push(ntohl(*pCSRC));
		pCSRC++;
	}

	memcpy(_buf, (char*) pCSRC, payload_size);
#else
	rtphdr hdr;
	memcpy(&hdr, m_pPktBuf->data, sizeof(rtphdr));

	int flags = ntohs(hdr.rh_flags);
	_payload_type = flags & 0x7f;			// media payload type
	_seqno = ntohs(hdr.rh_seqno);			// packet sequence number
	_ts = ntohl(hdr.rh_ts);					// media-specific time stamp
	_ssrc = ntohl(hdr.rh_ssrc);

	memcpy(_buf, m_pPktBuf->data + sizeof(rtphdr), payload_size);
#endif
	_len = payload_size;

	return true;
}



void RtpReceiver::SendAck(u_int16_t uSeqNo[], int iNumber)
{
	DB_ASSERT(iNumber < 256);

	BYTE szBuf[512];

	szBuf[0] = UDP_PT_ACKNOWLEDGEMENT;

	for (int i = 0; i < iNumber; i++)
	{
		u_int16_t uData = htons(uSeqNo[i]);

		memcpy(szBuf + 1 + 2 * i, &uData, 2);
	}


	m_pUdpNetwork->SendTo((const char*)szBuf, 
		sizeof(WORD)*iNumber + 1, 
		m_ulSenderAddress, 
		m_iSenderPort);
}

void RtpReceiver::SendAck(const string& sBuf)
{
	m_pUdpNetwork->SendTo(sBuf.data(), sBuf.size(), m_ulSenderAddress, m_iSenderPort);
}


#if 0
// From SupraVista, may be useful
bool RtpReceiver::CheckForRecv(SOCKET s, int msec)
{
	fd_set readfds;
	struct timeval tval;  

	tval.tv_sec = msec / 1000; // set tval to TIME_OUT_VAL sec
	tval.tv_usec = (msec % 1000) * 1000; 

	FD_ZERO(&readfds);
	FD_SET(s, &readfds);

	if (select(0, &readfds, NULL, NULL, &tval) != 1)
	{
		printf("select:error = %d\n", WSAGetLastError());
		return false;
	}

	return true;
}
#endif

void RtpReceiver::GetCSRC(std::queue<u_int32_t>& qCSRC)
{
	while (!m_CSRC.empty())
	{
		qCSRC.push(m_CSRC.front());
		m_CSRC.pop();
	}
}
