// ---------------------------------------------------------------------------
// File:       HttpRtpReceiver.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Receive RTP packets
// 
// Change Log:
// ---------------------------------------------------------------------------

#include "Base.h"  // automatic use of precompiled headers 

#include "debug.h"
#include "ESocket.h"
#include "pktbuf.h"
#include "HttpRtpReceiver.h"
#include "Thread.h"
#include "PayloadTypes.h"
#include "EUtil.h"

using std::string;

// ---------------------------------------------------------------------------
// HttpRtpReceiver class definition
// ---------------------------------------------------------------------------

// extract the packet from sBuf; iSize represents the received packet size
BOOL HttpRtpReceiver::ReceivePacket(
	const string& sBuf,
	int iBufSize,
	char& _payload_type, 
	u_int16_t& _seqno, 
	u_int32_t& _ts,
	u_int32_t& _ssrc,
	char *_buf, 
	int& _len, 
	const int _size)
{
	if (iBufSize < 0)
	{
		return false;
	}

	int iCopySize = sizeof(m_pPktBuf->data) > iBufSize ? iBufSize : sizeof(m_pPktBuf->data);
	memcpy((char*) m_pPktBuf->data, sBuf.data(), iCopySize);

	int payload_size = iBufSize - sizeof(rtphdr);

	if (payload_size < 0)
	{
		//DB("  ==> packet too small");
		return false;
	}

	if (payload_size > _size)
	{
		DB("  ==> packet too big for given buffer");
		return false;
	}

	int version = m_pPktBuf->data[0] >> 6;
	if (version != 2)
	{
		DB("  ==> bad version");
		//++badversion_;
		return false;
	}

	m_pPktBuf->len = iBufSize;

#ifndef _WIN32_WCE
	const rtphdr *rh = (rtphdr *)m_pPktBuf->data;

	//u_int32_t srcid = ntohl(rh->rh_ssrc);
	int flags = ntohs(rh->rh_flags);
	
	if ((flags & RTP_X) != 0)
	{
		 // The minimal-control audio/video profile explicitly forbids extensions
		 DB("  ==> bad extension");
		//++badext_;
		return false;
	}

	// Check for illegal payload types.  Most likely this is a session packet arriving
	// on the data port.
	int fmt = flags & 0x7f;
	if (!check_format(fmt))
	{
		DB("  ==> bad format");
		//++badfmt_;
		return false;
	}

	_payload_type = flags & 0x7f;			// media payload type
	_seqno = ntohs(rh->rh_seqno);			// packet sequence number

	_ts = ntohl(rh->rh_ts);					// media-specific time stamp
	_ssrc = ntohl(rh->rh_ssrc);

	int nCC = (flags & 0x0F00) >> 8;		// the CSRC count
	payload_size -= nCC*sizeof(u_int32_t);	// modify the payload to exclude the CSRCs
	if (payload_size < 0)
	{
		//DB("  ==> packet too small");
		return false;
	}

	// obtain CSRC from the rtp header and put it in the queue
	u_int32_t* pCSRC = (u_int32_t *) (rh + 1);
	for (int i=0; i<nCC; i++)
	{
		m_CSRC.push(ntohl(*pCSRC));
		pCSRC++;
	}

	memcpy(_buf, (char*) pCSRC, payload_size);
#else
	rtphdr hdr;
	memcpy(&hdr, m_pPktBuf->data, sizeof(rtphdr));

	int flags = ntohs(hdr.rh_flags);
	_payload_type = flags & 0x7f;			// media payload type
	_seqno = ntohs(hdr.rh_seqno);			// packet sequence number
	_ts = ntohl(hdr.rh_ts);					// media-specific time stamp
	_ssrc = ntohl(hdr.rh_ssrc);

	memcpy(_buf, m_pPktBuf->data + sizeof(rtphdr), payload_size);
#endif
	_len = payload_size;

	return true;
}
