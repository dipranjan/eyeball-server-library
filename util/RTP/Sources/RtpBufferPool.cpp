// ---------------------------------------------------------------------------
// File:       RtpBufferPool.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    
// 
// Change Log:
// ---------------------------------------------------------------------------

#include "Base.h"  // automatic use of precompiled headers 

#include "config.h"
#include "pktbuf.h"
#include "RtpBase.h"
#include "RtpBufferPool.h"
#if 1 //for Linux yamada
#include <netinet/in.h>	// htons, ntohs
#endif //for Linux yamada
	
// July 13: We use sequence numbering on a per layer basis.
// This helps to detect packet loss at the client end
// because, otherwise the client sees discontinuities in
// sequence numbering 

RtpBufferPool::RtpBufferPool() 
{
	srcid_= u_int(-1);

	for(int i=0;i<NLAYER;i++)
		seqno_[i] = 1;
}		


RtpBufferPool::~RtpBufferPool() 
{
}	

void RtpBufferPool::decrement_seqno(int layer)
{
	seqno_[layer] = seqno_[layer] - 1;
}

void RtpBufferPool::seqno(int layer, u_int16_t s)
{
	seqno_[layer] = s;
}

pktbuf* RtpBufferPool::alloc(
	u_int32_t ts, 
	int fmt, 
	u_int32_t ssrc, 
	int cc,
	int layer, int marker)
{
	pktbuf* pb = BufferPool::alloc(layer);

	/*unsigned long ccc =(unsigned long)pb;
	
	if(ccc>0x40000000&&ccc<0x50000000)
		printf("RtpBufferPool::alloc the value  of pb in ulong = %ul and ptr = %p \n",ccc,pb);
	*/
	
	initpkt(pb, ts, fmt, ssrc, cc, layer,marker); // add seq no, timestamp and RTP fields
	return (pb);
}

void RtpBufferPool::initpkt(
	pktbuf *pb, 
	u_int32_t ts, 
	int fmt, 
	u_int32_t ssrc, 
	int cc,
	int layer, int marker)
{
	//pb->layer = htonl(layer);
	//int flags = RTP_VERSION << 14 | cc << 8 | fmt;
	int flags = RTP_VERSION << 14 | cc << 8 | marker << 7 | fmt;
#ifndef _WIN32_WCE	
	rtphdr* rh = (rtphdr*)pb->data;
	rh->rh_flags = htons(flags);
	rh->rh_seqno = htons(seqno_[layer]);
	rh->rh_ts = htonl(ts); 
	rh->rh_ssrc = htonl(ssrc); //srcid_;
#else
	rtphdr hdr;
	hdr.rh_flags = htons(flags);
	hdr.rh_seqno = htons(seqno_[layer]);;
	hdr.rh_ts = htonl(ts);
	hdr.rh_ssrc = htonl(ssrc);
	memcpy(pb->data, &hdr, sizeof(rtphdr));
#endif

	seqno_[layer] = seqno_[layer]+1;
}

void RtpBufferPool::reset() 
{
	//srcid_= u_int(-1);

	for(int i=0;i<NLAYER;i++)
		seqno_[i] = 1;
}	

u_int16_t RtpBufferPool::next_seqno(int layer)
{
	return seqno_[layer];
}
