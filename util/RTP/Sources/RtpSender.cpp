// ---------------------------------------------------------------------------
// File:       RtpSender.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    
// 
// Change Log:
// ---------------------------------------------------------------------------

#include "Base.h"  // automatic use of precompiled headers 

#include "critsec.h"
#include "debug.h"
#include "ESocket.h"
#include "UdpNetwork.h"
#include "RtpBufferPool.h"
#include "PayloadTypes.h"
#include "RtpSender.h"
#include "EUtil.h"

#ifdef LOG_PACKET_SEND_TIME
#include "TextUtil.h"
#endif

using std::queue;

// ---------------------------------------------------------------------------
// Defines
// ---------------------------------------------------------------------------

// Packet time to live for UdpNetwork::open()
#define TTL		5

#ifdef LOG_PACKET_SEND_TIME
static bool s_iLogIndex = 0;	// only 1 AudioReceiver is logged
#endif

// ---------------------------------------------------------------------------
// RtpSender class implementation
// ---------------------------------------------------------------------------

RtpSender::RtpSender()
:
	m_pMutex(new Mutex),

	m_bInitialized(false),
	m_chPayloadType(0),		// this is already PCM, but oh well.
	m_hPacketEvent(NULL),
	m_uStreamSSRC(0),
	m_bOpen(false),
	m_layers(),
	m_pBufferPool(new RtpBufferPool),
#ifdef LOG_PACKET_SEND_TIME
	m_fpLog(NULL),
	m_nNumPacket(0),
	m_nInitTime(0),
#endif
	m_pSocket(NULL)
{
	ClearCSRC();
}

RtpSender::~RtpSender()
{
	
	Close();
	//printf("closed rtpsender\n");
	SAFE_DELETE(m_pBufferPool);
	//printf("safe deleted buffer pool\n");
	SAFE_DELETE(m_pMutex);
	//printf("After deleteing rtp sender mutex\n");
	
}

BOOL RtpSender::Open(char chPayloadType, HANDLE hPacketEvent)
{
	Critical_Section critsec(*m_pMutex);

	if (m_bInitialized)
		return false;

	m_chPayloadType = chPayloadType;
	m_hPacketEvent = hPacketEvent;
	m_bInitialized = true;

#ifdef LOG_PACKET_SEND_TIME
	s_iLogIndex++;
	tstring sFileName = "c:\\packettimelogsender" + itoa(s_iLogIndex) + ".log";
	m_fpLog = fopen(sFileName.mbs().c_str(), "w");
	if (!m_fpLog)
	{
		DB("Cannot create packet time log (sender).");
	}
#endif

	return true;
}

BOOL RtpSender::Open(char chPayloadType, DWORD dwSSRC,bool isSip)
{
	Critical_Section critsec(*m_pMutex);
	
	//DB("m_bOpen value :"<<m_bOpen);
	
	//printf ("m_bOpen value : %d\n",m_bOpen);
	
	if (m_bOpen)
		return false;

	m_chPayloadType = chPayloadType;
	m_uStreamSSRC = (u_int32_t)dwSSRC;
	m_bOpen = true;
	return true;
}



void RtpSender::Close(void)
{
	Critical_Section critsec(*m_pMutex);

	m_chPayloadType = 0;
	m_layers.clear();
	m_bInitialized = false;
	m_pSocket.reset();

	m_bOpen = false;

	ClearCSRC();

#ifdef LOG_PACKET_SEND_TIME
	if (m_fpLog)
	{
		fclose(m_fpLog);
	}
#endif
}

void RtpSender::ChangePayloadType(char chPayloadType)
{
	m_chPayloadType = chPayloadType;

}

void RtpSender::SetStreamSSRC(u_int32_t uSSRC)
{
	m_uStreamSSRC = uSSRC;
}


BOOL RtpSender::AddTarget(u_int32_t uAddress, u_int16_t uPort)
{
	Critical_Section critsec(*m_pMutex);

	DB_ASSERT(uAddress && uPort);
	// address and port in host byte order
	AddressPortPair app;
	app.first = uAddress;
	app.second = uPort;

	NetworkLayers::iterator nlayer = m_layers.find(app);
	if (nlayer != m_layers.end())
	{
		DB_ASSERT(0 && "Address and port app already exists.");
		return false;
	}

	DB("RtpSender::AddTarget - [" << intoa(htonl(uAddress)).c_str() << ", " << uPort << "]");
	
	SharedPtr<UdpNetwork> pNetwork(new UdpNetwork);

	// if m_pSocket is NULL, pNetwork->Open will open another socket to use
	//DB_ASSERT(m_pSocket.get());

	if (m_pSocket.get())
	{
		pNetwork->SetSharedSocket(m_pSocket);
	}

	// open sending socket
	if (!pNetwork->Open(htonl(uAddress), htons(uPort), 0, m_hPacketEvent))
	{
		pNetwork->Close();
		return false;
	}

	m_layers[app] = pNetwork;

	return true;
}

BOOL RtpSender::AddTarget(DWORD uAddress, WORD uPort,bool isSip)
{
	Critical_Section critsec(*m_pMutex);

	// address and port in host byte order
	if (TargetExists(uAddress, uPort))
	{
		DB_ASSERT(0 && "Address and port app already exists.");
		return false;
	}

	AddressPortPair app;
	app.first = htonl(uAddress);
	app.second = htons(uPort);

	DB("RtpSender::AddTarget - [" << intoa(htonl(uAddress)).c_str() << ", " << uPort << "]");
	m_targetMap[app] = app;
	return true;
}


BOOL RtpSender::DropTarget(u_int32_t uAddress, u_int16_t uPort)
{
	Critical_Section critsec(*m_pMutex);

	AddressPortPair app;
	app.first = uAddress;
	app.second = uPort;

	NetworkLayers::iterator nlayer = m_layers.find(app);
	if (nlayer == m_layers.end())
	{
		// Layer doesn't exist
		DB("Address and port pair doesn't exist.");
		return false;
	}
	DB("RtpSender::DropTarget - removing [" << intoa(htonl(uAddress)).c_str() << ", " << uPort << "]");

	// close network;
	(nlayer->second)->Close();

	m_layers.erase(app);

	return true;
}


BOOL RtpSender::DropTarget(DWORD uAddress, WORD uPort,bool isSip)
{
	Critical_Section critsec(*m_pMutex);

	if (!TargetExists(uAddress, uPort))
	{
		// Layer doesn't exist
		DB("Address and port pair doesn't exist.");
		return false;
	}

	AddressPortPair app;
	app.first = htonl(uAddress);
	app.second = htons(uPort);


	DB("RtpSender::DropTarget [" << intoa(htonl(uAddress)).c_str() << ", " << uPort << "]");
	m_targetMap.erase(app);
	return true;
}


BOOL RtpSender::TargetExists(DWORD uAddress, WORD uPort)
{
	Critical_Section critsec(*m_pMutex);

	AddressPortPair app;
	app.first = htonl(uAddress);
	app.second = htons(uPort);

	return m_targetMap.find(app) != m_targetMap.end();
}

void RtpSender::UpdateTarget(DWORD uAddress, WORD uPort, DWORD dwNewAddr, WORD wNewPort)
{
	Critical_Section critsec(*m_pMutex);
	if (!TargetExists(uAddress, uPort))
		return;

	AddressPortPair app;
	app.first = htonl(uAddress);
	app.second = htons(uPort);

	AddressPortPair appReal;
	appReal.first = htonl(dwNewAddr);
	appReal.second = htons(wNewPort);

	m_targetMap.erase(app);
	m_targetMap[app] = appReal;
}



BOOL RtpSender::Send(char *_buf, int _len, u_int32_t _ts)
{
	Critical_Section critsec(*m_pMutex);


#ifndef _WIN32_WCE
	// CSRC count
	int cc = m_CSRC.size();
	if (cc > MAX_CSRC)
	{
		cc = MAX_CSRC;
	}
	// size of CSRCs
	int nSizeCSRC = cc*sizeof(u_int32_t);

	if (_len > MAX_PAYLOAD_SIZE - nSizeCSRC)
	{
		DB("RtpSender::send - attempt to send packet > MAX_PAYLOAD_SIZE");
		return false;
	}

	pktbuf *pb = m_pBufferPool->alloc(_ts, m_chPayloadType, m_uStreamSSRC, cc);
	pb->len = _len + sizeof(rtphdr) + nSizeCSRC;

	rtphdr *rh = (rtphdr *)(pb->data);

	//++++++++++++++++++++++++++++++++++++++++
	if(pb->data[0] != 128)
	{
		cout<<"[ _WIN32_WCE ] Error !!! RTP Header first byte != 128 , found value = " <<pb->data[0]<<endl;
		pb->release();
		return false;
	}
	//++++++++++++++++++++++++++++++++++++++++


	u_int32_t* pCSRC = (u_int32_t *) (rh + 1);
	for (int i=0; i<cc; i++)
	{
		*pCSRC = htonl(m_CSRC.front());
		pCSRC++;
		m_CSRC.pop();
	}

	char *data = (char *)(pCSRC);

	memcpy(data, _buf, _len);

#else
	if (_len > MAX_PAYLOAD_SIZE)
	{
		DB("RtpSender::send - attempt to send packet > MAX_PAYLOAD_SIZE");
		return false;
	}

	pktbuf *pb = m_pBufferPool->alloc(_ts, m_chPayloadType, m_uStreamSSRC, 0);
	
	if(pb == NULL)
	{
		printf("pb = NULL in BOOL RtpSender::Send(char *_buf, int _len, u_int32_t _ts) \n");
	}
	pb->len = _len + sizeof(rtphdr);

	rtphdr *rh = (rtphdr *)(pb->data);
	
	if(rh == NULL)
	{
		printf("rh = NULL in BOOL RtpSender::Send(char *_buf, int _len, u_int32_t _ts) \n");
	}

	//++++++++++++++++++++++++++++++++++++++++
	#if 0
	printf("Header information : \n");
	printf("Size of rtp header  : %d\n",sizeof(rtphdr));
	printf("Packet Length : %d\n",pb->len);

	for(int headerCounter = 0;headerCounter < sizeof(rtphdr) ; headerCounter++)
	{
		printf("%d \n",pb->data[headerCounter]);
	}
	#endif
	
	if(pb->data == NULL)
	{
		printf("pb->data = NULL in BOOL RtpSender::Send(char *_buf, int _len, u_int32_t _ts) \n");
	}
	
	if(pb->data[0] != 128)
	{
		int static headerError=0;
		headerError++;
		if(headerError%100==0)
		{
			cout<<"Error !!! RTP Header first byte != 128 , found value = " <<pb->data[0]<<endl;
		}

		pb->release();
		return false;
	}
	//++++++++++++++++++++++++++++++++++++++++
	char *data = (char *)(rh + 1);
	memcpy(data, _buf, _len);
#endif

	NetworkLayers::iterator nlayer = m_layers.begin();

	for (; nlayer != m_layers.end(); nlayer++)
	{
#ifdef LOG_PACKET_SEND_TIME
		if (m_fpLog && nlayer == m_layers.begin())
		{
			// log only the first one
			m_nNumPacket++;
			if (m_nNumPacket == 10)
			{
				m_nInitTime = GetTickCount();
			}
			fprintf(m_fpLog, "%d\t%d\n", m_pBufferPool->next_seqno()-1, GetTickCount() - m_nInitTime);
		}
#endif
		(nlayer->second)->Send(pb);
	}

	if(pb == NULL)
	{
		//printf("");
		printf(" before release pb= NULL in BOOL RtpSender::Send(char *_buf, int _len, u_int32_t _ts) \n");
	}
	pb->release();

	return true;
}

BOOL RtpSender::Send(char *_buf, int _len, DWORD _ts,bool isSip)
{
	Critical_Section critsec(*m_pMutex);

	if (_len > MAX_PAYLOAD_SIZE)
	{
		DB("RtpSender::send - attempt to send packet > MAX_PAYLOAD_SIZE");
		return false;
	}

	pktbuf *pb = m_pBufferPool->alloc(_ts, m_chPayloadType, m_uStreamSSRC, 0);
	pb->len = _len + sizeof(rtphdr);

	rtphdr *rh = (rtphdr *)(pb->data);

	char *data = (char *)(rh + 1);
	memcpy(data, _buf, _len);

	TargetMap::iterator nlayer = m_targetMap.begin();
	for (; nlayer != m_targetMap.end(); nlayer++)
	{
		AddressPortPair& addr = nlayer->second;
		m_pSocket->SendTo((const char*)pb->data, pb->len, addr);
	}

	pb->release();
	return true;
}


BOOL RtpSender::Package(
	char* _buf,
	int _len, 
	u_int32_t _ts,
	pktbuf** ppPktBuf)
{
	Critical_Section critsec(*m_pMutex);

	if (_len > MAX_PAYLOAD_SIZE)
	{
		DB("RtpSender::send - attempt to send packet > MAX_PAYLOAD_SIZE");
		return false;
	}

	pktbuf *pb = m_pBufferPool->alloc(_ts, m_chPayloadType, m_uStreamSSRC, 0);
	pb->len = _len + sizeof(rtphdr);

	rtphdr *rh = (rtphdr *)(pb->data);

	char *data = (char *)(rh + 1);
	memcpy(data, _buf, _len);

	*ppPktBuf = pb;

	return true;
}


BOOL RtpSender::Package(
	char* _buf,
	int _len, 
	DWORD _ts,
	bool _m,
	pktbuf** ppPktBuf,bool isSip)
{
	Critical_Section critsec(*m_pMutex);

	if (_len > MAX_PAYLOAD_SIZE)
	{
		DB("RtpSender::send - attempt to send packet > MAX_PAYLOAD_SIZE");
		return false;
	}

	pktbuf *pb = m_pBufferPool->alloc(_ts, m_chPayloadType, m_uStreamSSRC, 0, 0, _m);
	pb->len = _len + sizeof(rtphdr);

	rtphdr *rh = (rtphdr *)(pb->data);

	char *data = (char *)(rh + 1);
	memcpy(data, _buf, _len);

	*ppPktBuf = pb;
	return true;
}


BOOL RtpSender::SendTo(
	u_int32_t uAddress,
	u_int16_t uPort,
	pktbuf* pb)
{
	Critical_Section critsec(*m_pMutex);

	AddressPortPair app;
	app.first = uAddress;
	app.second = uPort;

	NetworkLayers::iterator nlayer = m_layers.find(app);
	if (nlayer == m_layers.end())
	{
		// Layer doesn't exist
		DB_ASSERT(0 && "Address and port pair doesn't exist.");
		return false;
	}
	if(!pb)
		printf("We found pb = null in BOOL RtpSender::SendTo(u_int32_t uAddress,u_int16_t uPort,pktbuf* pb)\n");
	
	(nlayer->second)->Send(pb);

	return true;
}

int checkpb(pktbuf* pb)
{
	if(!pb)
	{
		printf("We found pb = null in BOOL RtpSender::SendTo(DWORD uAddress,WORD uPort,pktbuf* pb,bool isSip)\n");
		return -1;
	}
	
	if(!pb->data)
	{
		printf("We found pb->data = null in BOOL RtpSender::SendTo(DWORD uAddress,WORD uPort,pktbuf* pb,bool isSip)\n");
		return -1;
	}		
		
	
	u_int8_t* pCharData=pb->data;
	
	int a;
	a++;
	a=0;
	unsigned long cc =(unsigned long)pb;
	
	if(cc>0xc8000000||cc<0xc0000000)
	{
		printf("RtpSender::checkpb:  The pktbuf ptr is out of memory range = %p, returning without sending packet\n",pb);
		return -1;
	}
	
	unsigned short iPbLen=pb->len;	
	if(iPbLen>1312||iPbLen<12)
	{
		printf("\n\n\n\nWe have got a Send to function call with an odd length = %d=====================================================\n\n\n\n\n",iPbLen);
		return -1;
	}

	return iPbLen;
	
}

BOOL RtpSender::SendTo(
	DWORD uAddress,
	WORD uPort,
	pktbuf* pb,
	bool isSip
	)
{
	Critical_Section critsec(*m_pMutex);

	if (!TargetExists(uAddress, uPort))
	{
		// Layer doesn't exist
		DB_ASSERT(0 && "Address and port pair doesn't exist.");
		printf("Address and port pair doesn't exist in BOOL RtpSender::SendTo(DWORD uAddress,WORD uPort,pktbuf* pb,bool isSip\n");
		return false;
	}

	AddressPortPair app;
	app.first = htonl(uAddress);
	app.second = htons(uPort);

	AddressPortPair& addrReal = m_targetMap[app];
    int abc=checkpb(pb);
	if(abc==-1)
	{
		return false;
	}
	if (m_pSocket.get() == NULL)
	{
		printf("ESocket found null in BOOL RtpSender::SendTo(	DWORD uAddress,	WORD uPort,	pktbuf* pb,	bool isSip)");
		//return false;
	}
/*	if(!pb)
		printf("We found pb = null in BOOL RtpSender::SendTo(DWORD uAddress,WORD uPort,pktbuf* pb,bool isSip)\n");
	
	if(!pb->data)
		printf("We found pb->data = null in BOOL RtpSender::SendTo(DWORD uAddress,WORD uPort,pktbuf* pb,bool isSip)\n");
	
	u_int8_t* pCharData=pb->data;
	
	int a;
	a++;
	a=0;
	
	unsigned short iPbLen=pb->len;
	*/
	m_pSocket->SendTo((const char*)pb->data, pb->len, addrReal);
	//m_pSocket->SendTo((const char*)pCharData, iPbLen, addrReal);
	return true;
}



BOOL RtpSender::SendTo(pktbuf* pb,bool isSipcontact)
{
	
//	DB ("RtpSender::SendTo(pktbuf* pb,bool isSipcontact)");

	if (!isSipcontact)
	{
	Critical_Section critsec(*m_pMutex);

	NetworkLayers::iterator nlayer = m_layers.begin();
	
	for (; nlayer != m_layers.end(); nlayer++)
	{
		(nlayer->second)->Send(pb);
	}

	return true;
	}
	else
	{
	Critical_Section critsec(*m_pMutex);

	TargetMap::iterator nlayer = m_targetMap.begin();
	
	for (; nlayer != m_targetMap.end(); nlayer++)
	{
		AddressPortPair& addr = nlayer->second;
		m_pSocket->SendTo((const char*)pb->data, pb->len, addr);  //adopted from sip
	}

	return true;	
	}
	
	
	
	
	
}


BOOL RtpSender::GetSharedSocket(SharedPtr<ESocket>& pSocket)
{ 
	pSocket = m_pSocket;
	return TRUE;
}

BOOL RtpSender::SetSharedSocket(SharedPtr<ESocket>& pSocket)
{ 
	m_pSocket = pSocket;
	return TRUE;
}


bool RtpSender::HandleNetworkFeedback(void)
{
	if (m_pSocket.get() == NULL)
		return false;

	// used by audio sender
	char buf[512];
	u_int32_t uAddr = 0;
	u_int16_t uPort = 0;
	int iLen = 0;
	if ((iLen = m_pSocket->RecvFrom(buf, sizeof(buf), uAddr, uPort)) < 0)
		return false;

	if ((0 == uAddr) || (0 == uPort) || (iLen < 7))
		return false;
	
	NetworkLayers::iterator nlayer;

	if (buf[0] == UDP_PT_FIREWALL)
	{
		//DB("Firewall packet on the audio sender's port.");

		AddressPortPair app;
		app.first = ntohl(*(DWORD*)(buf+1));
		app.second = ntohs(*(WORD*)(buf+5));
		nlayer = m_layers.find(app);

		if (nlayer != m_layers.end())
		{
			(nlayer->second)->CheckRemotePort(uAddr, uPort);
		}
		else
		{
			DB("HandleNetworkFeeback: Firewall packet from unexpected Addr: " << (LookupHostName(app.first)).c_str() << ", Port: " << app.second);
		}
	}
	else
	{
		DB("HandleNetworkFeeback: Unknown packet from Addr: " << (LookupHostName(uAddr)).c_str() << ", Port: " << ntohs(uPort));
	}

	return true;
}


u_int16_t RtpSender::HandleNetworkFeedback(
	u_int32_t uAddr, // network byte order
	u_int16_t uPort, // network byte order
	const char* pBuf,
	int iLen)
{
	if (iLen <= 0)
		return 0;

	// handle PAT-firewall compatibility
	NetworkLayers::iterator nlayer;
	
	if (iLen >= 7 && pBuf[0] == UDP_PT_FIREWALL)
	{
		//DB("Firewall packet on the video sender's port.");
		AddressPortPair app;
		app.first = ntohl(*(DWORD*)(pBuf+1));
		app.second = ntohs(*(WORD*)(pBuf+5));
		
		nlayer = m_layers.find(app);

		if (nlayer != m_layers.end())
		{
			(nlayer->second)->CheckRemotePort(uAddr, uPort);
		}
		else
		{
			DB("HandleNetworkFeedback2: Firewall packet from unexpected Addr: " << (LookupHostName(app.first)).c_str() << ", Port: " << app.second);
		}

		// this is the firewall data instead of acknowledgement data
		return 0;
	}
	else if (iLen > 0 && (pBuf[0] == UDP_PT_ACKNOWLEDGEMENT || pBuf[0] == UDP_PT_P2PPROXY_ACK))
	{
		nlayer = m_layers.begin();

		for (; nlayer != m_layers.end(); nlayer++)
		{
			if ((nlayer->second)->GetRemoteAddr() == uAddr && 
				(nlayer->second)->GetDetectedPort() == uPort)
			{
				return (nlayer->second)->GetRemotePort();
			}
		}

		return 0;
	}
	else
	{
		DB("HandleNetworkFeedback2: unknown packet from Addr: " << (LookupHostName(uAddr)).c_str() << ", Port: " << ntohs(uPort));
	}


	return 0;
}


u_int16_t RtpSender::NextSeqNo(int layer)
{
	return m_pBufferPool->next_seqno(layer);
}

bool RtpSender::AddCSRC(u_int32_t uSSRC)
{
	Critical_Section critsec(*m_pMutex);

	if (m_CSRC.size() >= MAX_CSRC)
	{
		return false;
	}

	m_CSRC.push(uSSRC);
	return true;
}

void RtpSender::ClearCSRC()
{
	Critical_Section critsec(*m_pMutex);

	while (!m_CSRC.empty())
	{
		m_CSRC.pop();
	}
}


WORD RtpSender::GetExpectedPort(DWORD dwRealAddr, WORD wRealPort)
{
	AddressPortPair addr;
	addr.first = htonl(dwRealAddr);
	addr.second = htons(wRealPort);

	Critical_Section critsec(*m_pMutex);

	TargetMap::iterator iter = m_targetMap.begin();
	for (; iter != m_targetMap.end(); iter++)
	{
		if (iter->second == addr)
		{
			return ntohs((iter->first).second);
		}
	}
	return 0;
}
