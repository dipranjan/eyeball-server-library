// ---------------------------------------------------------------------------
// File:       HttpRtpSender.cpp
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    
// 
// Change Log:
// ---------------------------------------------------------------------------

#include "Base.h"  // automatic use of precompiled headers 

#include "critsec.h"
#include "ESocket.h"
#include "HttpMessage.h"
#include "RtpBufferPool.h"
#include "PayloadTypes.h"
#include "HttpRtpSender.h"
#include "EUtil.h"

// ---------------------------------------------------------------------------
// HttpRtpSender class implementation
// ---------------------------------------------------------------------------

HttpRtpSender::HttpRtpSender()
:
	m_pMutex(new Mutex),
	m_bSend(false),
	m_bIsAudio(true),
	m_pNetwork(NULL),
	m_SendMethod(NULL)
{
}

HttpRtpSender::~HttpRtpSender()
{
	Close();

	SAFE_DELETE(m_pMutex);
}

BOOL HttpRtpSender::Open(char chPayloadType, HANDLE hPacketEvent)
{
	RtpSender::Open(chPayloadType, hPacketEvent);

	return true;
}

void HttpRtpSender::Close(void)
{
	RtpSender::Close();

	Critical_Section critsec(*m_pMutex);
	m_pNetwork.reset();
}

BOOL HttpRtpSender::AddTarget(u_int32_t uAddress, u_int16_t uPort)
{
	m_bSend = true;
	return true;
}

BOOL HttpRtpSender::DropTarget(u_int32_t uAddress, u_int16_t uPort)
{
	m_bSend = false;
	return true;
}


BOOL HttpRtpSender::Send(char *_buf, int _len, u_int32_t _ts)
{
	Critical_Section critsec(*m_pMutex);

	// CSRC count
	int cc = m_CSRC.size();
	if (cc > MAX_CSRC)
	{
		cc = MAX_CSRC;
	}
	// size of CSRCs
	int nSizeCSRC = cc*sizeof(u_int32_t);

	if (_len > MAX_PAYLOAD_SIZE - nSizeCSRC)
	{
		DB("RtpSender::send - attempt to send packet > MAX_PAYLOAD_SIZE");
		return false;
	}

	pktbuf *pb = m_pBufferPool->alloc(_ts, m_chPayloadType, m_uStreamSSRC, cc);
	pb->len = _len + sizeof(rtphdr) + nSizeCSRC;

	rtphdr *rh = (rtphdr *)(pb->data);

	u_int32_t* pCSRC = (u_int32_t *) (rh + 1);
	for (int i=0; i<cc; i++)
	{
		*pCSRC = htonl(m_CSRC.front());
		pCSRC++;
		m_CSRC.pop();
	}

	char *data = (char *)(pCSRC);

	memcpy(data, _buf, _len);
	
#ifdef LOG_PACKET_SEND_TIME
		if (m_fpLog)
		{
			// log only the first one
			m_nNumPacket++;
			if (m_nNumPacket == 10)
			{
				m_nInitTime = GetTickCount();
			}
			fprintf(m_fpLog, "%d\t%d\n", m_pBufferPool->next_seqno()-1, GetTickCount() - m_nInitTime);
		}
#endif

	if (m_bSend)
	{
		DoSend(m_pNetwork, pb);
	}

	pb->release();

	return true;
}

BOOL HttpRtpSender::SendTo(
	u_int32_t uAddress,
	u_int16_t uPort,
	pktbuf* pb)
{
	Critical_Section critsec(*m_pMutex);

	if (!m_bSend)
	{
		return false;
	}

	DoSend(m_pNetwork, pb);

	return true;
}


BOOL HttpRtpSender::SendTo(pktbuf* pb)
{
	Critical_Section critsec(*m_pMutex);

	return SendTo(0, 0, pb);	
}


BOOL HttpRtpSender::GetSharedHttpMessage(SharedPtr<HttpMessage>& pNetwork)
{ 
	pNetwork = m_pNetwork;
	return TRUE;
}

BOOL HttpRtpSender::SetSharedHttpMessage(SharedPtr<HttpMessage>& pNetwork)
{ 
	m_pNetwork = pNetwork;
	return TRUE;
}

void HttpRtpSender::SetIsAudio(bool bIsAudio)
{
	m_bIsAudio = bIsAudio;
}

void HttpRtpSender::SetSendMethod(
	void (*method) (SharedPtr<HttpMessage>& pNetwork, const void* pb, bool bIsAudio))
{
	Critical_Section critsec(*m_pMutex);

	if (method)
	{
		m_SendMethod = method;
	}
}

bool HttpRtpSender::HandleNetworkFeedback(void)
{
	return true;
}

u_int16_t HttpRtpSender::HandleNetworkFeedback(
	u_int32_t uAddr, // network byte order
	u_int16_t uPort, // network byte order
	const char* pBuf,
	int iLen)
{
	if (iLen <= 0)
		return 0;

	// handle PAT-firewall compatibility

	if (iLen >= 7 && pBuf[0] == UDP_PT_FIREWALL)
	{
		//DB("Firewall packet on the video sender's port.");
		if (!m_bSend)
		{
			DB("HandleNetworkFeedback2: Firewall packet from unexpected Addr");
		}

		// this is the firewall data instead of acknowledgement data
		return 0;
	}
	else if (iLen > 0 && pBuf[0] == UDP_PT_ACKNOWLEDGEMENT)
	{
		return uPort;
	}
	else
	{
		DB("HandleNetworkFeedback2: unknown packet from Addr");
	}


	return 0;
}

void HttpRtpSender::DoSend(SharedPtr<HttpMessage>& pNetwork, const pktbuf* pb)
{
	if (!pNetwork.get())
	{
		return;
	}

	if (m_SendMethod)
	{
		// send using the SendMethod if it is available
		(*m_SendMethod)(m_pNetwork, (void*) pb, m_bIsAudio);
	}
	else
	{
		pNetwork->Send((char*) pb->dp, pb->len);
	}
}
