// ---------------------------------------------------------------------------
// File:       RtpBufferPool.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    
// 
// Change Log:
// ---------------------------------------------------------------------------


#ifndef RTP_BUFFER_POOL_H
#define RTP_BUFFER_POOL_H

#include "pktbuf.h"


// ---------------------------------------------------------------------------
// Defines
// ---------------------------------------------------------------------------

#define NLAYER      20


// ---------------------------------------------------------------------------

class RtpBufferPool : public BufferPool
{
public:
	RtpBufferPool();
	~RtpBufferPool();

	void seqno(int layer, u_int16_t s);
	u_int16_t next_seqno(int layer = 0);
	void decrement_seqno(int layer);
	//pktbuf* alloc(u_int32_t ts, int fmt, u_int32_t ssrc, int cc, int layer = 0);
	pktbuf* alloc(u_int32_t ts, int fmt, u_int32_t ssrc, int cc, int layer=0, int marker=0);

	void reset();

protected:
	void initpkt(
		pktbuf *pb, 
		u_int32_t ts, 
		int fmt, 
		u_int32_t ssrc,
		int cc, 
		int layer, int marker=0);

	u_int16_t seqno_[NLAYER];
	u_int32_t srcid_;
};

#endif // RTP_BUFFER_POOL_H
