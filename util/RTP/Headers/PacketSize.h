// ---------------------------------------------------------------------------
// File:       PacketSize.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    
// 
// Change Log:
// ---------------------------------------------------------------------------

#ifndef PACKET_SIZE_H
#define PACKET_SIZE_H

#include "RtpBase.h"

#define MAX_ETHERNET_FRAME		1500

#define IP_HEADER_SIZE			20
#define UDP_HEADER_SIZE			8

#define PKTBUF_SIZE \
	(MAX_ETHERNET_FRAME - (IP_HEADER_SIZE + UDP_HEADER_SIZE))

#define MAX_PAYLOAD_SIZE \
	(PKTBUF_SIZE - sizeof(rtphdr))

#define PACKET_HEADER_SIZE (IP_HEADER_SIZE + UDP_HEADER_SIZE + sizeof(rtphdr))

#endif // PACKET_SIZE_H
