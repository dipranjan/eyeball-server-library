// ---------------------------------------------------------------------------
// File:       RtpSender.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    
// 
// Change Log:
// ---------------------------------------------------------------------------

#ifndef RTP_SENDER_H
#define RTP_SENDER_H

// Disable length warnings from STL
//#pragma warning(disable:4786)

//#include <map>

//#include "SmartPtr.h"
#include "UdpNetwork.h"
#include "utypes.h"		// u_int32_t
#include "ENetwork.h"

#include <queue> //This line is included by Farhan

typedef std::map<AddressPortPair, SharedPtr<UdpNetwork> > NetworkLayers;

class ESocket;
class UdpNetwork;
class Mutex;
class RtpBufferPool;

//#define LOG_PACKET_SEND_TIME	// define to log the packet time for the first rtpsender

/**
 * Sends data using RTP.
 */
class RtpSender : public ISocketContainer
{
public:
    RtpSender();
	virtual ~RtpSender();

	/**
	 * Initiates use of this RtpSender object.
	 *
	 * @param chPayloadType  payload type
	 * @param hNetworkEvent  network event
	 *
	 * @return TRUE on success, FALSE on failure
	 */
	virtual BOOL Open(char chPayloadType, HANDLE hNetworkEvent = NULL);
	virtual BOOL Open(char chPayloadType, DWORD dwSSRC,bool isSip);

	/**
	 * Terminates use of this RtpSender object.
	 *
	 */
	virtual void Close(void);

	/**
	 * Changes the payload type.
	 */
	void ChangePayloadType(char chPayloadType);

	/**
	 * Adds the specified remote as a target.
	 * RtpSender maintains a list of receivers to support multiple clients.
	 *
	 * @param uAddress   IP address of remote (in host byte order)
	 * @param uPort		 UDP port of remote (in host byte order)
	 *
	 * @return TRUE on success, FALSE on failure
	 */
	virtual BOOL AddTarget(u_int32_t uAddress, u_int16_t uPort);
	virtual BOOL AddTarget(DWORD uAddress, WORD uPort,bool isSip);
	
	
	
	
	/**
	 * Drops the specified target.
	 *
	 * @param uAddress   IP address of remote (in host byte order)
	 * @param uPort      UDP port of remote (in host byte order)
	 *
	 * @return TRUE on success, FALSE on failure
	 */
	virtual BOOL DropTarget(u_int32_t uAddress, u_int16_t uPort);
	virtual BOOL DropTarget(DWORD uAddress, WORD uPort,bool isSip);
	
	virtual BOOL TargetExists(DWORD uAddress, WORD uPort);
	virtual void UpdateTarget(DWORD uAddress, WORD uPort, DWORD dwNewAddr, WORD wNewPort);


	/**
	 * Send packets to one or more networks.  Packet lengths must be less than or
	 * equal to MAX_PAYLOAD_SIZE, as this class does not do packetization since it
	 * it is application and codec specific.  However, it does handle RTP header
	 * fields.
	 *
	 * @param pBuf        pointer to data
	 * @param iLen        size of data buffer
	 * @param uTimeStamp  media-specific time stamp
	 *
	 * @return TRUE on success, FALSE on failure
	 */
	virtual BOOL Send(char* pBuf, int iLen, u_int32_t uTimeStamp);
	virtual BOOL Send(char* pBuf, int iLen, DWORD uTimeStamp,bool isSip);

	/**
	 * Packaging the packet to the packet buffer.
	 *
	 * @return TRUE on success, FALSE on failure
	 */	
	BOOL Package(char* pBuf, int iLen, u_int32_t uTimeStamp, pktbuf** ppPktBuf);
	BOOL Package(char* pBuf, int iLen, DWORD uTimeStamp, bool bMarker, pktbuf** ppPktBuf,bool isSip);

	/**
	 * Send packet to the specified networks.
	 * Address and port are in host byte order.
	 *
	 * @return TRUE on success, FALSE on failure
	 */
	virtual BOOL SendTo(u_int32_t uAddress, u_int16_t uPort, pktbuf* pb);
	virtual BOOL SendTo(DWORD uAddress, WORD uPort, pktbuf* pb,bool isSip);


	/**
	 * Send packet to all networks.
	 *
	 * @return TRUE on success, FALSE on failure
	 */	
	virtual BOOL SendTo(pktbuf* pb,bool isSipcontact=false);


	// ISocketContainer
	BOOL GetSharedSocket(SharedPtr<ESocket>& pSocket);
	BOOL SetSharedSocket(SharedPtr<ESocket>& pSocket);

	/**
	 * Set SSRC field in the RTP header.
	 *
	 * @param uSSRC, The syncronization source identifier.
	 *
	 */
	void SetStreamSSRC(u_int32_t uSSRC);

	u_int16_t NextSeqNo(int layer = 0);
	
	WORD GetExpectedPort(DWORD dwRealAddr, WORD wRealPort);

	/**
	 * Add a CSRC to the packet
	 * @return true if adding is successful
	 * @return false otherwise, when number of CSRC exceeds max allowed
	 */
	bool AddCSRC(u_int32_t uSSRC);

	/**
	 * Handles data received from the network.  The shared socket must be set
	 * for this function to work.
	 *
	 * In particular, this function enables the "auto-port" feature required
	 * for PAT-firewall compatibility.  This helps in the case where one side
	 * is directly connected to the Internet.
	 *
	 * Note: currently only network layer 0 is supported
	 *
	 * @return true on success, false on failure
	 */
	virtual bool HandleNetworkFeedback(void);

	/**
	* Handle auto port for PAT-firewall compatibility
	*
	* @param uAdd, the network address in network byte order.
	* @param uPort, the network port in network byte order
	* 
	* @return the remote port in network byte order
	*/

	virtual u_int16_t HandleNetworkFeedback(
		u_int32_t uAdd,
		u_int16_t uPort,
		const char* pBuf,
		int iLen);

protected:
	char m_chPayloadType;
	u_int32_t m_uStreamSSRC;
	RtpBufferPool *m_pBufferPool;

#ifdef LOG_PACKET_SEND_TIME
	FILE* m_fpLog;
	long m_nInitTime;
	int m_nNumPacket;
#endif

	std::queue<u_int32_t> m_CSRC;

private:
	void ClearCSRC();

private:
	// This mutex protects access to all member variables
	// e.g., so a separate thread can add/drop than the sending thread
	//Mutex *m_pMutex;

	bool m_bInitialized;
	HANDLE    m_hPacketEvent;

	NetworkLayers m_layers;
	SharedPtr<ESocket> m_pSocket;


	bool m_bOpen;
	//char m_chPayloadType;
	//DWORD m_uStreamSSRC;
	Mutex *m_pMutex;
	//RtpBufferPool *m_pBufferPool;
	//SharedPtr<ESocket> m_pSocket;

	// map expected address to real address, all in network byte order
	typedef std::map<AddressPortPair, AddressPortPair> TargetMap;
	TargetMap m_targetMap;
};

#endif // RTP_SENDER_H
