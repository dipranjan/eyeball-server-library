// ---------------------------------------------------------------------------
// File:       RtpReceiver.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Receive RTP packets
// 
// Change Log:
// ---------------------------------------------------------------------------

#ifndef RTP_RECEIVER_H
#define RTP_RECEIVER_H

//#define _WINSOCKAPI_
//#include <objbase.h>		// HANDLE

#include "SmartPtr.h"		//This line was commented, uncommented by Farhan
#include "utypes.h"
#include <queue>

class ESocket;
class UdpNetwork;
class pktbuf;

/**
 * Receives data using RTP.
 */
class RtpReceiver //:	public ISocketContainer		//This is commented out by Farhan
{
public:
	RtpReceiver();
	virtual ~RtpReceiver();

	/**
	 * Initiates use of this RtpReceiver object.
	 *
	 * @param hNotificationEvent  handle to event.  This will be set for each
	 *                            packet that is received.
	 * @param uAddress            address of remote host (in host byte order)
	 * @param uPort               port that data is to be received on (in host byte order)
	 */
	BOOL Open(
		HANDLE hNotificationEvent, 
		u_int32_t uAddress, 
		u_int16_t uPort);

	/**
	 * Terminates use of this RtpReceiver object.
	 */
	BOOL Close(void);

	/**
	 * Call this function to receive a packet after event is signalled.
	 *
	 * @param chPayloadType  payload type
	 * @param uSeqNo         packet sequence number
	 * @param uTimeStamp     media-specific time stamp
	 * @param pBuf           pointer to buffer
	 * @param iLen           payload size
	 * @param iSize          size of given buffer
	 */
	BOOL ReceivePacket(
		char& chPayloadType,
		u_int16_t& uSeqNo, 
		u_int32_t& uTimeStamp,
		u_int32_t& uSsrc,
        char* pBuf,
		int& iLen,
		const int iSize);

	// ISocketContainer
	BOOL GetSharedSocket(SharedPtr<ESocket>& pSocket);
	BOOL SetSharedSocket(SharedPtr<ESocket>& pSocket);


	void SendAck(u_int16_t uSeqNo[], int iNumber);
	void SendAck(const std::string& sBuf);

	// obtain the CSRC queue; the member m_CSRC is cleared after the call
	void GetCSRC(std::queue<u_int32_t>& qCSRC);

protected:
	bool        check_format(int fmt) const;
	pktbuf      *m_pPktBuf;
	std::queue<u_int32_t>	m_CSRC;

private:
	bool        m_bInitialized;

	UdpNetwork  *m_pUdpNetwork;

	u_int32_t   m_ulSenderAddress;		// for recvfrom
	int         m_iSenderPort;
	HANDLE      m_hNotificationEvent;
};

#endif // RTP_RECEIVER_H
