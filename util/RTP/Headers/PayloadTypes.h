// ---------------------------------------------------------------------------
// File:       PayloadTypes.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Define RTP payload types
// 
// Change Log:
// ---------------------------------------------------------------------------

#ifndef RTP_PAYLOAD_TYPES_H
#define RTP_PAYLOAD_TYPES_H

#define RTP_PT_LDCT		20	// Layered DCT codec 
#define RTP_PT_PVH		21	// prog. video w/ hybrid transform 
#define RTP_PT_BVC		22	// Berkeley video codec 

// RTP standard content encodings for video 
#define RTP_PT_RGB8		23 	// 8-bit dithered RGB 
#define RTP_PT_HDCC		24 	// SGI proprietary 
#define RTP_PT_CELLB	25 	// Sun CellB 
#define RTP_PT_JPEG		26	// JPEG 
#define RTP_PT_CUSEEME	27	// Cornell CU-SeeMe 
#define RTP_PT_NV		28	// Xerox PARC nv 
#define RTP_PT_PICW		29	// BB&N PictureWindow 
#define RTP_PT_CPV		30	// Concept/Bolter/Viewpoint codec 
#define RTP_PT_H261		31	// ITU H.261 
#define RTP_PT_MPEG		32 	// MPEG-I & MPEG-II 
#define RTP_PT_MP2T		33 	// MPEG-II either audio or video 
#define RTP_PT_ADV		34 	// ADV VIDEO 
#define RTP_PT_H263		34  // H263 VIDEO


// Experimental stuff added by kpatel 
#define RTP_PT_SC               50      // Semi compressed LCrCb 

// backward compat hack for decoding RTPv1 ivs streams
#define RTP_PT_H261_COMPAT 127

// RTP standard content encodings for audio 
#define RTP_PT_PCMU		0
#define RTP_PT_CELP		1
#define RTP_PT_GSM		3
#define RTP_PT_DVI		5
#define RTP_PT_LPC		7

// Our own definitions from dynamic payload types specified in RFC 1890

// Do not use RTP_PT_DVI since we support multiple sampling rates and channels
//#define RTP_PT_EYEBALL_ADPCM			100
#define RTP_PT_EYEBALL_MELP				101

#define RTP_PT_EYEBALL_EYESTREAM		102
#define RTP_PT_EYEBALL_EYESTREAM2		103
#define RTP_PT_EYEBALL_EYESTREAM3		104
#define RTP_PT_EYEBALL_EYESTREAM5		105
#define RTP_PT_EYEBALL_EYESTREAM6		106

#endif // RTP_PAYLOAD_TYPES_H
