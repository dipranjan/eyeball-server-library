// ---------------------------------------------------------------------------
// File:       HttpRtpReceiver.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    Receive RTP packets
// 
// Change Log:
// ---------------------------------------------------------------------------

#ifndef HTTP_RTP_RECEIVER_H
#define HTTP_RTP_RECEIVER_H


#include "RtpReceiver.h"

/**
 * Receives data using RTP.
 */
class HttpRtpReceiver : public RtpReceiver
{
public:

	HttpRtpReceiver() {};
	virtual ~HttpRtpReceiver() {};

	/**
	 * Call this function to extract a packet
	 *
	 * @param sMsg			 string storing the raw packet data
	 * @param iSize			 received packet size
	 * @param chPayloadType  payload type
	 * @param uSeqNo         packet sequence number
	 * @param uTimeStamp     media-specific time stamp
	 * @param pBuf           pointer to buffer
	 * @param iLen           payload size
	 * @param iSize          size of given buffer
	 */
	BOOL ReceivePacket(
		const std::string& sBuf,
		int iBufSize,
		char& chPayloadType,
		u_int16_t& uSeqNo, 
		u_int32_t& uTimeStamp,
		u_int32_t& uSsrc,
        char* pBuf,
		int& iLen,
		const int iSize);
};

#endif // HTTP_RTP_RECEIVER_H
