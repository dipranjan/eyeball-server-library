// ---------------------------------------------------------------------------
// File:       HttpRtpSender.h
//
// Copyright:  Eyeball Networks Inc. 1999-2001
//
// Purpose:    
// 
// Change Log:
// ---------------------------------------------------------------------------

#ifndef HTTP_RTP_SENDER_H
#define HTTP_RTP_SENDER_H

#include "RtpSender.h"

class HttpMessage;

/**
 * Sends data using RTP through HttpMessage
 * This class assumes to use a shared HttpMessage to send RTP through TCP socket
 */
class HttpRtpSender : public RtpSender
{
public:
    HttpRtpSender();
	virtual ~HttpRtpSender();

	virtual BOOL Open(char chPayloadType, HANDLE hNetworkEvent = NULL);

	/**
	 * Terminates use of this HttpRtpSender object.
	 *
	 */
	virtual void Close(void);

	/**
	 * Adds the specified remote as a target.
	 * HttpRtpSender maintains a list of receivers to support multiple clients.
	 *
	 * @param uAddress   IP address of remote (in host byte order)
	 * @param uPort		 UDP port of remote (in host byte order)
	 *
	 * @return TRUE on success, FALSE on failure
	 */
	virtual BOOL AddTarget(u_int32_t uAddress, u_int16_t uPort);
	
	/**
	 * Drops the specified target.
	 *
	 * @param uAddress   IP address of remote (in host byte order)
	 * @param uPort      UDP port of remote (in host byte order)
	 *
	 * @return TRUE on success, FALSE on failure
	 */
	virtual BOOL DropTarget(u_int32_t uAddress, u_int16_t uPort);


	/**
	 * Send packets to one or more networks.  Packet lengths must be less than or
	 * equal to MAX_PAYLOAD_SIZE, as this class does not do packetization since it
	 * it is application and codec specific.  However, it does handle RTP header
	 * fields.
	 *
	 * @param pBuf        pointer to data
	 * @param iLen        size of data buffer
	 * @param uTimeStamp  media-specific time stamp
	 *
	 * @return TRUE on success, FALSE on failure
	 */
	virtual BOOL Send(char* pBuf, int iLen, u_int32_t uTimeStamp);

	/**
	 * Send packet to the specified networks.
	 * Address and port are in host byte order.
	 *
	 * @return TRUE on success, FALSE on failure
	 */
	virtual BOOL SendTo(u_int32_t uAddress, u_int16_t uPort, pktbuf* pb);


	/**
	 * Send packet to all networks.
	 *
	 * @return TRUE on success, FALSE on failure
	 */	
	virtual BOOL SendTo(pktbuf* pb);


	BOOL GetSharedHttpMessage(SharedPtr<HttpMessage>& pNetwork);
	BOOL SetSharedHttpMessage(SharedPtr<HttpMessage>& pNetwork);

	/**
	 * Set whether this HttpRtpSender is used for transporting audio
	 */
	void SetIsAudio(bool bIsAudio);

	virtual bool HandleNetworkFeedback(void);
	virtual u_int16_t HandleNetworkFeedback(
		u_int32_t uAdd,
		u_int16_t uPort,
		const char* pBuf,
		int iLen);

	/**
	 * Set the send method function pointer
	 */	
	virtual void SetSendMethod(
		void (*method) (SharedPtr<HttpMessage>& pNetwork, const void* pb, bool bIsAudio));

protected:
	/**
	 * This method is called by the public Send methods, which are thread-safe
	 */
	virtual void DoSend(SharedPtr<HttpMessage>& pNetwork, const pktbuf* pb);

	// variable storing the function pointer of the send method, which is called by
	// DoSend
	void	(*m_SendMethod) (SharedPtr<HttpMessage>& pNetwork, const void* pb, bool bIsAudio);

	bool	m_bSend;

private:
	// This mutex protects access to all member variables
	Mutex *m_pMutex;

	bool m_bIsAudio;

	SharedPtr<HttpMessage> m_pNetwork;
};

#endif // HTTP_RTP_SENDER_H
