// ---------------------------------------------------------------------------
// File:       ebdecrypt.cc
//
// Copyright:  Eyeball Networks Inc. 2007
//
// Author:     Zion Kwok
//
// Purpose:    Decrypt passwords encrypted using ebpassword
//             COMPANY CONFIDENTIAL. TOP SECRET. DO NOT RELEASE.
//
// ---------------------------------------------------------------------------

#include "simple_crypt.h"
#include <iostream>
#include <string>

int main(int argc, const char *argv[])
{
	if (argc < 2)
        {
		std::cerr << "Usage: " << argv[0] << " <encrypted-password>" << std::endl;
		return 1;
	}

	std::string sDecryptedPassword = Crypt::simple_decrypt(argv[1]);
	std::cout << sDecryptedPassword.c_str() << std::endl;
	return 0;
}
