#ifndef _BASE64_H
#define _BASE64_H


bool Base64Encode(const char* szIn, const int iInLength, char *&szOut, int &iOutLength);
bool Base64Decode(const char* szIn, const int iInLength, char *&szOut, int &iOutLength);

/*
  bool Base64Encode(const std::string& sIn, std::string& sOut);
  bool Base64Decode(const std::string& sIn, std::string& sOut);
*/
#endif /* BASE64_H */

