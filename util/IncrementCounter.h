// IncrementCounter.h
// Interface for the CIncrementCounter class

#ifndef EB_INCREMENT_COUNTER_H_
#define EB_INCREMENT_COUNTER_H_

#include <pthread.h>

class CIncrementCounter
{
public:
	//! Constructor
	CIncrementCounter();

	//! Destructor
	~CIncrementCounter();

	//! Get next count
	unsigned int GetNextCount();

	//! Get next count
	void GetNextCount(unsigned int& uiHighCount, unsigned int& uiLowCount);

private:
	unsigned int m_uiHighCount;
	unsigned int m_uiLowCount;
	pthread_mutex_t m_Mutex;
};


#endif //EB_INCREMENT_COUNTER_H_

