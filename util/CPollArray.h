#ifndef _CPOLLARRAY_H_
#define _CPOLLARRAY_H_

#include <sys/poll.h>
#include <sys/resource.h>

namespace NEB{
        /// A class containing a poll array
        /**
        * This class does some required management.
        */
        class CPollArray
        {
                public:
                        /// The constructor.
                        CPollArray();
                        /// Get a pointer to pollfd structure array.
                        struct pollfd* Get() const { return m_ptrEventFds; }
                        /// Get the useful size of the pollfd array.
                        unsigned int Size() const { return m_iNumEvents; }
			int poll(int ms);
                        /// Add an Fd.
                        /**
                        * \param p_iFd is an fd.
                        * \param p_sEvents is the requested event of interest, default value is POLLIN.
                        * \return true if sucessful, otherwise false.
                        * \sa Add 
                        */
                        bool Add(int p_iFd, short p_sEvents = POLLIN);
                        /// Modify an Fd.
                        /**
                        * \param p_iFd is an fd.
                        * \param p_sEvents is the requested event of interest, default value is POLLIN.
                        * \return true if sucessful, otherwise false.
                        * \sa Modify 
                        */
			bool Modify(int p_iFd, short p_sEvents);
                        /// Remove an Fd.
                        /**
                        * \param p_iFd is an fd.
                        * \return true if sucessful, otherwise false.
                        * \sa Remove 
                        */
                        bool Remove(int p_iFd);
                        /// The destructor
                        /**
                        * Closes all the open fds.
                        */
                        ~CPollArray();

                protected:
                        /// Set maximum fd.
                        /**
                        * Set the maximum number of fd.
                        * \return maximum fd.
                        */
                        rlim_t SetMaxFds();
                        /// Get maximum file.
                        /**
                        * Get the number of actually available fds.
                        * \param p_iTryAtMost is the number of files to be tried at most.
                        * \return maximum number of files.
                        */
                        int GetMaxFiles(int p_iTryAtMost = 64000);
                        /// Function to close socket
                        /**
                        * \param p_iFd an fd of socket.
                        * \return true if sucessful, otherwise false.
                        */
                        bool CloseSocket(int p_iFd);

                private:
                        /// The array size.
                        int m_iMaxFd;
                        /// Maximum used Fd
                        int m_iMaxUsedFd;
			/// Num Events
			int m_iNumEvents;
                        /// A pointer to pollfd structure
                        struct pollfd* m_ptrPollFds;
			struct pollfd* m_ptrEventFds;
        };
}
#endif
