#!/usr/bin/perl -w
# set the data source name
# format: dbi:db type:db name:host:port
# mysql.s default port is 3306
# if you are running mysql on another host or port,
#    you must change it
use DBI;

sub escape_string;

my $dsn = 'dbi:mysql:eyeball:127.0.0.1:3306';

# set the user and password
my $user = 'root';
my $pass = '';

# now connect and get a database handle  
my $dbh = DBI->connect($dsn, $user, $pass)
 or die "Can.t connect to the DB: $DBI::errstr\n"; 
 
sub runquery{

    my($query) = @_;
    print ("run: $query");
    $querysth = $dbh->prepare($query);
    $querysth->execute;
    print ("\t\tdone\n");
}

# -- --------------------------------------------------------------------------------------
runquery("rename table TurnStatistics to turnstatistics2;");
runquery("rename table turnstatistics2 to turnstatistics;");

runquery("alter table turnstatistics change RecordTime `recordtime` datetime NOT NULL default '1970-01-01 00:00:00';");
runquery("alter table turnstatistics change Server `server` varchar(50) NOT NULL default ' ';");
runquery("alter table turnstatistics change Sessions `sessions` int(10) unsigned default '0';");
runquery("alter table turnstatistics change SessionMax `sessionsmax` int(10) unsigned default '0';");
runquery("alter table turnstatistics change SessionStarted `sessionsstarted` int(10) unsigned default '0';");
runquery("alter table turnstatistics change SessionEnded `sessionsended` int(10) unsigned default '0';");
runquery("alter table turnstatistics change PeekAllocated `peakallocated` int(10) unsigned default '0';");
runquery("alter table turnstatistics change PeekAllocatedMax `peakallocatedmax` int(10) unsigned default '0';");
runquery("alter table turnstatistics change UpStreamBytes `upstreambytes` bigint(20) unsigned default '0';");
runquery("alter table turnstatistics change DownStreamBytes `downstreambytes` bigint(20) unsigned default '0';");
runquery("alter table turnstatistics change UpStreamBitRate `upstreambitrate` int(10) unsigned default '0';");
runquery("alter table turnstatistics change DownStreamBitRate `downstreambitrate` int(10) unsigned default '0';");


#-- --------------------------------------------------------------------------------------
runquery("rename table TurnUsage to turnusage2;");
runquery("rename table turnusage2 to turnusage;");

runquery("alter table turnusage change Username `username` varchar(100) NOT NULL default ' ';");
runquery("alter table turnusage change Server `server` varchar(50) NOT NULL default ' ';");
runquery("alter table turnusage change IP1 `ip1` bigint(20) unsigned default '0';");
runquery("alter table turnusage change Port1 `port1` int(10) unsigned default '0';");
runquery("alter table turnusage change IP2 `ip2` bigint(20) unsigned default '0';");
runquery("alter table turnusage change Port2 `port2` int(10) unsigned default '0';");
runquery("alter table turnusage change IP3 `ip3` bigint(20) unsigned default '0';");
runquery("alter table turnusage change Port3 `port3` int(10) unsigned default '0';");
runquery("alter table turnusage change IP4 `ip4` bigint(20) unsigned default '0';");
runquery("alter table turnusage change Port4 `port4` int(10) unsigned default '0';");
runquery("alter table turnusage change Protocol `protocol` int(10) unsigned default '0';");
runquery("alter table turnusage change AllocationBegin `allocationbegin` datetime NOT NULL default '1970-01-01 00:00:00';");
runquery("alter table turnusage change Peakrate `peakrate` int(10) unsigned default '0';");


# -- --------------------------------------------------------------------------------------
runquery("rename table TurnUsageHistory to turnusage2;");
runquery("rename table turnusage2 to turnusagehistory;");

runquery("alter table turnusagehistory change Username `username` varchar(100) NOT NULL default ' ';");
runquery("alter table turnusagehistory change Server `server` varchar(50) NOT NULL default ' ';");
runquery("alter table turnusagehistory change IP1 `ip1` bigint(20) unsigned default '0';");
runquery("alter table turnusagehistory change Port1 `port1` int(10) unsigned default '0';");
runquery("alter table turnusagehistory change IP2 `ip2` bigint(20) unsigned default '0';");
runquery("alter table turnusagehistory change Port2 `port2` int(10) unsigned default '0';");
runquery("alter table turnusagehistory change IP3 `ip3` bigint(20) unsigned default '0';");
runquery("alter table turnusagehistory change Port3 `port3` int(10) unsigned default '0';");
runquery("alter table turnusagehistory change IP4 `ip4` bigint(20) unsigned default '0';");
runquery("alter table turnusagehistory change Port4 `port4` int(10) unsigned default '0';");
runquery("alter table turnusagehistory change Protocol `protocol` int(10) unsigned default '0';");
runquery("alter table turnusagehistory change AllocationBegin `allocationbegin` datetime NOT NULL default '1970-01-01 00:00:00';");
runquery("alter table turnusagehistory change AllocationEnd `allocationend` datetime NOT NULL default '1970-01-01 00:00:00';");
runquery("alter table turnusagehistory change PeakRate `peakrate` int(10) unsigned default '0';");
runquery("alter table turnusagehistory change UpStreamBytes `upstreambytes` bigint(20) unsigned default '0';");
runquery("alter table turnusagehistory change DownStreamBytes `downstreambytes` bigint(20) unsigned default '0';");
