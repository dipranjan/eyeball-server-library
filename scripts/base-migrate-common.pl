#!/usr/bin/perl -w
# set the data source name
# format: dbi:db type:db name:host:port
# mysql.s default port is 3306
# if you are running mysql on another host or port,
#    you must change it
use DBI;

sub escape_string;

my $dsn = 'dbi:mysql:eyeball:127.0.0.1:3306';

# set the user and password
my $user = 'root';
my $pass = '';

# now connect and get a database handle  
my $dbh = DBI->connect($dsn, $user, $pass)
 or die "Can.t connect to the DB: $DBI::errstr\n"; 
 
 sub changeUserIDToAccountID{
 
    my($tablename) = @_;
    
    $sth = $dbh->prepare("select distinct($tablename.User_ID) as username, account.Accounts_index as id from $tablename, account where $tablename.User_ID=account.User_ID");
    $sth->execute;
    
    while(my($username, $id) = $sth->fetchrow_array()){
        print("update $tablename set account_id=$id where User_ID='$username';\n");
    }
}

sub runquery{

    my($query) = @_;
    print ("run: $query");
    $sth = $dbh->prepare($query);
    $sth->execute;
    print ("\t\tdone\n");
}

#-- --------------------------------------------------------------------------------------


runquery("rename table Accounts to account;");

runquery("alter table account change `Accounts_index` `account_id` int unsigned not null auto_increment;");
runquery("alter table account change `Password` `password` varchar(32) not null default '';");
runquery("alter table account change `User_ID` `user_id` varchar(32) not null default '';");
runquery("alter table account change `Active` `active` varchar(1) not null default 'Y';");
runquery("alter table account change `Created` `created` datetime not null default '1970-01-01 00:00:00';");

# -- --------------------------------------------------------------------------------------

runquery("rename table ServerConfig to serverconfig2;");
runquery("rename table serverconfig2 to serverconfig;");

runquery("alter table serverconfig change Name name varchar(32) not null default '';");
runquery("alter table serverconfig change Value value varchar(255) not null default '';");
runquery("alter table serverconfig change Time recordtime datetime not null default '1970-01-01 00:00:00';");

# -- --------------------------------------------------------------------------------------

runquery("rename table StateServerRegistry to stateserverregistry2;");
runquery("rename table stateserverregistry2 to stateserverregistry;");

runquery("alter table stateserverregistry change Address address varchar(32) NOT NULL default ' ';");
runquery("alter table stateserverregistry change Status `status` varchar(21) NOT NULL default ' ';");
runquery("alter table stateserverregistry change Time `recordtime` datetime NOT NULL default '1970-01-01 00:00:00';");
runquery("alter table stateserverregistry change UserCount `usercount` int(10) unsigned NOT NULL default '0';");
runquery("alter table stateserverregistry change ProcessID `processid` int(10) unsigned NOT NULL default '0';");
runquery("alter table stateserverregistry change MessageCount `messagecount` int(10) unsigned NOT NULL default '0';");
runquery("alter table stateserverregistry change ResponseTime `responsetime` int(10) unsigned NOT NULL default '0';");
runquery("alter table stateserverregistry change ServerType `servertype` varchar(4) NOT NULL default 'ALL';");
