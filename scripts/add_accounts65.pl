#!/usr/bin/perl -w

sub add_user;

my @eyeball_users = ('chris','david','shahadat','zion','manjur','osama','feng',
                     'shantanu','zunnun','alex','craig','behrooz', 'stevens');

my @eyeball_fw = ('dlink1','linksys1','sonic1','pfu1','ipchains1','iptables1',
				'direct1','zyxel1','freebsd1','vista1','winproxy1','sonicwall1');

foreach $user (@eyeball_users) {
  add_user($user, 'password');
}

foreach $user (@eyeball_fw) {
  add_user($user, 'password');
}

for (my $i = 0; $i <= 100; $i++) {
  add_user("guest$i", 'password');
}

for (my $i = 0; $i <= 100; $i++) {
  add_user("eyeball$i", 'password');
}

for (my $i = 0; $i <= 100; $i++) {
  add_user("test_$i", 'password');
}

for (my $i = 0; $i <= 10; $i++) {
  add_user("am$i", 'password');
}

sub add_user {
  my ($username, $password) = @_;

  system("./provision65.pl -aadd -u$username -p$password");
}

