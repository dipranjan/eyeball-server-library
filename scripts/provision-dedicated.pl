#!/usr/bin/perl -w

sub escape_string;
sub add;
sub update;
sub updateNumber;

use Getopt::Std;

my %opts;
$opts{"a"} = '';
$opts{"u"} = '';
$opts{"p"} = '';
$opts{"n"} = '';
$opts{"m"} = '';
$opts{"c"} = '';

getopt('aupnmc', \%opts);

my $action = $opts{"a"};
my $username = $opts{"u"};
my $password = $opts{"p"};
my $phoneNumber = $opts{"n"};
my $method = $opts{"m"};
my $credit = $opts{"c"};

if ($action eq '') {
  die("Usage: ./provision-dedicated.pl -a <action> -n <phoneNumber> [-u <userID>][-p <password>][-c <credit>][-m paymentMethod].\nPossible actions: add, update, remove, enable, disable\n");
}


$username = &escape_string($username);
$password = &escape_string($password);
$phoneNumber = &escape_string($phoneNumber);
$method = &escape_string($method);
$credit = &escape_string($credit);

if ($action eq 'add') {
	
	&add;

} elsif ($action eq 'update') {

	&update;

} elsif ($action eq 'remove') {

	if($phoneNumber ne ''){
		print "DELETE from VoIPDedicatedNumber where PhoneNumber='$phoneNumber';\n";
	}elsif($username ne ''){
		print "UPDATE VoIPDedicatedNumber set Active='N' where User_ID='$username';\n";
		print "UPDATE VoIPDedicatedNumber set PaymentMethod='N' where User_ID='$username';\n";
		print "UPDATE VoIPDedicatedNumber set Credit='' where User_ID='$username';\n";
		print "UPDATE VoIPDedicatedNumber set User_ID='' where User_ID='$username';\n";
	}else{
		die("Usage: ./provision-dedicated.pl -a remove [-n <phoneNumber>] or [-u <userID>].\n");
	}
} elsif ($action eq 'enable') {

	if($phoneNumber ne ''){
		print "UPDATE VoIPDedicatedNumber set Active='Y' where PhoneNumber='$phoneNumber';\n";
	}elsif($username ne ''){
		print "UPDATE VoIPDedicatedNumber set Active='Y' where User_ID='$username';\n";
	}else{
		die("Usage: ./provision-dedicated.pl -a enable [-n <phoneNumber>] or [-u <userID>].\n");
	}
} elsif ($action eq 'disable') {

	if($phoneNumber ne ''){
		print "UPDATE VoIPDedicatedNumber set Active='N' where PhoneNumber='$phoneNumber';\n";
	}elsif($username ne ''){
		print "UPDATE VoIPDedicatedNumber set Active='N' where User_ID='$username';\n";
	}else{
		die("Usage: ./provision-dedicated.pl -a disable [-n <phoneNumber>] or [-u <userID>].\n");
	}
} else {
  die("Usage: ./provision-dedicated.pl -a <action> -n <phoneNumber> [-u <userID>][-p <password>][-c <credit>] [-m paymentMethod].\nPossible actions: add, update, remove, enable, disable\n");
}

sub escape_string {

  my $s1 = shift;

  $s1 =~ s/\\/\\\\/g;
  $s1 =~ s/\0/\\0/g;
  $s1 =~ s/\n/\\n/g;
  $s1 =~ s/\r/\\r/g;
  $s1 =~ s/\'/\\\'/g;
  $s1 =~ s/\"/\\\"/g;
  $s1 =~ s/\032/\\Z/g;

  return $s1;
}

sub add{
	if(($password eq '') || ($phoneNumber eq '')){
	   die("Usage: ./provision-dedicated.pl -a add -n <phoneNumber> -p <password> [-u <username>] [-c credit] [-m <paymentMethod>]\n");
	}
	print "INSERT into VoIPDedicatedNumber (PhoneNumber, Password) VALUES ('$phoneNumber','$password');\n";
	if($username ne ''){
  		print "UPDATE VoIPDedicatedNumber set UserID='$username' where PhoneNumber='$phoneNumber';\n";
  	}
  	if($credit ne ''){
  		print "UPDATE VoIPDedicatedNumber set Credit='$credit' where PhoneNumber='$phoneNumber';\n";
  	}
  	if($method ne ''){
  		print "UPDATE VoIPDedicatedNumber set PaymentMethod='$method' where PhoneNumber='$phoneNumber';\n";
  	}
}

sub update{
	
	# check if no user or phone number has been specified
	if(($username eq '') && ($phoneNumber eq '')){
	   die("Usage: ./provision-dedicated.pl -a update -n <phoneNumber> -u <username> [-p <password>] [-c credit] [-m <paymentMethod>]\n");
	
	# if both the username and password are specified we need to update the phone numbers userid
	# any subsequent options should be changed as well and tied to the phone number   
	}elsif(($username ne '') && ($phoneNumber ne '')){
		print "UPDATE VoIPDedicatedNumber set User_ID='$username' where PhoneNumber='$phoneNumber';\n";
		
		&updateNumber;
		

  	# if a userid is specified change all occurances with the provided options			
	}elsif($username ne ''){
		if($credit ne ''){
  			print "UPDATE VoIPDedicatedNumber set Credit='$credit' where User_ID='$username';\n";
  		}
  		if($method ne ''){
  			print "UPDATE VoIPDedicatedNumber set PaymentMethod='$method' where User_ID='$username';\n";
  		}
  		if($password ne ''){
  			print "UPDATE VoIPDedicatedNumber set Password='$password' where User_ID='$username';\n";
  		}
  	}
}

# contains the updates to database based upon the phone number
sub updateNumber{

		if($credit ne ''){
  			print "UPDATE VoIPDedicatedNumber set Credit='$credit' where PhoneNumber='$phoneNumber';\n";
  		}
  		if($method ne ''){
  			print "UPDATE VoIPDedicatedNumber set PaymentMethod='$method' where PhoneNumber='$phoneNumber';\n";
  		}
  		if($password ne ''){
  			print "UPDATE VoIPDedicatedNumber set Password='$password' where PhoneNumber='$phoneNumber';\n";
 		}  
}
