#!/usr/bin/perl

use Net::Telnet;
use Getopt::Long;

#my($hostname, $port, $command);
GetOptions('host=s', \$host, 'port=i', \$port, 'command=s', \$command);

usage () unless $host && $port && $command;

$telnet = new Net::Telnet ( Timeout=>10,
                            Errmode=>'die',
                            Port=>$port);

$telnet->open($host);
$telnet->print($command);
$welcome = $telnet->getline;
$result = $telnet->getline;
print ($result, "\n");

sub usage {
	print("Usage: ./service_command -host host -port port -command command\n");
	exit(1);
}
