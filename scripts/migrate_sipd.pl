#!/usr/bin/perl -w
# set the data source name
# format: dbi:db type:db name:host:port
# mysql.s default port is 3306
# if you are running mysql on another host or port,
#    you must change it
use DBI;

sub escape_string;

my $dsn = 'dbi:mysql:eyeball61:sip.eyeball.com:3306';
 
# set the user and password
my $user = 'server';
my $pass = 'eyeball';
 
# now connect and get a database handle  
my $dbh = DBI->connect($dsn, $user, $pass)
 or die "Can.t connect to the DB: $DBI::errstr\n"; 

$sth = $dbh->prepare("select User_ID,Email,FirstName,LastName,BirthDay,Gender,Address,City,StateProvince,Country,SubscriptionType,Created from AccountDetails");
$sth->execute;

while(my($User_ID, $Email, $FirstName, $LastName, $BirthDay, $Gender, $Address, $City, $StateProvince, $Country, $SubscriptionType, $Created) = $sth->fetchrow_array()) {

   $User_ID = &escape_string($User_ID);
   $Email = &escape_string($Email);
   $FirstName = &escape_string($FirstName);
   $LastName = &escape_string($LastName);
   $Address = &escape_string($Address);
   $City = &escape_string($City);
   print "INSERT into AccountDetails (User_ID,Email,FirstName,LastName,BirthDay,Gender,Address,City,StateProvince,Country,SubscriptionType,Created) VALUES ('$User_ID','$Email','$FirstName','$LastName','$BirthDay','$Gender','$Address','$City','$StateProvince','$Country','$SubscriptionType','$Created');\n";
} 

$sth = $dbh->prepare("select User_ID,Contact,Login,Logout,Reason from SipLoginsHistory");
$sth->execute;

while(my($User_ID, $Contact, $Login, $Logout, $Reason) = $sth->fetchrow_array()) {

   $User_ID = &escape_string($User_ID);
   $Contact = &escape_string($Contact);
   print "INSERT into SipLoginsHistory (User_ID,ProxyAddress,Contact,Login,Logout,Reason) VALUES ('$User_ID','Single Server','$Contact','$Login','$Logout','$Reason');\n";
} 

$sth = $dbh->prepare("select CallID,Caller,Callee,StartTime,EndTime from CallsHistory");
$sth->execute;

while(my($CallID, $Caller, $Callee, $StartTime, $EndTime) = $sth->fetchrow_array()) {

   $CallID = &escape_string($CallID);
   $Caller = &escape_string($Caller);
   $Callee = &escape_string($Callee);
   print "INSERT into CallsHistory (CallID,Caller,Callee,StartTime,EndTime) VALUES ('$CallID','$Caller','$Callee','$StartTime','$EndTime');\n";
} 

sub escape_string {

  my $s1 = shift;

  $s1 =~ s/\\/\\\\/g;
  $s1 =~ s/\0/\\0/g;
  $s1 =~ s/\n/\\n/g;
  $s1 =~ s/\r/\\r/g;
  $s1 =~ s/\'/\\\'/g;
  $s1 =~ s/\"/\\\"/g;
  $s1 =~ s/\032/\\Z/g;

  return $s1;
}

