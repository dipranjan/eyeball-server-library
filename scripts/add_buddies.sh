#!/bin/bash

# This script adds named users
domain='test.eyeball.com'

# add users to "users" array
users=(alex chris david shahadat zion osama feng shantanu zunnun craig behrooz stevens dlink1 linksys1 sonic1 pfu1 freebsd1 itables1 vista1 winproxy1 sonicwall1 direct1 zyxel1 )

for i in  "${users[@]}"
do
  ./provision.pl -a add -u $i -ppassword -f
done

for i in  "${users[@]}"
do
  for j in  "${users[@]}"
  do
    if [ "$i" != "$j" ]; then
      echo "INSERT INTO xmppsubscription (account_id,contactdisplayname,contactgroup,state,contact) values((select account_id from account where user_id='$i'), '$j', '', 'both', '$j@$domain');";
    fi
  done
done


