#!/usr/bin/perl -w

sub escape_string;

use Getopt::Std;

my %opts;
$opts{"a"} = '';
$opts{"u"} = '';
$opts{"p"} = '';
$opts{"f"} = '';
getopt('aup', \%opts);

my $action = $opts{"a"};
my $username = $opts{"u"};
my $password = $opts{"p"};
my $flag = $opts{"f"};

my $usage = "Usage: ./provision.pl -a <action> -u <username> [-p <password> -f]\nPossible actions: add, addadmin, update, remove, removeadmin, enable, disable\n";

if ($action eq '') {
  die("$usage");
}

if ($username eq '') {
  die("$usage");
}

my $des_hex_key = '010101010101010102020202020202020404040404040404';

if ($password ne '') {
  open(PASSWORD_FILE, "./pass3des $des_hex_key $username $password |");
  if (defined($encrypted_password = <PASSWORD_FILE>)) {
    chomp $encrypted_password;
    $password = $encrypted_password;
  }
  else {
    die("Error: failed to encrypt password\n");
  }
  close(PASSWORD_FILE);
}

$username = &escape_string($username);
$password = &escape_string($password);

if ($action eq 'add') {
  print "INSERT into account (user_id,password,active,created) VALUES ('$username','$password','Y',NOW());\n";
  if($flag){
	print("insert into afwaccount (account_id) values((select account_id from account where user_id='$username'));\n");
  }
} elsif ($action eq 'update') {
  print "UPDATE account set password='$password' where user_id='$username';\n";
} elsif ($action eq 'addadmin') {
  print "INSERT into account (user_id,password,active,created) VALUES ('$username','$password','Y',NOW());\n";
  print "INSERT into accountpermission (user_id, permission) values ('$username', 'ADMIN');\n";
  if($flag){
    print("insert into afwaccount (account_id) values((select account_id from account where user_id='$username'));\n");
  }
} elsif ($action eq 'remove') {
  print "DELETE from account where user_id='$username';\n";
  if($flag){
    print("delete from afwaccount where account_id=(select account_id from account where user_id='$username'));\n");
  }
} elsif ($action eq 'removeadmin') {
  print "DELETE from account where user_id='$username';\n";
  print "DELETE from accountpermission where user_id='$username';\n";
  if($flag){
    print("delete from afwaccount where account_id=(select account_id from account where user_id='$username'));\n");
  }
} elsif ($action eq 'enable') {
  print "UPDATE account set active='Y' where user_id='$username';\n";
} elsif ($action eq 'disable') {
  print "UPDATE account set active='N' where user_id='$username';\n";
} else {
  die("$usage");
}

sub escape_string {

  my $s1 = shift;

  $s1 =~ s/\\/\\\\/g;
  $s1 =~ s/\0/\\0/g;
  $s1 =~ s/\n/\\n/g;
  $s1 =~ s/\r/\\r/g;
  $s1 =~ s/\'/\\\'/g;
  $s1 =~ s/\"/\\\"/g;
  $s1 =~ s/\032/\\Z/g;

  return $s1;
}

