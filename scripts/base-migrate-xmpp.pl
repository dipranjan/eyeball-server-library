#!/usr/bin/perl -w
# set the data source name
# format: dbi:db type:db name:host:port
# mysql.s default port is 3306
# if you are running mysql on another host or port,
#    you must change it
use DBI;

sub escape_string;

my $dsn = 'dbi:mysql:eyeball:127.0.0.1:3306';

# set the user and password
my $user = 'root';
my $pass = '';

# now connect and get a database handle  
my $dbh = DBI->connect($dsn, $user, $pass)
 or die "Can.t connect to the DB: $DBI::errstr\n"; 
 
sub changeUserIDToAccountID{
 
    my($tablename) = @_;
    
    $sth = $dbh->prepare("select distinct($tablename.User_ID) as username, account.account_id as id from $tablename, account where $tablename.User_ID=account.user_id");
    $sth->execute;
    
    while(my($username, $id) = $sth->fetchrow_array()){
        runquery("update $tablename set account_id=$id where User_ID='$username';");
    }
}

sub runquery{

    my($query) = @_;
    print ("run: $query");
    $querysth = $dbh->prepare($query);
    $querysth->execute;
    print ("\t\tdone\n");
}


#-- --------------------------------------------------------------------------------------
runquery("rename table XmppBlockListNames to xmppblocklistname;");

runquery("alter table xmppblocklistname drop primary key;");
runquery("alter table xmppblocklistname add `xmppblocklistname_id` int(10) unsigned NOT NULL auto_increment, add primary key(xmppblocklistname_id);");
runquery("alter table xmppblocklistname add `account_id` int(10) unsigned NOT NULL default '0';");
runquery("alter table xmppblocklistname change ListName `listname` varchar(32) NOT NULL default ' ';");


changeUserIDToAccountID("xmppblocklistname");


runquery("alter table xmppblocklistname drop User_ID;");


# -- --------------------------------------------------------------------------------------
runquery("rename table XmppBlockListUsage to xmppblocklistusage2;");
runquery("rename table xmppblocklistusage2 to xmppblocklistusage;");

runquery("alter table xmppblocklistusage drop primary key;");
runquery("alter table xmppblocklistusage add  `account_id` int(10) unsigned NOT NULL default '0';");
runquery("alter table xmppblocklistusage change ListName `listname` varchar(32) default NULL;");
runquery("alter table xmppblocklistusage change Resource `resource` varchar(32) NOT NULL default ' ';");


changeUserIDToAccountID("xmppblocklistusage");


runquery("alter table xmppblocklistusage drop User_ID;");
runquery("alter table xmppblocklistusage add primary key(account_id);");


# -- --------------------------------------------------------------------------------------
runquery("rename table XmppBlockLists to xmppblocklist;");

runquery("alter table xmppblocklist add `xmppblocklist_id` int(10) unsigned NOT NULL auto_increment, add primary key(xmppblocklist_id);");
runquery("alter table xmppblocklist add `account_id` int(10) unsigned NOT NULL default '0';");
runquery("alter table xmppblocklist change ListName `listname` varchar(32) NOT NULL default ' ';");
runquery("alter table xmppblocklist change ItemType `type` int(10) unsigned NOT NULL default '0';");
runquery("alter table xmppblocklist change ItemAllow `allow` int(10) unsigned NOT NULL default '0';");
runquery("alter table xmppblocklist change Message `message` int(10) unsigned NOT NULL default '0';");
runquery("alter table xmppblocklist change PresenceIn `presencein` int(10) unsigned NOT NULL default '0';");
runquery("alter table xmppblocklist change ItemOrder `listorder` int(10) unsigned NOT NULL default '0';");
runquery("alter table xmppblocklist change PresenceOut `presenceout` int(10) unsigned NOT NULL default '0';");
runquery("alter table xmppblocklist change Iq `iq` int(10) unsigned NOT NULL default '0';");
runquery("alter table xmppblocklist change ItemValue `value` varchar(32) NOT NULL default ' ';");


changeUserIDToAccountID("xmppblocklist");


runquery("alter table xmppblocklist drop User_ID;");


# -- --------------------------------------------------------------------------------------
runquery("rename table XmppEdgeServerHistory to xmppserverhistory2;");
runquery("rename table xmppserverhistory2 to xmppserverhistory;");

runquery("alter table xmppserverhistory add xmppserverhistory_id int(10) unsigned not null auto_increment, add primary key(xmppserverhistory_id);");
runquery("alter table xmppserverhistory add `xmppserver_id` int(10) unsigned NOT NULL default '0';");
runquery("alter table xmppserverhistory change Address `address` varchar(32) NOT NULL default ' ';");
runquery("alter table xmppserverhistory change Action `action` varchar(16) NOT NULL default ' ';");
runquery("alter table xmppserverhistory change `Time` `recordtime` datetime NOT NULL default '1970-01-01 00:00:00';");


# -- --------------------------------------------------------------------------------------
runquery("rename table XmppEdgeServerRegistry to xmppserverregistry2;");
runquery("rename table xmppserverregistry2 to xmppserverregistry;");

runquery("alter table xmppserverregistry change Address `address` varchar(32) NOT NULL default ' ';");
runquery("alter table xmppserverregistry change `Time` `recordtime` datetime NOT NULL default '1970-01-01 00:00:00';");
runquery("alter table xmppserverregistry change ProcessID `processid` int(10) unsigned NOT NULL default '0';");


# -- --------------------------------------------------------------------------------------
runquery("rename table XmppLoginsHistory to xmpploginhistory2;");
runquery("rename table xmpploginhistory2 to xmpploginhistory;");

runquery("alter table xmpploginhistory add `xmpploginhistory_id` int(10) unsigned NOT NULL auto_increment, add primary key(xmpploginhistory_id);");
runquery("alter table xmpploginhistory add `account_id` int(10) unsigned NOT NULL default '0';");
runquery("alter table xmpploginhistory change ProxyAddress `proxyaddress` varchar(32) NOT NULL default ' ';");
runquery("alter table xmpploginhistory change Contact `contact` varchar(100) NOT NULL default ' ';");
runquery("alter table xmpploginhistory change Login `login` datetime NOT NULL default '1970-01-01 00:00:00';");
runquery("alter table xmpploginhistory change Logout `logout` datetime NOT NULL default '1970-01-01 00:00:00';");


changeUserIDToAccountID("xmpploginhistory");


runquery("alter table xmpploginhistory drop User_ID;");


# -- --------------------------------------------------------------------------------------
runquery("rename table XmppServerStatistics to xmppstatistics;");

runquery("alter table xmppstatistics drop primary key;");
runquery("alter table xmppstatistics add xmppstatistics_id int(10) unsigned not null auto_increment, add primary key(xmppstatistics_id);");
runquery("alter table xmppstatistics change RecordTime `recordtime` datetime NOT NULL default '1970-01-01 00:00:00';");
runquery("alter table xmppstatistics change ServerAddress `serveraddress` varchar(21) NOT NULL default ' ';");
runquery("alter table xmppstatistics change Connections `connections` int(10) unsigned NOT NULL default '0';");
runquery("alter table xmppstatistics change ActiveUsers `activeusers` int(10) unsigned NOT NULL default '0';");
runquery("alter table xmppstatistics change Login `login` int(10) unsigned NOT NULL default '0';");
runquery("alter table xmppstatistics change Logout `logout` int(10) unsigned NOT NULL default '0';");
runquery("alter table xmppstatistics change InstantMessages `instantmessages` int(10) unsigned NOT NULL default '0';");
runquery("alter table xmppstatistics change FileTransfers `filetransfers` int(10) unsigned NOT NULL default '0';");
runquery("alter table xmppstatistics change PresenceStanzas `presencestanzas` int(10) unsigned NOT NULL default '0';");
runquery("alter table xmppstatistics change IqStanzas `iqstanzas` int(10) unsigned NOT NULL default '0';");
runquery("alter table xmppstatistics change KeepAlives `keepalives` int(10) unsigned NOT NULL default '0';");


# -- --------------------------------------------------------------------------------------
runquery("rename table XmppSubscriptions to xmppsubscriptions2;");
runquery("rename table xmppsubscriptions2 to xmppsubscriptions;");

runquery("alter table xmppsubscriptions drop primary key;");
runquery("alter table xmppsubscriptions add `xmppsubscription_id` int(11) NOT NULL auto_increment, add primary key(xmppsubscription_id);");
runquery("alter table xmppsubscriptions change ContactGroup `contactgroup` varchar(32) NOT NULL default ' ';");
runquery("alter table xmppsubscriptions change State `state` varchar(24) NOT NULL default 'none';");
runquery("alter table xmppsubscriptions change ContactDisplayName `contactdisplayname` varchar(1024) NOT NULL default ' ';");
runquery("alter table xmppsubscriptions add  `account_id` int(10) unsigned NOT NULL default '0';");
runquery("alter table xmppsubscriptions change Contact `contact` varchar(48) NOT NULL default ' ';");


changeUserIDToAccountID("xmppsubscriptions");


runquery("alter table xmppsubscriptions drop User_ID;");


# -- --------------------------------------------------------------------------------------
runquery("rename table XmppOfflineMessages to xmppofflinemessages2;");
runquery("rename table xmppofflinemessages2 to xmppofflinemessages;");

runquery("alter table xmppofflinemessages add `xmpploginhistory_id` int(10) unsigned NOT NULL auto_increment, add primary key(xmpploginhistory_id);");
runquery("alter table xmppofflinemessages change ToUser_ID  `touserid` varchar(32) NOT NULL default ' ';");
runquery("alter table xmppofflinemessages change FromUser_ID `fromuserid` varchar(32) NOT NULL default ' ';");
runquery("alter table xmppofflinemessages change FromResource `fromresource` varchar(32) NOT NULL default ' ';");
runquery("alter table xmppofflinemessages change Message `message` varchar(64535) NOT NULL default ' ';");
runquery("alter table xmppofflinemessages change MessageID `messageid` varchar(40) NOT NULL default ' ';");
runquery("alter table xmppofflinemessages change `Timestamp` `recordtime` datetime NOT NULL default '1970-01-01 00:00:00';");


# -- --------------------------------------------------------------------------------------
runquery("rename table XmppPeerDomainConnections to xmpppeerdomainconnection;");

runquery("alter table xmpppeerdomainconnection change Domain  `domain` varchar(32) NOT NULL default ' ';");
runquery("alter table xmpppeerdomainconnection change State `state` varchar(12) NOT NULL default 'auto';");
runquery("alter table xmpppeerdomainconnection change DomainAddress `domainaddress` varchar(32) NOT NULL default ' ';");
runquery("alter table xmpppeerdomainconnection change ForwardAddress `forwardaddress` varchar(32) NOT NULL default ' ';");
runquery("alter table xmpppeerdomainconnection change RefreshTime `refreshtime` datetime NOT NULL default '1970-01-01 00:00:00';");


# -- --------------------------------------------------------------------------------------
runquery("rename table XmppPeerDomains to xmpppeerdomain;");

runquery("alter table xmpppeerdomain change Domain `domain` varchar(32) NOT NULL default ' ';");
runquery("alter table xmpppeerdomain change IncomingPassword `incomingpassword` varchar(32) NOT NULL default ' ';");
runquery("alter table xmpppeerdomain change OutgoingPassword `outgoingpassword` varchar(32) NOT NULL default ' ';");
runquery("alter table xmpppeerdomain change OutgoingAuthMethod `outgoingauthmethod` varchar(12) NOT NULL default 'auto';");
runquery("alter table xmpppeerdomain change Active `active` varchar(1) NOT NULL default 'Y';");
runquery("alter table xmpppeerdomain change Created `recordtime` datetime NOT NULL default '1970-01-01 00:00:00';");


# -- --------------------------------------------------------------------------------------
runquery("rename table XmppPrivateStorage to xmppprivatestorage2;");
runquery("rename table xmppprivatestorage2 to xmppprivatestorage;");

runquery("alter table xmppprivatestorage drop primary key;");
runquery("alter table xmppprivatestorage add  `xmppprivatestorage_id` int(10) unsigned NOT NULL auto_increment, add primary key(xmppprivatestorage_id);");
runquery("alter table xmppprivatestorage add `account_id` int(10) unsigned NOT NULL default '0';");
runquery("alter table xmppprivatestorage change NodeName `nodename` varchar(32) NOT NULL default ' ';");
runquery("alter table xmppprivatestorage change NameSpace `namespace` varchar(64) NOT NULL default ' ';");
runquery("alter table xmppprivatestorage change PrivateData `data` varchar(64535) NOT NULL default ' ';");


changeUserIDToAccountID("xmppprivatestorage");


runquery("alter table xmppprivatestorage drop User_ID;");


# -- --------------------------------------------------------------------------------------
runquery("rename table XmppResources to xmppresource;");

runquery("alter table xmppresource drop primary key;");
runquery("alter table xmppresource add `xmppresource_id` int(10) unsigned NOT NULL auto_increment, add primary key(xmppresource_id);");
runquery("alter table xmppresource add `account_id` int(10) unsigned NOT NULL default '0';");
runquery("alter table xmppresource change `Resource` `resource` varchar(32) NOT NULL default ' ';");
runquery("alter table xmppresource change State `state` varchar(12) NOT NULL default ' ';");
runquery("alter table xmppresource change Address `address` varchar(23) NOT NULL default ' ';");
runquery("alter table xmppresource change RequestedRoster `requestedroster` varchar(1) NOT NULL default 'N';");
runquery("alter table xmppresource change Priority `priority` int(10) unsigned NOT NULL default '0';");
runquery("alter table xmppresource change EdgeServerAddress `serveraddress` varchar(23) NOT NULL default ' ';");
runquery("alter table xmppresource change LoginTime `logintime` datetime NOT NULL default '1970-01-01 00:00:00';");
runquery("alter table xmppresource change LastPresence `lastpresence` longtext NOT NULL;");

  
changeUserIDToAccountID("xmppresource");

runquery("alter table xmppresource drop User_ID;");
