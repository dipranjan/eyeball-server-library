README file for provision-dedicated.pl

This README file contains information on how to administer the dedicated phone numbers. 

The usage of the provision-dedicated.pl script is as follows:

./provision-dedicated.pl -a<option>
				 -n<phoneNumber>
				 -u<userID>
				 -p<password>
				 -m<payment Method>
				 -c<credit>

Options:
-a - options <add>, <update>, <remove>, <enable>, <disable>
-n - dedicated phone number
-u - user ID
-p - password
-m - payment method allowed values <N, Pre, Post>
-c - credit (only used for Pre-paid calls)

Adding numbers to the database.

 The VoIPDedicatedNumber table contains the phone number and soft switch password. The minimum information needed to add a number is just the phone 
number and soft switch password. Additional information can be added at creation time to include user specific information.

Example usage:

- You wish to add a phone number "1234567890" and password "password"
./provision-dedicated.pl -aadd -n1234567890 -ppassword

- You wish to add a phone number complete with user information. Phone number "1234456789" password "password" userID "fred" payment method "Pre" 
and credit amount is 150
./provision-dedicated.pl -aadd -n1234456789 -ppassword -ufred -mPre -c150

- You wish to add a phone number complete with user information. Phone number "1234456789" password "password" userID "fred" payment method 
"Post" and credit amount is 150
./provision-dedicated.pl -aadd -n1234456789 -ppassword -ufred -mPost

Update phone number information in database

 Updates can be based upon phone number or userID. If a phone number is supplied the any additional parameters will be updated for the given phone 
number. If no phone number is given but a userID is given any additional parameters will be updated for all entries for the given user.

Example usage:

- You wish to change the userID "fred" assigned to the phone number "1234567890"
./provision-dedicated.pl -aupdate -n1234567890 -uwilma

- You wish to change the payment method to Post-paid for the phone number "1234567890"
./provision-dedicated.pl -aupdate -n1234567890 -mPost

- You wish to change the payment method to Post-paid for the userID "fred" (this will cause all dedicated numbers assigned to the userID "fred" to be 
changed to Post-paid calls.
./provision-dedicated.pl -aupdate -ufred -mPost

Remove a phone number or userID from the database

 A number can be removed using the remove command. All that needs to be specified is the phone number the administer wishes to remove.  A user can be 
removed from the database. All phone numbers that have been assigned to the user will be set to inactive and defaults values set for each individual 
phone number. If both a userID and phone number are given only the given phone number will be removed the user parameter will be ignored.

Example usage:

- You wish to remove the phone number "1234456789" from the database
./provision-dedicated.pl -aremove -n1234456789

- You wish to remove the userID "fred" from the database
./provision-dedicated.pl -aremove -u

Enable a phone number of userID in the database

 Either a userID or individual phone number can be enabled. If a phone number is provided the phone number will be enabled. If a userID us given all 
phone numbers assigned to the userID will be enabled. If both a phone number and userID is provided only the phone number will be enabled.

Example usage:

- You wish to enable the phone number "1234567890" from the database
./provision-dedicated.pl -aenable -n1234567890

- You wish to enable all the phone numbers assigned to the user "fred"
./provision-dedicated.pl -aenable -ufred

Disable a phone number of userID in the database

 Either a userID or individual phone number can be disabled. If a phone number is provided the phone number will be disabled. If a userID us given all 
phone numbers assigned to the userID will be disabled. If both a phone number and userID is provided only the phone number will be disabled.

Example usage:

- You wish to enable the phone number "1234567890" from the database
./provision-dedicated.pl -adisable -n1234567890

- You wish to enable all the phone numbers assigned to the user "fred"
./provision-dedicated.pl -adisable -ufred

