#!/usr/bin/perl -w
# set the data source name
# format: dbi:db type:db name:host:port
# mysql.s default port is 3306
# if you are running mysql on another host or port,
#    you must change it
use DBI;

sub escape_string;

my $dsn = 'dbi:mysql:eyeball:127.0.0.1:3306';

# set the user and password
my $user = 'root';
my $pass = '';

# now connect and get a database handle  
my $dbh = DBI->connect($dsn, $user, $pass)
 or die "Can.t connect to the DB: $DBI::errstr\n"; 
 
sub changeUserIDToAccountID{
 
    my($tablename) = @_;
    
    $sth = $dbh->prepare("select distinct($tablename.User_ID) as username, account.account_id as id from $tablename, account where $tablename.User_ID=account.user_id");
    $sth->execute;
    
    while(my($username, $id) = $sth->fetchrow_array()){
        runquery("update $tablename set account_id=$id where User_ID='$username';");
    }
}

sub runquery{

    my($query) = @_;
    print ("run: $query");
    $querysth = $dbh->prepare($query);
    $querysth->execute;
    print ("\t\tdone\n");
}

sub deleteCallIDFromTable{

    my($tablename) = @_;
    
    $deletesth = $dbh->prepare("select CallID from $tablename group by CallID having count(*) > 1");
    $deletesth->execute;
    
    while(my($callID) = $deletesth->fetchrow_array()){
        runquery("delete from $tablename where CallID='$callID'");
    }
}



# -- --------------------------------------------------------------------------------------
runquery("rename table Calls to sipcall;");

runquery("alter table sipcall change callid `callid` varchar(100) NOT NULL default ' ';");
runquery("alter table sipcall change caller `caller` varchar(100) NOT NULL default ' ';");
runquery("alter table sipcall change callee `callee` varchar(100) NOT NULL default ' ';");
runquery("alter table sipcall change StartTime `starttime` datetime NOT NULL default '1970-01-01 00:00:00';");

deleteCallIDFromTable("sipcall");

runquery("alter table sipcall add primary key(callid);");


# -- --------------------------------------------------------------------------------------
runquery("rename table CallsHistory to sipcallhistory;");

runquery("alter table sipcallhistory change callid `callid` varchar(100) NOT NULL default ' ';");
runquery("alter table sipcallhistory change caller `caller` varchar(100) NOT NULL default ' ';");
runquery("alter table sipcallhistory change callee `callee` varchar(100) NOT NULL default ' ';");
runquery("alter table sipcallhistory change StartTime `starttime` datetime NOT NULL default '1970-01-01 00:00:00';");
runquery("alter table sipcallhistory change EndTime `endtime` datetime NOT NULL default '1970-01-01 00:00:00';");

deleteCallIDFromTable("sipcallhistory");

runquery("alter table sipcallhistory add primary key(callid);");

# -- --------------------------------------------------------------------------------------
runquery("rename table SipLoginState to siploginstate2;");
runquery("rename table siploginstate2 to siploginstate;");

runquery("alter table siploginstate add `account_id` int(10) unsigned NOT NULL default '0';");
runquery("alter table siploginstate change ProxyAddress `proxyaddress` varchar(32) NOT NULL default ' ';");
runquery("alter table siploginstate change Contact `contact` varchar(32) NOT NULL default ' ';");
runquery("alter table siploginstate change Login `login` datetime NOT NULL default '1970-01-01 00:00:00';");
runquery("alter table siploginstate change Expiry `expires` int(10) unsigned NOT NULL default '0';");
runquery("alter table siploginstate change ForwardAddress `forwardaddress` varchar(32) NOT NULL default ' ';");
runquery("alter table siploginstate change `Hash` `hash` int(10) unsigned NOT NULL default '0';");


changeUserIDToAccountID("siploginstate");


runquery("alter table siploginstate drop primary key;");
runquery("alter table siploginstate add primary key (account_id);");
runquery("alter table siploginstate drop User_ID;");


# -- --------------------------------------------------------------------------------------
runquery("rename table SipLoginsHistory to siploginhistory2;");
runquery("rename table siploginhistory2 to siploginhistory;");

runquery("alter table siploginhistory add `siploginhistory_id` int(10) unsigned NOT NULL auto_increment, add primary key (`siploginhistory_id`);");
runquery("alter table siploginhistory add `account_id` int(10) unsigned NOT NULL default '0';");
runquery("alter table siploginhistory change ProxyAddress `proxyaddress` varchar(32) NOT NULL default ' ';");
runquery("alter table siploginhistory change Contact `contact` varchar(100) NOT NULL default ' ';");
runquery("alter table siploginhistory change Login `login` datetime NOT NULL default '1970-01-01 00:00:00';");
runquery("alter table siploginhistory change Logout `logout` datetime NOT NULL default '1970-01-01 00:00:00';");
runquery("alter table siploginhistory change Reason `reason` varchar(1) NOT NULL default 'N';");


changeUserIDToAccountID("siploginhistory");


runquery("alter table siploginhistory drop User_ID;");


# -- --------------------------------------------------------------------------------------
runquery("rename table SipProxyStatistics to sipstatistics;");

runquery("alter table sipstatistics drop primary key;");

runquery("alter table sipstatistics add `sipstatistics_id` int(11) NOT NULL auto_increment, add primary key (sipstatistics_id);");
runquery("alter table sipstatistics add `sipserver_id` int(10) unsigned NOT NULL default '0';");
runquery("alter table sipstatistics change RecordTime `recordtime` datetime NOT NULL default '1970-01-01 00:00:00';");
runquery("alter table sipstatistics change ProxyAddress `proxyaddress` varchar(21) NOT NULL default ' ';");
runquery("alter table sipstatistics change Connections `connections` int(10) unsigned NOT NULL default '0';");
runquery("alter table sipstatistics change Online `onlinecurrent` int(10) unsigned NOT NULL default '0';");
runquery("alter table sipstatistics change OnlineMax `onlinemax` int(10) unsigned NOT NULL default '0';");
runquery("alter table sipstatistics change Calls `calls` int(10) unsigned NOT NULL default '0';");
runquery("alter table sipstatistics change CallMinutes `callminutes` int(10) unsigned NOT NULL default '0';");
runquery("alter table sipstatistics change Throughput `throughput` int(10) unsigned NOT NULL default '0';");
runquery("alter table sipstatistics change Login `login` int(10) unsigned NOT NULL default '0';");
runquery("alter table sipstatistics change Logout `logout` int(10) unsigned NOT NULL default '0';");
runquery("alter table sipstatistics change CallsInitiated `callsinitiated` int(10) unsigned NOT NULL default '0';");
runquery("alter table sipstatistics change CallsEnded `callsended` int(10) unsigned NOT NULL default '0';");
runquery("alter table sipstatistics change STUN `stun` int(10) unsigned NOT NULL default '0';");
runquery("alter table sipstatistics change CallsP2P `callsp2p` int(10) unsigned NOT NULL default '0';");
runquery("alter table sipstatistics change CallsUDPRelay `callsudprelay` int(10) unsigned NOT NULL default '0';");
runquery("alter table sipstatistics change CallsTCPRelay `callstcprelay` int(10) unsigned NOT NULL default '0';");
runquery("alter table sipstatistics change CallsHTTPRelay `callshttprelay` int(10) unsigned NOT NULL default '0';");
runquery("alter table sipstatistics change CallsRelayError `callsrelayerror` int(10) unsigned NOT NULL default '0';");
runquery("alter table sipstatistics change CallsUnknown `callsunknown` int(10) unsigned NOT NULL default '0';");


# -- --------------------------------------------------------------------------------------
runquery("rename table SipEdgeProxyHistory to sipserverhistory;");

runquery("alter table sipserverhistory add `sipserverhistory_id` int(10) unsigned NOT NULL auto_increment, add primary key(sipserverhistory_id);");
runquery("alter table sipserverhistory add `sipserver_id` int(10) unsigned NOT NULL default '0';");
runquery("alter table sipserverhistory change Address `address` varchar(32) NOT NULL default ' ';");
runquery("alter table sipserverhistory change Action `action` varchar(16) NOT NULL default ' ';");
runquery("alter table sipserverhistory change Time `recordtime` datetime NOT NULL default '1970-01-01 00:00:00';");
