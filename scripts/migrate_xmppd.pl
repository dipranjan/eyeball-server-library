#!/usr/bin/perl -w
# set the data source name
# format: dbi:db type:db name:host:port
# mysql.s default port is 3306
# if you are running mysql on another host or port,
#    you must change it
use DBI;

sub get_name_from_jid;
sub escape_string;

my $dsn = 'dbi:mysql:eyeball61:sip.eyeball.com:3306';
 
# set the user and password
my $user = 'server';
my $pass = 'eyeball';
 
my $des_hex_key = '010101010101010102020202020202020404040404040404';

# now connect and get a database handle  
my $dbh = DBI->connect($dsn, $user, $pass)
 or die "Can.t connect to the DB: $DBI::errstr\n"; 

my $sth = $dbh->prepare("select username, password from authreg");
$sth->execute;

while(my($username, $password) = $sth->fetchrow_array()) {

   my $encrypted_password = "";
   open(PASSWORD_FILE, "./pass3des $des_hex_key $username $password |");
   if (defined($encrypted_password = <PASSWORD_FILE>)) {
     chomp $encrypted_password;
   }
   close(PASSWORD_FILE);
   $username = &escape_string($username);
   print "INSERT into Accounts (User_ID,Password,Active,Created) VALUES ('$username','$encrypted_password','Y',NOW());\n";
} 

$sth = $dbh->prepare("select `collection-owner`,jid,name,`to`,`from`,ask from `roster-items`");
$sth->execute;

while(my($collection_owner, $jid, $name, $to, $from, $ask) = $sth->fetchrow_array()) {

   my $user_id = &get_name_from_jid($collection_owner);
   my $contact = &get_name_from_jid($jid);
   if (!defined($name)) {
     $name = "";
   }
   my $state = 'none';
   my $pendingIn = 0;
   my $pendingOut = 0;
   if ($to == 0) {
     if ($from == 0) {
       $state = 'none';
     }
     else {
       $state = 'from';
     }
   }
   else {
     if ($from == 0) {
       $state = 'to';
     }
     else {
       $state = 'both';
     }
   }
   if (($to == 0) && ($ask == 1)) {
     $pendingOut = 1;
   }
   if (($from == 0) && ($ask == 2)) {
     $pendingIn = 1;
   }
   $user_id = &escape_string($user_id);
   $contact = &escape_string($contact);
   $name = &escape_string($name);
   print "INSERT into XmppSubscriptions (User_ID,Contact,State,PendingIn,PendingOut,ContactDisplayName,ContactGroup) VALUES ('$user_id','$contact','$state','$pendingIn','$pendingOut','$name','');\n";
} 

$sth = $dbh->prepare("select `collection-owner`,jid,`group` from `roster-groups`");
$sth->execute;

while(my($collection_owner, $jid, $group) = $sth->fetchrow_array()) {

   my $user_id = &escape_string(&get_name_from_jid($collection_owner));
   my $contact = &escape_string(&get_name_from_jid($jid));
   $group = &escape_string($group);
   print "UPDATE XmppSubscriptions set ContactGroup='$group' where User_ID='$user_id' and Contact='$contact';\n";
}

sub get_name_from_jid {

  my $jid = shift;
  my $index = index ($jid, "\@", 0);
  my $name = $jid;
  if ($index > 0) {
    $name = substr ($jid, 0, $index);
  }
  return $name;
}

sub escape_string {

  my $s1 = shift;

  $s1 =~ s/\\/\\\\/g;
  $s1 =~ s/\0/\\0/g;
  $s1 =~ s/\n/\\n/g;
  $s1 =~ s/\r/\\r/g;
  $s1 =~ s/\'/\\\'/g;
  $s1 =~ s/\"/\\\"/g;
  $s1 =~ s/\032/\\Z/g;

  return $s1;
}

