#!/usr/bin/perl -w
# set the data source name
# format: dbi:db type:db name:host:port
# mysql.s default port is 3306
# if you are running mysql on another host or port,
#    you must change it
use DBI;

sub escape_string;

my $dsn = 'dbi:mysql:eyeball:127.0.0.1:3306';

# set the user and password
my $user = 'root';
my $pass = '';

# now connect and get a database handle  
my $dbh = DBI->connect($dsn, $user, $pass)
 or die "Can.t connect to the DB: $DBI::errstr\n";
 
sub changeUserIDToAccountID{
 
    my($tablename) = @_;
    
    $sth = $dbh->prepare("select distinct($tablename.User_ID) as username, account.account_id as id from $tablename, account where $tablename.User_ID=account.user_id");
    $sth->execute;
    
    while(my($username, $id) = $sth->fetchrow_array()){
        runquery("update $tablename set account_id=$id where User_ID='$username';");
    }
}

sub runquery{

    my($query) = @_;
    print ("run: $query");
    $querysth = $dbh->prepare($query);
    $querysth->execute;
    print ("\t\tdone\n");
}


# -- --------------------------------------------------------------------------------------
runquery("rename table StunRelayStatistics to afwstatistics;");

runquery("alter table afwstatistics change Stats_index `afwstatistics_id` int(10) unsigned NOT NULL auto_increment;");
runquery("alter table afwstatistics change RecordTime  `recordtime` datetime NOT NULL default '1970-01-01 00:00:00';");
runquery("alter table afwstatistics change Server `afwserver_id` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwstatistics change Sessions `sessions` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwstatistics change SessionsMax  `sessionmax` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwstatistics change SessionsStarted  `sessionstarted` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwstatistics change SessionsEnded  `sessionended` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwstatistics change PeakAllocated  `peakallocated` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwstatistics change PeakAllocatedMax  `peakallocatedmax` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwstatistics change BytesToClient  `bytestoclient` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwstatistics change BytesFromClient  `bytesfromclient` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwstatistics change BytesToPeer  `bytestopeer` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwstatistics change BytesFromPeer  `bytesfrompeer` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwstatistics change StatsInterval  `writeinterval` int(10) unsigned NOT NULL default '0';");


# -- --------------------------------------------------------------------------------------
runquery("rename table StunRelaySummaryStatistics to afwsummary;");

runquery("alter table afwsummary change `date` `day` date NOT NULL default '1970-01-01';");
runquery("alter table afwsummary change server `afwserver_id` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwsummary change sessions `sessions` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwsummary change maxSessions `sessionmax` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwsummary change sessionsStarted `sessionstarted` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwsummary change sessionsEnded `sessionended` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwsummary change allocatedBandwidth `peakallocated` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwsummary change maxAllocatedBandwidth `peakallocatedmax` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwsummary change bytesToClient `bytestoclient` bigint(10) unsigned NOT NULL default '0';");
runquery("alter table afwsummary change bytesFromClient `bytesfromclient` bigint(10) unsigned NOT NULL default '0';");
runquery("alter table afwsummary change bytesToPeer `bytestopeer` bigint(10) unsigned NOT NULL default '0';");
runquery("alter table afwsummary change bytesFromPeer `bytesfrompeer` bigint(10) unsigned NOT NULL default '0';");
runquery("alter table afwsummary change intervalTotal `writeinterval` int(10) unsigned NOT NULL default '0';");


# -- --------------------------------------------------------------------------------------
runquery("rename table StunRelayUserStatistics to afwuserstatistics;");

runquery("alter table afwuserstatistics add `account_id` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwuserstatistics change `date` `day` date NOT NULL default '1970-01-01';");
runquery("alter table afwuserstatistics change bytesToPeer `bytestopeer` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwuserstatistics change bytesToClient `bytestoclient` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwuserstatistics change bytesFromPeer `bytesfrompeer` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwuserstatistics change bytesFromClient `bytesfromclient` int(10) unsigned NOT NULL default '0';");

# -- run the script to populate the account_id field

changeUserIDToAccountID("afwuserstatistics");

runquery("alter table afwuserstatistics drop primary key;");
runquery("alter table afwuserstatistics add primary key(`account_id`, `day`);");
runquery("alter table afwuserstatistics drop User_ID;");

# -- --------------------------------------------------------------------------------------
runquery("rename table StunRelayAccounts to afwaccount;");

runquery("alter table afwaccount change User_index `account_id` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwaccount change wiretap `wiretap` int(11) NOT NULL default '0';");
runquery("alter table afwaccount change afsStatus `afwstatus` int(11) NOT NULL default '1';");
runquery("alter table afwaccount change maxBandwidth `maxbandwidth` int(10) unsigned default NULL;");


# -- --------------------------------------------------------------------------------------

runquery("rename table StunRelayAllocationHistory to afwallocation;");

runquery("alter table afwallocation change Allocation_index `afwallocation_id` int(10) unsigned NOT NULL auto_increment;");
runquery("alter table afwallocation change User `account_id` int(10) unsigned default NULL;");
runquery("alter table afwallocation change Server `afwserver_id` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwallocation change ClientIP `clientip` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwallocation change ClientPort `clientport` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwallocation change AllocatedIP `allocatedip` int(10) unsigned default NULL;");
runquery("alter table afwallocation change AllocatedPort `allocatedport` int(10) unsigned default NULL;");
runquery("alter table afwallocation change ProtocolToClient `clientprotocol` varchar(3) default NULL;");
runquery("alter table afwallocation change ProtocolToPeer `peerprotocol` varchar(3) default NULL;");
runquery("alter table afwallocation change AllocationBegin `starttime` datetime default '1970-01-01 00:00:00';");
runquery("alter table afwallocation change AllocationEnd `endtime` datetime default NULL;");
runquery("alter table afwallocation change AllocatedBandwidth `bandwidth` int(10) unsigned default NULL;");
runquery("alter table afwallocation change CompletionStatus `status` varchar(15) default 'UNSPECIFIED';");
runquery("alter table afwallocation drop BytesToPeer;");
runquery("alter table afwallocation drop BytesToClient;");


# -- --------------------------------------------------------------------------------------

runquery("rename table StunRelayPermissionHistory to afwpermission;");

runquery("alter table afwpermission change Permission_index  `afwpermission_id` int(10) unsigned NOT NULL auto_increment;");
runquery("alter table afwpermission change Allocation_index `afwallocation_id` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwpermission change PeerIP `destinationip` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwpermission change PeerPort `destinationport` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwpermission change Begin `starttime` datetime default '1970-01-01 00:00:00';");
runquery("alter table afwpermission change End `endtime` datetime default NULL;");
runquery("alter table afwpermission change BytesToClient `bytestoclient` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwpermission change BytesFromClient `bytesfromclient` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwpermission change BytesToPeer `bytestopeer` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwpermission change BytesFromPeer `bytesfrompeer` int(10) unsigned NOT NULL default '0';");


# -- --------------------------------------------------------------------------------------

runquery("rename table StunRelayServerStatus to afwserver;");

runquery("alter table afwserver change server `afwserver_id` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwserver change `date` `recordtime` datetime NOT NULL default '1970-01-01 00:00:00';");
runquery("alter table afwserver change status `status` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwserver change udpip `udpip` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwserver change udpport `udpport` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwserver change tcpip `tcpip` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwserver change tcpport `tcpport` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwserver change tlsip `tlsip` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwserver change tlsport `tlsport` int(10) unsigned NOT NULL default '0';");


# -- --------------------------------------------------------------------------------------

runquery("rename table StunRelayShortTermCredentials to afwcredential;");

runquery("alter table afwcredential change User `account_id` int(10) unsigned NOT NULL default '0';");
runquery("alter table afwcredential change Username `username` varchar(32) NOT NULL default ' ';");
runquery("alter table afwcredential change Password `password` varchar(32) NOT NULL default '1';");
runquery("alter table afwcredential change Expiry `expires` datetime default '1970-01-01 00:00:00';");
