
#define _XOPEN_SOURCE
#include <stdlib.h>
#include <unistd.h>
#include <fstream>
#include <string>

#include "debug.h"
#include "log.h"

#include "processor_defines.h"

#include "core_globals.h"
#include "CoreConfigFile.h"

#include "string_utils.h"

#include "Socket.h"
#include "ServerMT.h"

#include "adminprocessor.h"

#include "Message.h"
#include "MessageProxy.h"
#include "simple_crypt.h"


using namespace std;

AdminProcessor::AdminProcessor()
{
  m_ResponseBuffer.clear();
  m_ResponseBuffer = "";
  m_iStatus = MSG_OK;

  // load the encrypted password for the CLI
#ifdef CLI_PASSWORD
  m_sCLIPassword = _loadCLIPassword(g_pCoreConf->m_sPassFile);
  m_iLoginCount  = 1;
  m_iLogInStatus = CLI_LOGIN_CHALLENGE;
  m_iPWChangeStatus = CLI_OLDPASS;
#endif

}// AdminProcessor::AdminProcessor()




AdminProcessor::~AdminProcessor()
{


}// AdminProcessor::~AdminProcessor()


#ifdef CLI_PASSWORD
string AdminProcessor::_loadCLIPassword(const string& sFilename)
{
  string password;

  if (sFilename != "")
  {
    password = Crypt::get_passwd(sFilename, "cli");
  }

  if( password == "" )
  {
    DB_AND_LOG("Cannot get password for user \"cli\" from file: " << sFilename);
  }
  else {
    DB("password loaded: "<<password);
  }
  return password;

}// string AdminProcessor::_loadCLIPassword(const string& sFilename)



int AdminProcessor::_saveCLIPassword(const string& sFilename, const string& passwd)
{
  int result = CLI_OK;

  if (!Crypt::set_passwd(sFilename, "cli", passwd)) 
    {
      DB_AND_LOG("could not write CLI password");
      result = CLI_ERROR;
    }

  DB("finished: "<<result);
  return result;

}// int AdminProcessor::_saveCLIPassword(string sFilename, string passwd)



void AdminProcessor::_changePassword(const string& content)
{
  //  m_iPWChangeStatus=CLI_NEWPASS2; // comment out to reset password
  if (m_iPWChangeStatus==CLI_OLDPASS)
    {
      if (content==m_sCLIPassword)
	{
	  m_iPWChangeStatus=CLI_NEWPASS1;
	  m_ResponseBuffer = "\r\nplease enter the new password: \377\373\001";
	  m_bPrompt = false;
	}
      else
	{
	  if (m_iLoginCount==3)
	    {
	      LOGL(2,"CLI: password failure");
	      m_ResponseBuffer = "\377\374\001\r\nfailed, password not changed\r\n\r\n";
	      m_iStatus= MSG_OK;
	    }
	  else
	    {
	      m_ResponseBuffer = "\r\nplease enter the old password: \377\373\001";
	      m_iLoginCount++;
	      m_bPrompt = false;
	    }
	}
    }
  else if (m_iPWChangeStatus==CLI_NEWPASS1)
    {
      m_iPWChangeStatus=CLI_NEWPASS2;
      m_sPassword1 = content;
      m_ResponseBuffer = "\r\nplease confirm the new password: \377\373\001";
      m_bPrompt = false;
    }
  else if (m_iPWChangeStatus==CLI_NEWPASS2)
    {
      if (m_sPassword1==content)
	{
	  if (_saveCLIPassword(g_pCoreConf->m_sPassFile,content)==CLI_OK)
	    {
	      m_sCLIPassword   = content;
	      m_ResponseBuffer = "\377\374\001\r\npassword changed\r\n";
	      LOGL(2,"CLI: password changed");
	      m_iStatus = MSG_OK;
	    }
	  else
	    m_ResponseBuffer = "\377\374\001\r\ncould not save new password\r\n";
	}
      else
	{// reset all variables
	  m_ResponseBuffer = "\377\374\001\r\npassword mismatch, not changed\r\n\r\n";
	  
	  m_sPassword1 = "";
	  m_iPWChangeStatus = CLI_OLDPASS;
	  m_iStatus = MSG_OK;
	}
      m_iLoginCount = 0;
    }

  m_iResponseMsgCount = 1;

}// void AdminProcessor::_changePassword(string content)
#endif


void AdminProcessor::_printHelp()
{
  
  m_ResponseBuffer = (string)"\r\n" +
    (string)"help                        -- show this help screen\r\n" +
    (string)"uptime                      -- total running time of the server\r\n" +
    (string)"connections | status        -- print connection statistics\r\n" +
    (string)"rotate log                  -- start using a new log file & save the previous one\r\n" +
    (string)"shutdown                    -- shutdown server\r\n" +
#ifdef CLI_PASSWORD
    (string)"passwd                      -- change the CLI password\r\n" +
#endif
    (string)"verbose [level]             -- print/set verbose level for logging (0-5)\r\n" +
    (string)"bye | quit | exit           -- terminate CLI session\r\n\r\n";

}// void AdminProcessor::_printHelp()



void AdminProcessor::_printUptime()
{
  string hpadding, mpadding, spadding;
  int hours, minutes, seconds;
  int iCurrentTime = time(NULL);
  int iStartTime = g_pServerMT->GetStartTime();
  hours   = (iCurrentTime-iStartTime)/3600;
  minutes = ((iCurrentTime-iStartTime)%3600)/60;
  seconds = (iCurrentTime-iStartTime)%60;
  if (hours<10)
    hpadding = "0";
  else
    hpadding = "";
  if (minutes<10)
    mpadding = "0";
  else
    mpadding = "";
  if (seconds<10)
    spadding = "0";
  else
    spadding = "";
  m_ResponseBuffer = "server running for "+hpadding+itos(hours)+":"+mpadding+itos(minutes)+":"+spadding+itos(seconds)+"\r\n\r\n";

}// void AdminProcessor::_printUptime()



IMessageProcessor* AdminProcessor::createNewInstance()
{
  AdminProcessor *pProc = new AdminProcessor();
  return pProc;
}// IMessageProcessor* AdminProcessor::createNewInstance()



bool AdminProcessor::isBusy()
{
  return false;
}// bool AdminProcessor::isBusy()



bool AdminProcessor::isFinal()
{
  return false;
}// bool AdminProcessor::isFinal()


int AdminProcessor::sendWelcome(string &sWelcome)
{
  int result = MSG_OK;

#ifdef CLI_PASSWORD
  sWelcome = "please enter the password: \377\373\001";
  m_bPrompt = false;
#else
  sWelcome = (string)"\377\374\001\r\n Welcome to " + 
	     g_pServerMT->GetServerName() + 
             (string)"\r\n\r\n Command Line Interface. Type 'help' for more options.\r\n\r\n";
#endif

  return result;

}// int AdminProcessor::sendWelcome(string &sWelcome)



int AdminProcessor::isValidMessage(const string &sBuf)
{

  LOG("AdminProcessor::isValidMessage not implemented");

  return MSG_INVALID;

}// int AdminProcessor::isValidMessage(const string &sBuf)




int AdminProcessor::isCompleteMessage(char *pBuf, int iBufLen)
{// always accept telnet messages
  return iBufLen;
}// int AdminProcessor::isCompleteMessage(char *pBuf, int iBufLen)




int AdminProcessor::processMessage(char *pBuf, int iBufLen, const string& IP, int port, int protocol, const string& recvdIP, int recvdPort, int recvdProto)
{
  DB_ENTER(AdminProcessor::processMessage);
  int result = MSG_INVALID;
  string content = "";
  char *szContent = new char[iBufLen+1];
  bzero(szContent,iBufLen+1);

  if (iBufLen>2)
    strncpy(szContent,pBuf,iBufLen-2);
  else
    strncpy(szContent,pBuf,iBufLen);
  content = szContent;

  m_bPrompt = true;

  m_iResponseMsgCount = 0;

  m_ResponseBuffer.clear();
  m_ResponseBuffer.reserve(1024);
  m_fromIP         = IP;
  m_fromPort       = port;
  m_fromProtocol   = protocol;

  //m_iLogInStatus   = CLI_LOGIN_SUCCESS; // comment out to set new password
  if (m_iStatus==CLI_SHUTDOWN)
    {
      if ((content=="y") || (content=="Y"))
	{
	  m_ResponseBuffer = "shutting down server...";
	  m_iStatus = MSG_CLOSE;
	  LOG("received shutdown message from CLI");
	  DB("received shutdown message from CLI");

	  m_bPrompt = false;

	  m_iStatus = MSG_CLOSE;
	  m_iResponseMsgCount=1;
	  result = MSG_RESPONSE_AVAIL;

	  g_pServerMT->Stop();
	}
      else
	{
	  m_iStatus = CLI_OK;
	  result = MSG_INVALID;
	}
    }
  else if (iBufLen==3) // Telnet command
    {
#ifdef DEBUG
      string chars_recvd;
      
      for (int i=0;i<iBufLen;i++)
	{
	  unsigned char c = pBuf[i];
	  chars_recvd += itos(c) + " ";
	  DB(c);
	}
      DB("received: "<<content.size()<<" '"<<chars_recvd<<"'");
#endif
      m_bPrompt = false;
      result = MSG_OK; // do not send a response
    }
  else
#ifdef CLI_PASSWORD
    if (m_iLogInStatus==CLI_LOGIN_CHALLENGE)
      {
	DB("checking password: " << content);

	if (content!=m_sCLIPassword)
	  {
	    if (m_iLoginCount==3)
	      {
		m_ResponseBuffer = "";
		m_iResponseMsgCount = 0;
		m_iStatus = MSG_CLOSE;
		DB_AND_LOGL(2,"CLI: password failure, auto logoff");
	      }
	    else
	      {
		m_ResponseBuffer = "\r\nplease enter the password: \377\373\001";
		m_iLoginCount++;
		m_iResponseMsgCount = 1;
	      }
	    m_bPrompt = false;
	  }
	else
	  {
	    m_iLogInStatus   = CLI_LOGIN_SUCCESS;

	    m_ResponseBuffer = (string)"\377\374\001\r\n\r\n Welcome to "
	      + g_pServerMT->GetServerName() 
              + (string)"\r\n\r\n Command Line Interface. Type 'help' for more options.\r\n\r\n";
	    m_iResponseMsgCount=1;

	    LOGL(2,"CLI: login from "<<IP);
	    DB("CLI: login from "<<IP);
	  }

	result = MSG_RESPONSE_AVAIL;

      }
    else if (m_iStatus==CLI_CHANGE_PASS)
      {
	_changePassword(content);
	m_iResponseMsgCount=1;
	result = MSG_RESPONSE_AVAIL;
      }
    else
#endif
      if (content=="help")
	{
	  _printHelp();
	  m_iResponseMsgCount=1;
	  result = MSG_RESPONSE_AVAIL;
	}
#ifdef CLI_PASSWORD
      else if (content=="passwd")
	{
	  m_iStatus    = CLI_CHANGE_PASS;
	  m_iLoginCount = 1;
	  m_iPWChangeStatus = CLI_OLDPASS;
	  m_ResponseBuffer = "please enter the old password:  \377\373\001";
	  m_bPrompt = false;
	  m_iResponseMsgCount=1;
	  result = MSG_RESPONSE_AVAIL;
	}
      else
#endif
	if (content=="status")
	  {
	    m_ResponseBuffer = g_pServerMT->GetPortInformation() +"\r\n";
	    m_iResponseMsgCount=1;
	    result = MSG_RESPONSE_AVAIL;
	  }
	else if (content=="connections")
	  {
	    m_ResponseBuffer = g_pServerMT->GetPortInformation() + "\r\n";
	    m_iResponseMsgCount=1;
	    result = MSG_RESPONSE_AVAIL;
	  }
	else if (content=="bye"||content=="quit"||content=="exit")
	  {
	    DB_AND_LOGL(2,"CLI: logoff");
	    m_ResponseBuffer = "exiting CLI...";

	    m_bPrompt = false;

	    m_iStatus = MSG_CLOSE;
	    m_iResponseMsgCount=1;
	    result = MSG_RESPONSE_AVAIL;
	  }
	else if (content=="shutdown")
	  {
	    m_ResponseBuffer = "Do you really want to shut down ? [Y/N] ";
	    m_bPrompt = false;
	    m_iStatus = CLI_SHUTDOWN;
	    m_iResponseMsgCount = 1;
	    result = MSG_RESPONSE_AVAIL;
	  }
	else if (content=="uptime")
	  {
	    _printUptime();
	    m_iResponseMsgCount=1;
	    result = MSG_RESPONSE_AVAIL;
	  }
	else if (content.find("verbose")!=string::npos)
	  {
	    char *endptr;
	    int pos = 7;
	    string substring = content.substr(pos,content.size()-pos+1);
	    long level = strtol(substring.c_str(),&endptr,10);

	    DB("setting verbose level to "<<level<<" "<<substring);

	    if ((level>=0)&&(level<=5)&&(level!=LONG_MIN)&&(level!=LONG_MAX)&&(endptr!=substring.c_str()))
	      {
		g_iVerboseLevel = level;
		m_ResponseBuffer = (string)"setting verbose level to "+itos(g_iVerboseLevel)+"\r\n\r\n";
		LOGL(2,"setting verbose level to "<<g_iVerboseLevel);
	      }
	    else
	      m_ResponseBuffer = (string)"current verbose level: "+itos(g_iVerboseLevel)+"\r\n\r\n";

	    m_iStatus = MSG_RESPONSE_AVAIL;
	    m_iResponseMsgCount=1;
	    result = MSG_RESPONSE_AVAIL;
	  }
	else if (content.find("rotate log")!=string::npos)
	  {
	    Log::rotate(g_pCoreConf->m_sLogFileName);
	    LOGL(2,"rotated log file");
	    DB("rotated log file");

	    m_ResponseBuffer = (string)"logfile rotated\r\n\r\n";

	    m_iStatus = MSG_RESPONSE_AVAIL;
	    m_iResponseMsgCount=1;
	    result = MSG_RESPONSE_AVAIL;
	  }
	else
	  {
	    DB("received unknown message: "<<content<<" '"<<szContent<<"'");
	    m_ResponseBuffer = " unknown command: "+content+(string)"\r\n Type 'help' for more options.\r\n\r\n";
	    m_iResponseMsgCount=1;
	    result = MSG_RESPONSE_AVAIL;
	  } 

  m_ResponseBuffer.reserve(m_ResponseBuffer.size());

  delete [] szContent;
  DB("finished: "<<result);

  return result;

}// int AdminProcessor::processMessage(char *pBuf, int iBufLen, string IP, int port, int proto, string recvdIP, int recvdPort, int recvdProto)


int AdminProcessor::getResponse(char *&pBuf, int &iBufLen, string &IP, int &port, int &protocol)
{
  DB_ENTER(AdminProcessor::getResponse);

  int result = MSG_INVALID;
  pBuf = NULL;
  iBufLen = 0;

  if (m_iResponseMsgCount>0)
    {
      if (m_bPrompt==true)
	m_ResponseBuffer += "/> ";

      pBuf = new char[m_ResponseBuffer.size()+1];
      bzero(pBuf,m_ResponseBuffer.size()+1);
      memcpy(pBuf,m_ResponseBuffer.c_str(),m_ResponseBuffer.size());
      iBufLen = m_ResponseBuffer.size();

      IP       = m_fromIP;
      port     = m_fromPort;
      protocol = m_fromProtocol;
      result   = MSG_OK;
      m_iResponseMsgCount--;

      DB("retrieving response: "<<pBuf);
    }
  
  if (m_iStatus==MSG_CLOSE)
    {// request to close the connection
      result = MSG_CLOSE;
    }

  m_ResponseBuffer.erase();
  m_ResponseBuffer.reserve(4);

  DB("finished: "<<result<<" "<<iBufLen);

  return result;

}// int AdminProcessor::getResponse(char *&pBuf, int &iBufLen, string &IP, int &port, int &protocol)



int AdminProcessor::getProcessorType()
{
  return ADMIN;
}// int AdminProcessor::getProcessorType()



int AdminProcessor::OnServiceNotify(int iServiceType, void *pResponse)
{
  return 0;
}// int AdminProcessor::OnServiceNotify(int iServiceType, void *pResponse)

