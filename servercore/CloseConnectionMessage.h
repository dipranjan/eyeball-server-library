// ---------------------------------------------------------------------------
// File:       CloseConnectionMessage.h
//
// Copyright:  Eyeball Networks Inc. 1999-2006
//
// Purpose:    Tell server to close a TCP or TLS connection
//
// Change Log: 03/01/2006 - Created
// ---------------------------------------------------------------------------

#ifndef __CLOSE_CONNECTION_MESSAGE_H_
#define __CLOSE_CONNECTION_MESSAGE_H_

#include "Message.h"
#include <string>

using std::string;
using std::ostream;

struct CloseConnectionMessage : public CommandMessage
{
	CloseConnectionMessage();
	virtual ~CloseConnectionMessage();

	// Convert to/from network byte order
	virtual void serialize(string& sBuf) const;
	virtual int  deserialize(const string& sBuf);

        virtual u_int16_t payload_length(void) const;

	//Converts string to 32-bit integer address
	//Returns false if address invalid or conversion failed
	bool setAddress(const string& sAddress);

	//Returns address as string
	string& getAddress(string& sAddress) const;

	u_int32_t m_u32Address;
	u_int16_t m_u16Port;
	u_int8_t m_u8Protocol;
	u_int8_t m_u8ServerMessageType;

#ifndef _WIN32_WCE
	friend ostream& operator<<(ostream& s, const CloseConnectionMessage& v);
#endif

};

#endif //__CLOSE_CONNECTION_MESSAGE_H_ 

