//ConnectionHashTable.cc
//Implementation of Connection Hash Table class
//Matches Address, Port, Protocol triple to Socket

#include "ConnectionHashTable.h"
#include <arpa/inet.h>
#include <string.h>

#include "debug.h"
#include "log.h"

//Constructor
CConnectionHashTable::CConnectionHashTable(int iMaxConnections)
{
	DB_ENTER(CConnectionHashTable::CConnectionHashTable);

	m_Table.init(iMaxConnections);
}

//Destructor
CConnectionHashTable::~CConnectionHashTable()
{
	DB_ENTER(CConnectionHashTable::~CConnectionHashTable);
}

//Put
//To remove element, put NULL for value
void CConnectionHashTable::Put(const string& sAddress, int iPort, int iProtocol, int iMessageType, void* value)
{
	DB_ENTER(CConnectionHashTable::Put1);

	string key = ToString(sAddress, iPort, iProtocol, iMessageType);

	if (key.size() == 8) {
		if (value == NULL) {
			m_Table.delFromTable(key);
            //LOGL(5, "=>> delete from hash table "<<sAddress<<" "<<iPort<<" "<<iProtocol<<" "<<iMessageType);
		}
		else {
			long* pValue = new long;
			*pValue = (long)value;
			m_Table.updateTable(key, pValue);
            //LOGL(5, "=>> update hash table "<<sAddress<<" "<<iPort<<" "<<iProtocol<<" "<<iMessageType);
		}
	}
}

//Put
//To remove element, put NULL for value 
//iAddress is in host byte order
void CConnectionHashTable::Put(int iAddress, int iPort, int iProtocol, int iMessageType, void* value)
{
	DB_ENTER(CConnectionHashTable::Put2);

	string key = ToString(iAddress, iPort, iProtocol, iMessageType);

	if (key.size() == 8) {
		if (value == NULL) {
			m_Table.delFromTable(key);
            //LOGL(5, "=>>? delete from hash table "<<iAddress<<" "<<iPort<<" "<<iProtocol<<" "<<iMessageType);
		}
		else {
			long* pValue = new long;
			*pValue = (long)value;
			m_Table.updateTable(key, pValue);
            //LOGL(5, "=>>? update hash table "<<iAddress<<" "<<iPort<<" "<<iProtocol<<" "<<iMessageType);
		}
	}
}

//Get
void* CConnectionHashTable::Get(const string& sAddress, int iPort, int iProtocol, int iMessageType)
{
	DB_ENTER(CConnectionHashTable::Get1);

    //LOGL(5, "=>> CConnectionHashTable::Get: "<<sAddress<<" "<<iPort<<" "<<iProtocol<<" "<<iMessageType);
	string key = ToString(sAddress, iPort, iProtocol, iMessageType);
    //for (int i=0; i<key.size(); i++) LOGL(5, "=>> key:"<<(int)(key[i]));
	if (key.size() == 8) {
		long value;
		if (m_Table.getFromTable(key, value)) {
            //LOGL(5, "=>> found and return");
			return (void*)value;
		}
		else {
            //LOGL(5, "=>> not found in table");
			return NULL;
		}
	}
	else {
        //LOGL(5, "=>> not found key size miss match");
		return NULL;
	}
}

//Get
//iAddress is in host byte order
void* CConnectionHashTable::Get(int iAddress, int iPort, int iProtocol, int iMessageType)
{
	DB_ENTER(CConnectionHashTable::Get2);

	string key = ToString(iAddress, iPort, iProtocol, iMessageType);
    //LOGL(5, "=>>? map get"<<iAddress<<" "<<iPort<<" "<<iProtocol<<" "<<iMessageType);
	if (key.size() == 8) {
		long value;
		if (m_Table.getFromTable(key, value)) {
            //LOGL(5, "=>>>? found and return ");
			return (void*)value;
		}
		else {
            //LOGL(5, "=>>>? not found in table");
			return NULL;
		}
	}
	else {
        //LOGL(5, "=>>? not found key size miss match");
		return NULL;
	}
}

//! Converts address(string), port, protocol to a string of 8 bytes
string CConnectionHashTable::ToString(const string& sAddress, int iPort, int iProtocol, int iMessageType) const
{
	DB_ENTER(CConnectionHashTable::ToString);

	struct in_addr address;
	int result = inet_pton(AF_INET, sAddress.c_str(), &address);

	if (result) {
		return ToString(ntohl(address.s_addr), iPort, iProtocol, iMessageType);
	}
	else {
		DB_AND_LOGL(3, "inet_pton failed for address: " << sAddress.c_str());
		return "";
	}
}

//! Converts address(int), port, protocol to a string of 7 bytes
//! Address in host byte order
string CConnectionHashTable::ToString(int iAddress, int iPort, int iProtocol, int iMessageType) const
{
	DB_ENTER(CConnectionHashTable::ToString2);

	DB(iAddress << ":" << iPort << "/" << iProtocol << "." << iMessageType);

	string buf;
	buf.resize(8);
	char* pBuf = (char*)buf.data();
	pBuf[0] = (char)(iAddress >> 24);
	pBuf[1] = (char)(iAddress >> 16);
	pBuf[2] = (char)(iAddress >> 8);
	pBuf[3] = (char)(iAddress);
	pBuf[4] = (char)(iPort >> 8);
	pBuf[5] = (char)iPort;
	pBuf[6] = (char)iProtocol;
	pBuf[7] = (char)iMessageType;

	return buf;
}

