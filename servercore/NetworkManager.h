//!  NetworkManager.h
//!  Interface for the Network Manager class

#ifndef _NETWORK_MANAGER_H_
#define _NETWORK_MANAGER_H_

#include <string>
#include <pthread.h>
#include <sys/epoll.h>
#include <pthread.h>
#include <deque>

using namespace std;

class ISocket;
class CConnectionHashTable;
class CTimeoutList;
class CThreadManager;
class CMessageProxy;
class CMessageScanner;
class IMessageProcessor;
class CommandMessage;

class CNetworkManager
{
public:
	//! Constructor
	CNetworkManager();

	//! Destructor
	~CNetworkManager();

	//! Get status of open ports
	string GetPortInformation();

	//! Get number of connections;
	int GetNumConnections();

	//! Set Message Proxy
	//! Determines what message types are supported
	void SetMessageProxy(const CMessageProxy* pMessageProxy);

	//! Set maximum client connections. Default: 10000 
	void SetMaximumClientConnections(int iNumConnections);

	//! Set maximum receive packet size. Default: 65536
	void SetMaxReceivePacketSize(int iNumBytes);

	//! Add UDP Server
	//! Returns true if successful
	bool AddUdpServer(string address, unsigned short port, int iMessageType);

	//! Add TCP Server
	//! Assume no connections yet and server not started yet
	//! Returns true if successful
	bool AddTcpServer(string address, unsigned short port, int iMessageType, int iMaxConnections);

	//! Add TLS Server
	//! Assume no connections yet and server not started yet
	//! Returns true if successful
	bool AddTlsServer(string address, unsigned short port, int iMessageType, int iMaxConnections);

	//! Start
	void Start();

	//! Stop
	void Stop();

	//! Close TCP/TLS Connection
	void CloseConnection(const string& sAddress, int iPort, int iProtocol, int iMessageType);

	//! Close TCP/TLS Connection later 
	void CloseConnectionLater(const string& sAddress, int iPort, int iProtocol, int iMessageType);

	//! Wait for Shutdown
	void WaitForShutdown();	

	//! Send Message 
	//! Network Manager will delete pcMessage
	void SendMessage(const string& sAddress, int iPort, int iProtocol, int iMessageType, 
			 char* pcMessage, int iLen, int iConvertToTLS,
                         bool bAddConnectionIfNecessary, const string& sTagForNewConnection);

	//! Process Server Command
	//! Called by Thread Manager
	void ProcessServerCommand(const char* pcMessage, int iLen);

	const CThreadManager* getThreadManager() const
	  {
	    return m_pThreadManager;
	  }


	//! retrieve the IP address of the local interface used to route messages to the target IP
	string GetOutboundIP(const string& sTargetIP, const int iPort, const int iProtocol, const int iMessageType);


protected:
	//! Main loop that polls sockets
	static void* NetworkManagerThread(void* arg);

	struct ServerInfo;
	struct StreamConnection;

	//! Add TCP/TLS Connection
	StreamConnection* AddConnection(const string& sAddress, int iPort, int iProtocol, ServerInfo* pServer, 
                                        const std::string& sTagForNewConnection);

	//! Convert TCP connection to TLS
	bool ConvertTCPToTLS(const string& sAddress, int iPort, int iMessageType);

	//! Close TCP/TLS Connection immediately and completely
	void CloseConnectionFinal(const string& sAddress, int iPort, int iProtocol, int iMessageType);

	//! Poll loop
	//! Called in NetworkManagerThread
	void PollLoop();

	int m_iEPollFd;

	pthread_t m_thread;
	int m_iCommandReadFd;
	int m_iCommandWriteFd;

	struct ServerInfo {
		int iMessageType;
		int iMaxConnections;
		int iNumConnections;
		ISocket* pSocket;
	};	

	ServerInfo* m_pUdpServers;
	ServerInfo* m_pTcpServers;
	ServerInfo* m_pTlsServers;

	int m_iNumUdpServers;
	int m_iNumTcpServers;
	int m_iNumTlsServers;

	struct StreamConnection {
		int iIndex;
		ServerInfo* pServer;
		ISocket* pSocket;
		char* pcRecvBuffer;
		int iRecvLen;
		IMessageProcessor* processor;
		int iTimeout;
	};

	StreamConnection** m_pConnections;	
	int m_iNumConnections;

	int m_iMaxAllowedConnections;

	//! Memory allocated and destroyed in thread
	CConnectionHashTable* m_pConnectionHashTable;
	
	//! Memory allocated and destroyed in thread
	CTimeoutList* m_pTimeoutList;

	const CMessageProxy* m_pMessageProxy;

	//! Memory allocated and destroyed in thread
	CThreadManager* m_pThreadManager;
	CMessageScanner* m_pMessageScanner;

	int m_iMaxRecvPacketSize;
	char* m_pcRecvBuffer;

	//! Memory allocated and destroyed in thread
	struct epoll_event* m_pEPollEvents;
	
	int m_iNumFixedSockets;
	bool m_bEndLoop;
    int m_iPollThreadID;
	bool bOverloadEntry;
    time_t iOverload_EntryTime;

protected:
	//! Process command 
	void ProcessCommand();

	//! Process UDP Server
	void ProcessUdpServer(ServerInfo* pServer);
	
	//! return true if message is received, else false
	bool ProcessUdpInternal(ServerInfo* pServer);

	//! Process TCP Server
	void ProcessTcpServer(ServerInfo* pServer);

	//! Process TLS Server
	void ProcessTlsServer(ServerInfo* pServer);

	//! Process Stream Connection
	void ProcessStreamConnection(StreamConnection* pConnection);

	//! Check Connection Timeouts
	void CheckConnectionTimeouts();

	//! Send TCP/TLS Message helper function
	//! Network Manager not delete pcMessage
	//! pConnection must not be NULL
	void SendTCPTLSMessageHelper(StreamConnection* pConnection, const string& sAddress, int iPort,
				     int iProtocol, int iMessageType, char* pcMessage, int iLen, bool& bClose, int iConvertToTLS);

	std::deque<CommandMessage*> m_CommandQueue;
	pthread_mutex_t m_CommandMutex;

	pthread_mutex_t m_ConnectionsMutex;
	pthread_mutex_t m_UdpEpollMutex;
};

#endif //_NETWORK_MANAGER_H_
