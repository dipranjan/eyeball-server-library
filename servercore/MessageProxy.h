//!  MessageProxy.h
//!  Interface of Message Proxy class that selects a Message Processor according to the message type
//!  Single-threaded -- to be used in conjunction with multi-threaded ThreadManager
//!  To be extended using subclasses

#ifndef _MESSAGE_PROXY_H_
#define _MESSAGE_PROXY_H_

#include <string>

using namespace std;

class CThreadManager;
class IMessageProcessor;

class CMessageProxy
{
public:
	//! Constructor
	CMessageProxy();

	//! Destructor
	virtual ~CMessageProxy();

	//! Creates Message Proxy of the same subclass 
	//! Need to Override 
	virtual CMessageProxy* createNewInstance() const;

	//! Set Thread Manager
	void SetThreadManager(CThreadManager* pThreadManager);

	//! Set Thread ID
	void SetThreadId(int iThreadId);

	//! Called by Thread Manager
	//! Selects a Message Processor to process message based on message type
	void ProcessMessage(const string& sToAddress, int iToPort, 
			    const string& sFromAddress, int iFromPort, int iProtocol,
			    int iMessageType, char* pcMessage, int iLen);

	//! Override this to handle more message types
	//! Only supports Telnet Admin in base implementation
	virtual IMessageProcessor* GetProcessorForMessageType(int iMessageType) const;

protected:
	CThreadManager* m_pThreadManager;
};

#endif //_MESSAGE_PROXY_H_



