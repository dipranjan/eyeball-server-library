#include "XMPPContactInfo.h"

ostream& operator<<(ostream& s, const XMPPContactInfo& v)
{
        s << v.m_sBareJid
          << " " << v.m_sDisplayName
          << " " << v.m_sGroup
          << " " << v.m_sSubscriptionState
          << (v.m_bPendingIn ? " Y" : " N")
          << (v.m_bPendingOut ? " Y" : " N");

        return s;
}

