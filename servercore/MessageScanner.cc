//MessageScanner.cc
//Implementation of CMessageScanner class
//Checks completeness and validity of message

#include "MessageScanner.h"
#include "MessageProxy.h"
#include "MessageProcessor.h"
#include "processor_defines.h"

#include "debug.h"
#include "log.h"

//Constructor
//CMessageProxy determines which message types are supported
CMessageScanner::CMessageScanner(const CMessageProxy* pMessageProxy)
: m_pMessageProxy(pMessageProxy),
  m_bMessageProxyNULL(false)
{
	DB_ENTER(CMessageScanner::CMessageScanner);

	if (pMessageProxy == NULL) {
		DB("pMessageProxy is NULL");
		m_bMessageProxyNULL = true;
		m_pMessageProxy = new CMessageProxy;
	}

	//Begin with fixed number of processors
	m_pProcessors = new IMessageProcessor*[NUM_PROCESSOR_TYPES];

	for (int i = 0; i < NUM_PROCESSOR_TYPES; i++) {
		DB("Get Processor for Message Type");
		m_pProcessors[i] 
		  = m_pMessageProxy->GetProcessorForMessageType(i);
	}
}

//Destructor
CMessageScanner::~CMessageScanner()
{
	DB_ENTER(CMessageScanner::~CMessageScanner);

	if (m_bMessageProxyNULL) {
		delete m_pMessageProxy;
	}

        if (m_pProcessors) {
		for (int i = 0; i < NUM_PROCESSOR_TYPES; i++) {
			delete m_pProcessors[i];
		}
		delete [] m_pProcessors;
	}
}

//Scan Message
//Calls IMessageProcessor::isCompleteMessage(char *pBuf, int iBufLen)
//
//Returns length > 0 if the message appears complete
//Otherwise, returns MESSAGE_SCAN_INCOMPLETE  if message is incomplete
//           returns MESSAGE_SCAN_UNSUPPORTED if message type is unsupported
int CMessageScanner::Scan(char *pBuf, int iBufLen, int iMessageType) const
{
	DB_ENTER(CMessageScanner::Scan);

	if ((iMessageType < 0) || (iMessageType >= NUM_PROCESSOR_TYPES)) {
		return MESSAGE_SCAN_UNSUPPORTED;
	}

	IMessageProcessor* pProcessor = m_pProcessors[iMessageType];

	if (pProcessor == NULL) {
		return MESSAGE_SCAN_UNSUPPORTED;
	}
	else {
		int result = pProcessor->isCompleteMessage(pBuf, iBufLen);
		if (result > iBufLen) { 
			//Expected length is bigger than buffer length 
			//Incomplete message
			return MESSAGE_SCAN_INCOMPLETE;
		}
		else if (result > 0) {
                        //Complete message OR
			//Expected length is smaller than packet
			//Complete message plus extra bytes
			return result; 
		}
		else {
			return MESSAGE_SCAN_INCOMPLETE;
		}
	}
}

//! Returns whether processor for message type supports multi-threading
bool CMessageScanner::SupportsMultiThreading(int iMessageType) const
{
	DB_ENTER(CMessageScanner::SupportMultiThreading);

	if ((iMessageType < 0) || (iMessageType >= NUM_PROCESSOR_TYPES)) {
		return false;
	}

	IMessageProcessor* pProcessor = m_pProcessors[iMessageType];
    if (pProcessor == NULL) {
		return false;
	}
	else {
		return pProcessor->isMT();
	}
}



