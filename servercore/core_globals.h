////////////////////////////////////////////////////////////////////////////////
//
//
//
// globals variables for the server core
//
// 08/08/2005 (larsb)
//
////////////////////////////////////////////////////////////////////////////////

/*
  $Id: core_globals.h,v 1.5 2006/09/21 00:21:29 zionk Exp $
*/



#ifndef _COREGLOBALS_H
#define _COREGLOBALS_H

#include <time.h>
#include <pthread.h>
#include <limits.h>

#define INFINITE                INT_MAX


// global definitions
#define MAXMSGSIZE              65536


#define MAXCONNECTIONS          4871


#define TCP_CONNECTION_TIMEOUT  INFINITE
#define TCP_CONNECTIONS         90000

#define CLI_TIMEOUT             60*60 // one hour cli connection timeout

#ifndef ADMIN_PORT
#define ADMIN_PORT              7010
#endif

#ifndef FORWARD_MESSAGE_PORT
#define FORWARD_MESSAGE_PORT	7020
#endif





#ifdef CLI_PASSWORD
#include <string>
using namespace std;

extern string g_sCLIPassword;
#endif









//! the mutex protecting message processor deletion
//! initialized by server

//DNS
//extern pthread_mutex_t *g_pDeleteMutex;


//void init_globals();


#endif




/*
  $Log: core_globals.h,v $
  Revision 1.5  2006/09/21 00:21:29  zionk
  Increase default max connections to 90000

  Revision 1.4  2006/07/18 16:55:13  zionk
  Move g_Starttime to member variable in CServerMT

  Revision 1.3  2006/04/27 23:15:30  zionk
  Add forward message udp and tcp ports

  Revision 1.2  2006/03/30 18:13:26  zionk
  Patch Lars' changes in Proxd

  Revision 1.1  2006/03/07 18:12:00  zionk
  Multi-threaded server core

  Revision 1.8  2005/12/19 01:22:55  larsb
  fixed bug in global mutex initialization

  Revision 1.7  2005/10/22 00:46:49  larsb
  added 100 Trying
  fixed IP:port routing problem

  Revision 1.6  2005/10/03 20:54:25  larsb
  added some config parameters

  Revision 1.5  2005/09/15 23:28:43  larsb
  *** empty log message ***

  Revision 1.4  2005/09/02 22:17:11  larsb
  clean up and polishing

  Revision 1.3  2005/08/18 18:11:35  larsb
  moved int g_iVerboseLevel to log.h/.cc (utils/)

  Revision 1.2  2005/08/17 00:06:32  larsb
  *** empty log message ***

  Revision 1.1  2005/08/08 18:55:57  larsb
  reoriganisation of command line interface (adminprocessor) and global variables

*/
