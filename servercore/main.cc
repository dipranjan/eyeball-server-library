/***************************************************************************
                          main.cpp  -  description
                             -------------------
    begin                : Tue Oct 12 10:13:05 BDT 2004
    copyright            : (C) Eyeball Networks Inc.|2004| by Zunnun
2004    email                : zunnun@engr.eyeball.com

   Updated to Server Core MT by Zion 
   March 2, 2006
 ***************************************************************************/


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <fstream>
#include <signal.h>  

#ifndef WIN32
#include <netinet/in.h>
#include <errno.h>
#include <fcntl.h>
#endif

// utilities from libutil.a
#include "log.h"
#include "debug.h"


// server base modules
#include "ServerMT.h"
#include "CoreConfigFile.h"
#include "core_globals.h"

//#define _USE_LICENSE_
using namespace std;

// Global variables 

CServerMT* g_pServerMT = NULL;
CCoreConfigFile* g_pCoreConf = NULL;

// Static variables

static CCoreConfigFile conf;
static string sConfFile;

// Function Prototypes
static void signal_handler(int iSigNum);
static void signal_handler_filesize(int signum);
static void RemovePidFile(void);
static void CleanUp(void);

// Options
#include "options.h"
#define EXPECTED_NPOS1  0

static void brief_usage(const char *prog_name, const char *reason)
{
        if (reason) {
                cerr << prog_name << ": " << reason << endl;
	}
        cerr << "Try `" << prog_name << " --help' for more information." << endl;
        return;
}

//-------------------------------------------------------------------------
int main(int argc, const char *argv[])
{
  int i;	

#ifdef _DEBUG_
  static char ServerPathName[1000];
  getcwd(ServerPathName, sizeof(ServerPathName));
  cout<<"#########"<<ServerPathName<<"#########\n";
#endif

  int iVerboseLevel = 0;
  sConfFile = "servercoremt.conf";
  bool bRunFgnd = false;

  static char ServerFileName[100];

  //Added by Farhan
  for(i = strlen(argv[0]) - 1; argv[0][i] != '/' && i >= 0; i--);
  i = (i < 0) ? 0 : i + 1;
  strncpy(ServerFileName, &argv[0][i], 99);

  //Addition ends here
  /***********
        Options code
  ************/
  static const char * const optv[] =
    {
      "v:verbose",
      "c:configfilename",
      "V|version",
      "e:ee",
      "h|help",
      "n|nolicense",
      "f|foreground",
      NULL
    };
        
  int optchar;
  const char *optarg;
  int npos(0);

  Options opts(*argv, optv);
  opts.ctrls(Options::PARSE_POS);

  OptArgvIter iter(--argc, ++argv);
  while ((optchar = opts(iter, optarg)))
    {
      switch (optchar)
	{
	case 'v':					//v:verbose
	  if( !optarg )
	    {
	      brief_usage(opts.name(), NULL);
	      return -1;
	    }
	  iVerboseLevel = atoi(optarg);
	  if(iVerboseLevel > 5 || iVerboseLevel < 0)
	    {
	      iVerboseLevel = -1;
	      cout << "Invalid verbose level" << endl;
	      return -1;
	    }
	  break;

	case 'c':					//c:configfilename
	  if( !optarg )
	    {
	      brief_usage(opts.name(), NULL);
	      return -1;
	    }
	  sConfFile = optarg;
	  break;

	case 'V':					//V|version
	  cout << "Eyeball Server Core MT 0.1 built on " << __DATE__ << " " << __TIME__ << endl;
	  return 0;
	  break;

	case 'h':					//h|help
	  cout << "Usage: " << ServerFileName << " [OPTIONS] " << endl << endl;
	  cout << "-h, --help\t\tdisplay this help and exit" << endl;
	  cout << "-c, --configfilename\tspecify configuration file" << endl;
          cout << "-f, --foreground\truns " << ServerFileName << " in foreground" << endl;
	  cout << "-V, --Version\t\toutput version information and exit" << endl;
	  cout << "-v, --verbose\t\tset verbosity level" << endl;
	  return 0;
	  break;

        case 'f':                                       //f|foreground
          bRunFgnd = true;
          break;

	case Options::POSITIONAL:
	  // Push positional arguments to the front
	  argv[npos++] = optarg;
	  break;

	case Options::BADCHAR:		// bad option ("-%c", *optarg)
	case Options::BADKWD:		// bad long-option ("--%s", optarg)
	case Options::AMBIGUOUS:	// ambiguous long-option ("--%s", optarg)
	default:
	  brief_usage(opts.name(), NULL);
	  return -1;
	  break;
	}
    }

  switch(npos)
    {
    case EXPECTED_NPOS1:
      // OK
      break;

    default:
      brief_usage(opts.name(), "Incorrect number of arguments");
      return -1;
    }
        
  // Prints Server name & version
  cout<<"\n---------------------------------------------------------------------------"<<endl;
  cout<<"\tEyeball Server Core MT"<<endl<<"\tVersion 0.1"<<endl;
  cout<<"---------------------------------------------------------------------------"<<endl;

  cout << "Reading Configuration Script\n";

  if(!conf.ReadFile(sConfFile.c_str()))
    {
      sleep(1);
      cerr << "\nError Reading Configuration file\nCannot start Eyeball Server Core 0.1\n";
      return -1;
    }

  cout << conf.getSettings();
  g_pCoreConf = &conf;

  // Process id if run in background
  pid_t pid = 0;		

  if (!bRunFgnd) {
      pid = fork();

      if(pid == -1)
	{
	  if(iVerboseLevel >= 2 && conf.m_bLogEnabled)
	    LOG("Could not create child process " << strerror(errno));
	  return -1;
	}                                
      else if(pid != 0)
	{
	  printf("Starting Eyeball Server Core MT 0.1\n");
	  exit(1);
	}
  }


  g_iVerboseLevel = iVerboseLevel;

  cout << "Background Process Started [OK]" <<endl;
           
  if(!access(conf.m_sPidFileName.c_str(), R_OK))
    {
      cerr << "Pid file exists: " << conf.m_sPidFileName << "\nExiting" << endl;
      return -1;
    }
        
  if (!LOG_OPEN(conf.m_sLogFileName.c_str()))
    {
      cerr << "Can't open log file " << conf.m_sLogFileName << endl;
      return -1;
    }

  LOG(conf.getSettings());

  if(SIG_ERR == signal(SIGXFSZ,signal_handler_filesize))
    {
      if(iVerboseLevel >= 2 && conf.m_bLogEnabled)
	LOG("Could not install signal handler for SIGXFSZ " << strerror(errno));
      return -1;
    }

  if(NULL == freopen(conf.m_sOutFileName.c_str(), "w+", stderr))
    {
      freopen(conf.m_sOutFileName.c_str(), "w", stderr);
    }

  /* PID file */
  pid = getpid();
  FILE *pidFile;

  pidFile = fopen(conf.m_sPidFileName.c_str(), "w");
  fprintf(pidFile, "%d\n", pid);
  fclose(pidFile);
  /* PID file */

  CServerMT server("Eyeball Server Core MT 0.1");
  
  //g_pServerMT must be initialized before signal handlers
  g_pServerMT = &server; 

  // Install handlers
#define SIGNAL(X, Y) \
        if(SIG_ERR == signal(X, Y)) \
        { \
                if(conf.m_bLogEnabled) \
                        LOG("Could not install signal handler for " << #X << " " << strerror(errno)); \
                g_pServerMT->Stop(); \
                return -1; \
        }

  SIGNAL(SIGINT, signal_handler);
  SIGNAL(SIGTERM, signal_handler);
  SIGNAL(SIGQUIT, signal_handler);
  //SIGNAL(SIGALRM, signal_handler);
  SIGNAL(SIGHUP, signal_handler);
  //SIGNAL(SIGPIPE, SIG_IGN);
  
  server.Run(&conf);

  //Shut down server
  RemovePidFile();

  cout << "SERVER SHUTDOWN\t\t\t [OK]" << endl;  
  DB("SERVER SHUTDOWN\t\t\t [OK]");
  if(conf.m_bLogEnabled) LOG("SERVER is exiting");

  CleanUp();

  return EXIT_SUCCESS;
}

//Remove the pid file
void RemovePidFile(void)
{
	static bool bPidFileRemoved = false;

        if(bPidFileRemoved)
                return;

	string sRemovePidCommand = "unlink ";
	sRemovePidCommand += conf.m_sPidFileName;
        
        if(conf.m_bLogEnabled) LOG("Removing pid file: " << conf.m_sPidFileName);
        system(sRemovePidCommand.c_str());
        
        bPidFileRemoved = true;
}
//Clean up code
void CleanUp(void)
{
	LOG_CLOSE();
        fflush(stderr);
        fflush(stdout);
}

/**/
void signal_handler(int iSigNum)
{
        switch(iSigNum)
        {
                case SIGTERM:
			DB("Received SIGTERM");
                        if(conf.m_bLogEnabled) LOG("Received SIGTERM");
			g_pServerMT->Stop();
                        break;
                case SIGINT:
			DB("Received SIGINT");
                        if(conf.m_bLogEnabled) LOG("Received SIGINT");
			g_pServerMT->Stop();
                        break;
                case SIGQUIT:
			DB("Received SIGQUIT");
                        if(conf.m_bLogEnabled) LOG("Received SIGQUIT");
			g_pServerMT->Stop();
                        break;
                case SIGALRM:
			DB("Received SIGALRM");
                        if(conf.m_bLogEnabled) LOG("Received SIGALRM");
			g_pServerMT->Stop();
                        break;
                case SIGHUP:
			DB("Received SIGHUP");
                        if(conf.m_bLogEnabled) LOG("Received SIGHUP");
			conf.ReadFile(sConfFile.c_str());
                        break;
                default:
			DB("Received other signal: " << iSigNum);
                        if(conf.m_bLogEnabled) LOG("Other signal: " << iSigNum << " received");
                        break;
        }

        return;
}
/**/

//-------------------------------------------------------------------------------------------------------------
void signal_handler_filesize(int signum)
{
        conf.m_bLogEnabled = false;
        cerr << "Received SIGXFSZ, removing pid file." << endl;
        cerr << "Message logging is now Disabled.\n";
        cout << "Received SIGXFSZ, removing pid file." << endl;
        cout << "Message logging is now Disabled.\n";
}
//-------------------------------------------------------------------------------------------------------------


