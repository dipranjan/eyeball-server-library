// NetworkManager.cc
// Implementaton of the Network Manager class

#include "NetworkManager.h"
#include "ThreadManager.h"
#include "MessageScanner.h"
#include "MessageProxy.h"
#include "MessageProcessor.h"
#include "Socket.h"
#include "ConnectionHashTable.h"
#include "TimeoutList.h"
#include "string_utils.h"
#include "Message.h"
#include "CloseConnectionMessage.h"
#include "SendToMessage.h"
#include "MessageProcessor.h"
#include "core_globals.h"
#include "CoreConfigFile.h"
#include "processor_defines.h"
#include <sys/poll.h>
#include <CpuUsage.h>

#include "debug.h"
#include "log.h"


//#ifdef DB_ENTER
//#undef DB_ENTER
//#endif

//#define DB_ENTER(func)   //cout<<#func<<endl;
//Constructor
CNetworkManager::CNetworkManager()
    : m_iEPollFd(-1),
      m_thread((pthread_t)-1),
      m_iCommandReadFd(-1),
      m_iCommandWriteFd(-1),
      m_pUdpServers(NULL),
      m_pTcpServers(NULL),
      m_pTlsServers(NULL),
      m_iNumUdpServers(0),
      m_iNumTcpServers(0),
      m_iNumTlsServers(0),
      m_pConnections(NULL),
      m_iNumConnections(0),
      m_iMaxAllowedConnections(10000),
      m_pConnectionHashTable(NULL),
      m_pTimeoutList(NULL),
      m_pMessageProxy(NULL),
      m_pThreadManager(NULL),
      m_pMessageScanner(NULL),
      m_iMaxRecvPacketSize(65536),
      m_pcRecvBuffer(NULL),
      m_pEPollEvents(NULL),
      m_iNumFixedSockets(2),
      m_bEndLoop(false),
      m_iPollThreadID(-1)
{
    DB_ENTER(CNetworkManager::CNetworkManager);

    m_pcRecvBuffer = new char[m_iMaxRecvPacketSize];
    pthread_mutex_init(&m_CommandMutex, NULL);
    pthread_mutex_init(&m_ConnectionsMutex, NULL);
    pthread_mutex_init(&m_UdpEpollMutex, NULL);
}

//Destructor
CNetworkManager::~CNetworkManager()
{
    DB_ENTER(CNetworkManager::~CNetworkManager);

    Stop();
    WaitForShutdown();

    int i;
    for (i = 0; i < m_iNumUdpServers; i++) {
        m_pUdpServers[i].pSocket->close();
        delete m_pUdpServers[i].pSocket;
    }
    delete [] m_pUdpServers;

    for (i = 0; i < m_iNumTcpServers; i++) {
        m_pTcpServers[i].pSocket->close();
        delete m_pTcpServers[i].pSocket;
    }
    delete [] m_pTcpServers;

    for (i = 0; i < m_iNumTlsServers; i++) {
        m_pTlsServers[i].pSocket->close();
        delete m_pTlsServers[i].pSocket;
    }
    delete [] m_pTlsServers;

    delete [] m_pcRecvBuffer;

    if (m_iCommandReadFd != -1) {
        ::close(m_iCommandReadFd);
        ::close(m_iCommandWriteFd);
    }

    while (!m_CommandQueue.empty()) {
        CommandMessage* pMsg = m_CommandQueue.front();
        m_CommandQueue.pop_front();
        delete pMsg;
    }

    pthread_mutex_destroy(&m_CommandMutex);
    pthread_mutex_destroy(&m_ConnectionsMutex);
    pthread_mutex_destroy(&m_UdpEpollMutex);
}

//Get status of open ports
string CNetworkManager::GetPortInformation()
{
    DB_ENTER(CNetworkManager::GetPortInformation);

    string sInfo;
    string sEndl = "\r\n";
    int i;
    StreamConnection *pConnection;
    ServerInfo* pInfo;
    ISocket* pSocket;

    sInfo = (string)"Port Information" + sEndl +
            (string)"================" + sEndl;

    if (m_iNumUdpServers > 0) {
        sInfo += (string)"UDP Servers:" + sEndl;
        for (i = 0; i < m_iNumUdpServers; i++) {
            pInfo = &(m_pUdpServers[i]);

            sInfo += (string)"Message Type: " +
                    itos(pInfo->iMessageType) + " -- ";

            pSocket = pInfo->pSocket;
            if (pSocket != NULL) {
                sInfo += pSocket->getLocalAddr() + (string)":" +
                        itos(pSocket->getLocalPort()) + sEndl;
            }
            else {
                sInfo += (string)"Socket uninitialized" + sEndl;
            }
        }
    }

    if (m_iNumTcpServers > 0) {
        sInfo += (string)"TCP Servers:" + sEndl;
        for (i = 0; i < m_iNumTcpServers; i++) {
            pInfo = &(m_pTcpServers[i]);

            sInfo += (string)"Message Type: " +
                    itos(pInfo->iMessageType) + " -- ";

            pSocket = pInfo->pSocket;
            if (pSocket != NULL) {
                sInfo += pSocket->getLocalAddr() + (string)":" +
                        itos(pSocket->getLocalPort()) +
                        (string)" " + itos(pInfo->iNumConnections) +
                        (string)"/" + itos(pInfo->iMaxConnections) +
                        (string)" connections" + sEndl;
            }
            else {
                sInfo += (string)"Socket uninitialized" + sEndl;
            }
        }
    }

    if (m_iNumTlsServers > 0) {
        sInfo += (string)"TLS Servers:" + sEndl;
        for (i = 0; i < m_iNumTlsServers; i++) {
            pInfo = &(m_pTlsServers[i]);

            sInfo += (string)"Message Type: " +
                    itos(pInfo->iMessageType) + " -- ";

            pSocket = pInfo->pSocket;
            if (pSocket != NULL) {
                sInfo += pSocket->getLocalAddr() + (string)":" +
                        itos(pSocket->getLocalPort()) +
                        (string)" " + itos(pInfo->iNumConnections) +
                        (string)"/" + itos(pInfo->iMaxConnections) +
                        (string)" connections" + sEndl;
            }
            else {
                sInfo += (string)"Socket uninitialized" + sEndl;
            }
        }
    }

    if (m_iNumConnections > 0) {
        sInfo += (string)"Client Connections: " + itos(m_iNumConnections) + sEndl;
        int iMaxShowConnections = m_iNumConnections;
        if (iMaxShowConnections > 100) {
            iMaxShowConnections = 100;
        }
        for (i = 0; i < iMaxShowConnections; i++) {
            pConnection = m_pConnections[i];
            pInfo = pConnection->pServer;

            if (pInfo != NULL) {
                sInfo += (string)"Message Type: " +
                        itos(pInfo->iMessageType) + " -- ";
            }
            else {
                sInfo += (string)"Message Type unknown";
            }

            pSocket = pConnection->pSocket;
            if (pSocket != NULL) {
                sInfo += pSocket->getLocalAddr() + (string)":" +
                        itos(pSocket->getLocalPort()) +
                        (string)" <=> " +
                        pSocket->getRemoteAddr() + (string)":" +
                        itos(pSocket->getRemotePort()) + sEndl;
            }
            else {
                sInfo += (string)"Socket uninitialized" + sEndl;
            }
        }
    }

    sInfo += (string)"================" + sEndl;
    return sInfo;
}

//! Get number of connections;
int CNetworkManager::GetNumConnections()
{
    DB_ENTER(CNetworkManager::GetNumConnections);

    return m_iNumConnections;
}

//Set Message Proxy
//Determines what message types are supported
void CNetworkManager::SetMessageProxy(const CMessageProxy* pMessageProxy)
{
    DB_ENTER(CNetworkManager::SetMessageProxy);

    m_pMessageProxy = pMessageProxy;
}

//Set maximum client connections. Default: 10000 
void CNetworkManager::SetMaximumClientConnections(int iNumConnections)
{
    DB_ENTER(CNetworkManager::SetMaximumClientConnections);

    m_iMaxAllowedConnections = iNumConnections;
}

//Set maximum receive packet size. Default: 65536
void CNetworkManager::SetMaxReceivePacketSize(int iNumBytes)
{
    DB_ENTER(CNetworkManager::SetMaxReceivePacketSize);

    if ((m_iMaxRecvPacketSize != iNumBytes) && (iNumBytes > 0)) {
        delete [] m_pcRecvBuffer;
        m_pcRecvBuffer = new char[iNumBytes];
        m_iMaxRecvPacketSize = iNumBytes;
    }
}

//Add UDP Server
//Returns true if successful
bool CNetworkManager::AddUdpServer(string address, unsigned short port, int iMessageType)
{
    DB_ENTER(CNetworkManager::AddUdpServer);

    ISocket* pNewSocket = new UDPSocket;
    if (pNewSocket->createSocket(0, address, port) < 0) {
        DB_AND_LOG("Failed to create UDP Server Socket at "
                   << address << ":" << port);

        delete pNewSocket;
        return false;
    }

    ServerInfo* tmp = m_pUdpServers;

    m_pUdpServers = new ServerInfo[m_iNumUdpServers + 1];
    for (int i = 0; i < m_iNumUdpServers; i++) {
        m_pUdpServers[i] = tmp[i];
    }
    delete [] tmp;

    ServerInfo* pNewServer = &(m_pUdpServers[m_iNumUdpServers]);

    pNewServer->iMessageType = iMessageType;
    pNewServer->pSocket = pNewSocket;
    m_iNumUdpServers++;
    return true;
}

//Add TCP Server
//Assume no connections yet and server not started yet
//Returns true if successful
bool CNetworkManager::AddTcpServer(string address, unsigned short port, int iMessageType, int iMaxConnections)
{
    DB_ENTER(CNetworkManager::AddTcpServer);

    ISocket* pNewSocket = new TCPSocket;
    if (pNewSocket->createSocket(0, address, port) < 0) {
        DB_AND_LOG("Failed to create TCP Server Socket at "
                   << address << ":" << port);

        delete pNewSocket;
        return false;
    }

    ServerInfo* tmp = m_pTcpServers;

    m_pTcpServers = new ServerInfo[m_iNumTcpServers + 1];
    for (int i = 0; i < m_iNumTcpServers; i++) {
        m_pTcpServers[i] = tmp[i];
    }
    delete [] tmp;

    ServerInfo* pNewServer = &(m_pTcpServers[m_iNumTcpServers]);

    pNewServer->iMessageType = iMessageType;
    pNewServer->iMaxConnections = iMaxConnections;
    pNewServer->iNumConnections = 0;
    pNewServer->pSocket = pNewSocket;
    m_iNumTcpServers++;
    return true;
}

//Add TLS Server
//Assume no connections yet and server not started yet
//Returns true if successful
bool CNetworkManager::AddTlsServer(string address, unsigned short port, int iMessageType, int iMaxConnections)
{
    DB_ENTER(CNetworkManager::AddTlsServer);

    ISocket* pNewSocket = new TLSSocket;
    if (pNewSocket->createSocket(0, address, port) < 0) {
        DB_AND_LOG("Failed to create TLS Server Socket at "
                   << address << ":" << port);

        delete pNewSocket;
        return false;
    }

    ServerInfo* tmp = m_pTlsServers;

    m_pTlsServers = new ServerInfo[m_iNumTlsServers + 1];
    for (int i = 0; i < m_iNumTlsServers; i++) {
        m_pTlsServers[i] = tmp[i];
    }
    delete [] tmp;

    ServerInfo* pNewServer = &(m_pTlsServers[m_iNumTlsServers]);

    pNewServer->iMessageType = iMessageType;
    pNewServer->iMaxConnections = iMaxConnections;
    pNewServer->iNumConnections = 0;
    pNewServer->pSocket = pNewSocket;
    m_iNumTlsServers++;
    return true;
}


string CNetworkManager::GetOutboundIP(const string& sTargetIP, const int iPort, const int iProtocol, const int iMessageType)
{
    string sOutboundIP;
    StreamConnection* pConnection
            = (StreamConnection*)m_pConnectionHashTable->Get(sTargetIP, iPort, iProtocol, iMessageType);

    if (pConnection&&pConnection->pSocket)
    {
        sOutboundIP = pConnection->pSocket->getLocalAddr();
    }
    else
    {// ToDo: establish connection to the destination to get the IP of the local interface
        // we ignore this for the moment in order to save time, we do not want to delay the message processing
    }

    return sOutboundIP;

}// string CNetworkManager::GetOutboundIP(const string& sTargetIP, const int iPort, const int iProtocol, const int iMessageType)




//Start
void CNetworkManager::Start()
{
    DB_ENTER(CNetworkManager::Start);

    if (m_iCommandReadFd == -1) {
        int iSocketPairFd[2];

        int result =
                socketpair(AF_UNIX, SOCK_DGRAM, 0, iSocketPairFd);

        if (result == 0) {
            // set non-blocking
            int flags = fcntl(iSocketPairFd[0], F_GETFL, 0);
            flags |= O_NONBLOCK | O_NDELAY;

            if (fcntl(iSocketPairFd[0], F_SETFL, flags) == -1) {
                ::close(iSocketPairFd[0]);
                ::close(iSocketPairFd[1]);

                DB_AND_LOG("Error: cannot set Command Read Socket to non-blocking");
                DB_AND_LOG("Network Manager not started.");
                return;
            }

            m_iCommandReadFd = iSocketPairFd[0];
            m_iCommandWriteFd = iSocketPairFd[1];
        }
        else {
            DB_AND_LOG("Error: Cannot create socket pair.");
            DB_AND_LOG("Network Manager not started.");
            return;
        }
    }

    m_bEndLoop = false;
    pthread_create(&m_thread, NULL, NetworkManagerThread, this);
}

//Stop
void CNetworkManager::Stop()
{
    DB_ENTER(CNetworkManager::Stop);

    CommandMessage* stopmsg = new CommandMessage(EYEBALL_SERVER_STOP);

    pthread_mutex_lock(&m_CommandMutex);
    m_CommandQueue.push_back(stopmsg);
    if (m_CommandQueue.size() == 1) {
        char dummy = 1;
        ::write(m_iCommandWriteFd, &dummy, 1);
    }
    pthread_mutex_unlock(&m_CommandMutex);
}

//Close TCP/TLS Connection
void CNetworkManager::CloseConnection(const string& sAddress, int iPort, int iProtocol, int iMessageType)
{
    DB_ENTER(CNetworkManager::CloseConnection);

    bool bLockedPollThread = (m_iPollThreadID == (int)pthread_self());
    //If in same thread, likely called in ProcessStreamConnection for single-threaded
    //message processor -- already in mutex lock. Avoiding double lock/unlock

    bool bLockedWorkerThread = false;
    if (!bLockedPollThread) {
        bLockedWorkerThread = (pthread_mutex_trylock(&m_ConnectionsMutex) == 0);
    }

    if (bLockedPollThread || bLockedWorkerThread) {
        //Don't block worker thread on close

        //Search for address, port, protocol in Hash Table
        StreamConnection* pConnection
                = (StreamConnection*)m_pConnectionHashTable->Get(
                    sAddress, iPort, iProtocol, iMessageType);

        if (pConnection == NULL) {
            DB_AND_LOGL(5, "Note: cannot find connection to close: "
                        << sAddress << ":" << iPort << "/" << iProtocol << "." << iMessageType);
        }
        else {
            //Remove binding
            ISocket* pSocket = pConnection->pSocket;

            if (pSocket->getFd() != -1) {
                if ((sAddress  != pSocket->getRemoteAddr()) ||
                        (iPort     != pSocket->getRemotePort()) ||
                        (iProtocol != pSocket->getProtocol()))
                {
                    DB_AND_LOGL(2, "Trying to close socket " << pSocket->getFd() << " at "
                                << sAddress << ":" << iPort << "/" << iProtocol
                                << " but found "
                                << pSocket->getRemoteAddr() << ":" << pSocket->getRemotePort()
                                << "/" << pSocket->getProtocol()
                                << ". Aborting close.");
                    pthread_mutex_unlock(&m_ConnectionsMutex);
                    return;
                }

                DB_AND_LOGL(4, "Closing connection partially: " << sAddress
                            << ":" << iPort << "/" << iProtocol);

                struct epoll_event event;
                memset(&event, 0, sizeof(event));

                if (epoll_ctl(m_iEPollFd, EPOLL_CTL_DEL, pSocket->getFd(), &event) == -1) {
                    DB_AND_LOGL(3, "Unable to remove connection socket "
                                << pSocket->getFd() << " from epoll:\n"
                                << sAddress << ":" << iPort << " <=> "
                                << pSocket->getLocalAddr() << ":"
                                << pSocket->getLocalPort());
                }

                pSocket->close();

                if (pConnection->processor != NULL) {
                    //single-threaded
                    pConnection->processor->removeBinding(sAddress,
                                                          iPort, iProtocol);
                }
                else {
                    //multi-threaded
                    string sToAddress = pSocket->getLocalAddr();

                    //ThreadManager will call MessageProxy
                    //MessageProxy will call IMessageProcessor::removeBinding()
                    m_pThreadManager->AddNewMessage(
                                sToAddress, pSocket->getLocalPort(),
                                sAddress, iPort, iProtocol,
                                pConnection->pServer->iMessageType, 0, 0);
                }

                //Erase from timeout list
                m_pTimeoutList->RemoveTimeout(sAddress, iPort, iProtocol, iMessageType);
            }
            CloseConnectionLater(sAddress, iPort, iProtocol, iMessageType);
        }

        if (bLockedWorkerThread) {
            pthread_mutex_unlock(&m_ConnectionsMutex);
        }
    }
    else
    { //cannot get lock
        CloseConnectionLater(sAddress, iPort, iProtocol, iMessageType);
    }
}

//Close TCP/TLS Connection Later 
void CNetworkManager::CloseConnectionLater(const string& sAddress, int iPort, int iProtocol, int iMessageType)
{
    DB_ENTER(CNetworkManager::CloseConnectionLater);

    CloseConnectionMessage* closemsg = new CloseConnectionMessage;
    closemsg->setAddress(sAddress);
    closemsg->m_u16Port = iPort;
    closemsg->m_u8Protocol = iProtocol;
    closemsg->m_u8ServerMessageType = iMessageType;

    pthread_mutex_lock(&m_CommandMutex);
    m_CommandQueue.push_back(closemsg);
    if (m_CommandQueue.size() == 1) {
        char dummy = 2;
        ::write(m_iCommandWriteFd, &dummy, 1);
    }
    pthread_mutex_unlock(&m_CommandMutex);
}

//Wait for Shutdown
void CNetworkManager::WaitForShutdown()
{
    DB_ENTER(CNetworkManager::WaitForShutdown);

    if (m_thread != (pthread_t)-1) {
        void* ptr;
        pthread_join(m_thread, &ptr);
        m_thread = (pthread_t)-1;
    }
}

//! Send Message
//! Network Manager will delete pcMessage
void CNetworkManager::SendMessage(const string& sAddress, int iPort, int iProtocol, int iMessageType, 
                                  char* pcMessage, int iLen, int iConvertToTLS, bool bAddConnectionIfNecessary, const string& sTagForNewConnection)
{
    DB_ENTER(CNetworkManager::SendMessage);

    int maxSendLimit = 16*1024;

    if ((pcMessage == NULL) || (iLen <= 0)) return;

    //    DB_AND_LOGL(4, "Ready To Send message to " << sAddress << ":" << iPort << "/" << iProtocol << " " << iMessageType
    //       << " of length " << iLen);


    bool bSendLater = false;

    //    char* tempPcMessage = pcMessage;
    //    int tempLen = iLen;
    //    for(int i=0;i<tempLen;i+=maxSendLimit)
    {
        //        iLen = min(maxSendLimit,(tempLen-i));
        //        pcMessage = tempPcMessage+i;

        DB_AND_LOGL(4, "Send message to " << sAddress << ":" << iPort << "/" << iProtocol << " " << iMessageType
                    << " of length " << iLen);
        //LOGL(5, " =================>>: sent to: "<<sAddress<<" "<<string(pcMessage,iLen));


        if (iProtocol == UDP) {
            //Find first UDP server port with this message type
            int i;
            for (i = 0; i < m_iNumUdpServers; i++) {
                if (m_pUdpServers[i].iMessageType == iMessageType) {
                    //Found
                    ISocket* pSocket = m_pUdpServers[i].pSocket;
                    int sendresult = pSocket->sendTo(
                                pcMessage, iLen, sAddress, iPort);

                    int pollindex = i + 1;

                    pthread_mutex_lock(&m_UdpEpollMutex);
                    if (sendresult == SOCK_OK) {
                        m_pEPollEvents[pollindex].events = EPOLLIN;
                        if (epoll_ctl(m_iEPollFd, EPOLL_CTL_MOD,
                                      pSocket->getFd(),
                                      &(m_pEPollEvents[pollindex])) == -1)
                        {
                            DB_AND_LOGL(3,"Unable to set UDP Server "
                                        " to EPOLLIN after send");
                        }
                    }
                    else if (sendresult == SOCK_WOULDBLOCK) {
                        m_pEPollEvents[pollindex].events = EPOLLIN | EPOLLOUT;
                        if (epoll_ctl(m_iEPollFd, EPOLL_CTL_MOD,
                                      pSocket->getFd(),
                                      &(m_pEPollEvents[pollindex])) == -1)
                        {
                            DB_AND_LOGL(3,"Unable to set UDP Server "
                                        " to EPOLLIN/OUT after send");
                        }
                    }
                    else if (sendresult == SOCK_ERR) {
                        //Do nothing.
                    }
                    pthread_mutex_unlock(&m_UdpEpollMutex);

                    break;
                }
            }

            if (i == m_iNumUdpServers) {
                DB_AND_LOGL(3, "Unable to send UDP message of Message Type " << iMessageType);
            }
        }
        else if ((iProtocol == TCP) || (iProtocol == TLS)) {
            if (m_iPollThreadID == (int)pthread_self()) {
                bSendLater = true;
            }
            else if (pthread_mutex_trylock(&m_ConnectionsMutex) == 0) {
                //Find connection that corresponds to the address, port, and protocol
                StreamConnection* pConnection
                        = (StreamConnection*)m_pConnectionHashTable->Get(
                            sAddress, iPort, iProtocol, iMessageType);

                bool bClose = false;

                if (pConnection != NULL) {
                    SendTCPTLSMessageHelper(pConnection, sAddress, iPort, iProtocol,
                                            iMessageType, pcMessage, iLen, bClose, iConvertToTLS);
                    //LOG("=>> bclose returned: "<<bClose);
                }
                else {
                    if (bAddConnectionIfNecessary)
                    {
                        bSendLater = true;
                    }
                    else {
                        if (pcMessage[iLen - 1] != '\0') {
                            pcMessage[iLen - 1] = '\0';
                        }
                        DB_AND_LOGL(5, "Cannot find connection to " << sAddress << ":"
                                    << iPort << "/" << iProtocol << "." << iMessageType
                                    << ". Unable to send message: " << pcMessage);
                        bSendLater = false;
                    }
                }

                pthread_mutex_unlock(&m_ConnectionsMutex);

                if (bClose) {
                    //LOG("=>> closing connection");
                    CloseConnection(sAddress, iPort, iProtocol, iMessageType);
                }
            }
            else {
                bSendLater = true;
            }
        }
        else {
            if (pcMessage[iLen - 1] != '\0') {
                pcMessage[iLen - 1] = '\0';
            }
            DB_AND_LOGL(3, "Unknown protocol: " << sAddress << ":" << iPort << "/"
                        << iProtocol << " " << pcMessage);
        }

    }

    //    pcMessage = tempPcMessage;
    //    iLen = tempLen;
    //LOG("=>> send later:"<<bSendLater);
    if (bSendLater) {
        SendToMessage* sendtomsg = new SendToMessage;
        sendtomsg->setAddress(sAddress);
        sendtomsg->m_u16Port = iPort;
        sendtomsg->m_u8Protocol = iProtocol;
        sendtomsg->m_u8MessageType = iMessageType;
        sendtomsg->m_u32MessagePtr = (u_int64_t)pcMessage;
        sendtomsg->m_u32MessageLen = iLen;
        sendtomsg->m_u8ConvertToTLS = iConvertToTLS;
        sendtomsg->m_u8AddConnection = (int)bAddConnectionIfNecessary;
        sendtomsg->m_sTagForNewConnection = sTagForNewConnection;

        pthread_mutex_lock(&m_CommandMutex);
        m_CommandQueue.push_back(sendtomsg);
        if (m_CommandQueue.size() == 1) {
            char dummy = 3;  // MC: initialize to avoid memcheck error
            ::write(m_iCommandWriteFd, &dummy, 1);
        }
        pthread_mutex_unlock(&m_CommandMutex);
    }
    else {
        delete [] pcMessage;
        pcMessage = NULL;
    }
}

//! Send TCP/TLS Message helper function
//! Network Manager not delete pcMessage 
//! pConnection must not be NULL
void CNetworkManager::SendTCPTLSMessageHelper(StreamConnection* pConnection, const string& sAddress, int iPort, 
                                              int iProtocol, int iMessageType, char* pcMessage, int iLen, bool& bClose, int iConvertToTLS)
{
    DB_ENTER(CNetworkManager::SendTCPTLSMessageHelper);
    //if(iLen <=0) return;
    int maxSendSize = 16*1024;

    int nextToSend=0;
    /*if( iLen > maxSendSize ){
        nextToSend = iLen-maxSendSize;
        iLen = maxSendSize;
    }
    else
    {
        nextToSend=0;
    }*/
    LOGL(5,"NetworkManager::SendTCPTLSMessageHelper::::: sending "<<iLen<<" Bytes; next to send:"<<nextToSend);

    bClose = false;

    //Update timeout
    ISocket* pSocket = pConnection->pSocket;

    if (pSocket->getFd() == -1) {
        DB_AND_LOGL(3, "Failed to send using closed socket to " << sAddress << ":" << iPort << "/" << iProtocol);
        return;
    }
    else {
        if ((sAddress  != pSocket->getRemoteAddr()) ||
                (iPort     != pSocket->getRemotePort()) ||
                (iProtocol != pSocket->getProtocol())   ||
                (iMessageType != pConnection->pServer->iMessageType))
        {
            DB_AND_LOGL(2, "Trying to send to socket " << pSocket->getFd() << " at "
                        << sAddress << ":" << iPort << "/" << iProtocol
                        << "." << iMessageType
                        << " but found "
                        << pSocket->getRemoteAddr() << ":"
                        << pSocket->getRemotePort()
                        << "/" << pSocket->getProtocol()
                        << "." << pConnection->pServer->iMessageType
                        << ". Aborting send.");
            return;
        }
    }

    if (pConnection->iTimeout != INFINITE) {
        m_pTimeoutList->AddTimeout(sAddress, iPort, iProtocol, iMessageType,
                                   pConnection->iTimeout);
    }

    if (iConvertToTLS == 1)
    {
        ConvertTCPToTLS(sAddress, iPort, iMessageType);

        //TCP socket was replaced with TLS socket
        pSocket = pConnection->pSocket;
    }

//    struct timespec spec;
//    clock_gettime(CLOCK_REALTIME,&spec);
//    long tm1 = spec.tv_sec*1000+spec.tv_nsec/1e6;
    int sendresult = pSocket->send(pcMessage, iLen);
//    tm1 = spec.tv_sec*1000+spec.tv_nsec/1e6 - tm1;

    //if (tm1>99) LOG("==>> sendtime:"<<tm1);

    if (iConvertToTLS == 2)
    {
        ConvertTCPToTLS(sAddress, iPort, iMessageType);

        //TCP socket was replaced with TLS socket
        pSocket = pConnection->pSocket;
    }

    int pollindex = m_iNumFixedSockets + pConnection->iIndex;
    if (sendresult == SOCK_OK) {
        m_pEPollEvents[pollindex].events = EPOLLIN;
        if (epoll_ctl(m_iEPollFd, EPOLL_CTL_MOD,
                      pSocket->getFd(),
                      &(m_pEPollEvents[pollindex])) == -1)
        {
            DB_AND_LOGL(3,"Unable to set TCP/TLS Server "
                        " to EPOLLIN after send");
        }
    }
    else if (sendresult == SOCK_WOULDBLOCK) {
        LOGL(5,"NetworkManager::SendTCPTLSMessageHelper SOCK_WOULDBLOCK");
        m_pEPollEvents[pollindex].events = EPOLLIN | EPOLLOUT;
        if (epoll_ctl(m_iEPollFd, EPOLL_CTL_MOD,
                      pSocket->getFd(),
                      &(m_pEPollEvents[pollindex])) == -1)
        {
            DB_AND_LOGL(3,"Unable to set TCP/TLS Server "
                        " to EPOLLIN/OUT after send");
        }
    }
    else if ((sendresult == SOCK_ERR) || (sendresult == SOCK_CLOSED)) {
        //Close Connection
        if (pSocket->getFd() != -1) {
            m_pEPollEvents[pollindex].events = 0;
            epoll_ctl(m_iEPollFd, EPOLL_CTL_DEL, pSocket->getFd(),
                      &(m_pEPollEvents[pollindex]));

            DB_AND_LOGL(3, "Failed to send packet to " << sAddress <<
                        ":" << iPort << "/" << iProtocol);
            bClose = true;
        }
    }
    //    if (nextToSend)
    //            SendTCPTLSMessageHelper(pConnection, sAddress,iPort,
    //                                                          iProtocol, iMessageType, pcMessage+iLen, nextToSend, bClose, iConvertToTLS);
}

//Process Server Command
//Called by Thread Manager
void CNetworkManager::ProcessServerCommand(const char* pcMessage, int iLen)
{
    DB_ENTER(CNetworkManager::ProcessServerCommand);
}

//Main loop that polls sockets
void* CNetworkManager::NetworkManagerThread(void* arg)
{
    DB_ENTER(CNetworkManager::NetworkManagerThread);

    CNetworkManager* pThis = (CNetworkManager*)arg;
    pThis->PollLoop();

    return NULL;
}

//Poll loop
//Called in NetworkManagerThread
void CNetworkManager::PollLoop()
{
    DB_ENTER(CNetworkManager::PollLoop);

    m_iPollThreadID = pthread_self();

    //Count sockets
    int i;
    int iNumSockets = 1 + //for stopping thread
            m_iNumUdpServers +
            m_iNumTcpServers +
            m_iNumTlsServers;

    m_iNumFixedSockets = iNumSockets;
    int iMaxSockets = iNumSockets;
    struct epoll_event* epollEvents = NULL;

    for (i = 0; i < m_iNumTcpServers; i++) {
        iMaxSockets += m_pTcpServers[i].iMaxConnections;
    }

    for (i = 0; i < m_iNumTlsServers; i++) {
        iMaxSockets += m_pTlsServers[i].iMaxConnections;
    }

    if (iMaxSockets > (m_iNumFixedSockets + m_iMaxAllowedConnections)) {
        iMaxSockets = m_iNumFixedSockets + m_iMaxAllowedConnections;
    }

    int iMaxConnections = iMaxSockets - m_iNumFixedSockets;

    m_pConnections = new StreamConnection*[iMaxConnections];
    m_pConnectionHashTable
            = new CConnectionHashTable(iMaxConnections);
    m_pTimeoutList = new CTimeoutList(iMaxConnections);

    DB("Create Thread Manager");

    m_pThreadManager = new CThreadManager(m_pMessageProxy, g_pCoreConf->m_iNumThreads);

    DB("Initialize Thread Manager");

    m_pThreadManager->SetNetworkManager(this);
    m_pThreadManager->Start();

    DB("Initialize Message Scanner");

    m_pMessageScanner = new CMessageScanner(m_pMessageProxy);

    m_iEPollFd = epoll_create(iMaxSockets);
    if (m_iEPollFd == -1) {
        int iEPollError = errno;
        char szErrBuf[100];
        strerror_r(iEPollError, szErrBuf, 100);
        DB_AND_LOG("epoll_create returned error: " << szErrBuf);
        m_bEndLoop = true;
    }
    else {

        DB("Filling poll array");

        m_pEPollEvents = new struct epoll_event [iMaxSockets];

        // MC: added to prevent memcheck warning
        memset(m_pEPollEvents, 0, sizeof(epoll_event) * iMaxSockets);
        // ---

        for (i = 0; i < iMaxSockets; i++) {
            m_pEPollEvents[i].data.u32 = i;
        }

        m_pEPollEvents[0].events = EPOLLIN;

        if (epoll_ctl(m_iEPollFd, EPOLL_CTL_ADD,
                      m_iCommandReadFd, &(m_pEPollEvents[0])) == -1)
        {
            DB_AND_LOG("Unable to add command read socket to epoll");
            m_bEndLoop = true;
        }

        int pollindex = 1;

        for (i = 0; i < m_iNumUdpServers && !m_bEndLoop; i++) {
            m_pEPollEvents[pollindex].events = EPOLLIN;

            ISocket* pSocket = m_pUdpServers[i].pSocket;
            if (epoll_ctl(m_iEPollFd, EPOLL_CTL_ADD,
                          pSocket->getFd(), &(m_pEPollEvents[pollindex])) == -1)
            {
                DB_AND_LOG("Unable to add UDP server at " <<
                           pSocket->getLocalAddr() << ":" <<
                           pSocket->getLocalPort());
                m_bEndLoop = true;
            }
            pollindex++;
        }

        for (i = 0; i < m_iNumTcpServers && !m_bEndLoop; i++) {
            m_pEPollEvents[pollindex].events = EPOLLIN;

            ISocket* pSocket = m_pTcpServers[i].pSocket;
            if (epoll_ctl(m_iEPollFd, EPOLL_CTL_ADD,
                          pSocket->getFd(), &(m_pEPollEvents[pollindex])) == -1)
            {
                DB_AND_LOG("Unable to add TCP server at " <<
                           pSocket->getLocalAddr() << ":" <<
                           pSocket->getLocalPort());
                m_bEndLoop = true;
            }
            pollindex++;
        }

        for (i = 0; i < m_iNumTlsServers && !m_bEndLoop; i++) {
            m_pEPollEvents[pollindex].events = EPOLLIN;

            ISocket* pSocket = m_pTlsServers[i].pSocket;
            if (epoll_ctl(m_iEPollFd, EPOLL_CTL_ADD,
                          pSocket->getFd(), &(m_pEPollEvents[pollindex])) == -1)
            {
                DB_AND_LOG("Unable to add TLS server at " <<
                           pSocket->getLocalAddr() << ":" <<
                           pSocket->getLocalPort());
                m_bEndLoop = true;
            }
            pollindex++;
        }
    }

    // poll on only one socket when message queue is full
    struct pollfd onepoll;
    memset(&onepoll, 0, sizeof(onepoll));
    onepoll.fd = m_iCommandReadFd;
    onepoll.events = POLLIN;

    if (!m_bEndLoop) {
        DB("Start poll loop");
        epollEvents = new struct epoll_event[iMaxSockets];
    }

    while (!m_bEndLoop) {
        iNumSockets = m_iNumFixedSockets + m_iNumConnections;

        //Check if connections timed out
        CheckConnectionTimeouts();

        DB("Polling " << iNumSockets << " sockets.");
        long t = time(NULL);
        //Poll with timeout of 30 seconds
        int iRet = ::epoll_wait(m_iEPollFd, epollEvents, iNumSockets, 30000);


        DB_AND_LOGL(5, "=====================================>>> Poll Returns " << iRet<<"Message Queue Size "<<m_pThreadManager->MessageQueueWaiting()<<"<<<==============================");

        if (iRet > 0) {
            struct epoll_event* pIter = &(epollEvents[0]);

            for (i = 0; i < iRet; i++, pIter++) {
                int iIndex = pIter->data.u32;
                if ((0 < iIndex) && (iIndex < m_iNumFixedSockets)) {
                    if (iIndex <= m_iNumUdpServers) {
                        //UDP Server
                        DB("Check UDP socket event");
                        int iUdpIndex = iIndex - 1;
                        if (pIter->events & EPOLLOUT) {
                            ISocket* pSocket = m_pUdpServers[iUdpIndex].pSocket;
                            pSocket->sendBuffered();
                            //LOG("===>> sendBuffered "<<__PRETTY_FUNCTION__<<"::"<<__LINE__);

                            pthread_mutex_lock(&m_UdpEpollMutex);
                            m_pEPollEvents[iIndex].events = EPOLLIN;

                            if (epoll_ctl(m_iEPollFd, EPOLL_CTL_MOD, pSocket->getFd(),
                                          &(m_pEPollEvents[iIndex])) == -1) {
                                DB_AND_LOGL(3, "Failed to set UDP socket to EPOLLIN");
                            }
                            pthread_mutex_unlock(&m_UdpEpollMutex);
                        }
                        if (pIter->events & EPOLLIN) {
                            ProcessUdpServer(&(m_pUdpServers[iUdpIndex]));
                        }
                        else if (!(pIter->events & EPOLLOUT)) {
                            DB_AND_LOG("Poll UDP Server " << iUdpIndex << ": "
                                       << pIter->events << " not handled");
                        }
                    }
                    else {
                        if (iIndex <= (m_iNumUdpServers + m_iNumTcpServers)) {
                            //TCP Server
                            DB("Check TCP socket event");
                            int iTcpIndex = iIndex - (m_iNumUdpServers + 1);
                            if (pIter->events & EPOLLIN) {
                                ProcessTcpServer(&(m_pTcpServers[iTcpIndex]));
                            }
                            else {
                                DB_AND_LOG("Poll TCP Server " << iTcpIndex << ": "
                                           << pIter->events << " not handled");
                            }
                        }
                        else {
                            //TLS Server
                            int iTlsIndex = iIndex - (m_iNumUdpServers + m_iNumTcpServers + 1);
                            if (pIter->events & EPOLLIN) {
                                ProcessTlsServer(&(m_pTlsServers[iTlsIndex]));
                            }
                            else {
                                DB_AND_LOG("Poll TLS Server " << iTlsIndex << ": "
                                           << pIter->events << " not handled");
                            }
                        }
                    }
                }
                else if (iIndex >= m_iNumFixedSockets) {
                    //Stream Connection
                    DB("Check stream connection");
                    pthread_mutex_lock(&m_ConnectionsMutex);
                    int iConnectionIndex = iIndex - m_iNumFixedSockets;
                    if (pIter->events & EPOLLOUT) {
                        StreamConnection* pConnection = m_pConnections[iConnectionIndex];
                        ISocket* pSocket = pConnection->pSocket;
                        int result = pSocket->sendBuffered();
                        //LOG("===>> sendBuffered "<<__PRETTY_FUNCTION__<<"::"<<__LINE__);
                        //result = SOCK_OK;
                        if (result == SOCK_OK) {
                            m_pEPollEvents[iIndex].events = EPOLLIN;

                            if (epoll_ctl(m_iEPollFd, EPOLL_CTL_MOD, pSocket->getFd(),
                                          &(m_pEPollEvents[iIndex])) == -1) {
                                DB_AND_LOGL(3, "Failed to set stream connection to EPOLLIN");
                            }

                            //Update timeout
                            if (pConnection->iTimeout != INFINITE) {
                                string sFromAddress = pSocket->getRemoteAddr();
                                int iFromPort = pSocket->getRemotePort();
                                int iProtocol = pSocket->getProtocol();
                                int iMessageType = pConnection->pServer->iMessageType;
                                m_pTimeoutList->AddTimeout(sFromAddress, iFromPort, iProtocol,
                                                           pConnection->iTimeout, iMessageType);
                            }
                        }
                        else if ((result == SOCK_ERR) || (result == SOCK_CLOSED))
                        {
                            //Close connection
                            string sAddress = pSocket->getRemoteAddr();
                            int iRemotePort = pSocket->getRemotePort();
                            int iProtocol = pSocket->getProtocol();
                            int iMessageType = pConnection->pServer->iMessageType;

                            if (result == SOCK_ERR) {
                                DB_AND_LOGL(3, "Send error: Closing stream connection to "
                                            << sAddress << ":" << iRemotePort << "/"
                                            << iProtocol << "." << iMessageType);
                            }
                            else
                            {
                                DB_AND_LOGL(4, "Failed to send to closed stream connection "
                                            << sAddress << ":" << iRemotePort << "/"
                                            << iProtocol << "." << iMessageType);
                            }
                            CloseConnection(sAddress, iRemotePort, iProtocol, iMessageType);
                        }
                    }
                    if (pIter->events & EPOLLIN) {
                        DB("Connection: " << iConnectionIndex << " of " << m_iNumConnections);
                        ProcessStreamConnection(m_pConnections[iConnectionIndex]);
                    }
                    else if (!(pIter->events & EPOLLOUT)) {
                        StreamConnection* pConnection = m_pConnections[iConnectionIndex];
                        ISocket* pSocket = pConnection->pSocket;
                        string sAddress = pSocket->getRemoteAddr();
                        int iRemotePort = pSocket->getRemotePort();
                        int iProtocol = pSocket->getProtocol();
                        int iMessageType = pConnection->pServer->iMessageType;

                        if (pIter->events & EPOLLERR) {
                            DB_AND_LOGL(3, "Epoll error at " << sAddress << ":" << iRemotePort
                                        << "/" << iProtocol << "." << iMessageType << ". Closing connection");
                        }
                        else if (pIter->events & EPOLLHUP) {
                            DB_AND_LOGL(3, "Epoll sighup at " << sAddress << ":" << iRemotePort
                                        << "/" << iProtocol << "." << iMessageType << ". Closing connection");
                        }
                        else {
                            DB_AND_LOGL(3, "Epoll event " << pIter->events <<
                                        " at " << sAddress << ":" << iRemotePort
                                        << "/" << iProtocol << "." << iMessageType << ". Closing connection");
                        }

                        CloseConnection(sAddress, iRemotePort, iProtocol, iMessageType);
                    }
                    pthread_mutex_unlock(&m_ConnectionsMutex);
                }
            }

            //Check commands last -- in case it closes a connection
            DB("Check command socket");

            ProcessCommand();
        }
        else {
            //Timeout or error
            //Generate Report
            //commented out: it clogs up the logs at verbosity 5
            //			DB_AND_LOGL(5, GetPortInformation());
        }

        //Mini poll loop when message queue is full
	    bool bOverloaded = m_pThreadManager->IsMessageQueueFullOrAlmostFull();
	
	    if ( (!bOverloadEntry)&&(bOverloaded) ) // just enter into overload region
	    {
	      bOverloadEntry = true;
	      iOverload_EntryTime = time(NULL);
	    }else if ( (bOverloadEntry)&&(!bOverloaded) )
        {
          bOverloadEntry = false;
	    }  
		
		if (bOverloaded)
		{
		  int tOverloadTime = time(NULL);
		  
          if (  ( (m_pThreadManager->IsMessageQueueFull()) || ( (tOverloadTime-iOverload_EntryTime) >300 ) )  ) {
            
			
			if (m_pThreadManager->IsMessageQueueFull())
			{
			  
			  DB_AND_LOGL(5, "Server overload condition: Message Queue is full. Temporarily not reading from sockets");
			  
			}else
			{
			  DB_AND_LOGL(5, "Server overload condition: Message Queue is almost full. Temporarily not reading from sockets");
			}

            ::poll(&onepoll, 1, 100);
            ProcessCommand();

            bool bOverloadReported = false;
            int icount =0;
            while (!m_bEndLoop && m_pThreadManager->IsMessageQueueFullOrAlmostFull()) {
                if (!bOverloadReported) {
                    if ((time(NULL) - tOverloadTime) > 1) {
                        bOverloadReported = true;
                        DB_AND_LOGL(2, "Server overloaded for more than one second");
                    }
                }
                ::poll(&onepoll, 1, 100);
                ProcessCommand();
                if ( (icount%600) == 0 )
                {
                        DB_AND_LOGL(5, "=====================================>>>Mini Poll Returns " << iRet<<"Message Queue Size "<<m_pThreadManager->MessageQueueWaiting()<<"<<<==============================");
                }
                ++icount;
            }
            DB_AND_LOGL(2, "Server resuming normal operation");
		  }	
        }


    }

    DB("Exited poll loop");

    if (m_iEPollFd != -1) {
        close(m_iEPollFd);
        m_iEPollFd = -1;
    }

    //Delete queued messages
    pthread_mutex_lock(&m_CommandMutex);
    CommandMessage* pCommandMessage;
    while (m_CommandQueue.size() > 0) {
        pCommandMessage = m_CommandQueue.front();
        delete pCommandMessage;
        m_CommandQueue.pop_front();
    }
    pthread_mutex_unlock(&m_CommandMutex);

    delete m_pMessageScanner;
    m_pMessageScanner = NULL;

    m_pThreadManager->Stop();
    delete m_pThreadManager;
    m_pThreadManager = NULL;

    delete [] epollEvents;

    delete [] m_pEPollEvents;
    m_pEPollEvents = NULL;

    delete [] m_pConnections;
    m_pConnections = NULL;

    delete m_pConnectionHashTable;
    m_pConnectionHashTable = NULL;

    delete m_pTimeoutList;
    m_pTimeoutList = NULL;

    m_iPollThreadID = -1;
}

//Process command 
void CNetworkManager::ProcessCommand()
{
    DB_ENTER(CNetworkManager::ProcessCommand);

    pthread_mutex_lock(&m_CommandMutex);

    char dummy = 0;

    if (m_CommandQueue.empty()) {
        // error case - nothing to process (so.. unlock and return)
        pthread_mutex_unlock(&m_CommandMutex);
        return;
    }

    CommandMessage* pCommandMessage = m_CommandQueue.front();
    m_CommandQueue.pop_front();
    if (m_CommandQueue.empty()) {
        ::read(m_iCommandReadFd, &dummy, 1);
    }

    pthread_mutex_unlock(&m_CommandMutex);

    pthread_mutex_lock(&m_ConnectionsMutex);

    int iNumCommandsProcessed = 0;
    bool bContinue = true;
    while (bContinue) {
        int iMessageType = pCommandMessage->m_u8MessageType;

        if (m_bEndLoop) {
            //Simply discard message
        }
        else if (iMessageType == EYEBALL_SERVER_STOP) {
            DB("Processing EYEBALL_SERVER_STOP");

            m_bEndLoop = true;

            //Shutdown all connections
            DB("Shutdown all connections");

            while (m_iNumConnections > 0) {
                StreamConnection* pConnection = m_pConnections[m_iNumConnections - 1];
                string sRemoteAddr = pConnection->pSocket->getRemoteAddr();
                CloseConnectionFinal(sRemoteAddr,
                                     pConnection->pSocket->getRemotePort(),
                                     pConnection->pSocket->getProtocol(),
                                     pConnection->pServer->iMessageType);
            }

            //End poll loop
            DB("End poll loop");
        }
        else if (iMessageType == EYEBALL_SERVER_SEND_TO) {
            DB("Detected Send To Message");

            SendToMessage* sendtomsg = (SendToMessage*)pCommandMessage;
            int iPort = sendtomsg->m_u16Port;
            int iProtocol = sendtomsg->m_u8Protocol;
            int iMessageType = sendtomsg->m_u8MessageType;
            char* pcMessage = (char*)sendtomsg->m_u32MessagePtr;
            int iLen = sendtomsg->m_u32MessageLen;
            int iConvertToTLS = sendtomsg->m_u8ConvertToTLS;
            bool bAddConnectionIfNecessary = (bool)sendtomsg->m_u8AddConnection;//true; for sip
            const string& sTagForNewConnection = sendtomsg->m_sTagForNewConnection;
            string sAddress;
            sendtomsg->getAddress(sAddress);

            //Find connection that corresponds to the address, port, and protocol
            StreamConnection* pConnection
                    = (StreamConnection*)m_pConnectionHashTable->Get(
                        sAddress, iPort, iProtocol, iMessageType);

            if ((pConnection == NULL) && (bAddConnectionIfNecessary) &&
                    (m_iNumConnections < m_iMaxAllowedConnections))
            {
                ServerInfo* pServer = NULL;
                int i;
                for (i = 0; i < m_iNumTcpServers; i++) {
                    if (m_pTcpServers[i].iMessageType == iMessageType) {
                        pServer = &(m_pTcpServers[i]);
                        break;
                    }
                }
                //LOG("=>> add connection : "<<sAddress<<" "<<sTagForNewConnection);
                pConnection = AddConnection(sAddress, iPort, TCP, pServer, sTagForNewConnection);
            } //create connection for edge-to-edge communication

            if (pConnection != NULL) {
                bool bClose = false;
                SendTCPTLSMessageHelper(pConnection, sAddress, iPort, iProtocol,
                                        iMessageType, pcMessage, iLen, bClose, iConvertToTLS);

                if (bClose) {
                    CloseConnection(sAddress, iPort, iProtocol, iMessageType);
                }
            }
            else
            {
                if (pcMessage[iLen - 1] != '\0') {
                    pcMessage[iLen - 1] = '\0';
                }
                DB_AND_LOGL(5, "Cannot find connection to " << sAddress << ":"
                            << iPort << "/" << iProtocol
                            << ". Unable to send message: " << pcMessage);
            }

            delete [] pcMessage;
            pcMessage = NULL;
        }
        else if (iMessageType == EYEBALL_SERVER_CLOSE_CONN) {
            DB("Detected Close Connection Message");

            CloseConnectionMessage* closemsg = (CloseConnectionMessage*)pCommandMessage;
            string sAddress;
            closemsg->getAddress(sAddress);
            // //cout<<"Detected Close Connection Message"<<endl;
            CloseConnectionFinal(sAddress, closemsg->m_u16Port, closemsg->m_u8Protocol,
                                 closemsg->m_u8ServerMessageType);
        }
        else {
            DB_AND_LOGL(3, "Unexpected server command message type: " << iMessageType);
        }
        delete pCommandMessage;
        pCommandMessage = NULL;

        if (++iNumCommandsProcessed == 20) break;

        pthread_mutex_lock(&m_CommandMutex);
        bContinue = false;
        if (m_CommandQueue.size() > 0) {
            pCommandMessage = m_CommandQueue.front();
            m_CommandQueue.pop_front();
            bContinue = true;

            if (m_CommandQueue.size() == 0) {
                //non-blocking read
                ::read(m_iCommandReadFd, &dummy, 1);  // one read should be enough
            }
        }
        pthread_mutex_unlock(&m_CommandMutex);
    }
    pthread_mutex_unlock(&m_ConnectionsMutex);
}

//Process UDP Server
void CNetworkManager::ProcessUdpServer(ServerInfo* pServer)
{
    for (int i = 0; i < 10; i++)
    {
        if (!ProcessUdpInternal(pServer))
            break;
    }
}

bool CNetworkManager::ProcessUdpInternal(ServerInfo* pServer)
{
    DB_ENTER(CNetworkManager::ProcessUdpInternal);

    string sFromAddress;
    int iFromPort;

    int iRecvLen = pServer->pSocket->recvFrom(m_pcRecvBuffer, m_iMaxRecvPacketSize, sFromAddress, iFromPort);

    if (iRecvLen <= 0) {
        return false;
    }

    int iMessageLength = m_pMessageScanner->Scan(m_pcRecvBuffer, iRecvLen, pServer->iMessageType);

    if (iMessageLength > 0) {
        //Full packet received
        char* pcMessage = new char[iMessageLength];
        memcpy(pcMessage, m_pcRecvBuffer, iMessageLength);

        string sToAddress = pServer->pSocket->getLocalAddr();

        m_pThreadManager->AddNewMessage(
                    sToAddress, pServer->pSocket->getLocalPort(),
                    sFromAddress, iFromPort, pServer->pSocket->getProtocol(),
                    pServer->iMessageType, pcMessage, iMessageLength);

        if (iMessageLength > iRecvLen) {
            //Error
            DB("Received extra bytes at the end of the message on UDP server");
        }
    }
    else if (iMessageLength == MESSAGE_SCAN_INCOMPLETE) {
        //Error
        DB("Received incomplete message on UDP server");
    }
    else if (iMessageLength == MESSAGE_SCAN_UNSUPPORTED) {
        //Error
        DB_AND_LOGL(3, "Received unsupported message type " << pServer->iMessageType);
    }

    return true;
}

//Process TCP Server
void CNetworkManager::ProcessTcpServer(ServerInfo* pServer)
{
    DB_ENTER(CNetworkManager::ProcessTcpServer);

    DB("new incoming stream connection event, "
       << pServer->iNumConnections << " active clients");

    // check two conditions:
    // 1. amount of connected clients below threshold for this server
    // 2. total amount of sockets not exceeded
    // 3. not almost overloaded

    bool bOverloaded = m_pThreadManager->IsMessageQueueFullOrAlmostFull();

    /*
    if (g_pCpuUsage->getCurrentCpuUsage()>g_pCoreConf->m_iTolerableCpuUsage)
    {
        LOGL(3, "CpuUsage is more than "<<g_pCoreConf->m_iTolerableCpuUsage);
        bOverloaded = true;
    }
    */

    if ((pServer->iMessageType == ADMIN) || ((pServer->iNumConnections < pServer->iMaxConnections) &&
                                             (m_iNumConnections < m_iMaxAllowedConnections) &&
                                             (!bOverloaded)))
    {
        ISocket* pNewSocket = pServer->pSocket->accept();

        if (pNewSocket == NULL) {
            DB_AND_LOGL(3, "new connection request not accepted");
        }
        else {
            IMessageProcessor* pProcessor
                    = m_pMessageProxy->GetProcessorForMessageType(pServer->iMessageType);

            if (pProcessor == NULL) {
                //Error
                DB_AND_LOGL(3,"Unsupported message type: "<< pServer->iMessageType);
            }
            else {
                string sRemoteAddr = pNewSocket->getRemoteAddr();
                int iRemotePort = pNewSocket->getRemotePort();
                int iProtocol = pNewSocket->getProtocol();
                int iMessageType = pServer->iMessageType;

                DB_AND_LOGL(4, "accepted connection from " << sRemoteAddr << ":" << iRemotePort);

                DB("Check whether connection to this address and port already exists");

                pthread_mutex_lock(&m_ConnectionsMutex);

                if (m_pConnectionHashTable->Get(sRemoteAddr, iRemotePort, iProtocol, iMessageType) != NULL) {
                    DB_AND_LOGL(3, "connection closed because connection to " <<
                                sRemoteAddr << ":" << iRemotePort <<
                                " already exists");
                    pNewSocket->close();
                    delete pNewSocket;
                }
                else {
                    StreamConnection* pNewConnection = new StreamConnection;
                    pNewConnection->iIndex = m_iNumConnections;
                    pNewConnection->pServer = pServer;
                    pNewConnection->pSocket = pNewSocket;
                    pNewConnection->pcRecvBuffer = NULL;
                    pNewConnection->iRecvLen = 0;
                    pNewConnection->processor = pProcessor;

                    pServer->iNumConnections++;

                    m_pConnections[m_iNumConnections] = pNewConnection;

                    DB("Add remote address");

                    m_pConnectionHashTable->Put(sRemoteAddr, iRemotePort, iProtocol, iMessageType,
                                                pNewConnection);

                    DB("Add expiry");

                    pNewConnection->iTimeout = pNewConnection->processor->getTimeout();
                    if (pNewConnection->iTimeout != INFINITE) {
                        if (pNewConnection->iTimeout == 0) {
                            //Use default timeout
                            pNewConnection->iTimeout = g_pCoreConf->m_iTCPConnectionTimeout;
                        }
                        m_pTimeoutList->AddTimeout(sRemoteAddr, iRemotePort, iProtocol, iMessageType,
                                                   pNewConnection->iTimeout);
                    }

                    DB("Send Welcome message");

                    struct epoll_event* pPollFd = &(m_pEPollEvents[m_iNumFixedSockets + m_iNumConnections]);
                    pPollFd->events = EPOLLIN;

                    m_iNumConnections++;

                    // send welcome message if desired
                    bool bClosed = false;
                    string sWelcome;
                    if (pNewConnection->processor->sendWelcome(sWelcome) == MSG_OK) {
                        int result = pNewSocket->send(sWelcome.c_str(), sWelcome.size());
                        if (result == SOCK_WOULDBLOCK) {
                            pPollFd->events = EPOLLIN | EPOLLOUT;
                        }
                        else if ((result == SOCK_ERR) || (result == SOCK_CLOSED)) {
                            //Close Connection
                            pPollFd->events = 0;
                            DB_AND_LOGL(3, "Failed to send welcome message to " <<
                                        sRemoteAddr << ":" << iRemotePort << "/" << iProtocol
                                        << "." << iMessageType);
                            CloseConnection(sRemoteAddr, iRemotePort, iProtocol, iMessageType);
                            bClosed = true;
                        }
                    }

                    if (!bClosed) {
                        DB("Add new connection to poll array");

                        if (epoll_ctl(m_iEPollFd, EPOLL_CTL_ADD, pNewSocket->getFd(), pPollFd) == -1) {
                            DB_AND_LOGL(3, "Failed to add connection socket to epoll:\n" <<
                                        sRemoteAddr << ":" << iRemotePort << " <=> " <<
                                        pNewSocket->getLocalAddr() << ":" <<
                                        pNewSocket->getLocalPort() << "." << iMessageType);

                            CloseConnection(sRemoteAddr, iRemotePort, iProtocol, iMessageType);
                            bClosed = true;
                        }
                    }

                    if (!bClosed) {
                        //Keep the processor only for single-threaded processors
                        if (pNewConnection->processor->isMT()) {
                            delete pNewConnection->processor;
                            pNewConnection->processor = NULL;
                        }
                    }
                }
                pthread_mutex_unlock(&m_ConnectionsMutex);
            }
        }
    }
    else {
        if (bOverloaded) {
            DB_AND_LOGL(3,"could not accept new stream server connection: server busy");
        }
        else {
            DB_AND_LOGL(3,"could not accept new stream server connection: too many open connections: "<< pServer->iNumConnections);
        }

        // reject the connection
        ISocket* pTempConnection = pServer->pSocket->accept();
        if (pTempConnection)
            pTempConnection->close();
        delete pTempConnection;
    }

    //Print connection stats if verbose = 5
}

//Process TLS Server
void CNetworkManager::ProcessTlsServer(ServerInfo* pServer)
{
    DB_ENTER(CNetworkManager::ProcessTlsServer);

    //Identical to ProcessTcpServer
    ProcessTcpServer(pServer);
}

//Process Stream Connection
void CNetworkManager::ProcessStreamConnection(StreamConnection* pConnection)
{
    DB_ENTER(CNetworkManager::ProcessStreamConnection);

    string sFromAddress;
    int iFromPort;
    int iProtocol = pConnection->pSocket->getProtocol();
    int iMessageType = pConnection->pServer->iMessageType;

    if (pConnection->pSocket->getFd() == -1) {
        DB_AND_LOGL(3, "Failed to receive using closed socket from "
                    << pConnection->pSocket->getRemoteAddr() << ":"
                    << pConnection->pSocket->getRemotePort() << "/"
                    << iProtocol);
        return;
    }

    //Fragments may already have been received
    int iReadSize = m_iMaxRecvPacketSize - pConnection->iRecvLen;

    int iRecvLen = pConnection->pSocket->recvFrom(m_pcRecvBuffer + pConnection->iRecvLen,
                                                  iReadSize, sFromAddress, iFromPort);

    if (iRecvLen <= 0) {

        if (iRecvLen != SOCK_WOULDBLOCK) {
            if (iRecvLen == SOCK_ERR) {
                DB_AND_LOGL(3, "Error while receiving from stream connection");
            }
            else if (iRecvLen == SOCK_CLOSED) {
                DB_AND_LOGL(4, "Detected connection to " << sFromAddress << ":" <<
                            iFromPort << "/" << iProtocol << "." << iMessageType << " closed");
            }
            else {
                DB_AND_LOGL(3, "Failed to receive from " << sFromAddress << ":" <<
                            iFromPort << "/" << iProtocol << "." << iMessageType);
            }

            if (iRecvLen < 0) {
                CloseConnection(sFromAddress, iFromPort, iProtocol, iMessageType);
            }
        }
        return;
    }
    else {
        //Update timeout
        if (pConnection->iTimeout != INFINITE) {
            m_pTimeoutList->AddTimeout(sFromAddress, iFromPort, iProtocol, iMessageType,
                                       pConnection->iTimeout);
        }
    }

    if (pConnection->iRecvLen > 0) {
        iRecvLen += pConnection->iRecvLen;

        memcpy(m_pcRecvBuffer, pConnection->pcRecvBuffer, pConnection->iRecvLen);
        pConnection->iRecvLen = 0;
        delete [] pConnection->pcRecvBuffer;
        pConnection->pcRecvBuffer = NULL;
    }

    int iMessageLength = m_pMessageScanner->Scan(m_pcRecvBuffer, iRecvLen, iMessageType);
    while (iMessageLength > 0) {
        char* pcMessage = new char[iMessageLength];
        memcpy(pcMessage, m_pcRecvBuffer, iMessageLength);

        string sToAddress = pConnection->pSocket->getLocalAddr();
        int iToPort = pConnection->pSocket->getLocalPort();
        if (m_pMessageScanner->SupportsMultiThreading(iMessageType)) {
            m_pThreadManager->AddNewMessage(
                        sToAddress, iToPort, sFromAddress, iFromPort, iProtocol,
                        iMessageType, pcMessage, iMessageLength);
        }
        else if (pConnection->processor == NULL)
        {
            DB_AND_LOGL(3, "No processor for connection with Message Type: " << iMessageType
                        << " " << sFromAddress << ":" << itos(iFromPort) << "/"
                        << itos(iProtocol) << " <-> " << sToAddress << ":"
                        << itos(iToPort) << "/" << itos(iProtocol));
        }
        else
        {
            int proc_result = pConnection->processor->processMessage(
                        pcMessage, iMessageLength,
                        sFromAddress, iFromPort, iProtocol,
                        sToAddress, iToPort, iProtocol);

            delete [] pcMessage;

            if (proc_result == MSG_RESPONSE_AVAIL) {
                char* pcResponse = NULL;
                int iResponseLen = 0;
                string sResponseAddress;
                int iResponsePort;
                int iResponseProtocol;

                int resp_result = pConnection->processor->getResponse(pcResponse, iResponseLen,
                                                                      sResponseAddress, iResponsePort, iResponseProtocol);

                if ((resp_result == MSG_OK) || (resp_result == MSG_CLOSE)) {
                    if ((pcResponse != NULL) && (iResponseLen > 0)) {
                        int result = pConnection->pSocket->send(pcResponse, iResponseLen);
                        int pollindex = m_iNumFixedSockets + pConnection->iIndex;
                        if (result == SOCK_WOULDBLOCK) {
                            m_pEPollEvents[pollindex].events = EPOLLIN | EPOLLOUT;
                            if (epoll_ctl(m_iEPollFd, EPOLL_CTL_MOD,
                                          pConnection->pSocket->getFd(),
                                          &(m_pEPollEvents[pollindex])) == -1)
                            {
                                DB_AND_LOGL(3,"Unable to set Stream Connection"
                                            " to EPOLLIN/OUT after send");
                            }
                        }

                        delete [] pcResponse;
                    }
                    if (resp_result == MSG_CLOSE) {
                        //Close connection
                        int pollindex = m_iNumFixedSockets + pConnection->iIndex;
                        m_pEPollEvents[pollindex].events = 0;
                        DB_AND_LOGL(4, "Message Processor " << pConnection->pServer->iMessageType <<
                                    " closing connection to " <<
                                    sFromAddress << ":" << iFromPort << "/" << iProtocol << "." << iMessageType);

                        CloseConnection(sFromAddress, iFromPort, iProtocol, iMessageType);
                    }
                }
            }

        }

        if (iMessageLength < iRecvLen) {
            iRecvLen -= iMessageLength;
            memmove(m_pcRecvBuffer, m_pcRecvBuffer + iMessageLength, iRecvLen);

            iMessageLength = m_pMessageScanner->Scan(m_pcRecvBuffer, iRecvLen, iMessageType);
        }
        else {
            return;
        }
    }

    if (iMessageLength == MESSAGE_SCAN_INCOMPLETE) {
        if (iRecvLen == m_iMaxRecvPacketSize) {
            DB_AND_LOGL(3, "Received oversized packet. Discarding.");
            m_pcRecvBuffer[iRecvLen - 1] = '\0';
            DB_AND_LOGL(5, m_pcRecvBuffer);
        }
        else {
            pConnection->pcRecvBuffer = new char[iRecvLen];
            pConnection->iRecvLen = iRecvLen;

            memcpy(pConnection->pcRecvBuffer, m_pcRecvBuffer, iRecvLen);
        }
    }
    else if (iMessageLength == MESSAGE_SCAN_UNSUPPORTED) {
        //Error
        DB_AND_LOGL(3, "Received unsupported message type " << iMessageType);
    }
}

//! Check Connection Timeouts
void CNetworkManager::CheckConnectionTimeouts()
{
    string sAddress;
    int iPort;
    int iProtocol;
    int iMessageType;

    while (m_pTimeoutList->GetExpired(sAddress, iPort, iProtocol, iMessageType)) {
        DB_AND_LOGL(4, "Connection expired " << sAddress << ":"
                    << iPort << "/" << iProtocol << "." << iMessageType);

        pthread_mutex_lock(&m_ConnectionsMutex);
        CloseConnection(sAddress, iPort, iProtocol, iMessageType);
        pthread_mutex_unlock(&m_ConnectionsMutex);
    }
}

//! Add TCP/TLS Connection
CNetworkManager::StreamConnection* CNetworkManager::AddConnection(const string& sAddress, int iPort, int iProtocol, CNetworkManager::ServerInfo* pServer, const string& sTagForNewConnection)
{
    StreamConnection* pConnection = NULL;
    ISocket* pSocket = NULL;
    if (pServer != NULL) {
        if (!m_pThreadManager->IsMessageQueueFullOrAlmostFull()) {
            if (iProtocol == TCP)
            {
                pSocket = new TCPSocket;
            }
            else if (iProtocol == TLS)
            {
                pSocket = new TLSSocket;
            }
        }
    }
    else {
        DB_AND_LOGL(3, "Failed to find Server Type to add connection for");
    }
    if (pSocket != NULL) {
        if (pSocket->createSocket(0, "any", 0)
                != SOCK_OK)
        {
            DB_AND_LOGL(5, "Failed to create socket to " <<
                        sAddress << ":" << iPort << "/" << iProtocol);
            delete pSocket;
            pSocket = NULL;
        }
    }
    if (pSocket != NULL) {
        if (pSocket->connect(sAddress, iPort) == SOCK_ERR)
        {
            DB_AND_LOGL(5, "Failed to connect to " <<
                        sAddress << ":" << iPort << "/" << iProtocol);
            delete pSocket;
            pSocket = NULL;
        }
    }
    if (pSocket != NULL) {
        //Create connection
        pServer->iNumConnections++;

        pConnection = new StreamConnection;
        pConnection->iIndex = m_iNumConnections;
        pConnection->pServer = pServer;
        pConnection->pSocket = pSocket;
        pConnection->pcRecvBuffer = NULL;
        pConnection->iRecvLen = 0;
        pConnection->processor = NULL;
        pConnection->iTimeout = g_pCoreConf->m_iTCPConnectionTimeout;

        m_pConnections[m_iNumConnections] = pConnection;

        int iMessageType = pServer->iMessageType;

        DB_AND_LOGL(4, "Add new connection: "
                    << sAddress << ":" << iPort << "/" << iProtocol << "." << iMessageType);

        m_pConnectionHashTable->Put(sAddress, iPort, iProtocol, iMessageType, pConnection);

        struct epoll_event* pPollFd
                = &(m_pEPollEvents[m_iNumFixedSockets + m_iNumConnections]);
        pPollFd->events = EPOLLIN;

        DB("Add new connection to poll array");

        m_iNumConnections++;

        if (epoll_ctl(m_iEPollFd, EPOLL_CTL_ADD, pSocket->getFd(), pPollFd) == -1) {
            DB_AND_LOGL(3, "Failed to add connection socket to epoll:\n" <<
                        sAddress << ":" << iPort << " <=> " <<
                        pSocket->getLocalAddr() << ":" <<
                        pSocket->getLocalPort() << "." << iMessageType);

            CloseConnection(sAddress, iPort, iProtocol, iMessageType);
            pConnection = NULL;
        }

        int iTagLen = (int)sTagForNewConnection.size();
        char* pTag  = new char[iTagLen];
        memcpy(pTag, sTagForNewConnection.data(), iTagLen);

        m_pThreadManager->AddNewMessage(
                    g_pCoreConf->m_sIP, pSocket->getLocalPort(),
                    sAddress, iPort, iProtocol,
                    pConnection->pServer->iMessageType, pTag, iTagLen);
    }
    return pConnection;
}

//! Convert TCP connection to TLS
bool CNetworkManager::ConvertTCPToTLS(const string& sAddress, int iPort, int iMessageType)
{
    //Find connection that corresponds to the address, port, and protocol
    StreamConnection* pConnection
            = (StreamConnection*)m_pConnectionHashTable->Get(
                sAddress, iPort, TCP, iMessageType);

    if (pConnection == NULL)
    {
        DB_AND_LOGL(3, "Failed to find TCP connection to add to TLS:\n" <<
                    sAddress << ":" << iPort << "/1");
    }
    else
    {
        TCPSocket* pTcpSocket = (TCPSocket*)pConnection->pSocket;
        TLSSocket* pTlsSocket = new TLSSocket(pTcpSocket);
        pConnection->pSocket = pTlsSocket;

        m_pConnectionHashTable->Put(sAddress, iPort, TCP, iMessageType, NULL);
        m_pConnectionHashTable->Put(sAddress, iPort, TLS, iMessageType, pConnection);
    }

    return (pConnection != NULL);
}

//! Close TCP/TLS Connection immediately and completely
void CNetworkManager::CloseConnectionFinal(const string& sAddress, int iPort, int iProtocol, int iMessageType)
{
    ////cout<<__FILE__<<"::"<<__FUNCTION__<<" "<<sAddress<<":"<<iPort<<endl;
    DB_ENTER(CNetworkManager::CloseConnectionFinal);

    //Search for address, port, protocol in Hash Table
    StreamConnection* pConnection
            = (StreamConnection*)m_pConnectionHashTable->Get(
                sAddress, iPort, iProtocol, iMessageType);

    if (pConnection == NULL) {
        DB_AND_LOGL(5, "Note: cannot find connection to close: "
                    << sAddress << ":" << iPort << "/" << iProtocol);
    }
    else {
        //Remove binding
        ISocket* pSocket = pConnection->pSocket;

        DB_AND_LOGL(4, "Closing connection completely: " << sAddress
                    << ":" << iPort << "/" << iProtocol);

        if (pSocket->getFd() != -1) {
            if ((sAddress  != pSocket->getRemoteAddr()) ||
                    (iPort     != pSocket->getRemotePort()) ||
                    (iProtocol != pSocket->getProtocol()))
            {
                DB_AND_LOGL(2, "Trying to close socket " << pSocket->getFd() << " at "
                            << sAddress << ":" << iPort << "/" << iProtocol
                            << " but found "
                            << pSocket->getRemoteAddr() << ":"
                            << pSocket->getRemotePort()
                            << "/" << pSocket->getProtocol()
                            << ". Aborting close.");
                return;
            }

            struct epoll_event event;
            memset(&event, 0, sizeof(event));

            if (epoll_ctl(m_iEPollFd, EPOLL_CTL_DEL, pSocket->getFd(), &event) == -1) {
                DB_AND_LOGL(3, "Unable to remove connection socket "
                            << pSocket->getFd() << " from epoll:\n"
                            << sAddress << ":" << iPort << " <=> "
                            << pSocket->getLocalAddr() << ":"
                            << pSocket->getLocalPort());
            }

            //Close socket
            pSocket->close();

            if (pConnection->processor != NULL) {
                //single-threaded
                pConnection->processor->removeBinding(sAddress,
                                                      iPort, iProtocol);
            }
            else {
                //multi-threaded
                string sToAddress = pSocket->getLocalAddr();

                //ThreadManager will call MessageProxy
                //MessageProxy will call IMessageProcessor::removeBinding()
                m_pThreadManager->AddNewMessage(
                            sToAddress, pSocket->getLocalPort(),
                            sAddress, iPort, iProtocol,
                            pConnection->pServer->iMessageType, 0, 0);
            }

            //Erase from timeout list
            m_pTimeoutList->RemoveTimeout(sAddress, iPort, iProtocol, iMessageType);
        }

        //Erase connection from hash table
        m_pConnectionHashTable->Put(sAddress, iPort, iProtocol, iMessageType, NULL);

        //Decrement number of connections at a server port
        pConnection->pServer->iNumConnections--;

        //Swap positions of connections
        int iIndex = pConnection->iIndex;
        int iLastIndex = m_iNumConnections - 1;

        if (iIndex < iLastIndex) {
            //Swap positions
            m_pConnections[iIndex] = m_pConnections[iLastIndex];
            m_pConnections[iIndex]->iIndex = iIndex;

            ISocket* pSwappedSocket= m_pConnections[iIndex]->pSocket;

            int iSwapSocketFd = pSwappedSocket->getFd();
            int iSwappedMessageType = m_pConnections[iIndex]->pServer->iMessageType;

            //Update connection hash table
            string sSwappedAddress = pSwappedSocket->getRemoteAddr();
            m_pConnectionHashTable->Put(sSwappedAddress, pSwappedSocket->getRemotePort(),
                                        pSwappedSocket->getProtocol(), iSwappedMessageType, m_pConnections[iIndex]);

            if (iSwapSocketFd != -1) {
                m_pEPollEvents[m_iNumFixedSockets + iIndex].events
                        = m_pEPollEvents[m_iNumFixedSockets + iLastIndex].events;

                if (epoll_ctl(m_iEPollFd, EPOLL_CTL_MOD,
                              iSwapSocketFd,
                              &(m_pEPollEvents[m_iNumFixedSockets + iIndex])) == -1) {
                    DB_AND_LOGL(3, "Unable to modify index for epoll:\n"
                                << m_pConnections[iIndex]->pSocket->getRemoteAddr() << ":"
                                << m_pConnections[iIndex]->pSocket->getRemotePort() << " <=> "
                                << m_pConnections[iIndex]->pSocket->getLocalAddr() << ":"
                                << m_pConnections[iIndex]->pSocket->getLocalPort());
                }
            }
        }
        m_pConnections[iLastIndex] = NULL;
        m_iNumConnections--;

        //Delete connection
        delete pSocket;
        delete [] pConnection->pcRecvBuffer;
        delete pConnection->processor;
        delete pConnection;

        DB("Closed connection");
    }
}

