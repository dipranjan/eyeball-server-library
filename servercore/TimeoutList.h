//!  TimeoutList.h
//!  Interface for the CTimeoutList class

#ifndef __TIMEOUT_LIST_H_
#define __TIMEOUT_LIST_H_

#include <string>
using std::string;

class CConnectionHashTable;

class CTimeoutList 
{
public:
	//! Constructor
	CTimeoutList(int iMaxConnections);

	//! Destructor
	~CTimeoutList();

	//! Add Timeout
	void AddTimeout(const string& sAddress, int iPort, int iProtocol, int iMessageType, int iTimeout);

	//! Remove Timeout
	void RemoveTimeout(const string& sAddress, int iPort, int iProtocol, int iMessageType);

	//! Get Expired
	//! Removes oldest expired item
	//! Returns true if something expired
	bool GetExpired(string& sAddress, int& iPort, int& iProtocol, int& iMessageType);

protected:
	struct node {
		string sAddress;
		unsigned short usPort;
		unsigned char ucProtocol;
		unsigned char ucMessageType;
		int iExpire;
		node* pNewer;
		node* pOlder;
	};

	node* m_pNewest;
	node* m_pOldest;

	CConnectionHashTable* m_pHashTable;
};

#endif //__TIMEOUT_LIST_H_

