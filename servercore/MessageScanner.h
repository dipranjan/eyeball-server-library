//! MessageScanner.h
//! Interface of CMessageScanner class
//! Checks completeness and validity of message

#ifndef _MESSAGE_SCANNER_H_
#define _MESSAGE_SCANNER_H_

class CMessageProxy;
class IMessageProcessor;

#define MESSAGE_SCAN_INCOMPLETE   0
#define MESSAGE_SCAN_INVALID     -1
#define MESSAGE_SCAN_UNSUPPORTED -2

class CMessageScanner
{
public:
	//! Constructor
	//! CMessageProxy determines which message types are supported
	CMessageScanner(const CMessageProxy* pMessageProxy);

	//! Destructor
	~CMessageScanner();

	//! Scan Message
	//! Calls IMessageProcessor::isCompleteMessage(char *pBuf, int iBufLen)
	//! 
	//! Returns length > 0 if the message appears complete 
	//! Otherwise, returns MESSAGE_SCAN_INCOMPLETE  if message is incomplete
	//!            returns MESSAGE_SCAN_UNSUPPORTED if message type is unsupported
	int Scan(char *pBuf, int iBufLen, int iMessageType) const;

	//! Returns whether processor for message type supports multi-threading
	bool SupportsMultiThreading(int iMessageType) const;

protected:
	const CMessageProxy* m_pMessageProxy;
	bool m_bMessageProxyNULL;

	IMessageProcessor** m_pProcessors;
};

#endif //_MESSAGE_SCANNER_H_


