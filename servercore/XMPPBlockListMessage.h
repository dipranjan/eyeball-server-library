// XMPPBlockListMessage.h
// Interface for the XMPPBlockListMessage class

#ifndef XMPP_BLOCK_LIST_MESSAGE_H
#define XMPP_BLOCK_LIST_MESSAGE_H

#include "QueryMessage.h"

#include <vector>
using std::vector;

class XMPPBlockListMessage : public QueryMessage
{
public:
  XMPPBlockListMessage();
  virtual ~XMPPBlockListMessage();

  virtual int deserialize(const string& buf);
  virtual void serialize(string& buf) const;

  virtual u_int16_t payload_length(void) const;

#ifndef _WIN32_WCE
        friend ostream& operator<<(ostream& s, const XMPPBlockListMessage& v);
#endif

public:

  enum {
    EACTION_GET_FOR_RESOURCE,
    EACTION_ADD_LIST,
    EACTION_REMOVE_LIST,
    EACTION_SET_DEFAULT,
    EACTION_RESET_DEFAULT,
    EACTION_USE_LIST,
    EACTION_UNUSE_LIST,
    EACTION_USE_DEFAULT,
    EACTION_SUCCESS,
    EACTION_LIST_NOT_FOUND,
    EACTION_CONFLICT,
  };

  string m_sName;
  u_int8_t m_u8Action;
  string m_sDefaultList;
  vector<string> m_List;

  vector<string> m_Resources;
  vector<string> m_ResourceBlockLists;

  const static string m_sDefaultListResource;
};

#endif//XMPP_BLOCK_LIST_MESSAGE_H
