//! ForwardMessageProcessor.h
//! Interface for the CForwardMessageProcessor class

#ifndef _RELAY__PROCESSOR_H_
#define _RELAY__PROCESSOR_H_

#include "MessageProcessor.h"
#include "processor_defines.h"

class CForwardMessageProcessor : public IMessageProcessor
{
public:

  //! Constructor
  CForwardMessageProcessor();

  //! Destructor
  virtual ~CForwardMessageProcessor();

  //! creates a new instance of the actual processor type (without copying any status information), required
  //! when instantiating new processors in a Connection object
  virtual IMessageProcessor* createNewInstance();

  //! does not contain validity checks, only checks for length and 'reasonable' content
  virtual int isCompleteMessage(char *pBuf, int iBufLen);   

  //! process a complete message; provide IP and port of message origin
  virtual int processMessage(char *pBuf, int iBufLen, const string& IP, int port, int proto, const string& recvdIP, int recvdPort, int recvdProto);

  //! return the response to the previously parsed message
  virtual int getResponse(char *&pBuf, int &iBufLen, string &IP, int &port, int &protocol) {return MSG_INVALID;}

  //! return the processor type
  virtual int getProcessorType() {return FORWARD_MESSAGES;}


  //Added for multi-threaded server

  //! multi-thread enabled
  virtual bool isMT() {return true;}
};





#endif

