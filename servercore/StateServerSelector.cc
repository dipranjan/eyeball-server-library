//! StateServerSelector.cc
//! Implementation of the CStateServerSelector class

#include "StateServerSelector.h"
#include <arpa/inet.h>

#include "debug.h"
#include "log.h"
#include "PeriodicTask.h"
#include "string_utils.h"
#include <time.h>

// Helper class...
class PeriodicStateServerUpdate : public CPeriodicTask
{
public:
  PeriodicStateServerUpdate(int nPeriod, CStateServerSelector* pSelector) : 
    CPeriodicTask(nPeriod),
    m_pSelector(pSelector)
  {
  }

  virtual ~PeriodicStateServerUpdate() {}  // do nothing

  virtual void DoTask()
  { 
    m_pSelector->UpdateStateServerStatus(); 
  }

private:
  CStateServerSelector* m_pSelector;
};

//! Constructor
CStateServerSelector::CStateServerSelector()
: m_pStateServerTable(NULL),
  m_iNumStateServers(0),
  m_iHashValueDivisor(0x3FFFFFFF),
  m_pPeriodicUpdate(NULL),
  m_iLastUpdate(0)
{
	DB_ENTER(CStateServerSelector::CStateServerSelector);

	pthread_mutex_init(&m_Mutex, NULL);
}

//! Destructor
CStateServerSelector::~CStateServerSelector()
{
	DB_ENTER(CStateServerSelector::~CStateServerSelector);

	if (NULL != m_pPeriodicUpdate)
	{
	  delete m_pPeriodicUpdate;
	  m_pPeriodicUpdate = NULL;
	}

	for (int i = 0; i < m_iNumStateServers; i++) {
		delete m_pStateServerTable[i];
	}
	delete [] m_pStateServerTable;

	pthread_mutex_destroy(&m_Mutex);
}

//! Choose state server
//! Returns true if there is a state server
//!   Assigns value to m_sServerAddress and m_iServerPort
//! Returns false if there are no state servers
bool CStateServerSelector::ChooseStateServer(int iHash, string& sServerAddress, int& iServerPort)
{
	DB_ENTER(CStateServerSelector::ChooseStateServer);
	bool bReturnValue = true;

	pthread_mutex_lock(&m_Mutex);

	if (m_iNumStateServers) {
		StateServerInfo* pStateServer = m_pStateServerTable[0];

		if (m_iNumStateServers > 1) {
			DB("Hash Value " << iHash);
			DB("Using Server " << (iHash / m_iHashValueDivisor + 1) << " of " << m_iNumStateServers);

			pStateServer = m_pStateServerTable[iHash / m_iHashValueDivisor];
		}
		else {
			DB("One state server");
		}

		sServerAddress = pStateServer->sAddress;
		iServerPort = pStateServer->iPort;

		DB("State server at " << sServerAddress << ":" << iServerPort);
	}
	else {
		DB("No state servers");
		bReturnValue = false;
	}

	pthread_mutex_unlock(&m_Mutex);

	return bReturnValue;
}

//! Set state server as down
//! Usually called after a query times out
//! May send message to State Server and write to database
//! Returns false if database write failed or if server address is unrecognized
bool CStateServerSelector::SetStateServerDown(const string& sServerAddress, int iServerPort)
{
	DB_ENTER(CStateServerSelector::SetStateServerDown);

	struct in_addr addr;
	int iResult = inet_pton(AF_INET, sServerAddress.c_str(), &addr);
	if (iResult <= 0) {
		return false;
	}

	unsigned int uiAddress = ntohl(addr.s_addr);
	bool bFound = false;

	pthread_mutex_lock(&m_Mutex);

	for (int i = 0; i < m_iNumStateServers; i++) {
		if (bFound) {
			m_pStateServerTable[i - 1] = m_pStateServerTable[i];
		}
		else {
			if (uiAddress == m_pStateServerTable[i]->iAddress) {
				if ((unsigned int)iServerPort == m_pStateServerTable[i]->iPort) {
					bFound = true;
					delete m_pStateServerTable[i];
				}
			}
		}
	}

	if (bFound) {
		if (--m_iNumStateServers) {
			m_iHashValueDivisor = 0x3FFFFFFF / m_iNumStateServers + 1;
		}
		else {
			m_iHashValueDivisor = 0x3FFFFFFF;
		}
	}

	pthread_mutex_unlock(&m_Mutex);

	if (bFound) {
		//Write to Database
	}

	return bFound;
}

//! Retrieve status of state servers
//! Could be called in a thread
//! Returns true if succeeded
//! Returns false if retrieval failed, eg. if database query failed
bool CStateServerSelector::UpdateStateServerStatus()
{
	DB_ENTER(CStateServerSelector::UpdateStateServerStatus);
	
	StateServerInfo** pTable = NULL;
	int iNumStateServers = 0;
	int iHashValueDivisor = 0x3FFFFFFF;

	iNumStateServers = GetListOfStateServers(pTable);
    LOGL(5, "NumStateServers: "<<iNumStateServers);
	if (iNumStateServers > 0) {
		//Check if previous list is the same as the new list
		bool bDifferent = true;
		if (iNumStateServers == m_iNumStateServers) {
			bDifferent = false;
			for (int i = 0; i < iNumStateServers; i++) {
				if ((pTable[i]->iAddress != m_pStateServerTable[i]->iAddress) ||
				    (pTable[i]->iPort != m_pStateServerTable[i]->iPort)) {
					bDifferent = true;
					break;
				}
			}
		}

		if (bDifferent) {
			iHashValueDivisor = 0x3FFFFFFF / iNumStateServers + 1;

			pthread_mutex_lock(&m_Mutex);

			StateServerInfo** pOldTable = m_pStateServerTable;
			int iOldNumStateServers = m_iNumStateServers;

			m_pStateServerTable = pTable;
			m_iNumStateServers = iNumStateServers;
			m_iHashValueDivisor = iHashValueDivisor;

			pthread_mutex_unlock(&m_Mutex);

			m_iLastUpdate = time(NULL);

			for (int i = 0; i < iOldNumStateServers; i++) {
				delete pOldTable[i];	
			}
			delete [] pOldTable;
		}
		else {
			for (int i = 0; i < iNumStateServers; i++) {
				delete pTable[i];	
			}
			delete [] pTable;
		}
	}
	return true;	
}

bool CStateServerSelector::ActivatePeriodicUpdate(int iPeriod)
{
  if (iPeriod < 0)
    return false;

  if (NULL != m_pPeriodicUpdate)
  {
    delete m_pPeriodicUpdate;
    m_pPeriodicUpdate = NULL;
  }
  
  if (iPeriod > 0)
  {
    m_pPeriodicUpdate = new PeriodicStateServerUpdate(iPeriod, this);
  }

  return true;
}

string CStateServerSelector::GetPortInformation() const
{
  return "\nActive state servers: " + itos(m_iNumStateServers);
}

