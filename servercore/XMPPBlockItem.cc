#include "XMPPBlockItem.h"

ostream& operator<<(ostream& s, const XMPPBlockItem& v)
{
        s << v.m_u8Type
          << " " << v.m_sValue
          << (v.m_bAllow ? " Y" : " N")
          << " " << v.m_u32Order
          << (v.m_bMessage ? " Y" : " N")
          << (v.m_bIQ? " Y" : " N")
          << (v.m_bPresenceIn ? " Y" : " N")
          << (v.m_bPresenceOut ? " Y" : " N");

        return s;
}

