// XmppContactInfo.h
// Interface for the XMPPContactInfo structure 

#ifndef XMPP_CONTACT_INFO_H
#define XMPP_CONTACT_INFO_H

#include <string>
using std::string;

#include <iostream>
using std::ostream;

struct XMPPContactInfo {
  XMPPContactInfo() 
    : m_sBareJid(),
      m_sDisplayName(),
      m_sGroup(),
      m_sSubscriptionState("none"),
      m_bPendingIn(false),
      m_bPendingOut(false)
  {
  }

  ~XMPPContactInfo() {}

  XMPPContactInfo(const XMPPContactInfo& c)
    : m_sBareJid(c.m_sBareJid),
      m_sDisplayName(c.m_sDisplayName),
      m_sGroup(c.m_sGroup),
      m_sSubscriptionState(c.m_sSubscriptionState),
      m_bPendingIn(c.m_bPendingIn),
      m_bPendingOut(c.m_bPendingOut)
  {
  }

#ifndef _WIN32_WCE
  friend ostream& operator<<(ostream& s, const XMPPContactInfo& v);
#endif

  string m_sBareJid;
  string m_sDisplayName;
  string m_sGroup;
  string m_sSubscriptionState;
  bool m_bPendingIn;
  bool m_bPendingOut;
};

#endif//XMPP_CONTACT_INFO_H
