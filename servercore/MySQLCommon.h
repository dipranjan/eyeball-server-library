

/*
  $Id: MySQLCommon.h,v 1.10 2007/05/15 16:22:48 zionk Exp $
*/

#ifndef __MYSQL_COMMON_H_
#define	__MYSQL_COMMON_H_


#include "MySQLBase.h" 


class MySQLCommon : public MySQLBase
{
 public:

  MySQLCommon();
  virtual ~MySQLCommon();

  int OpenUsingConfigFile(const string& sConfigFilename);

  int SetConfigValue(const string& sName, const string& sValue);
  int GetConfigValue(const string& sName, string& sValue);

  //! Convert between time (seconds since 1970)
  //! and Date Time used in MySQL: YYYY-MM-DD HH:MM:SS
  static void TimeToDateTime(time_t t, char* pcDateTime);
  static time_t DateTimeToTime(const char* pcDateTime);

  static string ContactToString(u_int32_t uAddr, u_int16_t uPort, u_int8_t uProtocol);
  static void StringToContact(const string& sContact, u_int32_t& uAddr, u_int16_t& uPort, u_int8_t& uProtocol);

  // Indicate that the state server list has changed.
  int SetLastSIPStateServerListChange();
  int SetLastXMPPStateServerListChange();

  // Perform query to see if the distributed state server list is stable.
  bool IsSIPStateServerListStable();
  bool IsXMPPStateServerListStable();

  // Retrieves the state server expiry time (same for all state servers)
  int GetStateServerExpiryTime() const;
  int GetEdgeServerExpiryTime() const;

  bool GetPassword(const string& sUser, string &sPass);
};

#endif


/*
  $Log: MySQLCommon.h,v $
  Revision 1.10  2007/05/15 16:22:48  zionk
  Begin modifications to servercoremt for XMPP server-to-server communication

  Revision 1.9  2006/09/21 00:22:52  zionk
  Add GetPassword

  Revision 1.8  2006/09/05 17:47:12  zionk
  Have separate LastStateServerListChange entries in ServerConfig table for
  SIP and XMPP

  Revision 1.7  2006/07/25 17:36:22  zionk
  Make functions static

  Revision 1.6  2006/07/07 21:08:53  mchung
  adjusted expiry times for state servers from 5 to 15 seconds
  (trying to tune for better behavior with slow database servers)

  Revision 1.5  2006/05/03 21:44:38  zionk
  Rename CMySQLCommon::Open() to OpenUsingConfigFile()

  Revision 1.4  2006/05/03 21:39:44  zionk
  Add open that uses MySQLConfigFile

  Revision 1.3  2006/05/01 21:34:57  zionk
  Protocol is now 8 bits instead of 16

  Revision 1.2  2006/05/01 19:49:14  michaelc
  last-state-server-list-change queries

  Revision 1.1  2006/04/24 20:46:31  zionk
  Move some common functionality, which is not SIP-specific but is not
  basic MySQL commands, to MySQLCommon

  Revision 1.1  2006/04/24 16:44:01  zionk
  Added MySQL.h and MySQL.cc

  Revision 1.6  2006/04/24 16:30:32  zionk
  Added MySQL.o and SIPStateServerSelector.o
  Transfered SIP-specific functionality from servercoremt

  Revision 1.12  2006/04/21 23:52:55  zionk
  Add GetCallIDsForUser

  Revision 1.11  2006/04/20 20:52:56  zionk
  Use MySQLBase

  Revision 1.10  2006/04/11 00:59:09  michaelc
  updated remote-logging to use database for server address/port

  Revision 1.9  2006/04/06 21:47:02  zionk
  Added getTemplate and putTemplate

  Revision 1.8  2006/03/28 23:31:53  michaelc
  define ServerList type

  Revision 1.7  2006/03/27 23:15:58  michaelc
  add: RegisterStateServer and GetStateServerList functions for using database to manage active state server list

  Revision 1.6  2006/03/23 20:02:30  zionk
  Changed char* to const char*. Also changed UserID field in InsertLoginsHistory
  to UserId (is it case-sensitive?)

  Revision 1.5  2006/03/21 01:34:26  michaelc
  read from database on startup

  Revision 1.4  2006/03/21 00:34:46  zionk
  Added InsertEdgeProxyHistory and InsertStateServerHistory

  Revision 1.3  2006/03/21 00:08:45  michaelc
  for debugging

  Revision 1.2  2006/03/20 17:54:27  zionk
  Added conversion functions to convert between time_t (seconds) and
  DateTime (YYYY-MM-DD HH:MM:SS)

  Revision 1.1  2006/03/17 23:46:34  michaelc
  *** empty log message ***

  Revision 1.11  2006/03/08 19:08:04  larsb
  cleanup stale information (calls,logins) at startup

  Revision 1.10  2006/03/01 06:55:16  larsb
  periodic logging of usage stats

  Revision 1.9  2006/01/20 06:05:04  larsb
  reorganized startup for licensing
  reconnect mysql (auto-connect disabled by mysql>5.03)

  Revision 1.8  2005/10/24 22:38:52  larsb
  *** empty log message ***

  Revision 1.7  2005/09/29 20:39:25  larsb
  bugfix: multiple registrations no longer possible
  bugfix: forwarding to own IP prevented

  Revision 1.6  2005/09/02 22:16:55  larsb
  clean up and polishing

  Revision 1.5  2005/08/25 00:15:42  larsb
  enhanced CLI: print_users and message stats
  revised (faster) response generation

  Revision 1.4  2005/08/17 22:34:58  larsb
  added database functions
  changed

*/

