//ServerMT.h
//Interface of multi-threaded server class

#ifndef __SERVER_MT_H_
#define __SERVER_MT_H_

#include <string>
using std::string;

#include <pthread.h>
#include <time.h>
#include "NetworkManager.h"

class CCoreConfigFile;
class CMessageProxy;
class CNetworkManager;
class CSyncUDPQuery;
class CStateServerSelector;

class CServerMT
{
public:

	//! Constructor
	CServerMT(const string& sServerName);

	//! Destructor
	virtual ~CServerMT();

	//! Run
	//! Configuration file is input
	//! Blocks until Server stops
	virtual void Run(CCoreConfigFile* pConf);

	//! Stop
	//! Tries to stop server
	virtual void Stop();

	//! Get status of open ports
	string GetPortInformation();

	//! Get number of connections
	int GetNumConnections();

	//! Get Server Name
	const string& GetServerName() const;

        //! Send Message
	//! pcMessage will be deleted
	//! Precondition: NetworkManager is running
        virtual void SendMessage(const string& sToAddress, int iToPort, int iProtocol,
                         int iMessageType, char* pcMessage, int iLen, int iConvertToTLS = 0,
                         bool bAddConnectionIfNecessary = false, const string& sTagForNewConnection = "");

        //! Forward Message
	//! pcMessage will be deleted
	//! Precondition: NetworkManager is running
        virtual void ForwardMessage(const string& sToAddress, int iToPort, int iProtocol,
	                    const string& sViaAddress, int iViaPort, int iViaProtocol,
                            int iMessageType, char* pcMessage, int iLen, 
			    bool bCloseConnection = false);

        //! Close TCP/TLS Connection
        void CloseConnection(const string& sAddress, int iPort, int iProtocol, int iMessageType);

	//! Send Query
        //! Returns length of Response message, or 0 for timeout, -1 for error
        //! The sequence number in pcMessage is changed
	//! Precondition: Network Manager is running
        int SendQuery(const string& sToAddress, int iToPort,
                char* pcMessage, int iLen, char** pcResponse);

        //! Choose state server
        //! Returns true if there is a state server
        //!   Assigns value to m_sServerAddress and m_iServerPort
        //! Returns false if there are no state servers
	//! Precondition: NetworkManager is running
        virtual bool ChooseStateServer(int iHash, string& sServerAddress, int& iServerPort);

        //! Set state server as down
        //! Usually called after a query times out
        //! May send message to State Server and write to database
        //! Returns false if database write failed or if server address is unrecognized
	//! Precondition: NetworkManager is running
        virtual bool SetStateServerDown(const string& sServerAddress, int iServerPort);

	//! Get start time
	time_t GetStartTime() const;

	const string& Get3DESKey() const;

	virtual const CNetworkManager* getNetworkManager() const;

 protected:
	//! Create Message Proxy
	virtual CMessageProxy* CreateMessageProxy();	

	//! Set up server ports
	//! Called in Run()
	//! Returns true if successful
	virtual bool SetupPorts();

	//! Final setup
	//! Called in Run() just before starting Network Manager
	//! Returns true if successful
	virtual bool FinalSetup();

	//! Cleanup
	//! Called in Run() just after shutting down Network Manager
	virtual void Cleanup();


	string m_sServerName;

	bool m_bRunning;
	bool m_bStopRequested;

	pthread_mutex_t m_StopMutex;

	//! Created and destroyed in Run()
	CNetworkManager* m_pNetworkManager;
	CSyncUDPQuery* m_pQuery;
	CStateServerSelector* m_pStateServerSelector;

	const CCoreConfigFile* m_pConfig; 

	time_t m_tStart;

	string m_s3DESKey;


};

//global
extern CServerMT* g_pServerMT;

#endif //__SERVER_MT_H_

