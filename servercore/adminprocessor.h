
/*
  $Id: adminprocessor.h,v 1.4 2006/08/29 21:00:10 zionk Exp $
*/

#ifndef _ADMINPROCESSOR
#define _ADMINPROCESSOR


#include <string>



#include "MessageProcessor.h"
#include "core_globals.h"


using namespace std;




// status definitions of the state machine for login and password changes
#define CLI_LOGIN_CHALLENGE       0
#define CLI_LOGIN_SUCCESS         1

#define CLI_LOGIN_TRY             3

#define CLI_OLDPASS               2
#define CLI_NEWPASS1              3
#define CLI_NEWPASS2              4
#define CLI_NEWPASS3              5

#define CLI_CHANGE_PASS           6

#define CLI_SHUTDOWN              7


// error codes
#define CLI_OK                    0
#define CLI_ERROR                 -1




class AdminProcessor : public IMessageProcessor
{

 protected:

#ifdef CLI_PASSWORD
  string m_sCLIPassword;
  string m_sPassword1;
  int m_iPWChangeStatus;
  int m_iLoginCount;
#endif

  int m_iResponseMsgCount;

  string m_ResponseBuffer;
  int m_iResponseBufLen;

  string m_fromIP;
  int m_fromPort;
  int m_fromProtocol;

  int m_iStatus;

  int m_iLogInStatus;

  bool m_bPrompt;

 protected:

#ifdef CLI_PASSWORD
  void _changePassword(const string& content);
  string _loadCLIPassword(const string& sFileName);
  int _saveCLIPassword(const string& sFileName, const string& passwd);
#endif

  virtual void _printHelp();
  virtual void _printUptime();


 public:

  AdminProcessor();
  
  virtual ~AdminProcessor();

  virtual IMessageProcessor* createNewInstance();

  virtual int sendWelcome(string &sWelcome);

  virtual int isValidMessage(const string &sBuf);

  virtual int isCompleteMessage(char *pBuf, int iBufLen);

  virtual int processMessage(char *pBuf, int iBufLen, const string& IP, int port, int protocol, const string& recvdIP, int recvdPort, int recvdProto);

  virtual int getResponse(char *&pBuf, int &iBufLen, string &IP, int &port, int &protocol);

  virtual int getProcessorType();

  virtual bool isBusy();

  virtual void setBusy(bool bBusy) {}

  virtual bool isFinal();

  virtual void setFinal(bool bFinal) {};

  virtual int OnServiceNotify(int iServiceType, void *pResponse);

  virtual int getTimeout() {return CLI_TIMEOUT;}

};





#endif



/*
  $Log: adminprocessor.h,v $
  Revision 1.4  2006/08/29 21:00:10  zionk
  Share password file between MySQL and command line interface. Command Line
  Interface has user "cli".

  Revision 1.3  2006/07/27 20:51:27  zionk
  Use const string& parameters

  Revision 1.2  2006/03/09 00:43:59  zionk
  Implemented getTimeout function

  Revision 1.1  2006/03/07 18:12:00  zionk
  Multi-threaded server core

  Revision 1.11  2006/02/24 18:13:38  zionk
  Make inline virtual functions virtual

  Revision 1.10  2005/10/22 00:46:49  larsb
  added 100 Trying
  fixed IP:port routing problem

  Revision 1.9  2005/10/19 22:05:18  larsb
  removed memory leak in send buffers

  Revision 1.8  2005/10/04 23:44:06  larsb
  added some comments

  Revision 1.7  2005/10/03 20:54:25  larsb
  added some config parameters

  Revision 1.6  2005/09/15 23:28:43  larsb
  *** empty log message ***

  Revision 1.5  2005/09/02 22:17:11  larsb
  clean up and polishing

  Revision 1.4  2005/08/17 22:34:20  larsb
  changed configuratioN: spilt into core_config.h (servercore/) and config.h/.cc (custom/)

  Revision 1.3  2005/08/17 00:06:32  larsb
  *** empty log message ***

  Revision 1.2  2005/08/13 00:40:43  larsb
  *** empty log message ***

  Revision 1.1  2005/08/08 18:55:57  larsb
  reoriganisation of command line interface (adminprocessor) and global variables

  Revision 1.4  2005/08/05 23:41:10  larsb
  server tag working

  Revision 1.3  2005/08/05 19:34:44  larsb
  *** empty log message ***

  Revision 1.2  2005/07/25 23:57:30  larsb
  moved global/config modules from servercore to custom
  added telnet admin configuration

  Revision 1.1  2005/07/25 22:40:33  larsb
  *** empty log message ***

*/

