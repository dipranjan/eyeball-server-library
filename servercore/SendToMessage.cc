// ---------------------------------------------------------------------------
// File:       SendToMessage.cc
//
// Copyright:  Eyeball Networks Inc. 1999-2006
//
// Purpose:    Tell server to send a message
//
// Change Log: 03/01/2006 - Created
// ---------------------------------------------------------------------------

#include "SendToMessage.h"
#include "Socket.h"
#include "string_utils.h"

#if defined(WIN32) || defined(_WIN32_WCE)
#include "Base.h"  // automatic use of precompiled headers
#endif

#include "debug.h"

SendToMessage::SendToMessage()
:
	CommandMessage(EYEBALL_SERVER_SEND_TO),

	m_u32Address(0),
	m_u16Port(0),
	m_u8Protocol(0),
	m_u8MessageType(0),
	m_u8ConvertToTLS(0),
	m_u8AddConnection(0)
{
	DB_ENTER(SendToMessage::SendToMessage);

	m_u16PayloadLength = 20;
}

SendToMessage::~SendToMessage()
{
	DB_ENTER(SendToMessage::~SendToMessage);
}

void SendToMessage::serialize(string& sBuf) const
{
	DB_ENTER(SendToMessage::serialize);

	SERIALIZE_BASE_CLASS(CommandMessage);

	SERIALIZE32(m_u32Address);
	SERIALIZE16(m_u16Port);
	SERIALIZE8(m_u8Protocol);
	SERIALIZE8(m_u8MessageType);
	//SERIALIZE64(m_u32MessagePtr);
	SERIALIZE32(m_u32MessageLen);
	SERIALIZE8(m_u8ConvertToTLS);
	SERIALIZE8(m_u8AddConnection);
        SERIALIZE_STR16(m_sTagForNewConnection);
}

int SendToMessage::deserialize(const string& sBuf)
{
	DB_ENTER(SendToMessage::deserialize);

	int iOffset = 0;

	DESERIALIZE_BASE_CLASS(CommandMessage);

	DESERIALIZE32(m_u32Address);
	DESERIALIZE16(m_u16Port);
	DESERIALIZE8(m_u8Protocol);
	DESERIALIZE8(m_u8MessageType);
	//DESERIALIZE64(m_u32MessagePtr);
	DESERIALIZE32(m_u32MessageLen);
	DESERIALIZE8(m_u8ConvertToTLS);
	DESERIALIZE8(m_u8AddConnection);
        DESERIALIZE_STR16(m_sTagForNewConnection);

	return iOffset;
}

u_int16_t SendToMessage::payload_length(void) const
{
        return sizeof(m_u32Address) + sizeof(m_u16Port) +
               sizeof(m_u8Protocol) + sizeof(m_u8MessageType) +
	       sizeof(m_u32MessagePtr) + sizeof(m_u32MessageLen) +
	       sizeof(m_u8ConvertToTLS) + sizeof(m_u8AddConnection) +
	       2 + m_sTagForNewConnection.size();
}

//Converts string to 32-bit integer address
//Returns false if address invalid or conversion failed
bool SendToMessage::setAddress(const string& sAddress)
{
	struct in_addr address;
	int result = inet_pton(AF_INET, sAddress.c_str(), &address);

	if (result) {
		m_u32Address = ntohl(address.s_addr);
		return true;
	}
	else {
		return false;
	}
}

//Returns address as a string 
string& SendToMessage::getAddress(string& sAddress) const
{
        struct in_addr address;
	memset(&address, 0, sizeof(address));
        address.s_addr = htonl(m_u32Address);

        char dst[INET_ADDRSTRLEN];
        if (inet_ntop(AF_INET, &address, dst, sizeof(dst)) != NULL) {
                sAddress = string(dst);
        }
        else {
                sAddress = string("");
        }

	return sAddress;
}       

#ifndef _WIN32_WCE
ostream& operator<<(ostream& s, const SendToMessage& v)
{
	string sProtocol;
	if (v.m_u8Protocol == UDP) {
		sProtocol = "UDP";
	}
	else if (v.m_u8Protocol == TCP) {
		sProtocol = "TCP";
	}
	else if (v.m_u8Protocol == TLS) {
		sProtocol = "TLS";
	}
	else {
		sProtocol = "Unknown: ";
		sProtocol += itos(v.m_u8Protocol);
	}

	string sAddress;
	v.getAddress(sAddress);

	s << (CommandMessage)v
		<< " " << sAddress
		<< ":" << v.m_u16Port << "/" << sProtocol.c_str()
		<< " Message Type: " << v.m_u8MessageType
		<< " Message Pointer: " << v.m_u32MessagePtr
		<< " Message Length: " << v.m_u32MessageLen
		<< " Convert to TLS: " << v.m_u8ConvertToTLS
		<< " Add Connection: " << v.m_u8AddConnection
		<< " Tag for New Connection: " << v.m_sTagForNewConnection.c_str();

	return s;
}
#endif
