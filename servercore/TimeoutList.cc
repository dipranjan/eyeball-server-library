// TimeoutList.cc
// Implementation for the CTimeoutList class

#include "TimeoutList.h"
#include <time.h>
#include "ConnectionHashTable.h"

#include "debug.h"
#include "log.h"

//! Constructor
CTimeoutList::CTimeoutList(int iMaxConnections)
: m_pNewest(NULL),
  m_pOldest(NULL),
  m_pHashTable(new CConnectionHashTable(iMaxConnections))
{
	DB_ENTER(CTimeoutList::CTimeoutList);
}

//! Destructor
CTimeoutList::~CTimeoutList()
{
	DB_ENTER(CTimeoutList::~CTimeoutList);

	node* pTmp = m_pOldest;

	while (m_pOldest != NULL) {
		m_pOldest = m_pOldest->pNewer;
		delete pTmp;
		pTmp = m_pOldest;
	}

	delete m_pHashTable;
}

//! Add Timeout
void CTimeoutList::AddTimeout(const string& sAddress, int iPort, int iProtocol, int iMessageType, int iTimeout)
{
	DB_ENTER(CTimeoutList::AddTimeout);

	//Search for existing entry
	node* pNode = (node*)m_pHashTable->Get(sAddress, iPort, iProtocol, iMessageType);
	int iExpire = time(NULL) + iTimeout;

	if (pNode != NULL) {
		pNode->iExpire = iExpire;		

		DB("remove timeout temporarily");

		//Remove from list
		if (pNode->pNewer == NULL) {
			//Newest
			if (pNode->pOlder == NULL) {
				//Only node
				//Don't move it
				return;
			}
			else {
				if ((iExpire - pNode->pOlder->iExpire) >= 0) {
					//iExpire still the newest (largest)
					return;
				}
				else {
					//Out of order. Remove for now
					m_pNewest = pNode->pOlder;
					m_pNewest->pNewer = NULL;
				}
			}
		}
		else {
			//Not the newest 
			if (pNode->pOlder == NULL) {
				//Oldest
				if ((iExpire - pNode->pNewer->iExpire) <= 0) {
					//iExpire still the oldest (smallest)
					return;
				}
				else {
					//Out of order. Remove for now 
					m_pOldest = pNode->pNewer;
					m_pOldest->pOlder = NULL;
				}
			}
			else {
				//In the middle
				if (((iExpire - pNode->pNewer->iExpire) <= 0) && 
				    ((iExpire - pNode->pOlder->iExpire) >= 0)) {
					//Still in order
					return;
				}
				else {
					//Out of order. Remove for now 
					pNode->pNewer->pOlder = pNode->pOlder;
					pNode->pOlder->pNewer = pNode->pNewer;
				}
			}
		}	
	}
	else {
		//Need to create new node
		pNode = new node;
		pNode->sAddress = sAddress;
		pNode->usPort = iPort;
		pNode->ucProtocol = iProtocol;
		pNode->ucMessageType = iMessageType;
		pNode->iExpire = iExpire;
	
		//Add to hash table
		m_pHashTable->Put(sAddress, iPort, iProtocol, iMessageType, pNode);
	}

	DB("Insert timeout");

	//At this point, pNode points to a node to be inserted into the linked list
	//Check from the newest, as most new expire times are newer
	node* pCurr = m_pNewest;

	while (pCurr != NULL) {
		if ((iExpire - pCurr->iExpire) >= 0) {
			//New timeout is newer than current timeout
			if (pCurr == m_pNewest) {
				m_pNewest = pNode;
			}
			else {
				pCurr->pNewer->pOlder = pNode;
			}
			pNode->pNewer = pCurr->pNewer;
			pCurr->pNewer = pNode;
			pNode->pOlder = pCurr;
			return;
		}
		pCurr = pCurr->pOlder;
	}

	DB("Oldest timeout");

	//Oldest
	if (m_pOldest != NULL) {
		m_pOldest->pOlder = pNode;
	}
	pNode->pNewer = m_pOldest;
	pNode->pOlder = NULL;
	m_pOldest = pNode;
	if (m_pNewest == NULL) {
		m_pNewest = pNode;
	}
}

//! Remove Timeout
void CTimeoutList::RemoveTimeout(const string& sAddress, int iPort, int iProtocol, int iMessageType)
{
	DB_ENTER(CTimeoutList::RemoveTimeout);

	//Search for existing entry
	node* pNode = (node*)m_pHashTable->Get(sAddress, iPort, iProtocol, iMessageType);

	if (pNode != NULL) {
		if (pNode->pNewer != NULL) {
			pNode->pNewer->pOlder = pNode->pOlder;
		}
		else {
			m_pNewest = pNode->pOlder;
		}

		if (pNode->pOlder != NULL) {
			pNode->pOlder->pNewer = pNode->pNewer;
		}
		else {
			m_pOldest = pNode->pNewer;
		}
		
		m_pHashTable->Put(sAddress, iPort, iProtocol, iMessageType, NULL);
		delete pNode;
	}
}

//! Get Expired
//! Removes oldest expired item
//! Returns true if something expired
bool CTimeoutList::GetExpired(string& sAddress, int& iPort, int& iProtocol, int& iMessageType)
{
	DB_ENTER(CTimeoutList::GetExpired);

	if (m_pOldest != NULL) {
		int iCurrTime = time(NULL);
		if ((m_pOldest->iExpire - iCurrTime) <= 0) {
			//Oldest expire is smaller than current time

			sAddress = m_pOldest->sAddress;
			iPort = m_pOldest->usPort;
			iProtocol = m_pOldest->ucProtocol;
			iMessageType = m_pOldest->ucMessageType;
			m_pHashTable->Put(sAddress, iPort, iProtocol, iMessageType, NULL);

			//Remove oldest
			node* pTmp = m_pOldest;
			m_pOldest = m_pOldest->pNewer;

			if (m_pOldest == NULL) {
				m_pNewest = NULL;
			}
			else {
				m_pOldest->pOlder = NULL;
			}

			delete pTmp;
			
			return true;
		}
	}

	return false;
}

