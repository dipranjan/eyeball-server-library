//! CoreConfigFile.h
//! Interface of Core Config File class

#ifndef __CORE_CONFIG_FILE_H_
#define __CORE_CONFIG_FILE_H_

#include "DBConfigFile.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <ifaddrs.h>
#include <errno.h>
class CCoreConfigFile : public DBConfigFile 
{
public:
	//! Constructor
	CCoreConfigFile();

	//! Destructor
	virtual ~CCoreConfigFile();

	//! Set Default Configuration
	virtual void SetDefaultConfiguration();

	//! Post-Parse Processing
	//! Returns true if successful
	virtual bool PostParseProcessing();

	//! Check Configuration
	//! Returns true if no errors
	virtual bool CheckConfiguration();

	//! Process Header/Value Pair
	//! Precondition: Header is lowercase. cHeader and cValue at least one character long
	//! Returns true if header found and handled, even if syntax errors are found
	virtual bool ProcessHeaderValuePair(const char* cHeader, const char* cValue);

	//! Returns string containing the configuration
	virtual string getSettings();

    void getAllIps(vector<string> &ips);
public:
  string m_sIP;
  string m_sPrivateIP;
  string m_sDomain;
  string m_sHostName;
  bool m_bIsBindAddrAny;
  //char *m_szFQHostname;

  //char *m_szConfPath;

  // size of the send buffers (TCP)
  int m_iSendBufferSize;

  // size of receive buffers (UDP and TCP)
  int m_iRecvBufferSize;

  int m_iTCPConnectionTimeout;

  // amount of TCP connections
  int m_iTCPConnections;
  int m_iTLSConnections;

  // Telnet port for Admin
  int m_iTelnetAdminPort;

  // Forward Message UDP Port
  int m_iForwardMessageUdpPort;

  // Forward Message TCP Port
  int m_iForwardMessageTcpPort;

  // UDP port for communication internal to the distributed server
  // Receives responses from other server components
  int m_iUdpQueryPort;

  //char telnetallow;
  //int maxtelcon;
  //char logmsg;
  bool m_bLogEnabled;
  int m_iLogMaxFileSize;
  int m_iLogMaxFileCount;

  string m_sLogFileName;
  string m_sPidFileName;
  string m_sOutFileName;
  //char m_bRecordEvents;

  ////////////////////////////////////////////////////////////
  // 27/6/2005 added by lars: TLS/SSL cert
  string m_sCertFile;
  string m_sCertKeyFile;
  string m_sCertUser;
  int    m_iTolerableCpuUsage;
  int    m_iCpuUsageCheckTimeoutSec;

  bool m_bDNSCache;

  int m_iDBUpdateStatsInterval;

  string m_sLicenseName;
  string m_sLicenseCertFile;
  string m_sEyeballCertFile;

  int m_iNumThreads;
  int m_iMessageQueueSize;
};

extern CCoreConfigFile* g_pCoreConf;

#endif //__CORE_CONFIG_FILE_H_

