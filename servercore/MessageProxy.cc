// MessageProxy.cc
// Implementation of Message Proxy class that selects a Message Processor according to the message type
// Single-threaded -- to be used in conjunction with multi-threaded ThreadManager
// To be extended using subclasses

#include "MessageProxy.h"
#include "ThreadManager.h"
#include "processor_defines.h"
#include "MessageProcessor.h"
#include "adminprocessor.h"
#include "ForwardMessageProcessor.h"
#include "Socket.h"

#include "debug.h"
#include "log.h"
#include <sys/time.h>

//Constructor
CMessageProxy::CMessageProxy()
: m_pThreadManager(NULL)
{
	DB_ENTER(CMessageProxy::CMessageProxy);
}

//Destructor
CMessageProxy::~CMessageProxy()
{
	DB_ENTER(CMessageProxy::~CMessageProxy);
}

//! Creates Message Proxy of the same subclass
//! Need to Override
CMessageProxy* CMessageProxy::createNewInstance() const
{
	DB_ENTER(CMessageProxy::createNewInstance);

	return new CMessageProxy;
}

//Set Thread Manager
void CMessageProxy::SetThreadManager(CThreadManager* pThreadManager)
{
	DB_ENTER(CMessageProxy::SetThreadManager);

	m_pThreadManager = pThreadManager;
}

//Called by Thread Manager
//Selects a Message Processor to process message based on message type
//This base implementation only handles Telnet, but can be used as a template for SIP or XMPP
void CMessageProxy::ProcessMessage(const string& sToAddress, int iToPort, 
const string& sFromAddress, int iFromPort, int iProtocol,
int iMessageType, char* pcMessage, int iLen)
{
	DB_ENTER(CMessageProxy::ProcessMessage);

#ifdef PRINT_TIME
        struct timeval now;
        gettimeofday(&now, NULL);
#endif //PRINT_TIME

	if (m_pThreadManager == NULL) {
		delete [] pcMessage;
		return;
	}

	/*
	// convert message to hex and show the first 100 bytes
	char hexVal[3];
	std::string hexStream = "";
	for (int i=0;i<((120>iLen)?iLen:120);i++)
	  {
	    sprintf(hexVal,"%02x",(int)((char*)pcMessage)[i]);
	    hexStream += hexVal + (std::string)" ";
	  }
	LOGL(5,"incoming message: " << hexStream);
	*/
	//Call virtual function
	IMessageProcessor* pMessageProcessor = this->GetProcessorForMessageType(iMessageType);

	if (pMessageProcessor != NULL) {
		if ((pcMessage != NULL) && (iLen > 0)) {
            int proc_result = pMessageProcessor->processMessage(pcMessage, iLen,
						sFromAddress, iFromPort, iProtocol,
						sToAddress, iToPort, iProtocol);
	
			if (proc_result == MSG_RESPONSE_AVAIL) {	
			  char* pcResponse = NULL;
			  int iResponseLen = 0;
			  string sResponseAddress;
			  int iResponsePort;
			  int iResponseProtocol;
			  
			  bool bSent = false;
			  
			  int resp_result = pMessageProcessor->getResponse(pcResponse, iResponseLen, 
									   sResponseAddress, iResponsePort, iResponseProtocol);
	
				if (resp_result != MSG_INVALID)
				{
					bool bSendResponse = ((pcResponse != NULL) && (iResponseLen > 0));
					if (bSendResponse) {
					  int iConvertToTLS = 0;
					  if (resp_result == MSG_CONVERT_TLS_BEFORE)
					    {
					      iConvertToTLS = 1;
					      iProtocol = TLS;
					    }
					  else if (resp_result == MSG_CONVERT_TLS_AFTER)
					    {
					      iConvertToTLS = 2;
					      iProtocol = TLS;
					    }	
					  m_pThreadManager->SendMessage(sResponseAddress, iResponsePort,
									iResponseProtocol, iMessageType, pcResponse, iResponseLen,
									iConvertToTLS);
					  
					  bSent = true;
					  
					}
					if (resp_result == MSG_CLOSE) {
					  //Close connection
					  m_pThreadManager->CloseConnection(sFromAddress, iFromPort, 
									    iProtocol, iMessageType, bSendResponse);
					}
				}
				
				// andaleeb fixing memory leaks
				if (iResponseLen > 0 && !bSent)
				  delete pcResponse;


			}
		}
		else {
			pMessageProcessor->removeBinding(sFromAddress, iFromPort, iProtocol);
		}
		delete pMessageProcessor;
	}

	//clean up memory
	delete [] pcMessage;

#ifdef PRINT_TIMING
        struct timeval done;
        gettimeofday(&done, NULL);
        int microseconds = (done.tv_sec - now.tv_sec) * 1000000 +
                           (done.tv_usec - now.tv_usec);
	if (microseconds > 10000) {
	        printf("Process Message done: %d us\n", microseconds);
	}
#endif //PRINT_TIMING
}

//Override this to handle more message types
//Only supports Telnet Admin in base implementation
IMessageProcessor* CMessageProxy::GetProcessorForMessageType(int iMessageType) const
{
	DB_ENTER(CMessageProxy::GetProcessorForMessageType);

	if (iMessageType == FORWARD_MESSAGES) {
		return new CForwardMessageProcessor;
	}
	else if (iMessageType == ADMIN) {
		return new AdminProcessor;	
	}

	return NULL;
}



