// ---------------------------------------------------------------------------
// File:       ForwardEdgeServerMessage.cc
//
// Copyright:  Eyeball Networks Inc. 1999-2006
//
// Purpose:    Forward SIP INVITE to another Edge Server
//
// Change Log: 03/14/2006 - Created
// ---------------------------------------------------------------------------

#include "ForwardEdgeServerMessage.h"
#include "Socket.h"
#include "string_utils.h"

#if defined(WIN32) || defined(_WIN32_WCE)
#include "Base.h"  // automatic use of precompiled headers
#endif

#include "debug.h"

ForwardEdgeServerMessage::ForwardEdgeServerMessage()
:
	CommandMessage(EYEBALL_SERVER_FORWARD_EDGE),

	m_u32Address(0),
	m_u16Port(0),
	m_u8Protocol(0),
	m_u8MessageProcessorType(0),
	m_u8CloseConnection(0)
{
	DB_ENTER(ForwardEdgeServerMessage::ForwardEdgeServerMessage);

	m_u16PayloadLength = 10;
}

ForwardEdgeServerMessage::~ForwardEdgeServerMessage()
{
	DB_ENTER(ForwardEdgeServerMessage::~ForwardEdgeServerMessage);
}

void ForwardEdgeServerMessage::serialize(string& sBuf) const
{
	DB_ENTER(ForwardEdgeServerMessage::serialize);

	SERIALIZE_BASE_CLASS(CommandMessage);
	SERIALIZE32(m_u32Address);
	SERIALIZE16(m_u16Port);
	SERIALIZE8(m_u8Protocol);
	SERIALIZE8(m_u8MessageProcessorType);
	SERIALIZE8(m_u8CloseConnection);
	SERIALIZE_STR16(m_sMessage);
}

int ForwardEdgeServerMessage::deserialize(const string& sBuf)
{
	DB_ENTER(ForwardEdgeServerMessage::deserialize);

	int iOffset = 0;

	DESERIALIZE_BASE_CLASS(CommandMessage);

	DESERIALIZE32(m_u32Address);
	DESERIALIZE16(m_u16Port);
	DESERIALIZE8(m_u8Protocol);
	DESERIALIZE8(m_u8MessageProcessorType);
	DESERIALIZE8(m_u8CloseConnection);
	DESERIALIZE_STR16(m_sMessage);

	return iOffset;
}

u_int16_t ForwardEdgeServerMessage::payload_length(void) const
{
	DB_ENTER(ForwardEdgeServerMessage::payload_length);

        return sizeof(m_u32Address) + sizeof(m_u16Port) +
               sizeof(m_u8Protocol) + sizeof(m_u8MessageProcessorType) +
	       sizeof(m_u8CloseConnection) + 2 + m_sMessage.size();
}

//Converts string to 32-bit integer address
//Returns false if address invalid or conversion failed
bool ForwardEdgeServerMessage::setAddress(const string& sAddress)
{
	DB_ENTER(ForwardEdgeServerMessage::setAddress);

	struct in_addr address;
	int result = inet_pton(AF_INET, sAddress.c_str(), &address);

	if (result) {
		m_u32Address = ntohl(address.s_addr);
		return true;
	}
	else {
		return false;
	}
}

//Returns address as a string 
string& ForwardEdgeServerMessage::getAddress(string& sAddress) const
{
	DB_ENTER(ForwardEdgeServerMessage::getAddress);

        struct in_addr address;
        memset(&address, 0, sizeof(address));
        address.s_addr = htonl(m_u32Address);

        char dst[INET_ADDRSTRLEN];
        if (inet_ntop(AF_INET, &address, dst, sizeof(dst)) != NULL) {
                sAddress = string(dst);
        }
        else {
                sAddress = string("");
        }

	return sAddress;
}       

#ifndef _WIN32_WCE
ostream& operator<<(ostream& s, const ForwardEdgeServerMessage& v)
{
	string sProtocol;
	if (v.m_u8Protocol == UDP) {
		sProtocol = "UDP";
	}
	else if (v.m_u8Protocol == TCP) {
		sProtocol = "TCP";
	}
	else if (v.m_u8Protocol == TLS) {
		sProtocol = "TLS";
	}
	else {
		sProtocol = "Unknown: ";
		sProtocol += itos(v.m_u8Protocol);
	}

	string sAddress;
	v.getAddress(sAddress);

	s << (CommandMessage)v
		<< " " << sAddress
		<< ":" << v.m_u16Port << "/" << sProtocol.c_str()
		<< " Message Processor Type: " 
		<< itos(v.m_u8MessageProcessorType)
		<< " Close Connection: "
		<< (v.m_u8CloseConnection ? "Yes" : "No")
		<< " " << v.m_sMessage;

	return s;
}
#endif
