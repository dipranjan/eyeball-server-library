#define STATE_SERVER_EXPIRY_TIME 60  // seconds
#define EDGE_SERVER_EXPIRY_TIME 20  // seconds

// time until the state server list is considered "stable"
#define SERVER_LIST_STABLE_TIME 30  // seconds

public:
  int SetConfigValue(const string& sName, const string& sValue);
  int GetConfigValue(const string& sName, string& sValue);

  //! Convert between time (seconds since 1970)
  //! and Date Time used in MySQL: YYYY-MM-DD HH:MM:SS
  static void TimeToDateTime(time_t t, char* pcDateTime);
  static void TimeToDate(time_t t, char* pcDateTime);
  static time_t DateTimeToTime(const char* pcDateTime);

  static string ContactToString(u_int32_t uAddr, u_int16_t uPort, u_int8_t uProtocol);
  static void StringToContact(const string& sContact, u_int32_t& uAddr, u_int16_t& uPort, u_int8_t& uProtocol);

  // Indicate that the state server list has changed.
  int SetLastSIPStateServerListChange();
  int SetLastXMPPStateServerListChange();

  // Perform query to see if the distributed state server list is stable.
  bool IsSIPStateServerListStable();
  bool IsXMPPStateServerListStable();

  // Retrieves the state server expiry time (same for all state servers)
  int GetStateServerExpiryTime() const;
  int GetEdgeServerExpiryTime() const;

  bool GetPassword(const string& sUser, string &sPass);

  string escape(const string& sIn);

  bool _isConnected();

  bool _reconnect();

  virtual bool Open();

  virtual int Open(const std::string& p_rsDatabaseName,
		      const std::vector<std::string>& sDatabaseHosts,
		      const std::string& p_rsDatabaseUser,
		      const std::string& p_rsDatabasePassword,
		      const unsigned int& p_ruiDatabasePort, 
			  char* szVersion, char* szComaptible, int CompatibleNumber );

  bool CheckDBVersion( char* szVersionName, char* szCompatibleName,
						int nCompatibleNumber );
  /// The name of database
  std::string m_sDatabaseName;
  /// the currently used host
  std::string m_sDBHost;
  /// the hostnames
  std::vector<std::string> m_sDatabaseHosts;
  /// The database username
  std::string m_sDatabaseUser;
  /// Database user's password
  std::string m_sDatabasePassword;
  /// TCP port number database server listening to
  unsigned int m_uiDatabasePort;
  /// A boolean to indicate whether connection is open or not
  bool m_bOpen;

