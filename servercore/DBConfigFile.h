//! DBConfigFile.h
//! Interface of Config File class

#ifndef __DB_CONFIG_FILE_H_
#define __DB_CONFIG_FILE_H_

#include <stdio.h>
#include <string>
using std::string;
#include <vector>
using std::vector;

class DBConfigFile 
{
public:
	//! Constructor
	DBConfigFile();

	//! Destructor
	virtual ~DBConfigFile();

	//! Read File
	//! Returns true if successful
	virtual bool ReadFile(const char* cFileName);

	//! Set Default Configuration
	virtual void SetDefaultConfiguration();

	//! Pre-Parse Processing
	//! Expected to call SetDefaultConfiguration()
	virtual void PreParseProcessing();

	//! Parse
	//! Precondition: file pointer is opened for reading
	//! Returns true if successful 
	virtual bool Parse(FILE* fp);

	//! Post-Parse Processing
	//! Returns true if successful
	virtual bool PostParseProcessing();

	//! Check Configuration
	//! Returns true if no errors
	virtual bool CheckConfiguration();

	//! Process Header/Value Pair
	//! Precondition: Header is lowercase. cHeader and cValue at least one character long
	//! Returns true if header found and handled, even if syntax errors are found
	virtual bool ProcessHeaderValuePair(const char* cHeader, const char* cValue);

        //! Returns string containing the configuration
        virtual string getSettings();

public:
	string m_sDBName;
	string m_sDBUser;
	vector<string> m_sDBHosts;
	int m_iDBPort;
	string m_sPassFile;

	string m_sLogDBName;
	string m_sLogDBUser;
	vector<string> m_sLogDBHosts;
	int m_iLogDBPort;

};

#endif //__MYSQL_CONFIG_FILE_H_

