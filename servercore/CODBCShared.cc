#include <string.h>
#include <algorithm>

#define SERVER_HAS_GONE_AWAY	08S01

int CODBC::SetConfigValue( const string& sName, const string& sValue ) {

	SQLHSTMT stmt;
	SQLRETURN ret;
	bool connection_lost = false;

redo_query:
        pthread_mutex_lock(&dbLock);
	ret = SQLAllocHandle( SQL_HANDLE_STMT, m_dbc, &stmt );
        pthread_mutex_unlock(&dbLock);
	if( !SQL_SUCCEEDED( ret ) )
		extract_error( "SQLAllocHandle()", m_env, SQL_HANDLE_ENV );

	char q[1024];
	sprintf( q, "update serverconfig set value='%s' where name='%s'",
				escape(sValue).c_str(), escape(sName).c_str());

	LOGL( 4, "ODBC:" << q );

	ret = SQLExecDirect(stmt, (SQLCHAR*)q, SQL_NTS);
	if( !SQL_SUCCEEDED( ret )) {
		extract_error( "SQLExecDirect", stmt, SQL_HANDLE_STMT );
		SQLFreeHandle( SQL_HANDLE_STMT, stmt );

                LOG( "re-establishing database connection" );
                if (!connection_lost && Open()) {
                        connection_lost = true;
                        goto redo_query;
                }

		return 0;
	}

	SQLFreeHandle( SQL_HANDLE_STMT, stmt );
	return 1;
}


int CODBC::GetConfigValue(const string& sName, string& sValue)
{
  SQLHSTMT stmt;
  SQLRETURN ret;
  bool connection_lost = false;

	sValue.clear();

	char q[1024];
	sprintf( q, "select value from serverconfig where name='%s'",
				escape(sName).c_str());

redo_query:
        pthread_mutex_lock(&dbLock);
	ret = SQLAllocHandle( SQL_HANDLE_STMT, m_dbc, &stmt );
        pthread_mutex_unlock(&dbLock);
	if( !SQL_SUCCEEDED( ret ) )
		extract_error( "SQLAllocHandle()", m_env, SQL_HANDLE_ENV );

	char value[512];

	SQLBindCol( stmt, 1, SQL_C_CHAR, value, sizeof(value), NULL );
	if( ret != SQL_SUCCESS )
		extract_error( "SQLBindCol", m_env, SQL_HANDLE_ENV );

  	LOGL( 4, "ODBC:" << q );

	ret = SQLExecDirect	( stmt, (SQLCHAR*)q, SQL_NTS );
	if( ret != SQL_SUCCESS ) {
		extract_error( "SQLExecDirect", m_env, SQL_HANDLE_ENV );
                LOG( "re-establishing database connection" );
                if (!connection_lost && Open()) {
                        connection_lost = true;
                        goto redo_query;
                }
		return 0;
	}

	ret = SQLFetch( stmt );
	if( !SQL_SUCCEEDED( ret ) && ret != SQL_NO_DATA) {
		extract_error( "SQLFetch", stmt, SQL_HANDLE_STMT );
		SQLFreeHandle( SQL_HANDLE_STMT, stmt );
		return 0;
	}

	SQLFreeHandle( SQL_HANDLE_STMT, stmt );
	sValue = value;
	return 1;  
} 

int CODBC::SetLastSIPStateServerListChange()
{
	SQLHSTMT stmt;
	SQLRETURN ret;
	bool connection_lost = false;

redo_query:
        pthread_mutex_lock(&dbLock);
	ret = SQLAllocHandle( SQL_HANDLE_STMT, m_dbc, &stmt );
        pthread_mutex_unlock(&dbLock);
	if( !SQL_SUCCEEDED( ret ) )
		extract_error( "SQLAllocHandle()", m_env, SQL_HANDLE_ENV );

	char q[1024];
	sprintf( q, "update serverconfig set recordtime=%lu"
           " where name='LastSIPStateServerListChange'", time(NULL) );

	LOGL( 4, "ODBC:" << q );

	ret = SQLExecDirect(stmt, (SQLCHAR*)q, SQL_NTS);
	if( !SQL_SUCCEEDED( ret )) {
		extract_error( "SQLExecDirect", stmt, SQL_HANDLE_STMT );
		SQLFreeHandle( SQL_HANDLE_STMT, stmt );

                LOG( "re-establishing database connection" );
                if (!connection_lost && Open()) {
                        connection_lost = true;
                        goto redo_query;
                }

		return 0;
	}

	SQLFreeHandle( SQL_HANDLE_STMT, stmt );
	return 1;
}

int CODBC::SetLastXMPPStateServerListChange()
{
	SQLHSTMT stmt;
	SQLRETURN ret;
	bool connection_lost = false;

redo_query:
        pthread_mutex_lock(&dbLock);
	ret = SQLAllocHandle( SQL_HANDLE_STMT, m_dbc, &stmt );
        pthread_mutex_unlock(&dbLock);
	if( !SQL_SUCCEEDED( ret ) )
		extract_error( "SQLAllocHandle()", m_env, SQL_HANDLE_ENV );

	char q[1024];
	sprintf( q, "update serverconfig set recordtime=%lu"
				" where name='LastXMPPStateServerListChange'", time(NULL) );

	LOGL( 4, "ODBC:" << q );

	ret = SQLExecDirect(stmt, (SQLCHAR*)q, SQL_NTS);
	if( !SQL_SUCCEEDED( ret )) {
		extract_error( "SQLExecDirect", stmt, SQL_HANDLE_STMT );
		SQLFreeHandle( SQL_HANDLE_STMT, stmt );

                LOG( "re-establishing database connection" );
                if (!connection_lost && Open()) {
                        connection_lost = true;
                        goto redo_query;
                }

		return 0;
	}

	SQLFreeHandle( SQL_HANDLE_STMT, stmt );
	return 1;
}

bool CODBC::IsSIPStateServerListStable()
{
  SQLHSTMT stmt;
  SQLRETURN ret;
  bool connection_lost = false;

	char q[1024];
	sprintf( q, "select recordtime from serverconfig where "
				"name='LastSIPStateServerListChange'"
				" and (%lu-recordtime) < %d",
				time(NULL), SERVER_LIST_STABLE_TIME );

redo_query:
        pthread_mutex_lock(&dbLock);
	ret = SQLAllocHandle( SQL_HANDLE_STMT, m_dbc, &stmt );
        pthread_mutex_unlock(&dbLock);
	if( !SQL_SUCCEEDED( ret ) )
		extract_error( "SQLAllocHandle()", m_env, SQL_HANDLE_ENV );

	char value[512];

	SQLBindCol( stmt, 1, SQL_C_CHAR, value, sizeof(value), NULL );
	if( ret != SQL_SUCCESS )
		extract_error( "SQLBindCol", m_env, SQL_HANDLE_ENV );

  	LOGL( 4, "ODBC:" << q );

	ret = SQLExecDirect	( stmt, (SQLCHAR*)q, SQL_NTS );
	if( ret != SQL_SUCCESS ) {
		extract_error( "SQLExecDirect", m_env, SQL_HANDLE_ENV );
                LOG( "re-establishing database connection" );
                if (!connection_lost && Open()) {
                        connection_lost = true;
                        goto redo_query;
                }
		return false;
	}

	ret = SQLFetch( stmt );
	if (ret == SQL_NO_DATA) {
		SQLFreeHandle( SQL_HANDLE_STMT, stmt );
		return true;
	}

	SQLFreeHandle( SQL_HANDLE_STMT, stmt );
	return false;
}

bool CODBC::IsXMPPStateServerListStable()
{
  SQLHSTMT stmt;
  SQLRETURN ret;
  bool connection_lost = false;

	char q[1024];
	sprintf( q, "select recordtime from serverconfig where "
				"name='LastXMPPStateServerListChange'"
				" and (%lu-recordtime) < %d",
				time(NULL), SERVER_LIST_STABLE_TIME );

redo_query:
        pthread_mutex_lock(&dbLock);
	ret = SQLAllocHandle( SQL_HANDLE_STMT, m_dbc, &stmt );
        pthread_mutex_unlock(&dbLock);
	if( !SQL_SUCCEEDED( ret ) )
		extract_error( "SQLAllocHandle()", m_env, SQL_HANDLE_ENV );

	char value[512];

	SQLBindCol( stmt, 1, SQL_C_CHAR, value, sizeof(value), NULL );
	if( ret != SQL_SUCCESS )
		extract_error( "SQLBindCol", m_env, SQL_HANDLE_ENV );

  	LOGL( 4, "ODBC:" << q );

	ret = SQLExecDirect	( stmt, (SQLCHAR*)q, SQL_NTS );
	if( ret != SQL_SUCCESS ) {
		extract_error( "SQLExecDirect", m_env, SQL_HANDLE_ENV );

                LOG( "re-establishing database connection" );
                if (!connection_lost && Open()) {
                        connection_lost = true;
                        goto redo_query;
                }
		return false;
        }

	ret = SQLFetch( stmt );
	if (ret == SQL_NO_DATA) {
		SQLFreeHandle( SQL_HANDLE_STMT, stmt );
		return true;
	}

	SQLFreeHandle( SQL_HANDLE_STMT, stmt );
	return false;
}


bool CODBC::GetPassword(const string& sUser, string &sPass)
{
  SQLHSTMT stmt;
  SQLRETURN ret;
  bool connection_lost = false;

    LOGL( 4, "Gettting password from database" );
	if( sUser.empty()) {
		LOGL(2, "Username is empty in GetPassword");
		return false;
	}
        
	char q[2048];
	sprintf( q, "select password from account where user_id='%s'"
				" and active='Y'", sUser.c_str());
	
redo_query:
        pthread_mutex_lock(&dbLock);
	ret = SQLAllocHandle( SQL_HANDLE_STMT, m_dbc, &stmt );
        pthread_mutex_unlock(&dbLock);
	if( !SQL_SUCCEEDED( ret ) )
		extract_error( "SQLAllocHandle()", m_env, SQL_HANDLE_ENV );

	char pwd[48];

	SQLBindCol( stmt, 1, SQL_C_CHAR, (SQLPOINTER)pwd, sizeof(pwd), NULL );
	if( ret != SQL_SUCCESS )
		extract_error( "SQLBindCol", m_env, SQL_HANDLE_ENV );

	LOGL( 4, "ODBC:" << q );

	ret = SQLExecDirect	( stmt, (SQLCHAR*)q, SQL_NTS );
	if( ret != SQL_SUCCESS ){
		LOGL( 2, "Password retrieval error from database" );
		extract_error( "SQLExecDirect", m_env, SQL_HANDLE_ENV );

                LOG( "re-establishing database connection" );
                if (!connection_lost && Open()) {
                        connection_lost = true;
                        goto redo_query;
                }

                return false;
	}	

	ret = SQLFetch( stmt );
	if( !SQL_SUCCEEDED( ret )) {
		extract_error( "SQLExecDirect", stmt, SQL_HANDLE_STMT );
		SQLFreeHandle( SQL_HANDLE_STMT, stmt );
		return false;
	}

	SQLFreeHandle( SQL_HANDLE_STMT, stmt );
    sPass = pwd;
	return true;
}

//! Convert between time (seconds since 1970)
//! and Date Time used in MySQLCommon: YYYY-MM-DD HH:MM:SS
void CODBC::TimeToDateTime(time_t t, char* pcDateTime) 
{
  struct tm timeinfo;

  if (localtime_r(&t, &timeinfo)) {
    sprintf(pcDateTime, "%04d-%02d-%02d %02d:%02d:%02d",
            1900 + timeinfo.tm_year, timeinfo.tm_mon + 1, timeinfo.tm_mday,
            timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec);
  }
  else {
    strcpy(pcDateTime, "1970-01-01 00:00:00");
  }
}

//! Convert between time (seconds since 1970)
//! and Date Time used in MySQLCommon: YYYY-MM-DD
void CODBC::TimeToDate(time_t t, char* pcDateTime) 
{
  struct tm timeinfo;

  if (localtime_r(&t, &timeinfo)) {
    sprintf(pcDateTime, "%04d-%02d-%02d",
            1900 + timeinfo.tm_year, timeinfo.tm_mon + 1, timeinfo.tm_mday);
  }
  else {
    strcpy(pcDateTime, "1970-01-01");
  }
}

time_t CODBC::DateTimeToTime(const char* pcDateTime) 
{
  if (strlen(pcDateTime) < 19) {
    return 0;
  }

  char dateTime[20];
  memcpy(dateTime, pcDateTime, 19);

  dateTime[ 4] = '\0';
  dateTime[ 7] = '\0';
  dateTime[10] = '\0';
  dateTime[13] = '\0';
  dateTime[16] = '\0';
  dateTime[19] = '\0';

  struct tm timeinfo;
  memset(&timeinfo, 0, sizeof(timeinfo));

  timeinfo.tm_year = atoi(&(dateTime[ 0])) - 1900;
  timeinfo.tm_mon  = atoi(&(dateTime[ 5])) - 1;
  timeinfo.tm_mday = atoi(&(dateTime[ 8]));
  timeinfo.tm_hour = atoi(&(dateTime[11]));
  timeinfo.tm_min  = atoi(&(dateTime[14]));
  timeinfo.tm_sec  = atoi(&(dateTime[17]));

  return mktime(&timeinfo);
}

string CODBC::ContactToString(u_int32_t uAddr, u_int16_t uPort, u_int8_t uProtocol) 
{
  char buf[32];
  memset(buf, 0, sizeof(buf));

  sprintf(buf, "%d.%d.%d.%d:%d/%d",
    (u_int8_t)(uAddr >> 24),
    (u_int8_t)(uAddr >> 16),
    (u_int8_t)(uAddr >> 8),
    (u_int8_t)uAddr,
    uPort,
    uProtocol);

  return buf;
}

void CODBC::StringToContact(const string& sContact, u_int32_t& uAddr, u_int16_t& uPort, u_int8_t& uProtocol) 
{
  int arr_int[4];
  memset(arr_int, 0, sizeof(arr_int));

  int nPort = 0;
  int nProtocol = 0;

  // unsafe, easy implementation
  sscanf(sContact.c_str(), "%d.%d.%d.%d:%d/%d",
      &arr_int[0],
      &arr_int[1],
      &arr_int[2],
      &arr_int[3],
      &nPort,
      &nProtocol);

  uPort = (u_int16_t)nPort;
  uProtocol = (u_int8_t)nProtocol;

  u_int8_t arr_uAddr[4];
  for (int i = 0; i < 4; i++)
  {
    arr_uAddr[i] = (u_int8_t)arr_int[i];
  }

  memcpy((void*)&uAddr, arr_uAddr, sizeof(uAddr));
  uAddr = ntohl(uAddr);
}


int CODBC::GetStateServerExpiryTime() const
{
  return STATE_SERVER_EXPIRY_TIME;
}

int CODBC::GetEdgeServerExpiryTime() const
{
  return EDGE_SERVER_EXPIRY_TIME;
}

string CODBC::escape(const string& sIn)
{
  int iLen = sIn.size();
  string sOut;
  sOut.resize(iLen << 1);
  const char* szIn  = sIn.data();
  char* szOut = (char*)sOut.data();

  int iOutIndex = 0;
  for (int i = 0; i < iLen; i++) {
    const char& ch = szIn[i];
    switch (ch) {
    case '\0':
      szOut[iOutIndex++] = '\\';
      szOut[iOutIndex++] = '0';
      break;
    case '\n':
      szOut[iOutIndex++] = '\\';
      szOut[iOutIndex++] = 'n';
      break;
    case '\r':
      szOut[iOutIndex++] = '\\';
      szOut[iOutIndex++] = 'r';
      break;
    case '\\':
      szOut[iOutIndex++] = '\\';
      szOut[iOutIndex++] = '\\';
      break;
    case '\"':
      szOut[iOutIndex++] = '\\';
      szOut[iOutIndex++] = '\"';
      break;
    case '\'':
      szOut[iOutIndex++] = '\\';
      szOut[iOutIndex++] = '\'';
      break;
    case '\032':
      szOut[iOutIndex++] = '\\';
      szOut[iOutIndex++] = 'Z';
      break;
    default:
      szOut[iOutIndex++] = ch;
    }
  }

  sOut.resize(iOutIndex);

  return sOut;
}

int CODBC::Open( const std::string& p_rsDatabaseName,
				  const std::vector<std::string>& sDatabaseHosts,
				  const std::string& p_rsDatabaseUser,
				  const std::string& p_rsDatabasePassword,
				  const unsigned int& p_ruiDatabasePort,
				  char* szDBVersion, char* szDBCompatible, int nCompatibleNum )
{
        m_sDatabaseName = p_rsDatabaseName;
        m_sDatabaseHosts = sDatabaseHosts;
        m_sDatabaseUser = p_rsDatabaseUser;
        m_sDatabasePassword = p_rsDatabasePassword;
        m_uiDatabasePort = p_ruiDatabasePort;
        m_sDBHost = "";
        return Open();
}


bool CODBC::Open()
{
    //printf("++++++++++++++++++++++++++++++++++++++\n");
	m_sDBHost = "";
	return (m_bOpen = _reconnect());
}



bool CODBC::_reconnect()
{
    unsigned int iIndex = 0;
	bool found = false;
	SQLRETURN ret;

	if( _isConnected() )
	{
        //printf("=========================> returning true is connected\n");
		return true;
	}

        pthread_mutex_lock(&dbLock);
	if (m_dbc) {
	  SQLDisconnect(m_dbc);
	  SQLFreeHandle(SQL_HANDLE_DBC, m_dbc);
	}
	if (m_env)
	  SQLFreeHandle(SQL_HANDLE_ENV, m_env);
        m_dbc = m_env = NULL;
        pthread_mutex_unlock(&dbLock);
		
	if( m_sDatabaseHosts.size() > 1 ) {
		// randomize host list
		next_permutation(m_sDatabaseHosts.begin(), m_sDatabaseHosts.end());
	}


	while ((iIndex < m_sDatabaseHosts.size()) && (found == false))	{

		char szConnect[1024];

		// database name is mandatory for ODBC, it is the DataSource
		// name from identifying the data base to connect to in
		//  /etc/odbc.ini

		sprintf( szConnect, "DSN=%s;", m_sDatabaseHosts[iIndex].c_str() );
		sprintf( szConnect, "%sUID=%s;", szConnect, m_sDatabaseUser.c_str() );
		sprintf( szConnect, "%sPWD=%s;", szConnect, m_sDatabasePassword.c_str() );

        LOG( "- ODBC - connecting to " << szConnect );
        LOG( "- ODBC - username " << m_sDatabaseUser << " and password " << m_sDatabasePassword);

                pthread_mutex_lock(&dbLock);
		ret = SQLAllocHandle(  SQL_HANDLE_ENV, SQL_NULL_HANDLE, &m_env );
                pthread_mutex_unlock(&dbLock);
		if( !SQL_SUCCEEDED( ret ) )
				extract_error( "SQLAllocHandle()", m_env, SQL_HANDLE_ENV );
		LOG( "- ODBC: after setting m_env");
                pthread_mutex_lock(&dbLock);
		ret = SQLSetEnvAttr(m_env, SQL_ATTR_ODBC_VERSION, (void *) SQL_OV_ODBC3, 0 );
                pthread_mutex_unlock(&dbLock);
		if( !SQL_SUCCEEDED( ret ) )
				extract_error( "SQLAllocHandle()", m_env, SQL_HANDLE_ENV );
		
		LOG( "- ODBC: after setting env attr ocbc version");
                pthread_mutex_lock(&dbLock);
		ret = SQLAllocHandle( SQL_HANDLE_DBC, m_env, &m_dbc );
                pthread_mutex_unlock(&dbLock);
		if( !SQL_SUCCEEDED( ret ) )
				extract_error( "SQLAllocHandle()", m_env, SQL_HANDLE_ENV );
			
		LOG( "- ODBC: after allocating env handle, m_dbc ");
		SQLCHAR outstr[1024];
		SQLSMALLINT outstrlen;
                pthread_mutex_lock(&dbLock);
		ret = SQLDriverConnect( m_dbc, (void*)1, (SQLCHAR*)szConnect,
												  SQL_NTS, outstr, sizeof(outstr), &outstrlen,
												  SQL_DRIVER_NOPROMPT );
                pthread_mutex_unlock(&dbLock);
                LOG( "- ODBC: after driver connect ");
		if( SQL_SUCCEEDED( ret )) {
			LOG( "Connected to "  << outstr );

			if( ret == SQL_SUCCESS_WITH_INFO) {
					printf( "Driver reported the following diagnostics\n" );
					extract_error( "SQLDriverConnect", m_dbc, SQL_HANDLE_DBC );
			}

			if( CheckDBVersion( DB_VERSION, DB_COMPATIBLE, DB_COMPATIBLE_NUM )) {
					found = true;
					break;
			}
		} else {

			LOG( "Failed to connect to database" );
			extract_error( "SQLDriverConnect", m_dbc, SQL_HANDLE_DBC );
                        pthread_mutex_lock(&dbLock);
                        SQLDisconnect(m_dbc);
			SQLFreeHandle(SQL_HANDLE_DBC, m_dbc);
			SQLFreeHandle(SQL_HANDLE_ENV, m_env);
			m_dbc = m_env = NULL;
                        pthread_mutex_unlock(&dbLock);
		}

		iIndex++;
	}

	if( found==true ) {
		m_sDBHost = m_sDatabaseHosts[iIndex];
		LOGL(4, "connected to database (" << m_sDBHost << ")");
	}
	else {
		DB_AND_LOG("connection to database cannot be established");
	}

	return found;
}

bool CODBC::_isConnected() { 
	
	if(( m_nLastError == ACCESS_DENIED )||
	   ( m_nLastError == CONNECTION_LOST )) {
		// clean up handles
                pthread_mutex_lock(&dbLock);
                SQLDisconnect(m_dbc);
		SQLFreeHandle(SQL_HANDLE_DBC, m_dbc);
		SQLFreeHandle(SQL_HANDLE_ENV, m_env);
		m_dbc = m_env = NULL;
                pthread_mutex_unlock(&dbLock);
		return (m_bOpen=false);
	}
	return m_bOpen;
}

bool CODBC::CheckDBVersion( char* szVersionName, char* szCompatibleName,
							int nCompatibleNumber ) {

	string sVersionName(szVersionName);
	string sVersionValue;
	string sCompatibleName(szCompatibleName);
	string sCompatibleValue;

	if( !GetConfigValue( sVersionName, sVersionValue )) {
		LOG( "unable to get config value - " << sVersionName );
		return false;
	}

	if( !GetConfigValue( sCompatibleName, sCompatibleValue )) {
		LOG( "unable to get config value - " << sCompatibleName );
		return false;
	}

	if(( nCompatibleNumber < atoi( sCompatibleValue.c_str() )) ||
	  ( nCompatibleNumber > atoi( sVersionValue.c_str() ))) {
		LOG( "FATAL: database version is not compaitle with this server - current version:"
			<< sVersionValue << "  compatible to:" << sCompatibleValue << "  server requires:"
			<< nCompatibleNumber );
		return false;
	}

	LOG( "Current DB version:" << sVersionValue << " compatible to:" << sCompatibleValue
		<< " server using:" << nCompatibleNumber );
	return true;
}


