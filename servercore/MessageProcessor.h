/************************************************************/
/*                                                          */
/*                                                          */
/*                                                          */
/*                                                          */
/* message processor interface class                        */
/*    provides methods for message parsing and processing   */
/* author: larsb                                            */
/*                                                          */
/************************************************************/

#ifndef _MSGPROCESSOR_H
#define _MSGPROCESSOR_H



#include <string>


#define MSG_CONVERT_TLS_BEFORE      5 // returned by getResponse if connection should be converted to TLS before sending message
#define MSG_CONVERT_TLS_AFTER       4 // returned by getResponse if connection should be converted to TLS after sending message
#define MSG_DELPROCESSOR            3 // returned by OnServiceNotify if processor can be deleted
#define MSG_CLOSE                   2 // returned by getResponse if connection should be closed (server initiated)
#define MSG_RESPONSE_AVAIL          1 // returned by processMessage if a response was generated


#define MSG_OK                      0
#define MSG_UNKNOWN                -1

#define MSG_INTERNAL               -2
#define MSG_MEMORY_ERROR           -3
#define MSG_INVALID                -4
#define MSG_FINAL_ERROR            -5
#define MSG_NOT_IMPLEMENTED        -6
#define MSG_ERROR_MAX              -7


using namespace std;


class IResponse;

class IMessageProcessor
{


 public:

  virtual ~IMessageProcessor() {};

  //! creates a new instance of the actual processor type (without copying any status information), required
  //! when instantiating new processors in a Connection object
  virtual IMessageProcessor* createNewInstance() = 0;

  //! send a welcome message, called by connection object upon startup
  virtual int sendWelcome(string &welcome) {return MSG_INVALID;}

  //! does not contain validity checks, only checks for length and 'reasonable' content
  virtual int isCompleteMessage(char *pBuf, int iBufLen) = 0;   

  //! process a complete message; provide IP and port of message origin
  virtual int processMessage(char *pBuf, int iBufLen, const string& IP, int port, int proto, const string& recvdIP, int recvdPort, int recvdProto) = 0;

  //! return the response to the previously parsed message
  virtual int getResponse(char *&pBuf, int &iBufLen, string &IP, int &port, int &protocol) = 0;

  //! return the processor type
  virtual int getProcessorType() = 0;

  //Added for multi-threaded server

  //! multi-thread enabled
  virtual bool isMT() {return false;}

  //! Get timeout in seconds
  //! Base implementation returns 0, which means to use the default
  virtual int getTimeout() {return 0;}

  //! Indicates connection lost
  virtual int removeBinding(const string& sAddress, int iPort, int iProtocol) {return 0;};

};





#endif

