// XMPPResourceListMessage.h
// Interface for the XMPPResourceListMessage class

#ifndef XMPP_RESOURCE_LIST_MESSAGE_H
#define XMPP_RESOURCE_LIST_MESSAGE_H

#include "QueryMessage.h"

#include <vector>
using std::vector;

class XMPPResourceListMessage : public QueryMessage
{
public:
  XMPPResourceListMessage();
  virtual ~XMPPResourceListMessage();

  virtual int deserialize(const string& buf);
  virtual void serialize(string& buf) const;

  virtual u_int16_t payload_length(void) const;

#ifndef _WIN32_WCE
        friend ostream& operator<<(ostream& s, const XMPPResourceListMessage& v);
#endif

public:
  string m_sName;
  vector<string>   m_ResourceList;
  vector<string>   m_ResourceStateList;
  vector<u_int8_t> m_ResourceRequestedRosterList;

};

#endif//XMPP_RESOURCE_LIST_MESSAGE_H
