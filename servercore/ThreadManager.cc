// ThreadManager.cc
// Implementation for the CThreadManager class

#include "ThreadManager.h" 
#include "MessageProxy.h"
#include "NetworkManager.h"
#include "ConnectionHashTable.h"
#include "processor_defines.h"
#include <arpa/inet.h>
#include "Socket.h" //UDP
#include "CoreConfigFile.h"

#include "debug.h"
#include "log.h"

#ifndef MAX_WORKER_THREADS
#define MAX_WORKER_THREADS 100
#endif

int MAX_WAITING_MESSAGES ;
int MANY_WAITING_MESSAGES ;

//Constructor
CThreadManager::CThreadManager(const CMessageProxy* pMessageProxy, int iNumThreads)
: m_pNetworkManager(NULL),
  m_pMessageProxy(pMessageProxy),
  m_bEnable(false),
  m_pConnectionHashTable(NULL)
{
	DB_ENTER(CThreadManager::CThreadManager);

    MANY_WAITING_MESSAGES = g_pCoreConf->m_iMessageQueueSize;
    MAX_WAITING_MESSAGES = MANY_WAITING_MESSAGES*3/2;

	if (iNumThreads >= 1) {
		if (iNumThreads <= MAX_WORKER_THREADS) {
			m_iNumThreads = iNumThreads;
		}
		else {
			m_iNumThreads = MAX_WORKER_THREADS;
		}
	}
	else {
		m_iNumThreads = 1;
	}

	DB("Set Up Worker Threads");

	m_pWorkerThreads = new ThreadInfoNode[m_iNumThreads];

	for (int i = 0; i < m_iNumThreads; i++) {
		ThreadInfo* pInfo = &(m_pWorkerThreads[i].info);
		pInfo->pThreadManager = this;
		pInfo->id = i;
		pInfo->thread = (pthread_t)-1;
		pthread_cond_init(&(pInfo->cond), NULL);
		pthread_mutex_init(&(pInfo->mutex), NULL);
		pInfo->message.iMessageType = IDLE;

		if (m_pMessageProxy != NULL) {
			pInfo->processor = m_pMessageProxy->createNewInstance();
		}
		else {
			pInfo->processor = new CMessageProxy;
		}

		pInfo->processor->SetThreadManager(this);
		
		ThreadInfoNode* pNode = &(m_pWorkerThreads[i]);
		pNode->iPrevIdleThread = i - 1;
		pNode->iNextIdleThread = i + 1;
		if (pNode->iNextIdleThread == m_iNumThreads) {
			pNode->iNextIdleThread = -1;
		}

		pNode->iPrevActiveThread = -1;
		pNode->iNextActiveThread = -1;
	}

	m_iFirstIdleThread = 0;
	m_iFirstActiveThread = -1;

	DB("Initialize Thread Manager mutexes");

	pthread_mutex_init(&m_ThreadListMutex, NULL);
	pthread_mutex_init(&m_MessageQueueMutex, NULL);
	pthread_cond_init(&m_StopCond, NULL);

        m_pConnectionHashTable = new CConnectionHashTable(m_iNumThreads);
}
	
//Destructor
CThreadManager::~CThreadManager()
{
	DB_ENTER(CThreadManager::~CThreadManager);

	Stop();	

	//Delete worker threads
	for (int i = 0; i < m_iNumThreads; i++) {
		ThreadInfo* pInfo = &(m_pWorkerThreads[i].info);
		pthread_cond_destroy(&(pInfo->cond));
		pthread_mutex_destroy(&(pInfo->mutex));

		delete pInfo->processor;
	}

	delete [] m_pWorkerThreads;	

	pthread_mutex_destroy(&m_ThreadListMutex);
	pthread_mutex_destroy(&m_MessageQueueMutex);
	pthread_cond_destroy(&m_StopCond);

        delete m_pConnectionHashTable;
}

//Set Network Manager
void CThreadManager::SetNetworkManager(CNetworkManager* pNetworkManager)
{
	DB_ENTER(CThreadManager::SetNetworkManager);

	m_pNetworkManager = pNetworkManager;
}

//Start
void CThreadManager::Start()
{
	DB_ENTER(CThreadManager::Start);

	DB("Enable Thread Manager");
    m_bEnable = true;

    for (int i2 = 0; i2 < m_iNumThreads; i2++) {
        pthread_create(&(m_pWorkerThreads[i2].info.thread), NULL, WorkerThreadFunction, &(m_pWorkerThreads[i2].info));
    }
}

//Stop
void CThreadManager::Stop()
{
	DB_ENTER(CThreadManager::Stop);

	m_bEnable = false;	

	//Wait until no more messages in queue
	pthread_mutex_lock(&m_MessageQueueMutex);

	if (m_MessageQueue.size() > 0) {
		pthread_cond_wait(&m_StopCond, &m_MessageQueueMutex);
	}

	pthread_mutex_unlock(&m_MessageQueueMutex);

	//Stop worker threads
	for (int i = 0; i < m_iNumThreads; i++) {
		ThreadInfo* pInfo = &(m_pWorkerThreads[i].info);
		pthread_t threadId = pInfo->thread;
		pInfo->thread = (pthread_t) -1;

		if (threadId != (pthread_t)-1) {
			pthread_mutex_lock(&(pInfo->mutex));
			pthread_mutex_unlock(&(pInfo->mutex));

			pthread_cond_signal(&(pInfo->cond));

			void* ptr;
			pthread_join(threadId, &ptr);
		}
	}

	DB("CThreadManager::Stop finished");
}


//! Is Message Queue Full
bool CThreadManager::IsMessageQueueFull()
{
	DB_ENTER(CThreadManager::IsMessageQueueFull);

	pthread_mutex_lock(&m_MessageQueueMutex);
	int iQueueSize = m_MessageQueue.size();
	pthread_mutex_unlock(&m_MessageQueueMutex);

	return (iQueueSize >= MAX_WAITING_MESSAGES);
}

//! Is Message Queue Full or Almost Full
bool CThreadManager::IsMessageQueueFullOrAlmostFull()
{
	DB_ENTER(CThreadManager::IsMessageQueueFull);

	pthread_mutex_lock(&m_MessageQueueMutex);
	int iQueueSize = m_MessageQueue.size();
	pthread_mutex_unlock(&m_MessageQueueMutex);

	return (iQueueSize >= MANY_WAITING_MESSAGES);
}


int CThreadManager::MessageQueueWaiting()
{

        pthread_mutex_lock(&m_MessageQueueMutex);
        int iQueueSize = m_MessageQueue.size();
        pthread_mutex_unlock(&m_MessageQueueMutex);

       return iQueueSize ;

}

//To be called by network thread
//Network thread notifies thread manager that a message is available
//Message is queued for processing or handled as a response message
void CThreadManager::AddNewMessage(const string& sToAddress, int iToPort,
const string& sFromAddress, int iFromPort, int iProtocol,
int iMessageType, char* pcMessage, int iLen)
{
	DB_ENTER(CThreadManager::AddNewMessage);

	if (!m_bEnable) return;

	MessageInfo mi;
	mi.sToAddress = sToAddress;
	mi.iToPort = iToPort,
	mi.sFromAddress = sFromAddress;
	mi.iFromPort = iFromPort;
	mi.iProtocol = iProtocol;
	mi.iMessageType = iMessageType;
	mi.pcMessage = pcMessage;
	mi.iLen = iLen;

        bool bQueueFull = false;

	pthread_mutex_lock(&m_MessageQueueMutex);
        if (m_bEnable && (m_MessageQueue.size() < MAX_WAITING_MESSAGES)) {
		m_MessageQueue.push_back(mi);
	}
	else {
		if (iProtocol == UDP) {
			bQueueFull = true;
		}
		else {
			m_MessageQueue.push_back(mi);
		}
	}
	DB(m_MessageQueue.size() << " messages in the queue");
	pthread_mutex_unlock(&m_MessageQueueMutex);
	
        if (bQueueFull) {
		//Discard message
		if (m_bEnable) {
			DB_AND_LOGL(5, "Too many messages waiting. Discarding message");
		}
		delete [] pcMessage;
	}
	else {
		AssignMessageToThread();
	}
}

//To be called by CMessageProxy in worker thread
//Passes message to Network Manager
//pcMessage will be deleted
void CThreadManager::SendMessage(const string& sToAddress, int iToPort, int iProtocol,
				 int iMessageType, char* pcMessage, int iLen, int iConvertToTLS)
{
	DB_ENTER(CThreadManager::SendMessage);

	if ((!m_bEnable) || (m_pNetworkManager == NULL)) {
		delete [] pcMessage;
		return;
	}

	m_pNetworkManager->SendMessage(sToAddress, iToPort, iProtocol,
				       iMessageType, pcMessage, iLen, iConvertToTLS, false, "");
}

//To be called by CMessageProxy in worker thread
//Close Connection
//Passes to Network Manager
void CThreadManager::CloseConnection(const string& sAddress, int iPort, int iProtocol, int iMessageType, bool bLater)
{
	DB_ENTER(CThreadManager::CloseConnection);

	if ((!m_bEnable) || (m_pNetworkManager == NULL)) return;

	if (bLater) {
		m_pNetworkManager->CloseConnectionLater(sAddress, iPort, iProtocol, iMessageType);
	}
	else {
		m_pNetworkManager->CloseConnection(sAddress, iPort, iProtocol, iMessageType);
	}
}



//To be called by worker thread
//Notifies network thread that a worker thread is available
void CThreadManager::MessageProcessingComplete(int iThreadId)
{
	DB_ENTER(CThreadManager::MessageProcessingComplete);

	ThreadInfoNode* pNode = &(m_pWorkerThreads[iThreadId]);
	ThreadInfo* pInfo = &(pNode->info);

	string sFromAddress;
	int iFromPort;
	int iProtocol;
	
	pthread_mutex_lock(&(pInfo->mutex));
	int iMessageType = pInfo->message.iMessageType;
	pInfo->message.iMessageType = IDLE;
	sFromAddress = pInfo->message.sFromAddress;
	iFromPort = pInfo->message.iFromPort;
	iProtocol = pInfo->message.iProtocol;

	pthread_mutex_unlock(&(pInfo->mutex));

	pthread_mutex_lock(&m_ThreadListMutex);
	
	if ((iProtocol == TCP) || (iProtocol == TLS)) {
		m_pConnectionHashTable->Put(sFromAddress, 
		  iFromPort, iProtocol, iMessageType, NULL);
	}

	//Move thread from ACTIVE to IDLE list

	if (pNode->iPrevActiveThread == -1) {
		m_iFirstActiveThread = pNode->iNextActiveThread;
	}
	else {
		m_pWorkerThreads[pNode->iPrevActiveThread].iNextActiveThread 
		= pNode->iNextActiveThread;
	}

	if (pNode->iNextActiveThread != -1) {
		m_pWorkerThreads[pNode->iNextActiveThread].iPrevActiveThread
		= pNode->iPrevActiveThread;
	}
	
	pNode->iPrevActiveThread = -1;
	pNode->iNextActiveThread = -1;

	pNode->iPrevIdleThread = -1;
	pNode->iNextIdleThread = m_iFirstIdleThread;
	
	if (m_iFirstIdleThread != -1) {
		m_pWorkerThreads[m_iFirstIdleThread].iPrevIdleThread = iThreadId;
	}

	m_iFirstIdleThread = iThreadId;

	pthread_mutex_unlock(&m_ThreadListMutex);

	AssignMessageToThread();
}

//Called by worker thread and network thread
//Called by AddNewMessage and MessageProcessingComplete
void CThreadManager::AssignMessageToThread()
{
	DB_ENTER(CThreadManager::AssignMessageToThread);

	//if (!m_bEnable) return;

	//Match idle thread to queue

	pthread_mutex_lock(&m_MessageQueueMutex);

	if (m_MessageQueue.size() > 0) {
		pthread_mutex_lock(&m_ThreadListMutex);

        int iThreadId = -1;
		MessageInfo mi;

		if (m_iFirstIdleThread != -1) {
			list<MessageInfo>::iterator iter = m_MessageQueue.begin();
            for (; iter != m_MessageQueue.end(); iter++)
            {
				iThreadId = m_iFirstIdleThread;

                if ((iter->iProtocol == TCP) || (iter->iProtocol == TLS))
                {
                    long iActiveThreadId = (long)m_pConnectionHashTable->Get(iter->sFromAddress,iter->iFromPort, iter->iProtocol, iter->iMessageType) - 1;
                    if (iActiveThreadId != -1) {
						iThreadId = -1;
					}
					else {

                        /*while (iThreadId == iActiveThreadId) {
							iThreadId = m_pWorkerThreads[iThreadId].iNextIdleThread;
						}

						if (iThreadId != -1) {
							mi = *iter;
							m_MessageQueue.erase(iter);
							break;
                        }*/
                        mi = *iter;
                        m_MessageQueue.erase(iter);
                        break;
					}
				}
                else
                {
                    mi = *iter;
					m_MessageQueue.erase(iter);
					break;
				}
			}
		}

		if (iThreadId != -1) {
			//Assign message to first Idle thread
			DB("Assign to thread #" << (iThreadId + 1));

			ThreadInfoNode* pNode = &(m_pWorkerThreads[iThreadId]);
			ThreadInfo* pInfo = &(pNode->info);

			DB("Found worker thread");

			pthread_mutex_lock(&(pInfo->mutex));
			pInfo->message = mi;
			pthread_mutex_unlock(&(pInfo->mutex));

			DB("Copied message information");

			pthread_cond_signal(&(pInfo->cond));

			DB("Signaled worker thread");

			if ((mi.iProtocol == TCP) || (mi.iProtocol == TLS)) {
				m_pConnectionHashTable->Put(mi.sFromAddress, 
				  mi.iFromPort, mi.iProtocol, mi.iMessageType, (void*)(iThreadId + 1));
			}

			//Move thread from IDLE to ACTIVE list
			DB("Fix thread list");
	
			if (pNode->iPrevIdleThread == -1) {
				m_iFirstIdleThread = pNode->iNextIdleThread;
			}
			else {
				m_pWorkerThreads[pNode->iPrevIdleThread].iNextIdleThread 
				= pNode->iNextIdleThread;
			}

			if (pNode->iNextIdleThread != -1) {
				m_pWorkerThreads[pNode->iNextIdleThread].iPrevIdleThread
				= pNode->iPrevIdleThread;
			}
	
			pNode->iPrevIdleThread = -1;
			pNode->iNextIdleThread = -1;

			pNode->iPrevActiveThread = -1;
			pNode->iNextActiveThread = m_iFirstActiveThread;
	
			if (m_iFirstActiveThread != -1) {
				m_pWorkerThreads[m_iFirstActiveThread].iPrevActiveThread = iThreadId;
			}

			m_iFirstActiveThread = iThreadId;
		}
		else {
			DB("No idle threads to assign message to");
		}
		
		pthread_mutex_unlock(&m_ThreadListMutex);
	}
	else {
		DB("No messages in the queue to assign");
		if (!m_bEnable) {
			pthread_cond_signal(&m_StopCond);
		}
	}

	pthread_mutex_unlock(&m_MessageQueueMutex);
}

	
void* CThreadManager::WorkerThreadFunction(void* arg)
{
	DB_ENTER(CThreadManager::WorkerThreadFunction);

	ThreadInfo* pThreadInfo = (ThreadInfo*)arg;

	//Infinite loop
	for(;;) {
		pthread_mutex_lock(&(pThreadInfo->mutex));

		if (pThreadInfo->message.iMessageType == IDLE) {
			if (pThreadInfo->pThreadManager->m_bEnable) {
				pthread_cond_wait(&(pThreadInfo->cond), &(pThreadInfo->mutex));
			}
		}

		DB("Worker Thread received message");

		MessageInfo mi = pThreadInfo->message;

		pthread_mutex_unlock(&(pThreadInfo->mutex));
			
		if (mi.iMessageType == IDLE) break; 

        //LOG("++++++++++++++++++++++++++++++++++++++++ worker thread");
		//Process task with taskarg
		pThreadInfo->processor->ProcessMessage( 
			mi.sToAddress, mi.iToPort, 
			mi.sFromAddress, mi.iFromPort, mi.iProtocol, 
			mi.iMessageType, mi.pcMessage, mi.iLen);

		//Notify thread manager that the task is done
		pThreadInfo->pThreadManager->MessageProcessingComplete(pThreadInfo->id);
	}

	DB("Worker Thread finished");

	return NULL;
}


