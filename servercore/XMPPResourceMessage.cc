// file: XMPPResourceMessage.cc

#include "XMPPResourceMessage.h"
#include "debug.h"
#include <arpa/inet.h>
#include "Socket.h"
#include "string_utils.h"

CIntMT XMPPResourceMessage::constCount;
CIntMT XMPPResourceMessage::destCount;
XMPPResourceMessage::XMPPResourceMessage()
:
  QueryMessage(EYEBALL_SERVER_XMPP_RESOURCE),
  m_sState("none"),
  m_u32ContactAddress(0),
  m_u16ContactPort(0),
  m_u8ContactProtocol(0),
  m_u32ForwardAddress(0),
  m_u16ForwardPort(0),
  m_u8ForwardProtocol(0),
  m_u8RequestedRoster(0),
  m_i8Priority(0),
  m_u32LoginTime(0)
{
  DB_ENTER(XMPPResourceMessage::XMPPResourceMessage);
  constCount+=1;
}

XMPPResourceMessage::~XMPPResourceMessage()
{
  DB_ENTER(XMPPResourceMessage::~XMPPResourceMessage);
  destCount+=1;
}

void XMPPResourceMessage::serialize(string& buf) const
{
  DB_ENTER(XMPPResourceMessage::serialize);

  QueryMessage::serialize(buf);

  // serialize message contents
  SerializeString16(m_sName, buf);
  SerializeString16(m_sResource, buf);
  SerializeString16(m_sState, buf);

  Serialize32(m_u32ContactAddress, buf);
  Serialize16(m_u16ContactPort, buf);
  Serialize8(m_u8ContactProtocol, buf);

  Serialize32(m_u32ForwardAddress, buf);
  Serialize16(m_u16ForwardPort, buf);
  Serialize8(m_u8ForwardProtocol, buf);

  Serialize8(m_u8RequestedRoster, buf);

  u_int8_t u8Priority = (u_int8_t)m_i8Priority; 
  Serialize8(u8Priority, buf);
  SerializeString16(m_sLastPresence, buf);

  Serialize32(m_u32LoginTime, buf);
}

int XMPPResourceMessage::deserialize(const string& buf)
{
  DB_ENTER(XMPPResourceMessage::deserialize);

  int iOffset = 0;

  iOffset += QueryMessage::deserialize(buf);

  // deserialize message contents
  if (!DeserializeString16(m_sName, buf, iOffset)) return -1;
  if (!DeserializeString16(m_sResource, buf, iOffset)) return -1;
  if (!DeserializeString16(m_sState, buf, iOffset)) return -1;

  if (!Deserialize32(m_u32ContactAddress, buf, iOffset)) return -1;
  if (!Deserialize16(m_u16ContactPort, buf, iOffset)) return -1;
  if (!Deserialize8(m_u8ContactProtocol, buf, iOffset)) return -1;

  if (!Deserialize32(m_u32ForwardAddress, buf, iOffset)) return -1;
  if (!Deserialize16(m_u16ForwardPort, buf, iOffset)) return -1;
  if (!Deserialize8(m_u8ForwardProtocol, buf, iOffset)) return -1;

  if (!Deserialize8(m_u8RequestedRoster, buf, iOffset)) return -1;

  u_int8_t u8Priority; 
  if (!Deserialize8(u8Priority, buf, iOffset)) return -1;
  m_i8Priority = (int8_t)u8Priority;

  if (!DeserializeString16(m_sLastPresence, buf, iOffset)) return -1;

  if (!Deserialize32(m_u32LoginTime, buf, iOffset)) return -1;

  return iOffset;
}

u_int16_t XMPPResourceMessage::payload_length() const
{
  DB_ENTER(XMPPResourceMessage::payload_length);

  return QueryMessage::payload_length() + 
	m_sName.size() + sizeof(u_int16_t) +
	m_sResource.size() + sizeof(u_int16_t) +
	m_sState.size() + sizeof(u_int16_t) +
	sizeof(m_u32ContactAddress) + 
	sizeof(m_u16ContactPort) +
	sizeof(m_u8ContactProtocol) +
	sizeof(m_u32ForwardAddress) +
	sizeof(m_u16ForwardPort) +
	sizeof(m_u8ForwardProtocol) +
	sizeof(m_u8RequestedRoster) +
	sizeof(m_i8Priority) +
        m_sLastPresence.size() + sizeof(u_int16_t) +
	sizeof(m_u32LoginTime);
}

void XMPPResourceMessage::setContactAddress(const string& sAddress)
{
  DB_ENTER(XMPPResourceMessage::setContactAddress);

  struct in_addr addr;
  memset(&addr, 0, sizeof(addr));
  inet_pton(AF_INET, sAddress.c_str(), &addr);
  m_u32ContactAddress = ntohl(addr.s_addr);
}

string& XMPPResourceMessage::getContactAddress(string& sAddress) const
{
  DB_ENTER(XMPPResourceMessage::getContactAddress);

  struct in_addr addr;
  memset(&addr, 0, sizeof(addr));
  addr.s_addr = htonl(m_u32ContactAddress);

  char dst[INET_ADDRSTRLEN];
  if (inet_ntop(AF_INET, &addr, dst, INET_ADDRSTRLEN) != NULL) {
    sAddress = (string)dst;
  }
  else {
    sAddress = (string)"";
  }
  return sAddress;
}

void XMPPResourceMessage::setForwardAddress(const string& sAddress)
{
  DB_ENTER(XMPPResourceMessage::setForwardAddress);

  struct in_addr addr;
  memset(&addr, 0, sizeof(addr));
  inet_pton(AF_INET, sAddress.c_str(), &addr);
  m_u32ForwardAddress = ntohl(addr.s_addr);
}

string& XMPPResourceMessage::getForwardAddress(string& sAddress) const
{
  DB_ENTER(XMPPResourceMessage::getForwardAddress);

  struct in_addr addr;
  memset(&addr, 0, sizeof(addr));
  addr.s_addr = htonl(m_u32ForwardAddress);

  char dst[INET_ADDRSTRLEN];
  if (inet_ntop(AF_INET, &addr, dst, INET_ADDRSTRLEN) != NULL) {
    sAddress = (string)dst;
  }
  else {
    sAddress = (string)"";
  }
  return sAddress;
}

ostream& operator<<(ostream& s, const XMPPResourceMessage& v)
{
        string sContactProtocol;
        if (v.m_u8ContactProtocol == UDP) {
                sContactProtocol = "UDP";
        }
        else if (v.m_u8ContactProtocol == TCP) {
                sContactProtocol = "TCP";
        }
        else if (v.m_u8ContactProtocol == TLS) {
                sContactProtocol = "TLS";
        }
        else {
                sContactProtocol = "Unknown: ";
                sContactProtocol += itos(v.m_u8ContactProtocol);
        }

        string sForwardProtocol;
        if (v.m_u8ForwardProtocol == UDP) {
                sForwardProtocol = "UDP";
        }
        else if (v.m_u8ForwardProtocol == TCP) {
                sForwardProtocol = "TCP";
        }
        else if (v.m_u8ForwardProtocol == TLS) {
                sForwardProtocol = "TLS";
        }
        else {
                sForwardProtocol = "Unknown: ";
                sForwardProtocol += itos(v.m_u8ForwardProtocol);
        }

        string sContactAddress;
        v.getContactAddress(sContactAddress);

        string sForwardAddress;
        v.getForwardAddress(sForwardAddress);

        s << (QueryMessage)v
                << " " << v.m_sName << " " << v.m_sResource
                << " " << v.m_sState << " " << sContactAddress
                << ":" << v.m_u16ContactPort << "/" << sContactProtocol.c_str()
                << " " << sForwardAddress
                << ":" << v.m_u16ForwardPort << "/" << sForwardProtocol
                << " " << v.m_u8RequestedRoster << " " << v.m_i8Priority
		<< " " << v.m_sLastPresence << " " << v.m_u32LoginTime;

        return s;
}

