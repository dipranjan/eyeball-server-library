


/*
  $Id: Socket.cc,v 1.37 2007/12/14 22:06:38 lennyp Exp $
*/

#include <simple_crypt.h>

#include <log.h>
#include <debug.h>

#include "string_utils.h"

#include "Socket.h"



using namespace ossl;


MsgBuffer::MsgBuffer()
{
    m_pBuffer = NULL;
    m_iBufLen = 0;
}// MsgBuffer::MsgBuffer()


MsgBuffer::MsgBuffer(const MsgBuffer &buf)
{
    if (this != &buf)
        *this = buf;

}// MsgBuffer::MsgBuffer(const MsgBuffer &buf)



MsgBuffer& MsgBuffer::operator=(const MsgBuffer &buf)
{
    if (this!=&buf)
    {
        m_DestIP   = buf.m_DestIP;
        m_DestPort = buf.m_DestPort;
        m_pBuffer  = buf.m_pBuffer;
        m_iBufLen  = buf.m_iBufLen;
    }

    return *this;

}// MsgBuffer& MsgBuffer::operator=(const MsgBuffer &buf)




static string getSocketErrorString(int errcode)
{
    if (errno==EPROTONOSUPPORT)
        return "EPROTONOSUPPORT";
    else if (errno==EAFNOSUPPORT)
        return "EAFNOSUPPORT";
    else if (errno==EACCES)
        return "EACCES";
    else if (errno==EFAULT)
        return "EFAULT";
    else if (errno==ENOTSOCK)
        return "ENOTSOCK";
    else if (errno==EISCONN)
        return "EISCONN";
    else if (errno==ECONNREFUSED)
        return "ECONNREFUSED";
    else if (errno==ETIMEDOUT)
        return "ETIMEDOUT";
    else if (errno==ENETUNREACH)
        return "ENETUNREACH";
    else if (errno==EADDRINUSE)
        return "EADDRINUSE";
    else if (errno==EINPROGRESS)
        return "EINPROGRESS";
    else if (errno==EALREADY)
        return "EALREADY";
    else if (errno==EAGAIN)
        return "EAGAIN";
    else if (errno==EBADF)
        return "EAGAIN";
    else if (errno==EWOULDBLOCK)
        return "EWOULDBLOCK";
    else if (errno==ENOBUFS)
        return "ENOBUFS";
    else if (errno==EINTR)
        return "EINTR";
    else if (errno==ENOMEM)
        return "ENOMEM";
    else if (errno==EINVAL)
        return "EINVAL";
    else if (errno==EPIPE)
        return "EPIPE";
    else if (errno==ENOMEM)
        return "ENOMEM";
    else if (errno==ENOMEM)
        return "ENOMEM";
    else
        return (string)"unknown error "+itos(errcode);

}// static string getSocketErrorString(int errcode)

CIntMT ISocket::m_iGlobalSendBufferUsage;
const int ISocket::m_iGlobalSendBufferMaxSize = 10 * 1024 * 1024;


inline int ISocket::_appendBuffer(const char *pBuffer, int iSize, const string& sIP, int iPort)
{
    int result;

    int iSpaceRemainingInMsgBuffer = m_iSendBufferSize - m_iMsgBufferSize;

    //LOG("=>> _appendBuffer: m_iSendBufferSize:"<<m_iSendBufferSize<<" m_iMsgBufferSize:"<<m_iMsgBufferSize<<" iSpaceRemainingInMsgBuffer:"<<iSpaceRemainingInMsgBuffer<<" need to save:"<<iSize);

    if (iSize<=iSpaceRemainingInMsgBuffer)
    {
        MsgBuffer msg;

        msg.m_DestIP   = sIP;
        msg.m_DestPort = iPort;

        msg.m_pBuffer = new char[iSize];
        memcpy(msg.m_pBuffer,pBuffer,iSize);
        msg.m_iBufLen = iSize;
        m_iMsgBufferSize += iSize;

        if (iSize > 0) {
            m_MsgBufferList.push_back(msg);
        }

        result = SOCK_WOULDBLOCK;
    }
    else if (((int)m_iGlobalSendBufferUsage+iSize)<m_iGlobalSendBufferMaxSize)
    {
        MsgBuffer msg;

        msg.m_DestIP   = sIP;
        msg.m_DestPort = iPort;

        msg.m_pBuffer = new char[iSize];
        memcpy(msg.m_pBuffer,pBuffer,iSize);
        msg.m_iBufLen = iSize;
        m_iMsgBufferSize += iSize;
        if (iSpaceRemainingInMsgBuffer > 0) {
            m_iGlobalSendBufferUsage += (iSize - iSpaceRemainingInMsgBuffer);
        }
        else {
            m_iGlobalSendBufferUsage += iSize;
        }

        if (iSize > 0) {
            m_MsgBufferList.push_back(msg);
        }

        result = SOCK_WOULDBLOCK;
    }
    else
    {
        //LOG("=>>ISocket::_appendBuffer::  SOCK_ERR");
        result = SOCK_ERR;
    }
    //LOG("=>> buffer size: "<<m_iMsgBufferSize<<" returning _appendBuffer:"<<result);
    return result;

}// inline int ISocket::_appendBuffer(char *pBuffer, int iSize, const string& sIP, int iPort)

inline void ISocket::_ReduceMsgBufferSize(int iSize)
{
    int iMsgBufferOverflow = m_iMsgBufferSize - m_iSendBufferSize;

    m_iMsgBufferSize -= iSize;
    if (iMsgBufferOverflow > 0) {
        if (iMsgBufferOverflow > iSize) {
            m_iGlobalSendBufferUsage -= iSize;
        }
        else {
            m_iGlobalSendBufferUsage -= iMsgBufferOverflow;
        }
    }
}

//Default 10KB
int ISocket::m_iSendBufferSize = 10*1024;

//Set Send Buffer Size. Default 10 KB
void ISocket::SetSendBufferSize(int iSize)
{
    if (iSize > 0) {
        m_iSendBufferSize = iSize;
    }
    else {
        //Error
        DB("Cannot set Send Buffer size to " << iSize);
        LOG("Cannot set Send Buffer size to " << iSize);
    }
} // void ISocket::SetSendBufferSize(int iSize);






UDPSocket::UDPSocket()
{
    m_socketfd = -1;

    m_iMsgBufferSize = 0;
    m_MsgBufferList.clear();

}// UDPSocket::UDPSocket()





UDPSocket::~UDPSocket()
{
    close();
}// UDPSocket::~UDPSocket()




int UDPSocket::createSocket(int type, const string& addr, int port)
{
    int on, flags;

    m_socketfd = socket(AF_INET, SOCK_DGRAM, 0);

    if(m_socketfd <0)
        return -1;

    // reuse address
    on = 1;
    if(setsockopt(m_socketfd, SOL_SOCKET, SO_REUSEADDR, (char *) &on, sizeof(on)) != 0)
        return -2;

    // bind
    bzero((char *) &m_Addr, sizeof(m_Addr));
    m_Addr.sin_family = AF_INET;
    if(addr!=(string)"any")
    {
        m_Addr.sin_addr.s_addr = inet_addr(addr.c_str());
        m_LocalAddr = addr;
        m_LocalPort = port;
    }
    else
    {
        m_Addr.sin_addr.s_addr = htonl(INADDR_ANY);
        m_LocalAddr = "any";
        m_LocalPort = 0;
    }

    m_Addr.sin_port = htons(port);
    if ( bind(m_socketfd, (struct sockaddr*) &m_Addr, sizeof (m_Addr)) <0) {
        DB_AND_LOGL(3, "bind to " << addr << ":" << port << " failed when creating UDP socket");
        return -3;
    }

    struct linger l;

    l.l_onoff = 1;
    l.l_linger = 0;
    if(setsockopt(m_socketfd, SOL_SOCKET, SO_LINGER, &l, sizeof(struct linger)) != 0)
        return -3;

    // set non-blocking
    flags = fcntl(m_socketfd, F_GETFL, 0);
    flags |= O_NONBLOCK | O_NDELAY;

    if (fcntl(m_socketfd, F_SETFL, flags) == -1)
        return -4;

    DB("finished creating UDPSocket");

    return SOCK_OK;

}// int UDPSocket::createSocket(int type, const string& addr, int port)




int UDPSocket::sendTo(const char *pBuffer, int iBufferSize, const string& addr, int port)
{
    struct sockaddr_in toaddr;
    struct sockaddr *pToaddr;
    socklen_t tolen;
    int sent;

    DB_ENTER(UDPSocket::sendTo);

    if (m_socketfd == -1) {
        return SOCK_ERR;
    }

    memset(&toaddr, 0, sizeof(toaddr));
    toaddr.sin_family = AF_INET;
    toaddr.sin_addr.s_addr = inet_addr(addr.c_str());
    toaddr.sin_port = htons(port);
    tolen = sizeof(toaddr);

    pToaddr = (struct sockaddr*) &toaddr;

    if (m_MsgBufferList.empty()==false)
        sendBuffered();

    sent = ::sendto(m_socketfd, pBuffer, iBufferSize, MSG_DONTWAIT, pToaddr, tolen);

    if (sent<=0)
    {
        if ((errno==EAGAIN)||(errno==EWOULDBLOCK))
        {
            sent = _appendBuffer(pBuffer, iBufferSize, addr, port);
            if (sent==SOCK_ERR)
            {
                DB_AND_LOGL(3,"UDP socket send buffer overflow");
            }
        }
        else
        {
            DB_AND_LOGL(3,"error sending: "<<getSocketErrorString(errno)<<" "<<strerror(errno));
            sent = SOCK_ERR;
        }
    }
    else
    {
        sent = SOCK_OK;
        DB("sent: "<<pBuffer);
    }

    DB("sent message '"<<pBuffer<<"' to "<<addr<<":"<<port<<" with result "<<sent);

    return sent;

}// int UDPSocket::sendTo(const char *pBuffer, int iBufferSize, const string& addr, int port)




int UDPSocket::send(const char *pBuffer, int iBufferSize)
{
    // no UDP send without address
    return SOCK_NOT_IMPLEMENTED;
}// int UDPSocket::send(const char *pBuffer, int iBufferSize)




int UDPSocket::sendBuffered()
{
    DB_ENTER(UDPSocket::sendBuffered);

    if (m_socketfd == -1) {
        return SOCK_ERR;
    }

    int sent = SOCK_OK;
    int i;
    MsgBufferList::iterator iter;

    iter = m_MsgBufferList.begin();
    for (i=0;i<(int)m_MsgBufferList.size();i++)
    {
        struct sockaddr_in toaddr;
        struct sockaddr *pToaddr;
        socklen_t tolen;

        memset(&toaddr, 0, sizeof(toaddr));
        toaddr.sin_family = AF_INET;
        toaddr.sin_addr.s_addr = inet_addr(iter->m_DestIP.c_str());
        toaddr.sin_port = htons(iter->m_DestPort);
        tolen = sizeof(toaddr);

        pToaddr = (struct sockaddr*) &toaddr;

        sent = ::sendto(m_socketfd,iter->m_pBuffer,iter->m_iBufLen, MSG_DONTWAIT, pToaddr, tolen);

        if (sent<=0)
        {
            if ((errno!=EAGAIN)&&(errno!=EWOULDBLOCK))
            {
                DB_AND_LOGL(3,"UDP socket: error sending: "<<getSocketErrorString(errno)<<" "<<strerror(errno));

                delete[] iter->m_pBuffer;
                _ReduceMsgBufferSize(iter->m_iBufLen);

                iter = m_MsgBufferList.erase(iter);
            }

            iter++;

            sent = SOCK_ERR;
        }
        else
        {
            delete[] iter->m_pBuffer;
            _ReduceMsgBufferSize(iter->m_iBufLen);

            iter = m_MsgBufferList.erase(iter);
            sent = SOCK_OK;
        }

    }

    DB("sending buffered "<<sent);

    return sent;

}// int UDPSocket::sendBuffered()




int UDPSocket::recvFrom(char *pBuf, int iBufLen, string &addr, int &port)
{
    struct sockaddr_in fromaddr;
    socklen_t fromlen = sizeof(fromaddr);
    in_addr a;
    int read;
    char szInAddr[INET_ADDRSTRLEN];

    if (m_socketfd == -1) {
        return SOCK_ERR;
    }

    read = ::recvfrom(m_socketfd, pBuf, iBufLen, 0, (struct sockaddr*)&fromaddr, &fromlen);
    if (read<0)
    {
        int iErrno = errno;
        if (iErrno != EAGAIN) {
            DB_AND_LOGL(3,"error receiving from UDP socket "<<getSocketErrorString(errno)<<" "<<strerror(errno));
        }
        read = SOCK_ERR;
    }
    else
    {
        a = fromaddr.sin_addr;
        port = ntohs(fromaddr.sin_port);

        if (inet_ntop(AF_INET, &a, szInAddr, sizeof(szInAddr)) == NULL) {
            szInAddr[0] = '\0';
        }
        addr = (string)szInAddr;
    }

    return read;

}// int UDPSocket::recvFrom(string &sBuf, string &addr, int &port)



int UDPSocket::close()
{
    if (m_socketfd != -1) {
        while (::close(m_socketfd) != 0) {
            if (errno != EINTR) break;
        }
    }

    MsgBufferList::iterator iter;

    for (iter=m_MsgBufferList.begin();iter!=m_MsgBufferList.end();iter++)
    {
        if (iter->m_pBuffer!=NULL)
            delete[] iter->m_pBuffer;
    }

    m_MsgBufferList.clear();

    _ReduceMsgBufferSize(m_iMsgBufferSize);
    return 0;
}// int UDPSocket::close()



int UDPSocket::getFd()
{
    return m_socketfd;
}// int UDPSocket::getFd()



TCPSocket::TCPSocket()
{
    m_socketfd = -1;

    m_ConnectState = 0;
    m_RemotePort = 0;

    m_iAllowedRemoteIPs = 0;
    m_iMsgBufferSize = 0;
    m_MsgBufferList.clear();

}// TCPSocket::TCPSocket()


TCPSocket::TCPSocket(int fd)
{
    m_socketfd = fd;

    m_ConnectState = 0;
    m_RemotePort = 0;

    m_iAllowedRemoteIPs = 0;
    m_iMsgBufferSize = 0;

    m_MsgBufferList.clear();
    DB("created socket from descriptor "<<m_socketfd);

}// TCPSocket::TCPSocket(int fd)


//! transfers a TCP socket to a new TCP socket and frees TCP socket (used by TLSSocket)
TCPSocket::TCPSocket(TCPSocket* tcpSocket)
{
    if (tcpSocket == NULL)
    {
        m_socketfd = -1;

        m_ConnectState = 0;
        m_RemotePort = 0;

        m_iAllowedRemoteIPs = 0;
        m_iMsgBufferSize = 0;
        m_MsgBufferList.clear();

        return;
    }

    m_socketfd       = tcpSocket->m_socketfd;
    m_bIsClient      = tcpSocket->m_bIsClient;
    m_RemoteAddr     = tcpSocket->m_RemoteAddr;
    m_RemotePort     = tcpSocket->m_RemotePort;
    m_LocalAddr      = tcpSocket->m_LocalAddr;
    m_LocalPort      = tcpSocket->m_LocalPort;
    m_ConnectState   = tcpSocket->m_ConnectState;
    memcpy(&m_ServAddr, &(tcpSocket->m_ServAddr), sizeof(m_ServAddr));
    m_iAllowedRemoteIPs  = tcpSocket->m_iAllowedRemoteIPs;
    m_szAllowedRemoteIPs = tcpSocket->m_szAllowedRemoteIPs;

    m_iMsgBufferSize = tcpSocket->m_iMsgBufferSize;
    m_MsgBufferList  = tcpSocket->m_MsgBufferList;

    tcpSocket->m_socketfd = -1;
    tcpSocket->m_MsgBufferList.clear();

    delete tcpSocket;
}

TCPSocket::~TCPSocket()
{
    close();
}// TCPSocket::~TCPSocket()


void TCPSocket::setAllowedIPs(int iAllowedIPs, char **szAllowedIPs)
{
    m_iAllowedRemoteIPs  = iAllowedIPs;
    m_szAllowedRemoteIPs = szAllowedIPs;
}// void TCPSocket::setAllowedIPs(int iAllowedIPs, char **szAllowedIPs)



int TCPSocket::createSocket(int type, const string& addr, int port)
{
    return createSocket(type, addr, port, 100);
}// int TCPSocket::createSocket(int type, const string& addr, int port)



int TCPSocket::createSocket(int type, const string& addr, int port, int backlog)
{
    int on, flags;

    m_socketfd = socket(AF_INET, SOCK_STREAM, 0);

    if(m_socketfd <0)
        return -1;

    // reuse address
    on = 1;
    if(setsockopt(m_socketfd, SOL_SOCKET, SO_REUSEADDR, (char *) &on, sizeof(on)) != 0)
        return -2;

    struct linger l;

    l.l_onoff = 1;
    l.l_linger = 0;
    if(setsockopt(m_socketfd, SOL_SOCKET, SO_LINGER, &l, sizeof(struct linger)) != 0)
        return -3;

    flags = 1;
    if (setsockopt(m_socketfd, SOL_TCP , TCP_NODELAY, &flags, sizeof(flags))<0)
    {// not scaring
        DB_AND_LOGL(5,"NO_DELAY not set "<<strerror(errno)<<" for socket on "<<addr<<":"<<port);
    }

    int optval = IPTOS_LOWDELAY;
    if (setsockopt(m_socketfd, IPPROTO_IP, IP_TOS, (void*)&optval, sizeof(optval))<0)
    {// not scaring
        DB_AND_LOGL(5,"LOWDELAY not set "<<strerror(errno)<<" for socket on "<<addr<<":"<<port);
    }


    // bind
    bzero((char *) &m_ServAddr, sizeof(m_ServAddr));
    m_ServAddr.sin_family = AF_INET;
    if(addr!=(string)"any")
    {
        m_ServAddr.sin_addr.s_addr = inet_addr(addr.c_str());
        m_ServAddr.sin_port = htons(port);
        m_bIsClient = false;
        m_LocalAddr = addr;
        m_LocalPort = port;
    }
    else
    {
        m_ServAddr.sin_addr.s_addr = htonl(INADDR_ANY);
        m_bIsClient = true;
        m_LocalAddr = "any";
        m_LocalPort = 0;
    }
    // cout<<"PORT: "<<m_ServAddr.sin_port<<endl;
    if ( bind(m_socketfd, (struct sockaddr*) &m_ServAddr, sizeof (m_ServAddr)) <0) {
        DB_AND_LOGL(3, "bind to " << addr << ":" << port << " failed when creating TCP socket");
        return -4;
    }

    // listen
    if (addr!=(string)"any")
    {
        if (listen(m_socketfd,backlog)<0)
            return -5;

        if (setsockopt(m_socketfd, SOL_SOCKET, SO_KEEPALIVE, (char *) &on, sizeof(on)) < 0)
        {
            LOGL(5, "Unable to enable SO_KEEPALIVE");
        }
    }

    // set non-blocking
    flags = fcntl(m_socketfd, F_GETFL, 0);
    flags |= O_NONBLOCK | O_NDELAY;

    if (fcntl(m_socketfd, F_SETFL, flags) == -1)
        return -6;

    DB("finished creating TCPSocket");

    return SOCK_OK;

}// int TCPSocket::createSocket(int type, const string& addr, int port, int backlog)




int TCPSocket::sendTo(const char *pBuffer, int iBufferSize, const string& addr, int port)
{
    return send(pBuffer,iBufferSize);
}// int TCPSocket::sendTo(const char *pBuffer, int iBufferSize, const string& addr, int port)



int TCPSocket::send(const char *pBuffer, int iBufferSize)
{
    DB_ENTER(TCPSocket::send);

    if (m_socketfd == -1) {
        return SOCK_ERR;
    }

    int sent = SOCK_OK;

    DB("TCPSocket sending "<<m_bIsClient);

    if ((m_bIsClient==true)&&(m_ConnectState==TCP_WAIT_CONNECT))
    {
        int err_code;
        socklen_t len = sizeof(err_code);

        if (getsockopt(m_socketfd,SOL_SOCKET,SO_ERROR,&err_code,&len) < 0)
        {
            DB_AND_LOGL(2, "getsockopt error: "<<errno<<" "<<strerror(errno));
            m_ConnectState = 0;
            sent = SOCK_ERR;
            return sent;
        }

        if (err_code==0)
        {
            sent = SOCK_OK;
            m_ConnectState = TCP_CONNECTED;
            DB("TCP Socket connected");
        }
        else if ((err_code==EINPROGRESS)||(err_code==EALREADY)||(err_code==EWOULDBLOCK))
        {
            sent = SOCK_WOULDBLOCK;
            //m_ConnectState = TCP_CONNECTED;
            DB("TCP Socket would block");

            sent = _appendBuffer(pBuffer,iBufferSize,(string)"",0);
            if (sent==SOCK_ERR)
            {
                DB_AND_LOGL(3,"TCP socket send buffer overflow");
            }

            return sent;
        }
        else
        {
            sent = SOCK_ERR;
            m_ConnectState = 0;
            DB_AND_LOGL(2,"connect error: "<<err_code<<" "<<strerror(err_code));
        }
    }

    if ((m_bIsClient==false)||(m_ConnectState==TCP_CONNECTED))
    {
        if (m_MsgBufferList.empty()==false){
            sent = sendBuffered();
            //LOG("===>> sendBuffered "<<__PRETTY_FUNCTION__<<"::"<<__LINE__);
        }

        sent = ::send(m_socketfd,pBuffer,iBufferSize,0);

        DB("message sent "<<sent);
        //LOG("=>>message tcp sent: "<<sent);
        if (sent<=0)
        {
            if ((errno==EAGAIN)||(errno==EWOULDBLOCK))
            {
                sent = _appendBuffer(pBuffer,iBufferSize,(string)"",0);
                if (sent==SOCK_ERR)
                {
                    DB_AND_LOGL(3,"TCP socket send buffer overflow");
                }
            }
            else
            {
                sent = SOCK_ERR;
                DB_AND_LOGL(3,"TCP socket send error: "<<errno<<" "<<strerror(errno));
            }
        }
        else if (sent<iBufferSize)
        {// partial send

            sent = _appendBuffer(pBuffer+sent,iBufferSize-sent,(string)"",0);
            if (sent==SOCK_ERR)
            {
                DB_AND_LOGL(3,"TCP Socket send buffer overflow");
            }
        }
        else
        {
            sent = SOCK_OK;
        }

    }
    else
    {// message could not be sent => must buffer

        sent = _appendBuffer(pBuffer,iBufferSize,(string)"",0);
        if (sent==SOCK_ERR)
        {
            DB_AND_LOGL(3,"TCP Socket send buffer overflow");
        }

    }

    DB_AND_LOGL(5, "finished, send message to "<<m_RemoteAddr<<":"<<m_RemotePort<<" with result "<<sent);

    return sent;

}// int TCPSocket::send(const char *pBuffer, int iBufferSize)




int TCPSocket::sendBuffered()
{
    DB_ENTER(TCPSocket::sendBuffered);

    //LOG("=>> TCPSocket::sendBuffered");
    if (m_socketfd == -1) {
        return SOCK_ERR;
    }

    int sent = SOCK_OK;
    int i;

    if ((m_bIsClient==true)&&(m_ConnectState==TCP_WAIT_CONNECT))
    {
        int err_code;
        socklen_t len = sizeof(err_code);

        if (getsockopt(m_socketfd,SOL_SOCKET,SO_ERROR,&err_code,&len) < 0)
        {
            DB_AND_LOGL(2,"getsockopt error: "<<errno<<" "<<strerror(errno));
            m_ConnectState = 0;
            sent = SOCK_ERR;
            return sent;
        }

        if (err_code==0)
        {
            sent = SOCK_OK;
            m_ConnectState = TCP_CONNECTED;
            DB("TCP Socket connected");
        }
        else if ((err_code==EINPROGRESS)||(err_code==EALREADY)||(err_code==EWOULDBLOCK))
        {
            sent = SOCK_WOULDBLOCK;
            //m_ConnectState = TCP_CONNECTED;
            DB("TCP Socket would block");
        }
        else
        {
            sent = SOCK_ERR;
            m_ConnectState = 0;
            DB_AND_LOGL(2,"connect error: "<<err_code<<" "<<strerror(err_code));
        }
    }

    MsgBufferList::iterator iter;

    iter = m_MsgBufferList.begin();
    for (i=0;i<(int)m_MsgBufferList.size();i++)
    {
        if (iter!=m_MsgBufferList.end())
        {
            struct sockaddr_in toaddr;
            struct sockaddr *pToaddr;
            socklen_t tolen;

            toaddr.sin_family = AF_INET;
            toaddr.sin_addr.s_addr = inet_addr(iter->m_DestIP.c_str());
            toaddr.sin_port = htons(iter->m_DestPort);
            tolen = sizeof(toaddr);

            pToaddr = (struct sockaddr*) &toaddr;

            sent = ::send(m_socketfd,iter->m_pBuffer,iter->m_iBufLen, 0);

            //LOG("===============>> sent:"<<sent<<" iter->m_iBufLen:"<<iter->m_iBufLen);
            if (sent==iter->m_iBufLen)
            {
                DB("sent buffered :"<<iter->m_pBuffer);

                delete[] iter->m_pBuffer;
                _ReduceMsgBufferSize(iter->m_iBufLen);

                iter = m_MsgBufferList.erase(iter);

                sent = SOCK_OK;
            }
            else if (sent>0)
            {
                memmove(iter->m_pBuffer,iter->m_pBuffer+sent,iter->m_iBufLen-sent);
                iter->m_iBufLen-=sent;
                _ReduceMsgBufferSize(sent);

                iter++;

                DB("sent part of buffered :"<<iter->m_pBuffer);

                //sent = SOCK_OK;// =>> must change here
                sent = SOCK_WOULDBLOCK;
            }
            else if ((errno==EAGAIN)||(errno==EWOULDBLOCK))
            {
                sent = SOCK_WOULDBLOCK;
                DB("socket would block");
                //LOG("=>> SOCK_WOULDBLOCK");
                break;
            }
            else
            {
                sent = SOCK_ERR;
                DB("error sending to socket "<<errno<<" "<<strerror(errno));

                delete[] iter->m_pBuffer;
                _ReduceMsgBufferSize(iter->m_iBufLen);
                iter = m_MsgBufferList.erase(iter);
                //LOG("=>> error sending to socket ");
                break;
            }
        }
    }


    DB("finished, result="<<sent);
    //LOG("====================>> TCPSocket::sendBuffered returned: "<<sent);

    return sent;

}// int TCPSocket::sendBuffered()




int TCPSocket::recvFrom(char *pBuf, int iBufLen, string &addr, int &port)
{
    DB_ENTER(TCPSocket::recvFrom);

    if (m_socketfd == -1) {
        return SOCK_ERR;
    }

    int result = SOCK_OK, recvd = 0;

    // set remote socket address and port
    addr = m_RemoteAddr;
    port = m_RemotePort;

    recvd = ::recv(m_socketfd,pBuf,iBufLen,0);
    if (recvd<0)
    {
        if ((errno==EAGAIN)||(errno==EWOULDBLOCK))
        {
            DB("socket would block");
            result = SOCK_WOULDBLOCK;
        }
        else
        {
            DB_AND_LOGL(3, "error receiving from TCP socket "<<errno<<" "<<strerror(errno));
            result = SOCK_ERR;
        }
    }
    else if (recvd==0)
    {
        DB_AND_LOGL(3, "connection to " << addr << ":" << port << " has been closed (Socket " << m_socketfd <<")");
        result = SOCK_CLOSED;
    }
    else
    {
        result = recvd;
#ifdef DEBUG
        char* pNewBuf = new char[recvd + 1];
        memcpy(pNewBuf, pBuf, recvd);
        pNewBuf[recvd] = '\0';
        DB("received message '"<<pNewBuf<<"' from "<<addr<<":"<<port<<" with result "<<result);
        delete [] pNewBuf;
#endif
    }

    return result;

}// int TCPSocket::recvFrom(char *pBuf, string &addr, int &port)



int TCPSocket::close()
{
    int iSocket = m_socketfd;
    if (m_socketfd != -1) {
        while (::close(m_socketfd) != 0) {
            if (errno != EINTR) break;
        }
        m_socketfd = -1;
    }

    MsgBufferList::iterator iter;

    for (iter=m_MsgBufferList.begin();iter!=m_MsgBufferList.end();iter++)
    {
        if (iter->m_pBuffer!=NULL)
            delete[] iter->m_pBuffer;
    }

    m_MsgBufferList.clear();

    _ReduceMsgBufferSize(m_iMsgBufferSize);

    DB_AND_LOGL(5, "closing TCP socket "<<iSocket);

    return 0;
}// int TCPSocket::close()




//! check if connection from szRemoteIP is allowed
bool TCPSocket::_checkAllowedRemoteIP(char *szRemoteIP)
{
    DB_ENTER(TCPSocket::_checkAllowedRemoteIP);

    bool result = false;
    int i = 0;

    if (m_iAllowedRemoteIPs==0)
    {
        result = true;
    }
    else
    {
        while ((result==false)&&(i<m_iAllowedRemoteIPs))
        {
            if ((strlen(szRemoteIP)==strlen(m_szAllowedRemoteIPs[i]))
                    &&(strncmp(szRemoteIP,m_szAllowedRemoteIPs[i],strlen(szRemoteIP))==0))
            {
                result = true;
            }
            else
                i++;
        }
    }

    DB("finished: "<<result);

    return result;

}// bool TCPSocket::_checkAllowedRemoteIP(char *szRemoteIP)




TCPSocket* TCPSocket::accept()
{
    DB_ENTER(TCPSocket::accept);

    TCPSocket *tcpClient = NULL;
    struct sockaddr cltAddr;
    int fd = -1;
    socklen_t remoteLen = sizeof(struct sockaddr);

    if (m_socketfd != -1) {
        fd = ::accept(m_socketfd,&cltAddr,&remoteLen);
    }
    if (fd<0)
    {
        DB_AND_LOGL(2,"TCP accept failed "<<errno<<" "<<strerror(errno));
    }
    else
    {
        struct sockaddr_in *cltAddr_in = (struct sockaddr_in *)&cltAddr;

        char szRemoteIP[INET_ADDRSTRLEN];
        if (inet_ntop(AF_INET, &(cltAddr_in->sin_addr),
                      szRemoteIP, sizeof(szRemoteIP)) == NULL) {
            szRemoteIP[0] = '\0';
        }

        // check if remote ip allowed
        if (_checkAllowedRemoteIP(szRemoteIP)==true)
        {
            // set socket non-blocking
            int flags = fcntl(fd, F_GETFL, 0);
            flags |=O_NONBLOCK;
            fcntl(fd,F_SETFL,flags);

            tcpClient = new TCPSocket(fd);
            if (tcpClient!=NULL)
            {
                tcpClient->m_LocalAddr = m_LocalAddr;
                tcpClient->m_LocalPort = m_LocalPort;
                tcpClient->m_RemoteAddr = szRemoteIP;
                tcpClient->m_RemotePort = ntohs(cltAddr_in->sin_port);
                tcpClient->m_bIsClient  = false;
            }
        }
        else
        {
            DB_AND_LOGL(4,"connection attempt from "<<szRemoteIP<<" (forbidden)");
        }

    }

    return tcpClient;

}// TCPSocket* TCPSocket::accept()





int TCPSocket::connect(const string &addr, int port)
{
    if (m_ConnectState == TCP_CONNECTED)
        return 0;

    struct sockaddr_in cltAddr;
    int result;

    DB_ENTER(TCPSocket::connect);

    m_RemoteAddr = addr;
    m_RemotePort = port;

    if (m_socketfd == -1) {
        return -1;
    }

    memset(&cltAddr, 0, sizeof(cltAddr));
    cltAddr.sin_family      = AF_INET;
    cltAddr.sin_addr.s_addr = inet_addr(addr.c_str());
    cltAddr.sin_port        = htons(port);

    /*
  int flags = fcntl(m_socketfd, F_GETFL, 0);
  flags &= ~O_NONBLOCK;

  DB("Set socket to blocking");
  if (fcntl(m_socketfd, F_SETFL, flags) == -1)
    return -1;
*/

    result = ::connect(m_socketfd,(const struct sockaddr*)&cltAddr,sizeof(struct sockaddr_in));

    /*
  DB("Set socket to nonblocking");
  flags |= O_NONBLOCK;
  if (fcntl(m_socketfd, F_SETFL, flags) == -1)
    return -1;
*/

    if (result<0)
    {
        if (errno==EINPROGRESS)
        {
            DB("connect would block");
            result = SOCK_WOULDBLOCK;

            m_ConnectState = TCP_WAIT_CONNECT;
        }
        else
        {
            DB_AND_LOGL(3,"connect to "<<addr<<":"<<port<<" failed "<<strerror(errno));
            result = SOCK_ERR;
            m_ConnectState = 0;
        }
    }
    else {
        m_ConnectState = TCP_CONNECTED;
    }

    return result;

}// int TCPSocket::connect(const string &addr, int port)




int TCPSocket::getFd()
{
    return m_socketfd;
}// int TCPSocket::getFd()




string TCPSocket::getRemoteAddr()
{
    return m_RemoteAddr;
}// string TCPSocket::getRemoteAddr()


int TCPSocket::getRemotePort()
{
    return m_RemotePort;
}// int TCPSocket::getRemotePort()





TLSSocket::TLSSocket()
    : TCPSocket()
{
    //cout<<"Constructing tls socket"<<endl;
    m_CTX = NULL;
    m_SSL = NULL;

    m_bCTXInitiated = false;
    m_ConnectState = 0;
    m_AcceptState  = 0;
    m_iMsgBufferSize = 0;
    m_MsgBufferList.clear();

}// TLSSocket::TLSSocket()



//! converts a connected TCP socket to TLS and frees TCP socket
TLSSocket::TLSSocket(TCPSocket* tcpSocket)
    : TCPSocket(tcpSocket)
{
    //cout<<"Constructing tls socket using tcp socket"<<endl;

    m_CTX = NULL;
    m_SSL = NULL;
    m_bCTXInitiated=false;
    m_ConnectState = 0;
    m_AcceptState  = 0;

    int result;
    if (m_bIsClient)
        result = _initClientTLS();
    else
        result = _initTLS();

    if (result==0)
    {
        DB("TLS initialized");
        if (m_bIsClient)
        {
            connect(m_RemoteAddr, m_RemotePort);
        }
        else
        {
            int accept_state = TLS_ACCEPTED;

            ossl::SSL* pNewSSL = SSL_new(m_CTX);
            SSL_set_fd(pNewSSL,m_socketfd);
            SSL_set_mode(pNewSSL,SSL_MODE_ACCEPT_MOVING_WRITE_BUFFER);//=>>
            result = SSL_accept(pNewSSL);

            if (result<0)
            {
                if (SSL_get_error(pNewSSL, result) == SSL_ERROR_WANT_READ)
                {
                    accept_state = TLS_WAIT_ACCEPT;//       LOGL(1,"accept would block - readability");
                }
                else if (SSL_get_error(pNewSSL, result) == SSL_ERROR_WANT_WRITE)
                {
                    accept_state = TLS_WAIT_ACCEPT;//       LOGL(1,"accept would block - writability");
                }
                else
                {
                    ERR_print_errors_fp(stderr);
                    DB_AND_LOGL(2,"error establishing TLS connection "<<SSL_get_error(pNewSSL,result));

                    SSL_free(pNewSSL);
                    return;
                }
            }

            m_SSL = pNewSSL;
            m_AcceptState = accept_state;
        }
    }
    else
    {
        if (m_socketfd != -1) {
            while (::close(m_socketfd) != 0) {
                if (errno != EINTR) break;
            }
            m_socketfd = -1;
        }
    }
}


TLSSocket::~TLSSocket()
{
    //cout<<"destructing tls socket"<<endl;
    close();
}// TLSSocket::~TLSSocket()




int TLSSocket::_initTLS()
{
    DB_ENTER(TLSSocket::_initTLS);

    int err_code;
    char *err_string;

    // set up the library and load error strings
    ERR_load_BIO_strings();
    ERR_load_crypto_strings();
    SSL_load_error_strings();
    SSL_library_init();

    // set up the SSL context
    // we understand SSL, TLS
#ifdef SSLV23
    m_CTX = SSL_CTX_new(SSLv23_method()); // TLSv1_method();
#else
    m_CTX = SSL_CTX_new(TLSv1_method()); // TLSv1_method();
#endif
    m_bCTXInitiated = true;
    //printf("SSL CTX new:  %x\n",m_CTX);
    if (m_CTX==NULL)
    {
        int code = ERR_get_error();
        LOG("SSL context creation failed");
        LOG("SSL context creation failed");
        err_string = ERR_error_string(code,NULL);
        if (err_string!=NULL)
        {
            LOG("reason: "<<err_string);
            DB("reason: "<<err_string);
        }
        //printf("SSL CTX FREE:  %x\n",m_CTX);
        SSL_CTX_free(m_CTX);
        m_CTX = NULL;
        return TLS_INTERNAL;
    }

    // load the trust store
    if(! SSL_CTX_load_verify_locations(m_CTX, m_sCertFile.c_str(), NULL))
    {
        int code = ERR_get_error();
        LOG("verify location from file '"<<m_sCertFile<<"' failed");
        DB("verify location from file '"<<m_sCertFile<<"' failed");
        err_string = ERR_error_string(code,NULL);
        if (err_string!=NULL)
        {
            LOG("reason: "<<err_string);
            DB("reason: "<<err_string);
        }
        //printf("SSL CTX FREE:  %x\n",m_CTX);
        SSL_CTX_free(m_CTX);
        m_CTX = NULL;
        return TLS_INTERNAL;
    }

    /* set the local certificate from CertFile */
    if (!SSL_CTX_use_certificate_chain_file(m_CTX, m_sCertFile.c_str()))
    {
        int code = ERR_get_error();
        LOG("cert setup from file '"<<m_sCertFile<<"' failed");
        DB("cert setup from file '"<<m_sCertFile<<"' failed");
        err_string = ERR_error_string(code,NULL);
        if (err_string!=NULL)
        {
            LOG("reason: "<<err_string);
            DB("reason: "<<err_string);
        }
        //printf("SSL CTX FREE:  %x\n",m_CTX);
        SSL_CTX_free(m_CTX);
        m_CTX = NULL;
        return TLS_INTERNAL;
    }

    /* set password callback */
    SSL_CTX_set_default_passwd_cb(m_CTX,pem_passwd_cb);

    /* set the private key from KeyFile */
    err_code = SSL_CTX_use_PrivateKey_file(m_CTX, m_sCertKeyFile.c_str(), SSL_FILETYPE_PEM);
    if (err_code==0)
    {
        int code = ERR_get_error();

        LOG("using keyfile '"<<m_sCertKeyFile<<"' failed");
        DB("using keyfile '"<<m_sCertKeyFile<<"' failed, code="<<code);

        err_string = ERR_error_string(code,NULL);
        if (err_string!=NULL)
        {
            LOG("reason: "<<err_string);
            DB("reason: "<<err_string);
        }
        //printf("SSL CTX FREE:  %x\n",m_CTX);
        SSL_CTX_free(m_CTX);
        m_CTX = NULL;
        return TLS_INTERNAL;
    }

    SSL_CTX_set_timeout(m_CTX,10);

    err_code = SSL_CTX_check_private_key(m_CTX);
    if (err_code==0)
    {
        int code = ERR_get_error();
        LOG("private key check failed");
        DB("private key check failed");
        err_string = ERR_error_string(code,NULL);
        if (err_string!=NULL)
        {
            LOG("reason: "<<err_string);
            DB("reason: "<<err_string);
        }
        //printf("SSL CTX FREE:  %x",m_CTX);
        m_CTX = NULL;
        return TLS_INTERNAL;
    }

    DB(" TLS context set up");

    return 0;

}// int TLSSocket::_initTLS()



int TLSSocket::_initClientTLS()
{

#ifdef SSLV23
    m_CTX = SSL_CTX_new(SSLv23_method());
#else
    m_CTX = SSL_CTX_new(TLSv1_method());
#endif
    m_bCTXInitiated = true;
    //printf("SSL CTX new:  %x\n",m_CTX);
    return 0;

}// int TLSSocket::_initClientTLS()



int TLSSocket::_handleError(int err_code, const char* caller)
{
    DB_ENTER(TLSSocket::_handleError);

    int result = SOCK_ERR;

    if (err_code==SSL_ERROR_ZERO_RETURN)
    {// the SSL connection was closed, we close the socket in this case
        result = SOCK_ERR;
        DB_AND_LOGL(5,caller<<" TLS Socket: connection closed");
    }
    else if ((err_code==SSL_ERROR_WANT_READ)||(err_code==SSL_ERROR_WANT_WRITE))
    {
        result = SOCK_WOULDBLOCK;
        DB(caller<<" TLS Socket: would block");
    }
    else if (err_code==SSL_ERROR_SYSCALL)
    {
        if ((errno==EWOULDBLOCK)||(errno==EAGAIN))
        {
            result = SOCK_WOULDBLOCK;
            DB(caller<<" TLS Socket would block");
        }
        else
        {
            result = SOCK_ERR;
            DB_AND_LOGL(5,caller<<" underlying socket error errno="<<errno<<" "<<strerror(errno));
        }
    }
    else if (err_code!=SSL_ERROR_NONE) // this check is somewhat paranoid
    {
        char *err_string;

        result = SOCK_ERR; // this means the socket will be closed
        err_string = ERR_error_string(err_code, NULL);
        if (err_string!=NULL)
        {
            DB_AND_LOGL(5,caller<<" TLS Socket: "<<_getErrorText(m_SSL,err_code)<<" "<<err_string);
        }
    }
    else
    {
        DB(caller<<" TLS Socket no error");
        result = SOCK_WOULDBLOCK;
    }

    return result;

}// int TLSSocket::_handleError()




int TLSSocket::createSocket(int type, const string& addr, int port, int backlog)
{
    int result = TLS_OK;

    DB_ENTER(TLSSocket::createSocket);

    result = TCPSocket::createSocket(0,addr,port,backlog);
    if (result==0)
    {
        if (addr=="any")
            result = _initClientTLS();
        else
            result = _initTLS();
    }

    if (result==0)
    {
        DB("TLS socket created");
    }
    else
    {
        if (m_socketfd != -1) {
            while (::close(m_socketfd) != 0) {
                if (errno != EINTR) break;
            }
            m_socketfd = -1;
        }
    }

    return result;

}// int TLSSocket::createSocket(int type, const string& addr, int port, int backlog)




TLSSocket* TLSSocket::accept()
{

    DB_ENTER(TLSSocket::accept);

    TLSSocket *pTLSSocket = NULL;
    int fd, result = 0;
    SSL *pNewSSL;
    struct sockaddr cltAddr;
    socklen_t remoteLen = sizeof(struct sockaddr);

    fd = ::accept(m_socketfd,&cltAddr,&remoteLen);
    if (fd<0)
    {
        DB_AND_LOGL(2,"TLS Socket error: accept failed "<<errno<<" "<<strerror(errno));
    }
    else
    {
        struct sockaddr_in *cltAddr_in = (struct sockaddr_in *)&cltAddr;
        int accept_state = TLS_ACCEPTED;

        DB("accepted connection");


        // set non-blocking
        int flags = fcntl(fd, F_GETFL, 0);
        flags |=O_NONBLOCK;
        fcntl(fd,F_SETFL,flags);

        pNewSSL = SSL_new(m_CTX);

        SSL_set_fd(pNewSSL,fd);
        SSL_set_mode(pNewSSL,SSL_MODE_ACCEPT_MOVING_WRITE_BUFFER); //=>>
        result = SSL_accept(pNewSSL);

        if (result<0)
        {
            if (SSL_get_error(pNewSSL, result) == SSL_ERROR_WANT_READ)
            {
                accept_state = TLS_WAIT_ACCEPT;//	      LOGL(1,"accept would block - readability");
            }
            else if (SSL_get_error(pNewSSL, result) == SSL_ERROR_WANT_WRITE)
            {
                accept_state = TLS_WAIT_ACCEPT;//	      LOGL(1,"accept would block - writability");
            }
            else
            {
                ERR_print_errors_fp(stderr);
                DB_AND_LOGL(2,"error establishing TLS connection "<<SSL_get_error(pNewSSL,result));

                SSL_free(pNewSSL);
                while (::close(fd) != 0) {
                    if (errno != EINTR) break;
                }

                return NULL;
            }
        }

        pTLSSocket               = new TLSSocket;
        if (pTLSSocket!=NULL)
        {
            pTLSSocket->m_SSL        = pNewSSL;
            pTLSSocket->m_CTX        = m_CTX;
            pTLSSocket->m_socketfd   = fd;
            pTLSSocket->m_AcceptState = accept_state;
            pTLSSocket->m_bIsClient  = false;

            char szRemoteIP[INET_ADDRSTRLEN];
            if (inet_ntop(AF_INET, &(cltAddr_in->sin_addr),
                          szRemoteIP, sizeof(szRemoteIP)) == NULL) {
                szRemoteIP[0] = '\0';
            }

            pTLSSocket->m_RemoteAddr = (string)szRemoteIP;
            pTLSSocket->m_RemotePort = ntohs(cltAddr_in->sin_port);

            DB("accepted new TLS connection, state="<<pTLSSocket->m_AcceptState);
        }
    }

    return pTLSSocket;

}//TLSSocket* TLSSocket::accept()




int TLSSocket::connect(const string &addr, int port)
{
    int result;

    result = TCPSocket::connect(addr,port);
    if (result==0)
    {
        m_SSL = SSL_new(m_CTX);           /* create new SSL connection state */
        SSL_set_fd(m_SSL, m_socketfd);    /* attach the socket descriptor */
        SSL_set_mode(m_SSL,SSL_MODE_ACCEPT_MOVING_WRITE_BUFFER);//=>>
        SSL_set_connect_state(m_SSL);

        m_iConnectionTry = 0;

        result = _connectSSL();
    }
    else if (errno==EINPROGRESS)
    {
        m_SSL = SSL_new(m_CTX);           /* create new SSL connection state */
        SSL_set_fd(m_SSL, m_socketfd);    /* attach the socket descriptor */
        SSL_set_mode(m_SSL,SSL_MODE_ACCEPT_MOVING_WRITE_BUFFER);//=>>
        SSL_set_connect_state(m_SSL);

        m_ConnectState = TLS_SOCKET_WAITING;

        result = SOCK_WOULDBLOCK;
    }
    else
    {
        DB_AND_LOGL(2,"could not connect to "<<addr<<":"<<port<<"/TLS"<<"; reason: "<< errno << " " << strerror(errno));

        m_ConnectState = 0;
        result = SOCK_ERR;
    }

    return result;

}// int TLSSocket::connect(const string &addr, int port)



int TLSSocket::_connectSSL()
{
    int result = SOCK_OK, connected;

    DB_ENTER(TLSSocket::_connectSSL);

    connected = SSL_connect(m_SSL);      /* perform the connection */

    if (connected<0)
    {
        // we should check errors here
        int err_code = SSL_get_error(m_SSL,connected);

        result = _handleError(err_code,"TLSSocket::_connectSSL");
        if (result==SOCK_WOULDBLOCK)
        {
            m_iConnectionTry++;

            if (m_iConnectionTry>50)
            {
                result = SOCK_ERR;
                DB_AND_LOGL(2,"TLS connect error: exceeded connection retry count");
            }
            else
                m_ConnectState = TLS_SOCKET_CONNECTED;

            DB("TLS Socket connect: SSL connection incomplete");

        }
        else
        {
            DB_AND_LOGL(2,"TLS Socket error: connect failed: "<<_getErrorText(m_SSL,err_code));
            DB("TLS connect failed: "<<_getErrorText(m_SSL,err_code));

            result = SOCK_ERR;
        }
    }
    else
    {
        result = SOCK_OK;
        m_ConnectState = TLS_CONNECTED;
        DB("TLS Socket connect: SSL connection complete");

        if (m_MsgBufferList.empty()==false)
            sendBuffered();
    }

    DB("finished: "<<result);

    return result;

}// int TLSSocket::_connectSSL()



int TLSSocket::send(const char *pBuffer, int iBufferSize)
{
    DB_ENTER(TLSSocket::send);

    //LOG("=>> entered into tlssocket::send");
    if (m_socketfd == -1) {
        return SOCK_ERR;
    }

    int result = SOCK_ERR;
    int sent = 0;
    int err_code;
    socklen_t len;

    DB("connection state: "<<m_bIsClient<<" "<<m_AcceptState<<" "<<m_ConnectState);

    if ((m_bIsClient==false)&&(m_AcceptState==TLS_WAIT_ACCEPT))
    {
        result = _acceptSSL(); // may set m_AcceptState
        if (result==TLS_ERROR)
        {
            result = SOCK_ERR;
            DB("TLS send: accept failed");
            //LOG("=>>TLS send: accept failed");
        }
        else
        {
            result = SOCK_WOULDBLOCK;
            //LOG("=>>TLS send: accept succeeded");
            DB("TLS send: accept succeeded");
        }
    }

    if (m_bIsClient==true)
    {
        if (m_ConnectState==TLS_SOCKET_WAITING)
        {
            getsockopt(m_socketfd,SOL_SOCKET,SO_ERROR,&err_code,&len);
            if (err_code==0)
            {
                m_ConnectState = TLS_SOCKET_CONNECTED;
                result = SOCK_WOULDBLOCK;
                DB("TLS Socket connected");
            }
        }

        if (m_ConnectState==TLS_SOCKET_CONNECTED)
        {
            result = _connectSSL();
        }
    }

    if (m_SSL!=NULL)
    {

        if ((m_AcceptState==TLS_ACCEPTED)||(m_ConnectState==TLS_CONNECTED))
        {
            if (m_MsgBufferList.empty()==false)
                sent = sendBuffered();

            sent = SSL_write(m_SSL,pBuffer,iBufferSize);

            DB("message sent "<<sent);
            //LOG("=>>message sent: "<<sent<<" tried to send:"<<iBufferSize);
            if (sent<=0)
            {
                err_code = SSL_get_error(m_SSL,sent);
                //LOG("=>> error_code: "<<err_code);
                //LOG("=>> ssl state:"<<SSL_state_string(m_SSL));
                if ((err_code==SSL_ERROR_WANT_READ)||(err_code==SSL_ERROR_WANT_WRITE))
                {
                    //LOG("=>> appendding buff");
                    sent = _appendBuffer(pBuffer,iBufferSize,(string)"",0);
                    if (sent==SOCK_ERR)
                    {
                        DB_AND_LOGL(3,"TLS socket send buffer overflow");
                    }
                    result = sent;
                }
                else if (err_code==SSL_ERROR_SYSCALL)
                {
                    DB_AND_LOGL(5,"underlying socket error "<<sent<<" errno="<<errno<<" "<<strerror(errno));
                    //LOG("=>> sent before check -1: "<<sent);
                    if (sent==-1)
                    {
                        //LOG("=>> errno: "<<errno);
                        if ((errno==EWOULDBLOCK)||(errno==EAGAIN))
                        {
                            //LOG("=>> appending to buffer");
                            sent = _appendBuffer(pBuffer,iBufferSize,(string)"",0);
                            if (sent==SOCK_ERR)
                            {
                                DB_AND_LOGL(3,"TLS socket send buffer overflow 2");
                            }
                            result = sent;
                        }
                        else
                        {
                            DB_AND_LOGL(3,"TLS socket send error1: "<<errno<<" "<<strerror(errno));
                            result = SOCK_ERR;
                        }
                    }
                    else
                    {
                        DB_AND_LOGL(3,"TLS socket send error2: "<<errno<<" "<<strerror(errno));
                        result = SOCK_ERR;
                    }
                }
                else
                {
                    char *err_string;

                    err_string = ERR_error_string(err_code, NULL);
                    if (err_string!=NULL)
                    {
                        DB_AND_LOGL(3,"TLS socket send error3: "<<err_code<<" "<<err_string);
                    }
                    else
                    {
                        DB_AND_LOGL(3,"TLS socket error: send unknown error");
                    }

                    result = SOCK_ERR;

                }

            }
            else if (sent<iBufferSize)
            {
                //LOG("=>> sent<iBufferSize:"<<sent<<" "<<iBufferSize);
                sent = _appendBuffer(pBuffer+sent,iBufferSize-sent,(string)"",0);
                if (sent==SOCK_ERR)
                {
                    DB_AND_LOGL(3,"TLS socket send buffer overflow3");
                }
                result = sent;
            }
            else
            {
                DB("TLS socket write successful "<<sent<<"=="<<iBufferSize);
                result = SOCK_OK;
            }

        }
        else
        {
            //LOG("=>> appending at end");
            sent = _appendBuffer(pBuffer,iBufferSize,(string)"",0);
            if (sent==SOCK_ERR)
            {
                DB_AND_LOGL(3,"TLS socket send buffer overflow4");
            }
            result = sent;
        }

    }

    DB_AND_LOGL(5, "finished, send message to "<<m_RemoteAddr<<":"<<m_RemotePort<<" with result "<<result);

    return result;

}// int TLSSocket::send(const char *pBuffer, int iBufferSize)



int TLSSocket::sendTo(const char *pBuffer, int iBufferSize, const string& addr, int port)
{
    DB_ENTER(TLSSocket::sendTo);
    return send(pBuffer,iBufferSize);
}// int TLSSocket::sendTo(const char *pBuffer, int iBufferSize, const string& addr, int port)




int TLSSocket::sendBuffered()
{
    DB_ENTER(TLSSocket::sendBuffered);

    if (m_socketfd == -1) {
        return SOCK_ERR;
    }

    //LOG("=>> TLSSocket::sendBuffered");
    int result = SOCK_OK;
    int err_code, i;
    socklen_t len;

    if ((m_bIsClient==false)&&(m_AcceptState==TLS_WAIT_ACCEPT))
    {
        result = _acceptSSL(); // may set m_AcceptState
        if (result==TLS_ERROR)
        {
            result = SOCK_ERR;
            DB("TLS send: accept failed");
        }
        else
        {
            result = SOCK_WOULDBLOCK;
            DB("TLS send: accept succeeded");
        }
    }

    if (m_bIsClient==true)
    {
        if (m_ConnectState==TLS_SOCKET_WAITING)
        {
            getsockopt(m_socketfd,SOL_SOCKET,SO_ERROR,&err_code,&len);
            if (err_code==0)
            {
                m_ConnectState = TLS_SOCKET_CONNECTED;
                result = SOCK_WOULDBLOCK;
                DB("TLS Socket connected");
            }
        }

        if (m_ConnectState==TLS_SOCKET_CONNECTED)
        {
            result = _connectSSL();
        }
    }


    MsgBufferList::iterator iter;
    int sent;

    iter = m_MsgBufferList.begin();
    for (i=0;i<(int)m_MsgBufferList.size();i++)
    {
        struct sockaddr_in toaddr;
        struct sockaddr *pToaddr;
        socklen_t tolen;

        toaddr.sin_family = AF_INET;
        toaddr.sin_addr.s_addr = inet_addr(iter->m_DestIP.c_str());
        toaddr.sin_port = htons(iter->m_DestPort);
        tolen = sizeof(toaddr);

        pToaddr = (struct sockaddr*) &toaddr;

        sent = SSL_write(m_SSL,iter->m_pBuffer,iter->m_iBufLen);

        //LOG("=>> sent:"<<sent<<" tried:"<<iter->m_iBufLen);
        if (sent<=0)
        {
            err_code = SSL_get_error(m_SSL,sent);
            if ((err_code==SSL_ERROR_WANT_READ)||(err_code==SSL_ERROR_WANT_WRITE))
            {
                result = SOCK_WOULDBLOCK;
                DB("TLS Socket send: would block");
                break;
            }
            else if (err_code==SSL_ERROR_SYSCALL)
            {
                DB("underlying socket error "<<sent<<" errno="<<errno<<" "<<strerror(errno));
                LOGL(5, "underlying socket error "<<sent<<" errno="<<errno<<" "<<strerror(errno));
                if (sent==-1)
                {
                    if ((errno==EWOULDBLOCK)||(errno==EAGAIN))
                    {
                        result = SOCK_WOULDBLOCK;
                        DB("TLS Socket receive would block");
                    }
                }
                else
                {
                    result = SOCK_ERR;
                }
                break;
            }
            else
            {
                char *err_string;

                err_string = ERR_error_string(err_code, NULL);
                if (err_string!=NULL)
                {
                    DB_AND_LOGL(5, "TLS Socket write: code: "<<err_code<<" "<<err_string);
                }
                else
                {
                    DB_AND_LOGL(5, "TLS socket error");
                }
                result = SOCK_ERR;
                break;
            }
        }
        else if (sent!=iter->m_iBufLen)
        {
            memmove(iter->m_pBuffer,iter->m_pBuffer+sent,iter->m_iBufLen-sent);
            iter->m_iBufLen-=sent;
            _ReduceMsgBufferSize(sent);

            result = SOCK_WOULDBLOCK;
            break;
        }
        else
        {
            delete[] iter->m_pBuffer;
            _ReduceMsgBufferSize(iter->m_iBufLen);

            iter = m_MsgBufferList.erase(iter);

            result = SOCK_OK;
        }

    }


    DB("finished, result="<<result);
    //LOG("=================>> TLSSocket::sendBuffered returned: "<<result);
    return result;

}// int TLSSocket::sendBuffered()




//! finished acceptance of SSL/TLS conenction
int TLSSocket::_acceptSSL()
{
    DB_ENTER(TLSSocket::_acceptSSL);

    int accepted, result = SOCK_OK, err_code;

    accepted = SSL_accept(m_SSL);
    if (accepted<0)
    {
        err_code = SSL_get_error(m_SSL,accepted);
        if (err_code==SSL_ERROR_NONE)
        {
            m_AcceptState = TLS_ACCEPTED;
            result = SOCK_OK;
            DB("TLS accept no error");
        }
        else if ((err_code==SSL_ERROR_WANT_READ)||(err_code==SSL_ERROR_WANT_WRITE))
        {
            result = SOCK_OK;
            DB("TLS accept would block");
        }
        else
        {
            char *err_string;

            err_string = ERR_error_string(err_code, NULL);
            if (err_string!=NULL)
            {
                DB_AND_LOGL(2,"TLS Socket accept failed: code: "<<_getErrorText(m_SSL,err_code)<<" "<<err_string);
            }
            else
            {
                DB_AND_LOGL(2,"TLS Socket accept failed: code: "<<_getErrorText(m_SSL,err_code));
            }

            result = TLS_ERROR;
        }
    }
    else
    {
        m_AcceptState = TLS_ACCEPTED;
        result = SOCK_OK;

        DB("TLS accept succeeded");
    }

    return result;

}// int TLSSocket::_acceptSSL()




//! receives up to MAXMSGSIZE data from the socket
//! parameters:
//! @char* message buffer
//! @string& address of peer
//! @int& port of peer 
int TLSSocket::recvFrom(char *pBuf, int iBufLen, string &addr, int &port)
{
    int result = SOCK_WOULDBLOCK, recvd = 0; // try again

    DB_ENTER(TLSSocket::recvFrom);

    // set remote socket address and port
    addr = m_RemoteAddr;
    port = m_RemotePort;

    DB("connection state: "<<m_bIsClient<<" "<<m_AcceptState<<" "<<m_ConnectState);

    // check if ssl accept is pending: just one state possible here, ::accept does not block
    if ((m_bIsClient==false)&&(m_AcceptState==TLS_WAIT_ACCEPT))
    {
        result = _acceptSSL(); // may set m_AcceptState
        if (result==TLS_ERROR)
        {
            result = SOCK_ERR;
            DB("TLS receive: accept failed");
        }
        else
        {
            result = SOCK_WOULDBLOCK;
            DB("TLS receive: accept succeeded");
        }
    }

    // check if connect is pending; can have two states as ::connect may block
    if (m_bIsClient==true)
    {
        if (m_ConnectState==TLS_SOCKET_WAITING)
        {
            int err_code, result;
            socklen_t len;
            getsockopt(m_socketfd,SOL_SOCKET,SO_ERROR,&err_code,&len);
            if (err_code==0)
            {
                m_ConnectState = TLS_SOCKET_CONNECTED;
                result = SOCK_WOULDBLOCK;
                DB("TLS Socket connected");
            }
        }
        if (m_ConnectState==TLS_SOCKET_CONNECTED)
        {
            // set socket non-blocking
            int flags = fcntl(m_socketfd, F_GETFL, 0);
            flags |=O_NONBLOCK;
            fcntl(m_socketfd,F_SETFL,flags);

            result = _connectSSL(); // may set m_ConnectState
        }
    }

    if (m_SSL!=NULL)
    {
        int err_code;

        if ((m_AcceptState==TLS_ACCEPTED)||(m_ConnectState==TLS_CONNECTED))
        {
            // this can be >0 (success), 0 (socket or transport shutdown), or <0 (error)
            recvd = SSL_read(m_SSL,pBuf,iBufLen);

            if (recvd<0)
            {
                err_code = SSL_get_error(m_SSL,result);
                result = _handleError(err_code,"TLSSocket::recvFrom");
            }
            else if (recvd==0)
            {
                DB("TLS Socket receive: connection closed");
                result = SOCK_CLOSED;
            }
            else
            {
                DB("received "<<recvd<<" bytes");
                result = recvd;
#ifdef DEBUG
                char* pNewBuf = new char[recvd + 1];
                memcpy(pNewBuf, pBuf, recvd);
                pNewBuf[recvd] = '\0';
                DB("finished, received message '"<<pNewBuf<<"' from "<<addr<<":"<<port<<" with result "<<result);
                delete [] pNewBuf;
#endif
            }

        }

    }
    else
    {
        DB_AND_LOGL(3,"TLS Socket receive: SSL down");
        result = SOCK_ERR;
    }


    return result;

}// int TLSSocket::recvFrom(char *pBuf, string &addr, int &port)



int TLSSocket::close()
{
    DB_ENTER(TLSSocket::close);

    if (m_SSL!=NULL)
    {
        SSL_shutdown(m_SSL);
        DB("connection shut down");
        SSL_free(m_SSL);
        DB("SSL object freed");

        m_SSL = NULL;
    }

    if (m_CTX!=NULL)
    {
        // flush sessions immediately
        SSL_CTX_flush_sessions(m_CTX, time(NULL));

        //dipal: changed it to remove leak; not perfectly sure: what the hell does m_bIsClient do
        // check here if crash
        //cout<<"ISCLIENT: "<<m_bIsClient<<endl;
        if (m_bIsClient==true || m_bCTXInitiated)
            shutdownTLS();

        m_CTX = NULL;
    }

    if (m_socketfd != -1) {
        while (::close(m_socketfd) != 0) {
            if (errno != EINTR) break;
        }
        m_socketfd = -1;
    }

    MsgBufferList::iterator iter;

    for (iter=m_MsgBufferList.begin();iter!=m_MsgBufferList.end();iter++)
    {
        if (iter->m_pBuffer!=NULL)
            delete[] iter->m_pBuffer;
    }

    m_MsgBufferList.clear();

    _ReduceMsgBufferSize(m_iMsgBufferSize);

    DB_AND_LOGL(5, "finished, TLS socket closed");

    return 0;

}// int TLSSocket::close()



void TLSSocket::shutdownTLS()
{
    // release the shared CTX object
    if (m_CTX!=NULL)
    {
        //printf("SSL CTX FREE:  %x\n",m_CTX);
        SSL_CTX_free(m_CTX);
        m_CTX=NULL;
    }

}// void TLSSocket::shutdownTLS()



const char* TLSSocket::_getErrorText(SSL* ssl, int err)
{
    assert(ssl != NULL);

    switch (err)
    {
    case SSL_ERROR_NONE:
        return "SSL_ERROR_NONE";
    case SSL_ERROR_WANT_WRITE:
        return "SSL_ERROR_WANT_WRITE";
    case SSL_ERROR_WANT_READ:
        return "SSL_ERROR_WANT_READ";
    case SSL_ERROR_WANT_CONNECT:
        return "SSL_ERROR_WANT_CONNECT";
    case SSL_ERROR_WANT_X509_LOOKUP:
        return "SSL_ERROR_WANT_X509_LOOKUP";
    case SSL_ERROR_SYSCALL:
        return "SSL_ERROR_SYSCALL";
    case SSL_ERROR_SSL:
        return "SSL_ERROR_SSL";
    case SSL_ERROR_ZERO_RETURN:
        return "SSL_ERROR_ZERO_RETURN";
    default:
        return "unknown";
    }
}// char* TLSSocket::_getErrorText(SSL* ssl, int err)

//Callback
int TLSSocket::pem_passwd_cb(char *buf, int size, int rwflag, void *userdata)
{
    string password;

    DB("getting password for user '" <<m_sCertUser<< "' from file '" <<m_sPassFile<< "'");

    if (m_sPassFile != "")
    {
        password = Crypt::get_passwd(m_sPassFile,m_sCertUser);
    }
    else
    {
        return 0;
    }

    int iPasswordLen = password.length();
    if (iPasswordLen < size)
    {
        memcpy(buf,password.c_str(),iPasswordLen);
        buf[iPasswordLen] = '\0';
        DB("password read: '"<<password<<"'");
        return iPasswordLen;
    }
    else
    {
        return 0;
    }

} //int TLSSocket::pem_passwd_cb(char *buf, int size, int rwflag, void *userdata)

string TLSSocket::m_sPassFile;
string TLSSocket::m_sCertFile;
string TLSSocket::m_sCertKeyFile;
string TLSSocket::m_sCertUser;

//Sets static variables
void TLSSocket::SetKeyInfo(const string& sPassFile, const string& sCertUser,
                           const string& sCertFile, const string& sCertKeyFile)
{
    m_sPassFile = sPassFile;
    m_sCertUser = sCertUser;
    m_sCertFile = sCertFile;
    m_sCertKeyFile = sCertKeyFile;
} // void TLSSocket::SetKeyInfo(const string& sPassFile, const string& sCertUser,
//                            const string& sCertFile, const string& sCertKeyFile)



/*
  $Log: Socket.cc,v $
  Revision 1.37  2007/12/14 22:06:38  lennyp
  added a debug msg

  Revision 1.36  2007/07/17 23:41:31  sshelfor
  Enable SO_KEEPALIVE for TCP connections.

  Revision 1.35  2007/05/18 00:56:43  zionk
  Fix convert TCP to TLS

  Revision 1.34  2007/05/15 16:22:48  zionk
  Begin modifications to servercoremt for XMPP server-to-server communication

  Revision 1.33  2006/12/08 22:08:54  larsb
  replaced use_certificate_file with use_certificate_chain_file

  Revision 1.32  2006/10/25 17:08:11  zionk
  initialize m_RemotePort to 0

  Revision 1.31  2006/10/25 16:12:05  zionk
  Erase a buffered msg in case of socket error. (otherwise may lead to double free)
  Set remote address in TCPSocket::connect, whether or not connect succeeds.

  Revision 1.30  2006/10/23 18:54:35  zionk
  Cleanup send buffer when closing socket.

  Revision 1.29  2006/09/18 20:50:05  zionk
  Check if m_socketfd is -1 before using it

  Revision 1.28  2006/09/14 21:03:53  zionk
  check if socket file descriptor is -1 before sending

  Revision 1.27  2006/09/12 00:53:22  zionk
  Use only close(). Don't use shutdwon().
  Check if close() returns EINTR

  Revision 1.26  2006/09/08 23:35:15  zionk
  change "buffer overflow" error to "send buffer overflow"

  Revision 1.25  2006/08/24 00:09:59  zionk
  Add global send buffer (10 MB). Fix spurious errors in sendbuffered (wrong
  return values, continuing to next stream segment instead of breaking...)

  Revision 1.24  2006/08/10 22:27:20  zionk
  Fix logging of closed socket

  Revision 1.23  2006/08/08 21:14:07  zionk
  Don't log error in UDP if recvFrom is blocking.

  Revision 1.22  2006/08/04 17:18:41  zionk
  memset addr structure to zero

  Revision 1.21  2006/08/03 23:05:25  zionk
  close socket only if m_socketfd is not -1

  Revision 1.20  2006/07/27 20:51:27  zionk
  Use const string& parameters

  Revision 1.19  2006/07/13 20:05:36  zionk
  Fix SOCK_CLOSED for TLS too

  Revision 1.18  2006/07/13 19:57:25  lennyp
  when socket is closed, return SOCK_CLOSED instead of SOCK_ERR

  Revision 1.17  2006/07/13 19:50:43  zionk
  Fix compile error

  Revision 1.16  2006/07/13 19:43:03  lennyp
  initialize getsockopt args to fix non-blocking connects

  Revision 1.15  2006/07/12 00:53:48  zionk
  Keep nonblocking connect

  Revision 1.14  2006/07/12 00:37:37  zionk
  Fixed bugs. Blocking connect

  Revision 1.13  2006/07/11 16:52:40  zionk
  Add null-termination to DEBUG output of received message

  Revision 1.12  2006/07/10 22:42:41  zionk
  Add some DB messages and combine DB and LOGL messages into DB_AND_LOGL

  Revision 1.11  2006/06/22 17:59:10  zionk
  call ::shutdown for TLSSocket::close()

  Revision 1.10  2006/06/05 23:02:23  zionk
  Add more info to log message

  Revision 1.9  2006/06/01 18:39:57  zionk
  Added logging for socket close and socket errors

  Revision 1.8  2006/05/30 23:41:16  zionk
  Don't print received messages when there is an error.
  Call ::shutdown in close()

  Revision 1.7  2006/05/12 22:10:45  zionk
  Move utils to util/string_utils

  Revision 1.6  2006/05/03 16:02:37  zionk
  Fix more compilation errors

  Revision 1.5  2006/05/02 19:52:51  zionk
  Don't log error if recvfrom returns 0

  Revision 1.4  2006/03/29 23:48:03  zionk
  Added debug/log message for bind failure

  Revision 1.3  2006/03/11 00:29:36  zionk
  Replaced string.length() >0 with string != ""

  Revision 1.2  2006/03/08 19:45:56  zionk
  Replace inet_ntoa with inet_ntop

  Revision 1.1  2006/03/07 18:12:00  zionk
  Multi-threaded server core

  Revision 1.23  2005/10/28 23:00:57  larsb
  *** empty log message ***

  Revision 1.22  2005/10/21 16:24:21  larsb
  modified configuration

  Revision 1.21  2005/10/20 02:09:12  larsb
  *** empty log message ***

  Revision 1.20  2005/10/20 02:07:34  larsb
  *** empty log message ***

  Revision 1.19  2005/10/19 22:05:18  larsb
  removed memory leak in send buffers

  Revision 1.18  2005/09/15 23:28:43  larsb
  *** empty log message ***

  Revision 1.17  2005/09/08 01:01:25  larsb
  fixed bug: connections could get lost in hashtable in case of collisions

  Revision 1.16  2005/09/06 22:43:08  larsb
  *** empty log message ***

  Revision 1.15  2005/09/02 22:17:11  larsb
  clean up and polishing

  Revision 1.14  2005/08/17 22:34:20  larsb
  changed configuratioN: spilt into core_config.h (servercore/) and config.h/.cc (custom/)

  Revision 1.13  2005/08/17 00:06:32  larsb
  *** empty log message ***

  Revision 1.12  2005/08/15 16:19:43  larsb
  *** empty log message ***

  Revision 1.11  2005/08/13 00:40:43  larsb
  *** empty log message ***

  Revision 1.10  2005/08/08 18:55:57  larsb
  reoriganisation of command line interface (adminprocessor) and global variables

  Revision 1.9  2005/08/05 19:34:55  larsb
  *** empty log message ***

  Revision 1.8  2005/07/27 00:48:04  larsb
  *** empty log message ***

  Revision 1.7  2005/07/23 00:16:00  larsb
  *** empty log message ***

  Revision 1.6  2005/07/22 01:59:37  larsb
  *** empty log message ***

  Revision 1.5  2005/07/21 18:50:38  larsb
  *** empty log message ***

  Revision 1.4  2005/07/16 00:56:02  larsb
  *** empty log message ***

  Revision 1.3  2005/07/14 22:11:11  larsb
  bugfix: accepted socket did not inherit O_NONBLOCK from parent

  Revision 1.2  2005/07/13 22:38:18  larsb
  upper bound for TCP/TLS connections removed
  added copy constructor for SocketConnection class

  Revision 1.1  2005/07/13 00:34:29  larsb
  core files for servers

  Revision 1.8  2005/07/09 00:18:43  larsb
  *** empty log message ***

  Revision 1.7  2005/07/08 00:18:04  larsb
  *** empty log message ***

  Revision 1.6  2005/07/04 23:18:29  larsb
  replaced old sip parsing; TCP working with MSN client

  Revision 1.5  2005/06/29 01:10:38  larsb
  bugfixes: TCP working

  Revision 1.4  2005/06/28 17:02:48  larsb
  added SSL/TLS support (see README.ssl)
  did some cleanup

  Revision 1.3  2005/06/22 16:27:44  larsb
  *** empty log message ***

  Revision 1.2  2005/06/21 00:13:16  larsb
  *** empty log message ***

  Revision 1.1  2005/06/20 19:39:33  larsb
  new structure for socket and message processing

*/
