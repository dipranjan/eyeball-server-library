#ifndef PASSWORD_MESSAGE_H
#define PASSWORD_MESSAGE_H

#include "QueryMessage.h"

using std::string;

class PasswordMessage : public QueryMessage
{
public:
  PasswordMessage();
  ~PasswordMessage();

  virtual int deserialize(const string& buf);
  virtual void serialize(string& buf) const;

  virtual u_int16_t payload_length(void) const;

#ifndef _WIN32_WCE
        friend ostream& operator<<(ostream& s, const PasswordMessage& v);
#endif

public:
  // all parameters are publicly accessed
  string m_sName;
  string m_sPassword;
};

#endif//PASSWORD_MESSAGE_H
