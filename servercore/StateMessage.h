#ifndef STATE_MESSAGE_H
#define STATE_MESSAGE_H

#include "QueryMessage.h"

class StateMessage : public QueryMessage
{
public:
  StateMessage();
  virtual ~StateMessage();

  virtual int deserialize(const string& buf);
  virtual void serialize(string& buf) const;

  virtual u_int16_t payload_length(void) const;

  void setAddress(const string& sAddress);
  string& getAddress(string& sAddress) const;
  void setProxyAddress(const string& sAddress);
  string& getProxyAddress(string& sAddress) const;
  void setForwardAddress(const string& sAddress);
  string& getForwardAddress(string& sAddress) const;

  void setLoginTimeAndExpiry(int iExpirySeconds);
  bool isExpired() const; 

#ifndef _WIN32_WCE
        friend ostream& operator<<(ostream& s, const StateMessage& v);
#endif

public:
  // all parameters are publicly accessed
  string m_sName;
  string m_sDomain;
  u_int32_t m_u32Addr;
  u_int16_t m_u16Port;
  u_int8_t  m_u8Protocol;

  u_int32_t m_u32ProxyAddr;
  u_int16_t m_u16ProxyPort;
  u_int8_t  m_u8ProxyProtocol;  

  u_int32_t m_u32ForwardAddr;
  u_int16_t m_u16ForwardPort;
  u_int8_t  m_u8ForwardProtocol;

  u_int32_t m_u32Login;
  u_int32_t m_u32Expiry;
};

#endif//STATE_MESSAGE_H
