// XmppContactsMessage.cc
// Interface for the XMPPContactsMessage class

#include "XMPPContactsMessage.h"
#include "debug.h"

XMPPContactsMessage::XMPPContactsMessage()
: QueryMessage(EYEBALL_SERVER_XMPP_CONTACTS),
  m_u8Action(EACTION_GET_ALL)
{
	DB_ENTER(XMPPContactsMessage::XMPPContactsMessage);

}

XMPPContactsMessage::~XMPPContactsMessage()
{
	DB_ENTER(XMPPContactsMessage::~XMPPContactsMessage);

}

int XMPPContactsMessage::deserialize(const string& buf)
{
  DB_ENTER(XMPPContactsMessage::deserialize);

  m_Contacts.clear();

  int iOffset = 0;

  iOffset += QueryMessage::deserialize(buf);

  if (!DeserializeString16(m_sName, buf, iOffset)) return -1;
  if (!Deserialize8(m_u8Action, buf, iOffset)) return -1;

  u_int16_t iSize = 0;

  // deserialize message contents
  if (!Deserialize16(iSize, buf, iOffset)) return -1;

  for (u_int16_t i = 0; i < iSize; i++) {
    XMPPContactInfo contact;
    if (!DeserializeString16(contact.m_sBareJid, buf, iOffset)) return -1;
    if (!DeserializeString16(contact.m_sDisplayName, buf, iOffset)) return -1;
    if (!DeserializeString16(contact.m_sGroup, buf, iOffset)) return -1;
    if (!DeserializeString16(contact.m_sSubscriptionState, buf, iOffset)) return -1;

    u_int8_t i8Pending;
    if (!Deserialize8(i8Pending, buf, iOffset)) return -1;
    contact.m_bPendingIn = (i8Pending != 0);

    if (!Deserialize8(i8Pending, buf, iOffset)) return -1;
    contact.m_bPendingOut = (i8Pending != 0);

    m_Contacts.push_back(contact);
  }

  return iOffset;
}

void XMPPContactsMessage::serialize(string& buf) const
{
  DB_ENTER(XMPPContactsMessage::serialize);

  QueryMessage::serialize(buf);

  // serialize message contents
  SerializeString16(m_sName, buf);
  Serialize8(m_u8Action, buf);

  u_int16_t iSize = m_Contacts.size();
  Serialize16(iSize, buf);

  for (u_int16_t i = 0; i < iSize; i++) {
    const XMPPContactInfo& contact = m_Contacts[i];
    SerializeString16(contact.m_sBareJid, buf);
    SerializeString16(contact.m_sDisplayName, buf);
    SerializeString16(contact.m_sGroup, buf);
    SerializeString16(contact.m_sSubscriptionState, buf);
    Serialize8(contact.m_bPendingIn ? 1 : 0, buf);
    Serialize8(contact.m_bPendingOut ? 1 : 0, buf);
  }
}

u_int16_t XMPPContactsMessage::payload_length(void) const
{
  DB_ENTER(XMPPContactsMessage::payload_length);

  u_int16_t iLength = QueryMessage::payload_length() + 
                      m_sName.size() + sizeof(u_int16_t) + 1 + 
                      sizeof(u_int16_t);

  int iNumContacts = m_Contacts.size();
  for (int i = 0; i < iNumContacts; i++) {
    const XMPPContactInfo& contactInfo = m_Contacts[i];
    iLength += contactInfo.m_sBareJid.size() + sizeof(u_int16_t) +
               contactInfo.m_sDisplayName.size() + sizeof(u_int16_t) +
               contactInfo.m_sGroup.size() + sizeof(u_int16_t) +
               contactInfo.m_sSubscriptionState.size() + sizeof(u_int16_t) + 2;
  }
  return iLength; 
}

unsigned int XMPPContactsMessage::getNumContacts() const
{
  DB_ENTER(XMPPContactsMessage::getNumContacts);

  return m_Contacts.size();
}

XMPPContactInfo XMPPContactsMessage::getContact(int iIndex)
{
  DB_ENTER(XMPPContactsMessage::getContact);

  if ((iIndex < 0) || (iIndex >= (int)m_Contacts.size())) {
    XMPPContactInfo blank;
    return blank;
  }
  else {
    return m_Contacts[iIndex];
  }
}

void XMPPContactsMessage::addContact(const XMPPContactInfo& contact)
{
  DB_ENTER(XMPPContactsMessage::addContact);

  m_Contacts.push_back(contact); 
}

ostream& operator<<(ostream& s, const XMPPContactsMessage& v)
{
        int iSize = v.m_Contacts.size();

        s << (QueryMessage)v
                << " " << v.m_sName << " " << (int)v.m_u8Action << " " << iSize;

        for (int i = 0; i < iSize; i++) {
		s << "\n" << v.m_Contacts[i];
        }

        return s;
}



