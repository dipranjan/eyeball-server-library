

#include <log.h>
#include <debug.h>
#include "simple_crypt.h"

//#include <iostream>
//#include <iomanip>
//#include <fstream>

#include "MySQLBase.h"

#include "config.h"
#include <sys/time.h>



using namespace std;
using namespace mysqlpp;




MySQLBase::MySQLBase()
: m_pCon(NULL),
  m_iPort(0),
  m_iCurrHostIndex(-1),
  m_iLastReconnectAttempt(0)
{
  DB_ENTER(MySQLBase::MySQLBase);

  pthread_mutex_init(&m_ReconnectMutex, NULL);
}// MySQLBase::MySQLBase()



MySQLBase::~MySQLBase()
{
  DB_ENTER(MySQLBase::~MySQLBase);

  if(m_pCon != NULL) {
    Close();
  }

  pthread_mutex_destroy(&m_ReconnectMutex);
}// MySQLBase::~MySQLBase()


int MySQLBase::Open(const string& sDBName, vector<string>& sHosts, int iPort, 
const string& sUser, const string& sPasswordFile)
{
  DB_ENTER(MySQLBase::Open);

  int result;
  
  m_sDBName = sDBName;
  m_sHosts = sHosts;
  m_iPort = iPort;
  m_sUser = sUser;

  if ((sPasswordFile != "")&&(sUser != ""))
    {
      m_sPassword = Crypt::get_passwd(sPasswordFile, sUser);
      
      if( m_sPassword == "" )
	{
	  DB_AND_LOG("Cannot get password for user: \"" << sUser << "\" from file: " << sPasswordFile);
	  return -1;
	}
    }

  result = _reconnect();

  if (result) {
    return 1;
  }
  else {
    return -1;
  }
  
}// int MySQLBase::Open(const string& sDBName, vector<string>& sHosts, int iPort, 
//const string& sUser, const string& sPasswordFile)

void MySQLBase::Close()
{
  DB_ENTER(MySQLBase::Close);

  pthread_mutex_lock(&m_ReconnectMutex);
  if(m_pCon != NULL) {
    if (m_pCon->connected()) {
      m_pCon->close();
    }
    delete m_pCon;
    m_pCon = NULL;
  }
  pthread_mutex_unlock(&m_ReconnectMutex);
} // void MySQLBase::Close()

bool MySQLBase::_isConnected()
{
  DB_ENTER(MySQLBase::_isConnected);
 
  return dbWorking;
}

bool MySQLBase::_reconnect()
{
  DB_ENTER(MySQLBase::_reconnect);

  pthread_mutex_lock(&m_ReconnectMutex);

  int iCurrTime = time(NULL);

  if (m_pCon != NULL) {
    if (m_pCon->connected()) {
      if (m_pCon->ping()==0) {
        //Reconnected
        pthread_mutex_unlock(&m_ReconnectMutex);
        return true;
      }
    }

    if ((m_iLastReconnectAttempt + 10 - iCurrTime) > 0) {
      //Skip reconnect
      pthread_mutex_unlock(&m_ReconnectMutex);
      return false;
    }
  }

  m_iLastReconnectAttempt = iCurrTime;

  int iNumHosts = m_sHosts.size();
  
  int iHostIndex = 0;
  if (iNumHosts > 1)
    {
      struct timeval tv;
      gettimeofday(&tv, NULL);
      srand(rand() ^ tv.tv_sec ^ tv.tv_usec);
      iHostIndex = rand() % iNumHosts;
      if (iHostIndex == m_iCurrHostIndex) {
        iHostIndex = (iHostIndex + 1) % iNumHosts;
      }
    }

  bool found = false;
  for (int i = 0; (i < iNumHosts) && (found==false); i++)
    {
	  try
	    {
	      if(m_pCon!=NULL)
              {
                if (m_pCon->connected()) {
                  m_pCon->close();
                }
                delete m_pCon;
                m_pCon = NULL;
              }
	      m_pCon = new Connection(true);

	      m_pCon->connect(m_sDBName.c_str(),               // database name
			      m_sHosts[iHostIndex].c_str(),    // host name
			      m_sUser.c_str(),                 // user name
			      m_sPassword.c_str(),             // password
			      m_iPort,                         // database port
			      0,                               // my_bool compress default
			      MYSQL_DEFAULT_TIMEOUT,           // connect_timeout default in seconds
			      NULL,                            // *socket_name default
			      0);                              // client_flag default

	      found = true;
	      
	      if (m_pCon->connected()==false)
	      {
	        DB_AND_LOG("database connection to "<<m_sHosts[iHostIndex]<<" cannot be established");
	      
	        found = false;
	      }
	    }
	  catch(...)
	    {
	      DB_AND_LOG("database connection to "<<m_sHosts[iHostIndex]<<" caused an exception");
	      
	      found = false;
	    }

          if (!found) {
            if (++iHostIndex == iNumHosts) {
              iHostIndex = 0;
            }
          }
    }
  
  if (found)
    {
      m_iCurrHostIndex = iHostIndex;
      DB_AND_LOG("connected to database (" << m_sHosts[iHostIndex] << ")");
    }
  else
    {
      m_iCurrHostIndex = -1;
      DB_AND_LOG("connection to database cannot be established");
    }

  pthread_mutex_unlock(&m_ReconnectMutex);

  return found;

}// bool MySQLBase::_reconnect()



int MySQLBase::_putTemplate(const string& sQuery, bool bRetry)
{
  DB_ENTER(MySQLBase::_putTemplate);
  dbWorking  = true;

  pthread_mutex_lock(&m_ReconnectMutex);
  if ((m_pCon == NULL) || (!m_pCon->connected())) {
    pthread_mutex_unlock(&m_ReconnectMutex);
    DB_AND_LOGL(3, "db "<<sQuery<<": no database connection");

    if (!bRetry) {
      dbWorking = false;
      return -1;
    }

    _reconnect();

    pthread_mutex_lock(&m_ReconnectMutex);

    if ((m_pCon == NULL) || (!m_pCon->connected())) {
      pthread_mutex_unlock(&m_ReconnectMutex);
      dbWorking = false;
      return -1;
    }
  }

  int iResult = -1;

  try
    {
      DB("Query: " << sQuery.c_str());
      ResNSel res;
      {
        Query query = m_pCon->query();
        //query<<sQuery;

        res = query.execute(sQuery.c_str());
      }
    
      if(res.success) {
        if (0 == res.rows)  // zero rows affected
          iResult = 0;
        else
          iResult = 1;
      } 
    }
  catch(BadQuery er)
    {
#ifdef MYSQL4
      DB_AND_LOGL(2,"db bad query "<<sQuery<<" error: "<<er.what());
#else
      DB_AND_LOGL(2,"db bad query "<<sQuery<<" error: "<<er.error);
#endif
    }
  catch(BadConversion er)
    {
#ifdef MYSQL4
      DB_AND_LOGL(2,"db bad conversion "<<sQuery<<" error: "<<er.what());
#else
      DB_AND_LOGL(2,"db bad conversion "<<sQuery<<" error");
#endif
    }	
  catch(...)
    {
#ifdef MYSQL4
      DB_AND_LOGL(2,"db "<<sQuery<<" error");
#else
      DB_AND_LOGL(2,"db "<<sQuery<<" error");
#endif
    }

  if (iResult == -1) {
    if (m_pCon->ping()!=0)
    {
      pthread_mutex_unlock(&m_ReconnectMutex);
      DB_AND_LOGL(3, "db "<<sQuery<<": database connection lost");

      bool bReconnected = _reconnect();
      if (bReconnected && bRetry) {
        return _putTemplate(sQuery, false);
      }
      dbWorking = false;
      return -1;
    }
    else {
      iResult = 0;
    }
  }  

  pthread_mutex_unlock(&m_ReconnectMutex);
  return iResult;
}// int MySQLBase::_putTemplate(const string& sQuery, bool bRetry)



int MySQLBase::_getTemplate(const string& sQuery, Result& res, bool bRetry)
{
  DB_ENTER(MySQLBase::_getTemplate);
  int iResult = -1;
  dbWorking = true;

  pthread_mutex_lock(&m_ReconnectMutex);
  if ((m_pCon == NULL) || (!m_pCon->connected())) {
    pthread_mutex_unlock(&m_ReconnectMutex);
    DB_AND_LOGL(3, "db "<<sQuery<<": no database connection");

    if (!bRetry) {
      dbWorking = false;
      return -1;
    }

    _reconnect();

    pthread_mutex_lock(&m_ReconnectMutex);

    if ((m_pCon == NULL) || (!m_pCon->connected())) {
      pthread_mutex_unlock(&m_ReconnectMutex);
      dbWorking = false;
      return -1;
    }
  }

  try
    {
      DB("Query: " << sQuery.c_str());
      {
        Query query = m_pCon->query();
        //query << sQuery;
        res = query.store(sQuery.c_str());
        if (query.success()) {
          iResult = res.size();
        }
      }
    }
  catch(BadQuery er)
    {
#ifdef MYSQL4
      DB_AND_LOGL(2,"db bad query "<<sQuery<<" error: "<<er.what());
#else
      DB_AND_LOGL(2,"db bad query "<<sQuery<<" error: "<<er.error);
#endif
    }
  catch(BadConversion er)
    {
#ifdef MYSQL4
      DB_AND_LOGL(2,"db bad conversion "<<sQuery<<" error: "<<er.what());
#else
      DB_AND_LOGL(2,"db bad conversion "<<sQuery<<" error");
#endif
    }
  catch(...)
    {
#ifdef MYSQL4
      DB_AND_LOGL(2,"db "<<sQuery<<" error");
#else
      DB_AND_LOGL(2,"db "<<sQuery<<" error");
#endif
    }

  if (iResult == -1) {
    if (m_pCon->ping()!=0)
    {
      pthread_mutex_unlock(&m_ReconnectMutex);
      DB_AND_LOGL(3, "db "<<sQuery<<": database connection lost");

      bool bReconnected = _reconnect();
      if (bReconnected && bRetry) {
        return _getTemplate(sQuery, res, false);
      }
      dbWorking = false;
      return -1;
    }
  }

  pthread_mutex_unlock(&m_ReconnectMutex);
  DB("finished: Result Size " << iResult);

  return iResult;

}// int MySQLBase::_getTemplate(const string& sQuery, Result& res, bRetry)

string MySQLBase::escape(const string& sIn)
{
  int iLen = sIn.size();
  string sOut;
  sOut.resize(iLen << 1);
  const char* szIn  = sIn.data();
  char* szOut = (char*)sOut.data();

  int iOutIndex = 0;
  for (int i = 0; i < iLen; i++) {
    const char& ch = szIn[i];
    switch (ch) {
    case '\0':
      szOut[iOutIndex++] = '\\';
      szOut[iOutIndex++] = '0';
      break;
    case '\n':
      szOut[iOutIndex++] = '\\';
      szOut[iOutIndex++] = 'n';
      break;
    case '\r':
      szOut[iOutIndex++] = '\\';
      szOut[iOutIndex++] = 'r';
      break;
    case '\\':
      szOut[iOutIndex++] = '\\';
      szOut[iOutIndex++] = '\\';
      break;
    case '\"':
      szOut[iOutIndex++] = '\\';
      szOut[iOutIndex++] = '\"';
      break;
    case '\'':
      szOut[iOutIndex++] = '\\';
      szOut[iOutIndex++] = '\'';
      break;
    case '\032':
      szOut[iOutIndex++] = '\\';
      szOut[iOutIndex++] = 'Z';
      break;
    default:
      szOut[iOutIndex++] = ch;
    }
  }

  sOut.resize(iOutIndex);

  return sOut;
}

