//DBConfigFile.cc
//Implemenation of Config File class

#include "DBConfigFile.h"

#include <iostream>
#include <unistd.h>
#include "debug.h"
#include "string_utils.h"
#include <stdlib.h>
#include <string.h>

using namespace std;
	
//Constructor
DBConfigFile::DBConfigFile()
{
}

//Destructor
DBConfigFile::~DBConfigFile()
{
}

//Read File
//Returns true if successful
bool DBConfigFile::ReadFile(const char* cFileName)
{
	FILE* fp = NULL;

	const char* myFileName = cFileName;
	if (myFileName == NULL) {
		myFileName = "NULL";
	}
	else {	
		fp = fopen(myFileName, "rb");
	}

	if (fp == NULL) {	
                cerr << "Error opening config file for reading: " << myFileName << endl;
		return false;
	}
	else {
		PreParseProcessing();

		if (Parse(fp)) {		
			if (PostParseProcessing()) {
				if (CheckConfiguration()) {
					fclose(fp);
					return true;
				}
				else {
					cerr << "Error found while checking configuration\n";
				}
			}
			else {
				cerr << "Error found during post-parse processing\n";
			}
		}
		else {
			cerr << "Error while parsing\n";
		}
		fclose(fp);
		return false;
	}	
}

//Set Default Configuration
void DBConfigFile::SetDefaultConfiguration()
{
        m_sDBName = "";
        m_sDBUser = "";
        m_sDBHosts.clear();
        m_iDBPort = 3306;
	m_sPassFile = "";

        m_sLogDBName = "";
        m_sLogDBUser = "";
        m_sLogDBHosts.clear();
        m_iLogDBPort = 3306;
}

//! Pre-Parse Processing
//! Expected to call SetDefaultConfiguration()
void DBConfigFile::PreParseProcessing()
{
	SetDefaultConfiguration();
}

//Parse
//Precondition: file pointer is opened for reading
//Returns true if successful
bool DBConfigFile::Parse(FILE* fp)
{
        #define HEADER_LEN              50
        #define HEADER_VALUE_LEN        500
        #define COMMENT                 '#'
        #define SPACE                   ' '
        #define EQUAL                   '='
        #define TAB                     '\t'

        char cHeader[HEADER_LEN];
        char cHeaderValue[HEADER_VALUE_LEN];

        char ch;

        while(!feof(fp))
        {
                ch = getc(fp);

                // removing comments
                if(ch == COMMENT)
                {
                        do
                        {
                                ch = getc(fp);
                        } while(ch != '\n');
                }
                else if(ch == '\n' || ch == '\r')
                {
                        ;
                }
                else
                {
			//Check Header
                        int _index = 0;
                        bool bComment = false;

                        while((ch != EQUAL && ch != '\r' && ch != '\n' ) && (_index < (HEADER_LEN-1)) && !feof(fp))
                        {
                                if(ch == COMMENT)
                                {
                                        cout<<"comment\n";
                                        bComment = true;
                                }
                                if(ch == SPACE || ch == TAB)
                                {
                                        ch = getc(fp);
                                        continue;
                                }
                                cHeader[_index++] = tolower(ch);
                                ch = getc(fp);
                        }

			cHeader[_index] = '\0';
			int iHeaderLen = _index;	

                        DB("HEADER = "<<cHeader<<":"<<iHeaderLen<<endl);

                        // value part
			if (iHeaderLen == 0) {
				//Empty line
			}
                        if ((ch == '\r') || (ch == '\n'))
                        {
                                DB("No = found\n");
                        }
                        else if(ch == EQUAL && bComment == false)
                        {
                                ch = getc(fp);
                                int _index = 0;

                                while(ch != '\r' && ch != '\n' && (_index < (HEADER_VALUE_LEN - 1)) && !feof(fp))
                                {
                                        if(ch == COMMENT)
                                        {
                                                DB("comment after value\n");

                                                while(ch != '\r' && ch != '\n' && !feof(fp))
                                                        ch = getc(fp);
                                                break;
                                        }
                                        if(ch == SPACE || ch == TAB)
                                        {
                                                ch = getc(fp);
                                                continue;
                                        }
                                        cHeaderValue[_index++] = ch;
                                        ch = getc(fp);
                                }

				cHeaderValue[_index] = '\0';
				int iValueLen = _index;

                                if(iValueLen == 0)
                                {
                                	cerr << cHeader << " has empty value\n";
					return false;
                                }
				else {
                                	DB("VALUE = "<<cHeaderValue<<":"<<iValueLen<<endl);

					if (!ProcessHeaderValuePair(cHeader, cHeaderValue)) {
                        //cout << "Warning: Skipping unrecognized header: " << cHeader << endl;
					}
				}
                        }
                }
        }

        return true;
}

//Post-Parse Processing
//Returns true if successful
bool DBConfigFile::PostParseProcessing()
{
        if((m_iDBPort <= 0) || (m_iDBPort >= 65536))
        {
                cout <<"Warning: DATABASE_PORT " << m_iDBPort << " is invalid" << endl;
                cout <<"DATABASE_PORT is set to 3306\n";
                m_iDBPort = 3306;
        }

        if((m_iLogDBPort <= 0) || (m_iLogDBPort >= 65536))
        {
                cout <<"Warning: LOG_DATABASE_PORT " << m_iLogDBPort << " is invalid" << endl;
                cout <<"LOG_DATABASE_PORT is set to 3306\n";
                m_iLogDBPort = 3306;
        }

	return true;
}

//Check Configuration
//Returns true if no errors
bool DBConfigFile::CheckConfiguration()
{
	bool bValid = true;

        if((m_iDBPort <= 0) || (m_iDBPort >= 65536))
        {
                cerr <<"Error in DATABASE_PORT" <<endl;
                cout <<"DATABASE_PORT is set to " << m_iDBPort << endl;
                bValid = false;
        }

        if((m_iLogDBPort <= 0) || (m_iLogDBPort >= 65536))
        {
                cerr <<"Error in LOG_DATABASE_PORT" <<endl;
                cout <<"LOG_DATABASE_PORT is set to " << m_iLogDBPort << endl;
                bValid = false;
        }

	return bValid;
}

//Process Header/Value Pair
//Precondition: Header is lowercase. cHeader and cValue at least one character long
//Returns true if header found and handled, even if syntax errors are found
bool DBConfigFile::ProcessHeaderValuePair(const char* cHeader, const char* cValue)
{
	int iIntValue = strtol(cValue, NULL, 0);

	switch (cHeader[0]) {
	case 'd':
		{
                        if(!strcmp(cHeader,"database_name"))
                        {
                                m_sDBName = cValue;
                                return true;
                        }
                        else if(!strcmp(cHeader,"database_host"))
                        {
                                m_sDBHosts.reserve(m_sDBHosts.size() + 1);
                                m_sDBHosts.push_back(cValue);
                                return true;
                        }
                        else if(!strcmp(cHeader,"database_port"))
                        {
                                m_iDBPort = iIntValue;
                                return true;
                        }
                        else if(!strcmp(cHeader,"database_user"))
                        {
                                m_sDBUser = cValue;
                                return true;
                        }
			break;
		}
	case 'l':
		{
                        if(!strcmp(cHeader,"log_database_name"))
                        {
                                m_sLogDBName = cValue;
                                return true;
                        }
                        else if(!strcmp(cHeader,"log_database_host"))
                        {
                                m_sLogDBHosts.reserve(m_sLogDBHosts.size() + 1);
                                m_sLogDBHosts.push_back(cValue);
                                return true;
                        }
                        else if(!strcmp(cHeader,"log_database_port"))
                        {
                                m_iLogDBPort = iIntValue;
                                return true;
                        }
                        else if(!strcmp(cHeader,"log_database_user"))
                        {
                                m_sLogDBUser = cValue;
                                return true;
                        }
			break;
		}
	case 'p':
		{
			if(!strcmp(cHeader,"password_file"))
			{
				m_sPassFile = cValue;
				return true;
			}
			break;
		}
	} //switch

	return false;
}

string DBConfigFile::getSettings()
{
	string sSettings;
	sSettings.reserve(1024);

        string sYes = "Y";
        string sNo = "N";
        string sEndl = "\n";

        sSettings += (string)"Database Name: " + m_sDBName + sEndl;
        for (unsigned int i = 0; i < m_sDBHosts.size(); i++) {
                sSettings += (string)"Database Host: " + m_sDBHosts[i] + sEndl;
        }
        sSettings += (string)"Database Port: " + itos(m_iDBPort) + sEndl;
        sSettings += (string)"Database User: " + m_sDBUser + sEndl;
        sSettings += (string)"Password File: " + m_sPassFile + sEndl;

        sSettings += (string)"Logging Database Name: " + m_sLogDBName + sEndl;
        for (unsigned int i = 0; i < m_sLogDBHosts.size(); i++) {
                sSettings += (string)"Logging Database Host: " + m_sLogDBHosts[i] + sEndl;
        }
        sSettings += (string)"Logging Database Port: " + itos(m_iLogDBPort) + sEndl;
        sSettings += (string)"Logging Database User: " + m_sLogDBUser + sEndl;

	return sSettings;
}

