// XmppContactsMessage.h
// Interface for the XMPPContactsMessage class

#ifndef XMPP_CONTACTS_MESSAGE_H
#define XMPP_CONTACTS_MESSAGE_H

#include "QueryMessage.h"
#include "XMPPContactInfo.h"

#include <vector>
using std::vector;

class XMPPContactsMessage : public QueryMessage
{
public:
  XMPPContactsMessage();
  ~XMPPContactsMessage();

  virtual int deserialize(const string& buf);
  virtual void serialize(string& buf) const;

  virtual u_int16_t payload_length(void) const;

  unsigned int getNumContacts() const;

  XMPPContactInfo getContact(int iIndex);
  void addContact(const XMPPContactInfo& contact);

#ifndef _WIN32_WCE
  friend ostream& operator<<(ostream& s, const XMPPContactsMessage& v);
#endif

public:
  string m_sName;

  enum EAction {
    EACTION_GET_ALL,
    EACTION_GET_ONE,
    EACTION_USER_SUBSCRIBE,
    EACTION_CONTACT_SUBSCRIBE,
    EACTION_USER_UNSUBSCRIBE,
    EACTION_CONTACT_UNSUBSCRIBE,
    EACTION_USER_SUBSCRIBED,
    EACTION_CONTACT_SUBSCRIBED,
    EACTION_USER_UNSUBSCRIBED,
    EACTION_CONTACT_UNSUBSCRIBED,
    EACTION_UPDATE_ONE,
    EACTION_REMOVE_ONE,
    EACTION_STATE_CHANGED
  };

  //! Possible GET actions:
  //!   "get all",           "get one"
  //!   "user subscribe",    "contact subscribe",
  //!   "user unsubscribe",  "contact unsubscribe",
  //!   "user subscribed",   "contact subscribed",
  //!   "user unsubscribed", "contact unsubscribed",
  //! Possible PUT actions:
  //!   "update one",        "remove one"
  u_int8_t m_u8Action;
  vector<XMPPContactInfo> m_Contacts;

};

#endif//XMPP_CONTACTS_MESSAGE_H
