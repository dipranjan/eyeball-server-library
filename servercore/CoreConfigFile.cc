//CoreConfigFile.cc
//Implemenation of Core Config File class

#include "CoreConfigFile.h"
#include "core_globals.h"

#include <iostream>
#include <unistd.h>
#include "debug.h"
#include "string_utils.h"
#include "checkip.h"
#include <string.h>
#include <limits.h>
#include <stdlib.h>

using namespace std;
	
//Constructor
CCoreConfigFile::CCoreConfigFile()
{
	DB_ENTER(CCoreConfigFile::CCoreConfigFile);
}

//Destructor
CCoreConfigFile::~CCoreConfigFile()
{
	DB_ENTER(CCoreConfigFile::~CCoreConfigFile);
}

//Set Default Configuration
void CCoreConfigFile::SetDefaultConfiguration()
{
	DB_ENTER(CCoreConfigFile::SetDefaultConfiguration);

	DBConfigFile::SetDefaultConfiguration();

	m_sIP = "";
	m_sPrivateIP = "";
	m_sDomain = "";

	m_sHostName = "";

	// 10k send buffer (for wouldblock events) for sockets, for TCP/UDP
	m_iSendBufferSize = 1024*10;

	m_iRecvBufferSize = 2*MAXMSGSIZE;

	// timeout for TCP connections
	m_iTCPConnectionTimeout = TCP_CONNECTION_TIMEOUT;

	m_iTCPConnections = TCP_CONNECTIONS;
	m_iTLSConnections = TCP_CONNECTIONS;

	m_iTelnetAdminPort = ADMIN_PORT;
	m_iForwardMessageUdpPort = FORWARD_MESSAGE_PORT;
	m_iForwardMessageTcpPort = FORWARD_MESSAGE_PORT;
	m_iUdpQueryPort = 0;

	m_bLogEnabled = true;
        m_iLogMaxFileSize = 10000000;
        m_iLogMaxFileCount = 100;

	m_sLogFileName = "/var/log/servercoremt.log";
	m_sPidFileName = "/var/run/servercoremt.pid";
	m_sOutFileName = "/var/log/servercoremt.out";

	m_sCertFile = "";
	m_sCertKeyFile = "";
	m_sCertUser = "";
    m_iTolerableCpuUsage = 90;
    m_iCpuUsageCheckTimeoutSec = 60*60;

	m_bDNSCache = true;

        m_iDBUpdateStatsInterval = 15; //15 minutes

        m_sLicenseName = "";
        m_sLicenseCertFile = "";
        m_sEyeballCertFile = "";

	m_iNumThreads = 16;

    m_iMessageQueueSize = 300;
}

//Post-Parse Processing
//Returns true if successful
bool CCoreConfigFile::PostParseProcessing()
{
	DB_ENTER(CCoreConfigFile::PostParseProcessing);

	DBConfigFile::PostParseProcessing();

	if (m_sIP != "")
	{
		char buf[256];
		if (gethostname(buf, 255) < 0)
		{
			m_sHostName = "";
		}
		else {
			m_sHostName = buf;
		}
	}

	if (m_sPrivateIP == "") {
		m_sPrivateIP = m_sIP;
	}

	if(m_iTCPConnections == 0) {
		m_iTCPConnections = TCP_CONNECTIONS;
	}

	if(m_iTLSConnections == 0) {
		m_iTLSConnections = TCP_CONNECTIONS;
	}

	if((m_iTelnetAdminPort <= 0) || (m_iTelnetAdminPort >= 65536))
	{
		cout <<"Warning: ADMIN_PORT " << m_iTelnetAdminPort << " is invalid" <<endl;
		cout <<"ADMIN_PORT is set to " << ADMIN_PORT << endl;
		m_iTelnetAdminPort = ADMIN_PORT;
	}

	if((m_iForwardMessageUdpPort <= 0) || (m_iForwardMessageUdpPort >= 65536))
	{
		cout <<"Warning: FORWARD_UDP_PORT " << m_iForwardMessageUdpPort << " is invalid" <<endl;
		cout <<"FORWARD_UDP_PORT is set to " << FORWARD_MESSAGE_PORT << endl;
		m_iForwardMessageUdpPort = FORWARD_MESSAGE_PORT;
	}

	if((m_iForwardMessageTcpPort <= 0) || (m_iForwardMessageTcpPort >= 65536))
	{
		cout <<"Warning: FORWARD_TCP_PORT " << m_iForwardMessageTcpPort << " is invalid" <<endl;
		cout <<"FORWARD_TCP_PORT is set to " << FORWARD_MESSAGE_PORT << endl;
		m_iForwardMessageTcpPort = FORWARD_MESSAGE_PORT;
	}

	if((m_iUdpQueryPort < 0) || (m_iUdpQueryPort >= 65536))
	{
		cout <<"Warning: UDP_QUERY_PORT " << m_iUdpQueryPort << " is invalid" <<endl;
		cout <<"UDP_QUERY_PORT will be chosen automatically\n";
		m_iUdpQueryPort = 0;
	}

        if(m_iDBUpdateStatsInterval <= 0)
        {
                cout <<"Warning: LOGGING_INTERVAL" << m_iDBUpdateStatsInterval << " is invalid" << endl;
                cout <<"LOGGING_INTERVAL is set to 15\n";
                m_iDBUpdateStatsInterval = 15;
        }

	if((m_iNumThreads < 1) || (m_iNumThreads > 100)) {
		cout <<"Warning: NUM_THREADS " << m_iNumThreads << " is invalid" << endl;
		int iNewValue = (m_iNumThreads < 1) ? 1 : 100;
		cout <<"NUM_THREADS is set to " << iNewValue << endl;
		m_iNumThreads = iNewValue;
	}

    if(m_iMessageQueueSize < 1 || m_iMessageQueueSize > 10000)
    {
        cout<<"Warning: message_queue_size must have a positive value and less or equal to 10000"<<endl;
        cout<<"message_queue_size is set to 300"<<endl;
        m_iMessageQueueSize = 300;
    }

	return true;
}

//Check Configuration
//Returns true if no errors
bool CCoreConfigFile::CheckConfiguration()
{
	DB_ENTER(CCoreConfigFile::CheckConfiguration);

	bool bValid = DBConfigFile::CheckConfiguration();

        if(m_sIP == "")
        {
                cerr<<"Error: BIND_ADDRESS is missing in configuration file\n";
                bValid = false;
        }
	else if (!isIPAddress(m_sIP)&&m_sIP!="any")
	{
		cerr<<"Error: BIND_ADDRESS (" << m_sIP << ") is not a valid IP address\n";
		bValid = false;
	}

        if(m_sPrivateIP == "")
        {
                cerr<<"Error: PRIVATE_ADDRESS is missing in configuration file\n";
                bValid = false;
        }
	else if (!isIPAddress(m_sPrivateIP)&&m_sPrivateIP!="any")
	{
		cerr<<"Error: PRIVATE_ADDRESS (" << m_sPrivateIP << ") is not a valid IP address\n";
		bValid = false;
	}

	if (m_sDomain == "") 
        {
                cerr<<"Error: domain_name is invalid or missing from configuration file\n";
                bValid = false;
        }

	if (m_iSendBufferSize <= 0)
	{
		cerr <<"Error in SENDBUFFER_SIZE"<<endl;
		cerr <<"SENDBUFFER_SIZE is set to "<<m_iSendBufferSize<<endl;
                bValid = false;
	}

	if (m_iRecvBufferSize <= 0)
	{
		cerr <<"Error in RECVBUFFER_SIZE"<<endl;
		cerr <<"RECVBUFFER_SIZE is set to "<<m_iRecvBufferSize<<endl;
                bValid = false;
	}

	if (m_iTCPConnections <= 0)
	{
		cerr <<"Error in TCP_CONNECTIONS"<<endl;
		cerr <<"TCP_CONNECTIONS is set to "<<m_iTCPConnections<<endl;
                bValid = false;
	}

	if (m_iTLSConnections <= 0)
	{
		cerr <<"Error in TLS_CONNECTIONS"<<endl;
		cerr <<"TLS_CONNECTIONS is set to "<<m_iTLSConnections<<endl;
                bValid = false;
	}

	if((m_iTelnetAdminPort <= 0) || (m_iTelnetAdminPort >= 65536))
	{
		cerr <<"Error in ADMIN_PORT" <<endl;
		cerr <<"ADMIN_PORT is set to "<<m_iTelnetAdminPort<<endl;
		bValid = false;
	}

	if((m_iForwardMessageUdpPort <= 0) || (m_iForwardMessageUdpPort >= 65536))
	{
		cerr <<"Error in FORWARD_UDP_PORT" <<endl;
		cerr <<"FORWARD_UDP_PORT is set to "<<m_iForwardMessageUdpPort<<endl;
		bValid = false;
	}

	if((m_iForwardMessageTcpPort <= 0) || (m_iForwardMessageTcpPort >= 65536))
	{
		cerr <<"Error in FORWARD_TCP_PORT" <<endl;
		cerr <<"FORWARD_TCP_PORT is set to "<<m_iForwardMessageTcpPort<<endl;
		bValid = false;
	}
	else {
		if (m_iForwardMessageTcpPort == m_iTelnetAdminPort)
		{
			cerr <<"Error: ADMIN_PORT and FORWARD_TCP_PORT are both " 
			     << m_iForwardMessageTcpPort << endl;
			bValid = false;
		}
	}

	if((m_iUdpQueryPort < 0) || (m_iUdpQueryPort >= 65536))
	{
		cerr <<"Error in UDP_QUERY_PORT" <<endl;
		cerr <<"UDP_QUERY_PORT is set to "<<m_iUdpQueryPort<<endl;
		bValid = false;
	}
	else {
		if (m_iUdpQueryPort == m_iForwardMessageUdpPort)
		{
			cerr <<"Error: FORWARD_UDP_PORT and UDP_QUERY_PORT are both " 
			     << m_iForwardMessageUdpPort << endl;
			bValid = false;
		}
	}

	if(m_bLogEnabled && (m_sLogFileName == ""))
	{
		cerr<<"logging enabled but log file name is empty\n";
		bValid = false;
	}

	if(m_sPidFileName == "")
	{
		cerr<<"pid file name is empty\n";
		bValid = false;
	}
	else {
		if (m_sLogFileName == m_sPidFileName)
		{
			cerr <<"Error: log file name and pid file name are both "
			     <<m_sPidFileName<<endl;
			bValid = false;	
		}
	}

#ifdef DEBUG
	if(m_sOutFileName == "")
	{
		cerr<<"out file name is empty\n";
		bValid = false;
	}
	else {
		if (m_bLogEnabled && (m_sLogFileName == m_sOutFileName)) 
		{
			cerr <<"Error: log file name and out file name are both "
			     <<m_sOutFileName<<endl;
			bValid = false;	
		}

		if (m_sPidFileName == m_sOutFileName) 
		{
			cerr <<"Error: pid file name and out file name are both "
			     <<m_sOutFileName<<endl;
			bValid = false;
		}
	}
#endif

        if(m_iDBUpdateStatsInterval <= 0)
        {
                cerr <<"Error in LOGGING_INTERVAL"<<endl;
                cerr <<"LOGGING_INTERVAL is set to " << m_iDBUpdateStatsInterval << endl;
                bValid = false;
        }

	if((m_iNumThreads < 1) || (m_iNumThreads > 100)) {
		cerr <<"Error in NUM_THREADS"<<endl;
		cerr <<"NUM_THREADS is set to " << m_iNumThreads << endl;
		bValid = false;
	}

	if (m_iLogMaxFileSize <= 0) {
		cerr <<"Error in LOG_MAX_FILE_SIZE"<<endl;
		cerr <<"LOG_MAX_FILE_SIZE is set to " << m_iLogMaxFileSize << endl;
		bValid = false;
	}

	if (m_iLogMaxFileCount <= 0) {
		cerr <<"Error in LOG_MAX_FILE_COUNT"<<endl;
		cerr <<"LOG_MAX_FILE_COUNT is set to " << m_iLogMaxFileCount << endl;
		bValid = false;
	}

	return bValid;
}

//Process Header/Value Pair
//Precondition: Header is lowercase. cHeader and cValue at least one character long
//Returns true if header found and handled, even if syntax errors are found
bool CCoreConfigFile::ProcessHeaderValuePair(const char* cHeader, const char* cValue)
{
	DB_ENTER(CCoreConfigFile::ProcessHeaderValuePair);

	int iIntValue = strtol(cValue, NULL, 0);

	switch (cHeader[0]) {
	case 'a':
		{
			if(!strcmp(cHeader,"admin_port"))
			{
				m_iTelnetAdminPort = iIntValue;
				return true;
			}
			break;
		}
	case 'b':
		{
			if(!strcmp(cHeader,"bind_address"))
                	{
				m_sIP = cValue;
				return true;
			}
			break;
		}
    case 'c':
        {
            if (!strcmp(cHeader,"cpu_usage_check_timeout"))
            {
                //m_iCpuUsageCheckTimeoutSec = max(15,iIntValue);
                m_iCpuUsageCheckTimeoutSec = 60*60;
                return true;
            }
            break;
        }
	case 'd':
		{
			if(!strcmp(cHeader,"domain_name"))
			{
				m_sDomain = cValue;			
				return true;
			}
			else if (!strcmp(cHeader,"dns_cache"))
			{
				if ((cValue[0] == 'y') || (cValue[0] == 'Y')) {
					m_bDNSCache = true;
					return true;
				}
				else if ((cValue[0] == 'n') || (cValue[0] == 'N')) {
					m_bDNSCache = false;
					return true;
				}

				//Warning
				cout<<"Warning: Unknown value in "<<cHeader<<endl;
				m_bDNSCache = true;
				cout<<"DNS Cache is now enabled\n";
				return true;
			}	
			break;
		}
	case 'e':
		{
                        if(!strcmp(cHeader,"eyeball_cert_file"))
                        {
                                m_sEyeballCertFile = cValue;
                                return true;
                        }
			break;
		}
	case 'f':
		{
			if(!strcmp(cHeader,"forward_udp_port"))
			{
				m_iForwardMessageUdpPort = iIntValue;
				return true;
			}
			else if(!strcmp(cHeader,"forward_tcp_port"))
			{
				m_iForwardMessageTcpPort = iIntValue;
				return true;
			}
			break;
		}
	case 'l':
		{
			if(!strcmp(cHeader,"log_enable") ||
			   !strcmp(cHeader,"log_sip_message")) 
			{
				if ((cValue[0] == 'y') || (cValue[0] == 'Y')) {
					m_bLogEnabled = true;
					return true;
				}
				else if ((cValue[0] == 'n') || (cValue[0] == 'N')) {
					m_bLogEnabled = false;
					return true;
				}

				//Error
				cout<<"Warning: Unknown value in "<<cHeader<<endl;
				m_bLogEnabled = true;
				cout<<"Logging is now enabled\n";
				return true;
			}
			else if(!strcmp(cHeader,"log_file"))
			{
				m_sLogFileName = cValue;
				return true;
			}
                        else if(!strcmp(cHeader,"license_name"))
                        {
                                m_sLicenseName = cValue;
                                return true;
                        }
                        else if (!strcmp(cHeader,"license_cert_file"))
                        {
                                m_sLicenseCertFile = cValue;
                                return true;
                        }
                        else if (!strcmp(cHeader,"logging_interval"))
                        {
                                m_iDBUpdateStatsInterval = iIntValue;
                                return true;
                        }
			else if (!strcmp(cHeader,"log_max_file_size"))
			{
				m_iLogMaxFileSize = iIntValue;
				return true;
			}
			else if (!strcmp(cHeader,"log_max_file_count"))
			{
				m_iLogMaxFileCount = iIntValue;
				return true;
			}
                        break;
		}
    case 'm':
    {
        if ( !strcmp(cHeader,"message_queue_size") )
        {
            m_iMessageQueueSize = iIntValue;
        }
        break;
    }
	case 'n':
		{
			if(!strcmp(cHeader,"num_threads"))
			{
				m_iNumThreads = iIntValue;
				return true;
			}
			break;
		}
	case 'o':
		{
			if(!strcmp(cHeader,"out_file"))
			{
				m_sOutFileName = cValue;
				return true;
			}
			break;
		}
	case 'p':
		{
			if(!strcmp(cHeader,"private_address"))
                	{
				m_sPrivateIP = cValue;
				return true;
			}
			else if(!strcmp(cHeader,"pid_file"))
			{
				m_sPidFileName = cValue;
				return true;
			}
			break;
		}
	case 'r':
		{
			if (!strcmp(cHeader,"recvbuffer_size"))
			{
				m_iRecvBufferSize = iIntValue;
				return true;	
			}
			break;
		}
	case 's':
		{
			if(!strcmp(cHeader,"sendbuffer_size"))
			{
				m_iSendBufferSize = iIntValue;
				return true;
			}
			break;
		}
	case 't':
		{
			if(!strcmp(cHeader,"tcp_sendbuffer_size"))
			{
				m_iSendBufferSize = iIntValue;
				return true;
			}
			else if (!strcmp(cHeader,"tcp_connection_timeout"))
			{
				m_iTCPConnectionTimeout = iIntValue;
				return true;
			}
			else if (!strcmp(cHeader,"tcp_connections"))
			{
				m_iTCPConnections = iIntValue;
				return true;
			}
			else if (!strcmp(cHeader,"tls_connections"))
			{
				m_iTLSConnections = iIntValue;
				return true;
			}
			else if (!strcmp(cHeader,"tls_cert_file"))
			{
				m_sCertFile = cValue;
				return true;
			}
			else if (!strcmp(cHeader,"tls_cert_keyfile"))
			{
				m_sCertKeyFile = cValue;
				return true;
			}
			else if (!strcmp(cHeader,"tls_cert_user"))
			{
				m_sCertUser = cValue;
				return true;
			}
            else if (!strcmp(cHeader,"tolerable_cpu_usage"))
            {
                m_iTolerableCpuUsage = iIntValue;
                return true;
            }
			break;
		}
	case 'u':
		{
			if (!strcmp(cHeader,"udp_query_port"))
			{
				m_iUdpQueryPort = iIntValue;
				return true;
			}
			break;
		}
	} //switch
    if (m_sIP=="any")
    {
        m_bIsBindAddrAny=true;
        vector<string> ips;
        this->getAllIps(ips);
        if (ips.size()==0)
        {
            cout<<"CANT SET m-sIP. getAllIps returned empty"<<endl;
        }
        else
        {
            cout<<"bind_address = any."<<endl;
            cout<<"Found interfaces : "<<endl;
            for (int i=0; i<ips.size(); i++) cout<<ips[i]<<endl;
            for (int i=0; i<ips.size(); i++)
            {
                if (ips[i]=="127.0.0.1") continue;
                m_sIP=ips[i];
                break;
            }
            cout<<"m_sIP is set to "<<m_sIP<<endl;
        }
    }
    else m_bIsBindAddrAny=false;

	return DBConfigFile::ProcessHeaderValuePair(cHeader, cValue);
}

string CCoreConfigFile::getSettings()
{
	DB_ENTER(CCoreConfigFile::getSettings);

	string sSettings;
	sSettings.reserve(4096);

	sSettings = DBConfigFile::getSettings();
        string sYes = "Y";
        string sNo = "N";
        string sEndl = "\n";

        sSettings  = "Server IP: " + m_sIP + sEndl;
	sSettings += (string)"Private IP: " + m_sPrivateIP + sEndl;
        sSettings += (string)"Domain: " + m_sDomain + sEndl;
        sSettings += (string)"Server Hostname: " + m_sHostName + sEndl;
        sSettings += (string)"Send Buffer Size: " + itos(m_iSendBufferSize) + sEndl;
        sSettings += (string)"Receive Buffer Size: " + itos(m_iRecvBufferSize) + sEndl;

        if (m_iTCPConnectionTimeout!=INFINITE) {
                sSettings += (string)"TCP connection timeout: " + itos(m_iTCPConnectionTimeout) + sEndl;
        }
        else {
                sSettings += "TCP connection timeout: INFINITE" + sEndl;
        }

        sSettings += (string)"Maximum Number of TCP Connections: " + itos(m_iTCPConnections) + sEndl;
        sSettings += (string)"Maximum Number of TLS Connections: " + itos(m_iTLSConnections) + sEndl;
        sSettings += (string)"Telnet Admin Port: " + itos(m_iTelnetAdminPort) + sEndl;
        sSettings += (string)"Forward Message UDP Port: " + itos(m_iForwardMessageUdpPort) + sEndl;
        sSettings += (string)"Forward Message TCP Port: " + itos(m_iForwardMessageTcpPort) + sEndl;
        sSettings += (string)"UDP Query Port: " + itos(m_iUdpQueryPort) + sEndl;

        sSettings += (string)"Logging Enabled: " +
                     (m_bLogEnabled ? sYes : sNo) + sEndl;

	sSettings += (string)"Log Max File Size: " + itos(m_iLogMaxFileSize) + sEndl;
        sSettings += (string)"Log Max File Count: " + itos(m_iLogMaxFileCount) + sEndl;
        sSettings += (string)"Log File Name: " + m_sLogFileName + sEndl;
        sSettings += (string)"Pid File Name: " + m_sPidFileName + sEndl;
#ifdef DEBUG
        sSettings += (string)"Out File Name: " + m_sOutFileName + sEndl;
#endif

        sSettings += (string)"TLS Certificate File: " + m_sCertFile + sEndl;
        sSettings += (string)"TLS Certificate Key File: " + m_sCertKeyFile + sEndl;
        sSettings += (string)"TLS Certificate User: " + m_sCertUser + sEndl;

        sSettings += (string)"Use DNS Cache: " +
                     (m_bDNSCache ? sYes : sNo) + sEndl;

        sSettings += (string)"Logging Time: every " + itos(m_iDBUpdateStatsInterval) + " minutes" + sEndl;

        sSettings += (string)"License Name: " + m_sLicenseName + sEndl;
        sSettings += (string)"License Certificate File: " + m_sLicenseCertFile + sEndl;
        sSettings += (string)"Eyeball Certificate File: " + m_sEyeballCertFile + sEndl;

	sSettings += (string)"Number of Message Processing Threads: " + itos(m_iNumThreads) + sEndl;
    sSettings += (string)"Size of Message Queue : "+ itos(m_iMessageQueueSize) + sEndl;

    sSettings += (string)"CpuUsage Check Timeout:"+itos(m_iCpuUsageCheckTimeoutSec)+"s"+sEndl;
    sSettings += (string)"Tolerable Cpu Usage: " + itos(m_iTolerableCpuUsage) +"%"+ sEndl;
        sSettings += DBConfigFile::getSettings();

        return sSettings;

}

void CCoreConfigFile::getAllIps(vector<string> &ips)
{
    ips.clear();

    struct ifaddrs *myaddrs, *ifa;
    void *in_addr;
    char buf[64];

    if(getifaddrs(&myaddrs) != 0)
    {
        //perror("getifaddrs");
        //exit(1);
        return;
    }

    for (ifa = myaddrs; ifa != NULL; ifa = ifa->ifa_next)
    {
        if (ifa->ifa_addr == NULL)
            continue;
        if (!(ifa->ifa_flags & IFF_UP))
            continue;

        switch (ifa->ifa_addr->sa_family)
        {
            case AF_INET:
            {
                struct sockaddr_in *s4 = (struct sockaddr_in *)ifa->ifa_addr;
                in_addr = &s4->sin_addr;
                break;
            }

//                case AF_INET6:
//                {
//                    struct sockaddr_in6 *s6 = (struct sockaddr_in6 *)ifa->ifa_addr;
//                    in_addr = &s6->sin6_addr;
//                    break;
//                }

            default:
                continue;
        }

        if (inet_ntop(ifa->ifa_addr->sa_family, in_addr, buf, sizeof(buf)))
        {
            ips.push_back(string(buf));
            //cout<<"!!!!!!!!!!!!!!!!! =>"<<ips.back()<<endl;
        }
    }

    freeifaddrs(myaddrs);
}



