//! StateServerSelector.h
//! Interface of the CStateServerSelector class

#ifndef __STATE_SERVER_SELECTOR_H_
#define __STATE_SERVER_SELECTOR_H_

#include <pthread.h>
#include <string>
using std::string;

class PeriodicStateServerUpdate;  // helper class

class CStateServerSelector
{
public:
	//! Constructor
	CStateServerSelector();

	//! Destructor
	virtual ~CStateServerSelector();

	//! Choose state server
	//! Returns true if there is a state server
	//!   Assigns value to m_sServerAddress and m_iServerPort
	//! Returns false if there are no state servers
	virtual bool ChooseStateServer(int iHash, string& sServerAddress, int& iServerPort);

	//! Set state server as down
	//! Usually called after a query times out
	//! May send message to State Server and write to database
	//! Returns false if database write failed or if server address is unrecognized
	virtual bool SetStateServerDown(const string& sServerAddress, int iServerPort);

	//! Retrieve status of state servers
	//! Could be called in a thread
	//! Returns true if succeeded
	//! Returns false if retrieval failed, eg. if database query failed
	virtual bool UpdateStateServerStatus(); 

	//! Start updating at the given regular interval
	//! Overrides any previous calls.
	//! If given iPeriod is 0, then periodic update is cancelled.
	//! Returns true if succeeded
	virtual bool ActivatePeriodicUpdate(int iPeriod);

	//! Retrieve port information for CLI (as a string)
	//! Returns port information as a string
        virtual string GetPortInformation() const;

protected:
	struct StateServerInfo {
		string sAddress;
		unsigned int iPort;
		unsigned int iAddress; //host byte order
	};

	//! Return list of state servers
	//! Return value is number of state servers
	virtual int GetListOfStateServers(StateServerInfo**& pServers) const = 0;

	StateServerInfo** m_pStateServerTable;
	int m_iNumStateServers;
	int m_iHashValueDivisor;

	pthread_mutex_t m_Mutex;

	PeriodicStateServerUpdate* m_pPeriodicUpdate;

	int m_iLastUpdate;
};

#endif //__STATE_SERVER_SELECTOR_H_

