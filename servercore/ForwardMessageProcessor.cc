//! ForwardMessageProcessor.cc
//! Interface for the CForwardMessageProcessor class

#include "ForwardMessageProcessor.h"
#include "debug.h"
#include "log.h"
#include "ForwardEdgeServerMessage.h"
#include "ServerMT.h"
#include <string.h>

//! Constructor
CForwardMessageProcessor::CForwardMessageProcessor()
{
	DB_ENTER(CForwardMessageProcessor::CForwardMessageProcessor);
}

//! Destructor
CForwardMessageProcessor::~CForwardMessageProcessor()
{
	DB_ENTER(CForwardMessageProcessor::~CForwardMessageProcessor);
}

//! creates a new instance of the actual processor type (without copying any status information), required
//! when instantiating new processors in a Connection object
IMessageProcessor* CForwardMessageProcessor::createNewInstance()
{
	DB_ENTER(CForwardMessageProcessor::createNewInstance);

	return new CForwardMessageProcessor;
}

//! does not contain validity checks, only checks for length and 'reasonable' content
int CForwardMessageProcessor::isCompleteMessage(char *pBuf, int iBufLen)
{
	DB_ENTER(CForwardMessageProcessor::isCompleteMessage);

	if (iBufLen > (CM_LENGTH + 10))
	{// Forward Edge Server Message
		int iPayloadLength = CM_GET_PAYLOAD_LENGTH(pBuf);
		return (CM_LENGTH + iPayloadLength);
	}

	return MSG_INVALID;
}

//! process a complete message; provide IP and port of message origin
int CForwardMessageProcessor::processMessage(char *pBuf, int iBufLen, const string& IP, int port, int proto, const string& recvdIP, int recvdPort, int recvdProto)
{
	DB_ENTER(CForwardMessageProcessor::processMessage);

	if ((iBufLen > (CM_LENGTH + 10)) &&
	    (CM_GET_MESSAGE_TYPE(pBuf) == EYEBALL_SERVER_FORWARD_EDGE))
	{// Forward Edge Server Message
		ForwardEdgeServerMessage fwdmsg;
		string sBuf;
		sBuf.assign(pBuf, iBufLen);
		int des_result = fwdmsg.deserialize(sBuf);
		if (des_result > 0) {
			//Forward Message
		        string sForwardAddress;
			fwdmsg.getAddress(sForwardAddress);

			int iResponseLen = fwdmsg.m_sMessage.size();
			char* pcResponse = new char[iResponseLen];
			memcpy(pcResponse, fwdmsg.m_sMessage.data(), iResponseLen);

			g_pServerMT->SendMessage(
			  sForwardAddress, fwdmsg.m_u16Port,
			  fwdmsg.m_u8Protocol, fwdmsg.m_u8MessageProcessorType,
			  pcResponse, iResponseLen, false);

			if (fwdmsg.m_u8CloseConnection) {
				g_pServerMT->CloseConnection(sForwardAddress,
				  fwdmsg.m_u16Port, fwdmsg.m_u8Protocol, FORWARD_MESSAGES);
			}
		}
		else {
			char tmp[40];
			strncpy(tmp, pBuf, 39);

			DB_AND_LOGL(4, "Failed to forward message to edge proxy: " << tmp);
		}
	}

	return MSG_INVALID;	
}


