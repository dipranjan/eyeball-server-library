// XMPPBlockListMessage.cc
// Interface for the XMPPBlockListMessage class

#include "XMPPBlockListMessage.h"
#include "debug.h"

const string XMPPBlockListMessage::m_sDefaultListResource = "-default-";

XMPPBlockListMessage::XMPPBlockListMessage()
: QueryMessage(EYEBALL_SERVER_XMPP_BLOCK_LIST),
  m_u8Action(EACTION_GET_FOR_RESOURCE)
{
	DB_ENTER(XMPPBlockListMessage::XMPPBlockListMessage);

}

XMPPBlockListMessage::~XMPPBlockListMessage()
{
	DB_ENTER(XMPPBlockListMessage::~XMPPBlockListMessage);

}

int XMPPBlockListMessage::deserialize(const string& buf)
{
  DB_ENTER(XMPPBlockListMessage::deserialize);

  m_List.clear();
  m_Resources.clear();
  m_ResourceBlockLists.clear();

  int iOffset = 0;

  iOffset += QueryMessage::deserialize(buf);

  if (!DeserializeString16(m_sName, buf, iOffset)) return -1;
  if (!Deserialize8(m_u8Action, buf, iOffset)) return -1;
  if (!DeserializeString16(m_sDefaultList, buf, iOffset)) return -1;

  if (!DeserializeStringVector16(m_List, buf, iOffset)) return -1;
  if (!DeserializeStringVector16(m_Resources, buf, iOffset)) return -1;
  if (!DeserializeStringVector16(m_ResourceBlockLists, buf, iOffset)) return -1;

  int iNumResources = m_Resources.size();
  int iNumBlockLists = m_ResourceBlockLists.size();

  if (iNumResources > iNumBlockLists) {
    for (int i = iNumBlockLists; i < iNumResources; i++) {
      m_ResourceBlockLists.push_back("");
    }
  }
  else if (iNumResources < iNumBlockLists) {
    m_ResourceBlockLists.resize(iNumResources);
  }

  return iOffset;
}

void XMPPBlockListMessage::serialize(string& buf) const
{
  DB_ENTER(XMPPBlockListMessage::serialize);

  QueryMessage::serialize(buf);

  // serialize message contents
  SerializeString16(m_sName, buf);
  Serialize8(m_u8Action, buf);
  SerializeString16(m_sDefaultList, buf);
  SerializeStringVector16(m_List, buf);
  SerializeStringVector16(m_Resources, buf);
  SerializeStringVector16(m_ResourceBlockLists, buf);
}

u_int16_t XMPPBlockListMessage::payload_length(void) const
{
  DB_ENTER(XMPPBlockListMessage::payload_length);

  u_int16_t iLength = QueryMessage::payload_length() + 
                      m_sName.size() + sizeof(u_int16_t) + 1 +
                      m_sDefaultList.size() + sizeof(u_int16_t) +
                      GetSerializeStringVector16Length(m_List) +
                      GetSerializeStringVector16Length(m_Resources) +
                      GetSerializeStringVector16Length(m_ResourceBlockLists);

  return iLength; 
}

ostream& operator<<(ostream& s, const XMPPBlockListMessage& v)
{
	int iSize = v.m_List.size();
	int iSize2 = v.m_Resources.size();
	int iSize3 = v.m_ResourceBlockLists.size();

        s << (QueryMessage)v
                << " " << v.m_sName << " " << v.m_u8Action
                << " " << v.m_sDefaultList
                << " " << iSize << " " << iSize2;

	for (int i = 0; i < iSize; i++) {
		s << " " << v.m_List[i];
	}

        for (int i = 0; i < iSize2; i++) {
		s << " " << v.m_Resources[i] << ": ";
		if (i < iSize3) {
			s << v.m_ResourceBlockLists[i];
		}
		else {
			s << "";
		}
	}	

        return s;
}


