


/*
  $Id: DNSService.h,v 1.3 2007/08/01 01:52:39 larsb Exp $
*/


#ifndef _DNSSERVICE_H
#define _DNSSERVICE_H

#include <string>
using std::string;

#include <DnsInterface.h>
#include "hashtable.h"

enum DnsResultType { DNS_OK=0, DNS_HOSTNAME, DNS_FAILURE };

struct DNSCacheEntry
{
  string m_sIP;
  int m_iPort;
  int m_iProtocol;
  DnsResultType m_Result;
};



class DNSService 
{

 protected:

  DnsResultType _lookupHostname(const string &sHostname, string &IP, int &port, int &protocol);


#ifdef TEST_VOIPD
 public:
#endif

  static HashTableMT<DNSCacheEntry> m_DNSCache;

 public:

  DNSService();
  ~DNSService();

  //! synchronous request, responses will be send to calling messageprocessor
  //! function tries DNS SRV first, then hostname lookup
  DnsResultType Lookup(const string& sDomain, const string& sService, int iProtocol,
			string& sFoundAddress, int& iFoundPort, int& iFoundProtocol);

};

#endif //_DNSSERVICE_H

