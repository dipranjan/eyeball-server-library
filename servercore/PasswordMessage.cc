// file: PasswordMessage.cc

#include "PasswordMessage.h"
#include "debug.h"

PasswordMessage::PasswordMessage()
:
  QueryMessage(EYEBALL_SERVER_PASSWORD),
  m_sName(),
  m_sPassword()
{
  DB_ENTER(PasswordMessage::PasswordMessage);
}

PasswordMessage::~PasswordMessage()
{
  DB_ENTER(PasswordMessage::~PasswordMessage);
}

void PasswordMessage::serialize(string& buf) const
{
  DB_ENTER(PasswordMessage::serialize);

  QueryMessage::serialize(buf);

  // serialize message contents
  SerializeString16(m_sName, buf);
  SerializeString16(m_sPassword, buf);
}

int PasswordMessage::deserialize(const string& buf)
{
  DB_ENTER(PasswordMessage::deserialize);

  int iOffset = 0;

  iOffset += QueryMessage::deserialize(buf);

  // deserialize message contents
  if (!DeserializeString16(m_sName, buf, iOffset))
    return -1;

  if (!DeserializeString16(m_sPassword, buf, iOffset))
    return -1;

  return iOffset;
}

u_int16_t PasswordMessage::payload_length() const
{
  DB_ENTER(PasswordMessage::payload_length);

  return QueryMessage::payload_length() + 
	m_sName.size() + sizeof(u_int16_t) +
	m_sPassword.size() + sizeof(u_int16_t);
}

ostream& operator<<(ostream& s, const PasswordMessage& v)
{
        s << (QueryMessage)v
                << " " << v.m_sName << " " << v.m_sPassword;

        return s;
}

