// ---------------------------------------------------------------------------
// File:       ForwardEdgeServerMessage.h
//
// Copyright:  Eyeball Networks Inc. 1999-2006
//
// Purpose:    Forward message through another Edge Server 
//
// Change Log: 03/14/2006 - Created
// ---------------------------------------------------------------------------

#ifndef __FORWARD_EDGE_SERVER_MESSAGE_H_
#define __FORWARD_EDGE_SERVER_MESSAGE_H_

#include "Message.h"
#include <string>

using std::string;
using std::ostream;

struct ForwardEdgeServerMessage : public CommandMessage
{
	ForwardEdgeServerMessage();
	virtual ~ForwardEdgeServerMessage();

	// Convert to/from network byte order
	virtual void serialize(string& sBuf) const;
	virtual int  deserialize(const string& sBuf);

        virtual u_int16_t payload_length(void) const;

	//Converts string to 32-bit integer address
	//Returns false if address invalid or conversion failed
	bool setAddress(const string& sAddress);

	//Returns address as a string
	string& getAddress(string& sAddress) const;

	//Network byte order
	u_int32_t m_u32Address;

	u_int16_t m_u16Port;
	u_int8_t  m_u8Protocol;
	u_int8_t  m_u8MessageProcessorType;
	u_int8_t  m_u8CloseConnection;

	string m_sMessage;
#ifndef _WIN32_WCE
	friend ostream& operator<<(ostream& s, const ForwardEdgeServerMessage& v);
#endif

};

#endif //__FORWARD_EDGE_PROXY_MESSAGE_H_ 

