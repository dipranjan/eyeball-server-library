// XmppBlockMessage.cc
// Interface for the XMPPBlockMessage class

#include "XMPPBlockMessage.h"
#include "debug.h"

XMPPBlockMessage::XMPPBlockMessage()
: QueryMessage(EYEBALL_SERVER_XMPP_BLOCK)
{
	DB_ENTER(XMPPBlockMessage::XMPPBlockMessage);

}

XMPPBlockMessage::~XMPPBlockMessage()
{
	DB_ENTER(XMPPBlockMessage::~XMPPBlockMessage);

}

int XMPPBlockMessage::deserialize(const string& buf)
{
  DB_ENTER(XMPPBlockMessage::deserialize);

  m_Block.clear();

  int iOffset = 0;

  iOffset += QueryMessage::deserialize(buf);

  if (!DeserializeString16(m_sName, buf, iOffset)) return -1;
  if (!DeserializeString16(m_sBlockName, buf, iOffset)) return -1;

  u_int16_t iSize = 0;

  // deserialize message contents
  if (!Deserialize16(iSize, buf, iOffset)) return -1;

  for (u_int16_t i = 0; i < iSize; i++) {
    XMPPBlockItem block;
    u_int8_t u8Allow, u8Message, u8IQ, u8PresenceIn, u8PresenceOut;

    if (!Deserialize8(block.m_u8Type, buf, iOffset)) return -1;
    if (!DeserializeString16(block.m_sValue, buf, iOffset)) return -1;

    if (!Deserialize8(u8Allow, buf, iOffset)) return -1;
    block.m_bAllow = (u8Allow != 0);

    if (!Deserialize32(block.m_u32Order, buf, iOffset)) return -1;

    if (!Deserialize8(u8Message, buf, iOffset)) return -1;
    block.m_bMessage = (u8Message != 0);

    if (!Deserialize8(u8IQ, buf, iOffset)) return -1;
    block.m_bIQ = (u8IQ != 0);

    if (!Deserialize8(u8PresenceIn, buf, iOffset)) return -1;
    block.m_bPresenceIn = (u8PresenceIn!= 0);

    if (!Deserialize8(u8PresenceOut, buf, iOffset)) return -1;
    block.m_bPresenceOut = (u8PresenceOut != 0);

    m_Block.push_back(block);
  }

  return iOffset;
}

void XMPPBlockMessage::serialize(string& buf) const
{
  DB_ENTER(XMPPBlockMessage::serialize);

  QueryMessage::serialize(buf);

  // serialize message contents
  SerializeString16(m_sName, buf);
  SerializeString16(m_sBlockName, buf);

  u_int16_t iSize = m_Block.size();
  Serialize16(iSize, buf);

  for (u_int16_t i = 0; i < iSize; i++) {
    const XMPPBlockItem& block = m_Block[i];
    Serialize8(block.m_u8Type, buf);
    SerializeString16(block.m_sValue, buf);
    Serialize8(block.m_bAllow ? 1 : 0, buf);
    Serialize32(block.m_u32Order, buf);

    Serialize8(block.m_bMessage ? 1 : 0, buf);
    Serialize8(block.m_bIQ? 1 : 0, buf);
    Serialize8(block.m_bPresenceIn ? 1 : 0, buf);
    Serialize8(block.m_bPresenceOut ? 1 : 0, buf);
  }
}

u_int16_t XMPPBlockMessage::payload_length(void) const
{
  DB_ENTER(XMPPBlockMessage::payload_length);

  u_int16_t iLength = QueryMessage::payload_length() + 
                      m_sName.size() + sizeof(u_int16_t) + 
                      m_sBlockName.size() + sizeof(u_int16_t) +
                      sizeof(u_int16_t);

  int iNumBlock = m_Block.size();
  for (int i = 0; i < iNumBlock; i++) {
    iLength += 10 + m_Block[i].m_sValue.size() + sizeof(u_int16_t);
  }
  return iLength; 
}

unsigned int XMPPBlockMessage::getNumBlock() const
{
  DB_ENTER(XMPPBlockMessage::getNumBlock);

  return m_Block.size();
}

XMPPBlockItem XMPPBlockMessage::getBlock(int iIndex)
{
  DB_ENTER(XMPPBlockMessage::getBlock);

  if ((iIndex < 0) || (iIndex >= (int)m_Block.size())) {
    XMPPBlockItem blank;
    return blank;
  }
  else {
    return m_Block[iIndex];
  }
}

void XMPPBlockMessage::addBlock(const XMPPBlockItem& block)
{
  DB_ENTER(XMPPBlockMessage::addBlock);

  m_Block.push_back(block); 
}

void XMPPBlockMessage::sortAscendingOrder()
{
  DB_ENTER(XMPPBlockMessage::sortAscendingOrder);

  //Selection sort
  int iNumItems_m1 = m_Block.size() - 1;
  for (int x = 0; x < iNumItems_m1; x++) {
    u_int32_t u32XOrder = m_Block[x].m_u32Order;
    for (int y = x + 1; y <= iNumItems_m1; y++) {
      if (u32XOrder > m_Block[y].m_u32Order) {
        //Swap
        XMPPBlockItem tempItem = m_Block[y];
        m_Block[y] = m_Block[x];
        m_Block[x] = tempItem;
        u32XOrder = tempItem.m_u32Order;
      }
    }
  }
}

ostream& operator<<(ostream& s, const XMPPBlockMessage& v)
{
        int iSize = v.m_Block.size();

        s << (QueryMessage)v
                << " " << v.m_sName << " " << v.m_sBlockName << " " << iSize;

        for (int i = 0; i < iSize; i++) {
		s << "\n" << v.m_Block[i];
        }

        return s;
}



