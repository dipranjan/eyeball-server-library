

#ifndef _PROCESSOR_DEFINES
#define _PROCESSOR_DEFINES

#define IDLE                 -1  // Nothing processor
#define FORWARD_MESSAGES      0  // Forward Messages 
#define ADMIN                 1  // Admin CLI
#define SIP                   2  // SIP
#define AS_SIP                3  // antispit processor
#define XMPP                  4  // XMPP
#define XMPP_SERVER_OUT       5  // XMPP Server-to-Server
#define XMPP_SERVER_IN        6  // XMPP Server-to-Server
#define XMPP_SERVER_IN_STANZA 7  // XMPP Server-to-Server (Individual Stanzas)
#define XMPP_SERVER_DIALBACK  8  // XMPP Server Dialback
#define XMPP_AUTH             9  // XMPP message with username and resource
#define BOSH                  10 // BOSH

#define NUM_PROCESSOR_TYPES   11



#endif

