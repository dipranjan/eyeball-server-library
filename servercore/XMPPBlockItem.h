// XmppBlockItem.h
// Interface for the XMPPBlockItem structure 

#ifndef XMPP_BLOCK_ITEM_H
#define XMPP_BLOCK_ITEM_H

#include <stdint.h>
#include <string>
using std::string;

#include <iostream>
using std::ostream;

struct XMPPBlockItem {
  XMPPBlockItem() 
    : m_u8Type(EBLOCK_JID),
      m_bAllow(true),
      m_bMessage(false),
      m_bIQ(false),
      m_bPresenceIn(false),
      m_bPresenceOut(false)
  {
  }

#ifndef _WIN32_WCE
  friend ostream& operator<<(ostream& s, const XMPPBlockItem& v);
#endif

  enum BlockType {
    EBLOCK_JID,
    EBLOCK_GROUP,
    EBLOCK_SUBSCRIPTION,
    EBLOCK_DEFAULT
  };

  uint8_t  m_u8Type;
  string    m_sValue;
  bool      m_bAllow;
  uint32_t m_u32Order;

  bool      m_bMessage;
  bool      m_bIQ;
  bool      m_bPresenceIn;
  bool      m_bPresenceOut;
};

#endif//XMPP_BLOCK_ITEM_H
