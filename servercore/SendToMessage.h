// ---------------------------------------------------------------------------
// File:       SendToMessage.h
//
// Copyright:  Eyeball Networks Inc. 1999-2006
//
// Purpose:    Tell server to send a message 
//
// Change Log: 03/01/2006 - Created
// ---------------------------------------------------------------------------

#ifndef __SEND_TO_MESSAGE_H_
#define __SEND_TO_MESSAGE_H_

#include "Message.h"
#include <string>

using std::string;
using std::ostream;

struct SendToMessage : public CommandMessage
{
	SendToMessage();
	virtual ~SendToMessage();

	// Convert to/from network byte order
	virtual void serialize(string& sBuf) const;
	virtual int  deserialize(const string& sBuf);

        virtual u_int16_t payload_length(void) const;

	//Converts string to 32-bit integer address
	//Returns false if address invalid or conversion failed
	bool setAddress(const string& sAddress);

	//Returns address as a string
	string& getAddress(string& sAddress) const;

	u_int32_t m_u32Address;
	u_int16_t m_u16Port;
	u_int8_t m_u8Protocol;
	u_int8_t m_u8MessageType;
	
	u_int64_t m_u32MessagePtr;
	u_int32_t m_u32MessageLen;

	u_int8_t m_u8ConvertToTLS;
	u_int8_t m_u8AddConnection;
	string m_sTagForNewConnection;

#ifndef _WIN32_WCE
	friend ostream& operator<<(ostream& s, const SendToMessage& v);
#endif

};

#endif //__SEND_TO_MESSAGE_H_ 

