#ifndef XMPP_RESOURCE_MESSAGE_H
#define XMPP_RESOURCE_MESSAGE_H

#include "QueryMessage.h"
#include "IntMT.h"

class XMPPResourceMessage : public QueryMessage
{
public:
  XMPPResourceMessage();
  virtual ~XMPPResourceMessage();

  virtual int deserialize(const string& buf);
  virtual void serialize(string& buf) const;

  virtual u_int16_t payload_length(void) const;

  void setContactAddress(const string& sAddress);
  string& getContactAddress(string& sAddress) const;
  void setForwardAddress(const string& sAddress);
  string& getForwardAddress(string& sAddress) const;

#ifndef _WIN32_WCE
        friend ostream& operator<<(ostream& s, const XMPPResourceMessage& v);
#endif
        static CIntMT constCount;
        static CIntMT destCount;
public:
  // all parameters are publicly accessed
  string    m_sName;
  string    m_sResource;
  string    m_sState;

  u_int32_t m_u32ContactAddress;
  u_int16_t m_u16ContactPort;
  u_int8_t  m_u8ContactProtocol;

  u_int32_t m_u32ForwardAddress;
  u_int16_t m_u16ForwardPort;
  u_int8_t  m_u8ForwardProtocol;

  u_int8_t  m_u8RequestedRoster;
  int8_t    m_i8Priority;
  string    m_sLastPresence;

  u_int32_t m_u32LoginTime;
};

#endif//XMPP_RESOURCE_MESSAGE_H
