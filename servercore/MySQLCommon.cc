
#include <log.h>
#include <debug.h>
#include "inet.h"
#include "string_utils.h"


#include "MySQLCommon.h"
#include "MySQLConfigFile.h"




using namespace std;
using namespace mysqlpp;

#define STATE_SERVER_EXPIRY_TIME 20  // seconds
#define EDGE_SERVER_EXPIRY_TIME 20  // seconds

// time until the state server list is considered "stable"
#define SERVER_LIST_STABLE_TIME 30  // seconds

MySQLCommon::MySQLCommon()
: MySQLBase()
{
  DB_ENTER(MySQLCommon::MySQLCommon);
}// MySQLCommon::MySQLCommon()



MySQLCommon::~MySQLCommon()
{
  DB_ENTER(MySQLCommon::~MySQLCommon);
}// MySQLCommon::~MySQLCommon()


int MySQLCommon::OpenUsingConfigFile(const string& sConfigFilename)
{
  DB_ENTER(MySQLCommon::OpenUsingConfigFile);

  CMySQLConfigFile conf;
  if (conf.ReadFile(sConfigFilename.c_str())) {
    return MySQLBase::Open(conf.m_sDBName, conf.m_sDBHosts,
                           conf.m_iDBPort, conf.m_sDBUser, conf.m_sPassFile);
  }
  return -1;
}


int MySQLCommon::SetConfigValue(const string& sName, const string& sValue)
{
  DB_ENTER(MySQLCommon::SetConfigValue);

  string sQuery;
  sQuery.reserve(128);
  sQuery = "update ServerConfig set Value='" + escape(sValue) +
           "' where Name='" + escape(sName) + "'";

  return _putTemplate(sQuery);
} // MySQLCommon::SetConfigValue()


int MySQLCommon::GetConfigValue(const string& sName, string& sValue)
{
  DB_ENTER(MySQLCommon::GetConfigValue);

  sValue = "";

  string sQuery;
  sQuery.reserve(128);
  sQuery = "select Value from ServerConfig where Name='" +
           escape(sName) + "'";

  Result res;
  if (_getTemplate(sQuery,res) > 0) {
      Row row = *res.begin();
#ifdef MYSQL4
      sValue = (string)row.at(0);
#else
      sValue = (string)row[0];
#endif
      return res.size();
  }
  return 0;
} // MySQLCommon::GetConfigValue



//! Convert between time (seconds since 1970)
//! and Date Time used in MySQLCommon: YYYY-MM-DD HH:MM:SS
void MySQLCommon::TimeToDateTime(time_t t, char* pcDateTime) 
{
  struct tm timeinfo;

  if (localtime_r(&t, &timeinfo)) {
    sprintf(pcDateTime, "%04d-%02d-%02d %02d:%02d:%02d",
            1900 + timeinfo.tm_year, timeinfo.tm_mon + 1, timeinfo.tm_mday,
            timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec);
  }
  else {
    strcpy(pcDateTime, "1970-01-01 00:00:00");
  }
}

time_t MySQLCommon::DateTimeToTime(const char* pcDateTime) 
{
  if (strlen(pcDateTime) < 19) {
    return 0;
  }

  char dateTime[20];
  memcpy(dateTime, pcDateTime, 19);

  dateTime[ 4] = '\0';
  dateTime[ 7] = '\0';
  dateTime[10] = '\0';
  dateTime[13] = '\0';
  dateTime[16] = '\0';
  dateTime[19] = '\0';

  struct tm timeinfo;
  memset(&timeinfo, 0, sizeof(timeinfo));

  timeinfo.tm_year = atoi(&(dateTime[ 0])) - 1900;
  timeinfo.tm_mon  = atoi(&(dateTime[ 5])) - 1;
  timeinfo.tm_mday = atoi(&(dateTime[ 8]));
  timeinfo.tm_hour = atoi(&(dateTime[11]));
  timeinfo.tm_min  = atoi(&(dateTime[14]));
  timeinfo.tm_sec  = atoi(&(dateTime[17]));

  return mktime(&timeinfo);
}

string MySQLCommon::ContactToString(u_int32_t uAddr, u_int16_t uPort, u_int8_t uProtocol) 
{
  char buf[32];
  memset(buf, 0, sizeof(buf));

  sprintf(buf, "%d.%d.%d.%d:%d/%d",
    (u_int8_t)(uAddr >> 24),
    (u_int8_t)(uAddr >> 16),
    (u_int8_t)(uAddr >> 8),
    (u_int8_t)uAddr,
    uPort,
    uProtocol);

  return buf;
}

void MySQLCommon::StringToContact(const string& sContact, u_int32_t& uAddr, u_int16_t& uPort, u_int8_t& uProtocol) 
{
  int arr_int[4];
  memset(arr_int, 0, sizeof(arr_int));

  int nPort = 0;
  int nProtocol = 0;

  // unsafe, easy implementation
  sscanf(sContact.c_str(), "%d.%d.%d.%d:%d/%d",
      &arr_int[0],
      &arr_int[1],
      &arr_int[2],
      &arr_int[3],
      &nPort,
      &nProtocol);

  uPort = (u_int16_t)nPort;
  uProtocol = (u_int8_t)nProtocol;

  u_int8_t arr_uAddr[4];
  for (int i = 0; i < 4; i++)
  {
    arr_uAddr[i] = (u_int8_t)arr_int[i];
  }

  memcpy((void*)&uAddr, arr_uAddr, sizeof(uAddr));
  uAddr = ntohl(uAddr);
}

int MySQLCommon::SetLastSIPStateServerListChange()
{
  // update LastSIPStateServerListChange time in database to "now"
  DB_ENTER(MySQLCommon::SetLastSIPStateServerListChange);

  string sQuery;
  sQuery.reserve(128);
  sQuery = "update ServerConfig set Time=NOW()" \
           " where Name='LastSIPStateServerListChange'";

  return _putTemplate(sQuery);
}

int MySQLCommon::SetLastXMPPStateServerListChange()
{
  // update LastXMPPStateServerListChange time in database to "now"
  DB_ENTER(MySQLCommon::SetLastXMPPStateServerListChange);

  string sQuery;
  sQuery.reserve(128);
  sQuery = "update ServerConfig set Time=NOW()" \
           " where Name='LastXMPPStateServerListChange'";

  return _putTemplate(sQuery);
}

bool MySQLCommon::IsSIPStateServerListStable()
{
  DB_ENTER(MySQLCommon::IsSIPStateServerListStable);
  // time list changes are considered recent: SERVER_LIST_STABLE_TIME
  // check if LastSIPStateServerListChange time from database is recent.

  string sQuery;
  sQuery.reserve(256);
  sQuery = "select Time from ServerConfig where Name='LastSIPStateServerListChange'" \
           " and (UNIX_TIMESTAMP() - UNIX_TIMESTAMP(Time)) < " + itos(SERVER_LIST_STABLE_TIME);

  Result res;
  return (_getTemplate(sQuery,res) <= 0); // true (stable) if the query is empty
}

bool MySQLCommon::IsXMPPStateServerListStable()
{
  DB_ENTER(MySQLCommon::IsXMPPStateServerListStable);
  // time list changes are considered recent: SERVER_LIST_STABLE_TIME
  // check if LastXMPPStateServerListChange time from database is recent.

  string sQuery;
  sQuery.reserve(256);
  sQuery = "select Time from ServerConfig where Name='LastXMPPStateServerListChange'" \
           " and (UNIX_TIMESTAMP() - UNIX_TIMESTAMP(Time)) < " + itos(SERVER_LIST_STABLE_TIME);

  Result res;
  return (_getTemplate(sQuery,res) <= 0); // true (stable) if the query is empty
}

int MySQLCommon::GetStateServerExpiryTime() const
{
  return STATE_SERVER_EXPIRY_TIME;
}

int MySQLCommon::GetEdgeServerExpiryTime() const
{
  return EDGE_SERVER_EXPIRY_TIME;
}

bool MySQLCommon::GetPassword(const string& sUser, string &sPass)
{
  DB_ENTER(MySQLCommon::GetPassword);

  sPass = "";

  string sQuery;
  sQuery.reserve(128);
  sQuery  = "select Password from Accounts where User_ID='";
  sQuery += escape(sUser);
  sQuery += "' and Active='Y'";

  bool bRet = false;
  Result res;
  if (_getTemplate(sQuery,res) > 0) {
      Row row = *res.begin();
#ifdef MYSQL4
      sPass = (string)row.at(0);
#else
      sPass = (string)row[0];
#endif

      bRet = true;
  }
  else {
    DB_AND_LOGL(5, "Unable to retrieve password for " << sUser);
  }
  return bRet;
}// int MySQLCommon::GetPassword(const string& sUser, string &sPass)


