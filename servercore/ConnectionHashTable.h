//! ConnectionHashTable.h
//! Interface for Connection Hash Table class
//! Matches Address, Port, Protocol triple to Socket

#ifndef __CONNECTION_HASH_TABLE_H_
#define __CONNECTION_HASH_TABLE_H_

#include <netinet/in.h> //in_addr_t
#include <string>
#include "hashtable.h"

using namespace std;

class CConnectionHashTable
{
public:
	//! Constructor
	CConnectionHashTable(int iMaxConnections);

	//! Destructor
	~CConnectionHashTable();

	//! Put
	//! To remove element, put NULL for value 
	void Put(const string& sAddress, int iPort, int iProtocol, int iMessageType, void* value);

	//! Put
	//! To remove element, put NULL for value
	//! iAddress is in host byte order
	void Put(int iAddress, int iPort, int iProtocol, int iMessageType, void* value);

	//! Get
	void* Get(const string& sAddress, int iPort, int iProtocol, int iMessageType);

	//! Get
	//! iAddress is in host byte order
	void* Get(int iAddress, int iPort, int iProtocol, int iMessageType);

protected:
	//! Converts address(string), port, protocol to a string of 8 bytes
	string ToString(const string& sAddress, int iPort, int iProtocol, int iMessageType) const;

	//! Converts address(int), port, protocol to a string of 8 bytes
	//! Address in host byte order
	string ToString(int iAddress, int iPort, int iProtocol, int iMessageType) const;

	HashTableMT<long> m_Table;
};

#endif //__CONNECTION_HASH_TABLE_H_
