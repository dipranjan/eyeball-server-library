// file: StateMessage.cc

#include "StateMessage.h"
#include <arpa/inet.h>
#include "debug.h"
#include "Socket.h"
#include "string_utils.h"

StateMessage::StateMessage()
:
  QueryMessage(EYEBALL_SERVER_STATE),
  m_sName(),
  m_sDomain(),
  m_u32Addr(0),
  m_u16Port(0),
  m_u8Protocol(0),
  m_u32ProxyAddr(0),
  m_u16ProxyPort(0),
  m_u8ProxyProtocol(0),
  m_u32ForwardAddr(0),
  m_u16ForwardPort(0),
  m_u8ForwardProtocol(0),

  m_u32Login(0),
  m_u32Expiry(0)
{
  DB_ENTER(StateMessage::StateMessage);
}

StateMessage::~StateMessage()
{
  DB_ENTER(StateMessage::~StateMessage);
}

void StateMessage::serialize(string& buf) const
{
  DB_ENTER(StateMessage::serialize);

  QueryMessage::serialize(buf);

  // serialize message contents
  SerializeString16(m_sName, buf);
  SerializeString16(m_sDomain, buf);

  Serialize32(m_u32Addr, buf);
  Serialize16(m_u16Port, buf);
  Serialize8(m_u8Protocol, buf);

  Serialize32(m_u32ProxyAddr, buf);
  Serialize16(m_u16ProxyPort, buf);
  Serialize8(m_u8ProxyProtocol, buf);

  Serialize32(m_u32ForwardAddr, buf);
  Serialize16(m_u16ForwardPort, buf);
  Serialize8(m_u8ForwardProtocol, buf);

  Serialize32(m_u32Login, buf);
  Serialize32(m_u32Expiry, buf);
}

int StateMessage::deserialize(const string& buf)
{
  DB_ENTER(StateMessage::deserialize);

  int iOffset = 0;

  iOffset += QueryMessage::deserialize(buf);

  // deserialize message contents
  if (!DeserializeString16(m_sName, buf, iOffset))
    return -1;

  if (!DeserializeString16(m_sDomain, buf, iOffset))
    return -1;

  if (!Deserialize32(m_u32Addr, buf, iOffset))
    return -1;

  if (!Deserialize16(m_u16Port, buf, iOffset))
    return -1;

  if (!Deserialize8(m_u8Protocol, buf, iOffset))
    return -1;

  if (!Deserialize32(m_u32ProxyAddr, buf, iOffset))
    return -1;

  if (!Deserialize16(m_u16ProxyPort, buf, iOffset))
    return -1;

  if (!Deserialize8(m_u8ProxyProtocol, buf, iOffset))
    return -1;

  if (!Deserialize32(m_u32ForwardAddr, buf, iOffset))
    return -1;

  if (!Deserialize16(m_u16ForwardPort, buf, iOffset))
    return -1;

  if (!Deserialize8(m_u8ForwardProtocol, buf, iOffset))
    return -1;

  if (!Deserialize32(m_u32Login, buf, iOffset))
    return -1;

  if (!Deserialize32(m_u32Expiry, buf, iOffset))
    return -1;

  return iOffset;
}

u_int16_t StateMessage::payload_length() const
{
  DB_ENTER(StateMessage::payload_length);

  return QueryMessage::payload_length() + 
	m_sName.size() + sizeof(u_int16_t) +
	m_sDomain.size() + sizeof(u_int16_t) +
	sizeof(m_u32Addr) +
	sizeof(m_u16Port) +
	sizeof(m_u8Protocol) +
	sizeof(m_u32ProxyAddr) +
	sizeof(m_u16ProxyPort) +
	sizeof(m_u8ProxyProtocol) +
	sizeof(m_u32ForwardAddr) +
	sizeof(m_u16ForwardPort) +
	sizeof(m_u8ForwardProtocol) +
	sizeof(m_u32Login) +
	sizeof(m_u32Expiry);
}

void StateMessage::setAddress(const string& sAddress)
{
  DB_ENTER(StateMessage::setAddress);

  struct in_addr addr;
  memset(&addr, 0, sizeof(addr));
  addr.s_addr = 0;
  inet_pton(AF_INET, sAddress.c_str(), &addr);
  m_u32Addr = ntohl(addr.s_addr);
}

string& StateMessage::getAddress(string& sAddress) const
{
  DB_ENTER(StateMessage::getAddress);

  struct in_addr addr;
  memset(&addr, 0, sizeof(addr));
  addr.s_addr = htonl(m_u32Addr);

  char dst[INET_ADDRSTRLEN];
  if (inet_ntop(AF_INET, &addr, dst, INET_ADDRSTRLEN) != NULL) {
    sAddress = (string)dst;
  }  
  else {
    sAddress = (string)"";
  }
  return sAddress;
}

void StateMessage::setProxyAddress(const string& sAddress)
{
  DB_ENTER(StateMessage::setProxyAddress);

  struct in_addr addr;
  memset(&addr, 0, sizeof(addr));
  inet_pton(AF_INET, sAddress.c_str(), &addr);
  m_u32ProxyAddr = ntohl(addr.s_addr);
}

string& StateMessage::getProxyAddress(string& sAddress) const
{
  DB_ENTER(StateMessage::getProxyAddress);

  struct in_addr addr;
  memset(&addr, 0, sizeof(addr));
  addr.s_addr = htonl(m_u32ProxyAddr);

  char dst[INET_ADDRSTRLEN];
  if (inet_ntop(AF_INET, &addr, dst, INET_ADDRSTRLEN) != NULL) {
    sAddress = (string)dst;
  }  
  else {
    sAddress = (string)"";
  }
  return sAddress;
}

void StateMessage::setForwardAddress(const string& sAddress)
{
  DB_ENTER(StateMessage::setForwardAddress);

  struct in_addr addr;
  memset(&addr, 0, sizeof(addr));
  inet_pton(AF_INET, sAddress.c_str(), &addr);
  m_u32ForwardAddr = ntohl(addr.s_addr);
}

string& StateMessage::getForwardAddress(string& sAddress) const
{
  DB_ENTER(StateMessage::getForwardAddress);

  struct in_addr addr;
  memset(&addr, 0, sizeof(addr));
  addr.s_addr = htonl(m_u32ForwardAddr);

  char dst[INET_ADDRSTRLEN];
  if (inet_ntop(AF_INET, &addr, dst, INET_ADDRSTRLEN) != NULL) {
    sAddress = (string)dst;
  }  
  else {
    sAddress = (string)"";
  }
  return sAddress;
}

void StateMessage::setLoginTimeAndExpiry(int iExpirySeconds)
{
  DB_ENTER(StateMessage::setLoginTimeAndExpiry);

  m_u32Login = time(NULL);
  m_u32Expiry = iExpirySeconds;
}

bool StateMessage::isExpired() const
{
  DB_ENTER(StateMessage::isExpired);

  if (m_u32Expiry) {
    if (((m_u32Login + m_u32Expiry) - time(NULL)) > 0) {
      return false;
    }
  }
  return true;
}

ostream& operator<<(ostream& s, const StateMessage& v)
{
        string sProtocol;
        if (v.m_u8Protocol == UDP) {
                sProtocol = "UDP";
        }
        else if (v.m_u8Protocol == TCP) {
                sProtocol = "TCP";
        }
        else if (v.m_u8Protocol == TLS) {
                sProtocol = "TLS";
        }
        else {
                sProtocol = "Unknown: ";
                sProtocol += itos(v.m_u8Protocol);
        }

        string sProxyProtocol;
        if (v.m_u8ProxyProtocol == UDP) {
                sProxyProtocol = "UDP";
        }
        else if (v.m_u8ProxyProtocol == TCP) {
                sProxyProtocol = "TCP";
        }
        else if (v.m_u8ProxyProtocol == TLS) {
                sProxyProtocol = "TLS";
        }
        else {
                sProxyProtocol = "Unknown: ";
                sProxyProtocol += itos(v.m_u8ProxyProtocol);
        }

        string sForwardProtocol;
        if (v.m_u8ForwardProtocol == UDP) {
                sForwardProtocol = "UDP";
        }
        else if (v.m_u8ForwardProtocol == TCP) {
                sForwardProtocol = "TCP";
        }
        else if (v.m_u8ForwardProtocol == TLS) {
                sForwardProtocol = "TLS";
        }
        else {
                sForwardProtocol = "Unknown: ";
                sForwardProtocol += itos(v.m_u8ForwardProtocol);
        }

        string sAddress;
        v.getAddress(sAddress);

	string sProxyAddress;
	v.getProxyAddress(sProxyAddress);

	string sForwardAddress;
	v.getForwardAddress(sForwardAddress);

        s << (QueryMessage)v
                << " " << v.m_sDomain << " " << sAddress
                << ":" << v.m_u16Port << "/" << sProtocol.c_str()
                << " " << sProxyAddress << ":" << v.m_u16ProxyPort
		<< "/" << sProxyProtocol << " " << sForwardAddress
		<< ":" << v.m_u16ForwardPort << "/" << sForwardProtocol
		<< " Login: " << v.m_u32Login << " Expiry: "
		<< v.m_u32Expiry << " seconds ";

        return s;
}


