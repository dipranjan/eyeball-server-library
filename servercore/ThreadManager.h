//!  ThreadManager.h
//!  Interface for the CThreadManager class

#ifndef _THREADMANAGER_H_
#define _THREADMANAGER_H_

#include <pthread.h>
#include <list>
#include <string>
#include <netinet/in.h> //in_addr_t

using namespace std;

class CNetworkManager;
class CMessageProxy;
class CConnectionHashTable;

class CThreadManager 
{
public:
	//! Constructor
	//! Message Proxy determines what message types are supported
	CThreadManager(const CMessageProxy* pMessageProxy, int iNumThreads = 16);
	
	//! Destructor
	~CThreadManager();

	//! Set Network Manager
	void SetNetworkManager(CNetworkManager* pNetworkManager);

	//! Start
	void Start();

	//! Stop
	void Stop();

        //! Is Message Queue Full
	bool IsMessageQueueFull();

    int MessageQueueWaiting();
	
        //! Is Message Queue Full or almost full
	bool IsMessageQueueFullOrAlmostFull();

	//! To be called by network thread
	//! Network thread notifies thread manager that a message is available
	//! Message is queued for processing or handled as a response message
	void AddNewMessage(const string& sToAddress, int iToPort, const string& sFromAddress, int iFromPort, int iProtocol,
			   int iMessageType, char* pcMessage, int iLen);

	//! To be called by CMessageProxy in worker thread
	//! Passes message to Network Manager
	//! pcMessage will be deleted
	void SendMessage(const string& sToAddress, int iToPort, int iProtocol,
			 int iMessageType, char* pcMessage, int iLen, int iConvertToTLS);

	//! To be called by CMessageProxy in worker thread
	//! Close connection
	//! Passes to Network Manager
	void CloseConnection(const string& sAddress, int iPort, int iProtocol, int iMessageType, bool bLater);

	//! To be called by worker thread
	//! Notifies network thread that a worker thread is available
	void MessageProcessingComplete(int iThreadId);

protected:
	//! Called by worker thread and network thread
	//! Called by AddNewMessage and MessageProcessingComplete
	void AssignMessageToThread();

	//! Thread function
	static void* WorkerThreadFunction(void* arg);
	
	struct MessageInfo {
		string sToAddress;
		int iToPort;
		string sFromAddress;
		int iFromPort;
		int iProtocol;
		int iMessageType;
		char* pcMessage;
		int iLen;
	};

	struct ThreadInfo {
		CThreadManager* pThreadManager;
		int id;
		pthread_t thread;
		pthread_cond_t cond;
		pthread_mutex_t mutex; 
		MessageInfo message;
		CMessageProxy* processor;
	};

	//! Doubly-linked list
	struct ThreadInfoNode {
		ThreadInfo info;
		int iPrevIdleThread;
		int iNextIdleThread;
		int iPrevActiveThread;
		int iNextActiveThread;
	};

	ThreadInfoNode* m_pWorkerThreads;

	int m_iFirstIdleThread;
	int m_iFirstActiveThread;
	pthread_mutex_t m_ThreadListMutex;

	int m_iNumThreads;

	list<MessageInfo> m_MessageQueue;
	pthread_mutex_t m_MessageQueueMutex;

	CNetworkManager* m_pNetworkManager;
	const CMessageProxy* m_pMessageProxy;
	bool m_bEnable;

        CConnectionHashTable* m_pConnectionHashTable;

	pthread_cond_t m_StopCond;

};

#endif //_THREADMANAGER_H_

