//ServerMT.cc
//Implementation of multi-threaded server class

#include "ServerMT.h"
#include "NetworkManager.h"
#include "MessageScanner.h"
#include "MessageProxy.h"
#include "Socket.h"
#include "CoreConfigFile.h"
#include "processor_defines.h"
#include "SyncUDPQuery.h"
#include "StateServerSelector.h"
#include "ForwardEdgeServerMessage.h"
#include "simple_crypt.h"

#include "debug.h"
#include "log.h"

//Constructor
CServerMT::CServerMT(const string& sServerName)
: m_sServerName(sServerName),
  m_bRunning(false),
  m_bStopRequested(false),
  m_pNetworkManager(NULL),
  m_pQuery(NULL),
  m_pStateServerSelector(NULL)
{
	DB_ENTER(CServerMT::CServerMT);

	m_tStart = time(NULL);
	pthread_mutex_init(&m_StopMutex, NULL);
}

//Destructor
CServerMT::~CServerMT()
{
	DB_ENTER(CServerMT::~CServerMT);

	pthread_mutex_destroy(&m_StopMutex);
}

//Get Server Name
const string& CServerMT::GetServerName() const
{
	DB_ENTER(CServerMT::GetServerName);

	return m_sServerName;
} 

//! Send Message
//! pcMessage will be deleted
//! Precondition: NetworkManager is running
void CServerMT::SendMessage(const string& sToAddress, int iToPort, int iProtocol,
                 int iMessageType, char* pcMessage, int iLen, int iConvertToTLS,
                 bool bAddConnectionIfNecessary, const string& sTagForNewConnection)
{
	DB_ENTER(CServerMT::SendMessage);

	m_pNetworkManager->SendMessage(sToAddress, iToPort, iProtocol,
			iMessageType, pcMessage, iLen, iConvertToTLS, 
			bAddConnectionIfNecessary, sTagForNewConnection);
}

//! Forward Message
//! pcMessage will be deleted
//! Precondition: NetworkManager is running
void CServerMT::ForwardMessage(const string& sToAddress, int iToPort, int iProtocol,
                               const string& sViaAddress, int iViaPort, int iViaProtocol,
                               int iMessageType, char* pcMessage, int iLen,
			       bool bCloseConnection)
{
	DB_ENTER(CServerMT::ForwardMessage);

	//First check if forward address is our own address
	if (sViaAddress == g_pCoreConf->m_sPrivateIP)
	{
		bool bForwardViaMe = false;
		if (iViaProtocol == UDP)
		{
			bForwardViaMe = (iViaPort == g_pCoreConf->m_iForwardMessageUdpPort);
		}
		else
		{
			bForwardViaMe = (iViaPort == g_pCoreConf->m_iForwardMessageTcpPort);
		}

		if (bForwardViaMe)
		{
			SendMessage(sToAddress, iToPort, iProtocol, iMessageType,
			            pcMessage, iLen, false, false);

			if (bCloseConnection)
			{
				CloseConnection(sToAddress, iToPort, iProtocol, iMessageType);
			}
		}
		return;
	}

    //cout<<"===========================================> "<<sViaAddress<<":"<<iViaPort<<endl;

	ForwardEdgeServerMessage fwdmsg;
	fwdmsg.setAddress(sToAddress);
	fwdmsg.m_u16Port    = iToPort;
	fwdmsg.m_u8Protocol = iProtocol;
	fwdmsg.m_u8MessageProcessorType = iMessageType;
	fwdmsg.m_u8CloseConnection = (bCloseConnection ? 1 : 0);
	fwdmsg.m_sMessage.assign(pcMessage, iLen);

	string sBuf;
	fwdmsg.serialize(sBuf);

	int iForwardLen = sBuf.size();
	delete [] pcMessage;
	char* pcForward = new char[iForwardLen];
	memcpy(pcForward, sBuf.data(), iForwardLen);

	m_pNetworkManager->SendMessage(sViaAddress, iViaPort, iViaProtocol,
			FORWARD_MESSAGES, pcForward, iForwardLen, false, true, "");
}

//Close TCP/TLS Connection
void CServerMT::CloseConnection(const string& sAddress, int iPort, int iProtocol, int iMessageType)
{
	DB_ENTER(CServerMT::CloseConnection);

	pthread_mutex_lock(&m_StopMutex);

    if (m_bRunning) {
		m_pNetworkManager->CloseConnection(sAddress, iPort, iProtocol, iMessageType);
	}

	pthread_mutex_unlock(&m_StopMutex);
}

//! Send Query
//! Returns length of Response message, or 0 for timeout, -1 for error
//! The sequence number in pcMessage is changed
//! Precondition: NetworkManager is running
int CServerMT::SendQuery(const string& sToAddress, int iToPort,
			 char* pcMessage, int iLen, char** pcResponse)
{
	DB_ENTER(CServerMT::SendQuery);

	int iRet = m_pQuery->SendTo(sToAddress, iToPort,
	  			    pcMessage, iLen, pcResponse,
                    0, 1000 * 1000 * 1000); //100 ms

	if (iRet == 0) {
		//Timeout. Retry
		iRet = m_pQuery->SendTo(sToAddress, iToPort,
	  				pcMessage, iLen, pcResponse,
                        0, 1000 * 1000 * 1000); //400 ms

		
		if (iRet == 0) {
			//Timeout. Retry
			iRet = m_pQuery->SendTo(sToAddress, iToPort,
		  				pcMessage, iLen, pcResponse,
                            0, 1600 * 1000 * 1000); //1600 ms
		}
	}
		
	DB("Send Query returned: " << iRet);
	return iRet;
}

//! Choose state server
//! Returns true if there is a state server
//!   Assigns value to m_sServerAddress and m_iServerPort
//! Returns false if there are no state servers
//! Precondition: NetworkManager is running
bool CServerMT::ChooseStateServer(int iHash, string& sServerAddress, int& iServerPort)
{
	DB_ENTER(CServerMT::ChooseStateServer);

	return m_pStateServerSelector->ChooseStateServer(iHash, sServerAddress, iServerPort);
}

//! Set state server as down
//! Usually called after a query times out
//! May send message to State Server and write to database
//! Returns false if database write failed or if server address is unrecognized
//! Precondition: NetworkManager is running
bool CServerMT::SetStateServerDown(const string& sServerAddress, int iServerPort)
{
	DB_ENTER(CServerMT::SetStateServerDown);

	return m_pStateServerSelector->SetStateServerDown(sServerAddress, iServerPort);
}

//! Get start time
time_t CServerMT::GetStartTime() const
{
	DB_ENTER(CServerMT::GetStartTime);

	return m_tStart;
}

//Run
//Configuration file is input
//Blocks until Server stops
void CServerMT::Run(CCoreConfigFile* pConf)
{
	DB_ENTER(CServerMT::Run);

	pthread_mutex_lock(&m_StopMutex);
		
	if (m_bRunning || m_bStopRequested) {
		m_bStopRequested = false;
		pthread_mutex_unlock(&m_StopMutex);
		return;
	}

	m_pConfig = pConf;

        m_s3DESKey = Crypt::get_passwd(pConf->m_sPassFile, "3des");

	LOGL(5,"retrieving 3DES key from passfile " << pConf->m_sPassFile << ": " << m_s3DESKey);

	//Use subclass of CMessageProxy to handle different message types
	CMessageProxy* pMessageProxy = CreateMessageProxy();
	if (pMessageProxy == NULL) {
		cout << "Unable to create Message Proxy" << endl;
		cerr << "Unable to create Message Proxy" << endl;
		DB_AND_LOG("Unable to create Message Proxy");
		DB_AND_LOG("Abort Server");
		pthread_mutex_unlock(&m_StopMutex);
		return;
	}

	DB("Initialize Sockets");

	//Initialize sockets
	ISocket::SetSendBufferSize(pConf->m_iSendBufferSize);

	TLSSocket::SetKeyInfo(pConf->m_sPassFile, pConf->m_sCertUser,
			      pConf->m_sCertFile, pConf->m_sCertKeyFile);

	DB("Create Network Manager");
	CNetworkManager networkManager;
	m_pNetworkManager = &networkManager;

	//Initialize Network Manager
	DB("Initialize Network Manager");

	networkManager.SetMessageProxy(pMessageProxy);

       	//Set maximum client connections. Default: 10000 
        networkManager.SetMaximumClientConnections(pConf->m_iTCPConnections);

        //Set maximum receive packet size. Default: 65536
	networkManager.SetMaxReceivePacketSize(pConf->m_iRecvBufferSize);

	if (!SetupPorts()) {
		cout << "Unable to setup ports" <<endl;
		cerr << "Unable to setup ports" <<endl;
		DB_AND_LOG("Unable to setup ports");
		DB_AND_LOG("Abort Server");
		pthread_mutex_unlock(&m_StopMutex);
		delete pMessageProxy;
		return;
	}

	DB("Set log file size and count");

	LOG_MAX_FILE_SIZE(pConf->m_iLogMaxFileSize);
	LOG_MAX_FILE_COUNT(pConf->m_iLogMaxFileCount);

	DB("Create UDP Query");

	CSyncUDPQuery query(pConf->m_sPrivateIP, pConf->m_iUdpQueryPort, 
			    pConf->m_iNumThreads + 1);
	query.Start();
	m_pQuery = &query;

	if (!FinalSetup()) {
		DB_AND_LOG("Final setup failed");
		DB_AND_LOG("Abort Server");
		pthread_mutex_unlock(&m_StopMutex);
		delete pMessageProxy;
		Cleanup();
		return;
	}

	DB_AND_LOG("Start Network Manager");

    networkManager.Start();


	DB_AND_LOG("Server running");

	m_bRunning = true;


	pthread_mutex_unlock(&m_StopMutex);

	//Block until Network Manager stops
	networkManager.WaitForShutdown();

	DB_AND_LOG("Server stopped running");

	Cleanup();

	m_pNetworkManager = NULL;
	delete pMessageProxy;

	pthread_mutex_lock(&m_StopMutex);
	m_bRunning = false;
	pthread_mutex_unlock(&m_StopMutex);
	
	query.Stop();
}

//Stop
void CServerMT::Stop()
{
	DB_ENTER(CServerMT::Stop);

	pthread_mutex_lock(&m_StopMutex);

	if (m_bRunning && !m_bStopRequested) {
		m_pNetworkManager->Stop();
	}
	m_bStopRequested = true;
	
	pthread_mutex_unlock(&m_StopMutex);
}

//Get status of open ports
string CServerMT::GetPortInformation()
{
	DB_ENTER(CServerMT::GetPortInformation);

	string sStats;

	pthread_mutex_lock(&m_StopMutex);

	if (m_bRunning) {
		sStats = m_pNetworkManager->GetPortInformation();

		// include state-server status
		sStats += m_pStateServerSelector->GetPortInformation();
		sStats += "\n================\n";
	}
	else {
		sStats = "Server is not running. No active sockets.";
	}
	pthread_mutex_unlock(&m_StopMutex);

	return sStats;
}// string CServerMT::GetPortInformation()

//! Get number of connections;
int CServerMT::GetNumConnections()
{
	DB_ENTER(CServerMT::GetNumConnections);

	int iNumConnections = 0;

	pthread_mutex_lock(&m_StopMutex);

	if (m_bRunning) {
		iNumConnections = m_pNetworkManager->GetNumConnections();
	}

	pthread_mutex_unlock(&m_StopMutex);

	return iNumConnections;
}

//! Create Message Proxy
CMessageProxy* CServerMT::CreateMessageProxy()
{
	return new CMessageProxy;
}

//! Set up server ports
//! Called in Run()
//! Returns true if successful
bool CServerMT::SetupPorts()
{
	DB_ENTER(CServerMT::SetupPorts);

        //Add TCP Server for Local Telnet Admin
	if (!m_pNetworkManager->AddTcpServer((string)"127.0.0.1", m_pConfig->m_iTelnetAdminPort, 
		ADMIN, m_pConfig->m_iTCPConnections)) {
		cerr << "Unable to add Local Telnet Admin Server" << endl;
		DB_AND_LOG("Unable to add Local Telnet Admin Server");
		return false;
	}

/*
        //Add TCP Server for Remote Telnet Admin
       	 if (!m_pNetworkManager->AddTcpServer(m_pConfig->m_sIP, m_pConfig->m_iTelnetAdminPort, 
		ADMIN, m_pConfig->m_iTCPConnections)) {
		cerr << "Unable to add Remote Telnet Admin Server" << endl;
		DB_AND_LOG("Unable to add Remote Telnet Admin Server");
		return false;
	}
*/

	//if (m_pConfig->m_sIP != m_pConfig->m_sPrivateIP) {
	        //Add TCP Server for Private IP Telnet Admin
       		 if (!m_pNetworkManager->AddTcpServer(m_pConfig->m_sPrivateIP, m_pConfig->m_iTelnetAdminPort, 
			ADMIN, m_pConfig->m_iTCPConnections)) {
			cerr << "Unable to add Private IP Telnet Admin Server" << endl;
			DB_AND_LOG("Unable to add Private IP Telnet Admin Server");
			return false;
		}
	//}

        //Add UDP Server for Forwarding Messages 
	if (!m_pNetworkManager->AddUdpServer(m_pConfig->m_sPrivateIP, 
		m_pConfig->m_iForwardMessageUdpPort, FORWARD_MESSAGES)) {
		cerr << "Unable to add Message Forwarding UDP Server" << endl;
		DB_AND_LOG("Unable to add Message Forwarding UDP Server");
		return false;
	}

        //Add TCP Server for Forwarding Messages 
	if (!m_pNetworkManager->AddTcpServer(m_pConfig->m_sPrivateIP, 
		m_pConfig->m_iForwardMessageTcpPort, FORWARD_MESSAGES, 
		m_pConfig->m_iTCPConnections)) {
		cerr << "Unable to add Message Forwarding TCP Server" << endl;
		DB_AND_LOG("Unable to add Message Forwarding TCP Server");
		return false;
	}

	return true;
}

//! Final setup
//! Called in Run() just before starting Network Manager
//! Returns true if successful
bool CServerMT::FinalSetup()
{
	DB_ENTER(CServerMT::FinalSetup);

	return true;
}

//! Cleanup
//! Called in Run() just after shutting down Network Manager
void CServerMT::Cleanup()
{
	DB_ENTER(CServerMT::Cleanup);

	if (m_pStateServerSelector != NULL) {	
		//delete m_pStateServerSelector;
		m_pStateServerSelector = NULL;
	}
}

const string& CServerMT::Get3DESKey() const
{
	DB_ENTER(CServerMT::Get3DESKey);

	return m_s3DESKey;
}


const CNetworkManager* CServerMT::getNetworkManager() const
{
  DB_ENTER(CServerMT::getNetworkManager);

  return m_pNetworkManager;
}// CNetworkManager* CServerMT::getNetworkManager() const


