// ---------------------------------------------------------------------------
// File:       CloseConnectionMessage.cc
//
// Copyright:  Eyeball Networks Inc. 1999-2006
//
// Purpose:    Tell server to close a TCP or TLS connection
//
// Change Log: 03/01/2006 - Created
// ---------------------------------------------------------------------------

#include "CloseConnectionMessage.h"
#include "Socket.h"

#if defined(WIN32) || defined(_WIN32_WCE)
#include "Base.h"  // automatic use of precompiled headers
#endif

#include "debug.h"

CloseConnectionMessage::CloseConnectionMessage()
:
	CommandMessage(EYEBALL_SERVER_CLOSE_CONN),

	m_u32Address(0),
	m_u16Port(0),
	m_u8Protocol(0),
	m_u8ServerMessageType(0)
{
	DB_ENTER(CloseConnectionMessage::~CloseConnectionMessage);
	m_u16PayloadLength = 8;
}

CloseConnectionMessage::~CloseConnectionMessage()
{
	DB_ENTER(CloseConnectionMessage::~CloseConnectionMessage);
}

void CloseConnectionMessage::serialize(string& sBuf) const
{
	DB_ENTER(CloseConnectionMessage::serialize);

	SERIALIZE_BASE_CLASS(CommandMessage);

	SERIALIZE32(m_u32Address);
	SERIALIZE16(m_u16Port);
	SERIALIZE8(m_u8Protocol);
	SERIALIZE8(m_u8ServerMessageType);
}

int CloseConnectionMessage::deserialize(const string& sBuf)
{
	DB_ENTER(CloseConnectionMessage::deserialize);

	int iOffset = 0;

	DESERIALIZE_BASE_CLASS(CommandMessage);

	DESERIALIZE32(m_u32Address);
	DESERIALIZE16(m_u16Port);
	DESERIALIZE8(m_u8Protocol);
	DESERIALIZE8(m_u8ServerMessageType);

	return iOffset;
}

u_int16_t CloseConnectionMessage::payload_length(void) const
{
	return sizeof(m_u32Address) + sizeof(m_u16Port) +
	       sizeof(m_u8Protocol) + sizeof(m_u8ServerMessageType);
}

//Converts string to 32-bit integer address
//Returns false if address invalid or conversion failed
bool CloseConnectionMessage::setAddress(const string& sAddress)
{
	struct in_addr address;
	int result = inet_pton(AF_INET, sAddress.c_str(), &address);

	if (result) {
		m_u32Address = ntohl(address.s_addr);
		return true;
	}
	else {
		return false;
	}
}

//Returns address as string
string& CloseConnectionMessage::getAddress(string& sAddress) const
{
	struct in_addr address;
        memset(&address, 0, sizeof(address));
	address.s_addr = htonl(m_u32Address);

	char dst[INET_ADDRSTRLEN];
	if (inet_ntop(AF_INET, &address, dst, sizeof(dst)) != NULL) {
		sAddress = string(dst);
	}
	else {
		sAddress = string("");
	}

	return sAddress;
}

#ifndef _WIN32_WCE
std::ostream& operator<<(ostream& s, const CloseConnectionMessage& v)
{
	string sProtocol;
	if (v.m_u8Protocol == UDP) {
		sProtocol = "UDP";
	}
	else if (v.m_u8Protocol == TCP) {
		sProtocol = "TCP";
	}
	else if (v.m_u8Protocol == TLS) {
		sProtocol = "TLS";
	}
	else {
		sProtocol = "Unknown";
	}

	string sAddress;
	v.getAddress(sAddress);

	s << (CommandMessage)v
		<< " " << sAddress 
		<< ":" << v.m_u16Port << "/" << sProtocol.c_str() 
		<< "." << v.m_u8ServerMessageType;

	return s;
}
#endif
