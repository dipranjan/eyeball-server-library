

/*
  $Id: DNSService.cc,v 1.10 2006/08/23 00:15:38 zionk Exp $
*/


#include <debug.h>
#include <log.h>

#include <DnsResolve.h>

#include <processor_defines.h>
#include "Socket.h"
#include "string_utils.h"

#include "CoreConfigFile.h"
#include "DNSService.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif


static bool bCacheInit = false;
HashTableMT<DNSCacheEntry> DNSService::m_DNSCache;



DNSService::DNSService()
{
  if (!bCacheInit) {
    bCacheInit = true;
    m_DNSCache.init(16);
  }
}// DNSService::DNSService()



DNSService::~DNSService()
{

}// DNSService::~DNSService()




//! determine IP address of given hostname, return DNS_HOSTNAME on success, DNS_FAILURE otherwise
DnsResultType DNSService::_lookupHostname(const string &sHostname, string &IP, int &port, int &protocol)
{
  DB_ENTER(DNSService::_lookupHostname);
  DnsResultType result = DNS_FAILURE;

  struct hostent *host;
  host = gethostbyname(sHostname.c_str());
  if(host == NULL)
    {
      DB_AND_LOGL(4, "host for domain "<<sHostname<<" not found");
		  
      result = DNS_FAILURE;
    }
  else
    {
      struct in_addr in;
      memcpy(&in.s_addr, host->h_addr_list[0], host->h_length);
      
      char dst[INET_ADDRSTRLEN];
      if (inet_ntop(AF_INET, &in, dst, INET_ADDRSTRLEN) != NULL) {
	IP = (string)dst;
      }
      else {
        IP = (string)"";
      }
      
      port     = 5060; // choose 5060 by default
      protocol = UDP;  // choose UDP by default
      
      result = DNS_HOSTNAME;
      
      DB("found host "<<IP<<":"<<port<<"/"<<protocol);
      
      //      LOGL(5,"result from lookup for host "<<sHostname<<": "<<IP<<":"<<itos(port));
    }

  return result;

}// DnsResultType DNSService::_lookupHostname(const string &sHostname, string &IP, int &port, int &protocol)



DnsResultType DNSService::Lookup(const string& sDomain, const string& sService, int iProtocol,
				 string& sFoundAddress, int& iFoundPort, int& iFoundProtocol)
{
  DB_ENTER(DNSService::request);

  bool bUseCache = g_pCoreConf->m_bDNSCache;
  DNSCacheEntry entry;
  bool bEntryFound = false;

  if (bUseCache) {
    if (m_DNSCache.getFromTable(sDomain, entry)) {
      bEntryFound = true;
      if (entry.m_Result == DNS_OK) {
        sFoundAddress  = entry.m_sIP;
        iFoundPort     = entry.m_iPort;
        iFoundProtocol = entry.m_iProtocol;
        return DNS_OK;
      }
      else {
        DB(sDomain << " hostname lookup only");
      }
    }
    else {
      DB(sDomain << " not in the cache");
    }
  }

  DnsResultType result = DNS_FAILURE;
  bool finished = false;
  DnsResolve resolve;

  int count;
  AddressPortPair addrport;
  string sProtocol;

  iFoundProtocol = iProtocol;
  if (iProtocol == UDP) {
    sProtocol = "udp";
  }
  else if (iProtocol == TCP) {
    sProtocol = "tcp";
  }
  else if (iProtocol == TLS) {
    sProtocol = "tls";
  }
  else {
    DB_AND_LOGL(3, "Invalid protocol in DNS request: " << iProtocol);
    return DNS_FAILURE;
  }

  while (finished==false)
    {
      DB("resolve.Lookup");
      count = resolve.Lookup(sDomain,sService,sProtocol);
      DB("resolve.Lookup returned");
      if (count>0)
	{
	  struct in_addr inaddr;
      
	  resolve.GetResult(0,addrport);
      
	  inaddr.s_addr     = htonl(addrport.first);

	  char buf[INET_ADDRSTRLEN];
	  inet_ntop(AF_INET, &(inaddr.s_addr), buf, INET_ADDRSTRLEN);
	  sFoundAddress = buf;

	  iFoundPort = addrport.second;

	  result = DNS_OK;

	  LOGL(5,"result of DNS SRV lookup for domain "<<sDomain<<": "
		 <<sFoundAddress<<":"<<itos(iFoundPort)<<"/"<<sProtocol);

	  finished = true;
	}
      else if ((iFoundProtocol==TLS)||(iFoundProtocol==TCP))
	{
	  if (iFoundProtocol==TLS)
	    {
	      sProtocol = "tcp";
	      iFoundProtocol = TCP;
	    }
	  else
	    {
	      sProtocol = "udp";
	      iFoundProtocol = UDP;
	    }
	}
      else
	finished = true; // no more options, bail out

    }

  if (result==DNS_FAILURE)
    {
      if (bEntryFound) {
        sFoundAddress  = entry.m_sIP;
        iFoundPort     = entry.m_iPort;
        iFoundProtocol = entry.m_iProtocol;
        return DNS_HOSTNAME;
      }
      else {
        result = _lookupHostname(sDomain,sFoundAddress,iFoundPort,iFoundProtocol);
      }
    }

  if (((result == DNS_OK) || (result == DNS_HOSTNAME)) && bUseCache)
    {
      DNSCacheEntry *pEntry;

      pEntry = new DNSCacheEntry;
      pEntry->m_sIP        = sFoundAddress;
      pEntry->m_iPort      = iFoundPort;
      pEntry->m_iProtocol  = iFoundProtocol;
      pEntry->m_Result     = result;

      m_DNSCache.updateTable(sDomain,pEntry);
    }

  return result;

}// DnsResultType DNSService::Lookup(const string& sDomain, const string& sService, int iProtocol,
 //				     string& sFoundAddress, int& iFoundPort, int& iFoundProtocol)




/*
  $Log: DNSService.cc,v $
  Revision 1.10  2006/08/23 00:15:38  zionk
  Fix infinite loop when DNS SRV lookup fails with TCP

  Revision 1.9  2006/07/13 22:10:33  zionk
  Use thread-safe getFromTable()

  Revision 1.8  2006/06/14 19:50:06  zionk
  don't use string buffer directly

  Revision 1.7  2006/05/12 22:10:45  zionk
  Move utils to util/string_utils

  Revision 1.6  2006/05/03 16:02:37  zionk
  Fix more compilation errors

  Revision 1.5  2006/04/21 17:47:00  zionk
  Use updateTable instead of addToTable

  Revision 1.4  2006/03/16 00:16:39  zionk
  initialized DNS cache

  Revision 1.3  2006/03/14 22:45:34  zionk
  Don't assign string directly when using inet_ntop

  Revision 1.2  2006/03/10 01:37:14  zionk
  Renamed request function as Lookup

  Revision 1.1  2006/03/09 22:27:44  zionk
  Modified from servercore. Synchronous request instead of asynchronous

  Revision 1.7  2006/03/02 20:55:16  larsb
  password changes now saved to correct filename

  Revision 1.6  2005/10/28 23:00:57  larsb
  *** empty log message ***

  Revision 1.5  2005/10/24 22:37:06  larsb
  *** empty log message ***

  Revision 1.4  2005/10/22 00:46:49  larsb
  added 100 Trying
  fixed IP:port routing problem

  Revision 1.3  2005/10/21 16:24:21  larsb
  modified configuration

  Revision 1.2  2005/10/19 22:05:18  larsb
  removed memory leak in send buffers

  Revision 1.1  2005/09/15 23:29:56  larsb
  *** empty log message ***

*/

