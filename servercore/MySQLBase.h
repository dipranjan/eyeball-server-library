#ifndef __MYSQL_BASE_H_
#define	__MYSQL_BASE_H_


#include <string>
#include <vector>
using std::string;
using std::vector;

#include <mysql++.h>

#define MYSQL_DEFAULT_TIMEOUT         2

#include <pthread.h>


class MySQLBase
{
 public:

  MySQLBase();
  virtual ~MySQLBase();

  int Open(const string& sDBName, vector<string>& sHosts, int iPort,
           const string& sUser, const string& sPasswordFile);

  void Close();

 protected:

  bool _reconnect();
  bool _isConnected();

  int _getTemplate(const string& sQuery, mysqlpp::Result& res, bool bRetry = true);
  int _putTemplate(const string& sQuery, bool bRetry = true);

  // all user-input strings should be escaped before used in queries
  string escape(const string& s);

 protected:

  mysqlpp::Connection *m_pCon;
  pthread_mutex_t m_ReconnectMutex;

  string m_sDBName;
  vector<string> m_sHosts;
  int m_iPort;
  string m_sUser;
  string m_sPassword;
  int m_iCurrHostIndex;

  int m_iLastReconnectAttempt;
  bool dbWorking;
};



#endif


