// XMPPResourceListMessage.cc
// Interface for the XMPPResourceListMessage class

#include "XMPPResourceListMessage.h"
#include "debug.h"

XMPPResourceListMessage::XMPPResourceListMessage()
: QueryMessage(EYEBALL_SERVER_XMPP_RESOURCE_LIST)
{
	DB_ENTER(XMPPResourceListMessage::XMPPResourceListMessage);

}

XMPPResourceListMessage::~XMPPResourceListMessage()
{
	DB_ENTER(XMPPResourceListMessage::~XMPPResourceListMessage);

}

int XMPPResourceListMessage::deserialize(const string& buf)
{
  DB_ENTER(XMPPResourceListMessage::deserialize);

  m_ResourceList.clear();
  m_ResourceStateList.clear();
  m_ResourceRequestedRosterList.clear();

  int iOffset = 0;

  iOffset += QueryMessage::deserialize(buf);

  if (!DeserializeString16(m_sName, buf, iOffset)) return -1;

  if (!DeserializeStringVector16(m_ResourceList, buf, iOffset)) return -1;
  if (!DeserializeStringVector16(m_ResourceStateList, buf, iOffset)) return -1;
  if (!Deserialize8Vector16(m_ResourceRequestedRosterList, buf, iOffset)) return -1;

  int iNumResources = m_ResourceList.size();
  int iNumState = m_ResourceStateList.size();
  int iNumRoster = m_ResourceRequestedRosterList.size();

  if (iNumResources != iNumState) {
    if (iNumState > iNumResources) {
      m_ResourceStateList.resize(iNumResources);
    }
    else {
      for (int i = iNumState; i < iNumResources; i++) {
        m_ResourceStateList.push_back("");
      }
    }
  }

  if (iNumResources != iNumRoster) {
    if (iNumRoster > iNumResources) {
      m_ResourceStateList.resize(iNumResources);
    }
    else {
      for (int i = iNumRoster; i < iNumResources; i++) {
        m_ResourceRequestedRosterList.push_back(0);
      }
    }
  }

  return iOffset;
}

void XMPPResourceListMessage::serialize(string& buf) const
{
  DB_ENTER(XMPPResourceListMessage::serialize);

  QueryMessage::serialize(buf);

  // serialize message contents
  SerializeString16(m_sName, buf);
  SerializeStringVector16(m_ResourceList, buf);
  SerializeStringVector16(m_ResourceStateList, buf);
  Serialize8Vector16(m_ResourceRequestedRosterList, buf);
}

u_int16_t XMPPResourceListMessage::payload_length(void) const
{
  DB_ENTER(XMPPResourceListMessage::payload_length);

  u_int16_t iLength = QueryMessage::payload_length() + 
                      m_sName.size() + sizeof(u_int16_t) + 
                      GetSerializeStringVector16Length(m_ResourceList) +
                      GetSerializeStringVector16Length(m_ResourceStateList) +
                      GetSerialize8Vector16Length(m_ResourceRequestedRosterList);
  return iLength; 
}

ostream& operator<<(ostream& s, const XMPPResourceListMessage& v)
{
	int iSize = v.m_ResourceList.size();
	int iSize2 = v.m_ResourceStateList.size();
	int iSize3 = v.m_ResourceRequestedRosterList.size();

        s << (QueryMessage)v
                << " " << v.m_sName << " " << iSize;

	for (int i = 0; i < iSize; i++) {
		s << " " << v.m_ResourceList[i];
		if (i < iSize2) {
			s << " " << v.m_ResourceStateList[i];
		}
		else {
			s << " none";
		}
		if (i < iSize3) {
			s << " " << v.m_ResourceRequestedRosterList[i];
		}
		else {
			s << " 0";
		}
	}

        return s;
}


