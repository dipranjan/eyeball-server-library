/************************************************************/
/*                                                          */
/*                                                          */
/*                                                          */
/*                                                          */
/*                                                          */
/* udp tcp tls socket classes                               */
/* uses openssl to implement tls                            */
/* author: larsb                                            */
/*                                                          */
/************************************************************/
/*
  $Id: Socket.h,v 1.7 2007/05/15 16:22:49 zionk Exp $
*/


#ifndef _SOCKET_H
#define _SOCKET_H

#define SSLV23

#include <ostream>
#include <list>

#include <netinet/in.h>
#include <netinet/in_systm.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netdb.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>

#include <string>
using std::string;

#include <errno.h>
namespace ossl
{
#include <openssl/ssl.h>
#include <openssl/bio.h>
#include <openssl/err.h>
}


#include "IntMT.h"


#define MAX_MSGSIZE  65536



// protocol connection types

#define TCP                      1
#define TLS                      2
#define UDP                      3
#define TELNET                   4


// diverse
#define DEFAULT_RETRY            3





//! error codes

#define SOCK_OK                  0

#define SOCK_INVALID_PARAMETERS -1
#define SOCK_NOT_IMPLEMENTED    -2
#define SOCK_CLOSED             -3
#define SOCK_NO_BUFFER          -4
#define SOCK_ERR                -5
#define SOCK_WOULDBLOCK         -6


#define TCP_OK                   0
#define TLS_OK                   0


#define TLS_CLOSED              -1
#define TCP_CLOSED              -1
#define TLS_INVALID_PARAMETERS  -2
#define TCP_INVALID_PARAMETERS  -2
#define TLS_ERROR               -3
#define TLS_INTERNAL            -1000

//! TCP connection states
#define TCP_CONNECTED            1
#define TCP_WAIT_CONNECT         2

//! TLS connection states
#define TLS_ACCEPTED             1
#define TLS_WAIT_ACCEPT          2

#define TLS_CONNECTED            1
#define TLS_SOCKET_CONNECTED     2
#define TLS_SOCKET_WAITING       3



using namespace std;



/*
typedef struct 
{
  string m_DestIP;
  short  m_DestPort;
  char  *m_pBuffer;
  int    m_iBufLen;
} MsgBuffer;
*/

class MsgBuffer
{
 public:
  string m_DestIP;
  short  m_DestPort;
  char  *m_pBuffer;
  int    m_iBufLen;

 public:

  MsgBuffer();
  
  MsgBuffer(const MsgBuffer &buf);
    
  MsgBuffer& operator=(const MsgBuffer &buf);

};


typedef std::list<MsgBuffer> MsgBufferList;



class ISocket
{

 protected:

  MsgBufferList m_MsgBufferList;
  unsigned int m_iMsgBufferSize;

 protected:

  virtual int _appendBuffer(const char *pBuffer, int iSize, const string& sIP, int iPort);
  void _ReduceMsgBufferSize(int iSize);

 public:

  virtual ~ISocket() {};

  virtual void setAllowedIPs(int iAllowedIPs, char **szAllowedIPs) = 0;

  virtual int createSocket(int type, const string& addr, int port) = 0;

  virtual int sendTo(const char *pBuffer, int iBufferSize, const string& addr, int port) = 0;

  virtual int send(const char *pBuffer, int iBufferSize) = 0;
 
  virtual int sendBuffered() = 0;

  virtual int recvFrom(char *pBuf, int iBufLen, string &addr, int &port) = 0;

  virtual ISocket* accept() = 0;

  virtual int connect(const string &addr, int port) = 0;

  virtual int close() = 0;

  virtual int getFd() = 0;

  virtual string getRemoteAddr() = 0;

  virtual int getRemotePort() = 0;

  virtual string getLocalAddr() = 0;

  virtual int getLocalPort() = 0;

  virtual int getProtocol() = 0;

  virtual char* getProtocolString() = 0;

protected:
  static int m_iSendBufferSize;

public:
  //Set Send Buffer Size. Default 10 KB
  static void SetSendBufferSize(int iSize);

private:
  static CIntMT m_iGlobalSendBufferUsage;
  const static int m_iGlobalSendBufferMaxSize;
};





class UDPSocket : public ISocket
{

 protected:

  int m_socketfd;

  struct sockaddr_in m_Addr;

  string m_LocalAddr;
  int m_LocalPort;

  string m_AddrBuffer;
  int m_PortBuffer;

 public:

  UDPSocket();

  virtual ~UDPSocket();

  virtual void setAllowedIPs(int iAllowedIPs, char **szAllowedIPs) {};

  //! create a new socket; by default, the socket is set to non-blocking and reusing the address and port
  virtual int createSocket(int type, const string& addr, int port);
  
  virtual int sendTo(const char *pBuffer, int iBufferSize, const string& addr, int port);

  virtual int send(const char *pBuffer, int iBufferSize);

  virtual int sendBuffered();

  //! received data from socket and copy senders address and port to parameters
  virtual int recvFrom(char *pBuf, int iBufLen, string& addr, int &port);

  virtual UDPSocket* accept() { return 0; }

  virtual int connect(const string &addr, int port) { return 0; }

  virtual int close();

  virtual int getFd();

  virtual string getRemoteAddr() { return ""; }

  virtual int getRemotePort() { return 0; }

  virtual string getLocalAddr() { return m_LocalAddr; }

  virtual int getLocalPort() { return m_LocalPort; }

  virtual int getProtocol() { return UDP; }

  virtual char* getProtocolString() { return "UDP"; }
};




class TCPSocket : public ISocket
{

 protected:

  int m_socketfd;

  bool m_bIsClient;

  string m_RemoteAddr;
  int m_RemotePort;

  string m_LocalAddr;
  int m_LocalPort;

  int m_ConnectState;

  struct sockaddr_in m_ServAddr;  // the address of the socket (if it is a server)

  int     m_iAllowedRemoteIPs;    // number of allowed remote IPs
  char    **m_szAllowedRemoteIPs; // array of allowed remote IPs

 protected:

  //! transfers a TCP socket to a new Socket and frees old TCP socket (used by TLSSocket)
  TCPSocket(TCPSocket* tcpSocket);
  bool _checkAllowedRemoteIP(char *szRemoteIP);

 public: 

  TCPSocket();
  TCPSocket(int fd);

  virtual ~TCPSocket();

  virtual void setAllowedIPs(int iAllowedIPs, char **szAllowedIPs);
  
  //! creates a server socket which is listening
  virtual int createSocket(int type, const string& addr, int port);

  //! creates a server socket which is listening
  virtual int createSocket(int type, const string& addr, int port, int iBacklog);

  virtual int sendTo(const char *pBuffer, int iBufferSize, const string& addr, int port);

  virtual int send(const char *pBuffer, int iBufferSize);
 
  virtual int sendBuffered();

  virtual int recvFrom(char *pBuf, int iBufLen, string &addr, int &port);

  virtual int close();

  virtual int getFd();

  virtual TCPSocket* accept();

  virtual int connect(const string &addr, int port);

  virtual string getRemoteAddr();

  virtual int getRemotePort();

  virtual string getLocalAddr() { return m_LocalAddr; }

  virtual int getLocalPort() { return m_LocalPort; }

  virtual int getProtocol() { return TCP; }

  virtual char* getProtocolString() { return "TCP"; }
};



class TLSSocket : public TCPSocket
{

 protected:

  //! the openSSL context
  ossl::SSL_CTX *m_CTX;

  //! the openSSL member
  ossl::SSL *m_SSL;

  int m_AcceptState;
  int m_ConnectState;
  bool m_bCTXInitiated;
  int m_iConnectionTry;

 protected:

  //! initialize the underlying TLS for servers
  int _initTLS();

  //! initialize TLS for a client socket
  int _initClientTLS();

  //! connect SSL with connected socket
  int _connectSSL();

  //! accept the SSL connection for accepted socket
  int _acceptSSL();

  //! returns error codes as strings
  static const char* _getErrorText(ossl::SSL* ssl, int err);

  //! handle an OpenSSL error
  int _handleError(int err_code, const char *caller);

 public:

  TLSSocket();

  //! converts a connected TCP socket to TLS and frees TCP socket
  TLSSocket(TCPSocket* tcpSocket);

  virtual ~TLSSocket();

  //! creates a server socket which is listening
  virtual int createSocket(int type, const string& addr, int port, int iBacklog);

  virtual int send(const char *pBuffer, int iBufferSize);

  virtual int sendTo(const char *pBuffer, int iBufferSize, const string& addr, int port);
 
  virtual int sendBuffered();

  virtual int recvFrom(char *pBuf, int iBufLen, string &addr, int &port);

  virtual int close();

  virtual TLSSocket* accept();

  virtual int connect(const string &addr, int port);

  virtual int getProtocol() { return TLS; }

  virtual char* getProtocolString() { return "TLS"; }

  virtual void shutdownTLS();

protected:
  static string m_sPassFile;
  static string m_sCertUser;
  static string m_sCertFile;
  static string m_sCertKeyFile;

  static int pem_passwd_cb(char *buf, int size, int rwflag, void *userdata);

public:
  //Sets static variables
  static void SetKeyInfo(const string& sPassFile, const string& sCertUser, 
			 const string& sCertFile, const string& sCertKeyFile); 

};




#endif


/*
  $Log: Socket.h,v $
  Revision 1.7  2007/05/15 16:22:49  zionk
  Begin modifications to servercoremt for XMPP server-to-server communication

  Revision 1.6  2006/08/24 00:09:59  zionk
  Add global send buffer (10 MB). Fix spurious errors in sendbuffered (wrong
  return values, continuing to next stream segment instead of breaking...)

  Revision 1.5  2006/08/02 01:00:07  mchung
  remove extra qualifier

  Revision 1.4  2006/07/27 20:51:27  zionk
  Use const string& parameters

  Revision 1.3  2006/05/29 16:35:11  zionk
  Add errno.h -- probably defined inside namespace ossl

  Revision 1.2  2006/05/26 00:04:03  zionk
  Don't need ; after namespace n {}

  Revision 1.1  2006/03/07 18:12:00  zionk
  Multi-threaded server core

  Revision 1.11  2005/10/24 19:21:13  larsb
  *** empty log message ***

  Revision 1.10  2005/10/21 16:24:21  larsb
  modified configuration

  Revision 1.9  2005/10/20 02:07:35  larsb
  *** empty log message ***

  Revision 1.8  2005/10/19 22:05:18  larsb
  removed memory leak in send buffers

  Revision 1.7  2005/08/13 00:40:43  larsb
  *** empty log message ***

  Revision 1.6  2005/08/05 19:34:55  larsb
  *** empty log message ***

  Revision 1.5  2005/07/22 01:59:37  larsb
  *** empty log message ***

  Revision 1.4  2005/07/21 18:50:38  larsb
  *** empty log message ***

  Revision 1.3  2005/07/16 00:56:02  larsb
  *** empty log message ***

  Revision 1.2  2005/07/14 22:11:11  larsb
  bugfix: accepted socket did not inherit O_NONBLOCK from parent

  Revision 1.1  2005/07/13 00:34:29  larsb
  core files for servers

  Revision 1.6  2005/07/09 00:18:43  larsb
  *** empty log message ***

  Revision 1.5  2005/07/04 23:18:29  larsb
  replaced old sip parsing; TCP working with MSN client

  Revision 1.4  2005/06/29 01:10:38  larsb
  bugfixes: TCP working

  Revision 1.3  2005/06/28 17:02:48  larsb
  added SSL/TLS support (see README.ssl)
  did some cleanup

  Revision 1.2  2005/06/22 16:27:44  larsb
  *** empty log message ***

  Revision 1.1  2005/06/20 19:39:33  larsb
  new structure for socket and message processing

*/
