// XmppBlockMessage.h
// Interface for the XMPPBlockMessage class

#ifndef XMPP_BLOCK_MESSAGE_H
#define XMPP_BLOCK_MESSAGE_H

#include "QueryMessage.h"
#include "XMPPBlockItem.h"

#include <vector>
using std::vector;

class XMPPBlockMessage : public QueryMessage
{
public:
  XMPPBlockMessage();
  ~XMPPBlockMessage();

  virtual int deserialize(const string& buf);
  virtual void serialize(string& buf) const;

  virtual u_int16_t payload_length(void) const;

  unsigned int getNumBlock() const;

  XMPPBlockItem getBlock(int iIndex);
  void addBlock(const XMPPBlockItem& block);

  void sortAscendingOrder();

#ifndef _WIN32_WCE
  friend ostream& operator<<(ostream& s, const XMPPBlockMessage& v);
#endif

public:
  string m_sName;
  string m_sBlockName;
  vector<XMPPBlockItem> m_Block;

};

#endif//XMPP_BLOCK_MESSAGE_H
